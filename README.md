# Java Notes

<img src="https://www.noroff.no/images/docs/vp2018/Noroff-logo_STDM_vertikal_RGB.jpg" alt="banner" width="450"/>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)
[![web](https://img.shields.io/static/v1?logo=icloud&message=Online&label=web&color=success)](https://noroff-accelerate.gitlab.io/java/course-notes/)

> Course Notes for the Noroff Accelerate Course in Java

## Table of Contents

- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)
- [License](#license)

## Install

```sh
$ npm install
```

## Usage

```sh
$ npm start
```

## Maintainers

[Craig Marais (@muskatel)](https://gitlab.com/muskatel)

[Dean von Schoultz (@Deanvons)](https://gitlab.com/Deanvons)

[Dewald Els (@sumodevelopment)](https://gitlab.com/sumodevelopment)

[Greg Linklater (@EternalDeiwos)](https://www.noroff.no/en/contact/staff/53-academic/347-greg-linklater)

[Nicholas Lennox (@NicholasLennox)](https://gitlab.com/NicholasLennox)

[Sean Skinner (@SeanSkinner)](https://gitlab.com/SeanSkinner)

## Contributing

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

## License

Copyright 2021, Noroff Accelerate AS
