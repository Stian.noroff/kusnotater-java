# SQL to REST
This lesson teaches the process of exposing data from a data store to a web browser in a RESTful way. The concept of request-response is discussed with a focus on extracting information from requests to produce expected responses. This lesson is largely practical with a walkthrough of how to approach the problem, what to ask oneself, and how to execute those ideas.

**Learning objectives:**

You will be able to...
- **explain** the process of converting data from a datastore into information consumable over http.
- **create** model classes to represent data, given a database.
- **organize** database information into models, given models and a database.
- **demonstrate** exposing information over REST endpoints, given a requirements description and a set of models 
- **organize** related endpoints into controllers.

---

This lesson covers a relatively common example of already having a database and wanting to create a REST API from it.

> For this example we will use again use the SQLite3 version of the Northwind database ([Northwind_small.sqlite](https://github.com/jpwhite3/northwind-SQLite3)).

It is important to remember that we should actually be using a hosted SQL database, but for the class the SQLite3 file avoids some of the hosting complexities. (These will be covered in later lessons).

##Initial Project Setup

We will once again use [Spring Initializr](https://start.spring.io/) to create the basis of our project, this time we will call it `SQLtoREST`.

![Spring Initialzr Configuration](./resources/01_SpringInitialzr.png)

We have used the following dependencies:

- Spring Web
- Spring Boot DevTools (optional)

Generate the project and extract it to an appropriate location on your computer.

Next, create the `README.md` and modifying the `.gitignore` as follows.

Here are some suggestions for content for these files:

- `.gitignore` (This file may be hidden on your system): [[suggestion]](https://www.toptal.com/developers/gitignore/api/java,intellij,windows,macos,linux)

- `README.md` (change appropriately):

Download the `Northwind_small.sqlite` file and copy into `/src/main/resources` folder.

Using Maven, the project will then have the following structure (before opening it with IntelliJ):

```
 /SQLtoREST/
   │
   ├─ HELP.md
   │
   ├─ mvnw
   │
   ├─ mvnw.cmd
   │
   ├─ pom.xml
   │
   ├─ README.md
   │
   └─ src/
       │
       ├─ main
       │   │
       │   ├─ java/no/noroff/SQLtoREST/
       │   │   │
       │   │   └─ SqLtoRestApplication.java
       │   │ 
       │   └─ resources/
       │       │
       │       ├─ application.properties
       │       │
       │       ├─ Northwind_small.sqlite
       │       │
       │       ├─ static/
       │       │
       │       └─ templates/
       │
       └─ test/java/no/noroff/SQLtoREST/
           │
           └─ SqLtoRestApplicationTests.java
```

Open the project with IntelliJ and run the application. Navigate to [localhost:8080](http://localhost:8080/) using a web browser. You should once again see a _White Label Error_ page.

> If you do not see a white label error page, repeat the above steps.

We are now ready to start building the code to interact with the SQLite file and serve content from server endpoints.

## Adding to the pom.xml

To open the `Northwind_small.sqlite` file from Java we will need an appropriate JDBC, we can go ahead and add the `sqlite-jdbc` to the project by adding the following to the dependencies portion of the `pom.xml`:

```xml
<!-- https://mvnrepository.com/artifact/org.xerial/sqlite-jdbc -->
<dependency>
    <groupId>org.xerial</groupId>
    <artifactId>sqlite-jdbc</artifactId>
    <version>3.32.3.1</version> <!--Get the latest from Maven repository-->
</dependency>
```

When working with this `pom.xml` file you will also notice additional dependencies that were not included in the previous Spring Boot example.

## Creating classes

For this project we will be creating the `model` and `controller` portions of the MVC pattern.

To help us organize things better we will create a `controllers` package and a `models` package both inside the `no.noroff.SQLtoREST` package.

![Added packages](./resources/02_packages.png)

For now we will only be reading customer data, so we need to create the following two files:

- `models/Customer.java` 
- `controllers/CustomerController.java`

### Customer Model

Create the `Customer.java` file inside the models package, and add the following code:

```java
package no.noroff.SQLtoREST.models;

public class Customer {
    private String customerId;
    private String contactName;
    private String city;
    private String phone;

    public Customer(String customerId, String contactName, String city, String phone){
        this.customerId = customerId;
        this.contactName = contactName;
        this.city = city;
        this.phone = phone;
    }
    // Getters and setters ommited
}
```

### Loading data

The first thing we need is a place to store the connection string, this will be done in a `ConnectionHelper` class, where we have the connection string stored as a constant than can be globally accessed. 

```java
public class ConnectionHelper {
    public static final String CONNECTION_URL = "jdbc:sqlite::resource:Northwind_small.sqlite";
}
```

Now we need some methods to actually get the data from the database. This will be done in a `CustomerRepository` class.

> Calling our data access class a repository is just a naming convention used in Spring Data.

```java
public class CustomerRepository {
  // Setting up the connection object we need.
    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;
    ...
```

The steps to manipulate data are the same and follow the steps:

1. Open connection
2. Prepare a statement
3. Execute and handle the results
4. Return model with data inside

These steps will be shown for the `getCustomerById` and the `addCustomer` methods.

```java
public Customer getCustomerById(String custId){
  Customer customer = null;
  try{
    // Connect to DB
    conn = DriverManager.getConnection(URL);
    logger.log("Connection to SQLite has been established.");

    // Make SQL query
    PreparedStatement preparedStatement =
      conn.prepareStatement("SELECT Id,CompanyName, ContactName,Phone FROM customer WHERE Id = ?");
    preparedStatement.setString(1,custId);
    // Execute Query
    ResultSet resultSet = preparedStatement.executeQuery();

    while (resultSet.next()) {
      customer = new Customer(
        resultSet.getString("Id"),
        resultSet.getString("CompanyName"),
        resultSet.getString("ContactName"),
        resultSet.getString("Phone")
      );
    }
    logger.log("Select specific customer successful");
  }
  catch (Exception exception){
    logger.log(exception.toString());
  }
  finally {
    try {
      conn.close();
    }
    catch (Exception exception){
      logger.log(exception.toString());
    }
  }
  return customer;
}
```

```java
public Boolean addCustomer(Customer customer){
 Boolean success = false;
  try{
    // Connect to DB
    conn = DriverManager.getConnection(URL);
    logger.log("Connection to SQLite has been established.");

    // Make SQL query
    PreparedStatement preparedStatement =
      conn.prepareStatement("INSERT INTO customer(Id,CompanyName,ContactName,Phone) VALUES(?,?,?,?)");
    preparedStatement.setString(1,customer.getCustomerId());
    preparedStatement.setString(2,customer.getCompanyName());
    preparedStatement.setString(3,customer.getContactName());
    preparedStatement.setString(4,customer.getPhone());
    // Execute Query
    int result = preparedStatement.executeUpdate();
    success = (result != 0);
    logger.log("Add customer successful");
  }
  catch (Exception exception){
    logger.log(exception.toString());
  }
  finally {
   try {
    conn.close();
   }
   catch (Exception exception){
    logger.log(exception.toString());
   }
  }
  return success;
}
```

> These classes should look familiar from the JDBC section in the Spring Framework lesson. They are the same classes, repurposed.

### Customer Controller

Recall, controller handle our user interaction and they give orders to our model layer (the data access classes). The controllers should contain no domain logic (database manipulation) as it is not the controllers job.

A controller should cater for one domain object typically, this means we will have one `CustomerController` to handle all the interaction for the customers. 

> This doesnt mean every table in your database should be a controller, some domain object are used with others exclusively, this is a design choice in your hands. It is primarily driven by what your exposed endpoints will look like. 

For our purposes we will create 4 endpoints, which will correspond to the following actions:

- Getting all customers
- Getting a specific customer with different ways of extracting information
- Adding a new customer
- Updating an existing customer

> Our endpoints will be prefixed with `/api/` for navigation purposes

```java
@RestController
public class CustomerController {
    // We pass all the work to the repository
    private CustomerRepository customerRepository = new CustomerRepository();
```

The following code is for getting all customers.

```java
@RequestMapping(value="/api/customers", method = RequestMethod.GET)
public ArrayList<Customer> getAllCustomers(){
  return customerRepository.getAllCustomers();
}
```

The following code is for getting a specific customer, with various ways of extracting informaiton from the URI.

```java
/*
  This returns a specific customer, based on a given Id.
  We use a query string here to extract the Id.
  E.g. api/customer?id=ALFKI
  Seen as a BAD practive
*/
@RequestMapping(value = "api/customer", method = RequestMethod.GET)
public Customer getCustomerByQueryId(@RequestParam(value="id", defaultValue = "ALFKI") String id){
  return customerRepository.getCustomerById(id);
}
/*
  This returns a specific customer, based on a given Id.
  We use a header here to extract the Id.
*/
@RequestMapping(value = "api/customerheader", method = RequestMethod.GET)
public Customer getCustomerByHeaderId(@RequestHeader("id") String id){
  return customerRepository.getCustomerById(id);
}
/*
  This returns a specific customer, based on a given Id.
  We use a path variable here to extract the Id.
*/
@RequestMapping(value = "api/customers/{id}", method = RequestMethod.GET)
public Customer getCustomerByPathId(@PathVariable String id){
  return customerRepository.getCustomerById(id);
}
```

The following code is for adding a new customer, the customer information is extracted and automatically deserialized into our `Customer` object.

```java
/*
  This adds a new customer.
  It takes the new customer from the body of the request.
*/
@RequestMapping(value = "api/customers", method = RequestMethod.POST)
public Boolean addNewCustomer(@RequestBody Customer customer){
  return customerRepository.addCustomer(customer);
}
```

Finally, this code is for updating a customer. The functionality mentioned in the comment is a check we will do in a later lesson when working with Hibernate. The reason it exists here is because it is expected, as its a way to check if the body contains the correct object.

```java
/*
  This updates an existing customer.
  It takes the new customer data from the body of the request.
  As well as taking the Id of the customer from the path variables, this is
  to do a check if the body matches the id. Just a layer of saftey.
*/
@RequestMapping(value = "api/customers/{id}", method = RequestMethod.PUT)
public Boolean updateExistingCustomer(@PathVariable String id, @RequestBody Customer customer){
  return customerRepository.updateCustomer(customer);
}
```

## CORS

_Cross Origin Resource Sharing_ (CORS) is the process of one server providing content (resources) for a web page that is served by a different server. 

Setting up CORS correctly lets you specify (or restrict) which other domains may access the content provided. This gives you another layer of control over when/what can interact with your server.

This is increasingly common when building micro-services or using cloud platforms.

> _simple requests_ (certain request types or data types) do not trigger a CORS pre-flight check, and are thus not effected.
> 
> A more in-depth explanation can be found on [MDN CORS](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS)

### Implementing CORS

CORS can be quite a headache when you first encounter it, but from the server side (using Spring framework) it are relatively simple to set up. Simply mark the `Controller` or `Mapping` (or both) with the `@CrossOrigin` annotation:

```java
@CrossOrigin
@RestController
public class CustomerController {
  // other code ...
```
Marking the `Crontroller` as `@CrossOrigin` will do so for all the endpoints therein, whole the individual configurations on the end point will override those of the `Controller`. 

### CrossOrigin Attributes

The `@CrossOrigin` annotation has several attributes which you may need to specify to properly configure your cross origin policy:

- origins
- methods
- allowedHeaders
- exposedHeaders
- allowCredentials
- maxAge

Most often you will only need to set the `origins` property, for example:

```java
@CrossOrigin(origins = "*")
```

Setting it to this allows any origin to access our resources.

> RESOURCE Example repo where the notes were made. [Link](https://gitlab.com/NicholasLennox/sql-to-rest-example).

---

Copyright 2021, Noroff Accelerate AS