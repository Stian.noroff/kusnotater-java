# JPA and Hibernate
This lesson covers the well known and used Java Persistence API (JPA), using Spring Boot and Hibernate as its implementation. This lesson covers the process of creating a PostgreSQL database using Hibernate and JPA Entities.

**Learning objectives:**

You will be able to...
- **describe** JPA.
- **discuss** the role of JPA in modern Java development.
- **define** Hibernate in the context of the JPA.
- **configure** a PostgreSQL database.
- **generate** a database schema using Hibernate, given a scenario description.
- **contrast** entity schema annotations.
- **design** entities which result in various relationships between tables.

---

## Java Persistence API

The Java Persistence API is a set of concepts, not a framework, that is concerned with persistence. Persistence is about a more permanent storage than the simple programed scoped Java classes. Not all classes need to be persisted, but the ones relating the business objects are ideal candidates. 

The JPA specification lets you define which objects should be persisted, and how those objects should be persisted in your Java applications.

> These objects are called `Entities`

JPA is implemented using Object Relational Mapping tools, one such tool is Hibernate. Hibernate was the original ORM for JPA, but there are other models which exist.

## Spring Data JPA

Spring Data is the Spring Framework's wrapper for JPA implementations. Spring Data uses _annotations_ and _interfaces_ to implement these concepts. We utilize these implentations to create databases and web applications. 

The Spring Framework uses the Domain-driven design (DDD) approach and implements all the avaialble concepts in JPA. DDD approaches create configuration classes and model classes that are designed to reflect the domain (business rules) of the web application's context. The most widely used aspect is _Repositories_ that can handle all _CRUD_ functionality. 

> Instead of Repositories to have model-specific behaviour, you can simply use the Context to have a generalized approach.

While utilizing these repositories, all that needs to be done is to create an interface extending the correct parent interface and Spring will create SQL statements to reflect behaviour.

## Hibernate

The purpose of _Hibernate_ is to provide an object-relational mapping (ORM). In the simplest terms, this means it will handle all of our SQL statements and database connection. This will cut down on the volume of repetitive code that we will need to write significantly.

While Hibernate (or any JPA implementation) will look after converting objects (POJOs) into database records and vice versa; we will still need to manage the in-memory versions of these objects ourselves. These are the _Entities_ we configure.

> Hibernate is the most widely used JPA implentations

Hibernate does not only map our Java classes to database tables, it also maps our Java datatypes to their SQL equivalents. Hibernate also handles the automatic conversion of the ResultSet - no more headaches.

> In terms of architecture, Hibernate sits between your data access layer and the relational database.

Hibernate, or any ORM, does a task that we as developers want to avoid doing: setting up database tables, relationships, and creating helper classes to interact with database models. These tasks are very time consuming and error prone - ORMs help simplify this process greatly if we simply follow convention.


**Dependencies**

When creating a Spring Boot application that you intend to use Hibernate with, the following dependencies must be installed:
- Web – normal one we used for REST
- JPA – this enables JPA and hibernate

The following dependencies are good additions
- PostgreSQL – installs JDBC automatically

## Setting up the database

We are now shifting away from using simple database providers like Sqlite, and moving onto a proper database provider - in our case PostgreSQL. 

### PostgreSQL

[PostGresql](https://www.postgresql.org/) is an all included solution to a permanent storage with a tool to manage them. When Postgresql is installed with defaults, a postgres user is requested and an accompaning password. The Postgresql server will be available on the _localhost:5432_ address. When Postgresql is installed, an application called _PgAdmin_ is also installed, this is the application we use to manage our databases. 

### Configuring the database

To use Postgresql we need to add database information to our Java application, this is to configure which database to use and the credentials used to access it. This is done in the application.properties file, it is the file _Spring_ uses for configuration variables. 

**application.properties**

```
spring.jpa.hibernate.ddl-auto=update

spring.datasource.url: jdbc:postgresql://localhost:5432/citadel?currentSchema=public&user=username&password=password
spring.datasource.username: postgres
spring.datasource.password: postgres
```
This file is located inside the resources folder.

> When using Postgres hosted on Heroku we can use `spring.datasource.url: ${DATABASE_URL}` instead, this will load the database information from the heroku contianer.

`spring.jpa.hibernate.ddl-auto=update` will mean that Hibernate will auto generate tables and columns as needed. This is determined by how we use annotations in our models.

## Creating a schema

To create our database schemas, we heavily utilize annotations. These do all the heavy lifting for us so to speak. For the most part, little to no customization needs to be done and we can rely on defaults. These annotations allow not only database tables to be created, but relationships between tables as well.

### Entities

The entities we use are simple domain models which we configure with annotations. They are the classes we want to be persisted as they  represent our database tables. Hibernate uses implicit conversion to map Java datatypes to database datatypes. 

We can configure it further with the org.hibernate.type.Type package, configured as an annotation:

- String -> VARCHAR
- Boolean -> BIT
- Long -> BIGINT

Most are very straight forward and complete list with hibernate.type.Type configs can be found [here](https://docs.jboss.org/hibernate/orm/current/userguide/html_single/Hibernate_User_Guide.html#basic-provided).

Below is an example of two domain models, these should be POJOs:

```java
public class Book {
    private String title;
    private String isbn;
    private String author;
}
```

```java
public class Author {
    private String firstName;
    private String lastName;
    private Date dob;
}
```

To turn these domain models into entities, we add some simple configuration. The first thing we do is annotate the class as an `@Entity`. This takes our class name by default, but can be customized with a `name` parameter.

> This is not the resulting tables name, but what the class is called in our applications persistance, you should aim to have this match the desired table name. 

Table names are automatically generated based on the name of the class, the same is for every column in the database being named according to the fields in the _Entity_. These can be customized with `@Table(name="foo")` and `@Column(name="bar")`. You can add configuration for type of the column. It is
`@Basic` by default, meaning the nullability is set to true and has a default of fetch functionality of eager loading.

> Can specify with @org.hibernate.annotations.Type( type = "nstring" ) for example.

Finally, our entity needs an identifier, this results in the _Primary Key_ of the table. This is configured by adding @Id on the variable that is intended to be the primary key. The code block below shows our domain model transformed into an entity.

```java
@Entity
@Table(name="author")
public class Author {
    @Id
    private Long id;

    @Column(name="first_name")
    private String firstName;

    @Column(name="last_name")
    private String lastName;

    @Column(name="date_of_birth")
    private Date dob;
}
```


**Enumerators**

Hibernate can be made to work enumerators if there is a desire to store enumerator values. The enum field needs to be annotated `@Enumerated` with either:
- (EnumType.ORDINAL) to show the enums value, i.e. 1,2,3,…
- Or (EnumType.STRING) to show the enums value name, i.e. Monday, Tuesday, Wednesday,…

as parameters.

**Generating fields**

When your primary keys are intended to be auto incremented, you can add a `@GeneratedValue` annotation to configure it as such. There are a few types, but the two we care about are:

@GeneratedValue(strategy=GenerationType.x)
- AUTO - Persistence provider will pick automatically (default)
- IDENTITY – Similar to AUTO, but ensures unique values within the current column.

> Auto will make a unique value for the entire database, Identity will make a more realistic unique value per table.

**Column defaults**

If you want to set default values for a column you can use `@ColumnDefault()` and provide a default value. If this is omiited then the default values for the respective datatypes are inserted instead.

**Embedded types**

If we have a class that is not an entity, but is a component of one, we can annotate it as `@Embeddable`. Hibernate will not try make tables for these classes, but use their fields in the entities they are embedded in.

```java
@Embeddable
public class Publisher {
    @Column(name="publisher_name")
    private String name;
    @Column(name="publisher_country")
    private String country;
}
```

**Natural Id**

A natural Id has some real world meaning but is not suitable for a primary key in a database. For example, in books this is the ISBN numbers (ISBN10 and ISBN13). These can be annotated as @NaturalId and Hibernate is able to do operations with these as if they were primary keys.

### Relationships

Until this point, the Entities have been configured in isolation, the real power is to add relationships. This is done very simply by embedding data into entities.

There are 4 main associations that result in relationships
- @OneToMany
- @ManyToOne – the child side of one to many
- @OneToOne
- @ManyToMany – don't need to create a linking table

Another aspect is that all relationships have a direction:

- Unidirectional – one direction, can only be access from one side (parent to child)
- Bidirectional – both directions, developer can access relationship from both sides (parent to child AND child to parent)

**@OneToMany**

This represents the parent child relationship. One `author` may have many `books`. To do this we annotate a list (or set) of `books` in author with `@OneToMany`. We supply `@JoinColumn` to place our foreign key in the `book` table.

> If we left it out, Hibernate would create a joining table

Unidirectional example:

```java
// Author entity - unidirectional
@OneToMany
@JoinColumn(name="author_id")
Set<Book> books;
```

Bidirectional:

```java
// Author entity - bidirectional
@OneToMany(mappedBy="author")
Set<Book> books;
```

```java
// Book entity - child
@ManyToOne
@JoinColumn(name="author_id")
private Author author;
```

With the configuration before we can go Author->Books but not Book->Author.  If we want this configuration, we need to set a @ManyToOne in the child (`book`). We also move the `@JoinColumn` to the child, this allows us to navigate both ways.

> The resulting database schema is the same, this is just for navigation

The table where @JoinColumn is located is called the owning table.

> This just means, its where the FK sits

The mappedBy in Author is vital as it signals a two-way relationship, without it, it would be two one-way relationships each with their own FK mapped. Hibernate will also create an association table.

**@OneToOne**

The configuration is similar to `@OneToMany`, but there are no Lists or Sets, just single objecs. For unidirectional relationships we have `@OneToOne` and `@JoinColumn` annotations in one of the tables. As for bidirectional relationships, we need `@OneToOne` in both tables and `@JoinColumn` in the _owning_ table, with _mappedBy_ set in the other.

```java
// Address entity
@OneToOne(mappedBy="address")
private Author author;
```

```java
// Author entity (owner)
@OneToOne
@JoinColumn(name="address_id")
private Address address;
```

**@ManyToMany**

Hibernate allows us to skip the creation of linking tables and allow them to be automatically generated based on our configurations. For unidirectional relationships `@ManyToMany` is placed in one table with a List (or set) referencing the other entity.

For bidirectional relationships `@ManyToMany` is placed in both tables, _mappedBy_ set in one (to configure two-way relationship).

For both relationships we need to configure @JoinTable, and Hibernate automatically generates a joining table for us. We can leave it to be defaulted with implicit naming, or configure it ourselves.

```java
// Book entity
@ManyToMany(mappedBy="books")
public Set<Library> libraries;
```

```java
// Library entity
@ManyToMany
@JoinTable(
    name="book_library",
    joinColumn={@JoinColumn(name="library_id")},
    inverseJoinColumns={@JoinColumn(name="book_id")}
)
public Set<Book> books;
```

> For most occassions a bidirectional relationship is the best course of action as unidirectional relationships prevent some actions.


**Lazy and eager loading**

Sometimes we can over-query accidentally with hibernate. This results in bringing in a lot of unrelated data and overhead. We define `fetch = FetchType.EAGER (or LAZY)` to control lazy-loading in the `@ManyToMany`, `@OneToMany`, or `@ManyToOne` configurations.

This Prevents us from accidentally loading all the related data unless we explicitly ask for it (with lazy loading).

I.e. We would load all books in a library if we just asked for a library (if set to EAGER ), but just the library data (if set to LAZY).

---

Copyright 2021, Noroff Accelerate AS