# Del 2

# Grunnleggende Java Del 2

**Læringsmål**

Du vil lære å…

- **beskrive** de forskjellige operatorene i Java.
- **demonstrere** forskjellige metoder for å søke gjennom strenger.
- **modifisere** en streng i henhold til krav.
- **diskutere** rollen modularisering har i programmering.
- **beskrive** og demonstrere rekursjon
- **diskutere** unntakshåndtering (eng. Exception handling) i Java.
- **demonstrere** hvordan opprette tilpassede unntak

---

## Operators

Java operatorer (les: operatører) er måter å utføre forskjellige operasjoner på variabler. Det er flere typer operatorer:

- Aritmetiske (Arithmetic) operatorer
- Tilordne (Assignment) operatorer
- Sammenligne (Comparison) operatorer
- Logiske (Logical) operatorer

### Aritmetisk

Aritmetiske operatorer er brukt for å utføre vanlige matematiske operasjoner. Tabellen nedenfor viser vanlige aritmetiske operasjoner:

[Untitled](Del%202%203521c841610d4e92ad6f44bc2f321076/Untitled%20Database%208649121d36e0431ea682faed337981fa.csv)

Modulo-operatoren `%` regner ut **resten**. Når `a = 5` er delt på `b = 3`, blir resten 2. Den er som oftest brukt med **integers** (heltall).

### Tilordning

Tilordnede operatorer brukes for å tilordne verdier til variabler. Vi har allerede sett den grunnleggende `=` operatoren som tilordner verdier.

[Untitled](Del%202%203521c841610d4e92ad6f44bc2f321076/Untitled%20Database%2002f952bbf85843df8ab5339b669bf9dc.csv)

### Sammenlignende

Sammenlignende operatorer er brukt for å sammenligne to verdier.

[Untitled](Del%202%203521c841610d4e92ad6f44bc2f321076/Untitled%20Database%20fe18905ce0c842d09b8add545bbd4f0a.csv)

### Logiske

Logiske operatorer brukes til å fastslå logikken mellom to variabler eller verdier.

[Untitled](Del%202%203521c841610d4e92ad6f44bc2f321076/Untitled%20Database%200c0864c9050d4b56b51f4e3788078cef.csv)

> INFO: Det finnes en ekstra type operator som kalles “Bitwise” operatorer. Brukt for å utføre operasjoner på individuelle bits, det er ikke noe som er ofte brukt men er verdigfullt når aktuelt.
> 

### Operatørprioritet

Operatørprioritet bestemmer hvilken rekkefølge de forskjellige operatorene i et uttrykk blir evaluert.

Se følgende linje med kode:

```java
int result = 12 - 4 * 2;
```

Hva vil `result` bli?

Svaret er `4`.

Aritmetiske operatorer er lette å forstå ettersom de følger matematiske regler. I dette tilfellet har  multiplikasjon `*` høyere prioritet enn `-`, og blir utført først.

Parantes `()` overstyrer prioriteten. Det tvinger operasjonene innenfor parantesene til å bli evaluert først. Se følgende linje med kode:

```java
int result = (12 - 4) * 2;
```

Hva blir `result?`

Svaret er `16`.

Dette er fordi parantesene har høyest prioritet og er utføres dermed først, `(12 - 4) = 8`. Dette blir så multiplisert med `2` for å få `16`.

Tabellen nedenfor kan bli brukt som en referanse:

[Untitled](Del%202%203521c841610d4e92ad6f44bc2f321076/Untitled%20Database%201491feadb9244cb4bed37f9c2f0efc38.csv)

> **MERK**: () er ikke listet opp ettersom den alltid har høyest prioritet.
> 

> **MERK**: Alle **Bitwise** relaterte prioriteter var utelatt.
> 

## Strenge Manipulering

Husk at strenger er et objekt som representerer en sekvens av char-verdier. Et array av characters (karakterer) virker på samme måte som en Java streng.

Java-streng klassen gir forskjellige metoder for å utføre operasjoner på strenger som `compare()`, `concat()`, `equals()`, `split()`, `length()`, `replace()`, `substring()` og mange flere.

### Konkatenering

Konkatenering betyr å legge sammen to eller flere strenger. Gjort med enten `.concat()` metoden eller så kan man bruke `+` operatoren. Kodeutdraget nedenfor viser disse to metodene:

```java
String str1 = "Hello";
String str2 = "World";
// Using concat
String str3 = str1.concat(str2);
// Using + operator
String str4 = str1 + str2;
```

### Bruke Strenger

Forskjellige innebygde metoder kan bli brukt for å få informasjon ut av en streng, kodeutdraget nedenfor illustrerer noen av disse:

Metoden `length()` returnerer lengden av en Streng:

```java
String s = "Hello World";    
System.out.println(s.length()); // OUTPUT: 11
```

Metoden `chatAt(int index)` returnerer en char-verdi på de gitte indekstallet:

```java
String s = "Hello World";    
char ch = s.charAt(0);
System.out.println(ch); // OUTPUT: H
// Getting the last character in a string
System.out.println(s.charAt(s.length()-1)); // OUTPUT: d
```

Metoden `indexOf()` returnerer posisjonen til den første hendelsen av en den spesifikke karakteren eller strengen:

```java
String s = "Hello World";
System.out.println(s.indexOf('e')); // OUTPUT: 1
// The second argument is 'fromIndex' and is inclusive
System.out.println(s.indexOf('o', 5)); // OUTPUT: 7
System.out.println(s.indexOf("lo")); // OUTPUT: 3
```

Metoden `lastIndexOf()` returnerer den siste indeksen av den spesifserte karakteren eller strengen:

```java
String s = "Hello World"; 
System.out.println(s.lastIndexOf('o')); // OUTPUT: 7
```

Metoden `contains()` søker etter en sekvens av karakterer i en streng. Den returnerer true hvis sekvensen er funnet:

```java
String s = "Hello World"; 
System.out.println(s.contains("Hello")); // OUTPUT: true
System.out.println(s.contains("Wrld")); // OUTPUT: false
```

Metodene `startsWith()` og `endsWith()` fastslår om en streng begynner eller slutter med en spesifikk substreng:

```java
String s = "Hello World"; 
System.out.println(s.startsWith("H")); // OUTPUT: true
// Can set a offset index
System.out.println(s.startsWith("H", 1)); // OUTPUT: false
System.out.println(s.endsWith("d")); // OUTPUT: true
```

### Endre Strenger

Forskjellige innebygde metoder finnes for å endre strenger. Kodeutdragene nedenfor illustrerer noen av disse.

Metoden `substring(int startIndex, int endIndex)` returnerer en del av strengen:

```java
String s = "Hello World";
// endIndex is optional
System.out.println(s.substring(6)); // OUTPUT: World
System.out.println(s.substring(6, 8)); // OUTPUT: Wo
```

Metoden `valueOf()` konverterer forskjellige datatyper til stringer:

```java
int number = 1;
String numberAsString = String.valueOf(number);
String numberAsStringV2 = number + ""; // Using implicit concat
```

Små og store bokstaver kan endres ved å bruke enten `toLowerCase()` eller `toUpperCase()`:

```java
String word = "heLLo";
System.out.println(word.toUpperCase()); // OUTPUT: HELLO
System.out.println(word.toLowerCase()); // OUTPUT: hello
```

Metodene `replace()`, `replaceAll()` og `replaceFirst()` returnerer en ny streng med noen eller alle hendelser erstattet fra den originale strengen:

```java
String s = "Hello World";
System.out.println(s.replace("l","p")); // OUTPUT: Heppo Worpd
System.out.println(s.replaceFirst("l","p")); // OUTPUT: Heplo World
System.out.println(s.replaceAll("l","p")); // OUTPUT: Heppo Worpd
```

> **MERK**: både replace() og replaceAll() erstatter alle hendelser.
> 

Når vil du bruke `replaceAll()` i stedet for `replace()`? Svaret er enkelt, `replaceAll()` tar inn et regulært uttrykk **(Regular Expression (Regex))** som en parameter. Dette gir stor fleksibilitet gjennom mønster-matching.

> **INFO**: Regex er en måte å søke strenger med bruk av et søkemønster. Søkemønsteret kan brukes til å beskrive det man søker etter.
> 

Metoden `split()` splitter strengen med et gitt regulært uttrykk og returnerer et char-array:

```java
String s = "Hello World";
String[] words = s.split(" "); // Splits by whitespace
```

`trim()`-metoden  eliminerer ledende og etterfølgende tomrom:

```java
String spaced = "   Hello World    ";
System.out.println(spaced.trim()); // OUTPUT: "Hello World"
```

> **MERK**: trim() eliminerer ikke mellomrom mellom ord.
> 

### Regulære uttrykk

> SME NOTE: Flagged for moving to separate page and linked to. Can show Regex, how its used in Java, and how to write patterns.
> 

Et regulært uttrykk (eng. **regular expression**) kan være en enkel bokstav, eller et mer komplisert mønster. Vi kan importere Java.util.regex pakken for å jobbe med regulære uttrykk. Denne pakken inkluderer følgende klasser klasser:

- `Pattern`-klasse - Definerer et mønster (Brukes i et søk).
- `Matcher`-klasse- Brukes til å søke etter mønsteret
- `PatternSyntaxException`-klasse - Indikerer syntaksfeil i et regulært uttryksmønster

Mønsteret blir laget ved å bruke `Pattern.compile()`-metoden. Den første parameteren indikerer hvilket mønster det søkes etter, og den andre parameteren (valgfritt) har et flagg for å indikere at søket ikke bør skille mellom store og små bokstaver.

`matcher()`-metoden benyttes for å søke etter et mønster i en streng. Det returnerer et Matcher-objekt som inneholder informasjon om søket som ble utført.

`find()`-metoden returnerer *true* hvis mønsteret ble funnet i strengen og *false* hvis det ikke ble funnet. Kodebiten nedenfor viser et grunnleggende bruksområde:

```java
Pattern pattern = Pattern.compile("accelerate", Pattern.CASE_INSENSITIVE);
Matcher matcher = pattern.matcher("Noroff Accelerate");
boolean matchFound = matcher.find(); // OUTPUT: true
```

Regulære uttrykk er mektige på grunn av dets evner til mønster-matching. Komplekse søkemønster kan defineres ved å bruke spesifikk syntaks. Complex search patterns can be defined using specific syntax. Tabellene nedenfor illustrerer byggesteinene til et regulært uttrykksmønster.

Klammer benyttes til å finne et utvalg av tegn:

[Untitled](Del%202%203521c841610d4e92ad6f44bc2f321076/Untitled%20Database%205d0d12ad57d64134b1a58d3acff46d71.csv)

Metakarakterer er tegn som har en spesiell betydning:

[Untitled](Del%202%203521c841610d4e92ad6f44bc2f321076/Untitled%20Database%2044add523b74a41a6a95ffb5af9b7adf7.csv)

Kvantifikatorer definerer mengder:

[Untitled](Del%202%203521c841610d4e92ad6f44bc2f321076/Untitled%20Database%2097824902fb8e4a479a35e79123bb648c.csv)

> **MERK**: Hvis uttrykket ditt trenger å søke etter et av spesialtegnene, kan du bruke backslash ( ) for å unnslippe dem.
> 

> **RESSURSER**: Regex-testing og eksempler: Regex Cheatsheet, Regex Cheatsheet Stanford, Regexer, og Regex 101.
> 

### String Pool

For å forstå String pools bedre, er det to konsepter som må introduseres: **stack** og **heap**.

Disse to konseptene utforskes i detalj senere, og omhandler minnehåndtering. Primitiver lagres i en rask, liten minnedel kjent som en **stack**, mens objekter lagres i en stor, langsom minnedel kjent som **heap**. Objekter lagret på heap har en referanse på stacket for å peke på dataene.

**String pool** er ikke annet enn et lagringsområde i Java *heap* (haugen) **der strengliteraler er lagret. Det er også kjent som **String Intern Pool** eller **String Constant Pool**.

Som standard er den tom og vedlikeholdt privat av klassen **Java String**. Når vi oppretter en strengliteral, sjekker JVM først om den literalen finnes i **String pool**. Hvis bokstaven finnes i pool, returnerer den en *referanse* til den pool-forekomsten. Hvis bokstaven ikke finnes i pool, opprettes et nytt Strengobjekt. Kodeutdraget nedenfor viser et eksempel på dette:

```java
String foo = "Hello";
String bar = "Hello";
String baz = new String("Hello");
```

Når vi erklærer en variabel `foo` med verdien av strengliteralen `"Hello"` oppretter JVM en ny instans i String Pool; fordi literalen ikke eksisterer ennå.

Så når vi erklærer variabelen `bar` med verdien av strengen literal `"Hello"`, returnerer den bare en referanse til den eksisterende literalen. Dette betyr at det bare er én forekomst av `"Hello"` i string pool; både 'foo' og 'bar' peker på det.

Til slutt, når vi erklærer variabelen `baz`, bruker vi nøkkelordet **new**. Dette vil opprette en ny forekomst av strengliteralen `"Hello"` og tilordne den til `baz`.

### Strenglikhet (String Equality)

Det finnes flere måter vi kan sammenligne strenger på. Disse sammenligningene fungerer på fundamentalt ulikt vis, som inkluderer:

- `equals()`  metoden
- `==` operatør
- `compareTo()` metoden

`equals()`-metoden sammenligner strengen sitt originale innhold. Den sammenligner strengverdier for likhet.

```java
String foo = "Hello";
String bar = "hello";
// The significance of the new keyword is explained later
String baz = new String("Hello");
System.out.println(foo.equals(bar)); // OUTPUT: false
System.out.println(foo.equalsIgnoreCase(bar)); // OUTPUT: true
System.out.println(foo.equals(baz)); // OUTPUT: true
```

`==` operatøren sammenligner **referanser** og ikke verdier, noe som betyr at den må være den må være den samme forekomsten.

```java
String foo = "Hello";
String bar = "Hello";
String baz = new String("Hello");
System.out.println(foo == bar); // OUTPUT: true
System.out.println(foo == baz); // OUTPUT: false
```

Hvorfor er `foo` lik  `bar`, men ikke lik `baz`? `foo` og `bar` referrer til samme instans, mens  `baz` er en ny instans. Selv om `foo` og  `bar` deklareres separat, referer de til samme instans i minnet. Husk den tidligere forklaringen av *String Pools*.

> **MERK**: Måten equals() og == er forskjellig for strenger, gjelder også for alle andre type objekter.
> 

`compareTo()` metoden sammenligner verdier *leksikografisk* og returnerer en integer-verdi som forteller om den første strengen er mindre, større eller lik den andre strengen. 

Anta at `foo` og `bar` er to Strengobjekter. Dersom:

- `foo == bar` : Gir metoden 0 som resultat.
- `foo > bar` : Gir metoden en positiv verdi.
- `foo < bar` : Gir metoden en negativ verdi.

> **INFO**: Leksikografisk rekkefølge er i esssens alfabetisk rekkefølge. Mer informasjon finner du [her](https://en.wikipedia.org/wiki/Lexicographic_order).
> 

### StringBuilder

Java `StringBuilder` blir brukt til å lage muterbare strenger.

> **INFO**: StringBuilder er en enkel implementering av noe som kalles et Builder-mønster (eng. Builder Design Pattern). Designmønstre vil dekkes senere.
> 

StringBuilders konstrueres på forskjellige måter:

- `StringBuilder()` lager en tom StringBuilder som starter med en initiell kapasitet på 16.
- `StringBuilder(String str)`  lager en StringBuilder med den spesifiserte stringen.
- `StringBuilder(int length)`  lager en tom StringBuilder med den spesifiserte kapasiteten som lengde.

**Kapasitet** er ikke lengden til  `CharSequence` i StringBuilder. Kapasiteten er antall mellomromstegn som har blitt allokert, og er vanligvis mer enn lengden. Kapasiteten vil øke for å imøtekomme tillegg.

Når vi først har en forekomst av StringBuilder, er det metoder tilgjengelig som utvider de tidligere strengmetodene vi har sett:

- `append()`  legger strengen til på slutten av builderen.
- `insert()` legger til en streng ved en indeks i builderen.
- `reverse()` reverserer builderen.
- `replace()` erstatter den gitte strengen fra de angitte start- og sluttindeksene. Den fjerner strengen mellom indeksene, og setter deretter inn den nye strengen.

```java
StringBuilder builder = new StringBuilder();
builder.append("foo");
builder.append("bar");
builder.insert(1,"baz");
System.out.println(builder); // OUTPUT: fbazoobar
System.out.println(builder.reverse()); // OUTPUT: raboozabf
builder.replace(2,6,"box");
System.out.println(builder); // OUTPUT: raboxabf
```

## Modularisering

Modularisering i programmering er ideen om å bryte opp logikk i arbeidsenheter (eng. Units of Work). Hver arbeidsenhet har et selvstendig definert formål, fungerer isolert og kan sekvenseres inn i større oppgaver. Fordelen med dette er at arbeidsenhetene er gjenbrukbare, noe som reduserer behovet for å gjenta kode. Feil oppstår isolert innenfor en arbeidsenhet. Oppdatering av arbeidsenheter trenger kun å skje på ett sted.

Modulær design følger utilsiktet reglene for det man kan kalle "del og hersk" problemløsningsstrategi. Det er mange andre fordeler knyttet til den modulære utformingen av programvaren:

- Mindre komponenter er lettere å vedlikeholde
- Programmer kan deles inn basert på funksjonelle aspekter
- Lettere å forstå hver modul og deres formål
- Lettere å gjenbruke og refaktorisere moduler
- Bedre abstraksjon mellom moduler
- Sparer tid på utvikling, feilsøking, testing og distribuering av et program

> **MERK**: Modularisering er kjernen i programvaredesign. En senere leksjon er dedikert til design.
> 

> **MERK**: Modularisering er sterkt knyttet til et objektorientert programmeringskonsept kalt innkapsling.
> 

Det er flere ulike måter å oppnå modularisering i programvareutvikling. For omfanget av denne leksjonen fokuserer vi på to: **pakker** og **metoder**.

### Pakker

En pakke er en måte å gruppere relaterte klasser sammen. Det er en mappe i en filkatalog. Vi bruker pakker for å unngå navnekonflikter og for å skrive en kode som er lettere å vedlikeholde. Pakkene er delt inn i to kategorier:

- Innebygde pakker (pakker fra Java API)
- Brukerdefinerte pakker (lag dine egne pakker)

Java API er et bibliotek med forhåndsskrevne klasser som er gratis å bruke, og inkludert i *Java Development Environment*. Biblioteket er delt inn i pakker og klasser. Det betyr at du enten kan importere en enkelt klasse (sammen med dens metoder og attributter), eller en hel pakke som inneholder alle klassene tilhørende den angitte pakken.

For å bruke en klasse eller en pakke fra biblioteket, må du bruke **import** nøkkelordet:

```java
import package.name.Class; // Import a single class
import package.name.*; // Import the whole package
```

> **MERK**: Måten for å importere innebygde pakker og klasser på er den samme som ved brukerdefinerte.
> 

Eksempelvis, for å lese brukerdata fra konsollen, kan vi bruke `Scanner`-klassen.

Denne klassen utgjør en del av `java.util` -pakken. For å importere denne klassen skriver vi følgende erklæring:

```java
import java.util.Scanner;
```

Java bruker en filsystemkatalog for å lagre pakker som mapper. Ved oppretting av en klasse i en mappe, kan dens pakke defineres ved å bruke `package` nøkkelordet:

```java
package no.noroff.accelerate.util;
```

Dette pakkenavnet er direkte relatert til mappen hvor klassen er lagret. I dette tilfellet er filstrukturen som følger:  `project source root -> no -> noroff -> accelerate -> util`.

> **MERK**: Intellij vil generere pakkedeklarasjonen, og tvinger pakkenavn til å samsvare med mappenavnene.
> 

> **MERK**: Å ha dypt nestede mapper er en Java-konvensjon. De nestede mappene representerer organisasjonsdetaljer og er tilstede for å unngå konflikter med andre bibliotek.
> 

### Metoder

En metode er en blokk med kode som utfører en oppgave. Det gjør at koden kan **gjenbrukes**. Metoden kan skrives én gang, og brukes mange ganger ved å **påkalle** den. Metoder kan ha null eller flere **parametere** sendt til seg mellom parenteser `()`.

> **MERK**: Metoder sørger for enkel modifikasjon og lesbarhet for koden.
> 

Den viktigste metoden i enhver Java-applikasjon er `main(String[] args)`-metoden. Vi har allerede sett denne metoden. og husker at den fungerer som inngangspunktet til applikasjonen vår, som betyr at den påkalles når programmet kjøres.

> **MERK**: Et Java-program kan kjøres med flere argumenter som overføres, og blir tilgjengelig i `main`-metoden gjennom parameteren `args`.
> 

En metode er definert av en ***header*** og innholdet i metoden er i ***body*** En metode følger den generelle malen:

```java
method header {
    method body
}
```

Bruke følgende mal for metode**-header**:

```java
accessmodifer returntype name (parameters)
```

En metodes **signatur** er navnet og parameterlisten fra metode-header. En metodes signatur må være kjent for å kunne bruke den.

**Tilgangsmodifikatorer** spesifiserer synligheten til metoden. Java har fire typer:

- **Public**: Metoden er tilgjengelig for alle klasser i applikasjonen vår.
- **Private**: Metoden er kun tilgjengelig i den definerende klassen.
- **Protected**: Metoden er tilgjengelig innenfor samme pakke eller underklasser i en annen pakke.
- **Default**: Når vi ikke bruker noen tilgangsmodifikator i metodedeklarasjonen, bruker Java *standard*. Metoden er kun synlig i samme pakke.

Disse kan oppsumeres i følgende tabell:

[Untitled](Del%202%203521c841610d4e92ad6f44bc2f321076/Untitled%20Database%2040851df583ce4ba8806e57e2b3dd3dc5.csv)

**Return-type** er en datatype som metoden returnerer. Den kan ha en *primitiv* datatype, *objekt, samling, void* osv. Hvis metoden **ikke** returnerer noe, bruker vi `void`nøkkelordet.

> **MERK**: Non-void returtyper fremtvinger en returerkæring i *body* som samsvarer med datatypen spesifisert i returtypen.
> 

Metodenavnet er unikt, og bør tydelig og konsist reflektere den tiltenkte funksjonen.

> **MERK**: Tydelige metodenavn forbedrer lesbarheten, noe som er en essensiell egenskap for god programvareutvikling.
> 

**Parameterlisten** er en liste over parametere atskilt med komma og omsluttet av parantes. Det inneholder datatype og variabelnavn. Hvis metoden ikke har noen parametere, er parentesen tom.

metode**-body** inneholder alle handlingene som skal utføres innelukket i krøllparantes.

La oss for eksempel definere en metode som er ansvarlig for å legge til to heltall og returnere summen deres. Kodebiten nedenfor illustrerer dette eksemplet:

```java
public int add(int num1, int num2) {
    return num1 + num2;
}
```

Det er en konvensjon for å eliminere unødvendige kodelinjer. Om man hadde sagt `int sum = num1 + num2;` deretter`return sum;`, ville det blitt for ordrikt, og endt opp med å skape rot i koden.

> **MERK**: Det er en viktig avveining du må gjøre mellom detaljerthet og lesbarhet. Generelt foretrekker vi lesbarhet innen programvareutvikling for bedrifter.
> 

Når en metode har en kontrollstruktur som `if..else` eller`switch`, har den flere kodestier. Det må finnes en returerklæring for hver kodesti. Kodebiten nedenfor illustrerer dette:

```java
public String handleReading(double value) {
    if(value > 100) {
        return "High value warning!";
    }
    return "Normal level";
}
```

> **MERK**: Vi trengte ikke `else` siden det ikke var noen annen verdi å sjekke. Hvis det kom forbi det første utsagnet, vet vi hva vi skal returnere. Å ha `else` ville vært overflødig.
> 

### Statiske nøkkelord

Noen metoder har et ekstra `static` nøkkelord. En slik er `main()`-metoden.

```java
public static void main(String[] args)
```

Dette nøkkelordet har en enkel men signifikant betydning: 

En statisk metode er et medlem av selve klassen og ikke instansen.

Hva betyr dette? En statisk metode kan påkalles uten behov for å lage en instans av en klasse. Dette gir en stor fordel ved å gjøre programminnet ditt effektivt (dvs. det sparer minne).

> **MERK**: Indre klasser og variabler kan også være statiske.
> 

Kodeutdraget nedenfor illustrerer et eksempel på `add`-metoden fra tidligere from before, men som en statisk metode i en `Calculator` klasse:

```java
public class Calculator {
    public static int add(int num1, int num2) {
        return num1 + num2;
    }
}

public static void main(String[] args) {
    // Note: no new keyword is needed
    int result = Calculator.add(1,1);
}
```

> **ADVARSEL**: Vær forsiktig med overbruk av *static* dette kan føre til svært kronglete og komplekse programmer som ville vært langt enklere uten dens bruk.
> 

### Metodeoverbelasting

Med metodeoverbelastning kan flere metoder ha samme navn med forskjellige parametere og returtyper. Kodeutdraget nedenfor illustrerer dette:

```java
int fooBar(int x) {}
float fooBar(float x) {}
double fooBar(double x, double y) {}
```

Kompilatoren vil bestemme hvilken metode som skal påberopes basert på parameterne som er oppgitt og returtypene som forventes.

> **MERK**: I stedet for å definere to metoder som skal gjøre det samme, er det bedre å overbelaste én, og forbedre lesbarheten.
> 

### Scope

Scope (omfang) i Java-kontroller der variabler er tilgjengelige. Variabler er kun tilgjengelige innenfor regionen de er opprettet. Det betyr at variabler som er deklarert direkte i en metode, er tilgjengelige hvor som helst i metoden etter kodelinjen der de ble deklarert. Kodebiten nedenfor illustrerer metode-scope:

```java
public static void main(String[] args) {
    // Code here CANNOT use x
    int x = 100;
    // Code here can use x
    System.out.println(x);
}
```

En annen type scope er **block scope**. En kodeblokk refererer til all koden mellom krøllparenteser {}. Variabler som er deklarert inne i kodeblokker er bare tilgjengelige med koden mellom de krøllparentesene, som følger linjen der variabelen ble deklarert:

```java
public static void main(String[] args) {
    // Code here CANNOT use x
}

public static void foo() {
    int x = 100;
    // Code here can use x
}
```

> **MERK**: Omfanget kan påvirkes av tilgangsmodifikatorer som: *private*, *protected* og *public*.
> 

### Rekursjon

Rekursjon er teknikken for å lage et funksjonskall. Hovedformålet er å erstatte løkker i et forsøk på å redusere mengden kode som skrives.

> **MERK**: Rekursjon kan være litt vanskelig å forstå. Av den grunn unngås det vanligvis for ikke-trivielle oppgaver.
> 

> ADVARSEL: Hvis en juniorutvikler ikke kan forstå koden din, gjør du det feil. Derfor favoriserer enterprise-utviklere lesbarhet.
> 

Med det sagt, la oss se på et enkelt eksempel:

*Bruk rekursjon for å legge sammen alle tallene fra 0 til 10*

dvs. 10+9+8+7+6+5+4+3+2+1+0=55. Kodeutdraget nedenfor viser hvordan dette kan oppnås:

```java
public static void main(String[] args) {
    System.out.println(sum(0, 10));
} 

public static int sum(int start, int end) {
    // Halting condition
    if (end > start) {
        // Recursive call
        return end + sum(start, end - 1);
    } else {
        // Stops the recurisive process
        return end;
    }
}
```

Når denne metoden påkalles, skjer følgende:

```
10 + sum(9)
10 + ( 9 + sum(8) )
10 + ( 9 + ( 8 + sum(7) ) )
...
10 + 9 + 8 + 7 + 6 + 5 + 4 + 3 + 2 + 1 + sum(0)
10 + 9 + 8 + 7 + 6 + 5 + 4 + 3 + 2 + 1 + 0
```

Hver `return`-erklæring legger til en *stack* som vil kjøres når det ikke er flere rekursive påkallinger. Det betyr at kompilatoren bare vil gjøre beregningen når stacket er fullført.

> **MERK**: Rekursjon har ofte et problem med å gå tom for minne på stacket når problemene er ikke-trivielle. En senere leksjon går mer inn på minnehåndtering i Java
> 

Kodeutdraget nedenfor illustrerer det samme eksempelet ved bruk av loops:

```java
public static int sumLoops(int start, int end) {
    int sum = 0;
    for (int i = start; i <= end; i++) {
        sum += i;
    }
    return sum;
}
```

## Unntakshåndtering

IDE-en kommuniserer syntaksfeil i kode mens *runtime-*feil forårsaker unntak (unormale betingelser) til å bli kastet (stopp og generere en feilmelding). Runtime-unntak stopper programmet når det kjører.

Håndtering av disse unntakene er nødvendig for å opprettholde normal flyt av applikasjonen. Dette gjøres vanligvis ved å bruke en `try..catch` erklæring.

### Unntak

Det finnes tre typer unntak:

- Avmerket unntak
- Uavmerket unntak
- Error

### Kontrollerte unntak

Klasser som direkte arver `Throwable`-klassen bortsett fra `RuntimeException`og `Error`. F.eks. `IOException`, `SQLException` osv. kontrollerte unntak sjekkes ved *kompileringstidspunkt*.

### Ikke-kontrollerte unntak

Klasser som arver `RuntimeException` er kjent som ikke-kontrollerte unntak. Eksempelvis. `ArithmeticException`, `NullPointerException`, `ArrayIndexOutOfBoundsException`, osv. ikke-kontrollerte unntak sjekkes ved *runtime*.

### Error

Errors er uopprettelige. F.eks. `OutOfMemoryError`, `VirtualMachineError`, `AssertionError` etc.

### Nøkkelord

Java gir fem nøkkelord som brukes til å håndtere unntaket. Følgende tabell beskriver hver av dem:

[Untitled](Del%202%203521c841610d4e92ad6f44bc2f321076/Untitled%20Database%203b6db03709c6469c92a1c838b48826ed.csv)

`try` og `catch` nøkkelord kommer i par, i følgende mal:

```java
try {
  //  Block of code to try
}
catch(Exception e) {
  //  Block of code to handle errors
}
```

Eksemplet nedenfor viser en delemetode for vår tidligere definerte Kalkulator-klasse. Det gir et `ArithmeticException` når vi prøver å dele på null:

```java
public class Calculator {
    public static double divide(double numerator, double denominator) throws ArithmeticException {
        if(denominator == 0) {
            throw new ArithmeticException("Cant divide by Zero!");
        }
        return numerator/denominator;
    }
}

public class Main {
    public static void main(String[] args) {
        try {
            System.out.println(Calculator.divide(1, 0));
        } catch (ArithmeticException e) {
            System.out.println(e.getMessage());
        }
    }
}
```

> NOTE: Throw kan utelates, men det er en konvensjon å inkludere det slik det vises på verktøytipset for å si "denne metoden gir følgende unntak".
> 

Man kan kun ta hensyn til kontrollerte unntak:

- ikke-kontrollerte unntak kan garderes mot i koden. For eksempel om  `NullPointerException` oppstår, er det utviklerens feil at hen ikke sjekket koden før den brukes.
- Errors er utenfor vår kontroll

### Egendefinerte unntak

Java gir mulighet til å lage egendefinerte unntak ved å utvide `Exception`-klassen. Vanligvis opprettes egendefinerte unntak for å følge spesifikk forretningslogikk. Kodebiten nedenfor illustrerer opprettelsen og bruken av et tilpasset unntak:

```java
public class FooBarException extends Exception {
    public FooBarException(String message) {
        super(message);
    }
}

// It can be used with:
throw new FooBarException("Custom exception message");
```

> MERK:  `extends` og `super` nøkkelordene er relatert til objektorientert programmering.
> 

---

Copyright 2021, Noroff Accelerate AS