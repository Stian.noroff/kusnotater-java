# Intro to Web Security

This lesson provides a basic overview of the role of authentication and authorization in modern web development. It covers some common concepts found within this area, with a focus on authentication from a practical viewpoint. Two common authentication flows are discussed, implicit flow and auth code flow. In the discussion of these flows, OAuth and Open ID Connect are introduced and discussed. Finally, token-based authentication is explored.

**Learning objectives:**

You will be able to...
- **discuss** the role security plays in modern software development.
- **describe** common authentication flows.
- **explain** what a JWT is and its role in modern authentication solutions.
- **contrast** cookie-based authentication and token-based authentication. 

---

## Authentication and Authorization

`Authentication` is the process by which an application identifies an individual. It is the programmatical equivalent of asking "Who are you?". This is typically established through login into a user database at which some proof or indicator is provided for subsequent activity.

`Authorization` is the process of deciding whether an individual can access certain resources. It is the programmatical equivalent of asking can you have this resource? It is essentially access control. 

Authentication and authorization processes are incredibly important in Web development. Most comprehensive Web applications have a user base so that individuals using the website can store data unique to them and have access to it across uses of the application. For the data to be tied to the user across sessions, the website needs the user to prove that they are who they say they are. 

> A user's data is only available to them.

These processes are incredibly important. There are many different approaches and scenarios for authentication. The correct ways of doing them have been formalized as standards and protocols which developers can use as guidelines to ensure they are doing it correctly.

## Standards and Protocols

Authentication and authorization processes for a Web application which are robust and secure can be complex. The Web dev industry developed standards and protocols for this to provide a clear secure way of approaching authentication and authorization. Known as OAuth 2.0

`OAuth 2.0` lays out a model in which a service provides access to an unknown user/client by checking their identity and managing their access. Essentially, separating the authentication logic as a dedicated service within a system/application.

This was repeatedly misused to find out information about the user/client during authentication. This issue was addressed by OpenID Connect, which is an additional layer ontop of OAuth2.0.

`OpenID Connect` is a protocol which extends but separates the approach to gather information about a user/client when authorizing them for access. Essentially, OpenID Connect provides the specs for gathering client info. OAuth 2.0 provides the specs for authentication and access control. Together, they form that foundation for various correct and secure approaches to authentication.

> When using OpenID connect you are redirected to login somewhere else then taken back. Google's authentication does this.

OpenID connect and most modern authentication solutions make use of JSON web tokens (JWT). The next section is decicated to exanding on that.

## JSON Web Tokens

`JWT` is a acronym for `JSON Web Token`, and is pronounced as _jot_[^1]. JWT are defined in [RFC 7519 - JSON Web Token (JWT)](https://tools.ietf.org/html/rfc7519), this is a standard so each library that uses JWT can interpret (and use)it as the choose. Within the [RFC 7519](https://tools.ietf.org/html/rfc7519) standard there are a number of defined _claims_, although custom claims can also be added.
[^1]: _jot_ is a lot shorter than _JavaScript Object Notation Web Token_.

The JWT is composed of three parts (discussed below), once all the claims are composed into a JSON object, they are then encrypted (together) using a defined algorithm. 

> Handy website to encode and decode JWTs can be found [here](https://jwt.io/#debugger-io ).

### Parts of a JWT

Anyone planning to use JWT should review the headers and claims stated within [RFC 7519](https://tools.ietf.org/html/rfc7519), but some of the more common ones will be discussed bellow.

When looking at a JWT it will be encoded and will appear as a base 64 string (BASE64URL) seperated into three parts (`header`, `payload`, and `signature`) by two periods (<span style="color:#fb015b">header</span><span style="color:black">.</span><span style="color:#d63aff">payload</span><span style="color:black">.</span><span style="color:#00b9f1">signature</span>).

An example JWT:

```BASE64URL
eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI0MjAwIiwibmFtZSI6Ik9sYSBOb3JkbWFubiIsImFkbWluIjp0cnVlLCJpYXQiOjE1OTE5NTYwMDB9.OK4eTjie_R-QzzK2u3eowncawjVyP-GntytWM_e3T-cpOevcA_vCHDpOmsb3dK1dORkBX2nm47ZOCWKQ5ZRke1VLSo3456BAVlUeDugIKKpKZs6edKs9eWgy41vl110ynrQ63UxF38Z0TVYCEzLmOsjuqWHJhFqQ89o0AziGoZcqkpg4ToEStbSG5d75J36XV8aR2OMSdAM3Yn95igFoQ2uogw1T3cXdAdondKpNGC9EU2Edx-Qnb1jHaO_I0mzrlmCNiGgiLcluVlI780NUS-_6k8aHPG4zcgP7eiPn2LqeXK2G5gTJDhWURYKAtujVyRDbniJvfP5b5GbsV1Bc3Q
```

The same JWT with line breaks and colour coding for display purposes:

<pre>
<span style="color:#fb015b">eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9</span><span style="color:black">.</span><span style="color:#d63aff">ey</br>JzdWIiOiI0MjAwIiwibmFtZSI6Ik9sYSBOb3Jkb</br>WFubiIsImFkbWluIjp0cnVlLCJpYXQiOjE1OTE5</br>NTYwMDB9</span><span style="color:black">.</span><span style="color:#00b9f1">OK4eTjie_R-QzzK2u3eowncawjVyP-</br>GntytWM_e3T-cpOevcA_vCHDpOmsb3dK1dORkBX</br>2nm47ZOCWKQ5ZRke1VLSo3456BAVlUeDugIKKpK</br>Zs6edKs9eWgy41vl110ynrQ63UxF38Z0TVYCEzL</br>mOsjuqWHJhFqQ89o0AziGoZcqkpg4ToEStbSG5d</br>75J36XV8aR2OMSdAM3Yn95igFoQ2uogw1T3cXdA</br>dondKpNGC9EU2Edx-Qnb1jHaO_I0mzrlmCNiGgi</br>LcluVlI780NUS-_6k8aHPG4zcgP7eiPn2LqeXK2</br>G5gTJDhWURYKAtujVyRDbniJvfP5b5GbsV1Bc3Q</span>
</pre>

In practise however, a JWT is either a `JWS` (JSON Web Signature) or `JWE` (JSON Web Encryption), depending on the specifications within the header.

**JWS** 

[RFC 7515 - JSON Web Signature (JWS)](https://tools.ietf.org/html/rfc7515)

> JWS provides integrity verification only

This JWT is signed using the specified digital signature algorithm (`alg`) and private key. This ensures that the payload has not been tampered with (modified).

The JWS is encoded into a `UTF8` `BASE64URL` for ease of transmission.

**Consuming a JWS**

Reading the contents of a JWS is easy, so it should not be considered secure from that standpoint. (Does not provide secrecy.)

The public (matching the private key) is required to verify the integrity of the data based on the signature.

**JWE**

[RFC 7516 - JSON Web Encryption (JWE)](https://tools.ietf.org/html/rfc7516)

> JWE provides integrity verification and secrecy

This JWT is encrypted using the specified encryption algorithm (`enc`) and secret key (`cek`). The content is now cryptographically secured. The encrypted content is then signed using the specified digital signature algorithm (`alg`) and private key as with JWS.

The JWE is encoded into a `UTF8` `BASE64URL` for ease of transmission.

**Consuming a JWE**

The public (matching the private key) is required to verify the integrity of the data based on the signature.

Decrypting the data will require the use of the same secret key.

#### Header

The basis for any JWT is the `JOSE` header (JavScript Object Signing and Encryption). This JOSE header is the first section of the JWT; within it the content and algorithms are specified. 

- When creating a JWT, the header is created first.
- When reading a JWT, the header is read first.

The headers to consider when working with a JWT are as follows:

**typ (Type)**

This is used to identify the content type as a [application/jwt](https://www.iana.org/assignments/media-types/application/jwt).

**recommendation:** set this to `JWT`:

```
"typ": "JWT"
```

**cty (Content Type)**

This defines the 'structural information', used when the token is made up more than one object. 

**recommendation:** usually this can be omitted.

**alg (Algorithm)**

This header identifies the digital signature algorithm used by the token.

Some of the common options are:

- none
- RS256: asymmetric public-private key pair (**recommended**)
- HS256: a shared (secret) key

A full list of algorithms for use with JWS can be found in Section 3.1 of [RFC 7518 - JSON Web Algorithms (JWA)](https://tools.ietf.org/html/rfc7518)

**recommendation:** Use RS256

**Example Header**

Using the above recommendations, the basic header (from the example JWT above) looks as follows:

<pre>
<span style="color:#fb015b">eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9</span>
</pre>

and is decoded as: 

<pre>
<span style="color:#fb015b">{
  "alg": "RS256",
  "typ": "JWT"
}</span>
</pre>

As stated before, this header is required (and must be valid) before a token can be created or read.

#### Payload

The payload represents the data (`claims`) that you want to maintain the integrity of. You are free to add your own claims (read [4.3. Private Claims](https://tools.ietf.org/html/rfc7519#section-4.3)), but it is best to use the standardized ones first to avoid possible collisions.

> All claims are optional.

**iss (Issuer)**

The issuer is the server, application, or company that is responsible for issuing (signing/encrypting) the JWT.

**recommendation:** set this to your company name:

```
"iss": "Noroff Accelerate"
```

**sub (Subject)**
This identifies the _thing_ described by the JWT, in most cases this is the active user.

**recommendation:** set this to a unique ID for the given user:

```
"sub": "4200"
```

**aud (Audience)**
This identifies the intended domain, application, or front-end application that will use the JWT.

**recommendation:** set this to the name of your application:

```
"aud": "Web - Dashboard"
```

**exp (Expiration Time)**
An end time for before which the token can be used.
(It will be invalid **after** this time.)

This is a NumericDate number[^2].

**recommendation:** set this based on your own needs:

```
"exp": 1592042000
```

**nbf (Not Before)**
A start time for when the token can be used.
(It will be invalid **before** this time.)

This is a NumericDate number[^2].

**recommendation:** usually this can be omitted.

**iat (Issued At)**
The time when the token was issued, and can be used to calculate the age of the token.

This is a NumericDate number[^2].

**recommendation:** Set this equal to current date/time as the token is created.

```
"iat": 1591956000
```

**jti (JWT ID)**
Unique key for identifying the JWT.

**recommendation:** Generate a [UUID](https://en.wikipedia.org/wiki/Universally_unique_identifier) on the server.

```
"jti": "9a871145-6665-4e2c-9190-a6db1c50ad72"
```

[^2]: Time since 00:00 01 January 1970 GMT

**Example Payload**

<pre>
<span style="color:#d63aff">eyJzdWIiOiI0MjAwIiwibmFtZSI6Ik9sYSBOb3JkbWFubiIsImFkbWluIjp0cnVlLCJpYXQiOjE1OTE5NTYwMDB9</span>
</pre>

will decoded to: 

<pre>
<span style="color:#d63aff">{
  "sub": "4200",
  "name": "Ola Nordmann",
  "admin": false,
  "iat": 1516239022
}</span>
</pre>

In this example `name` and `admin` are private claims.

#### Signature

To create the signature part you have to take the encoded header, the encoded payload, a secret, the algorithm specified in the header, and sign that.

For example if you want to use the `HMAC SHA256` algorithm, the signature will be created in the following way:

```
HMACSHA256(
  base64UrlEncode(header) + "." +
  base64UrlEncode(payload),
  secret)
```
The signature is used to verify the message wasn't changed along the way, and, in the case of tokens signed with a private key, it can also verify that the sender of the JWT is who it says it is.

**Example Signature**

<pre>
<span style="color:#00b9f1;">OK4eTjie_R-QzzK2u3eowncawjVyP-GntytWM_e3T-cpOevcA_vCHDpOmsb3dK1dORkBX2nm47ZOCWKQ5ZRke1VLSo3456BAVlUeDugIKKpKZs6edKs9eWgy41vl110ynrQ63UxF38Z0TVYCEzLmOsjuqWHJhFqQ89o0AziGoZcqkpg4ToEStbSG5d75J36XV8aR2OMSdAM3Yn95igFoQ2uogw1T3cXdAdondKpNGC9EU2Edx-Qnb1jHaO_I0mzrlmCNiGgiLcluVlI780NUS-_6k8aHPG4zcgP7eiPn2LqeXK2G5gTJDhWURYKAtujVyRDbniJvfP5b5GbsV1Bc3Q</span>
</pre>

> In most situations, a JWT is generated for us. We just provide some specifics and the encoding and signed is done automatically.

Once you have a valid access JWT form a provider, you typically pass it to the resource server for validation using the `Authorization` header as a `Bearer` token in the request.

## Authentication flows

There are numerous authentication scenarios. These are often referred to as flows and are dictated by the Web application design. A flow which you are most likely familiar with is the `implicit flow`.  

> Most application need to use this flow so you have likely experienced it. 

### Implicit flow

You navigate to a Website you attempt to do something which requires authentication, you get redirected, login or register and then continue once logged in.
Implicit Flow
 

1.	Request Denied. Redirect to login.
2.	Login successful. Get Access Token in return
3.	If Registering user is added to database
4.	Present access token in request. Request successful. Get Resource.

There are various other flows. For example if a Website allows a third party login service such as Google, Twitter or Facebook and it needs information from these services an `Auth Code flow` would be more common.

### Auth Code Flow 
 

1.	Attempt access to endpoint.
2.	Get redirected to external Identity Provider (IdP) i.e. Google
3.	Login at IdP. Get Auth Code in response. Get redirected to callback inside Identity Server (local web API).
4.	Identity Server (i.e. local web API) exchanges auth code for access token with external IdP.
5.	Identity Server uses access token to interact with Google API to get profile information to register user account (or link external identity).
1.	Identity Server registers, or associates with, a
user in the local database.
6.	Identity Server returns a local access token (i.e. Implicit Flow) to the User Agent (this is separate to the access token from Google)
7.	The user accesses endpoint using local access token (Implicit flow continued)

The implicit flow still happens within the Auth Code flow hence implicit. Users are still stored in the local DB but the authentication and initial user information comes from the 3rd party service
The detail about how to use the protocols in Web application flows is detailed in the official documentation. LINK.
Web developers typically follow all of the naming conventions, return types, expected values and security measures defined in these protocols. 

It is expected of industry standard developers. The above mentioned flows are some examples but a true understanding of correct authentication within a Web application take experience and time. There are numerous dynamics and design considerations and conventions. These are outside of the scope of this course. The authentication approach used in this course is designed to make the nuances of the process easier and clear. This is discussed in the Autherization Services topic.

## Cookies and Sessions

An important function of browsers is the storage and tracking of information about a user and his/activity. This is done through data stored in the browser known as a cookie. It contains information which effects HTTP requests. This is how Web applications communication specifics back and forth about a user. It is an important part of authentication. Browser storage is done with either cookies or session variables. Cookies are only stored on the client-side machine, whereas sessions get stored both client-side and server-side.

**Session**

A session creates a file in a temporary directory on the server where registered session variables and their values are stored. This data will be available to all pages on the site during that visit. A session ends when a user closes the browser or after leaving the site; the server will terminate the session after a predetermined period, commonly 30 minutes.

**Cookies**

Cookies are text files stored on the client computer and are kept for tracking purposes. A sever script sends cookies to the browser and will contain information about the client; for example: name, age, etc. The browser stores information on a local machine for future use. The next time a browser sends any request to a web server, then it sends those cookies information to the server and the information is used to identify the user. 

## Tokens in authentication

To understand the advantages of token-based authentication and what problems it soved, it need to be contrasted with cookie-based authenticaiton.

**Cookie-based authentication**

The default, tried-and-true method for handling user authentication for a long time. It is _stateful_,meaning that an authentication record or session must be kept both server and client-side. The server keeps track of active sessions in a database. While on the front-end a cookie is created that holds a session identifier.

Cookie-based authentication flow has the following order:

1. User enters their login credentials.
2. Server verifies the credentials are correct and creates a session which is then stored in a database.
3. A cookie with the session ID is placed in the users browser.
4. On subsequent requests, the session ID is verified against the database and if valid the request processed.
5. Once a user logs out of the app, the session is destroyed both client-side and server-side.

**Token-based authentication**

Increased in popularity due to _Single Page Applications_ and the _Internet of Things_. Tokens go hand-in-hand with these technologies due to ease of storage.

> A JWT is normally the token of choice.

The big advantage of tokens is that they are _stateless_. This means there does not need to be a record kept on the server. This record would typically be a session about who is logged in. How token-based authentication gets around tracking who is logged in, is by using _validation_. Looking when tokens were issued, who they were issued by, and when they expire; these tokens are sent to the server for verification on every request.

> Recall, RESTful APIs need to also be stateless. This combination has allowed REST and Tokens to become dominant. 

Token-based authentication flow has the following order:

1. User enters their login credentials.
2. Server verifies the credentials are correct and returns a signed token.
3. This token is stored client-side, most commonly in local storage - but can be stored in session storage or a cookie as well.
4. Subsequent requests to the server include this token as an additional Authorization header or through one of the other methods mentioned above.
5. The server decodes the JWT and if the token is valid processes the request.
6. Once a user logs out, the token is destroyed client-side, no interaction with the server is necessary.

#### Advantages

The following subsections deal with each aspect that can be considered as an advantage, this is done as a summary. There are other aspects but the ones listed below are a generalization.

**1. Stateless, Scalable, and Decoupled**

Being stateless is the biggest advantage as the backend doesn't have to keep record of tokens. The server's only job is to check validity as tokens are even signed by third parties like OAuth.

**2. Cross Domain and CORS**

Cookies work well with single domains and sub domains. However, managing them across domains is complicated as they have a specific browser they are attached to. Token-based approach with CORS enabled makes it trivial to expose APIs to different services and domains.

**3. Store Data in the JWT**

With a cookie-based approach, you simply store the session id in a cookie. Tokens can store any metadata as long as it is valid JSON.

> Be careful however – tokens are signed not encrypted by default, so don't store secret information like passwords.

**4. Performance**

When using the cookie-based authentication, the back-end has to do a lookup to a database. This takes longer than decoding a token as it is a network round trip. As you can store additional data inside the JWT, you can pass the user's permission level. This allows you to save an additional lookup call to get and process the requested data.

**5. Mobile Ready**

Modern APIs do not only interact with the browser. A single API can serve both the browser and native mobile platforms like iOS and Android. Native mobile platforms and cookies do not mix well, as there is no easy way to store and use a cookie - they rely on other methods of storage. 

Tokens, on the other hand, are much easier to implement on both iOS and Android. Tokens are also easier to implement for Internet of Things applications and services that do not have a concept of a cookie store.

## Spring Security introduction

Spring Security is a powerful and highly customizable authentication and access-control addition built into the Spring Framework. 

> It is the standard for securing Spring-based applications.

It has:
- Comprehensive and extensible support for both Authentication and Authorization
- Protection against attacks like session fixation, clickjacking, cross site request forgery, etc
- Servlet API integration
- Optional integration with Spring Web MVC

and much more.

Spring Security is set up through `@Configuration` classes. In these classes you can specify which endpoints need to be protected and what needs to happen if authentication is failed.

> By protected, we mean that the user is logged in and has the rights to see the resources.

Spring Security is also heavily integrated with token-based authentication and other 3rd party libraries. It is able to use and create roles for users and work with claims - two common features in JWTs.

> Roles and claims is the access control part.

---

Copyright 2021, Noroff Accelerate Ats