# Spring Security and IPDs
This lesson covers using an external identity provider to issue JWTs and manage users. The example used throughout this lesson uses Keycloak. This includes a basic front end (client) that Keycloak interacts with and issues valid access JWTs, and a Java resource server that validates the authenticity of the provided JWT then allows or denies access to protected resources. Other external methods are also mentioned, this includes using Google and the Firebase SDK.

**Learning objectives:**

You will be able to...
- **construct** a Keycloak instance locally, given appropriate Docker images.
- **generate** and **configure** realms, clients, and claims. Given a working local Keycloak instance.
- **create** a simple client to recieve access JWTs, given a configured Keycloak instance.

---

Recall in the Spring Security Basics lesson, we did everything internally. This means that our Java application is responsible for:
- User registration
- User authentication
- JWT generation
- JWT validation
- Role-based authorization

This is the traditional monolithic approach. There is nothing wrong with doing it this way, however, there has been a shift in industry to use external identity providers.

> This is because of two reasons, the first being a more microservices approach, and the second is that is is far simpler to let the user creation and authentication be handled somewhere else, by someone else.

In this case, we are going to lean heavily on a very popular federated authority provider called [Keycloak](https://www.keycloak.org/).

In this case, Keycloak does the following for us:
- User registration
- User authentication
- JWT generation

This leaves the following up to our Java application:
- JWT validation
- Role-based authentication.

> This configuration forces us to approach security in a slightly different manner as before.

## Project structure

We will need three independent systems:
- A front end client with functionality to access our other systems. This can be an Http service to connect to our Java server and Keycloak instance.
- An Authorization server, this is Keycloak. It will be running on our computer as a seperate application through Docker.
- A resource server, this is our Java backend that holds the resources.

> Our users and passwords are stored in Keycloak. You can sync Keycloak up to an existing database, but we are not doing that in this lesson.

The following diagram details the flow of the application:

![Flow](./resources/authflow.png)

1. An unauthenticated user attempts to access our resources.
2. They are redirected to the login/register for Keycloak. Here they either login with existing credentials or create a new account. This is stored in Keycloak.
3. After a successful registration the client recieves a JWT token which can now be used to access our server.
4. The client makes another request, this time with a valid token, to our server. The token is validated against what weconfigure to be a valid issuer.
5. Once the token is validated, any resources are fetched or added to the database.
6. The client then recieves the response from our server.

> We will configure our Java application as a resource server. 

We will first focus on our Authorization server, which is an instance of Keycloak.

## Common terminology

We have already seen some OAuth2.0 and OpenID connect terms in our previous lessons. However, this is a full list of everything we will need to understand to 1. Use Keycloak properly, and 2. Setup our resource server.

> We are doing Auth code flow now

**Resource Owner**

You, the owner of your identity, your data, and any actions that can be performed with your accounts.

**Client**

The application that wants to access data or perform actions on behalf of the _Resource Owner_.

**Authorization Server**

The application that knows the _Resource Owner_, meaning; where the _Resource Owner_ already has an account.

**Resource Server**

The Application Programming Interface (API) or service the Client wants to use on behalf of the Resource Owner.

**Redirect URI**

The URL the _Authorization Server_ will redirect the _Resource Owner_ back to after granting permission to the _Client_. This is sometimes referred to as the "Callback URL".

**Response Type**

The type of information the _Client_ expects to receive. The most common _Response Type_ is code, where the _Client_ expects an _Authorization Code_.

**Scope**

These are the granular permissions the _Client_ wants, such as access to data or to perform actions.


**Consent**

The _Authorization Server_ takes the _Scopes_ the _Client_ is requesting, and verifies with the _Resource Owner_ whether or not they want to give the _Client_ permission.

**Client ID**

This ID is used to identify the _Client_ with the _Authorization Server_.

**Client Secret**

This is a secret password that only the _Client_ and _Authorization Server_ know. This allows them to securely share information privately behind the scenes.

**Authorization Code**

A short-lived temporary code the _Client_ gives the _Authorization Server_ in exchange for an _Access Token_.

**Access Token**

The key the client will use to communicate with the _Resource Server_. This is like a badge or key card that gives the _Client_ permission to request data or perform actions with the _Resource Server_ on your behalf

> This is our JWT

> In our Spring Security Basics lesson we made our resource server and authorization server the same application. Doing this allowed us to avoid a lot of these terms as we did not have to use them.

## Keycloak

This section is going to provide some light introduction to what keycloak is and what it offers us.

> Keycloak acts as our Authorization server

Keycloak is an open source _Identity and Access Management solution_ aimed at modern applications and services. It makes it easy to secure applications and services with little to no code.

> Keycloak provides our roles/claims/scope as well

**Single-Sign On**

Users authenticate with Keycloak rather than our Java application. This means that your applications doesn't have to deal with login forms, authenticating users, and storing users. Once logged-in to Keycloak, users don't have to login again.

> A single Keycloak instance can be the Authorization server for multiple applications.

**Identity Brokering and Social Login**

Enabling login with social networks is easy to add through the admin console. It's just a matter of selecting the social network you want to add. No code or changes to your application is required.

> The admin console is a very powerful interface we use to do all our identity configuration.

**User Federation**

Keycloak has built-in support to connect to existing LDAP or Active Directory servers. You can also implement your own provider if you have users in other stores, such as a relational database.

> We will not be doing this for our demonstraton.

**Client Adapters**

_Keycloak Client Adapters_ makes it really easy to secure applications and services. Keycloak has adapters available for a number of platforms and programming languages.

**Admin Console**

Through the admin console administrators can centrally manage all aspects of the Keycloak server. You can:
- Enable and disable various features. They can configure identity brokering and user federation.
- Create and manage applications and services, and define fine-grained authorization policies.
- Manage users, including permissions and sessions.

![Keycloak admin](./resources/keycloak_admin.png)

**Account Management Console**

Through the account management console users can manage their own accounts. They can update the profile, change passwords, and setup two-factor authentication. Users can also manage sessions as well as view history for the account.

If social login or identity brokering is enabled, users can also link their accounts with additional providers to allow them to authenticate to the same account with different identity providers.

### Creating a Keycloak instance 

To set up keycloak locally, we simply make use of Docker. There are clear instructions on how to setup an instance of Keycloak locally [from jBoss](https://github.com/keycloak/keycloak-containers/blob/12.0.2/server/README.md).

We will be creating an instance of Keycloak locally with the username and passwork of `admin` and `admin`. To do this we open a terminal and run:
```bash
docker run -p 8083:8080 -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin jboss/keycloak 
``` 

> The `-e` stands for environment vaiable.

> Give it some time to start up.

> We are expoing port `8083` because our resource server will run on `8080`.

If you navigate to `http://localhost:8083/auth/admin` you should be greated with a Keycloak login screen. 

![Keycloak login](./resources/keycloak_login.png)

Now we can go into keycloak and start setting things up through our admin.

### Setting up our users

Once we have logged in to our account, we can mange: realms, users, credentials, scopes, claim, and much more. 

> This section is adapted from the official [Keycloak docs](https://www.keycloak.org/docs/latest/server_admin/index.html#admin-console).

**Realms**

When we launch Keycloak for the first time we are greated with a _master realm_ this realm is for admins and has access to all other realms you create. We should `NOT` be using this realm for our users. We should make our own one.

To make a new realm we hover over the realm name in the top left and select `Add realm`.

![Realm menu](./resources/add-realm-menu.png)

Then you will be greated with the `Add realm` page:

![Realm add](./resources/create-realm.png)

Here we provide a name, in our case `noroff`.
> We can also import a realm. Keycloak lets you import and export for any settings, so it is possible to configure a Keycloak instance, export its information as JSON and then import the settings when deploying.

Once we have created a realm, we can configure how users will log in. You can manage various aspects, such as:
- If users can register themselves
- Having their email be their username
- Allow them to reset passwords
- Remember me functionality
- Email verification
- SSL requirements

Out of all of these we only care about two for our purposes:
- Users being able to register
- SSL

We want users to be able to register themselves, this way we dont have to create predefined users. And the Require SSL is about HTTPS connections, by default it is on external. This means that users can interact with Keycloak without SSL so long as they stick to private IP addresses like localhost, 127.0.0.1, 10.x.x.x, 192.168.x.x, and 172.16.x.x. If you try to access Keycloak without SSL from a non-private IP address you will get an error.

![Realm login](./resources/keycloak_realm_login.png)

We can manage users from the side menu. This allows us to:
- Search existing users
- Create new users
- Delete users
- Change users credentials
- Require actions like; updating passwords, configuring OTPs, verifying emails, ect.

> The user management is out of scope of what we are doing for this lesson, it is still a very powerful tool and you should [learn about it](https://www.keycloak.org/docs/latest/server_admin/index.html#user-management).

> We are going to create a dummy user to configure our resource server.We are setting a password as well.

![User create](./resources/keycloak_user_create.png)

When we set `User registration` we enabled a new page the users can interact with, the registration. This is accessed by a `register` link appearing on the login form.

> These forms can be customized using [themes](https://www.keycloak.org/docs/latest/server_development/#_themes).

![Keycloak login](./resources/registration-link.png)

![Keycloak register](./resources/registration-form.png)

You can even add Google reCAPTCHA to your forms:

![Keycloak flow](./resources/registration-flow.png)

> You access this by going to the `Authentication` left menu item and go to the `Flows` tab. Select the `Registration` flow from the drop down list on this page.

> This requires a setup with secrets, we are not covering this.

**Clients**

Clients are entities that can request authentication of a user. These are the front end aspects that try access our resources and are diverted to Keycloak. 

We want to create a front end client in Keycloak so we can connect it to our Keycloak instance. We do this by naviagating to `clients` on the left hand side and selecting `create`.

> We will try keep the front end client as simple as possible as this is a backend focussed course.

![Client page](./resources/clients.png)

![Client creation](./resources/keycloak_client_creation.png)

The important aspects to consider are the `Valid redirect URIs` and `Web Origins`, these configure Keycloak to be able to recieve traffic from that url and redirect to subdomains of it.

> We can assume for now we have a web-based application running and acting as our front end at `http://localhost:8000`.

> The Client ID is important as it is used to identity the application for OIDC requests. We also want this client to be able to do browser logins, so we do NOT configure its access type to be _bearer-only_.

> Keycloak is now configured to recieve requests from the client, then people can start registering and getting access tokens.

## Front end client

As stated before, we are keeping this as simple as possible as this is a backend focussed course. We will be using one of the many adapters keycloak has. We are going to make a simple JavaScript based application (no fancy React), and use the [Keycloak JavaScript Adapter](https://www.keycloak.org/docs/latest/securing_apps/index.html#_javascript_adapter) to configure our front end.

To configure our application to point to Keycloak and use it, we need to get the Keycloak.js script and download our `Keycloak.json` config.

We get our script from `http://localhost:8083/auth/js/keycloak.js` and our json config can be downloaded from our client on Keycloak by going to the installation tab and selecting Keycloak OIDC JSON.

![Client creation](./resources/keycloak_client_json.png)

We create a very simple application that has several functions:
- Login
- Logout
- Refresh
- Access Token JSON
- Access Token
- Refresh Token

All these functions make use of the built-in functionality of the `keycloak.js` functions. We are writing nothing ourselves.

```javascript
<ul class="nav navbar-nav navbar-primary">
    <li><a href="#" onclick="kc.login(login)">Login</a></li>
    <li><a href="#" onclick="kc.logout()">Logout</a></li>
    <li><a href="#" onclick="kc.updateToken(-1)">Refresh</a></li>
    <li><a href="#" onclick="output(kc.idTokenParsed)">ID Token JSON</a></li>
    <li><a href="#" onclick="output(kc.tokenParsed)">Access Token JSON</a></li>
    <li><a href="#" onclick="output(kc.idToken)">ID Token</a></li>
    <li><a href="#" onclick="output(kc.token)">Access Token</a></li>
    <li><a href="#" onclick="output(kc.refreshToken)">Refresh Token</a></li>
</ul>
```

> The output() function simply adds elements to the DOM to render out the returned data.

This front end will simply print out tokens so we can decode and have a look at them. See what claims are inside and so on. This will better allow us to setup our tokens for consumption by our resource server. 

> The code for this front end can be found [here](https://gitlab.com/NicholasLennox/keycloak-simple-client).

> Instructions on how to run the Docker container for that application can be found in its README.md

Navigating to `http://localhost:8000`, we are redirected to our _Keycloak_ login.

![Client login](./resources/keycloak_client_login.png)

> Notice we are on the `noroff` realm.

We can either register a new user or use our created user from before:
- Username/email: nicholas.lennox@noroff.no
- Password: password

This results in us logging into our client, and being presented with the following screen:

![Client logged in](./resources/keycloak_client_logged.png)

> By default our front end is configured to show the users name.

Recall, we use access tokens to access protected resources. Lets click on Access Token and see what the token looks like:

![Client access token](./resources/keycloak_client_access_token.png)

You should notice this token is much longer than the one we generated with Spring Security. That is because that token only contained the username as a subject. This token contains a lot more, as it comes from an industry recognized proiver. Heading over to [jwt.io](https://jwt.io/) and going to the debugger, we paste this token in and get out:

![Client access token header](./resources/keycloak_client_token_header.png)

![Client access token payload](./resources/keycloak_client_token_payload.png)

![Client access token signature](./resources/keycloak_client_token_signature.png)

There are a few vital things to note here about the token: scope and roles are included - we can use this to add our custom roles, and the signature is actually encrypted. This is how a proper JWT is constructed, what we did in the generation was the most straight forward bare bones approach. We would ideally add some extra information to the payload, but for the purposes of that lesson, it just would add bloat. 

> The difference between this project and what was made for Spring Security Basics lesson, is that this project is following _OAuth2.0_ and _OIDC_ with scopes and identity providers. Whereas that project was doing simple implicit flow with light _OAuth2.0_ as it didnt need an external identity provider.

### Adding roles

We can add realm-level roles, or client-level roles in Keycloak. This is down to scope and your descisions. We are going to add some roles to our client.

- User
- Moderator
- Administrator

To add these roles, we navigate to Clients, then select our my-app client and go to the roles tab.

![Client roles](./resources/keycloak_client_roles.png)

Then we add the three roles:

![Client roles added](./resources/keycloak_client_roles_added.png)

Now we can assign these roles to our users, in this case we are going to make `nicholas.lennox@noroff.no` an _Administrator_. We do this by going to _Users_, then finding our user, going to _Role Mappings_, then selecting `my-app` from _Client Roles_ dropdown, and adding the _Administrator_ role.

![Client roles assigned](./resources/keycloak_client_roles_user.png)

If we go back to our client and login again, then decode the JWT, we will find our roles have been added, under `my-app`. They would appear under `realm_access` if we had made the role a _realm-role_ not a _client-role_.

![Client new roles assigned](./resources/keycloak_client_token_payload_newroles.png)

The same process but with global roles:


![Client realm roles](./resources/keycloak_realm_roles.png)


![Client new roles assigned](./resources/keycloak_client_token_payload_realmroles.png)

> Notice how our _Administrator_ role is now in `realm_access`.

We can also set default roles for new users of the system. We can do this by navigating to _Roles_ and selecting the _Default Roles_ and then assigning over. Here is an example where provide the default role of `User`, meaning any new users will have the `User` role.

![Keycloak default role](./resources/keycloak_default_roles.png)

## Resource server

This is the next step. We need to create a Java Spring app with Spring Security and the follwoing dependency.  

```
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-oauth2-resource-server</artifactId>
</dependency>
```

We then need to add to our `application.properties` file to configure our Keycloak instance as a issuer of JWTs.

```
# Configure Spring Security with Keycloak tokens (will just be different URLs if you go with another IDP)
spring.security.oauth2.resourceserver.jwt.issuer-uri= http://localhost:8083/auth/realms/noroff
spring.security.oauth2.resourceserver.jwt.jwk-set-uri= http://localhost:8083/auth/realms/noroff/protocol/openid-connect/certs
```

We need to alter a few things in our Keycloak config for this to work as intended.

> This is because we dont have control over how we assign authorities to our users as they are created elsewhere. 

Our goal is to get our access tokens to display the roles of our user in the root of the token, as seen in the snippet below:

![Roles in root of scope](./resources/Keycloak-roles-intoken.png)

To get our claims like this we need to alter the mappings for realm-roles. If you dont already have roles added as scope, then you can do it by following the screenshot below. Adding roles to scope makes the realm-roles appear in our claims.

![Roles in claims](./resources/Keycloak-roles-scope.png)

Once you have added roles to the scope, then you can alter how roles are represented. This is done by altering the mappings, done in the screenshot below.

![Roles in claims](./resources/Keycloak-roles-edited.png)

Once you have done the steps above, you should see roles in the root of the token. Once we have the roles configured, we can make some alterations in Spring Security to use the roles to provide access control. 

Previously, we used `granted authorities` to do access control, when we recieve a token from an IDP, Spring Security makes these authorities from the scope of the JWT. We need to tell Spring Security to use roles instead of scope for authorities.

We do this by creating a `JwtGrantedAuthoritiesConverter` class and configure it to use our `roles` claim and add a prefix of `ROLE_`. We want to do this so we can use `hasRole` or `@PreAuthorize` as we have previously.

```java
@Bean
public JwtGrantedAuthoritiesConverter jwtGrantedAuthoritiesConverter() {
  JwtGrantedAuthoritiesConverter jwtGrantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
  jwtGrantedAuthoritiesConverter.setAuthoritiesClaimName("roles");
  jwtGrantedAuthoritiesConverter.setAuthorityPrefix("ROLE_");
  return jwtGrantedAuthoritiesConverter;
}
```

What we have done here, is tell Spring Security:

- Use roles as our claim thats coverted into authorities (instead of the default scope)
- Add a ROLE_ prefix to each authority.

This will result in our Principal object having the following values for its authority:

```json
"authorities": [
  {
    "authority": "ROLE_User"
  }
],
```

Now we can use `.hasRole("User")`, or `.hasAuthority("ROLE_User")`, or `.access("hasRole('User')")` in our `SecurityConfig`. This is shown below in a snippet of our `SecurityConfig`:

```java
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {
  @Override
  protected void configure(HttpSecurity http) throws Exception {
    // Custom converter to show roles instead of scopes
    final JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter();
    jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(this.jwtGrantedAuthoritiesConverter());

    http.cors()
      .and()
      .authorizeRequests()
      .antMatchers("/user/info", "user/info/principal").permitAll()
      .antMatchers("/api/resources/user").hasRole("User")
      .antMatchers("/api/resources/admin").hasRole("Administrator")
      .anyRequest()
      .authenticated()
      .and()
      .oauth2ResourceServer()
      .jwt()
      // Using our converter
      .jwtAuthenticationConverter(jwtAuthenticationConverter);
    }

    // Implementation of replacing authorities with our roles
    @Bean
    public JwtGrantedAuthoritiesConverter jwtGrantedAuthoritiesConverter() {
      // You can use setAuthoritiesClaimName method
      JwtGrantedAuthoritiesConverter jwtGrantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
      jwtGrantedAuthoritiesConverter.setAuthoritiesClaimName("roles");
      jwtGrantedAuthoritiesConverter.setAuthorityPrefix("ROLE_");
      return jwtGrantedAuthoritiesConverter;
    }
}
```

An implementation of this resource server can be found [here](https://gitlab.com/NicholasLennox/spring-security-keycloak-resource).

> The last thing to do is find a way to assign roles to users through Keycloak, that doesnt involve manaully setting it. There are some Keycloak REST API calls you can make, seen [here](https://www.keycloak.org/docs-api/5.0/rest-api/index.html).

---

Copyright 2021, Noroff Accelerate AS
