# 4. Jobbe med Git

# **Jobbe med Git**

Git er en distribuert versjonskontrollprogramvare, som lar deg samarbeide med andre mens du opprettholder *snapshots* av koden din på definerte punkter. Git brukes ofte på en online (distribuert) måte, men den kan også brukes utelukkende lokalt på enheten din. Samarbeidende programvareutvikling

Git er en kritisk del av fremtiden din som utvikler!

## **Installasjon og oppsett**

I alle tilfeller er installasjonen av Git relativt rask og enkel.

På Windows:

Installer `Git for Windows`tilgjengelig [her](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://gitforwindows.org/)

På Macintosh (du må installere [Homebrew](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://brew.sh/) først):

```
$ brew install git
```

På Linux bruk din foretrukne pakkebehandling:

```
$ sudo apt-get install git
```

Når den er installert, kan du sjekke versjonen ved å kjøre følgende kommando:

```
$ git --version
git version 2.21.1
```

Fra dette tidspunktet kan du begynne å bruke Git på en usikret måte, men for dette kurset anbefaler vi sterkt å bruke SSH til enhver tid.

## **SSH-nøkler**

### **Sette opp nøkler**

### **Åpne en terminal**

**Windows:**

Bruk git bash:

- Åpne `Explorer`
- Høyreklikk i en hvilken som helst mappe
- Velg `Open Git Bash here`

> Hvis du støter på tillatelsesfeil senere, må du kanskje kjøre Git Bash på administrativt nivå
> 

**Mac eller Linux:**

Enhver terminal vil gjøre det!

### **Sjekk om du allerede har en nøkkel**

> Hvis du allerede har en nøkkel, IKKE generer en ny!
> 
> 
> I så fall hopper du videre til `Copying the key to the clipboard`
> 

Kjør følgende kommando fra terminalen vi nettopp åpnet:

```bash
$ ls -al ~/.ssh
```

> Disse kommandoene vil ikke fungere fra Windows-ledeteksten, de kan fungere fra Windows Powershell, men for konsistens anbefaler vi å bruke Git Bash.
> 

Kommandoen ovenfor viser en liste over filer. Hvis noen finnes, vil de være i par (en uten utvidelse og en annen med `.pub`utvidelse).

Dette bildet viser nøklene mine (og `known_hosts`filen) på min MacBook:

![https://noroff-accelerate.gitlab.io/java/course-notes/21_WorkingGIT/resources/02_keys.png](https://noroff-accelerate.gitlab.io/java/course-notes/21_WorkingGIT/resources/02_keys.png)

> Disse vil enten være `id_rsa` eller `id_ed25519`
> 
> 
> Den andre halvdelen angir krypteringen som er brukt, `ed25519`anbefales nå i stedet for på `rsa` grunn av sikkerhetshensyn.
> 

Hvis det finnes en fil, har du allerede en nøkkel(er)!

**Den offentlige nøkkelen**

Denne filen slutter med `.pub`filtypen ( `id_ed25519.pub`), og kan deles fritt. Den brukes av andre for å bekrefte identiteten din.

> Tenk på det som ditt bilde-ID, når noen ser det kan de bekrefte at du er den du sier du er.
> 

**Den private nøkkelen**

Denne filen slutter uten utvidelse ( `id_ed25519`)

Dette er den private nøkkelen, og skal **ALDRI** kopieres eller deles!

> Hvis den offentlige nøkkelen er bilde-ID, vil den private nøkkelen være ditt fysiske ansikt.
> 

![https://noroff-accelerate.gitlab.io/java/course-notes/21_WorkingGIT/resources/03_sharekeys.jpg](https://noroff-accelerate.gitlab.io/java/course-notes/21_WorkingGIT/resources/03_sharekeys.jpg)

### **Genererer en nøkkel**

> Er du sikker?
> 
> 
> Denne prosessen er ikke-reversibel, og hvis du har brukt en nøkkel et annet sted, må du oppdatere den igjen med en ny generert nøkkel.
> 

De følgende trinnene vil generere en ny nøkkel ved å bruke `ed25519`, men les gjennom alle trinnene før du fortsetter, siden det er flere spørsmål som vises.

1. Start med følgende kommando, bruk e-posten din i stedet for `you@example.com`
    
    :
    

```bash
$ ssh-keygen -t ed25519 -C "you@example.com"
```

1. La nøkkelen opprettes på standardplasseringen:

```bash
/something/something/.ssh/id_ed25519
```

Du må trykke`Enter`

1. Du vil bli bedt om å skrive inn en passordfrase (passord) og gjenta den etterpå.
    
    > Spørringen vil ikke vise tegn (eller ”*” mens du skriver)
    > 
2. Når du er ferdig, vil du bli presentert en visuell representasjon av nøkkelen din.
3. Sjekk om du har en nøkkelagent som kjører:

```bash
$ eval $(ssh-agent -s)
```

Du skal få presentert en `pid`(prosess-ID) og et nummer, dette indikerer at prosessen kjører.

> Kommandoen `ssh-agent -s`starter agenten hvis den ikke kjører.
> 
1. Legg til nøkkelen din til agenten:

```bash
$ ssh-add ~/.ssh/id_ed25519
```

På dette tidspunktet kan du bli bedt om å angi passordfrasen for nøkkelen.

Nøkkelen er nå generert og klar til å brukes for å identifisere deg, men du trenger en egen nøkkel for hver enhet (datamaskin) du ønsker å bli identifisert fra.

Ved å bruke samme e-postadresse er det mulig å ha flere enheter *registrert* som din identitet, for eksempel en stasjonær og en bærbar datamaskin. Disse forskjellige enhetene har unike nøkler som kan ha separate passordfraser.

### **Kopierer nøkkelen til utklippstavlen**

For å bruke nøkkelen (som på Github/Gitlab) må du kopiere den ut av `id_ed25519.pub`filen, dette er enkelt å oppnå med en av følgende kommandoer:

På Windows:

```bash
$ clip < ~/.ssh/id_ed25519.pub
```

På Macintosh:

```bash
$ pbcopy < ~/.ssh/id_ed25519.pub
```

På Linux (installer `xclip`først):

```bash
$ sudo apt-get install xclip
$ xclip -sel clip < ~/.ssh/id_ed25519.pub
```

> I kontoinnstillingene dine på Github/Gitlab må du lime inn nøkkelen og gi den et identifiserende navn, det er en god idé å matche dette navnet til selve datamaskinen.
> 

### **Globale innstillinger**

Selv om SSH-nøkkelen er veldig sikker og har e-postadressen vår lagret i den, er det fortsatt to innstillinger vi kan angi i Git-applikasjonen for å hjelpe den bedre å *gjenspeile* identiteten vår.

Vi starter med å sette vår `global user name`, som følger:

```bash
$ git config --global user.name "your name"
```

Dette sikrer at git-meldingene våre vil ha navnet vårt knyttet til seg som standard.

For det andre spesifiserer vi eksplisitt e-postadressen vår som den vi angir i nøkkelen vår:

```bash
$ git config --global user.email you@example.com
```

Disse to trinnene kan virke trivielle, men i det lange løp vil de hjelpe oss å jobbe lettere med Git.

## **Kloning**

I en senere leksjon skal vi bruke SQLite og trenger en JDBC-driver, og vi vil bruke en driver som er en del av `org.xerial`pakken. Anta at mens vi bruker denne driveren støter vi på en feil. Vi bør starte med å sende inn en `Issue` på  [GitHub-siden for sqlite-jdbc](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://github.com/xerial/sqlite-jdbc) .

Vi kunne som utviklere også laste ned en kopi av kildekoden og prøve å finne problemet i koden selv. Ser vi på depotet ( *repo* ), kan vi se en grønn `Code` knapp. Ved å trykke på denne knappen vil vi (avhengig av kontoen vår) ha noen alternativer:

- Last ned (Last ned som ZIP)
- Klone med HTTPS
- Klon med SSH

> På GitLab er det i stedet en blå Cloneknapp.
> 

### **Last ned (Last ned som ZIP)**

Dette lar oss bruke nettleseren vår til å laste ned den gjeldende grenen av *repoet* som et komprimert arkiv, dette er nyttig når vi bare vil se hvordan en kodebit ble utført eller når vi ikke vil bidra tilbake til *repoen* .

Hvis vi ønsker å få en oppdatert versjon av en *repo* som vi lastet ned på denne måten, må vi laste ned den nye versjonen manuelt (overskrive eksisterende filer uten hensyn).

### **Klone med HTTPS**

Kloning med HTTPS lar oss bruke git-klienten vår til å laste ned en kopi av *repoet.* Ved å bruke den samme klienten kan vi hente nye oppdaterte versjoner av *repoet* og oppdatere filer med git versjonskontrollsystemet.

Ved å bruke kontoen brukernavn/passord-paret kan du bidra med endringene dine til et eksternt *repo* . Mens GitHub foreslår dette som den anbefalte måten å koble til, er vi uenige og foreslår i stedet bruk av SSH-nøkler.

### **Klon med SSH**

På samme måte som HTTP-klonen, vil SSH-versjonen fungere med en git-klient og lar oss enkelt hente endringer. Det vil imidlertid ytterligere gi oss muligheten til å bidra direkte til det samme *repoet* ved å bruke vår SSH-legitimasjon.

Dette vil spare mye tid i det lange løp og er den foretrukne metoden som bør brukes når man jobber med git. Gjennom denne metoden er de eneste enhetene som kan endre *repoene* dine de som er manuelt konfigurert av deg, og de krever unike passordfraser for å fungere.

**Problemer med port 22**

Hvis du støter på nettverksrelaterte problemer på grunn av bruk av port 22, kan du legge til følgende i `config`filen i `~/.ssh/`:

```
Host gitlab.com
 Hostname altssh.gitlab.com
 User git
 Port 443
 PreferredAuthentications publickey
 IdentityFile ~/.ssh/id_ed25519
```

Husk å lagre filen, og du må kanskje starte bash-økten på nytt.

## Remotes **(Origin)**

Remotes representerer de forskjellige serververtsbaserte versjonene av kildekoden din. Disse kan være på fysiske datamaskiner du har satt opp eller kontoer med GitHub eller GitLab, alt som kjører git-serverprogramvaren.

> En guide for å sette opp din egen server finner du i [Git Server Documentation](https://git-scm.com/book/en/v2/Git-on-the-Server-Setting-Up-the-Server).
> 

Når du har en av disse *eksterne versjonene av et repo*, kaller vi det et *remote* .

Vi merker disse med praktiske navn, hvor standard er `origin`. Du kan fritt navngi remotes for din egen lettvinthet ved å bruke git-klienten din.

Personlig holder jeg et duplikat av klassenotatene mine i en sekundær remote som jeg har navngitt `backup`.

## Branches (grener)

Branching er en viktig del av samarbeidet. En branch er en annen versjon av samme kildekode på samme server.

Når vi brancher lager vi en ny branch ut av den forrige, dette kan sees på som en splittelse. Etter branchen har vi originalversjonen og branchversjonen. Disse kan nå endres separat uten å påvirke hverandre.

Standard første versjon av kildekoden din kalles `master`(som i master record).

Du kan fritt gi nytt navn `master`eller andre brancher etter eget ønske, men det anbefales å beholde `master` branchen.

Når vi beskriver en branch kan vi også angi dens plassering, for eksempel:

**Eksempler på brancher:**

```
origin/master
backup/master

origin/dev
origin/release
```

Når du refererer til lokale brancher (på din egen datamaskin) inkluderer du ikke remotenavnet.

**Lokale brancher:**

```
master
dev
release
```

### Branch**navn**

Branchnavn bør angi enten eierskap eller formål eller begge deler.

Så hvis jeg skulle lage en feilrettingsbranch (lokalt), ville jeg kalt den:

`craigm-bugfix`

Konvensjonen av `owner`- `purpose`er en enkel en som unngår forvirring.

### **Eksterne brancher**

Som vi sa tidligere, vil du ha en `master` branch (lokal og ekstern), men det er ikke typisk å lage mange eksterne brancher. I stedet bør eksterne brancher matche versjon, milepæler eller oppgaver:

**Eksempler på eksterne grener**

`origin/master
origin/test
origin/experimental
origin/dev
origin/release`

### Branchenes **gylne regel**

**Branche seg tidlig, branche seg ofte.**

Du kan alltid opprette så mange brancher du trenger, i praksis er det best å ikke gjøre dette på remotes, men si å gjøre på din lokale maskin.

Eksterne brancher vil være synlige for alle bidragsytere og kan skape unødvendig rot for dem.

## Commits

> En commits er et `save point` for koden din.
> 

Når du gjør endringer i koden din, vil du med jevne mellomrom ønske å lagre disse endringene. Disse endringene er forskjellige fra at du lagrer en fil i IDE-en din. I stedet er dette et felles valg for å registrere fremgang som er gjort.

Du kan skrive om og lagre et stykke kode mange ganger før du er fornøyd med hvordan det fungerer; alle de tidsbesparende filendringene og bygg nye versjoner av programvaren. Ikke før du er fornøyd med at du har gjort fremskritt på en oppgave, vil du bidra med endringene mot den endelige versjonen av prosjektet.

> Det er også lurt å få for vane å commite med jevne mellomrom gjennom dagen, for eksempel før lunsj eller når du pakker sammen på slutten av dagen.
> 

Selv om du gjør ytterligere endringer, kan du alltid tilbakestille dem til en commit.

> Commits er gjort til de lokale versjonene av *repoet* , ikke til remotes .
> 

### Commit meldinger

Hver commit du oppretter må ha en melding som beskriver hva den gitte commit har oppnådd.

Commit-meldinger bør alltid være to ting:

- Gir mening
- Kort

En commit-melding kan teknisk sett være flerlinjet, i praksis blir bare den første linjen noensinne sett på.

### Conventional **commits**

Conventional Commits er et sett med standarder satt ut for å hjelpe utviklere med å lage bedre forpliktelsesmeldinger.

Det anbefales sterkt at du følger spesifikasjonene til [Conventional Commits](https://www.conventionalcommits.org/) både i dine personlige prosjekter og mens du jobber i team.

Det har også den ekstra fordelen at ved å bruke en commit-meldingskonvensjon, kan du lage automatiserte programvareendringslogger for prosjektet.

## Pull og Push

Dette er de to metodene du vil bruke for å kopiere kildekodeendringer mellom det lokale *repoet* og det eksterne *repoet* .

Pull er handlingen for å hente endringer fra fjernkontrollen. Dette vil (som standard) trekke for **alle brancher** som finnes på remoten, dette er grunnen til at det er en god ide å utvikle brancher som bare eksisterer på din lokale datamaskin.

> Du bør alltid pull før push . Du har bedre (enklere) kontroll for å fikse problemer på din egen lokale maskin.
> 

Pushing er handlingen for å sende endringene dine til det eksterne *repoet*, men hvis noen har gjort endringer som ikke er kompatible med det du har gjort, vil du støte på problemer 1 .

> *1.* Selve volumet og typene feil som kan oppstå kan ikke dekkes her. I stedet foreslår vi at du følger et flittig sett med praksis for å unngå konfliktskapende scenarier.
> 
> 
> [↩](https://noroff--accelerate-gitlab-io.translate.goog/java/course-notes/21_WorkingGIT/?_x_tr_sl=en&_x_tr_tl=no&_x_tr_hl=no&_x_tr_pto=wapp#reffn_1)[Link til merging]
> 

## Merging

Til slutt må du lagre endringer som er gjort på en branch til en branch nærmere master (eller master selv).

Dette gjøres gjennom en merge.

### Merging **lokalt**

Fortell ganske enkelt git-klienten om å slå sammen en branch til en annen.

For eksempel kan jeg slå sammen `bugfix`til `dev`. Ved å gjøre dette ber jeg min git om å beregne hele forskjellen mellom de to branchene og prøve å "klemme" den ene inn i den andre. Hvis ingen motstridende endringer har skjedd, kan klienten automatisk slå sammen den ene branchen til den andre.

Hvis det oppstår konflikter, må du redigere filer manuelt for å fullføre sammenslåingen. Git-klienten din hjelper deg kanskje visuelt med denne prosessen.

> Du kan eventuelt slette sidebranchen etter sammenslåingen, så bare den siste branchen gjenstår.
> 

Denne typen sammenslåing er helt greit for deg å gjøre når du bare jobber med din egen kildekode, men mer typisk vil du være i et team og endringer kan påvirke kildekoden utover din egen.

### Merging **eksternt (foretrukket)**

Den bedre måten å merge brancher på er å lage en mergeforespørsel fra GitHub/GitLab-nettstedet.

Vi kan velge en branch og opprette en mergeforespørsel til en annen. Vi må skrive en kort beskrivelse for å forklare formålet med mergingen, og eventuelt kan vi gi noen til å vurdere den.

> Prøv alltid å få en annen utvikler til å vurdere mergen for deg.
> 

Den beste praksisen i dette scenariet er å la mottakeren være åpen, og i stedet bør den første tilgjengelige utvikleren håndtere mergeforespørselen. avhengig av prosjektet kan denne anmelderen trenge å være en seniorutvikler eller noen med forhøyede rettigheter på det eksterne *repoet* .

## **Visuelle klienter**

Det er mange visuelle klienter tilgjengelig som vil hjelpe deg med å utføre alle oppgavene beskrevet her (og mer), mange IDE-er har til og med innebygde git-klienter som kan være veldig nyttige. Under panseret vil alle git-klienter fortsatt utføre de samme git-kommandoene (via en intern kommandolinje).

Min personlige preferanse er [Sublime Merge](https://www.sublimemerge.com/) , mange studenter liker også [Git Kraken](https://www.gitkraken.com/) . Det er viktig å huske at mens klienten vil hjelpe deg med oppgaver, må du fortsatt forstå hvilken git-kommando som må utføres.

## **Jobber med IntelliJ (og andre) og git**

Ethvert prosjekt du lager bør alltid ha følgende to filer:

- `README.md`
- `.gitignore`

### **README.md**

Denne filen er egentlig bare en beskrivelse av prosjektet og dets bidragsytere, men du kan legge til all informasjon du ønsker her.

Denne filen er skrevet i markdown.

### **.gitignore**

Det er noen filer (eller mapper) som du ikke bør sende til eksterne *repoer* , vanligvis er disse filer som kompilert kildekode, biblioteker som vil bli lastet ned av byggesystemet ditt, sensitive filer eller systemfiler.

Git har et innebygd system for å håndtere dette problemet, det kalles "ignored files", som spores i `.gitignore`filen.

> Hensikten her er å holde det eksterne repoet rent og ryddig, og å ikke spore filer som git ikke kan versjonskontrollere.
> 

Du kan manuelt legge til denne filen direkte eller tenkte git-kommandoer, men det enkleste er å generere en omfattende `.gitignore`fil ved å bruke nettverktøyet på [gitignore.io](https://www.toptal.com/developers/gitignore) .

[gitignore.io](https://www.toptal.com/developers/gitignore) krever at du viser operativsystemene, programmeringsspråkene, byggesystemer og IDE-er som vil påvirke prosjektet ditt; den vil da generere innholdet i en `.gitignore`fil som du kan lagre eller manuelt kopiere over.

## **En Git-påminnelse**

Dette er de to reglene som vil hjelpe deg å unngå *de fleste* problemene du vil støte på når du jobber med git:

- Branche tidlig, branche eg ofte
- *pull* før du *pusher*

---

Copyright 2021, Noroff Accelerate AS