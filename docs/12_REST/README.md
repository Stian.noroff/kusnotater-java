# Communication with HTTP

In this lesson you will learn how communication is handled on the web through HTTP requests and responses. You will also learn about various standards that enable this communication.

**Learning objectives:**

You will be able to...

- **contrast** APIs with Web APIs.
- **explain** the REST architecture.
- **define** the typical strucutre of an http request.
- **discuss** HTTP Verbs and common HTTP Status Codes.
- **demonstrate** endpoint naming in accordance with REST best practices.
- **describe** the role Postman plays in API creation.

---

## Hypertext Transfer Protocol (HTTP)

In order to develop Web applications you require a basic working understanding about how the Internet functions. The Internet is simply a massive global network of networks. The Web is the technologies which enables people to leverage the Internet in meaningful ways, i.e. through Web applications/sites using HTML, CSS, Javascript and backend technologies like ASP.NET, Java or Express. Without these Web technologies to provide intricate user interfaces, the Internet is simply a means to send basic data packets between computers around the world. The figure below illustrates the exchange of Web files and data that occurs over the Internet during a typical Website interaction. 

![BasicWebSiteFlow](resources/BasicWebSiteFlow.png)

A user enters a Website address (URL) into the browser, www.website.com. The browser then triggers a request to the server hosted at www.website.com for the content of the websites landing page which is provided in the form of static HTML, CSS and Javascript files, these are downloaded and stored by the browser. The browser then renders the HTML and CSS to present the home page. The JS code will then trigger further requests for more files and data depending on the users interactions and design of the Website. Files and data are transimitted across the Internet numerous times between the browser and the server where the Website is hosted. As Web developers, it is therefore critical to understand how data is transmitted over the Internet. 

Hyper Text Transfer Protocol (HTTP) is a network protocol designed for transfering resources like plain text, HTML and images over the Internet. It is used in a Client - Server relationship to allow resources to be distributed over the Internet in an agreed upon way. HTTP is the "Command Language" which both the sender (Client) and the receiver (Server) must adhere to in order to communicate. The figure below illustrates the basic interaction which typically occurs.

![HTTPOverview](resources/HTTPClientServer.png)

The Client will send an HTTP Request to the Server who will then send an HTTP Response. This is most used by a user-agent, typically a browser (Client) and a Web server (Server) to send and receive data, which facilitates Web sitesas shown in the previous example. 

The figure below illustrates this relationship in the context of Web application development.

![HTTPOverview](resources/WebApplication.png)


The above figure illustrates a more 'traditional' Web application where the entire Web application is in one monolithic solution. The Client requests Web pages which are then received and displayed by the browser. The Backend and Frontend logical operations are all contained within a single Web application.

Modern Web application developement seperates backend and frontend development into seperate applications, to make responsibilities clear and facilitating numerous other development benefits. The end result is a Website/Web application or service which may seem like a single application, but in fact is spread across multple seperately deployed application. This is known as a **Rest Architecture**.

## REST Architecture

REST is an acronym for REpresentational State Transfer. It is an architectural style for distributed hypermedia systems and was first presented by Roy Fielding in 2000. REST has 6 guiding constraints that need to be satisfied for an interface to be considered RESTful. 

1. Client–server – By separating the user interface concerns from the data storage concerns, we improve the portability of the user interface across multiple platforms and improve scalability by simplifying the server components.

2. Stateless – Each request from client to server must contain all of the information necessary to understand the request, and cannot take advantage of any stored context on the server. Session state is therefore kept entirely on the client. 

3. Cacheable – Cache constraints require that the data within a response to a request be implicitly or explicitly labeled as cacheable or non-cacheable. If a response is cacheable, then a client cache is given the right to reuse that response data for later, equivalent requests. 

4. Uniform interface – By applying the software engineering principle of generality to the component interface, the overall system architecture is simplified and the visibility of interactions is improved. In order to obtain a uniform interface, multiple architectural constraints are needed to guide the behavior of components. REST is defined by four interface constraints: identification of resources; manipulation of resources through representations; self-descriptive messages; and, hypermedia as the engine of application state. 

5. Layered system – The layered system style allows an architecture to be composed of hierarchical layers by constraining component behavior such that each component cannot "see" beyond the immediate layer with which they are interacting. 

6. Code on demand (optional) – REST allows client functionality to be extended by downloading and executing code in the form of applets or scripts. This simplifies clients by reducing the number of features required to be pre-implemented. 

The abstraction of information in REST is a _resource_, this can be any information. To identify the particular resource involved in an interaction, REST uses resource identifiers. The state of a resource at any given time is known as resource representation; this representation consists of data, metadata describing the data, and hypermedia links which can help clients transition to the next desired state. The data format of a representation is known as a media type. The media type identifies a specification that defines how a representation is processed. The most common media type seen is **JSON**. 

Using a RESTful convention has many pros and cons but some of the most common reasons for creating and using RESTful services are:

- All responses can be simple HTTP status codes
- Language independent, clients can easily be different languages/technologies to the server
- Data format is flexible (usually JSON, simple to work with)
- Security is handled with layers, not technologies (easier to implement)

Applying the REST architecture to Web Applications results in a seperation of the front end from the backend. Functionaly it operates as a single application, however it is more flexible, scalable and makes development operations cleaner. The figure below illustrates the REST Architecture commonly adopted in modern Web application development. This is an alternative to the monolithic appliction shown earlier in this lesson.

![HTTPOverview](resources/HTTPOverview.png)

> **INFO** *Note:* The details of frontend applications will be covered in the Frontend part of this course. For now we are only interested in the development details of the Backend.

The backend entails managing data and making it available to clients with **Web APIs** and Databases hosted on a server. It will deliver data using JSON and HTTP response codes.

> **INFO** *Note:* You will be building RESTful Web API's as a back end Web developer

## Web APIs

API is the acronym for _Application Programming Interface_. It is a software interface that allows two applications to interact with each other without any user intervention. APIs allows a product or service to communicate with other products and services without having to know how they're implemented. 

This has been the primary way our interaction with libraries and dependencies have been. Recall the design lessons where we have been learning how to make our applications cleaner by relying on creating good interfaces for interaction.

A _Web API_ is simply an API accessable over a network. Web APIs are commonly known as _Web services_, this is done to create a clear distinction between network and non-network APIs. 

> **INFO** *Note:* All Web Services are APIs, but not all APIs are Web services. We can interchange the terms _Web API_ and _Web Service_ for our uses.

Web API implements protocol specification and thus it incorporates concepts like _caching_, _URIs_, _versioning_, _request/response headers_, and various _content formats_ in it.

Web APIs return information in the form of web documents, the most common type are returned in the [JSON](https://www.json.org/json-en.html) format. JSON stands for _JavaScript Object Notation_ and is a standardized way of communicating in a very readable, easily parsable, and easy to generate way.

> **INFO** *Note:* You will learn how to build a Web API in the next lesson.

To recap as per the above figure, in modern Web development the backend component of a Website/Application is a Web API which is soley responsible for managing data for the Website/Application. The Frontend ClientApp is static HTML, Javascript and CSS files are downloaded by a Browser from the frontend web server, which will then request data and operations from the Web API over HTTP to populate the Website with relevant data based on user interaction. The next step is to investigate how HTTP requests to a Web Api work. 

## HTTP Requests

A Restful HTTP request from the client to the server usually consists of the following components:

- **URL Path** [https://api.example.com/user]
- **HTTP Method** [GET, PUT, POST, PATCH, DELETE]
- **Header** – (optional) additional information that the client needs to pass along in the request such as Authorization credentials, Content-Type of the body, User-Agent to define what type of application is making the request, and more]
- **Parameters** – (optional) variable fields that alter how the resource will be returned.
- **Body** – (optional) contains data that needs to be sent to the server.

The figure below illustrates an example of an HTTP request to the example api for some data

![HTTPRequestExample](resources/HTTPRequestDiagram.png)

> __NOTE__: HTTP Responses follow the same pattern but contains a HTTP status code instead of an HTTP Method

## Http Methods/Verbs

HTTP defines a set of request methods to indicate the desired action to be performed for a given resource. Although they can also be nouns, these request methods are sometimes referred to as HTTP verbs. 

Each of them implements a different semantic, but some common features are shared by a group of them: e.g. a request method can be safe, idempotent, or cacheable.

> __NOTE__: Idempotency is a concept used to ensure robustness of APIs, it can get fairly complicated. For now all that needs to be known is that an idempotent request will ALWAYS produce the same effect no matter how many times it is called.

There are many verbs, we will focus on a few important ones: GET, POST, PUT, PATCH, and DELETE.

**GET**

The [HTTP GET](https://tools.ietf.org/html/rfc7231#section-4.3.1) method requests a representation of the specified resource. Requests using GET should only be used to request data - meaning they shouldn't include data. 

> *HTTP GET is idempotent. The same request returns the same data.*

The included data here refers to data in the request body.

**POST**

The [HTTP POST](https://tools.ietf.org/html/rfc7231#section-4.3.3) method sends data to the server. The type of the body of the request is indicated by the _Content-Type_ header. A POST request is typically sent via an HTML form and results in a change on the server.

> HTTP POST is not idempotent. Multiple POSTS result in new resources being created each time.

Content-Type is typically application/json and the JSON data is passed in the request body.

**PUT**

The [HTTP PUT](https://tools.ietf.org/html/rfc7231#section-4.3.4) request method creates a new resource or replaces a representation of the target resource with the request payload.

> HTTP PUT is idempotent. Multiple requests result in nothing new as the first updates the resource.

**PATCH**

The [HTTP PATCH](https://tools.ietf.org/html/rfc5789) request method applies partial modifications to a resource.

PATCH is similar the "update" concept found in CRUD. A PATCH request is considered a set of instructions on how to modify a resource. Contrast this with PUT; which is a complete representation of a resource. Both PUT and PATCH can be used to update resources.

> __NOTE__: HTTP PATCH is typically idempotent, however scenarios could arise in which it is not.

**DELETE**

The [HTTP DELETE](https://tools.ietf.org/html/rfc7231#section-4.3.5) request method deletes the specified resource.

> HTTP DELETE is idempotent. Once a resource is gone, its gone.

## Http Status Codes

There are many status codes that responses from servers can provide. Each of them mean something specific. Luckily they are grouped into "bands" of 100 where related codes are numbered. HTTP response status codes indicate whether a specific HTTP request has been successfully completed. 

Responses are grouped in five classes:

- Informational responses (100–199)
- Successful responses (200–299)
- Redirects (300–399)
- Client errors (400–499)
- Server errors (500–599)

There are many included in these classes, we will just focus on a few important ones that you will encounter. The figure below illustrates the response received from the previous HTTP Request example. 

![](resources/HTTPResponseDiag.png)

**200 OK**

The [HTTP 200 OK](https://tools.ietf.org/html/rfc7231#section-6.3.1) success status response code indicates that the request has succeeded. A 200 response is cacheable by default.

The meaning of a success depends on the HTTP request method:

- GET: The resource has been fetched and is transmitted in the message body.
- POST: The resource describing the result of the action is transmitted in the message body

The successful result of a PUT or a DELETE is often not a _200 OK_ but a _204 No Content_ (or a : when the resource is uploaded for the first time).

**201 Created**

The [HTTP 201 Created](https://tools.ietf.org/html/rfc7231#section-6.3.2) success status response code indicates that the request has succeeded and has led to the creation of a resource. The new resource is effectively created before this response is sent back and the new resource is returned in the body of the message, its location being either the URL of the request, or the content of the Location header.

The common use case of this status code is as the result of a POST request.

**204 No Content**

The [HTTP 204 No Content](https://tools.ietf.org/html/rfc7231#section-6.3.5) success status response code indicates that a request has succeeded, but that the client doesn't need to navigate away from its current page.

This might be used, for example, when implementing "save and continue editing" functionality for a wiki site. In this case an PUT request would be used to save the page, and the 204 No Content response would be sent to indicate that the editor should not be replaced by some other page.

> Recall from _200 Ok_, _204 No Content_ is often the response for PUT and DELETE.

**400 Bad Request**

The [HTTP 400 Bad Request](https://tools.ietf.org/html/rfc7231#section-6.5.1) response status code indicates that the server cannot or will not process the request due to something that is perceived to be a client error (e.g., malformed request syntax, invalid request message framing, or deceptive request routing).

> The client should not repeat this request without modification.

**401 Unauthorized**

The [HTTP 401 Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1) client error status response code indicates that the request has not been applied because it lacks valid authentication credentials for the target resource.

**403 Forbidden**

The [HTTP 403 Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3) client error status response code indicates that the server understood the request but refuses to authorize it.

This status is similar to 401, but in this case, re-authenticating will make no difference. The access is permanently forbidden and tied to the application logic, such as insufficient rights to a resource.

> Authentication and Authorization are concepts we discuss in a later lesson.

**404 Not Found**

The [HTTP 404 Not Found](https://tools.ietf.org/html/rfc7231#section-6.5.4) client error response code indicates that the server can't find the requested resource. Links that lead to a 404 page are often called broken or dead links and can be subject to [link rot](https://en.wikipedia.org/wiki/Link_rot).

A 404 status code does not indicate whether the resource is temporarily or permanently missing. But if a resource is permanently removed, a _410 Gone_ should be used instead of a 404 status.

**405 Method Not Allowed**

The [HTTP 405 Not Allowed](https://tools.ietf.org/html/rfc7231#section-6.5.5) client error response code indicates that the request method is known by the server but is not supported by the target resource.

> For example, an API may forbid DELETE-ing a resource.

**500 Internal Server Error**

The [HTTP 500 Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1) server error response code indicates that the server encountered an unexpected condition that prevented it from fulfilling the request.

This error response is a generic "catch-all" response. Usually, this indicates the server cannot find a better 5xx error code to response. Sometimes, server administrators log error responses like the 500 status code with more details about the request to prevent the error from happening again in the future.

## Postman

Testing Web APIs is a crucial step in backend development as this is an indication of what is working and what needs fixing. There are many tools available to configure and send HTTP requests so that APIs can be tested, a very common one is **Postman**. Postman is a well-supported robust API testing tool designed to help develop APIs faster by allowing easy testing and even automated testing. Using this type of tool allows Web API's to be tested without the need for a frontend application which would be making these requests.

There are many features available to developers through postman, this section will detail the most common features used. Postman allows the **request method** to be chosen in an easy manner, the figure below illustrates this.

![Postman request types](resources/0302_fig26_postman_requests.png)

Headers can be added to a request by selecting the **Headers** tab, the figure below illustrates this. 

![Postman request headers](resources/0302_fig27_postman_headers.png)

Information can be added to the body in the **Body** tab, here extra formatting options are presented. For JSON data, you select **raw**. The figure below illustrates this.

![Postman request body](resources/0302_fig28_postman_body.png)

The figure below shows what the example api call would look like if made from postman.

![](resources/HTTPGetwithnewendpoint.png)

![Postman request Get Example](resources/HTTPExampleRequest.png)

![](resources/POSTRequestexample2.png)

![Postman Post example](resources/POSTExample.png)

> **ACTIVITY** *Try it out!* There are many [free publicly hosted Web Api's](https://mixedanalytics.com/blog/list-actually-free-open-no-auth-needed-apis/). Use Postman to send a GET request for a programming joke to the [JokesAPI](https://sv443.net/jokeapi/v2/). You can use the URL https://v2.jokeapi.dev/joke/Programming. You don't need add any headers to this request. You should get a joke in JSON format back in the HTTP Response. 

---

You can easily set **environment variables** by following below steps:

- Click on **Manage Environment** from Settings (icon available at top right corner).

- Click on **ADD** button.

- Write down the **Name** of the **Environment**. 

- Fill key and value, which can be used as variable in collection later.

The figure below illustrates this.

![Postman environment variables](resources/0302_fig29_postman_environment.png)

You can bundle APIs calls together into a **collection** and export them to share with other developers. The figure below illustrates the creation of a collection.  

![Postman collections](resources/0302_fig30_postman_collection_creation.png)

The figure below illustrates how to **export** collections.

![Postman export collection](resources/0302_fig31_postman_collection_export.png)

The figure below illustrates how to **import** collections.

![Postman import collection](resources/0302_fig32_postman_collection_import.png)

> __NOTE__: You will work more closely with configuring and sending HTTP Requests in the Frontend portion of this course.

Writing API tests in Postman is straight forward. You can write and run tests for each request using JavaScript.  You navigate to the **Test** tab. The figure below illustrates the description of a test.

![Postman test creation](resources/0302_fig33_test_name.png)

The figure below gives an example of a **test script**. 

![Postman test script](resources/0302_fig34_postman_test_script.png)

The figure below is an example of a **test result**. 

![Postman test result](resources/0302_fig35_postman_test_result.png)

The above example was a simple test to see if a response code received is 200. You can have as many tests as you want for a request. Most tests are as simple and one liner JavaScript statements. The snippet below illustrates some extra tests that could be written for this request. 

![Postman example tests](resources/0302_fig36_postman_test_example.png)

## REST Naming Conventions

As mentioned earlier, in REST, primary data representation is called Resource. Having a strong and consistent REST resource naming strategy is good design. REST APIs use `Uniform Resource Identifiers` (URIs) to address resources. REST API designers should create URIs that convey a REST API’s resource model to its potential client developers. They do this by creating URI templates that allow the clients to construct URLs from using resources. URI templates are simply URLs with variables, called expressions and wrapped in curly braces. When resources are named well, an API is intuitive and easy to use. If done poorly, that same API can feel difficult to use and understand. 

A `resource` can be a `singleton` or a `collection`. For example, `customers` is a collection resource and `customer` is a singleton resource (in a banking domain). We can identify `customers` collection resource using the URI `/customers`. We can identify a single `customer` resource using the URI `/customers/{customerId}`. 

A resource may also contain sub-collection resources. For example, sub-collection resource `accounts` of a particular `customer` can be identified using the URI `/customers/{customerId}/accounts` (in a banking domain). Similarly, a singleton resource `account` inside the sub-collection resource `accounts` can be identified as follows: `/customers/{customerId}/accounts/{accountId}`.  

This section contains some best practices involved when deciding on endpoint names and the overall structure of your API. 

### Use nouns to represent resources 

RESTful URI should refer to a resource that is a _thing_ (noun) instead of referring to an _action_ (verb) because nouns have properties which verbs do not have. Some examples of a resource are: 

- Users of the system 
- User Accounts 
- Network Devices etc. 

The snippet below gives some example URIs for the above nouns. 

```
http://api.example.com/device-management/managed-devices
http://api.example.com/device-management/managed-devices/{device-id}
http://api.example.com/user-management/users
http://api.example.com/user-management/users/{id}
```

You can divide the resource archetypes into four categories (`document`, `collection`, `store` and `controller`) and then you should always target to put a resource into one archetype and then use it’s naming convention consistently. For uniformity’s sake, resist the temptation to design resources that are hybrids of more than one archetype. Take note that this is just an example of how URIs can be constructed based on noun names, it is not the required format. The point is to pick a consistent way of representing different areas of your application.  

**1. Document**

A document resource is a singular concept that is like an object instance or database record. In REST, you can view it as a single resource inside resource collection. A document's state representation typically includes both fields with values and links to other related resources. You use a `singular` name to denote document resource archetype. The snippet below provides some examples. 

```
http://api.example.com/device-management/managed-devices/{device-id}
http://api.example.com/user-management/users/{id}
```

**2. Collection**
A collection resource is a server-managed directory of resources. Clients may propose new resources to be added to a collection. However, it is up to the collection to choose to create a new resource or not. A collection resource chooses what it wants to contain and decides the URIs of each contained resource. Use `plural` names to denote collection resource archetype. The snippet below provides some examples. 

```
http://api.example.com/device-management/managed-devices
http://api.example.com/user-management/users
http://api.example.com/user-management/users/{id}/accounts
```

**3. Store**
A store is a client-managed resource repository. A store resource lets an API client put resources in, get them back out, and decide when to delete them. A store never generates new URIs. Instead, each stored resource has a URI that was chosen by a client when it was initially put into the store. Use `plural` name to denote store resource archetype. The snippet below provides some examples. 

```
http://api.example.com/cart-management/users/{id}/carts
http://api.example.com/song-management/users/{id}/playlists
```

**4. Controller**
A controller resource models a procedural concept. Controller resources are like executable functions, with parameters, return values, inputs and outputs. Use `verb` to denote controller archetype. The snippet below provides some examples. 

```
http://api.example.com/cart-management/users/{id}/carts/checkout
http://api.example.com/song-management/users/{id}/playlists/play
```

### Consistency is key

Use consistent resource naming conventions and URI formatting for minimum ambiguity and maximum readability and maintainability. Some good rules for consistency are in the following subsections. 

**1. Use forward slash (/) to indicate hierarchical relationships**
The forward slash (`/`) character is used in the path portion of the URI to indicate a hierarchical relationship between resources. 

```
http://api.example.com/device-management
http://api.example.com/device-management/managed-devices
http://api.example.com/device-management/managed-devices/{id}
http://api.example.com/device-management/managed-devices/{id}/scripts
http://api.example.com/device-management/managed-devices/{id}/scripts/{id}
```

**2. Do not use trailing forward slash (/) in URIs**
As the last character within a URI’s path, a forward slash (`/`) adds no semantic value and may cause confusion. It’s better to drop them completely. 

Bad example:
```
http://api.example.com/device-management/managed-devices/
```

> 

**3. Use hyphens (-) to improve the readability of URIs**
To make your URIs easy for people to scan and interpret, use the hyphen (`-`) character to improve the readability of names in long path segments. 

Bad example:
```
http://api.example.com/managedEntities/{id}/installScriptLocation
```

Readable example:
```
http://api.example.com/managed-entities/{id}/install-script-location
```

**4. Do not use underscores (_)**
It's possible to use an underscore in place of a hyphen to be used as separator. Depending on the application's font, it's possible that the underscore character can either get partially obscured or completely hidden in some browsers or screens. To avoid this confusion, use hyphens (`-`) instead of underscores (`_`). 

Bad example:
```
http://api.example.com/managed_entities/{id}/install_script_location
```

**5. Use lowercase letters in URIs**
When convenient, lowercase letters should be consistently preferred in URI paths. RFC 3986 defines URIs as case-sensitive except for the scheme and host components. 

**6. Do not use file extensions**
File extensions look bad and do not add any advantage. Removing them decreases the length of URIs as well. 

Apart from above reason, if you want to highlight the media type of API using file extension then you should rely on the media type, as communicated through the `Content-Type` header, to determine how to process the body’s content. 

### Never use CRUD function names in URIs 

URIs should not be used to indicate that a CRUD function is performed. URIs should be used to uniquely identify resources and not any action upon them. HTTP request methods should be used to indicate which CRUD function is performed. 

Bad example:
```
http://api.example.com/device-management/get-managed-devices/
```

### Use query component to filter URI collection 

Many times, you will come across requirements where you will need a collection of resources sorted, filtered or limited based on some certain resource attribute. For this, do not create new APIs – rather enable `sorting`, `filtering` and `pagination` capabilities in resource collection API and pass the input parameters as query parameters. 

```
.../device-management/managed-devices
.../device-management/managed-devices?region=USA
.../device-management/managed-devices?region=USA&brand=XYZ
.../device-management/managed-devices?region=USA&brand=XYZ&sort=installation-date
```

### Representing related resources

When a resource is requested form an API, often there is an associated resource in the database. An example of this is `books` and `authors`; book has authors. When a book resource is requested, the associated authors should also be returned.  The most straight forward way of returning the authors is to have an array of the author `id`’s, as shown in the code snippet below. 

```json
{
  "bookid": 1,
  "booktitle": "Example book",
  "authors": [
    {
      "authorid": 1,
      ...
    },
    {
      "authorid": 2,
      ...
    }
  ]
}
```` 

This seems okay, but it has several drawbacks. If the client wanted to get the information of these authors, they would have to construct the URLs themselves by looking at the documentation. This is not best practices as APIs should be designed with the client having no prior knowledge beyond the initial URI (bookmark). This is solved by returning links to the resources in addition to an identifier. In this case, an `authorLink` is added. 

```json
{
  "bookid": 1,
  "booktitle": "Example book",
  "authors": [
    {
      "authorid": 1,
      "authorLink": "/authors/1",
    },
    {
      "authorid": 2,
      "authorLink": "/authors/2",
    }
  ]
}
```` 

This way the client does not have to look through the documentation to see how to navigate to related authors, and they know that authors and books are related. Including a `self` property makes it explicit what web resource’s properties we are talking about without requiring contextual knowledge—like what URL did you perform a GET on to retrieve this representation—or application-specific knowledge. This is especially important for nested JSON objects where the outer context doesn’t help establish what resource we’re talking about, but it is good practice everywhere. The code snippet below shows the same book response with this new field in mind. 

```json
{
  "self": "/books/1",
  "bookid": 1,
  "booktitle": "Example book",
  "authors": [
    {
      "authorid": 1,
      "self": "/authors/1",
    },
    {
      "authorid": 2,
      "self": "/authors/2",
    }
  ]
}
```

There are several other specifications to choose from that define more complex approaches to link representation, like `Siren`, `HAL`, `JSON-LD`, `RDF/JSON`, `Collection+JSON`, `Hydra`, and probably several more. Each of these adds significant features and concepts beyond just how to represent links. The purpose of this section was to illustrate the use of links to help clients when using the API. The key point is to not force clients to go through documentation to identify how to get the related resources and to explicitly show that there is a relationship between entities. This takes much of the guess work out for the clients. For example, Microsoft use [HATEOAS-style](https://docs.microsoft.com/en-us/azure/architecture/best-practices/api-implementation#provide-links-to-support-hateoas-style-navigation-and-discovery-of-resources) navigation and discovery of resources for API design; this method provides a links object where you describe all related resources and provide URLs and operation types. 

### Filtering, Sorting, and Pagination 

**Filtering**

The ability to filter data is an important feature that all REST APIs should provide. Basic filtering allows selecting resources by matching one or more members of the resource to values passed as query parameters. For example: 
```
GET /books?genre=horror 
```
Will return all books that have the genre of horror. More complex implementations may add support for basic operators (e.g. `<`,`>`) and complex boolean operators (e.g. `and` & `or`) that can group multiple filters together.  
```
GET /books?genre=horror&language=English&price<1000 
```
The implementation of the search is done on the server side; therefore, the way searching is handled is up to the developer. The simple query method shown here is straight forward from the client's perspective but could be complex from a development point of view; it is still the most common method seen. 

**Sorting** 

Sorting is normally determined through the use of the `sort` query string parameter. The value of this parameter is a comma-separated list of sort `keys`. Sort `directions` can optionally be appended to each sort key, separated by the `:` character. Sort directions are typically either `asc` for ascending or `desc` for descending. 
```
GET /books?genre=horror&sort=price:asc 
```
This will get all books with the horror genre and sort then by price ascending. Once again, there are many ways to handle the sorting process and it is done server side; this is just the most common solution. 

**Pagination** 

API queries to dense databases could potentially return millions, if not billions, of results. This results in a very heavy overhead and load on the server and creates delays for the client. Pagination helps to limit the number of results to help keep network traffic in check. When returning results that are paginated, it is best practice to include some additional fields. 

- first 
- prev 
- self 
- next 
- last 

These fields will have URLs generated to navigate to; the first page, the previous page, this page, the next page, and the last page.

**Offset pagination**
This is the simplest form of paging. `Limit/Offset` became popular with apps using `SQL databases` which already have `LIMIT` and `OFFSET` as part of the `SQL SELECT` syntax. Very little business logic is required to implement Limit/Offset paging.  

An example of Limit/Offset Paging is:  
```
GET /items?limit=20&offset=100  
```
This query would return the 20 rows starting with the 100th row. The downside of offset pagination is that it stumbles when dealing with large offset values and adding new entries to the table can cause confusion, which is known as page drift. 

**Keyset pagination**

Keyset pagination uses the filter values of the previous page to determine the next set of items. Those results will then be indexed. 

Consider this example: 

- Client requests most recent items `GET /items?limit=20` 

- When clicking the next page, the query finds the minimum created date of `2019–01–20T00:00:00`. This is then used to create a query filter for the next page. `GET /items?limit=20&created:lte:2019-01-20T00:00:00`. 

And so on… 

The benefits of this approach is that it doesn’t require additional backend logic. It only requires one limit URL parameter. It also features consistent ordering, even when new items are added to the database. It also works smoothly with large offset values. 

**Seek pagination**

Seek pagination is the next step beyond keyset pagination. Adding the queries `after_id` and `before_id`, you can remove the constraints of filters and sorting. Unique identifiers are more stable and static than lower cardinality fields such as state enums or category names. The only downside to seek pagination is it can be challenging to create custom sort orders. 

Consider this example: 

- Client requests a list of the most recent items `GET items?limit=20`. 

- Client requests a list of the next 20 items, using the results of the first query `GET /items?limit=20&after_id=20`. 

- Client requests the next/scroll page, using the final entry from the second page as the starting point `GET /items?limit=20&after_id=40`. 

Benefits of Seek Pagination: 

- Uncouples filter logic from pagination logic 

- Consistent ordering, even when new items are added to the database. Sorts newest added items well. 

- Works smoothly, even with large offsets. 

Disadvantages of Seek Pagination: 

- More demanding of backend resources using offset or keyset pagination 

- When items are deleted from the database, `start_id` may no longer be a valid id. 

### Versioning 

An API is very rarely completely unchanged after release, a redesign is often done at some point. To prevent breaking all the clients using the API, versioning can be used. This allows the legacy system to be operational in parallel with the new one and give clients time to switch over or accept the use of a deprecated resource. There are three main ways to version your API: 

- URL, also known as route versioning (e.g. `/v1/:foo/:bar/:baz`). 
- Versioning via a custom header (e.g. `Accept-Version: v2`). 
- Query string parameter (e.g. `/:foo/:bar/:baz?version=v2`). 

Of the three mentioned, route versioning is the most common. It is very simple to achieve because it is simply a prefix of `vx` to the endpoints. Many frameworks support route-defined versioning, and when it doesn’t, you can simply structure your API so that it allows for route versioning. Because of this, it’s extremely easy to implement; however, it does add length to your REST API route, which is the downside. 

Custom headers may seem very straight forward and easy, but not all clients want to supply a custom header because they simply want to extract some information. Typically, you don’t want to force the client into doing something they may not want to. Additionally, it requires overhead on the server to parse prior to the response, which could add latency. 

The final option is the "bad practice" option, where versioning is an afterthought for the designers. This method should be avoided at all costs, but if there is no other option; it can be done.

---

Copyright 2022, Noroff Accelerate AS
