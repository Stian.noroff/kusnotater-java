# Prominente Java-funksjoner

# Prominente Java-funksjoner

I denne leksjonen vil du lære om prominente funksjoner som finnes i Java for å lage flere funksjonsrike programmer.

**Læringsmål:**

Du vil lære å…

- **beskrive** filsysteminteraksjon i Java.
- **demonstrere** bruken av banevariabler for å finne filer i en Java-applikasjon.
- **administrere** filer i en Java-applikasjon, dette inkluderer **opprettelse**, **modifikasjon**,  og **undersøkelse**
- **forsvar** Java-applikasjonen din mot filunntak ved å konstruere riktig feilhåndtering, der unntak **forutses** og **administreres**.
- **diskutere** hvordan minne administreres i Java.
- **demonstrere** forskjellen mellom verdi- og referansetyper.
- **diskutere** og **demonstrere** Lambda-funksjoner.
- **beskrive** Stream API-biblioteket.
- **demonstrere** Stream API-operasjoner.

---

## Grunnleggende I/O

Java I/O (Input og Output) brukes til å behandle input og produsere utdata. Java bruker konseptet med en stream for I/O-operasjoner. Pakken `java.io` inneholder alle klassene som kreves for input- og output-operasjoner. Vi kan utføre filhåndtering via **Java I/O API**.

En stream er en sekvens av data. I Java er en streamm sammensatt av byte. Tre streams opprettes automatisk for oss. Alle disse streamene er knyttet til konsollen.

1. `System.out`: standard output-stream
2. `System.in`: standard input-stream
3. `System.err`: standard error stream

> **MERK**: Vi har brukt standard output stream for å printe meldinger til konsollen
> 

### Input/Output streams

En Java-applikasjon bruker en **input-stream** til å lese data fra en kilde; det kan være en fil, en matrise, en periferisk enhet eller en socket.

En Java-applikasjon bruker en **output-stream** til å skrive data til en destinasjon; det kan være en fil, en matrise, en periferisk enhet eller en socket.

Avhengig av dataene en stream har, kan den klassifiseres som en:

- Bytestream
- Tegnstream

**Bytestrøm** brukes til å lese og skrive en enkelt byte (8 bits) med data. Alle bytestrømklasser er avledet fra de abstrakte basisklassene `InputStream` og`OutputStream`.

**Tegnstrøm** brukes til å lese og skrive et enkelt tegn med data. Alle tegnstrømklassene er avledet fra abstrakte basisklasser `Reader` og `Writer`.

Illustrasjonen nedenfor viser dette hierarkiet:

![Untitled](Prominente%20Java-funksjoner%20cdddcd5a6ab446dcb519e3a6ca6ff463/Untitled.png)

> **MERK**: There are more classes lower in the hierarchy, this is a simplified view of the abstractions.
> 

Den vanligste bruken er å lese og skrive data til filer. Resten av denne seksjonen beskriver hvordan dette kan utføres

### Filklasse

`File`-klassen og `java.io`-pakken brukes til å utføre ulike operasjoner på filer og kataloger.

En **fil** er en navngitt plassering som kan brukes til å lagre relatert informasjon. En **mappe** er en samling av filer og undermapper. En katalog inne i en katalog er kjent som **undermapper**.

For å lage et `File`-objekt må vi importere `java.io.File`-pakken. Kodeutdraget nedenfor illustrerer hvordan du oppretter en fil:

```java
import java.io.File;
...
// Path to file
String pathToFile = "foobar.txt";
// Creates an object of File using the path
File file = new File(pathToFile);
```

> **MERK**: Dette oppretter ikke filen på disken. Det er en abstraksjon i applikasjonen.
> 

Fra dette punktet har vi forskjellige metoder til rådighet for å manipulere dette filobjektet. Den vanligste er å skape filen. Kodeutdraget nedenfor illustrerer opprettelsen av en fil og kontrollerer dens tillatelser.

```java
// Create the file
try {
    // If the file already exists, returns false
    boolean created = file.createNewFile();
    // Checking permissions
    boolean canWrite = file.canWrite();
    boolean canRead = file.canRead();
} catch (IOException e) {
    // Have to cater for IOExceptions
    e.printStackTrace();
}
```

### Skriving

Å skrive data til filer gjøres ved hjelp av `FileWriter`-klassen. Det strekker seg `OutputStreamWriter`, som strekker seg `Writer`. En visuell representasjon kan sees nedenfor:

![Untitled](Prominente%20Java-funksjoner%20cdddcd5a6ab446dcb519e3a6ca6ff463/Untitled%201.png)

Å lage en ny `FileWriter` gjøres ved å sende stien til der filen er, enten gjennom et `String` eller ved å bruke et `File`-objekt. Kodeutdraget nedenfor illustrerer de forskjellige overbelastede konstruktørene:

```java
import java.io.FileWriter;
// Using the name of the file in a path
FileWriter output = new FileWriter(String name);
// Using an object of the file
FileWriter output = new FileWriter(File fileObj);
// Optional: Changing the default encoding
FileWriter output = new FileWriter(String file, Charset cs);
// Optional: Append the data if the file already exists
FileWriter output = new FileWriter(String file, boolean append);
```

> **MERK**: Standardkodingen for a `FileWriter` er **UTF8**. `Charset.forName()`-metoden brukes til å spesifisere typen tegnkoding. De ulike typene finner du [her](https://docs.oracle.com/javase/7/docs/api/java/nio/charset/Charset.html) .
> 

Når vi har en instans av en `FileWriter`, kan vi enkelt bruke de forskjellige metodene som tilbys:

- `write(char ch)` - skriver et enkelt tegn
- `write(char[] array)` - skriver tegnene fra den angitte *array*
- `write(String data)` - skriver den angitte strengen
- `flush()` - skriver alle dataene som finnes til den tilsvarende destinasjonen
- `append()` - setter inn det angitte tegnet
- `close()` - brukes til å lukke *writer.* Når dette først er påkalt kan vi ikke bruke den  igjen. `close()` utfører først `flush()`.

> **MERK**: Fil IO er svært utsatt for feil, ekaster en `IOException`; dekkes med en *try-catch.*
> 

Kodeutdraget nedenfor viser hvordan `FileWriter` kan brukes:

```java
import java.io.FileWriter;
...
String pathToFile = "output.txt";
String dataToWrite = "Hello World";
// Create new FileWriter resource
try (FileWriter output = new FileWriter(pathToFile)) {
    // Write data
    output.write(dataToWrite);
} catch (IOException e) {
    e.printStackTrace();
}
// OUTPUT: output.txt containing - Hello World
```

Utdraget ovenfor vil opprette en fil som kalles `output.txt` i roten av applikasjonen vår (ved siden av **src** ). Hvis vi ønsket å spesifisere en målmappe, ville vi brukt en relativ sti. Dette eksemplet overskriver også alle data inne i filen.

Eksemplet nedenfor gjengir eksemplet ovenfor, men overskriver ikke eksisterende data, og oppretter en fil i en **ressursmappe** .

```java
// Note the relative path
String pathToFile = "resources/output.txt";
String dataToWrite = "Hello World";
// Create new FileWriter resource
try (FileWriter output = new FileWriter(pathToFile, true)) {
    // Write data
    output.write(dataToWrite);
} catch (IOException e) {
    e.printStackTrace();
}
```

> **MERK**: Ressursmappen må eksistere først. `FileWriter` kan ikke lage en katalog.
> 

Legg merke til at *try-catch-*setningen som brukes ser annerledes ut enn du er vant til. *try-with*-ressurser syntaktisk sukker eksisterer for å forenkle tilgang til eksterne ressurser. Den lukker automatisk alle ressurser når du forlater kodeblokken, og reduserer mengden *boilerplate*-kode som må skrives.

Kodeutdraget nedenfor viser samme eksempel som før, men ved å bruke den tradisjonelle *try-catch-finally-*metoden:

```java
String pathToFile = "resources/output.txt";
String dataToWrite = "Hello World";
// Need to declare it here to use in finally
FileWriter output = null;
try {
    // Create instance of FileWriter
    output = new FileWriter(pathToFile, true);
    // Write data
    output.write(dataToWrite);
} catch (IOException e) {
    e.printStackTrace();
} finally {
    // Close the FileWriter if it exists
    if(output != null) {
        output.close();
    }
}
```

En ulempe med å bruke `FileWriter` er at den skriver et enkelt tegn om gangen. Det økte antallet IO-operasjoner kan påvirke ytelsen til applikasjonen negativt. `BufferedWriter` forbedrer ytelsen når den brukes sammen med en `FileWriter`.

`BufferedWriter` opprettholder en intern buffer på 8192 tegn . Under skriveoperasjonen skrives tegnene til den interne bufferen i stedet for disken. Tegnene i bufferen skrives kun til disken når selve bufferen er full eller writer er lukket. 

> **MERK**: Antall operasjoner på disken reduseres, noe som øker ytelsen.
> 

For å bruke en `BufferedWriter`, må vi importere `java.io.BufferedWriter`-pakken. Vi sender deretter vår `FileWriter` til `BufferedWriter` i sin konstruktør. Vi kan også tilpasse bufferstørrelsen med en valgfri parameter.

Kodeutdraget nedenfor illustrerer bruken av `BufferedWriter` med `FileWriter` for en mer effektiv operasjon:

```java
import java.io.BufferedWriter;
import java.io.FileWriter;
...
String pathToFile = "resources/output.txt";
String dataToWrite = "Hello World";
try (FileWriter input = new FileWriter(pathToFile);
BufferedWriter output = new BufferedWriter(input)) {
    // Write data
    output.write(dataToWrite);
} catch (IOException e) {
    e.printStackTrace();
}
```

> **MERK:** For å skrive flere linjer i deklarasjonen til ressursen kan de skilles med `;` . `BufferedWriter output = new BufferedWriter(new FileWriter(pathToFile)))` kan også skrives, hvis én linje er ønsket.
> 

`BufferedWriter` har lignende metoder til `FileWriter`, som `write()`, `flush()`, `close()`, og `append()`fordi den forlenger `Writer`. `BufferedReader` har en ekstra nyttig metode kalt `newLine()` som skriver en linjeskilletegn for deg.

### Lesing

Lesing av data fra en fil kan gjøres på ulike måter, avhengig av intensjonen. Data kan leses per byte, tegn, linje eller hele filen.

### FileReader

The `FileReader` class of the `[java.io](http://java.io)` package can be used to read data (in characters) from files. It extends the `InputStreamReader` class. Klassen `FileReader` til `java.io` pakken kan brukes til å lese data (i tegn) fra filer. Det utvider `InputStreamReader`-klassen.

En visuell representasjon kan sees nedenfor:

![Untitled](Prominente%20Java-funksjoner%20cdddcd5a6ab446dcb519e3a6ca6ff463/Untitled%202.png)

Opprettelsen av `FileReader` er identisk med `FileWriter`. `FileReader` har følgende metoder:

- `read()` - leser et enkelt tegn
- `read(char[] array)` - leser tegnene og lagrer i den angitte *array*
- `close()` - lukker leseren
- `skip(long n)` – hopper over tegn

Kodeutdraget nedenfor leser hvert tegn fra en fil:

```java
String pathToFile = "resources/output.txt";
// Data structure to hold our characters
char[] characters = new char[3];
try(FileReader reader = new FileReader(pathToFile)) {
    // Reads  characters into our array until the array is full
    reader.read(characters);
    // Prints out each character
    for (char ch:characters) {
        System.out.print(ch);
    } // OUTPUT: He
    // Need to skip from the start of the reader
    reader.skip(4);
    // Read new characters into buffer
    reader.read(characters);
    // Prints out each character
    for (char ch:characters) {
        System.out.print(ch);
    } // OUTPUT: Wo
} catch(IOException e) {
    e.printStackTrace();
}
```

Kodeutdraget nedenfor printer de to første tegnene fra filen vår, flytter to elementer fremover og skriver deretter ut de to neste:

```java
String pathToFile = "resources/output.txt";
// Data structure to hold our characters
char[] characters = new char[3];
try(FileReader reader = new FileReader(pathToFile)) {
    // Reads  characters into our array until the array is full
    reader.read(characters);
    // Prints out each character
    for (char ch:characters) {
        System.out.print(ch);
    } // OUTPUT: He
    // Need to skip from the start of the reader
    reader.skip(4);
    // Read new characters into buffer
    reader.read(characters);
    // Prints out each character
    for (char ch:characters) {
        System.out.print(ch);
    } // OUTPUT: Wo
} catch(IOException e) {
    e.printStackTrace();
}
```

> NOTE: `skip()` hopper fra starten av filen, ikke der vi stoppet å lese
> 

### BufferedReader

Mye på samme måte som `BufferedWriter` med `FileWriter`, kan `BufferedReader` brukes med `FileReader` for å forbedre ytelsen. `BufferedReader` er konstruert på samme måte som `BufferedWriter` og har lignende metoder som `FileReader` fordi den utvider `Reader`-abstraktklassen.

Kodeutdraget nedenfor illustrerer en nyttig ekstra metode `BufferedReader` har for å lese en hel linje inn om gangen:

```java
String pathToFile = "resources/output.txt";
// Declare reader resources
try(FileReader input = new FileReader(pathToFile);
    BufferedReader reader = new BufferedReader(input)) {
    String line;
    // readLine will be null when there are no more lines
    while ((line = reader.readLine()) != null) {
        System.out.println(line); // OUTPUT: Hello World
    }
} catch (IOException e) {
    e.printStackTrace();
}
```

> **MERK**: `BufferedReader` kan brukes på samme måte som `FileReader`.
> 

`BufferedReader` godtar alle `Reader` som et konstruktørargument. Dette betyr at du kan sende `System.in` via en `InputStreamReader` og lese bruker-input via konsollen. Kodeutdraget nedenfor illustrerer dette:

```java
// System.in is the standard input stream
try (InputStreamReader console =new InputStreamReader(System.in);
    BufferedReader reader =new BufferedReader(console)) {
    System.out.println("Enter your name:");
    String name=reader.readLine();
    System.out.println("Welcome " + name);
} catch (IOException e) {
    e.printStackTrace();
}
```

> **MERK**: `Scanner`-klassen gitt av `java.util.Scanner` er mer robust når det gjelder å samle bruker-input. Den har mange metoder som bidrar til å begrense input-typer.
> 

### java.nio.file-pakke

`java.nio.file`-pakken inneholder klassen `Files`. Denne klassen kan brukes til å enkelt lese alt innhold fra en fil inn i en samling.

> **MERK**: Denne pakken bruker ikke **Streams** .
> 

I kodeutdraget nedenfor brukes `Files`-klassen for å lese alle linjene inn til en `List<String>`:

```java
import java.nio.file.Files;
import java.nio.file.Paths;
...
String pathToFile = "resources/output.txt";
try {
    List<String> lines = Files.readAllLines(Paths.get(pathToFile));
    for (String line: lines) {
        System.out.println(line);
    }
} catch (IOException e) {
    e.printStackTrace();
}
```

Hvis man ikke ønsker å lese linje for linje og i stedet vil lese alt innholdet i en enkelt variabel, kan  `readAllBytes` brukes. Kodeutdraget nedenfor illustrerer dette:

```java
String pathToFile = "resources/output.txt";
try {
    // A string can be created from a byte[]
    String data = new String(Files.readAllBytes(Paths.get(pathToFile)));
    System.out.println(data);
} catch (IOException e) {
    e.printStackTrace();
}
```

Java NIO står for **Java New Input Output** . Det er et bufferbasert alternativ til standard IO introdusert i Java 4 for høyhastighets IO-operasjoner. Det er kun merkbart ved svært høye tilkoblingsnivåer. Den ekstra logikken for å håndtere alle bufferne gjør det enklere å bruke standard IO ved færre tilkoblinger.

### IO-sammendrag

Standard IO i java kommer fra `java.io`-pakken og er Stream-basert. Det er input-strømmer for å lese data og output-strømmer for å skrive data.

Det er to typer strømmer: **Byte** og **Character** . Bytestrømmer er representert ved de abstrakte klassene `InputStream` og `OutputStream`. Tegn-strømmer er representert ved de abstrakte klassene `Reader` og `Writer`.

Klassen `File` er en abstraksjon av en fil på disk og utfører ulike operasjoner på filer. Hvis du vil lese og skrive data, er en *bane* til en fil akseptabelt.

En vanlig måte å skrive til en fil på er å bruke `FileWriter`, som skriver et enkelt tegn om gangen. For å forbedre ytelsen ved å redusere antall IO-operasjoner på disken, kan du kombinere `FileWriter` med en `BufferedWriter`.

Lesing av data fra filer kan gjøres på mange måter. For å lese et enkelt tegn om gangen, bruk `FileReader` eller `BufferedReader`. For å lese inn data linje for linje, må du bruke `BufferedReader`. `Scanner` kommer fra `java.util.Scanner`-pakken og kan brukes til å lese brukerinput samt data fra filer atskilt med tokens.

All IO er utsatt for feil og bør pakkes inn i en *try-with-resources*-blokk med en `IOException` fanget. *try-with-resources* lukker automatisk strømmer når de forlater deres scope.

Et alternativ til den strømmebaserte `java.io`-metoden er å bruke den bufferbaserte `java.nio.file` `Files`-klassen. Det er mer effektivt, men utfordrende å jobbe rundt.

## Minnehåndtering

Java allokerer og deallokerer automatisk minne til objekter og krever ingen innblanding fra utviklere. Det er imidlertid fortsatt fordelaktig å forstå dens indre funksjoner, da det er noen situasjoner der du vil støte på minnerelaterte problemer.

Minnehåndtering håndteres av **JVM** og en **Garbage Collector (GC)**. GC er ansvarlig for å frigjøre minne når et objekt ikke lenger er nødvendig.

> **MERK**: Ved å ikke vite hvordan GC og Java-minnet er utformet, kan du ha objekter som ikke kvalifiserer for GC, selv om du ikke lenger bruker dem. Noe som resulterer i minnelekkasjer.
> 

JVM definerer ulike runtime-dataområder som brukes under kjøringen av et program. Noen av disse områdene er opprettet av JVM med andre opprettet av trådene som brukes i programmet.

- Minneområdet opprettet av JVM blir ødelagt kun når JVM avsluttes.
- Dataområdene i tråden opprettes under instansiering og ødelegges når tråden går ut.

Minnet er delt inn i 5 deler:

- Metodeområde
- Heap
- Stack
- PC-Register
- Native minne

En illustrasjon av minnestrukturen i Java er gitt nedenfor:

![Untitled](Prominente%20Java-funksjoner%20cdddcd5a6ab446dcb519e3a6ca6ff463/Untitled%203.png)

> **INFO**: Begrepene som er involvert i dette diagrammet er beskrevet i underseksjonene nedenfor.
> 

### Stack

Stack-minne er ansvarlig for å holde referanser til heap-objekter og for lagring av **verdityper** (primitive typer), som holder selve verdien i stedet for en referanse til et objekt fra heap.

I tillegg har variabler på stacken en viss synlighet, også kalt **scope**. Kun objekter fra det aktive omfanget brukes. For eksempel, forutsatt at vi ikke har noen globale omfangsvariabler (felt), og bare lokale variabler, hvis kompilatoren kjører en metodes *body*, kan den bare få tilgang til objekter fra stacken som er innenfor metodens *body*. Den kan ikke få tilgang til andre lokale variabler, da de er utenfor omfanget. Når metoden er fullført og kommer tilbake, spretter toppen av stacken ut, og det aktive scopet endres.

> **MERK**: En ramme lages på stacken per tråd, inkludert de som er laget av JVM.
> 

### Stack-ramme

En stack-ramme er en datastruktur som inneholder trådens data. Tråddata representerer tilstanden til tråden i gjeldende metode.

- Den brukes til å lagre delresultater og data.
- Den utfører også dynamisk lenking, verdier returnert av metoder og utsendte unntak.
- Når en metode påkalles, opprettes en ny ramme. Rammen blir ødelagt når metoden er fullført
- Hver ramme inneholder sin egen *Local Variable Array* (LVA), *Operand Stack* (OS) og *Frame Data* (FD).
    - Størrelsene på LVA, OS og FD ble bestemt på kompileringstidspunktet.
- Kun én ramme (rammen for den utførende metoden) er aktiv til enhver tid i en gitt kontrolltråd.
    - Kalles gjeldende ramme, og metoden er kjent som gjeldende metode.
    - Metodeklassen kalles gjeldende klasse.
- Rammen stopper i gjeldende metode hvis metoden påkaller en annen metode.
- Rammen opprettet av en tråd er lokal for den tråden og kan ikke refereres til av noen annen tråd.

### Heap

Heap er et delt runtime-dataområde og lagrer det faktiske objektet i minnet. Dette minnet ble instansiert under JVM-oppstarten og tildeles alle klasseforekomster og arrays.

Heap kan være av fast eller dynamisk størrelse avhengig av systemets konfigurasjon. JVM gir brukerkontroll for å initialisere eller variere størrelsen på heapen.

Når et nytt nøkkelord brukes, blir objektet tildelt en plass i *heap* , men referansen til det samme finnes på *stacken* .

> **MERK**: Det er bare en enkelt heap.
> 

Når vi kjører kodelinjen:

```java
Scanner sc = new Scanner(System.in);
```

Et objekt i `Scanner`-klassen blir allokert til heapen og referansen `sc` blir skjøvet til stacken

> **MERK**: Garbage collection er obligatorisk i heapen
> 

### Metodeområde

Metodeområdet er en del av *heap-*minnet som deles mellom alle trådene. Den opprettes når JVM starter opp og brukes til å lagre klassestruktur, superklassenavn, grensesnittnavn og konstruktører. JVM lagrer følgende typer informasjon i metodeområdet:

- Et fullt kvalifisert navn av en type (f.eks. String)
- Typens modifikatorer
- Typens direkte superklassenavn
- En strukturert liste over de fullt kvalifiserte navnene på supergrensesnitt

> **MERK**: Selv om metodeområdet logisk sett er en del av heapen, kan det hende at garbage collection skjer
> 

### PC-register

Hver JVM-tråd som utfører oppgaven til en spesifikk metode har et programtellerregister knyttet til seg. PC-registeret er i stand til å lagre returadressen eller en innfødt peker på en bestemt plattform.

### Native-minne

Også kalt C-stabler, native metodestabler er ikke skrevet i Java, Java Native Interface (JNI) kaller native stack. Dette minnet tildeles for hver tråd ved opprettelse, og det kan være av fast eller dynamisk karakter.

### Garbage collection (datasanering)

Garbage collection er automatisk i Java. Når ingen live tråd har tilgang til objektet, er det kvalifisert for garbage collection. Dette skjer når stack-rammen fjernes, noe som resulterer i at referansen til det objektet ikke lenger eksisterer.

arbage collection-prosessen fører til at resten av prosessene eller trådene settes på pause og er dermed kostbart

> **MERK**: Dette kan justeres ved å bruke forskjellige Garbage collection-algoritmer. Det er utenfor scopet av det vi trenger å dekke.
> 

### Referansetyper

Området med minnehåndtering som påvirker utviklerne mest er verdi- og referansetyper . De har allerede blitt dekket tidligere, men en oppfriskning:

- Verdi- primitiver
- Referanse - objekter

Den største forskjellen mellom de to er at når du endrer en referansetype, blir den endret overalt, mens verdityper er scoped.

I Java er det forskjellige typer referanser: sterke, svake, myke og fantom-referanser. De er forskjellige i hvordan gjenstandene er garbage collected. Som standard er alle objekter sterke referanser med mindre de er eksplisitt definert av utvikleren. Bruken av disse ulike typene er utenfor dette kursets omfang.

For å illustrere virkningen referansetyper har på en applikasjon, vurder følgende situasjon.

Vi har et objekt, `Foo` som har `int`-feltet `bar`. Vi har også en `int`-variabel i `main()` kalt `value`. Det er en metode kalt `changeValues(int number, Foo objectNumber)` som endrer hver variabel i scopet av metoden ved å øke dem med 10. 

Kodeutdraget nedenfor illustrerer dette:

```java
public class Foo {
    private int bar = 1;

    public int getBar() {
        return bar;
    }

    public void setBar(int bar) {
        this.bar = bar;
    }
}

public class Main {

    public static void main(String[] args) {
        int myVal = 1;
        Foo myRef = new Foo();
        changeValues(myVal, myRef);
        System.out.println(myVal); // OUTPUT: 1
        System.out.println(myRef.getBar()); // OUTPUT: 11
    }

    public static void changeValues(int number, Foo objectNumber) {
        number += 10;
        objectNumber.setBar(objectNumber.getBar() + 10);
    }
}
```

*Hvorfor er output forskjellig når de begge hadde samme handling utført på dem?*

Svaret er: `Foo` er et objekt og objekter blir referert til.

Vi kan se på hvordan dette fungerer:

- når `int myVal = 1` er deklarert, legges den på stacken i `main()`-rammen for å lagres i minnet.
- når `Foo myRef = new Foo()` er deklarert, plasseres et nytt objekt av typen `foo` på heap og referansen `reference` plasseres på stack-rammen.
- når vi påkaller `changeValues(myVal, myRef)`, får man tilgang til stack-rammen som inneholder dataene for gjeldende `changeValues`-metode. Når dette skjer, lages en kopi av `myVal`. Det er en egen `int number = 1` i stack-rammen for `changeValues`-metoden i tillegg til den i stack-rammen for `main`-metoden.
- parameteren `Foo objectNumber` lager ikke en kopi, fordi den er et objekt. Det peker bare på referansen i `main`-metodens stack-ramme.
- når vi endrer `number += 10` blir kopien endret.
- når vi endrer `objectNumber.setBar(objectNumber.getBar() + 10)` endrer vi den opprinnelige verdien.
- når vi kommer tilbake til scopet for `main`, har vi `int myVal` som ble erklært i denne rammen, med verdien 1.
- den endrede verdien er nå den nye verdien for `myRef` i dette scopet.

## Lambda-uttrykk

Et lambda-uttrykk er en kort kodeblokk som tar inn parametere og returnerer en verdi. Lambda-uttrykk ligner på metoder, men de trenger ikke et navn, og de kan implementeres rett i *body* til en metode. Lambda-uttrykk tillater elementer av funksjonell programmering.

> **MERK**: Lambda-uttrykk er anonyme metoder.
> 

Det er flere maler vi kan velge mellom avhengig av våre behov.

En enkelt parameter med en enkelt kodelinje som skal kjøres:

```
parameter -> expression
```

Flere parametere med en enkelt kodelinje som skal utføres:

```
(parameter1, parameter2) -> expression
```

Flere parametere med flere kodelinjer:

```
(parameter1, parameter2) -> { code block }
```

`Expression` returnerer automatisk en verdi hvis mulig. `code block` trenger en dedikert `return`-erklæring for å returnere en verdi. Lambda-uttrykk sendes vanligvis som parametere til en metode. Ta for eksempel en `List`, den har en `forEach`-metode som kjører noe kode for hvert element på innsiden. Vi kan spesifisere hva som skal utføres med en lambda-funksjon:

```java
List<String> days = List.of("Monday", "Tuesday", "Wednesday", "Thursday","Friday");
// Print each element
days.forEach(d -> System.out.println(d));
```

Parameteren `d` i uttrykket er bare et alias for det gjeldende elementet. Hvis du ser på argumenttypen for `forEach`-metoden, vil du se at det er et `Consumer`-grensesnitt. Det er mange typer `Consumer`-grensesnitt som representerer variasjoner av lambda-uttrykk. Dette gjør at vi kan lagre lambda-uttrykk som variabler til å brukes senere. Kodeutdraget nedenfor illustrerer dette:

```java
List<String> days = List.of("Monday", "Tuesday", "Wednesday", "Thursday","Friday");
Consumer<String> printString = s -> System.out.println(s);
// Print each element
days.forEach(printString);
```

Det generiske argumentet for `Consumer` representerer typen parameter for uttrykket.

> **MERK**: Lagring av et lambda-uttrykk som en variabel brukes bare når du vil gjenbruke det, men på det tidspunktet kan du like godt lage en navngitt metode.
> 

## Stream API

Pakken `java.util.stream` inneholder klasser, grensesnitt og enums for å gi en mer funksjonell tilnærming til programmering i Java.

> **MERK**: Dette er ikke det samme som Stream fra I/O.
> 

En stream kan defineres som en sekvens av elementer fra en kilde. Kilden til elementer her refererer til en *Collection* eller *Array* som gir data til streamen

Streams er i stand til intern iterasjon, noe som betyr at de itererer selve elementene. Dette skiller seg fra samlinger som er avhengige av Iterable-grensesnittet og utvikleren som implementerer iterasjonen.

Operasjoner utført på en stream endrer ikke kilden. For eksempel, filtrering av en stream hentet fra en samling produserer en ny stream uten de filtrerte elementene, i stedet for å fjerne elementer fra kildesamlingen.

> INFO: Tankeprosessen involvert når du arbeider med Streams er veldig lik den med å skrive SQL-setninger. Hvis du ikke er kjent med SQL, dekkes det i en senere leksjon.
> 

Kodeutdraget nedenfor viser hvordan man oppretter en stream:

```java
List<String> names = List.of("Monday", "Tuesday", "Wednesday", "Thursday","Friday", "Saturday", "Sunday");
Stream<String> namesStream = names.stream();
// Creating a stream directly
Stream<String> streamNames = Stream.of("Monday", "Tuesday", "Wednesday", "Thursday","Friday", "Saturday", "Sunday");
```

Nå som vi har *Collection* som en stream, kan vi utføre mange operasjoner som filter, map, limit, reduce, find, match og så videre. De fleste av stream-operasjonene returnerer selve streamen slik at resultatene deres kan bli *pipelinet*. Disse operasjonene kalles mellomoperasjoner og deres funksjon er å ta inn input, behandle dem og returnere output til målet. Metoden `collect()` er en terminaloperasjon som normalt er tilstede ved slutten av en pipeline-operasjon for å markere slutten av strømmen. Kodeutdraget nedenfor illustrerer å avslutte streamen:

```java
List<String> streamBackToList = namesStream.collect(Collectors.toList());
```

Kodeutdraget nedenfor sammenligner iterering over en Collection mot en Stream:

```java
// Using iterative for
for (int i = 0; i < collection.size(); i++) {
    System.out.println(collection.get(i));
}
// Using foreach
for (String name: collection) {
    System.out.println(name);
}
// Using Stream with lambda
collection.stream().forEach(name -> System.out.println(name));
// Alternative version without using a cast parameter and the method reference operator. It is passed each element by the forEach operation
collection.stream().forEach(System.out::println);
```

### Stream-operasjoner

Den virkelige styrken til Streams er når operasjoner utføres på samlingene gjennom mapping, filtrering, søking og sortering.

### filter()

Metoden `filter` brukes til å eliminere elementer basert på visse kriterier. Filtermetoden tar et predikat (et lambda-uttrykk i vårt tilfelle), som kalles for hvert element i streamen. Hvis elementet skal inkluderes i den resulterende streamen, skal predikatet returnere true. Hvis elementet ikke skal inkluderes, skal predikatet returnere false. Kodeutdraget nedenfor filtrerer ut alle tallene over en terskel:

```java
Stream<Integer> temperatures = Stream.of(17,15,24,23,13,28,30,24);
// Get unique temperatures above 20
List<Integer> above20 = temperatures
    .filter(t -> t > 20)
    .distinct()
    .collect(Collectors.toList());
```

> **MERK**: Hver lenket metode på en ny linje er å forbedre lesbarheten
> 

### map()

Metoden `map` brukes til å kartlegge hvert element til dets korresponderende resultat.

```java
List<String> words = List.of("foo", "BAR", "bAz");

// Convert all to uppercase & print
// OUTPUT: FOO BAR BAZ
words.stream()
    .map(String::toUpperCase)
    .forEach(System.out::println);

// Convert all to lowercase, sort & print
// OUTPUT: bar baz foo
words.stream().map(String::toLowerCase)
    .sorted()
    .forEach(System.out::println);

// Can chain with filter
// OUTPUT: baz
words.stream()
    .filter(w -> w.startsWith("b")) // Case sensitive
    .map(String::toLowerCase)
    .forEach(System.out::println);
```

> **MERK**: Vi må bruke `stream()` hver gang og kan ikke bare bruke `Stream.of`. Grunnen til dette er at streamen blir ødelagt når vi bruker en avsluttende operasjon som `forEach` eller `collectors`.
> 

### reduce()

Stream API har flere avsluttende setninger som aggregerer en stream til en enkelt verdi: min, max, count og sum. Det er omstendigheter når en utvikler trenger å tilpasse denne prosessen, `reduce`-metoden gjør nettopp det.

Denne metoden tar inn to parametere, det gjeldende elementet og det neste elementet, og returnerer et `Optional` resultat. Scopet av hva som kan gjøres med *reduce* er nesten ubegrenset, kodeutdraget nedenfor viser to grunnleggende scenarier: konkatenering av en liste med strenger og summering av en liste med tall.

```java
List<String> words = List.of("foo","bar","baz","qux","quux");
List<Integer> numbers = List.of(1,2,5,4,7,13,6,57);
String wordsMerged = words.stream().reduce((s1,s2) -> s1 + "-" + s2).get();
int total = numbers.stream().reduce((n1,n2) -> n1 + n2).get();
System.out.println(wordsMerged); // OUTPUT: foo-bar-baz-qux-quux
System.out.println(total); // OUTPUT: 95
```

### Diverse operasjoner

Denne underseksjonen viser noen mindre, relaterte ikke-terminerende operasjoner som kan brukes til å samle informasjon fra collections.

```java
// List of metasyntactic variables
List<String> words = List.of(
    "foo","bar","baz","qux","quux","quuz",
    "corge","grault","garply","waldo","fred");
// Print the first 5
words.stream().limit(5).forEach(System.out::println);
// Count how many start with q
long countStartingWithQ = words.stream().filter(w -> w.startsWith("q")).count();
// See if any elements start with ba
boolean anyStartWithBA = words.stream().anyMatch(w -> w.startsWith("ba"));
// See if no elements start with z
boolean noneStartsWithZ = words.stream().noneMatch(w -> w.startsWith("z"));
// See if all elements start with a
boolean allStartsWithA = words.stream().allMatch(w -> w.startsWith("a"));

// List of numbers
List<Integer> numbers = List.of(1,4,3,2,5,16,7,5,28,12);
// Get the minimum using min (special case of reduce)
int min = numbers.stream().min(Integer::compareTo).get(); // min returns an Optional<T>
// Get the maximum using max (special case of reduce)
int max = numbers.stream().max(Integer::compareTo).get(); // max returns an Optional<T>
// Using summaryStatistics. Needs a IntStream to work
IntSummaryStatistics stats = numbers.stream().mapToInt(n -> n).summaryStatistics();
int statMin = stats.getMin();
int statMax = stats.getMax();
double statAvg = stats.getAverage();
long statTotal = stats.getSum();
```

Metodene `min` og `max` er spesielle tilfeller av å redusere. De tar inn et slags argument, `Comparator`. Da vi skrev `Integer::compareTo` var det samme som å skrive `(val1,val2) -> val1.compareTo(val2)`. Når vi bruker primitiver, kan vi ganske enkelt bruke `compareTo`-metoden på klasseinnpakningene. dvs. `String::compareTo`, `Double::compareTo` osv. Når vi har tilpassede objekter, må vi sammenligne egenskaper for å se hvilket objekt som er størst. Dette kan gjøres ved å definere hvilken egenskap som trenger sammenligning, for eksempel `Comparator.comparing(Employee::getSalary)` hvis vi brukte en *Employee Salary* som sammenligningsgrunnlag. Vi kan også overstyre `compareTo`-metoden i selve `Employee`-klassen og deretter bruke `Employee::compareTo` som argument.

## Sammendrag

- IO i Java bruker streams til å konsumere input og produsere output.
- Filklassen representerer en abstraksjon for en fil som kan brukes til å opprette og slette filer.
- FileReaders og BufferReaders kan brukes til å lese data fra en fil.
- FileWriter kan brukes til å skrive data til en fil.
- Scanner er en generell IO-klasse som kan brukes til å lese bruker-input.
- Java har automatisk minnehåndtering gjennom *garbage collection*.
- Stack-minne inneholder alle verdiene til en applikasjon som kjører.
- Heap-minne eksisterer for å holde objekter, og peker på en referanse på stacken. Gjenstander på heap må samles opp.
- Endringer i objekter påvirker den opprinnelige forekomsten, da de er referansetyper.
- Lambda-uttrykk er anonyme metoder for å gi on-the-fly funksjonalitet.
- Stream API gir nyttige operasjoner for mapping, filtrering og reduksjon av collections.

> **RESSURS**: Et eksempelprosjekt som dekker de fleste konsepter for hele modulen finner du [her](https://gitlab.com/noroff-accelerate/java/projects/module-1-weather-station-sample-project).
> 

---

Copyright 2021, Noroff Accelerate AS