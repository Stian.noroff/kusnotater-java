# Objektorientert design

I denne leksjonen vil objektorientert praksis være i fokus. Dette inkluderer en introduksjon til SOLID-prinsippene og hvordan de legger til rette for ren, vedlikeholdbar og testbar kode. Leksjonen vil fokusere på hvert prinsipp individuelt og vise deres bruk ved å rydde opp i en eksisterende kodebase. Designmønstre introduseres deretter som en mer praktisk implementering av SOLID-prinsippene.

**Læringsmål:**

Etter denne leksjonen vil du være i stand til å…

- **beskrive** de fem SOLID-prinsippene.
- **diskutere** hvorfor det er fordelaktig å utvikle ren, vedlikeholdbar og testbar kode.
- **vurdere** i hvilken grad gitt kode er på linje med SOLID-prinsipper.
- **modifisere** eksisterende kode for å samsvare med SOLID-prinsipper.
- **forklare** og **vurdere** rollene til KISS, DRY, og YAGNI.
- **diskutere** hva designmønster er, og rollen de har i industrien.
- **beskrive** de forskjellige kategoriene av designmønster

---

Å vite hvordan man gjør Objekt Orientert Programmering er bare halve utfordringen. Å gjøre det i samsvar med den beste praksisen er den andre halvdelen. Hovedgrunnen til hvorfor dette er slik er en enkel grunn:

*For bedriftsløsninger, er du ikke den eneste utvikleren som jobber på prosjektet og det prosjektet går gjennom flere iterasjoner.*

Med dette i tankene, som en utvikler ønsker du å lage programmer som er enkelt leselige, gjenbrukbare, og har et klart *formål.* Dette resulterer i mindre tid og penger brukt på å integrere kode siden det er mindre sannsynlig at en feil oppstår.

På grunn av den iterative naturen til bedriftsløsninger, spesielt i nåtidens verden av *Agil* utvikling, er det et sterkt krav at koden som er skrevet, enkelt kan utvides og endres. Dette betyr at du burde skrive kode som kan ha funksjoner lagt til, og fortsatt opprettholde den originale funksjonaliteten. Dette, kombinert med behovet for automatisering, resulterer i at testing er en integrert del av programvareutviklingslivssyklusen.

Som sett tidligere, er enhetstesting et kraftig verktøy som kan brukes til å iterere i små trinn på eksisterende kode i et raskt tempo, samtidig som funksjonalitet sikres. Av denne grunn er det å skrive kode som er lett testbar en viktig komponent i god programmeringsdesign.

Det er flere prinsipper og retningslinjer som hjelper utviklere med å skrive bedre kode, denne leksjonen dekker noen av de mest populære.

## Kobling

Kobling er mer som en måleenhet. Det er en måte å måle hvor mye forskjellige programkomponenter avhenger av hverandre. Sammenhengende klasser er enten *tett* eller *løst* koblet.

Klasser som er *tett* koblet er vanskeligere å bruke igjen hver for seg siden de er avhengig av andre klasser for å fungere. Dette er problematisk når man vurderer målet beskrevet i introduksjonen til denne leksjonen. *Løst* koblet klasser har bedre portabilitet og gjenbruk som gir enklere utvidelse og endring uten å ødelegge andre aspekter av systemet. Det er klart at utviklere:

> MERK: Streber etter å utvikle løst koblede klasser
> 

Så langt har tett og løst kobling blitt diskutert mer i forhold til resultatene i stedet for hva disse individuelle komponentene betyr.

**Tett kobling**

En mer formell definisjon av tett kobling kan bli beskrevet som:

*Hvis A vet mer en den burde om hvordan B var implementert, så er A og B **tett koblet**.*

Klasser er sett som tett koblet når de endrer sammen, som betyr: når klassen A endres, må klassen B også endres. En bivirkning av tett kobling kan være at klassene deler mange avhengigheter, som resulterer i vanskeligheter for å oppretholde og endre koden.

Den tetteste formen for kobling er når en klasse endrer dataen til en annen klasse, dvs. selve klassen er gitt og handlet på. Dette kalles `innhold kobling` og er noe som burde unngås.

**Løs kobling**

På den andre siden av spekteret er løs kobling, noe utviklere burde strebe mot. En er formell definisjon av løs kobling kan bli beskrevet som: 

*Hvis den eneste kunnskapen klassen A har om klassen B, er hva klassen B har eksponert gjennom sitt grensesnitt, da er klassene A og B løst koblet.*

Den ideele tilstanden er å ha klasser kommunisere rent gjennom *data*, som resulterer i at klassene er helt uavhengige.

## Kohesjon

Kohesjon er enda et verktøy brukt for å måle klasse likhet, i motsetning til kobling, fokuserer kohesjon på elementene av en *enkel klasse*. Det er en måte å måle i hvilken grad elementer er funksjonelt relatert - med andre ord er delene av klassen knyttet til å gjøre det samme.

Kohesjon kan enten være *høy* eller *lav*. Kohesjon går ofte hånd i hånd med kobling, hvor klasser som har *høy kohesjon* individuelt, ofte har løs kobling.

**Høy kohesjon**

Kohesjon kan bli visualisert som lim, hvis alt henger sammen, er kohesjonen høy. Hva betyr det å *henge sammen*? Hvis elementene (metoder) av klassen tjener samme formål, feks. forskjellige måter å skrive ut linjer fra en tekstfil, da henger de sammen; som resulterer i høy kohesjon. Dette er knyttet til ansvar, hvis en klasse har et ansvar og alle elementene til den klassen tjener det ansvaret så er kohesjonen til den klassen høy.

**Lav kohesjon**

Når elementer til en klasse gjør forskjellige ting, har forskjellige ansvar, er klassen ufokusert og derfor har det lav kohesjon. Dette resulterer i at det eneste forholdet til de forskjellige elementene er lokasjonen. Hvis vi tar det tidligere eksempelet om å skrive ut; hvis det var en metode lagt til som konverterte tekstlinjen til [pig latin](https://en.wikipedia.org/wiki/Pig_Latin#:~:text=Pig%20Latin%20is%20a%20language,to%20create%20such%20a%20suffix.) i klassen, ville kohesjonen vært lavere. Den ideelle måten å fikse dette er å ha en klasse dedikert til forskjellig måter å konvertere tekst og så bruke *skrive ut* klassen til å skrive ut den konverterte linjen - dette øker kohesjonen og løsere kobling.

> MERK: Du kan ikke tilegne et antall til kohesjon og kobling, det er mer abstrakt og kommer med erfaring fra både velskrevet og dårlig skrevet kode.
> 

På grunn av dens abstrakte natur, kan kohesjon og kobling være vanskelig å gripe for nyere utviklere. Heldigvis er dette et gammelt problem og svært smartere personer har lagd prinsipper og retningslinjer for å hjelpe oppnå løs kobling og høy kohesjon. Den mest kjente er SOLID prinsippene.

## S.O.L.I.D Prinsippene

På tidlig 2000 tallet foreslo Robert C. Matin, en fremtredende figur i bevegelsen for å skrive bedre kode, et sett av prinsipper i hans artikkel [“Design Principles and Design Patterns”](https://web.archive.org/web/20150906155800/http://www.objectmentor.com/resources/articles/Principles_and_Patterns.pdf). 
Disse prinsippene gjorde programdesign enklere å forstå, mer fleksible og oppretholdbare. Siden da har SOLID prinsippene vært innarbeidet i bedriftsløsninger og er ofte temaet til mange `tekniske intervju for junior utviklere`.

Prinsippene gir veiledning på typiske metoder for å oppnå *løs kobling* og *høy kohesjon*. De er kraftige når man vil skalere applikasjoner siden kompleksiteten til systemene blir lavere. En ekstra bivirkning av dette, er at systemer er enklere å teste. Hver bokstav i SOLID akronymet representerer et prinsipp, de følgene underseksjonene forklarer hvert prinsipp.

### Single Responsibility

Single-responsibility Prinsippet (SRP) sier:

*En klasse burde ha en og bare en grunn til bli endret, som betyr at en klasse burde bare ha en jobb.*

Dette hjelper på mange måter:

- Det hjelper testing siden det er færre tester per klasse.
- Det er mindre funksjonalitet og avhengigheter i klassen, derfor er kobling løsere.
- Mindre, pakkede klasser er enklere å søke - forbedrer prosjekt organisering.

**Eksempel på prinsippet**

Dette eksempelet har en `Book` klasse, den har noen metoder som relaterer til en manipulasjon av en bok.

```java
public class Book {
    private String name;
    private String author;
    private String text;

    // constructor, getters and setters

    // methods relating to book properties
    public String replaceWord(String word) {
        return text.replaceAll(word, text);
    }

    public boolean isWordIntext(String word) {
        return text.contains(word);
    }

    // method to print book
    public void printText() {
        System.out.println(text);
    }
}
```

Denne klassen har følgende metoder: `replaceWord`, `isWordInText` og `printText`. Disse metodene ser ut til å være beslektede, men det er et unntak - printText. Denne metoden har ansvaret for å skrive ut teksten, mens de to andre metodene har ansvaret for å manipulere teksten.

Videre har den en tett kobling til `System.out.println`metoden, noe som begrenser måten tekst kan vises på. Hva om det måtte være en utvidelse for å skrive ut tekst på forskjellige måter - denne klassen må endres hver gang (dette problemet er løst i Open/Closed-prinsippet).

For å følge `SRP` må vi trekke ut utskriftsmetoden til en annen klasse som har ansvaret for å skrive ut tekst på ulike måter. Erstatte koden med et kall til en annen klasse (kalt deferring), og ganske enkelt sende teksten (som data) til den klassen. Dette økte både kohesjonen og løsner koblingen ettersom vi kommuniserer med data mellom to mindre, sammenhengende klasser.

```java
public class BookWriter {
    // methods to print the book, from Book class
    public void printTextToConsole(String text) {
        System.out.println(text);
    }

    public void logTextToFile(String text) {
        // logic to log the text
    }
}
```

Dette eksemplet virker rett frem når det er en trinnvis prosess og bruddet er så klart. Dette er imidlertid svært nyansert og krever kunnskap om problemet, forretningsbehov og applikasjonsartikturen.

En enkel måte å tilnærme seg dette på er å spørre «hva er denne klassen ment å gjøre? Gjør den én ting?" og prøv å ikke overtenke det for da er det lett til å ende opp i en [analysis paralysis](https://en.wikipedia.org/wiki/Analysis_paralysis#Software_development).

SRP-brudd blir som regel rettet under gjennomgangen av koden. Det betyr at det er veldig vanskelig å fange opp bruddene når koden skrives. Så ikke bekymre deg hvis klassene dine ikke er perfekte de første gangene.

### Open/Closed - norsk: Åpne/Stengt

Open-closed Principle (S.R.P.) sier følgende:

*Objekter eller enheter bør være åpne for utvidelse, men stengt for modifikasjon.*

Dette prinsippet er ganske selvforklarende ved sin definisjon, arv(*inheritence* ) og grensesnitt hjelper i stor grad med å følge dette prinsippet.

**Eksempel på prinsippet**

> MERK: Dette eksemplet er dårlig med vilje. Det illustrerer et trekk i riktig retning, men på feil måte.
> 

La oss ta scenariet med å modellere en kalkulator. Vi ønsker å lage et grensesnitt (interface) for å referere til enhver kalkulatoroperasjon, kalt `CalculatorOperation.` Deretter kan vi lage en `Addition`  (tilleggsoperasjon) som implementerer det grensesnittet og lager en kalkulatorklasse for å utføre operasjonen.

```java
public interface CalculatorOperation {}

// Addition class
public class Addition implements CalculatorOperation {
    private double left;
    private double right;
    // getters, setters, and constructor
}
// Subtraction class - similar to Addition

// Calculator class to perform operations
public class Calculator {
    public double calculate(CalculatorOperation operation) {
        if (operation instanceof Addition) {
            Addition addition = (Addition) operation;
            return addition.getLeft() + addition.getRight();
        } else if (operation instanceof Subtraction) {
            // same as above but subtract
        }
    }
}
```

Som du kan se, dersom det er noen ekstra operasjoner, i dette eksempelet: `Subtraction`, må `Calculator`-klassen tilpasses. 
Dette bryter Open/Closed-prinsippet. Å fikse dette problemet krever at vi revurderer tilnærmingen.

Vi kan beholde klassen for hver operasjon, samt hvordan operandene lagres - dette er for å begrense mengden endringer som må gjøres for å fikse bruddet.

Vi kan ganske enkelt legge til en `perform` metode til  `CalculatorOperation`-grensesnittet, og i stedet for å sjekke forekomsttypen, ber vi ganske enkelt operasjonen utføre, og avhengig av forekomsten - vil den gjøre noe annerledes.

```java
public interface CalculatorOperation {
    void perform();
}

// Addition class with functionality
public class Addition implements CalculatorOperation {
    private double left;
    private double right;
    // getters, setters, and constructor 

    // Perform method
    @Override
    public double perform() {
        return left + right;
    }
}

// Improved calculator class
public class Calculator {
    public double calculate(CalculatorOperation operation) {
        return operation.perform();
    }
}
```

Dette resulterer i at langt mindre kode er i kalkulatorklassen, og hver gang vi legger til en ny operasjon trenger ingenting å endres. Koden er ikke OCP-kompatibel, og som en bonus, løst koblet.

### Liskov Substitution

Liskov Substitution prinsippet tilsier: 

*La q(x) være en egenskap som kan bevises om objekter av x av type T. Da skal q(y) kunne bevises for objekter y av type S der S er en undertype av T.*

Dette betyr at hver underklasse eller avledet klasse bør være substituerbar med sin base eller overordnede klasse. For å være substituerbar må undertypen oppføre seg som sin supertype. Dette høres ganske komplisert ut, men det blir normalt sett automatisk ved å følge de andre prinsippene og riktig arv(inheritence).

Det eneste som må huskes er overstyrte metoder og dataene knyttet til de. Prøv alltid å overstyre(override) metoder som har annen funksjonalitet enn deres overordnede(parent), og ikke lag nye metoder for å håndtere variasjonen. På denne måten kan programmet erstatte overordnede klasser med basisklasser, og programmet skal fortsatt fungere som det skal - dette betyr ikke kompilering, dette betyr at outputen fortsatt er det du forventer.

> MERK: Dette prinsippet er svært subtilt og vanskelig å finne brudd, det er minst håndhevet i kodegjennomganger. Imidlertid unngås det normalt helt når de andre prinsippene følges.
> 

**Eksempel på prinsippet**

Dette prinsippet forklares best med et eksempel, hvor et brudd er tvunget. La oss anta at du prøver å lage en applikasjon for sosiale medier som har ulike former for`posts` et generelt  `Post`, et `Tag`og en `Mention.`

> MERK: Databaseklassen nevnt her er en dummy-klasse, vi antar at alle disse metodene er implementert riktig.
> 

```java
class Post
{
    void CreatePost(Database db, string postMessage)
    {
        db.Add(postMessage);
    }
}

class TagPost : Post
{
    override void CreatePost(Database db, string postMessage)
    {
        db.AddAsTag(postMessage);
    }
}

class MentionPost : Post
{
    void CreateMentionPost(Database db, string postMessage)
    {
        string user = postMessage.parseUser();

        db.NotifyUser(user);
        db.OverrideExistingMention(user, postMessage);
        base.CreatePost(db, postMessage);
    }
}
```

For å håndtere innleggene er det en `PostHandle`-klasse:

```java
class PostHandler
{
    private database = new Database();

    void HandleNewPosts() {
        List<string> newPosts = database.getUnhandledPostsMessages();

        foreach (string postMessage in newPosts)
        {
            Post post;

            if (postMessage.StartsWith("#"))
            {
                post = new TagPost();
            }
            else if (postMessage.StartsWith("@"))
            {
                post = new MentionPost();
            }
            else {
                post = new Post();
            }

            post.CreatePost(database, postMessage);
        }
    }
}
```

Ved å se på koden vil du se at `MentionPost` legger inn ved å bruke `CreateMentionPost`metoden, og `PostHandler`klassen bruker *polymorphism* for å referere til enhver `Post`undertype som en `Post`, og kaller `CreatePost`metoden. Dette resulterer i at `MentionPost`saken ikke blir håndtert som forventet når den erstatter `Post` - fordi `Post`overordnet klasse har sin `CreatePost`metode kalt da den ikke er overstyrt i barnet, noe som bryter med prinsippet.

Å fikse dette er ganske enkelt, i stedet for å lage en metode for å håndtere den lille variasjonen av hvordan innlegg opprettes, overstyr ganske enkelt `CreatePost`.

```java
class MentionPost : Post
{
    override void CreatePost(Database db, string postMessage)
    {
        string user = postMessage.parseUser();

        db.NotifyUser(user);
        db.OverrideExistingMention(user, postMessage);
        base.CreatePost(db, postMessage);
    }
}
```

Dette resulterer i at alle barn av`Post` kan erstatte `Post`og alt fungerer som det skal. 

Husk:

### Grensesnittsegregering

Grensesnittsegregeringsprinsippet sier:

*En klient skal aldri bli tvunget til å implementere et grensesnitt den ikke bruker, eller klienter skal ikke tvinges til å være avhengig av metoder de ikke bruker.*

Dette prinsippet er veldig likt`SRP` og `OCP`, men for grensesnitt og atferd, hvor vi streber etter å dele opp større grensesnitt i mindre, mer fokuserte versjoner. Som andre refactoring-prosesser er dette veldig tidkrevende, men resulterer i en mye mer fleksibel kode.

**Eksempel på prinsippet**

For å illustrere fallgruvene ved å ikke følge dette prinsippet, la oss se på et eksempel på modellering av betalingssystemer. I dette eksemplet har vi et betalingsgrensesnitt som fungerer som abstraksjonen for alle betalinger.

> MERK: Å bruke Object som en type er et resultat av at dette er et eksempel på design snarere enn en virkelighetsimplementering.
> 

```java
public interface Payment {
    void initiatePayments();
    Object status();
    List<Object> getPayments();
}
```

La oss nå legge til en `BankPayment` bankbetalingsimplementering.

```java
public class BankPayment implements Payment {
    @Override
    public void initiatePayments() { // ... }

    @Override 
    public Object status() { // ... }

    @Override 
    public List<Object> getPayments() { // ... }
}
```

Alt er imidlertid i orden på dette stadiet, når funksjonaliteten må utvides ved å foreta en ny type betaling, `LoanPayment`. Denne nye betalingen har noen krav som ennå ikke eksisterer, nemlig `initiateLoanSettlement` og `initiateRepayment`. Disse legges deretter til den eksisterende koden, noe som resulterer i:

```java
public interface Payment {
    // original methods
    ...
    void initiateLoanSettlement();
    void initiateRepayment();
}

public class LoanPayment implements Payment {
    @Override
    public void initiatePayments() { 
        throw new UnsupportedOperationException();
    }

    @Override 
    public Object status() { // ... }

    @Override 
    public List<Object> getPayments() { // ... }

    @Override
    public void initiateLoanSettlement() { // ... }

    @Override
    public void initiateRepayment() { // ... }
}
```

Nå har vi et problem, både lånebetaling og bankbetaling må implementere metoder de ikke trenger, så `BankPayment`må nå endres:

```java
public class BankPayment implements Payment {
    @Override
    public void initiatePayments() { // ... }

    @Override 
    public Object status() { // ... }

    @Override 
    public List<Object> getPayments() { // ... }

    @Override
    public void initiateLoanSettlement() { 
        throw new UnsupportedOperationException();
    }

    @Override
    public void initiateRepayment() { 
        throw new UnsupportedOperationException();
    }
}
```

Dette bryter nå med grensesnittsegregeringsprinsippet, og har du et godt øye, bryter det også med enkeltansvarsprinsippet og åpent lukket prinsipp.

> MERK: Tenk over hvorfor disse prinsippene brytes.
> 

> MERK: Det er ofte slik at hvis kode bryter med ett av SOLID-prinsippene, er det også i strid med andre.
> 

Dette skyldes den naturlige overlappingen og spesifikke forskjeller i omfanget av prinsippene. I disse tilfellene er det best å velge det mest spesifikke prinsippet som blir brutt og gjøre endringer i henhold til det.

I dette tilfellet, for å fikse grensesnittsegregeringsbruddet, bruker vi grensesnitt OG arv(inheritence) for å lage et arveverk av grensesnitt for å trekke ut vanlige metoder og ha forskjeller innkapslet i forskjellige grensesnitt.

I dette tilfellet; `status`og `getPayments`er felles for alle typer betalinger, initiatePayments er bankspesifikk, og `initiateLoanSettlement` og`initiateRepayment` er lånespesifikke.

```java
public interface Payment {
    Object status();
    List<Object> getPayments();
}

public interface Bank extends Payment {
    void initiatePayments();
}

public interface Loan extends Payment {
    void initiateLoanSettlement();
    void initiateRepayment();
}
```

Dette resulterer i tre grensesnitt; en overordnet(parent) `Payment`, `Bank` og`Loan`, alle med egen logikk med arv(inheritence ) som utnyttes for gjenbruk av kode. Betalingsimplementeringene kan nå velge hvilket grensesnitt som skal implementeres.

Lar klassene bare implementere metoder de trenger, og ingenting mer. Resulterer i ingen flere unntak, renere kode, færre feil og ikke flere SRP- og OCP-brudd.

```java
public class BankPayment implements Bank {
    // ...
}

public class LoanPayment implements Loan {
    // ...
}
```

**Grensesnitt vs arv(Inheritance)**

Når man ser på arv(**Inheritance)**, virker det som det opplagte valget for å modellere arvinger og ulik atferd. Dette skyldes at vinduet vårt er begrenset til en mindre skala.

I industrien hvis alt ble modellert utelukkende med arv, ville det være arverekker(**Inheritance)** så dype at du ikke ville være i stand til å finne ut hvilken oppførsel som gjelder for din spesifikke klasse. Dette blir så forsterket av mangelen på fleksibilitet som følger med arv(**Inheritance)** - du MÅ implementere all den nedarvede atferden.

Vi har allerede sett hvordan grensesnitt knytter atferd til enhver klasse som implementerer dem. For relasjoner som er dypere enn besteforeldre-barnebarn, er det langt bedre å prøve å modellere et flatere arvelag**(flatter heirarchy)** og utsette noe oppførsel til grensesnitt, eller til og med en familie av grensesnitt.

Å komponere klasser med atferd i stedet for å prøve å modellere et arveverk(**heirarchy)** er en annen populær tilnærming. Denne forestillingen om tenkning kalles "favorisering av komposisjon fremfor arv" og er grunnlaget for det meste av det som driver ideen bak Design Patterns - hvor kunnskapen om denne leksjonen er grunnlaget.

### Dependency Inversion (Avhengighetsinversjon)

Dependency inversion prinsippet tilsier: 

*Entiteter må være avhengige av abstraksjoner, ikke av konkreter. Den sier at modulen på høyt nivå ikke må være avhengig av modulen på lavt nivå, men de bør være avhengig av abstraksjoner.*

Dette prinsippet tillater frakobling. I stedet for høynivåmoduler avhengig av lavnivåmoduler, vil begge avhenge av abstraksjoner.

> MERK: Abstraksjoner bør ikke avhenge av detaljer. Detaljer bør avhenge av abstraksjoner.
> 

Som et resultat har vi kode som er godt strukturert, svært frakoblet og gjenbrukbar. Konseptet **"programmering til abstraksjon i stedet for implementering"** er en nøkkeldriver i dannelsen av designmønstre.

**Eksempel på prinsippet**

For best å illustrere prinsippet, må vi bryte det ved å lage tett koblet kode. I dette eksemplet modellerer vi datamaskinene til en databutikk. I vårt tilfelle har en `Computer`et `StandardKeyboard`og en `Monitor`.

```java
public class Computer {
    private final StandardKeyboard keyboard;
    private final StandardMonitor monitor;

    // constructor
    public Computer() {
        monitor = new StandardMonitor();
        keyboard = new StandardKeyboard();
    }
}
```

Dette ser greit ut til å begynne med, helt til du ser litt nærmere på det. Hva om vi ønsket å bruke et annet tastatur? En annen ting som ikke er klart før du tenker deg om, er de *nye* søkeordene.

Å instansiere en *ny StandardMonitor* og et *nytt StandardKeyboard* kobler klassen din tett til disse lavnivåimplementeringene. Basert på avhengighetsinversjonsprinsippet(dependency inversion principle) bør dette ikke skje. Det ideelle er å fjerne disse lavnivåavhengighetene og heller være avhengig av abstraksjoner.

```java
public interface Keyboard { // ... }

public class StandardKeyboard implements Keyboard { // ... }

public interface Monitor { // ... }

public class StandardMonitor implements Monitor { // ... }

public public class Computer {
    private final Keyboard keyboard;
    private final Monitor monitor;

    // constructor
    public Computer(Keyboard keyboard, Monitor monitor) {
        this.keyboard = keyboard;
        this.monitor = monitor;
    }
}
```

Now the `Keyboard` is an interface and we have classes for each kind of `Keyboard` that implement the abstraction. As of this example there is no behaviour for the keyboards, this is just for demonstration purposes.

Another important change was the dependency inversion in the constructor where abstractions are passed, and through polymorphism, can be any type of Keyboard at runtime. An added benefit of this is that our class is far easier to test - something to always keep in mind.

## Design Patterns ( Desgin mønstre)

Designmønstre er gjenbrukbare løsninger på vanlige problemer som finnes i programvareutvikling. Disse mønstrene er ikke konkrete løsninger, men snarere maler på hvordan disse vanlige problemene kan løses i ulike situasjoner.

> MERK: Mønstre er ikke algoritmer. Algoritmer definerer et klart sett med handlinger som kan oppnå et eller annet mål, mønstre er en mer overordnet beskrivelse av en løsning.
> 

Disse mønstrene representerer de ulike beste praksisene som brukes av erfarne objektorienterte programvareutviklere, først vist i en bok skrevet av «Gang of Four» med tittelen [Design Patterns - Elements of Reusable Object-Oriented Software]([https://www.oreilly](https://www.oreilly/). com/library/view/design-patterns-elements/0201633612/). Siden dette har flere mønstre blitt lagt til, men de er fortsatt basert på de originale idealene som er fremsatt i denne boken.

Opprettelsen av designmønstre var basert på to prinsipper:

1. **Program til abstraksjoner ikke implementeringer**
2. **Foretrekk komposisjon fremfor arv**

### Fordeler

Gjenbruk av designmønstre bidrar til å forhindre subtile problemer som kan forårsake store problemer og forbedrer kodelesbarheten for kodere og arkitekter som er kjent med mønstrene. Dette er fordi Design Patterns er et vanlig språk for kjente, godt forståtte navn for programvareinteraksjoner.

Designmønstre sparer tid ved å bygge programvare basert på erfaringer fra andre utviklere og beste praksis. Designmønstre hjelper til med å gjøre koden din mer testbar, noe som sparer enda mer tid siden refaktorering ikke trenger å gjøres. I tillegg til dette hjelper læring av disse mønstrene uerfarne utviklere til å lære programvaredesign på en enkel og rask måte.

På grunn av at Design Patterns er velprøvde og dokumenterte løsninger bygget på kunnskap og erfaring fra eksperter, er det en høy grad av åpenhet i applikasjonene.

> MERK: Designmønstre er industristandardtilnærmingen.
> 

En ting å være forsiktig med er overbruk av designmønstre. Å bruke dem overalt vil forårsake mye mer skade enn nytte. Når du bestemmer deg for om du skal bruke et designmønster eller ikke. Å skynde seg å inkludere mønstre og abstraksjoner uten behov er en rask vei til altfor komplekse applikasjoner.

> ADVARSEL: Hvis alt du har er en hammer, ser alt ut som en spiker.
> 

### Kategorier

Det er mange designmønstre som har blitt laget gjennom årene. De faller vanligvis innenfor en av tre kategorier:

- Kreasjonsmønstre
- Strukturelle mønstre
- Atferdsmønstre

> INFO: Det er en fjerde kategori, kalt J2EE Patterns, som omhandler front-end-relaterte problemer.
> 

Følgende underavsnitt gir en forklaring på kategoriene.

### Kreativt

Disse designmønstrene gir en måte å lage objekter på mens de skjuler opprettelseslogikken, i stedet for å instansiere objekter direkte ved hjelp av ny operatør. Økende fleksibilitet og gjenbruk av eksisterende kode.

### Strukturell

Disse mønstrene forklarer hvordan man setter sammen objekter og klasser til større strukturer samtidig som disse strukturene er fleksible og effektive.

**Atferdsmessig**

Disse mønstrene er opptatt av algoritmer og tildeling av ansvar og kommunikasjon mellom objekter.

Implementering av designmønstre er utenfor omfanget av dette kurset og bør tas opp på nytt senere når du har mer erfaring.

For å oppsummere, så denne leksjonen på hvorfor utviklere ønsker å lage systemer med god design og fordelene med det. Vanlige beregninger, nemlig kobling og kohesjon, ble dekket og kontekst gitt for hver. De SOLIDE prinsippene som bidrar til å forbedre disse beregningene ble også diskutert med et eksempel på hver gitt og hva hvert prinsipp viser til bordet. Til slutt ble mindre vanlige prinsipper å ha i bakhodet dekket, nemlig; DRY, KISS og YAGNI.

To svært viktige konsepter ble introdusert i denne leksjonen og spiller en viktig rolle for å forstå designmønstre:

**- Foretrekk komposisjon fremfor arv.**
**- Program til abstraksjoner og ikke implementeringer.**

Denne leksjonen var en teoretisk titt på prinsippene bak god objektorientert design. Du vil begynne å legge merke til mønstre i bedriftsløsninger eller rammeverk som følger disse prinsippene etter hvert som din selvtillit vokser med praksis.

> INFO: For de ukjente eller nyere elevene kan denne leksjonen ha føltes litt overveldende og det er en usikkerhet i alt du skriver nå. Ikke bekymre deg, det blir mye lettere med tiden.
> 

---

Copyright 2021, Noroff Accelerate AS