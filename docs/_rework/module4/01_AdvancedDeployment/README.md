# Avansert Deployment

# Avansert Deployment

I denne leksjonen vil du lære om å lage fleksible konfigurasjoner for å distribuere til forskjellige miljøer. Du vil også lære hvordan du oppretter en full CI/CD-pipeline gjennom Gitlab for å lage et Docker-bilde av applikasjonen din og distribuere det bildet til en Heroku-applikasjon.

## Implementeringsmiljøer

Når programvare utvikles, går den gjennom sykluser og stadier. Disse syklusene og stadiene utgjør det som er kjent som Software Development Life Cycle (SDLC). Det er vanligvis delt opp i flere trinn, disse trinnene varierer, men er vanligvis:

1. Planlegging
2. Krav
3. Design
4. Bygge
5. Dokument
6. Test
7. Utplassere
8. Vedlikeholde

Hvert av disse stadiene krever involvering av ulike parter og interessenter. Denne syklusen ser også litt annerledes ut når den er i en smidig kontekst, men den underliggende prosessen er den samme.

Når vi undersøker denne prosessen kan vi trekke ut flere stadier som er knyttet til faktisk kode. Disse er:

- bygge
- test
- utplassere

Disse tre stadiene, sett på fra perspektivet til koding, involverer:

- skrive kode for å møte funksjonalitet
- å bekrefte at koden er riktig
- publisere applikasjonen til en skyserver

Disse prosessene resulterer i *distribusjonsmiljøer.* De definerer konfigurasjonen av applikasjonen for å oppnå ønsket mål. Det er mange forskjellige miljøer et selskap kan bruke, noen vanlige er:

- *utvikling* (dev) - faktisk utvikling på en utviklers lokale maskin
- *funksjon* – nye funksjoner lagt til i en allerede distribuert applikasjon ( ligner på dev)
- *hurtigreparasjon* - små feilrettinger som ikke er i samme skala som en dev/funksjonsutgivelse
- *test* - verifisering av funksjonalitet
- *iscenesettelse* - foreløpig skymiljø for å sikre at alle bevegelige deler fungerer etter hensikten
- *produksjon* (prod) - endelig, klientvendt distribusjon

Hver av disse har forskjellige formål og trenger spesifikk konfigurasjon. For våre formål vil vi diskutere tre miljøer:

**Utvikling**

Dette er det opprinnelige miljøet utvikleren jobber med. Her er all grunnkoden skrevet og eventuelle oppdateringer kan gjøres. Dette miljøet er vanligvis konfigurert slik at alle utviklerne som jobber med det har tilgang til den samme konfigurasjonen.

Ved å bruke våre tidligere eksempler vil et *utviklingsmiljø* bli konfigurert slik:

- Lokal PostgreSQL-database (enten installert eller gjennom Docker)
- Hibernate konfigurert med `ddl -auto= create`
- `spring.sql.init .mode = alltid`for å bruke `data.sql`

Det er dette vi har jobbet med så langt, det er designet for å replikeres og la Hibernate diktere strukturen.

En av funksjonene er utviklet, testet lokalt ved å kjøre og observere utdata. Tester skrives deretter ved hjelp av deterministiske data ( init + data.sql ), vi kan lage et testmiljø.

**Testing**

Når funksjonen er utviklet og noen enhets-/integrasjonstester er skrevet, må vi bruke en engangsmåte for å kjøre testene isolert. Vi kan gjenbruke vår lokale postgres- database, men testene vil bli kjørt på en server (GitLab-repository) som ikke har tilgang til databasen. Det finnes måter å omgå dette på, men den vanligste tilnærmingen er å bruke en innebygd database. Den som skal brukes for Spring Development er en `H2-`database.

Vårt miljø vil som sådan være som følger:

- Innebygd H2-database
- Hibernate konfigurert med `ddl -auto= create-drop`
- `data.sql`

Det er ikke nødvendig å konfigurere `spring.sql.init .mode`da det `alltid er`standard for innebygde databaser. Denne konfigurasjonen lar databasen opprettes, fylles ut med data, få tester til å kjøre med dataene og deretter ødelegges. Det hele gjøres internt i applikasjonen og krever ingen eksterne ressurser.

Når testene har bestått og applikasjonen er bekreftet til å fungere etter hensikten, er neste trinn å publisere applikasjonen til skyen.

**Produksjon**

Vi kombinerer *iscenesettelse* og *produksjon* for enkelhets skyld. Staging-miljøet er ofte begrenset til et lite antall utvalgte brukere, og det er derfor begrenset til visse IP-er og utviklerteam. Hensikten med iscenesettelsen er å teste på et nesten produksjonsnivå, men i et ikke-produksjonsmiljø for å verifisere at applikasjonen vil oppføre seg riktig etter distribusjon.

> **MERK** : I virkeligheten er iscenesettelse og produksjon atskilt ettersom det er testverktøy som kjøres på iscenesettelse. I vårt omfang bruker vi ikke disse verktøyene og dermed er det nesten ingen forskjell mellom de to miljøene.
> 

Når vi er på produksjonsstadiet, ønsker vi å distribuere applikasjonen vår til en skyplattform og få tilgang til den av publikum. Dette innebærer også at databasen flyttes og fylles ut med startdata. Denne databasen skal ikke endres når den er opprettet og skal inneholde de daglige aktivitetene til systemet. Sikkerhetskopier kan konfigureres på skyplattformen som er vert for databasen.

Vårt miljø vil som sådan være som følger:

- Distribuert PostgreSQL-database
- Hibernate konfigurert med `ddl -auto= ingen`
- `spring.sql.init .modus = aldri`

> **MERK** : Denne konfigurasjonen forutsetter at databasen er riktig konfigurert med de dataene som trengs.
> 

Produksjonen rulles også ofte ut til større og større brukergrupper samtidig. Dette hjelper til med å fange opp alt som er gått glipp av de forrige stadiene, eller et uforutsett ytelsesproblem.

## Spring Ekstern konfigurasjon

Catering for disse forskjellige distribusjonene kan bli en kjedelig oppgave hvis den ikke gjøres riktig. Et godt eksempel på dette er å ha en enkelt konfigurasjonsfil med statisk konfigurasjon. Dette vil føre til problemer så snart andre utviklere jobber med prosjektet, eller applikasjonen må flyttes til et annet miljø. Dette vil kreve konstant redigering av databasekonfigurasjoner og passord.

En løsning på dette er å få konfigurasjonen til å kunne endres. Heldigvis tilbyr Spring og Docker mange løsninger på dette problemet. Følgende er aktuelle måter å ha forskjellige miljøkonfigurasjoner på:

- Flere `application.properties`filer koblet til spesifikke *profiler*
- Flere dedikerte konfigurasjoner *Bønner* knyttet til spesifikke *profiler*
- Miljøvariabler med standardverdier i `application.properties`levert av en `.env`fil eller *Docker*

Valget av hvilken tilnærming er helt opp til utvikler og bedrift. Imidlertid presser bruken av spesifikke teknologier valget i en bestemt retning. De følgende delene vil illustrere hver tilnærming individuelt og avsluttes med en anbefaling basert på teknologistabelen som brukes i dette kurset.

> **RESSURSER**: Flere eiendomsfiler, [ref](https://docs.spring.io/spring-boot/docs/1.2.0.M1/reference/html/boot-features-external-config.html#boot-features-external-config-profile-specific-properties)
> 

> **RESSURSER**: Fjærprofiler, [ref](https://docs.spring.io/spring-boot/docs/1.2.0.M1/reference/html/boot-features-profiles.html)
> 

### Flere konfigurasjoner

Spring lar deg konfigurere flere `application.properties`-filer som lastes inn basert på en annen Spring-profil.

> **DOKUMENTASJON** : Spring dokumenter for profilspesifikke egenskaper
> 

Vi kan da levere en profil på kjøretid med flagget `- Dspring.profiles.active`(gjennom vår Dockerfile eller direkte). Vi kan også spesifisere en aktiv profil ved å redigere kjøringskonfigurasjonen vår i Intellij direkte, bildet nedenfor illustrerer dette:

![Untitled](Avansert%20Deployment%20d3fc652777c74d8fb8dd50e4a24eb9c2/Untitled.png)

![Untitled](Avansert%20Deployment%20d3fc652777c74d8fb8dd50e4a24eb9c2/Untitled%201.png)

For å imøtekomme de forskjellige miljøene, lager vi ulike egenskapsfiler:

- `application-dev.properties`
- `application-test.properties`
- `application-prod.properties`

Hver av disse filene vises nedenfor:

```
# application-dev.properties

# Datasource configuration
spring.datasource.url= jdbc:postgresql://localhost:5432/EnvironmentDemo
spring.datasource.username= postgres
spring.datasource.password= postgres

# Hibernate behaviour
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.PostgreSQL95Dialect
spring.jpa.hibernate.ddl-auto=create

#Turn Statistics on
logging.level.org.hibernate.stat= debug
# Enable logging of SQL
spring.jpa.show-sql=true

# Enable seeding
spring.sql.init.platform= postgres
spring.jpa.defer-datasource-initialization= true
spring.sql.init.mode= always

# Swagger config
springdoc.swagger-ui.operationsSorter=method
```

Dette er vårt standard utviklingsmiljø hvor vi jobber lokalt og vil ha alt logget. Det neste miljøet å se på er for testing:

```
# application-test.properties

# Datasource configuration
spring.datasource.url=jdbc:h2:mem:testdb
spring.datasource.driverClassName=org.h2.Driver
spring.datasource.username=sa
spring.datasource.password=sa

# Hibernate behaviour
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.H2Dialect
spring.jpa.hibernate.ddl-auto=create-drop

#Turn Statistics on
logging.level.org.hibernate.stat= debug

# Enable logging of SQL
spring.jpa.show-sql=true

# Let Hibernate decide schema, loads data.sql automatically
spring.jpa.database-platform=org.hibernate.dialect.H2Dialect
spring.jpa.defer-datasource-initialization= true
```

Her ønsker vi å konfigurere en innebygd H2-database slik at applikasjonen vår kan testes uten eksterne avhengigheter og vi har full kontroll over dataene. Her ber vi også hibernate om å slette databasen når testene er fullført. Til slutt har vi produksjonsmiljøet:

```
# application-prod.properties

# Datasource configuration
spring.datasource.url= jdbc:postgresql://ec2-52-212-228-71.eu-west-1.compute.amazonaws.com:5432/da9qhj0f6h6ruk
spring.datasource.username= wgkrmlcxrsqdfc
spring.datasource.password= b2b5af97459911ef0bb39ef72282ef32e0b3c86ebbf851cde101b9e91745bd80

# Hibernate behaviour
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.PostgreSQL95Dialect
spring.jpa.hibernate.ddl-auto=none

#Turn Statistics on
logging.level.org.hibernate.stat= debug

# Swagger config
springdoc.swagger-ui.operationsSorter=method
```

Her ønsker vi å koble til vår publiserte database og også slå av generert SQL-logging, da det vil rote loggene våre. Vi beholder feilsøkingsloggene slik vi ønsker å se når noe går galt. Vi tillater heller ikke Hibernate å endre databaseskjemaet , eller fylle det ut gjennom `data.sql`. Husk at en produksjonsdatabase allerede er konfigurert og har data. Dette kan være fra brukeraktivitet, eller en migrering, eller et innledende oppsett gjennom skript (eller til og med dvalemodus).

Du vil legge merke til at vi har avslørt databaselegitimasjonen vår i produksjonskonfigurasjonen. Dette er en sårbarhet ettersom noen med tilgang til kildekoden vår lett kan finne denne informasjonen. Løsningen er å bruke miljøvariabler, noe som er dekket i en dedikert del.

> **DOKUMENTASJON** : Ytterligere Spring ressurser om profiler: [1](https://docs.spring.io/spring-boot/docs/1.2.0.M1/reference/html/boot-features-profiles.html) , [2](https://docs.spring.io/spring-boot/docs/1.2.0.M1/reference/html/howto-properties-and-configuration.html#howto-change-configuration-depending-on-the-environment) , [3](https://docs.spring.io/spring-boot/docs/1.2.0.M1/reference/html/boot-features-external-config.html#boot-features-external-config-profile-specific-properties)
> 

### Bean-basert konfig

Spring lar deg definere klasser som er ansvarlige for konfigurasjonen. En veldig vanlig bruk av dette er å definere datakilder. Vi kan også koble hver klasse som skal lastes med en spesifikk profil.

Dette gjøres slik at datakilden ikke trenger å konfigureres i egenskapsfilen. Å konfigurere en datakilde i en egenskapsfil OG i en Bean er **overflødig** . En Bean kan imidlertid brukes til å overskrive en bekreftelse i `application.properties`.

> **MERK** : Å koble en Bean til en bestemt profil er ikke begrenset til bare konfigurasjonsbeans . Enhver Bean kan konfigureres til å laste for spesifikke profiler med de samme merknadene. Dette gjør falske avhengigheter for tester langt enklere.
> 

For dette eksemplet, la oss definere tre konfigurasjonsklasser, en for hver profildatakilde:

```java
@Configuration
@Profile("dev")
public class DatabaseTestConfig {
    @Bean
    public DataSource getDataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName("org.postgresql.Driver");
        dataSourceBuilder.url("jdbc:postgresql://localhost:5432/EnvironmentDemo");
        dataSourceBuilder.username("postgres");
        dataSourceBuilder.password("postgres");
        return dataSourceBuilder.build();
    }
}
```

```java
@Configuration
@Profile("test")
public class DatabaseTestConfig {
    @Bean
    public DataSource getDataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName("org.h2.Driver");
        dataSourceBuilder.url("jdbc:h2:mem:test");
        dataSourceBuilder.username("sa");
        dataSourceBuilder.password("sa");
        return dataSourceBuilder.build();
    }
}
```

```java
@Configuration
@Profile("prod")
public class DatabaseProdConfig {
    @Bean
    public DataSource getDataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName("org.postgresql.Driver");
        dataSourceBuilder.url("jdbc:postgresql://ec2-52-212-228-71.eu-west-1.compute.amazonaws.com:5432/da9qhj0f6h6ruk");
        dataSourceBuilder.username("wgkrmlcxrsqdfc");
        dataSourceBuilder.password("b2b5af97459911ef0bb39ef72282ef32e0b3c86ebbf851cde101b9e91745bd80");
        return dataSourceBuilder.build();
    }
}
```

Vi kan injisere miljøvariabler i disse klassene ved å bruke `@Value("#{systemEnvironment['ENV_VAR']}")`for å unngå å avsløre legitimasjonen vår for produksjon. Den følgende delen viser bruken av miljøvariabler for konfigurasjon.

### Miljøbasert konfig

Miljøvariabler er verdier som sendes normalt under kjøretid, noe som betyr at de ikke er en del av koden i hvile. Dette kan komme fra mange kilder: `.env-`filer, systemvariabler, injisert av en sky levert, eller fra en Dockerfil for å nevne noen.

Vi har allerede jobbet med Docker, så vi vil sende variablene våre gjennom det. Dette betyr at vi må konfigurere applikasjonen vår til å ha en spesifikk konfigurasjon med mindre annet er spesifisert.

For å gjøre dette fjerner vi profilkonfigurasjonen og bruker en enkelt `application.properties`-fil som er konfigurert til å bruke miljøvariabler eller en standardverdi.

> **MERK** : Du kan injisere miljøvariabler inn i bønner med @Value("#{systemEnvironment['ENV_VAR']}")som nevnt i forrige seksjon.
> 

Hvordan dette kan konfigureres i vår application.properties er med følgende syntaks: `${ ENV_VAR:default }`. For å illustrere dette, vurder følgende egenskapsfil:

```
# General config
spring.profiles.active=${SPRING_PROFILE:dev}
server.port=${PORT:8080}

# Datasource config
spring.datasource.url= ${DATABASE_URL:jdbc:postgresql://localhost:5432/EnvironmentDemo?user=postgres&password=postgres}

# Hibernate config
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.PostgreSQL95Dialect
spring.jpa.hibernate.ddl-auto=${DDL_MODE:create}

# Logging config
spring.jpa.show-sql= ${SHOW_JPA_SQL:true}
logging.level.org.hibernate.stat= debug

# Seeding config
spring.sql.init.platform= postgres
spring.jpa.defer-datasource-initialization= true
spring.sql.init.mode= ${INIT_MODE:always}

# Swagger config
springdoc.swagger-ui.operationsSorter=method
```

Et aspekt som gjøres annerledes her er `spring.datasource.url`som nå er konfigurert på én linje inkludert *brukernavn* og *passord* . Årsaken til dette er: for det første, det er én egenskap i stedet for tre, og for det andre injiserer skyleverandører ofte hele datakilden som en enkelt eiendom.

I denne konfigurasjonen gjør vi flere ting for å sette opp standardmiljøet vårt, `dev`. Vi angir den aktive profilen, gir datakildedetaljer og konfigurerer Hibernate med seeding. Dette vil være konfigurasjonen hvis ingenting leveres eksternt. Dette betyr at hvis vi bare kjører applikasjonen, er dette konfigurasjonen.

Husk at vi kan levere miljøvariabler på mange måter. De fleste av våre tilpassede env-variabler vil bli levert gjennom vår Dockerfile , disse er:

- `SPRING_PROFILE`
- `DDL_MODE`
- `SHOW_JPA_SQL`
- `INIT_MODE`

Vi trenger ikke å endre port da vi skal eksponere applikasjonen på standard `8080`. Vi definerer heller ikke `DATABASE_URL`i vår Dockerfile , da dette er injisert av Heroku.

Husk at vi bruker Heroku som vårt produksjonsmiljø. Den har en spesiell måte å gi databaseinformasjon på, via en miljøvariabel kalt `DATABASE_URL`.

> **DOKUMENTASJON** : [Relevant side](https://devcenter.heroku.com/changelog-items/438) om DATABASE_URL og Heroku.
> 

Ved å konsultere den medfølgende dokumentasjonen ser vi at URL-en er injisert i følgende format:

`postgres://{user}:{password}@{hostname}:{port}/{database-name}`

Vi trenger imidlertid at datakilden er i følgende format for å fungere med JDBC:

`jdbc:postgresql://{hostname}:{port}/{database-name}&{user}&{password}`

Årsaken til denne uoverensstemmelsen er veldig enkel, Heroku er generalisert og applikasjonen vår bruker *JDBC* . Dette vil resultere i noen forskjeller. Vi kan enkelt fikse dette ved å lage en konfigurasjonsklasse som kun laster inn i `prod -`profilen. Vi vet at datakilden er en Heroku-database, og vi vet hvordan inndataformatet blir. Vi kan konfigurere vår egen datakilde ved å trekke ut informasjon om den injiserte URL-en og lage vår egen datakilde .

> **MERK** : På våren har @Configuration-bønner forrang over application.properties.
> 

> **DOKUMENTASJON** : Oversettelse av Heroku- datakilden til Java vises [her](https://devcenter.heroku.com/articles/connecting-to-relational-databases-on-heroku-with-java#using-the-database_url-in-spring-with-java-configuration) .
> 

Fra dokumentasjonen ovenfor vil vi implementere “bruke DATABASE_URL direkte” og tilpasse den til å bruke det vi har tidligere, en `DataSource`(dette hjelper bare å unngå en annen avhengighet).

```java
@Configuration
@Profile("prod")
public class ProdDatabaseConfig {
    @Bean
    public DataSource dataSource() throws URISyntaxException {
        URI dbUri = new URI(System.getenv("DATABASE_URL"));
        String username = dbUri.getUserInfo().split(":")[0];
        String password = dbUri.getUserInfo().split(":")[1];
        String dbUrl = "jdbc:postgresql://" + dbUri.getHost() + ':' + dbUri.getPort() + dbUri.getPath();
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.url(dbUrl);
        dataSourceBuilder.username(username);
        dataSourceBuilder.password(password);
        return dataSourceBuilder.build();
    }
}
```

Til slutt, for å publisere til Heroku, er Dockerfilen vår konfigurert som:

```
FROM gradle:jdk17 AS gradle
WORKDIR /app
COPY . .
RUN gradle bootJar

FROM openjdk:17 as runtime
WORKDIR /app

ENV SPRING_PROFILE "prod"
ENV DDL_AUTO "none"
ENV INIT_MODE "never"
ENV SHOW_JPA_SQL "false"

COPY --from=gradle /app/build/libs/*.jar /app/app.jar
RUN chown -R 1000:1000 /app
USER 1000:1000
ENTRYPOINT ["java", "-jar", "app.jar"]
```

> **MERK** : Dette er veldig likt Dockerfilen sett i Docker - leksjonen, den har bare noen ekstra miljøkonfigurasjoner.
> 

Når dette er konfigurert, kan vi sende til Heroku:

```bash
heroku login
heroku container: login
heroku container:push web --app app-name
heroku container:release web --app app-name
```

Hvis vi inspiserer loggene til den publiserte applikasjonen, ser vi at `prod -`profilen er aktiv og vi er i stand til å samhandle med databasen på Heroku.

Viktigere, på grunn av Hobby-tier-databaser, vil dataene bli tilbakestilt ganske ofte av Heroku. Ha dette i bakhodet. En mulig løsning for dette er å la `INIT_MODE`fylle databasen hver gang applikasjonen publiseres på nytt. I virkeligheten ville en betalt tjeneste ikke tilbakestilles. Heroku endrer også databaselegitimasjonen med noen få dagers mellomrom, dette påvirker ikke applikasjonen vår, da konfigurasjonen injiseres av Heroku.

### Anbefaling

Etter å ha sett flere alternativer, er det en foretrukket måte å håndtere miljøkonfigurasjoner på i vår kontekst. Det vil si: *bruke miljøvariabler og Docker* .

Docker gir fleksibilitet og muligheten til enkelt å gi kjøretidskonfigurasjon. Heroku injiserer `DATABASE_URL`automatisk, vi trenger bare å skrive en produksjonskonfigurasjon for å oversette den. Denne tilnærmingen tilbyr den mest effektive måten å håndtere forskjellige konfigurasjoner på. Vi trenger også bare å lage konfigurasjonsklasser for å utvide kompatibiliteten vår - de fungerer som *dekoratorer* for konfigurasjonen vår.

Det neste trinnet herfra er et spørsmål om automatisering.

## CI/CD pipeline med Gitlab

I kurset så langt har vi sett noen metoder for å implementere CI. Dette er: automatisert testing og kjørbar oppretting. Disse er kjent som byggeartefakter - da de er et resultat av en eller annen prosess. Vi har også dekket Docker og sett hvordan vi kan bygge uforanderlige bilder som øyeblikksbilder av hele miljøet vårt som bare kan distribueres med en enkelt kommando til en plattform som Heroku.

Mens Heroku har sine egne pipelines, vil vi holde oss til Gitlab for å lage vår fulle CI/CD-pipeline. For å oppfylle ønsket funksjonalitet må vi gjøre to ting:

- bygg Docker-bildet av applikasjonen vår og lagre den i Gitlab-repositories *Container Registry*
- publiser det lagrede Docker-bildet til en Heroku-applikasjon

Dette betyr at vi ikke kan påkalle oss  `heroku container:push`siden det utsetter alle stadier til Heroku, vi gjør det separat i Gitlab. Dette er for å enten kunne omdistribuere automatisk etter hvert trykk, eller ha en “ett klikk”-distribusjon vi kan trigge fra vårt Gitlab-lager – og unngå å måtte logge på Heroku CLI.

### Bygg artefakter

Den første delen er å bygge og lagre Docker-bildet i vårt Gitlab-lager, dette gjøres ved å tilpasse [Docker in Docker-malen](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Docker.gitlab-ci.yml) fra Gitlab. Vår `gitlab-ci.yml`vil inneholde følgende:

```
# More readable naming and defining the Heroku app name
variables:
  # Tag it with commit reference
  CONTAINER_TEST_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
  CONTAINER_RELEASE_IMAGE: $CI_REGISTRY_IMAGE:latest
  HEROKU_APP: acc-fake-prod

docker-build:
  # Use the official docker image.
  image: docker:latest
  stage: build
  services:
    - docker:dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    # We build the image ID into a text file
    - docker build --iidfile imageid.txt -t $CONTAINER_TEST_IMAGE .
    - docker tag $CONTAINER_TEST_IMAGE $CONTAINER_RELEASE_IMAGE
    - docker push -a $CI_REGISTRY_IMAGE
  # Save the Docker image id in a file - will be used later for Heroku
  artifacts:
    paths:
      - imageid.txt
  # Run this job in a branch where a Dockerfile exists
  rules:
    - if: $CI_COMMIT_BRANCH
      exists:
        - Dockerfile
    # Only run this on a protected branch:
    # https://docs.gitlab.com/ee/user/project/protected_branches.html
    - if: '$CI_COMMIT_REF_PROTECTED == "true"'
```

Det er mange aspekter å diskutere når man ser på denne konfigurasjonen. Det mest åpenbare aspektet er alle variablene:

- `$CI_REGISTRY`
- `$CI_REGISTRY_USER`
- `$CI_REGISTRY_PASSWORD`
- `$CI_REGISTRY_IMAGE`
- `$CI_COMMIT_REF_NAME`
- `$CI_COMMIT_BRANCH`
- `$CI_COMMIT_REF_PROTECTED`

Disse er alle miljøvariabler som brukes til å konfigurere byggestadiet for forskjellige depoter. Husk at hvert depot har sitt eget containerregister som vi kan presse til.

Det neste spørsmålet er: *hvor kommer disse variablene fra?* Gitlab injiserer dem automatisk. Det er mange variabler som kan brukes, disse er bare for å få tilgang til det private containerregisteret og navngi bildet vårt.

> **DOKUMENTASJON** : En uttømmende liste over CICD-variabler her .
> 

Det neste aspektet å vurdere er denne `iidfilen`som er inkludert i `docker build -`kommandoen. Dette er ganske enkelt et byggeargument som gjør at ID-en til bildet kan lagres i en fil. Dette er noe vi vil bruke for å fortelle Heroku hvilket bilde vi vil at det skal bruke.

> **DOKUMENTASJON** : Docker build - kommandoliste , inkludert iidfile.
> 

De andre konfigurasjonene er: sørge for at vi har en Dockerfile på grenen, og at grenen er beskyttet (har lese-skriverettigheter). Vår hovedgren er beskyttet som standard.

> **MERK** : Vi kan bygge Docker-bildet vårt i Heroku-registeret. Vi valgte å ikke gjøre det, da dette knytter oss til Heroku. Å ha bildet vårt på Gitlab er en separasjon som gir fleksibilitet, vi trenger kun å endre distribusjonsstadiet for å endre distribusjoner, mens vi trenger å skrive om hele pipelinen fullstendig hvis vi også lagret bildet på Heroku .
> 

Hvis vi inspiserer containerregisteret på vårt depot , vil vi se følgende:

![Untitled](Avansert%20Deployment%20d3fc652777c74d8fb8dd50e4a24eb9c2/Untitled%202.png)

Her kan vi se enkeltbildet med to tagger, `hoved`og `siste`. Dette er bare for å ha et spor av hver gren som påvirket dette bildet, samt en tagg som brukes til distribusjon.

Hvis vi inspiserer vår `image.txt-`artefakt som inneholder bilde- IDen , finner vi følgende verdi:

```
sha256:8ffc3ed2539ed857bb92b52e98598c5d641bd3542f6d0098b77beef5d55b40c7
```

Dette er referansen til Docker-bildet vårt, som vi trenger for distribusjonen vår. Det neste trinnet er å ta Docker-bildet vårt fra Gitlab og sende det til Heroku for å oppdatere og omdistribuere applikasjonen.

### Deployment

For å distribuere til Heroku uten at bildet er vert på Heroku, krever følgende trinn:

1. Aktiverer krølleforespørsler i vår pipeline, slik at vi kan sende bildet til Heroku
2. Logge inn på vårt private containerregister på Gitlab
3. Trekker Docker-bildet vårt og merker det for Heroku-distribusjon
4. Skyver bildet vårt til containerregisteret på Heroku for appen vår
5. Oppdaterer bildet som brukes for øyeblikket for løpeappen vår. Dette vil distribuere bildet på nytt med det oppdaterte.

Nok en gang kunne vi ha bygget Docker-bildet på dette stadiet og pushet det direkte til Heroku. Dette fjerner imidlertid fleksibiliteten vi har når bildet lagres i vårt private register. Vi kan ganske enkelt sende kopier til hvilken distribusjonsplattform vi ønsker.

Trinn 4 og 5 virker veldig like, men de er viktigst forskjellige. Trinn 4 er ganske enkelt å skyve det nye bildet vårt til Heroku-applikasjonsregisteret. Appen bruker en kjørende beholder basert på det gamle registeret fortsatt. Trinn 5 er der vi forteller Heroku at det er et nytt bilde som skal brukes for nettimplementeringen, noe som får det til å omplassere.

For å logge inn på Heroku og bruke API, trenger vi en `HEROKU_API_KEY`. Dette er et autentiseringstoken som lar oss få tilgang til den distribuerte applikasjonen. Dette tokenet kan hentes ved å bruke CLI-verktøyene. Åpne en terminal og deretter:

```bash
heroku login
heroku auth:token # lasts 1 month
```

Du kopierer deretter tokenet som er gitt, og går deretter inn i Gitlab-depotet ditt: `Innstillinger > CICD > Variabler > Legg til variabel`og lag en variabel kalt `HEROKU_API_KEY`og lim inn tokenet som dets verdi som er beskyttet og maskert. Nå er tokenet tilgjengelig for deg i konfigurasjonen din.

Bildet nedenfor viser den nylig lagt til variabelen i vårt depot:

![Untitled](Avansert%20Deployment%20d3fc652777c74d8fb8dd50e4a24eb9c2/Untitled%203.png)

Pipeline-variabel

> **DOKUMENTASJON** : Heroku [API-referanse](https://devcenter.heroku.com/articles/platform-api-reference) .
> 

Disse trinnene resulterer i følgende trinn:

```
heroku:
  stage: deploy
  image: docker:latest
  needs:
    - docker-build
  services:
    - docker:dind
  rules:
    - if: '$CI_COMMIT_REF_PROTECTED == "true"'
  # Manually triggered in Gitlab
  when: manual
  before_script:
    - docker info
    # Allow curl requests to be made in the pipeline
    - apk add --no-cache curl
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker login --username=_ --password=${HEROKU_API_KEY} registry.heroku.com
    - docker pull $CONTAINER_RELEASE_IMAGE
    # Push our image from Gitlab to Heroku's registry with a specific tag
    - docker tag $CONTAINER_RELEASE_IMAGE registry.heroku.com/$HEROKU_APP/web:latest
    - docker push registry.heroku.com/$HEROKU_APP/web:latest
    # Update the deployment to make Heroku redeploy
    - |-
      curl -X PATCH https://api.heroku.com/apps/$HEROKU_APP/formation 
      --header "Content-Type: application/json" 
      --header "Accept: application/vnd.heroku+json; version=3.docker-releases" 
      --header "Authorization: Bearer ${HEROKU_API_KEY}" 
      --data '{ "updates": [ { "type": "web", "docker_image": "'$(cat imageid.txt)'" } ] }'
```

Hvis vi nå skyver en endring inn i hovedgrenen vår, vil vi få følgende rørledning utløst:

![Untitled](Avansert%20Deployment%20d3fc652777c74d8fb8dd50e4a24eb9c2/Untitled%204.png)

Her kan vi se byggejobben vår er fullført, og vi kan utløse distribusjonen vår ved å trykke på play.

> **MERK** : Vi kan få dette gjort automatisk ved å utelate when:maunali ci-konfigurasjonen vår.
> 

Når vi inspiserer distribusjonsjobben, ser vi at oppdateringsforespørselen som sendes til Heroku har følgende verdi:

```json
{
  "app": {
    "id": "d6f3ae29-56a4-40d8-a098-5996d5cec398",
    "name": "acc-fake-prod"
  },
  "command": "java -jar app.jar",
  "created_at": "",
  "id": "0ddb2b0c-eecb-4b4f-be45-d845b541df80",
  "type": "web",
  "quantity": 1,
  "size": "Free",
  "updated_at": "",
  "docker_image": {
    "id": "310e7a06-18ac-492d-9742-f0f69da28a73",
    "image_ref": null
  }
}
```

Det er dette som utløser Heroku til å omdistribuere applikasjonen vår.

For referanse er dette hele gitlab -ci-filen:

```
variables:
  CONTAINER_TEST_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
  CONTAINER_RELEASE_IMAGE: $CI_REGISTRY_IMAGE:latest
  HEROKU_APP: acc-fake-prod

docker-build:
  image: docker:latest
  stage: build
  services:
    - docker:dind
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
  script:
    - docker build --iidfile imageid.txt -t $CONTAINER_TEST_IMAGE .
    - docker tag $CONTAINER_TEST_IMAGE $CONTAINER_RELEASE_IMAGE
    - docker push -a $CI_REGISTRY_IMAGE
  artifacts:
    paths:
      - imageid.txt
  rules:
    - if: $CI_COMMIT_BRANCH
      exists:
        - Dockerfile
    - if: '$CI_COMMIT_REF_PROTECTED == "true"'

heroku:
  stage: deploy
  image: docker:latest
  needs:
    - docker-build
  services:
    - docker:dind
  rules:
    - if: '$CI_COMMIT_REF_PROTECTED == "true"'
  when: manual
  before_script:
    - docker info
    - apk add --no-cache curl
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker login --username=_ --password=${HEROKU_API_KEY} registry.heroku.com
    - docker pull $CONTAINER_RELEASE_IMAGE
    - docker tag $CONTAINER_RELEASE_IMAGE registry.heroku.com/$HEROKU_APP/web:latest
    - docker push registry.heroku.com/$HEROKU_APP/web:latest
    - |-
      curl -X PATCH https://api.heroku.com/apps/$HEROKU_APP/formation --header "Content-Type: application/json" --header "Accept: application/vnd.heroku+json; version=3.docker-releases" --header "Authorization: Bearer ${HEROKU_API_KEY}" --data '{ "updates": [ { "type": "web", "docker_image": "'$(cat imageid.txt)'" } ] }'
```

> **RESSURS** : Repoet som ble brukt til denne leksjonen finner du [her](https://gitlab.com/NicholasLennox/heroku-cicd-docker) .
> 

Copyright 2022, Noroff Accelerate AS