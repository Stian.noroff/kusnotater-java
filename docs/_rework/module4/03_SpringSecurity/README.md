# 3. Spring Security

# Spring Security

## Introduksjon

[Spring Security](https://docs.spring.io/spring-security/site/docs/5.0.0.RELEASE/reference/htmlsingle/) er et kraftig og svært tilpassbart tillegg for autentisering og tilgangskontroll innebygd i Spring Framework.

Det har:

- Omfattende og utvidbar støtte for både autentisering og autorisasjon
- Beskyttelse mot angrep som øktfiksering, clickjacking, forfalskning av forespørsler på tvers av nettsteder, etc
- Servlet API-integrasjon
- Valgfri integrasjon med Spring Web MVC

og mye mer.

Spring Security settes opp gjennom `@Configuration`-klasser. I disse klassene kan du spesifisere hvilke endepunkter som må beskyttes og hva som må skje hvis autentiseringen mislykkes.

> MERK : Med beskyttet mener vi at brukeren er pålogget og har rettigheter til å se ressursene.
> 

Spring Security er også sterkt integrert med token-basert autentisering og andre tredjepartsbiblioteker. Det er mange avhengigheter som gjør at Spring Security kan brukes i enhver sammenheng.

Husk at for våre formål ønsker vi å gjøre tilstandsløs sikkerhet ved å bruke JWT-er. Vi vil at Java-applikasjonen vår skal fungere som en ressursserver, noe som betyr at den skal holde ressursene våre og ikke være ansvarlig for identitetsadministrasjon. Det bør ganske enkelt validere de oppgitte tokenene med den konfigurerte leverandøren.

For å friske opp minnet ditt, skal vi dekke noen vanlige termer som trengs for å forstå Spring Security:

- **Autentisering:** prosess for å bekrefte identiteten til en bruker, basert på oppgitt legitimasjon. Et vanlig eksempel er å skrive inn et brukernavn og et passord når du logger inn på en nettside. Du kan tenke på det som et svar på spørsmålet *Hvem er du?* .
- **Autorisasjon:** prosess for å avgjøre om en bruker har riktig tillatelse til å utføre en bestemt handling eller lese bestemte data , forutsatt at brukeren er vellykket autentisert. Du kan tenke på det som et svar på spørsmålet *Kan en bruker gjøre/lese dette?* .
- **Prinsipp:** den for øyeblikket autentiserte brukeren.
- **Tildelt autoritet:** tillatelsen til den autentiserte brukeren.
- **Rolle eller krav:** en gruppe med tillatelser til den autentiserte brukeren. Normalt som et sett med *krav*.

## Standardkonfigurasjon

Spring Security har en standardkonfigurasjon som følger med. Den beskytter alle endepunkter som standard og har en stateful sikkerhetskonfigurasjon med økter og informasjonskapsler. Spring Security genererer også html-sider til serveren som: påloggings-, register- og feilsider.

For å se denne standardoppførselen, lag en applikasjon med Spring Security-avhengigheten fra Spring Initialzr .

```json
dependencies {
    implementation 'org.springframework.boot:spring-boot-starter-data-jpa'
    implementation 'org.springframework.boot:spring-boot-starter-security'
    implementation 'org.springframework.boot:spring-boot-starter-web'
    compileOnly 'org.projectlombok:lombok'
    runtimeOnly 'org.postgresql:postgresql'
    annotationProcessor 'org.projectlombok:lombok'
    testImplementation 'org.springframework.boot:spring-boot-starter-test'
    testImplementation 'org.springframework.security:spring-security-test'
}
```

Applikasjonen har ikke hatt noen ekstra konfigurasjon og har en enkelt kontroller:

```java
@Controller
@RequestMapping("api/v1/resources")
public class ResourceController {

    @GetMapping("public")
    public ResponseEntity getPublic() {
        ResponseMessage message = new ResponseMessage();
        message.setMessage("Public resources");
        return ResponseEntity.ok(message);
    }
}
```

Hvis vi navigerer til `localhost:8080/api/v1/resources/public`, ser vi følgende skjermbilde:

![Untitled](3%20Spring%20Security%2056f56297526a44e7a51fbb23d53ae123/Untitled.png)

Vi ble omdirigert til et `/login`-endepunkt og servert en visning som fungerer som en påloggingsskjerm. Dette er fordi vi ikke er autentisert i applikasjonen, noe som betyr at det ikke er noe `autentiseringsprinsipp`i Spring Securitys **sikkerhetskontekst** . Det er andre synspunkter som Spring Security kommer med, men vi ønsker ikke å bruke øktbasert sikkerhet, vi må rekonfigurere Spring Security til å bruke tilstandsløs sikkerhet og JWT-er.

### Sikkerhetsarkitektur

Før vi konfigurerer Spring Security, må vi forstå hva som skjer for å oppnå denne oppførselen.

### Filterkjeder

Når du legger til Spring Security i applikasjonen din, oppretter og registrerer den automatisk en *filterkjede* som en proxy. Denne filterkjeden fanger opp alle innkommende forespørsler og bestemmer hva som skal skje basert på de faktiske forespørselsdetaljene. Som navnet antyder, består en filterkjede av mange filtre som alle har individuelle ansvarsområder. Disse inkluderer, men er ikke begrenset til: sjekke offentlig tilgjengelighet for URL-er, sjekke om brukeren allerede er autentisert (øktbasert), og sjekke om brukeren har autorisasjon til å utføre handlingen.

Filterkjeden sitter mellom klienten og Http Servlet. Dette fungerer som et lag som fanger opp forespørsler som en gatekeeper før forespørsler sendes til kontrollere.

![Untitled](3%20Spring%20Security%2056f56297526a44e7a51fbb23d53ae123/Untitled%201.png)

Disse filtrene er i en bestemt rekkefølge, og du kan legge til dine egne tilpassede filtre foran dem (som betyr at filtrene dine vil være den første linjen) ved å fylle *Spring Security -* filtrene tilbake.

> MERK : Vi vil ikke lage vår egen filterkjede, vi bruker en avhengighet for dette. Det er imidlertid mulig å lage din egen.
> 

### AuthenticationManager

Du kan tenke på dette som en koordinator der du kan registrere flere *leverandører* , og basert på forespørselstypen vil den levere en autentiseringsforespørsel til riktig leverandør.

### AuthenticationProvider

Dette behandler spesifikke typer autentisering. Det er et grensesnitt, og det viser bare to funksjoner:

- `authenticate`utfører autentisering med forespørselen.
- `supports` kontrollerer om denne leverandøren støtter den angitte autentiseringstypen.

Det er mange flere aspekter ved Spring Securitys arkitektur, vi vil ikke dekke dem i denne leksjonen. Målet vårt er å konfigurere en OAuth2.0-ressursserver, som det er en avhengighet for som legger til all nødvendig implementering.

> RESSURSE : [Spring Security Architecture](https://spring.io/guides/topicals/spring-security-architecture)
> 

> RESSURSE : [Prøvelager](https://gitlab.com/NicholasLennox/spring-security-internal) med fullstendig manuell JWT-konfigurasjon. Bruker ikke en identitetsleverandør , men viser viktige arkitektoniske implementeringer for tilstandsløs sikkerhet.
> 

## OAuth2 ressursserver

For å oppnå ønsket konfigurasjon, må vi bruke OAuth2-ressursserveravhengigheten. Dette inkluderer en JWT-autentiseringsmekanisme, filterkjeder, autentiseringsleverandører og tillatelsesoversettelser. Det gjør en stor mengde konfigurasjon for oss.

På et høyt nivå håndterer OAuth2-avhengigheten JWT Bearer-autentisering på følgende måte:

![Untitled](3%20Spring%20Security%2056f56297526a44e7a51fbb23d53ae123/Untitled%202.png)

Med en nærmere titt på filterkjeden:

![Untitled](3%20Spring%20Security%2056f56297526a44e7a51fbb23d53ae123/Untitled%203.png)

> DOKUMENTASJON : [Oversikt over OAuth2 ressursserver.](https://docs.spring.io/spring-security/reference/servlet/oauth2/resource-server/index.html)
> 

Det er konfigurasjoner som må gjøres:

1. Konfigurer hvem som utstedte JWT-ene
2. Fortell Spring Security å bruke JWT-er ikke økter

> RESSURSE : [Spring MVC-integrasjon](https://docs.spring.io/spring-security/reference/servlet/integrations/mvc.html)
> 

> RESSURSE : [CORS](https://docs.spring.io/spring-security/reference/servlet/integrations/cors.html)
> 

### Konfigurasjon av egenskaper

Det er to egenskaper som Spring Security trenger for å dekode JWT-er og validere dem:

1. Hvem utstedte tokenet
2. Hvor er nøkkelsettet som ble brukt til å kode tokenet

Uten disse konfigurasjonene må du spesifisere en tilpasset JWT-dekoder. Husk at vi brukte **Keycloak** som vår identitetsleverandør, dette betyr at Keycloak utstedte JWT-ene våre fra vårt tilpassede *Noroff -* rike. Den kodet også JWT med en spesifikk nøkkel. For å fortelle Spring Security hva disse verdiene er, legger vi til disse to feltene i `application.properties`:

```java
spring.security.oauth2.resourceserver.jwt.issuer-uri= 
http://localhost:8083/auth/realms/noroff
spring.security.oauth2.resourceserver.jwt.jwk-set-uri= 
http://localhost:8083/auth/realms/noroff/protocol/openid-connect/certs
```

> MERK : Keycloak må distribueres på port 8083 og ha Noroff-riket konfigurert, ellers vil dette ikke peke noe sted .
> 

Hvis vi går til `jwk-set uri`-verdien, får vi følgende svar:

```json
{
  "keys": [
  {
    "kid": "9RE5U7kSqsY6Yq8bEw6ar_SGHaxERkvIBSaNIo-wqqU",
    "kty": "RSA",
    "alg": "RS256",
    "use": "enc",
    "n": "omitted",
    "e": "AQAB",
    "x5c": [
    "omitted"
    ],
    "x5t": "Ns46UKmBwbXxZ-tNnxt8KzjRxEA",
    "x5t#S256": "MKJpqOCKxWNpmtjadyvPuMTFfI9tjMOwQ-o-I0KdIWQ"
  },
  {
    "kid": "gBLtsAKZDAFZWx23SpHKNOy6BjhfSNv8eCOCOJs3424",
    "kty": "RSA",
    "alg": "RS256",
    "use": "sig",
    "n": "omitted",
    "e": "AQAB",
    "x5c": [
    "omitted"
    ],
    "x5t": "4wR13_6iBEZNVmnposDOFXCz8_k",
    "x5t#S256": "CIm1H6-vy-sG25fel0mpPfqeDnCdxslBtGwc5aughG8"
  }
  ]
}
```

Dette er en liste over nøklene som Keycloak- riket vårt kan signere tokens med, hvis en JWT har blitt signert med en av disse nøklene og `iss`-feltet samsvarer med verdien av `jwt.issuer-uri`så er tokenet gyldig og vi kan fortsette.

> DOKUMENTASJON : Mer informasjon om [JWT-dekoding](https://docs.spring.io/spring-security/reference/servlet/oauth2/resource-server/jwt.html#oauth2resourceserver-jwt-architecture)
> 

### Sikkerhetskonfigurasjon

For å kunne tilby tilpasset konfigurasjon, må vi levere en konfigurasjonsbean (på lignende måte da vi dekket distribusjon). Denne konfigurasjonen må merkes med `@EnableWebSecurity`for å sende `HttpSecurity`-konfigurasjonen til oss og tillate oss å lage en tilpasset filterkjede som Spring Security bruker i stedet for standard:

```java
@EnableWebSecurity
public class SecurityConfig {
  @Bean
  public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
    http
        .cors().and()
        // Sessions will not be used
        .sessionManagement().disable()
        // Disable CSRF -- not necessary when there are no sessions
        .csrf().disable()
        // Enable security for http requests
        .authorizeHttpRequests(authorize -> authorize
          // All endpoints are protected
          .anyRequest().authenticated()
        )
        .oauth2ResourceServer()
        .jwt();
    return http.build();
  }
}
```

> MERK : WebSecurityAdapter-varianten av denne konfigurasjonen er utdatert.
> 

Hvis vi nå besøker `localhost:8080/api/v1/resources/public`, vil vi motta bare et 401-svar, som ment. Hva om noen av våre endepunkter skulle være åpne for offentligheten, eller være begrenset til spesifikke *krav* eller *roller* ? For å gjøre dette, må vi tilpasse konfigurasjonen vår og inkludere noe som heter `mvcMatchers`:

```java
// Enable security for http requests
.authorizeHttpRequests(authorize -> authorize
    // Specify paths where public access is allowed
    .mvcMatchers("/api/v1/resources/public").permitAll()
    // All remaining paths require authentication
    .anyRequest().authenticated()
)
```

> MERK : `antMatchers` og `mvcMatchers` er i hovedsak de samme, `mvcMatchers` tillater bare bedre matching og er ikke den foretrukne metoden for matching.
> 

Kontrolleren vår har blitt oppdatert til å inkludere et andre endepunkt:

```java
@Controller
@RequestMapping("api/v1/resources")
public class ResourceController {

    @GetMapping("public")
    public ResponseEntity getPublic() {
        ResponseMessage message = new ResponseMessage();
        message.setMessage("Public resources");
        return ResponseEntity.ok(message);
    }

    @GetMapping("protected")
    public ResponseEntity getProtected() {
        ResponseMessage message = new ResponseMessage();
        message.setMessage("Protected resources");
        return ResponseEntity.ok(message);
    }
}
```

Hvis vi nå navigerer til `/api/v1/resources/public`, har vi tilgang til ressursene. Men hvis vi navigerer til `/api/v1/resources/protected`, blir vi fortsatt nektet. Alt fungerer etter hensikten.

> DOKUMENTASJON : Du kan lese mer om godkjenning av endepunkter [her](https://docs.spring.io/spring-security/reference/servlet/authorization/authorize-http-requests.html) .
> 

For å få tilgang til vårt beskyttede endepunkt, må vi levere en gyldig JWT fra Keycloak . Når vi logger på med frontenden fra forrige leksjon , legger vi ved tokenet som en `Autorisasjonshode`med verdien av `bærertoken`. Dette kan enkelt gjøres i *Postman*:

![Untitled](3%20Spring%20Security%2056f56297526a44e7a51fbb23d53ae123/Untitled%204.png)

Vi kan da få tilgang til de beskyttede ressursene. Det neste trinnet er å begrense tilgangen basert på våre tildelte myndigheter.

### Samarbeide med myndigheter

Husk at myndigheter i hovedsak er en liste over tillatelser en bruker har i applikasjonen vår . Som standard er de konfigurert fra *SCOPE* . Vi kan tilpasse dette og levere egne myndigheter, for eksempel *ROLE* .

Før vi går over til tilpasning, må vi først forstå hvordan de bevilgede myndighetene er laget. Som standard vil Spring Security og OAuth2 lese inn alle verdiene i *omfangskravet* , legge til `SCOPE_`til det, og deretter legge det til listen over godkjente myndigheter. Dette lar oss legge til tillatelseskrav i våre `autoriserteHttpRequests`ved å bruke scope:

```java
// Enable security for http requests
.authorizeHttpRequests(authorize -> authorize
  // Specify paths where public access is allowed
  .mvcMatchers("/api/v1/resources/public").permitAll()
  // Specify paths to be protected with scope
  .mvcMatchers("/api/v1/resources/scope").hasAuthority("SCOPE_profile")
  // All remaining paths require authentication
  .anyRequest().authenticated()
)
```

Her bruker vi `hasAuthority("SCOPE_profile")`-metoden for å spesifisere at en scope-verdi for `profilen`må eksistere i JWT. Scope er en måte å gi tilgangskontroll på, men den har også andre formål, en mer direkte tilgangskontrollimplementering er å bruke *roller* .

Roller er den foretrukne metoden for å utføre tilgangskontroll, siden roller kan eksistere separat for omfang og de tilbyr mer fleksibilitet i kartlegging og kategorisering av brukere. Å bruke roller innkapsler også tilgangskontroll vekk fra generelle brukertillatelser.

For å aktivere rollebasert tilgangskontroll trenger vi bare å legge rollene til de innvilgede myndighetene. Dette er fordi det allerede er en `.hasRole()`spesifikt gitt gjennom Spring Security:

```java
// Specify paths to be protected with role
.mvcMatchers("/api/v1/resources/roles").hasRole("ADMIN")
```

> MERK : Du kan legge til `@PreAuthorize("hasRole('ADMIN')")`på kontrollermetoden direkte i stedet hvis du ønsker det.
> 

Alt dette krever for å fungere er at de innvilgede myndighetene inneholder en autoritet med verdien `"ROLE_ADMIN"`- legg merke til prefikset ROLE_ som ikke er inkludert i `hasRole`-metoden.

Vi har allerede roller som kommer fra Keycloak i et krav kalt *roller* , som er en tilpasset kartlegging vi laget i forrige leksjon. Alt vi trenger å gjøre nå er å ta *rollene* kravet og enten: legge det til de innvilgede myndighetene, eller erstatte de eksisterende myndighetene med rollene.

### Erstatte myndigheter

Denne tilnærmingen benytter seg av `JwtAuthenticationConverter`som er ansvarlig for å gjøre en JWT om til et sett med autoriteter. Her leverer vi en bean metode for å ta *rollene* kravet, prefikser hver rolle med `ROLE_`og bruker de som tildelte myndigheter:

```java
@Bean
public JwtAuthenticationConverter jwtRoleAuthenticationConverter() {
    JwtGrantedAuthoritiesConverter grantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
    // Use roles claim as authorities
    grantedAuthoritiesConverter.setAuthoritiesClaimName("roles");
    // Add the ROLE_ prefix - for hasRole
    grantedAuthoritiesConverter.setAuthorityPrefix("ROLE_");

    JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter();
    jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(grantedAuthoritiesConverter);
    return jwtAuthenticationConverter;
}
```

Da kan vi bruke den i vår `filterkjede`:

```java
.oauth2ResourceServer(oauth2 -> oauth2
  .jwt(jwt -> jwt
    .jwtAuthenticationConverter(jwtRoleAuthenticationConverter())
  )
);
```

> DOKUMENTASJON : [Uttrekke myndigheter manuelt](https://docs.spring.io/spring-security/reference/servlet/oauth2/resource-server/jwt.html#oauth2resourceserver-jwt-authorization-extraction)
> 

Nå, hvis vi prøver access `/api/v1/resources/roles`uten `ADMIN`-rollen, får vi en 403. Den fullstendige koden for sikkerhetskonfigurasjonen kan sees nedenfor, den er også forkortet for enklere konfigurasjoner:

```java
@Bean
public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
  http
    // Enable CORS -- this is further configured on the controllers
    .cors().and()
    // Sessions will not be used
    .sessionManagement().disable()
    // Disable CSRF -- not necessary when there are no sessions
    .csrf().disable()
    // Enable security for http requests
    .authorizeHttpRequests(authorize -> authorize
      // Specify paths where public access is allowed
      .mvcMatchers("/api/v1/resources/public").permitAll()
      // Specify paths to be protected with scope
      .mvcMatchers("/api/v1/resources/scope").hasAuthority("SCOPE_profile")
      // Specify paths to be protected with role
      .mvcMatchers("/api/v1/resources/roles").hasRole("ADMIN")
      // All remaining paths require authentication
      .anyRequest().authenticated()
    )
    .oauth2ResourceServer()
    .jwt()
    .jwtAuthenticationConverter(jwtRoleAuthenticationConverter());
  return http.build();
}

@Bean
public JwtAuthenticationConverter jwtRoleAuthenticationConverter() {
  JwtGrantedAuthoritiesConverter grantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
  // Use roles claim as authorities
  grantedAuthoritiesConverter.setAuthoritiesClaimName("roles");
  // Add the ROLE_ prefix - for hasRole
  grantedAuthoritiesConverter.setAuthorityPrefix("ROLE_");

  JwtAuthenticationConverter jwtAuthenticationConverter = new JwtAuthenticationConverter();
  jwtAuthenticationConverter.setJwtGrantedAuthoritiesConverter(grantedAuthoritiesConverter);
  return jwtAuthenticationConverter;
}
```

Det er en annen metode der vi kan legge til flere krav til en forening av bevilgede myndigheter. Dette dekkes ikke i timen, men kan sees i denne [prøverepoen](https://gitlab.com/noroff-accelerate/java/projects/hibernate-with-ci/-/blob/master/src/main/java/no/qux/demo/hibernate/config/SecurityConfig.java#L43) .

> DOKUMENTASJON : [Konfigurere autorisasjon](https://docs.spring.io/spring-security/reference/servlet/oauth2/resource-server/jwt.html#oauth2resourceserver-jwt-authorization)
> 

> DOKUMENTASJON : [Godkjenner Http-forespørsler](https://docs.spring.io/spring-security/reference/servlet/authorization/authorize-http-requests.html)
> 

## Synkroniserer identitet

Det neste logiske trinnet i vår ressursserverkonfigurasjon er en metode for å synkronisere databasebrukerne med deres respektive identiteter. For eksempel, i systemet vårt, må brukerne våre fylle ut en `bio`for å ha en fullstendig profil:

```java
@Entity
@Table(name = "app_user")
@Getter
@Setter
public class AppUser {
    @Id
    private String uid;
    private String bio;
    private boolean complete;
}
```

Uid er `bruker`-ID. Det vil være vårt `subject` fra JWT, da det er hovednøkkelen som brukes i Keycloak . Andre tilbydere vil også ha et lignende felt, `sub`er et standardkrav.

Vårt mål er å bruke `sub` som vår brukers ID slik at vi kan koble identiteten vår til databasen vår. For å utforske dette, må vi finne en måte å få tilgang til JWT i kontrollerene våre. Dette gjøres via en `@AuthenticationPrinciple`-kommentar. Kontrolleren nedenfor illustrerer dette:

```java
@RestController
@RequestMapping("api/v1/users")
public class UserController {

    @GetMapping("info")
    public ResponseEntity getLoggedInUserInfo(@AuthenticationPrincipal Jwt principal) {
        Map<String, String> map = new HashMap<>();
        map.put("subject", principal.getClaimAsString("sub"));
        map.put("user_name", principal.getClaimAsString("preferred_username"));
        map.put("email", principal.getClaimAsString("email"));
        map.put("first_name", principal.getClaimAsString("given_name"));
        map.put("last_name", principal.getClaimAsString("family_name"));
        map.put("roles", String.valueOf(principal.getClaimAsStringList("roles")));
        return ResponseEntity.ok(map);
    }
```

Ved å kalle `api/v1/users/info`-endepunktet får vi følgende svar:

```json
{
    "subject": "c97e6618-d58d-4386-a27b-8eb49b534d89",
    "user_name": "nicholas.lennox@noroff.no",
    "roles": "[ADMIN]",
    "last_name": "Lennox",
    "first_name": "Nicholas",
    "email": "nicholas.lennox@noroff.no"
}
```

Det vi ser her er en representasjon av JWT uten noen av Spring Security behandlingen. Dette lar oss trekke ut bestemte krav med `getClaimsAsString`-metoden. For å se hva Spring Security bruker i sikkerhetssammenheng, må vi injisere et `Principal` direkte:

```java
@GetMapping("principal")
public ResponseEntity getPrincipal(Principal user){
    return ResponseEntity.ok(user);
}
```

Nå, ved å ringe `api/v1/users/principal`-endepunktet, får vi følgende svar:

```json
{
    "authorities": [
        {
            "authority": "ROLE_ADMIN"
        }
    ],
    "details": {
        "remoteAddress": "0:0:0:0:0:0:0:1",
        "sessionId": null
    },
    "authenticated": true,
    "principal": {
        "tokenValue": 
        // omitted - far more printed out
```

Dette er hva Spring Security bruker og sender rundt for å sikre at alt gjøres i henhold til den definerte konfigurasjonen. Nå er det viktige spørsmålet, hvordan kan vi synkronisere vår `identitet`og `applikasjonsbruker`? En veldig direkte tilnærming er å bruke JWT direkte, vi kan ha et endepunkt som grensesnittet kan ringe etter en vellykket autentisering for å hente brukeren fra databasen, eller kaste en 404 hvis de ikke eksisterer:

```java
@GetMapping("current")
public ResponseEntity getCurrentlyLoggedInUser(@AuthenticationPrincipal Jwt jwt) {
  return ResponseEntity.ok(
    userService.findById(
      jwt.getClaimAsString("sub")
    )
  );
}
```

Med vår UserServiceImpl har følgende metode:

```java
@Override
public AppUser getById(String uid) {
    return userRepository.findById(uid)
      .orElseThrow(() -> new UserNotFoundException());
}
```

Legg merke til hvordan vi ikke ber om noen banevariabel, da dette er for å få den påloggede brukeren. Dette betyr at vi kan bruke JWT og trekke ut ID-en deres.

Hvis vi får en 404 fra dette, vet vi at vår påloggede bruker ikke er i databasen. Grensesnittet vårt kan deretter omdirigere brukeren til en «fullfør profilen din»-side, eller bare legge til brukeren uten andre inndata, via et add-endepunkt:

```java
@PostMapping("register")
public ResponseEntity addNewUserFromJwt(@AuthenticationPrincipal Jwt jwt) {
    AppUser user = userService.add(jwt.getClaimAsString("sub"));
    URI uri = URI.create("api/v1/users/" + user.getUid());
    return ResponseEntity.created(uri).build();
}
```

Dette er et spesifikt endepunkt for å lage en databasebruker basert på JWT. Tjenesten implementeres som:

```java
@Override
public AppUser add(String uid) {
    // Prevents internal server error for duplicates
    if(userRepository.existsById(uid))
        throw new UserAlreadyExistsException();
    // Create new user
    AppUser user = new AppUser();
    user.setUid(uid);
    user.setComplete(false);
    return userRepository.save(user);
}
```

Som standard er profilen ufullstendig før de oppretter en bio via en oppdatering (PUT). Det er viktig å merke seg at vi kunne hatt et tradisjonelt `POST`-endepunkt med en `AppUser`i *body*’en, dette var for å illustrere hvordan man bruker JWT. Det finnes måter å omgå denne manuelle prosessen, for eksempel å legge til et tilpasset filter eller konfigurere autentiseringsadministratorer.

## CORS

Cross Origin Resource Sharing (CORS) er en mekanisme for å tillate kun autoriserte kilder å få tilgang til serveren din. Dette gjøres via en preflight-forespørsel og er noe som må konfigureres i serveren – selv om det er en feil du ser i frontend. Som standard gjør ikke Postman en pre-flight-forespørsel, og det er derfor du aldri får CORS-feil, men de publiserte grensesnittene dine vil gjøre det.

Når du distribuerer frontend- og backend-applikasjoner til separate nettadresser, vil ikke backend-applikasjonen som standard godta forespørsler fra andre opprinnelser enn sin egen. Ettersom vi aktiverte CORS-sjekking i `SecurityConfig`-klassen vår, må vi eksplisitt tillate at enhver annen opprinnelse vi ønsker skal kunne bruke API-en vår. Dette gjøres først og fremst ved å bruke `@CrossOrigin`-kommentaren på våre kontrollerklasser eller endepunkter:

```java
@RestController
@CrossOrigin("http://localhost:3000")
// OR
@RestController
@CrossOrigin(
        origins = { 
                "https://username.gitlab.io", 
                "http://localhost:3000" 
        }
)
```

> RESSURSE : Artikkel om [CORS-konfigurasjon](https://www.baeldung.com/spring-cors)
> 

> RESSURSE : [Repository](https://gitlab.com/NicholasLennox/spring-security-demo) fra leksjon
> 

> RESSURSE : [Ekstra repo](https://gitlab.com/noroff-accelerate/java/projects/hibernate-with-ci) med ekstra Swagger-konfigurasjon og implisitt flyt.
> 

Copyright 2022, Noroff Accelerate AS