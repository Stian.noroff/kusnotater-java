# 2. Nettsikkerhet- og identitetsleverandører

# Nettsikkerhet- og identitetsleverandører

Denne leksjonen gir en grunnleggende oversikt over rollen til autentisering og autorisasjon i moderne webutvikling. Den dekker noen vanlige konsepter som finnes innenfor dette området, med fokus på token-basert autentisering fra et praktisk synspunkt. Denne leksjonen dekker også bruken av en ekstern identitetsleverandør for å utstede JWT-er og administrere brukere.

**Læringsmål:**

Du vil kunne…

- **diskutere** rollen sikkerhet spiller i moderne programvareutvikling.
- **beskrive** vanlige autentiseringsflyter.
- **forklare** hva en JWT er og dens rolle i moderne autentiseringsløsninger.
- **skille mellom** informasjonskapselbasert autentisering og tokenbasert autentisering.
- **konstruer** en Keycloak- forekomst lokalt, gitt passende Docker-bilder.
- **generere** og **konfigurere “**realms”, klienter og krav. Gitt en fungerende lokal Keycloak- forekomst.
- **opprette** en enkel klient for å motta tilgang JWT-er, gitt en konfigurert Keycloak- forekomst.

## Autentisering og autorisasjon

`Autentisering`er prosessen der en applikasjon identifiserer en person. Det er den programmatiske ekvivalenten til å spørre «Hvem er du?». Dette etableres vanligvis gjennom pålogging i en brukerdatabase hvor det er gitt noe bevis eller indikator for påfølgende aktivitet.

> MERK : En feil i autentisering resulterer i 401 Uautorisert. Det er en lite intuitiv erklæring.
> 

`Autorisasjon`er prosessen med å avgjøre om en person kan få tilgang til visse ressurser. Det er den programmatiske ekvivalenten til å spørre kan du ha denne ressursen? Det er tilgangskontroll.

> MERK : En feil i autorisasjonen resulterer i en 403 Forbidden .
> 

*Autentiserings-* og *autorisasjonsprosesser* er utrolig viktige i webutvikling. De fleste omfattende nettapplikasjoner har en brukerbase slik at enkeltpersoner som bruker nettstedet kan lagre data som er unike for dem og ha tilgang til dem på tvers av bruken av applikasjonen. For at dataene skal knyttes til brukeren på tvers av økter, trenger nettstedet at brukeren beviser at de er den de sier de er.

> MERK : En brukers data skal bare være tilgjengelig for dem.
> 

Det er mange forskjellige tilnærminger og scenarier for autentisering. De riktige måtene å gjøre dem på er blitt formalisert som standarder og protokoller som utviklere kan bruke som retningslinjer for å sikre at de gjør det riktig.

## OAuth2.0 og Open ID Connect

Autentiserings- og autorisasjonsprosesser for en webapplikasjon kan være komplekse. Webutviklingsindustrien utviklet standarder og protokoller for dette for å gi en klar og sikker måte å nærme seg autentisering og autorisasjon på. Kjent som **OAuth 2.0**

`OAuth 2.0`legger ut en modell der en tjeneste gir tilgang til en ukjent bruker/klient ved å sjekke deres identitet og administrere deres tilgang. I hovedsak skiller autentiseringslogikken som en dedikert tjeneste i et system/applikasjon.

Dessverre ble dette gjentatte ganger misbrukt for å finne informasjon om brukeren/klienten under autentisering. Dette problemet ble løst av **OpenID Connect**, som er et ekstra lag på toppen av OAuth2.0.

`OpenID Connect`er en protokoll som utvider, men skiller tilnærmingen for å samle informasjon om en bruker/klient når de autoriserer dem for tilgang. I hovedsak gir OpenID Connect spesifikasjonene for innsamling av klientidentiteter (autentisering). OAuth 2.0 gir spesifikasjonene for tilgangskontroll. Sammen danner de grunnlaget for ulike korrekte og sikre tilnærminger til autentisering.

> MERK : Når du bruker OpenID Connect blir du omdirigert til pålogging et annet sted og deretter tatt tilbake. Dette er kjent som å bruke en identitetsleverandør, som Google er et godt eksempel på.
> 

OpenID connect og de fleste moderne autentiseringsløsninger bruker JSON web-tokens (JWT).

## Json Web Tokens (JWT)

`JWT`er et akronym for `JSON Web Token`, og uttales som *jot* . JWT er definert i [RFC 7519 - JSON Web Token (JWT)](https://tools.ietf.org/html/rfc7519) , dette er en standard, slik at hvert bibliotek som bruker JWT kan tolke (og bruke) det som det velges. Innenfor [RFC 7519 -](https://tools.ietf.org/html/rfc7519) standarden er det en rekke definerte *krav* , men tilpassede krav kan også legges til.

Det kalles ofte en *jot* , som er kortere enn *JavaScript Object Notation Web Token* .

JWT består av tre deler, når alle påstandene er satt sammen til et JSON-objekt, blir de deretter kodet (sammen) ved hjelp av en definert algoritme.

> **RESSURSER** : Nettsted for å kode og dekode JWT-er finner du [her](https://jwt.io/#debugger-io).
> 

### Struktur

Alle som planlegger å bruke JWT bør gjennomgå overskriftene og påstandene i [RFC 7519](https://tools.ietf.org/html/rfc7519) , men noen av de mer vanlige vil bli diskutert nedenfor .

Når du ser på en JWT vil den være kodet og vil vises som en base 64-streng (BASE64URL) delt i tre deler ( `header`, `payload`og `signatur`) med to punktum ( header.payload .signature ).

Et eksempel på JWT:

```
eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI0MjAwIiwibmFtZSI6Ik9sYSBOb3JkbWFubiIsImFkbWluIjp0cnVlLCJpYXQiOjE1OTE5NTYwMDB9.OK4eTjie_R-QzzK2u3eowncawjVyP-GntytWM_e3T-cpOevcA_vCHDpOmsb3dK1dORkBX2nm47ZOCWKQ5ZRke1VLSo3456BAVlUeDugIKKpKZs6edKs9eWgy41vl110ynrQ63UxF38Z0TVYCEzLmOsjuqWHJhFqQ89o0AziGoZcqkpg4ToEStbSG5d75J36XV8aR2OMSdAM3Yn95igFoQ2uogw1T3cXdAdondKpNGC9EU2Edx-Qnb1jHaO_I0mzrlmCNiGgiLcluVlI780NUS-_6k8aHPG4zcgP7eiPn2LqeXK2G5gTJDhWURYKAtujVyRDbniJvfP5b5GbsV1Bc3Q
```

Den samme JWT med linjeskift og fargekoding for visningsformål:

```
eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.ey
JzdWIiOiI0MjAwIiwibmFtZSI6Ik9sYSBOb3Jkb
WFubiIsImFkbWluIjp0cnVlLCJpYXQiOjE1OTE5
NTYwMDB9.OK4eTjie_R-QzzK2u3eowncawjVyP-
GntytWM_e3T-cpOevcA_vCHDpOmsb3dK1dORkBX
2nm47ZOCWKQ5ZRke1VLSo3456BAVlUeDugIKKpK
Zs6edKs9eWgy41vl110ynrQ63UxF38Z0TVYCEzL
mOsjuqWHJhFqQ89o0AziGoZcqkpg4ToEStbSG5d
75J36XV8aR2OMSdAM3Yn95igFoQ2uogw1T3cXdA
dondKpNGC9EU2Edx-Qnb1jHaO_I0mzrlmCNiGgi
LcluVlI780NUS-_6k8aHPG4zcgP7eiPn2LqeXK2
G5gTJDhWURYKAtujVyRDbniJvfP5b5GbsV1Bc3Q
```

I praksis er imidlertid en JWT enten en `JWS`(JSON Web Signature) eller `JWE`(JSON Web Encryption), avhengig av spesifikasjonene i overskriften.

> **MERK** : Følgende underseksjoner gir mer kontekst til JWT-er og er ikke relevante for grunnleggende forståelse.
> 

**JWS**

[RFC 7515 - JSON Web Signature (JWS)](https://tools.ietf.org/html/rfc7515)

> JWS gir kun integritetsverifisering
> 

Denne JWT er signert ved å bruke den spesifiserte digitale signaturalgoritmen ( `alg`) og privat nøkkel. Dette sikrer at nyttelasten ikke har blitt tuklet med (modifisert).

JWS er kodet inn i en `UTF8` `BASE64URL`for enkel overføring.

**Bruke en JWS**

Det er enkelt å lese innholdet i en JWS, så det bør ikke anses som sikkert fra det synspunktet. (Gir ikke hemmelighold.)

Det offentlige (som samsvarer med den private nøkkelen) kreves for å bekrefte integriteten til dataene basert på signaturen.

**JWE**

[RFC 7516 - JSON Web Encryption (JWE)](https://tools.ietf.org/html/rfc7516)

> JWE gir integritetsverifisering og hemmelighold
> 

Denne JWT er kryptert ved hjelp av den spesifiserte krypteringsalgoritmen ( `enc`) og hemmelig nøkkel ( `cek`). Innholdet er nå kryptografisk sikret. Det krypterte innholdet signeres deretter ved hjelp av den spesifiserte digitale signaturalgoritmen ( `alg`) og privat nøkkel som med JWS.

JWE er kodet inn i en `UTF8` `BASE64URL`for enkel overføring.

**Forbruke en JWE**

Det offentlige (som samsvarer med den private nøkkelen) kreves for å bekrefte integriteten til dataene basert på signaturen.

Dekryptering av data vil kreve bruk av samme hemmelige nøkkel.

### Overskrift

Grunnlaget for enhver JWT er `JOSE -`headeren ( JavScript Object Signing and Encryption). Denne JOSE-overskriften er den første delen av JWT; i den er innholdet og algoritmene spesifisert.

- Når du oppretter en JWT, opprettes overskriften først.
- Når du leser en JWT, leses overskriften først.

Overskriftene du bør vurdere når du arbeider med en JWT er som følger:

**typ (Type)**

Dette brukes til å identifisere innholdstypen som en [søknad/](https://www.iana.org/assignments/media-types/application/jwt) [https://www.iana.org/assignments/media-types/application/jwtjwt](https://www.iana.org/assignments/media-types/application/jwt) [https://www.iana.org/assignments/media-types/application/jwt](https://www.iana.org/assignments/media-types/application/jwt).

**anbefaling:** sett dette til `JWT`:

```
"typ": "JWT"
```

**cty (innholdstype)**

Dette definerer “strukturinformasjonen”, som brukes når tokenet består av mer enn ett objekt.

**anbefaling:** vanligvis kan dette utelates.

**alg (algoritme)**

Denne overskriften identifiserer den digitale signaturalgoritmen som brukes av tokenet.

Noen av de vanlige alternativene er:

- ingen
- RS256: asymmetrisk offentlig-privat nøkkelpar ( **anbefalt** )
- HS256: en delt (hemmelig) nøkkel

En fullstendig liste over algoritmer for bruk med JWS finner du i avsnitt 3.1 av [RFC 7518 - JSON Web Algorithms (JWA)](https://tools.ietf.org/html/rfc7518)

**anbefaling:** Bruk RS256

**Eksempel overskrift**

Ved å bruke anbefalingene ovenfor ser den grunnleggende overskriften (fra eksempelet JWT ovenfor) ut som følger:

`eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9`

og er dekodet som:

```
{
  "alg": "RS256",
  "typ": "JWT"
}
```

Som nevnt før, er denne overskriften nødvendig (og må være gyldig) før et token kan opprettes eller leses.

### Payload

Nyttelasten representerer dataene ( `kravene`) som du ønsker å opprettholde integriteten til. Du står fritt til å legge inn dine egne krav (les [4.3. Private krav](https://tools.ietf.org/html/rfc7519#section-4.3) ), men det er best å bruke de standardiserte først for å unngå mulige kollisjoner.

> Alle krav er valgfrie.
> 

**iss (utsteder)**

Utstederen er serveren, applikasjonen eller selskapet som er ansvarlig for å utstede (signere/kryptere) JWT.

**anbefaling:** angi dette til firmanavnet ditt:

```
"iss": "Noroff Accelerate"
```

**sub (Subject)** Dette identifiserer *tingen* beskrevet av JWT, i de fleste tilfeller er dette den aktive brukeren.

**anbefaling:** sett dette til en unik ID for den gitte brukeren:  

```
"sub": "4200"
```

**aud (Audience)** Dette identifiserer det tiltenkte domenet, applikasjonen eller frontend-applikasjonen som vil bruke JWT.

**anbefaling:** angi dette til navnet på søknaden din:

```
"aud": "Web - Dashboard"
```

**exp (Utløpstid)** En sluttid som tokenet kan brukes før. (Den vil være ugyldig **etter** dette tidspunktet.)

**anbefaling:** angi dette basert på dine egne behov:

```
"exp": 1592042000"iat": 1591956000
```

**nbf (Ikke før)** Et starttidspunkt for når token kan brukes. (Den vil være ugyldig **før** dette tidspunktet.)

**anbefaling:** vanligvis kan dette utelates.

**iat (Issued At)** Tidspunktet da tokenet ble utstedt, og kan brukes til å beregne alderen på tokenet.

**anbefaling:** Sett dette lik gjeldende dato/klokkeslett når tokenet opprettes.

```
"iat": 1591956000
```

**jti (JWT ID)** Unik nøkkel for å identifisere JWT.

**anbefaling:** Generer en [UUID](https://en.wikipedia.org/wiki/Universally_unique_identifier) på serveren.

```
"jti": "9a871145-6665-4e2c-9190-a6db1c50ad72"
```

**Eksempel på nyttelast:**

`eyJzdWIiOiI0MjAwIiwibmFtZSI6Ik9sYSBOb3JkbWFubiIsImFkbWluIjp0cnVlLCJpYXQiOjE1OTE5NTYwMDB9`

vil dekodes til:

```json
{
  "sub": "4200",
  "name": "Ola Nordmann",
  "admin": false,
  "iat": 1516239022
}
```

I dette eksemplet er `navn`og `admin`private krav.

### Signatur

For å lage signaturdelen må du ta den kodede overskriften, den kodede nyttelasten, en hemmelighet, algoritmen spesifisert i overskriften, og signere det.

Hvis du for eksempel vil bruke `HMAC SHA256`-algoritmen, vil signaturen bli opprettet på følgende måte:

```
HMACSHA256(
  base64UrlEncode(header) + "." +
  base64UrlEncode(payload),
  secret)
```

Signaturen brukes til å bekrefte at meldingen ikke ble endret underveis, og i tilfelle av tokens signert med en privat nøkkel, kan den også bekrefte at avsenderen av JWT er den den sier den er.

**Eksempel Signatur**

```json
OK4eTjie_R-QzzK2u3eowncawjVyP-GntytWM_e3T-
cpOevcA_vCHDpOmsb3dK1dORkBX2nm47ZOCWKQ5ZRke1VLSo3456BAVlUeDugIKKpKZs6edKs9eWgy41vl110ynrQ63UxF38Z0TVYCEzLmOsjuqWHJhFqQ89o0AziGoZcqkpg4ToEStbSG5d75J36XV8aR2OMSdAM3Yn95igFoQ2uogw1T3cXdAdondKpNGC9EU2Edx-
Qnb1jHaO_I0mzrlmCNiGgiLcluVlI780NUS-
_6k8aHPG4zcgP7eiPn2LqeXK2G5gTJDhWURYKAtujVyRDbniJvfP5b5GbsV1Bc3Q
```

> I de fleste situasjoner genereres en JWT for oss. Vi gir bare noen detaljer, og kodingen og signeringen gjøres automatisk.
> 

Når du har en gyldig JWT-tilgang fra en leverandør, sender du den vanligvis til ressursserveren for validering ved å bruke `autorisasjonsoverskriften`som `bærertoken`i forespørselen.

### Kontrastert med økter

For å forstå fordelene med token-basert autentisering og hvilke problemer den løste, må den kontrasteres med informasjonskapselbasert autentisering.

### Cookie-basert autentisering

Standard, utprøvd metode for å håndtere brukerautentisering i lang tid. Den er *stateful* , noe som betyr at en autentiseringsoppføring eller sesjon må beholdes både på server- og klientsiden. Serveren holder styr på aktive økter i en database. Mens det er på front-end, opprettes en informasjonskapsel som inneholder en øktidentifikator.

Informasjonskapselbasert autentiseringsflyt har følgende rekkefølge:

1. Brukeren skriver inn påloggingsinformasjonen.
2. Serveren bekrefter at legitimasjonen er korrekt og oppretter en økt som deretter lagres i en database.
3. En informasjonskapsel med økt-IDen plasseres i brukerens nettleser.
4. Ved påfølgende forespørsler verifiseres økt-ID-en mot databasen, og hvis gyldig behandles forespørselen.
5. Når en bruker logger ut av appen, blir økten ødelagt både på klientsiden og serversiden.

### Tokenbasert autentisering

Økt i popularitet på grunn av *enkeltsideapplikasjoner* og tingenes *internett* . Tokens går hånd i hånd med disse teknologiene på grunn av enkel lagring.

Den store fordelen med tokens er at de er *stateless*. Dette betyr at det ikke trenger å holdes journal på serveren. Denne posten vil typisk være en økt om hvem som er pålogget. Hvordan tokenbasert autentisering kommer rundt sporing av hvem som er pålogget, er ved å bruke *validering*. Ser når tokens ble utstedt, hvem de ble utstedt av, og når de utløper; disse tokens sendes til serveren for verifisering på hver forespørsel.

> **MERK** : Husk at RESTful APIer må også være statsløse. Denne kombinasjonen har gjort at REST og Tokens har blitt dominerende.
> 

Tokenbasert autentiseringsflyt har følgende rekkefølge:

1. Brukeren skriver inn påloggingsinformasjonen.
2. Serveren bekrefter at legitimasjonen er korrekt og returnerer et signert token.
3. Dette tokenet lagres på klientsiden, oftest i lokal lagring - men kan også lagres i øktlagring eller en informasjonskapsel.
4. Påfølgende forespørsler til serveren inkluderer dette tokenet som en ekstra autorisasjonsoverskrift eller gjennom en av de andre metodene nevnt ovenfor.
5. Serveren dekoder JWT og behandler forespørselen hvis tokenet er gyldig.
6. Når en bruker logger ut, blir tokenet ødelagt på klientsiden, ingen interaksjon med serveren er nødvendig.

### Tokenbaserte fordeler

Følgende underkapitler tar for seg hvert aspekt som kan anses som en fordel, dette gjøres som en oppsummering. Det er andre aspekter, men de som er oppført nedenfor er en generalisering.

### 1. Statsløs, skalerbar og frakoblet

Å være statsløs er den største fordelen siden backend ikke trenger å holde oversikt over tokens. Serverens eneste jobb er å sjekke gyldigheten ettersom tokens til og med er signert av tredjeparter som OAuth.

### 2. Cross Domain og CORS

Informasjonskapsler fungerer godt med enkeltdomener og underdomener. Det er imidlertid komplisert å administrere dem på tvers av domener, siden de har en bestemt nettleser de er knyttet til. Tokenbasert tilnærming med CORS aktivert gjør det trivielt å eksponere APIer for forskjellige tjenester og domener.

### 3. Lagre data i JWT

Med en informasjonskapselbasert tilnærming lagrer du ganske enkelt økt-ID-en i en informasjonskapsel. Tokens kan lagre alle metadata så lenge de er gyldige JSON.

> Vær imidlertid forsiktig - tokens er signert ikke kryptert som standard, så ikke lagre hemmelig informasjon som passord.
> 

### 4. Ytelse

Når du bruker den informasjonskapselbaserte autentiseringen, må back-end gjøre et oppslag til en database. Dette tar lengre tid enn å dekode et token da det er en nettverksreise. Ettersom du kan lagre tilleggsdata inne i JWT, kan du passere brukerens tillatelsesnivå. Dette lar deg lagre en ekstra oppslagsanrop for å hente og behandle de forespurte dataene.

### 5. Klar for mobil

Moderne API-er samhandler ikke bare med nettleseren. Et enkelt API kan betjene både nettleseren og native mobile plattformer som iOS og Android. Innfødte mobilplattformer og informasjonskapsler blander seg dårlig, siden det ikke er noen enkel måte å lagre og bruke en informasjonskapsel på – de er avhengige av andre lagringsmetoder.

Tokens er derimot mye enklere å implementere på både iOS og Android. Tokens er også enklere å implementere for Internet of Things-applikasjoner og tjenester som ikke har konseptet som en informasjonskapselbutikk.

## Autentisering flyter

Det er mange autentiseringsscenarier. Disse blir ofte referert til som flyter og er diktert av webapplikasjonsdesignet. Når en ekstern identitetsleverandør brukes, ikke en internt administrert øktbasert tilnærming, er de vanlige flytene:

- Implisitt flyt
- Auth kode flyt

Selv om disse to flytene kan føles like avhengig av implementeringen, er de ganske forskjellige under panseret.

> MERK : For våre formål bruker vi Auth-kodeflyt .
> 

### Implisitt flyt

Kort sagt, dette er når backend sender en forespørsel om autentisering til identitetsleverandøren.

Illustrasjonen nedenfor viser denne prosessen:

![Untitled](2%20Nettsikkerhet-%20og%20identitetsleverand%C3%B8rer%2059c2044a2d8f460aa91db8fa30954ef9/Untitled.png)

Denne prosessen involverer følgende trinn:

1. Klienten gir innloggingsdetaljer til serveren.
2. Serveren ber den oppgitte brukeren om å bli autentisert av identitetsleverandøren og mottar deretter tilgangs- og id-tokens.
3. Serveren sender disse tokenene tilbake til klienten.
4. Klienten legger ved tokenet ved hver forespørsel.
5. Serveren bekrefter ektheten til tokenet.
6. Serveren verifiserer tilgangsrettigheter og henter deretter de forespurte dataene. Vil returnere en 403 hvis ikke tillatt.
7. Dataene returneres til klienten.

Mange nettsteder fungerer på denne måten, den er identisk med den sesjonsbaserte modellen når det gjelder interaksjon. Imidlertid kan et nettsted som ser ut til å bruke implisitt flyt faktisk utføre autentiseringskodeflyt, men bare skjule det. Hvis et nettsted omdirigerer deg til en separat server eller et pop-out-vindu, er det IKKE implisitt flyt.

> MERK : Implisitt flyt kan brukes til å teste autentisering på en backend isolert ved å la OpenAPI /Swagger autentisere seg selv.
> 

### Auth Code Flow

Kort sagt, dette er når grensesnittet (klienten) sender en forespørsel om autentisering til identitetsleverandøren. Dette er en mer vanlig tilnærming i moderne utvikling, og alle de store grensesnittene har adapterbiblioteker for å hjelpe de fleste store identitetsleverandører.

Illustrasjonen nedenfor viser denne prosessen:

![Untitled](2%20Nettsikkerhet-%20og%20identitetsleverand%C3%B8rer%2059c2044a2d8f460aa91db8fa30954ef9/Untitled%201.png)

Denne prosessen involverer følgende trinn:

1. Klienten prøver å få tilgang til ressurser uten token. Klienten mottar da et 401-svar.
2. Klienten omdirigerer brukeren til identitetsleverandøren for å logge på og autentisere. Klienten mottar tilgangs- og id-tokens.
3. Klienten sender deretter forespørselen på nytt, men med token vedlagt.
4. Serveren bekrefter ektheten til tokenet
5. Serveren verifiserer tilgangsrettigheter og henter deretter de forespurte dataene. Vil returnere en 403 hvis ikke tillatt.
6. Dataene returneres til klienten

I denne tilnærmingen er nøkkelforskjellen hvor autentiseringen finner sted og hvordan dette påvirker brukeren. Et veldig godt eksempel på dette er Googles pålogging – popup-vinduet. Det er også viktig å merke seg at bare fordi nettstedet ikke omdirigerer deg, betyr det ikke at det ikke utfører godkjenningskodeflyt. Noen biblioteker tillater at denne prosessen skjules for ikke å bryte brukerflyten eller tvinge frem et bestemt påloggingsskjema.

## Identitetsleverandører

Denne delen omhandler oppsett og bruk av en selvvertsbasert identitetsleverandør kalt [https://www.keycloak.org/Keycloak](https://www.keycloak.org/) [https://www.keycloak.org/](https://www.keycloak.org/).

Når det gjelder ansvar, gjør *Keycloak* følgende for oss:

- Bruker registrering
- Bruker autentisering
- JWT generasjon

Dette overlater følgende til vår Java-applikasjon:

- JWT-validering
- Rollebasert autentisering.

### Prosjektstruktur

Vi trenger tre uavhengige systemer:

- En *grensesnittklient med* funksjonalitet for å få tilgang til våre andre systemer. Dette kan være en Http-tjeneste for å koble til vår Java-server og Keycloak- instans.
- En *autorisasjonsserver* , dette er Keycloak . Den vil kjøre på datamaskinen vår som en separat applikasjon gjennom Docker.
- En *ressursserver* , dette er vår Java-backend som holder ressursene.

Våre brukere og passord er lagret i *Keycloak* , det betyr at identiteten administreres av Keycloak .

> MERK : Denne leksjonen inneholder ikke ressursserveren, og heller ikke synkronisering av identiteter. Det gjøres i neste leksjon.
> 

> MERK : Husk at vi utfører Auth-kodeflyt.
> 

### OIDC-vilkår

Noen ofte brukte OIDC-termer er forklart i de følgende underavsnittene.

### Ressurseier

Du, eieren av identiteten din, dataene dine og alle handlinger som kan utføres med kontoene dine.

### Klient

Applikasjonen som ønsker å få tilgang til data eller utføre handlinger på vegne av *ressurseieren* .

### Autorisasjonsserver

Applikasjonen som kjenner *ressurseieren* , som betyr; der *ressurseieren* allerede har en konto.

### Ressursserver

Application Programming Interface (API) eller tjeneste klienten ønsker å bruke på vegne av ressurseieren.

### Omdiriger URI

URL-en *autorisasjonsserveren* vil omdirigere *ressurseieren* tilbake til etter å ha gitt tillatelse til *klienten* . Dette blir noen ganger referert til som “Callback URL”.

### Svartype

Den typen informasjon *kunden* forventer å motta. Den vanligste *svartypen* er kode, hvor *klienten* forventer en *autorisasjonskode* .

### Omfang

Dette er de detaljerte tillatelsene *klienten* ønsker, for eksempel tilgang til data eller å utføre handlinger.

### Samtykke

*Autorisasjonsserveren* tar *omfanget* klienten ber om, og bekrefter med *ressurseieren om* de ønsker å gi *klienten* tillatelse *eller ikke.*

### Klient-ID

Denne IDen brukes til å identifisere *klienten* med *autorisasjonsserveren* .

### Klienthemmelighet

Dette er et hemmelig passord som bare *klienten* og *autorisasjonsserveren* kjenner til. Dette lar dem trygt dele informasjon privat bak kulissene.

### Godkjennelseskoden

En kortvarig midlertidig kode som *klienten* gir *autorisasjonsserveren* i bytte mot et *tilgangstoken* .

### Tilgangstoken

Nøkkelen klienten skal bruke for å kommunisere med *ressursserveren* . Dette er som et merke eller nøkkelkort som  gir *klienten* tillatelse til å be om data eller utføre handlinger med *ressursserveren* på dine vegne.

### Nøkkelkappe

Denne delen skal gi en introduksjon til hva Keycloak er og hva den tilbyr oss.

> MERK : Husk at Keycloak fungerer som vår autorisasjonsserver .
> 

Keycloak er en åpen kildekode *Identity and Access Management-løsning* rettet mot moderne applikasjoner og tjenester. Det gjør det enkelt å sikre applikasjoner og tjenester med lite eller ingen kode.

> MERK : Keycloak gir også våre roller/krav/omfang
> 

### Enkel pålogging

Brukere autentiserer med Keycloak i stedet for vår Java-applikasjon. Dette betyr at applikasjonene dine ikke trenger å håndtere påloggingsskjemaer, autentisering av brukere og lagring av brukere. Når de er logget inn på Keycloak , trenger ikke brukere å logge på igjen.

> En enkelt Keycloak- instans kan være autorisasjonsserveren for flere applikasjoner.
> 

### Identitetsmegling og sosial pålogging

Å aktivere pålogging med sosiale nettverk er enkelt å legge til via administrasjonskonsollen. Det er bare å velge det sosiale nettverket du vil legge til. Ingen kode eller endringer i applikasjonen er nødvendig.

> MERK : Administrasjonskonsollen er et veldig kraftig grensesnitt vi bruker til å utføre all vår identitetskonfigurasjon.
> 

### Brukerforbund

Keycloak har innebygd støtte for å koble til eksisterende LDAP- eller Active Directory-servere. Du kan også implementere din egen leverandør dersom du har brukere i andre butikker, for eksempel en relasjonsdatabase.

> MERK : Vi vil ikke dekke dette i leksjonen.
> 

### Klientadaptere

*Keycloak Client Adapters* gjør det veldig enkelt å sikre applikasjoner og tjenester. Keycloak har adaptere tilgjengelig for en rekke plattformer og programmeringsspråk.

### Administrasjonskonsoll

Gjennom administrasjonskonsollen kan administratorer sentralt administrere alle aspekter av Keycloak- serveren. Du kan:

- Aktiver og deaktiver ulike funksjoner. De kan konfigurere identitetsmegling og brukerforbund.
- Opprett og administrer applikasjoner og tjenester, og definer finkornet autorisasjonspolicyer.
- Administrer brukere, inkludert tillatelser og økter.

![Untitled](2%20Nettsikkerhet-%20og%20identitetsleverand%C3%B8rer%2059c2044a2d8f460aa91db8fa30954ef9/Untitled%202.png)

> MERK: En Keycloak admin API kan også samhandles med.
> 

### Kontoadministrasjonskonsoll

Gjennom kontoadministrasjonskonsollen kan brukere administrere sine egne kontoer. De kan oppdatere profilen, endre passord og sette opp tofaktorautentisering. Brukere kan også administrere økter samt se historikk for kontoen.

Hvis sosial pålogging eller identitetsmegling er aktivert, kan brukere også koble kontoene sine med flere leverandører for å la dem autentisere seg til samme konto med forskjellige identitetsleverandører.

### Opprette en Keycloak- forekomst

For å sette opp keycloak lokalt, bruker vi ganske enkelt Docker. Det er klare instruksjoner om hvordan du setter opp en forekomst av Keycloak lokalt [fra jBoss](https://github.com/keycloak/keycloak-containers/blob/12.0.2/server/README.md)

Vi vil lage en forekomst av Keycloak lokalt med brukernavnet og passordet til `admin`og `admin`. For å gjøre dette åpner vi en terminal og kjører:

```bash
docker run -p 8083:8080 -e KEYCLOAK_USER=admin -e KEYCLOAK_PASSWORD=admin jboss / keycloak
```

> `-e` står for miljø tilgjengelighet.
> 

> Gi det litt tid til å starte opp.
> 

> Vi viser port `8083` fordi ressursserverenvår vil kjøre på `8080`.
> 

Hvis du navigerer til `http://localhost:8083/auth/admin`, bør du få en Keycloak - påloggingsskjerm.

![Untitled](2%20Nettsikkerhet-%20og%20identitetsleverand%C3%B8rer%2059c2044a2d8f460aa91db8fa30954ef9/Untitled%203.png)

Nå kan vi gå inn i keycloak og begynne å sette opp ting gjennom administratoren vår.

### Sette opp brukere

Når vi har logget på kontoen vår, kan vi administrere : riker, brukere, legitimasjon, omfang, krav og mye mer.

> Denne delen er tilpasset fra de offisielle [Keycloak docs’ene](https://www.keycloak.org/docs/latest/server_admin/index.html#admin-console)
> 

### Rikene

Når vi lanserer Keycloak for første gang, blir vi beundret med et mesterrike , dette *riket* er for administratorer og har tilgang til alle andre riker du oppretter. Vi bør `IKKE`bruke dette riket for våre brukere. Vi burde lage vår egen.

For å lage et nytt rike holder vi musepekeren over rikets navn øverst til venstre og velger `Legg til rike`.

![Untitled](2%20Nettsikkerhet-%20og%20identitetsleverand%C3%B8rer%2059c2044a2d8f460aa91db8fa30954ef9/Untitled%204.png)

Da vil du bli møtt med siden `Add realm`:

![Untitled](2%20Nettsikkerhet-%20og%20identitetsleverand%C3%B8rer%2059c2044a2d8f460aa91db8fa30954ef9/Untitled%205.png)

Her oppgir vi et navn, i vårt tilfelle `noroff`.

> Vi kan også importere et rike. Keycloak lar deg importere og eksportere for alle innstillinger, slik at det er mulig å konfigurere en Keycloak- forekomst, eksportere informasjonen som JSON og deretter importere innstillingene ved distribusjon.
> 

Når vi har opprettet et rike, kan vi konfigurere hvordan brukere skal logge på. Du kan administrere ulike aspekter, for eksempel:

- Hvis brukere kan registrere seg selv
- At e-postadressen deres er brukernavnet
- Tillat dem å tilbakestille passord
- Husk meg funksjonalitet
- Epostbekreftelse
- SSL-krav

Av alle disse bryr vi oss bare om to for våre formål:

- Brukere kan registrere seg
- SSL

Vi ønsker at brukere skal kunne registrere seg selv, på denne måten trenger vi ikke å opprette forhåndsdefinerte brukere. Og Require SSL handler om HTTPS-tilkoblinger, som standard er den på ekstern. Dette betyr at brukere kan samhandle med Keycloak uten SSL så lenge de holder seg til private IP-adresser som localhost, 127.0.0.1, 10.xxx, 192.168.xx og 172.16.xx Hvis du prøver å få tilgang til Keycloak uten SSL fra en ikke- privat IP- adresse får du en feilmelding.

![Untitled](2%20Nettsikkerhet-%20og%20identitetsleverand%C3%B8rer%2059c2044a2d8f460aa91db8fa30954ef9/Untitled%206.png)

Vi kan administrere brukere fra sidemenyen. Dette lar oss:

- Søk etter eksisterende brukere
- Opprett nye brukere
- Slette brukere
- Endre brukerlegitimasjon
- Krev handlinger som; oppdatere passord, konfigurere engangskoder, bekrefte e-poster osv .

> Brukeradministrasjonen er utenfor omfanget av det vi gjør for denne leksjonen, det er fortsatt et veldig kraftig verktøy og du bør [lære om det](https://www.keycloak.org/docs/latest/server_admin/index.html#user-management) .
> 

> Vi skal opprette en dummy-bruker for å konfigurere ressursserveren vår . Vi setter også et passord.
> 

![Untitled](2%20Nettsikkerhet-%20og%20identitetsleverand%C3%B8rer%2059c2044a2d8f460aa91db8fa30954ef9/Untitled%207.png)

Da vi satte inn `brukerregistrering ,`aktiverte vi en ny side brukerne kan samhandle med, registreringen. Dette er tilgjengelig via en `registerkobling`som vises på påloggingsskjemaet.

> Disse skjemaene kan tilpasses ved hjelp av temaer .
> 

![Untitled](2%20Nettsikkerhet-%20og%20identitetsleverand%C3%B8rer%2059c2044a2d8f460aa91db8fa30954ef9/Untitled%208.png)

![Untitled](2%20Nettsikkerhet-%20og%20identitetsleverand%C3%B8rer%2059c2044a2d8f460aa91db8fa30954ef9/Untitled%209.png)

Du kan til og med legge til Google reCAPTCHA i skjemaene dine:

![Untitled](2%20Nettsikkerhet-%20og%20identitetsleverand%C3%B8rer%2059c2044a2d8f460aa91db8fa30954ef9/Untitled%2010.png)

> Du får tilgang til dette ved å gå til menyelementet `Authentication` til venstre og gå til `Flows` -fanen. Velg `Registration` flyten fra rullegardinlisten på denne siden.
> 

> Dette krever et oppsett med hemmeligheter, vi dekker ikke dette.
> 

### Klienter

Klienter er enheter som kan be om autentisering av en bruker. Dette er frontend - aspektene som prøver å få tilgang til ressursene våre og viderekobles til Keycloak .

Vi ønsker å lage en grensesnittklient i Keycloak slik at vi kan koble den til vår Keycloak- instans. Vi gjør dette ved å navigere til `clients` på venstre side og velge `create`.

> Vi vil prøve å holde front-end -klienten så enkel som mulig, da dette er et backend- fokusert kurs.
> 

![Untitled](2%20Nettsikkerhet-%20og%20identitetsleverand%C3%B8rer%2059c2044a2d8f460aa91db8fa30954ef9/Untitled%2011.png)

![Untitled](2%20Nettsikkerhet-%20og%20identitetsleverand%C3%B8rer%2059c2044a2d8f460aa91db8fa30954ef9/Untitled%2012.png)

De viktige aspektene å vurdere er de `gyldige omdirigerings-URIene`og `Web Origins`, disse konfigurerer Keycloak til å kunne motta trafikk fra den url - en og omdirigere til underdomener av den.

> Vi kan foreløpig anta at vi har en nettbasert applikasjon som kjører og fungerer som grensesnittet vårt på `http://localhost:8000`.
> 

> Klient-ID-en er viktig siden den brukes til å identifisere applikasjonen for OIDC-forespørsler. Vi ønsker også at denne klienten skal kunne gjøre nettleserpålogginger, så vi konfigurerer IKKE tilgangstypen til å være bare bærer .
> 

> Keycloak er nå konfigurert til å motta forespørsler fra klienten, så kan folk begynne å registrere seg og få tilgangstokens.
> 

### Frontend-klient

Som nevnt før, holder vi dette så enkelt som mulig da dette er et backend- fokusert kurs. Vi kommer til å bruke en av de mange adapterne keycloak har. Vi skal lage en enkel JavaScript-basert applikasjon (ingen fancy React), og bruke [Keycloak JavaScript Adapteren](https://www.keycloak.org/docs/latest/securing_apps/index.html#_javascript_adapter) til å konfigurere grensesnittet vårt.

For å konfigurere applikasjonen vår til å peke til Keycloak og bruke den, må vi hente Keycloak.js-skriptet og laste ned vår `Keycloak.json`-konfigurasjon.

Vi henter skriptet vårt fra `http://localhost:8083/auth/js/keycloak.js`og vår json - konfigurasjon kan lastes ned fra klienten vår på Keycloak ved å gå til installasjonsfanen og velge Keycloak OIDC JSON.

![Untitled](2%20Nettsikkerhet-%20og%20identitetsleverand%C3%B8rer%2059c2044a2d8f460aa91db8fa30954ef9/Untitled%2013.png)

Vi lager en veldig enkel applikasjon som har flere funksjoner:

- Logg Inn
- Logg ut
- Forfriske
- Tilgang Token JSON
- Tilgangstoken
- Oppdater token

Alle disse funksjonene benytter seg av den innebygde funksjonaliteten til `keycloak.js`-funksjonene. Vi skriver ingenting selv.

```html
<ul class="nav navbar-nav navbar-primary">
    <li><a href="#" onclick="kc.login(login)">Login</a></li>
    <li><a href="#" onclick="kc.logout()">Logout</a></li>
    <li><a href="#" onclick="kc.updateToken(-1)">Refresh</a></li>
    <li><a href="#" onclick="output(kc.idTokenParsed)">ID Token JSON</a></li>
    <li><a href="#" onclick="output(kc.tokenParsed)">Access Token JSON</a></li>
    <li><a href="#" onclick="output(kc.idToken)">ID Token</a></li>
    <li><a href="#" onclick="output(kc.token)">Access Token</a></li>
    <li><a href="#" onclick="output(kc.refreshToken)">Refresh Token</a></li>
</ul>
```

> Output-funksjonen ( ) legger ganske enkelt til elementer til DOM for å gjengi de returnerte dataene.
> 

Denne front-enden vil ganske enkelt skrive ut tokens slik at vi kan dekode og ta en titt på dem. Se hvilke krav som er inne og så videre. Dette vil bedre tillate oss å konfigurere våre tokens for forbruk av vår ressursserver.

> Koden for dette grensesnittet finner du [her](https://gitlab.com/NicholasLennox/keycloak-simple-client) .
> 

> Instruksjoner om hvordan du kjører Docker-beholderen for det programmet finner du i README.md
> 

vi navigerer til `http://localhost:8000`, blir vi omdirigert til vår *Keycloak-* pålogging.

![Untitled](2%20Nettsikkerhet-%20og%20identitetsleverand%C3%B8rer%2059c2044a2d8f460aa91db8fa30954ef9/Untitled%2014.png)

> Legg merke til at vi er på Noroff-riket.
> 

Vi kan enten registrere en ny bruker eller bruke vår opprettede bruker fra før:

- Brukernavn/e-post: nicholas.lennox@noroff.no
- Passord: passord

Dette resulterer i at vi logger inn på vår klient og får følgende skjermbilde:

![Untitled](2%20Nettsikkerhet-%20og%20identitetsleverand%C3%B8rer%2059c2044a2d8f460aa91db8fa30954ef9/Untitled%2015.png)

> Som standard er grensesnittet vårt konfigurert til å vise brukernavnet.
> 

Husk at vi bruker tilgangstokener for å få tilgang til beskyttede ressurser. La oss klikke på Access Token og se hvordan tokenet ser ut:

![Untitled](2%20Nettsikkerhet-%20og%20identitetsleverand%C3%B8rer%2059c2044a2d8f460aa91db8fa30954ef9/Untitled%2016.png)

Du bør legge merke til at dette tokenet er mye lengre enn det vi genererte med Spring Security. Det er fordi det tokenet bare inneholdt brukernavnet som et emne. Dette tokenet inneholder mye mer, siden det kommer fra en bransjeanerkjent leverandør. Når vi går over til [jwt.io](https://jwt.io/) og går til feilsøkeren, limer vi inn dette tokenet og går ut:

![Untitled](2%20Nettsikkerhet-%20og%20identitetsleverand%C3%B8rer%2059c2044a2d8f460aa91db8fa30954ef9/Untitled%2017.png)

![Untitled](2%20Nettsikkerhet-%20og%20identitetsleverand%C3%B8rer%2059c2044a2d8f460aa91db8fa30954ef9/Untitled%2018.png)

![Untitled](2%20Nettsikkerhet-%20og%20identitetsleverand%C3%B8rer%2059c2044a2d8f460aa91db8fa30954ef9/Untitled%2019.png)

Det er noen viktige ting å merke seg her om tokenet: omfang og roller er inkludert - vi kan bruke dette til å legge til våre egendefinerte roller, og signaturen er faktisk kryptert . Dette er hvordan en skikkelig JWT er konstruert, det vi gjorde i generasjonen var den mest rett frem “bare bones” tilnærmingen. Vi vil ideelt sett legge til litt ekstra informasjon til nyttelasten, men for den leksjonens formål vil det bare legge til oppblåsthet.

> Forskjellen mellom dette prosjektet og det som ble laget for Spring Security Basics-leksjonen, er at dette prosjektet følger OAuth2.0 og OIDC med omfang og identitetsleverandører. Mens det prosjektet gjorde enkel implisitt flyt med lett OAuth2.0, da det ikke trengte en ekstern identitetsleverandør.
> 

### Legge til roller

Vi kan legge til roller på rikenivå, eller roller på klientnivå i Keycloak. Dette er ned til omfanget og dine avgjørelser. Vi kommer til å legge til noen roller til vår klient.

- Bruker
- Moderator
- Administrator

For å legge til disse rollene, navigerer vi til klienter, velger deretter min app-klient og går til roller-fanen.

![Untitled](2%20Nettsikkerhet-%20og%20identitetsleverand%C3%B8rer%2059c2044a2d8f460aa91db8fa30954ef9/Untitled%2020.png)

Så legger vi til de tre rollene:

![Untitled](2%20Nettsikkerhet-%20og%20identitetsleverand%C3%B8rer%2059c2044a2d8f460aa91db8fa30954ef9/Untitled%2021.png)

Nå kan vi tildele disse rollene til brukerne våre, i dette tilfellet skal vi gjøre `nicholas.lennox@noroff.no til`en *Administrator*. Vi gjør dette ved å gå til *Brukere* , deretter finne brukeren vår, gå til *Rolletilordninger* , deretter velge `my-app`fra rullegardinmenyen *Client Roles* og legge til *Administrator-*rollen.

![Untitled](2%20Nettsikkerhet-%20og%20identitetsleverand%C3%B8rer%2059c2044a2d8f460aa91db8fa30954ef9/Untitled%2022.png)

Hvis vi går tilbake til klienten vår og logger på igjen, og deretter dekoder JWT, vil vi finne at rollene våre er lagt til, under `my-app`. De ville dukket opp under `realm_access`hvis vi hadde gjort rollen til en *realm-rolle,* ikke en *client-rolle*.

![Untitled](2%20Nettsikkerhet-%20og%20identitetsleverand%C3%B8rer%2059c2044a2d8f460aa91db8fa30954ef9/Untitled%2023.png)

Samme prosess, men med globale roller:

![Untitled](2%20Nettsikkerhet-%20og%20identitetsleverand%C3%B8rer%2059c2044a2d8f460aa91db8fa30954ef9/Untitled%2024.png)

![Untitled](2%20Nettsikkerhet-%20og%20identitetsleverand%C3%B8rer%2059c2044a2d8f460aa91db8fa30954ef9/Untitled%2025.png)

> Legg merke til hvordan administratorrollen vår nå er i realm_access.
> 

Vi kan også sette standardroller for nye brukere av systemet. Vi kan gjøre dette ved å navigere til *Roller* og velge *standardrollene* og deretter tildele over. Her er et eksempel der du oppgir standardrollen som `User`, noe som betyr at alle nye brukere vil ha `User` rollen.

![Untitled](2%20Nettsikkerhet-%20og%20identitetsleverand%C3%B8rer%2059c2044a2d8f460aa91db8fa30954ef9/Untitled%2026.png)

Copyright 2022, Noroff Accelerate AS