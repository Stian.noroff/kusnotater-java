# 4. Containerisering med Docker

# Docker

Denne leksjonen dekker konseptet containerisering og dets rolle i moderne programvareutvikling. Docker-beholderplattformen brukes som verktøy for å lage kjørende beholdere og vise verdien deres. Dette inkluderer å skrive Dockerfiler, utforske de vanlige konfigurasjonene som er tilgjengelige gjennom disse filene, samt å bruke flere bilder i en komponerfil. Til slutt utforskes handlingen med å publisere en containerisert applikasjon.

**Læringsmål:**

Du vil kunne…

- **diskutere** rollen containerisering spiller i moderne programvareutvikling.
- **kontrast** beholdere med virtuelle maskiner.
- **forklar** hva Docker er.
- **opprette** en Dockerfile som replikerer et gitt programs miljø.
- **lage** et Docker-bilde, gitt en Dockerfile .
- **opprette** en Docker-beholder, gitt et Docker-bilde.
- **demonstrere** prosessen med å legge til et Docker-bilde til et online containerregister .
- **demonstrere** publisering av et Docker-bilde til en plattform som Heroku.
- **komponer** flere gitte Docker-bilder sammen for å **lage** flere kjørende, sammenkoblede containere.

Å distribuere en fullverdig nettapplikasjon kan vise seg å være en komplisert oppgave hvis den gjøres manuelt. Plattformer som Heroku og Docker forenkler denne prosessen betraktelig, og lar utviklere fokusere på produktivitet i stedet for konfigurasjon av webservere. Containerisering brukes for å oppnå dette.

> Docker er et åpen kildekode, Linux-basert verktøy for distribusjon og administrasjon av containere. Containere lar applikasjoner pakkes, overføres og kjøres enkelt på tvers av flere plattformer.
> 

Denne leksjonen er dedikert til å utvide påstanden ovenfor. Den dekker teorien rundt Docker, dens praktiske implementering og dens rolle i moderne programvareutvikling.

## Containere

> For å forstå Docker og dens mekanismer , må konseptet containere først forstås.
> 

En container er det som er kjent som en `standard unit of software`, der koden er pakket sammen med alle dens avhengigheter inkludert i en lett virtualisert form isolert fra resten av miljøet. Med andre ord kan du virtualisere en del av systemet ditt og dele det.

Dette tillater høye nivåer av portabilitet da disse containerne kan kjøres raskt og pålitelig fra ett miljø til et annet. Containere revolusjonerte cloud computing ved å gi en grense på applikasjonsnivå. Denne isolasjonen betyr at hvis noe går galt med beholderen, er det bare den som påvirkes og ikke resten av serveren.

Cloud computing og containere har gitt opphav til et nytt paradigme innen tjenestelevering kalt `Microservices`. Detaljene rundt dette er utenfor vårt omfang. En sterkt forenklet forklaring er at du deler opp din monolittiske applikasjon i flere tjenester som kjører uavhengig av hverandre. Disse tjenestene kan kommunisere internt med hverandre eller omverdenen.

Docker har blitt standarden for å lage og distribuere containere, men det er ikke det eneste verktøyet. Suksessen har ført til et partnerskap med Micrsoft for å bringe Docker-beholdere til Windows gjennom [Docker Desktop](https://www.docker.com/products/docker-desktop) -applikasjonen, ved å utnytte Hyper-V (Windows Hypervisor) eller nylig med Windows Subsystem for Linux 1 og 2 ( [WSL2](https://devblogs.microsoft.com/commandline/wsl2-will-be-generally-available-in-windows-10-version-2004/) ).

> For formålet med dette kurset, installer Docker Desktop-applikasjonen.
> 

> Mikrotjenester er utenfor omfanget av dette kurset, men du oppfordres til å lese deg opp om dem, da de er sterkt knyttet til containere og cloud computing.
> 

## Docker-arkitektur

Docker bruker en klient-server-arkitektur. Klienten snakker med Docker- *daemonen,* som er ansvarlig for å bygge, kjøre og distribuere Docker-containere. Klienten og daemonen kan eksistere på samme datamaskin, men det er ikke nødvendig - da de kommuniserer ved hjelp av en REST API over UNIX-sockets eller et nettverksgrensesnitt.

### Docker -daemon

Docker-daemonen ( `dockerd`) lytter etter Docker API-forespørsler og administrerer Docker-objekter som bilder, containere, nettverk og volumer. En daemon kan også kommunisere med andre dademoner for å administrere Docker-tjenester. Dette er en bakgrunnsoppgave når Docker Desktop er installert.

### Docker-klient

Docker-klienten ( `docker`) er den primære måten mange brukere samhandler med Docker. Når du bruker kommandoer som `docker run`, sender klienten disse kommandoene til `dockerd`, som utfører dem. `Docker` -kommandoen bruker Docker API. Docker-klienten kan kommunisere med mer enn én daemon. Når du installerer `Docker Desktop`får du tilgang til alle CLI-kommandoene nevnt her, og [mange flere](https://docs.docker.com/engine/reference/commandline/docker/) .

### Docker-registre

Et Docker-register lagrer Docker-bilder. [Docker Hub](https://hub.docker.com/) er et offentlig register som alle kan bruke, og Docker er konfigurert til å se etter bilder på Docker Hub som standard. Det er her det meste av basisbildet du skal bruke lagres.

Når du bruker kommandoene `docker pull`eller `docker run`, hentes de nødvendige bildene fra det konfigurerte registret. Når du bruker `docker push`-kommandoen, blir bildet ditt sendt til det konfigurerte registeret.

## Bilder og Docker-filer

Bilder er øyeblikksbilder av programvare som inneholder alle kravene for at en applikasjon skal kjøre som ment. Dette inkluderer instruksjoner om hvordan du starter, hvilke ressurser som trengs, ulike konfigurasjoner, miljøvariabler, biblioteker, den faktiske koden og nødvendige kjøretider. Disse bildene kan deretter kjøres for å lage en eller flere kjørende forekomster av den definerte applikasjonen og miljøet - dette er beholderen som ble diskutert tidligere.

Docker-bilder er uforanderlige, noe som betyr at de ikke kan endres, men kan kopieres, dette gir styrken til å være et konsistent element når man tester nye systemer. Dette gjør et Docker-bilde til en gjenbrukbar ressurs som kan distribueres på hvilken som helst vert og deles med andre utviklere gjennom et hvilket som helst containerregister (GitHub og GitLab har bestemmelser for containerregistre i tillegg til at Docker Hub er standard).

Som enhver kjørbar fil fra tradisjonell programmering, trenger et bilde en skriftlig fil for å forstå hva som kreves. Disse instruksjonene er definert i en Dockerfile , hvor alle instruksjonene nevnt ovenfor er skrevet, i [programmeringsspråket Go](https://golang.org/) .

> Detaljene til; hva som går inn i en Dockerfile, hvordan lage bilder fra disse filene, og kjøre forekomster av bildene som containere; er detaljert senere i leksjonen.
> 

## Beholdere vs virtuelle maskiner

På det mest grunnleggende nivået; containere og virtuelle maskiner har lignende ressursisolering og allokeringsfordeler, men fungerer annerledes fordi containere virtualiserer operativsystemet i stedet for maskinvare. Dette betyr at containere er mer bærbare og effektive.

![Untitled](4%20Containerisering%20med%20Docker%20898ff89692984a9eafbdde01825f15d1/Untitled.png)

![Untitled](4%20Containerisering%20med%20Docker%20898ff89692984a9eafbdde01825f15d1/Untitled%201.png)

*Struktur av virtuelle maskiner (venstre) vs containere (høyre)*

For å fordype oss i uttalelsen ovenfor kan vi starte med `virtuelle maskiner`(VM). Som nevnt ovenfor er VM-er en abstraksjon av fysisk maskinvare, som gjør en server til mange servere. Hypervisoren lar flere VM-er kjøre på en enkelt maskin. Hver VM inkluderer en full kopi av et operativsystem, applikasjonen, nødvendige binærfiler og biblioteker – som tar opp mye plass (vanligvis GB). Bortsett fra de ekstra plasskravene, er VM-er vanligvis trege med å starte opp ettersom de kjører på en del av den tilgjengelige maskinvaren, for ikke å nevne kostnadene forbundet med å ha flere lisenser for programvare hvis VM-en ikke kjører på en åpen kildekode -plattform som Linux.

`Containere` er en abstraksjon i applaget som pakker kode og avhengigheter sammen. Flere containere kan kjøres på samme maskin og dele OS-kernelen med andre containere, som hver kjører som isolerte prosesser i brukerområdet. Beholdere tar opp mindre plass enn VM-er (container-bilder er vanligvis titalls MB store), kan håndtere flere applikasjoner og krever færre VM-er og operativsystemer.

> I praksis brukes containere og virtuelle maskiner ofte sammen. Der en VM leies fra en skyleverandør og containere deretter vert på den. Det er langt mer effektivt enn å ha en dedikert VM for hver tjeneste.
> 

## Hvorfor bruke containere?

> Denne delen kan føles noe repeterende, siden den er en oppsummering av fordelene ved å bruke containere og hva de kan gi til virksomheten din.
> 

**Isolering**

En stor fordel med containerteknologi er isolering av ressurser: RAM, prosesser, enheter og nettverk er virtualisert på operativsystemnivå, og applikasjonene er isolert fra hverandre. Dette betyr at du ikke trenger å bekymre deg for avhengighetskonflikter eller delte ressurser, fordi hver applikasjon har definerte begrensninger i bruken av ressurser. I tillegg, takket være isolasjon, er beskyttelsesnivået høyere.

**Økning av produktivitet**

En annen fordel med containere, kanskje den mest åpenbare, er muligheten til å være vert for en stor mengde containere også på PC eller bærbar PC. Dette gjør at det alltid er et distribusjons- og testmiljø tilgjengelig for enhver applikasjon - en langt dyrere operasjon med virtuelle maskiner.

Denne typen teknologi resulterer i en økning av utviklernes produktivitet, takket være fjerning av avhengigheter og konflikter mellom ulike tjenester. Hver beholder kan være vert for en applikasjon eller en enkelt del av den, og som vi så ovenfor, er den isolert fra andre beholdere. På denne måten kan utviklere glemme synkronisering og avhengigheter for enhver tjeneste, i tillegg til at de står fritt til å kjøre oppdateringene uten å bekymre seg for mulige problemer mellom komponentene.

**Enkel utplassering og kortere oppstartstider**

Hver beholder inkluderer ikke bare applikasjonen, men også alle pakkene som brukes til å kjøre den. Dette forenkler enhver distribusjonsoperasjon og forenkler distribusjonen på forskjellige operativsystemer uten ytterligere konfigurering. I tillegg til dette gir virtualisering av kun operativsystemet langt kortere oppstartstider.

**Sammenhengende miljø**

Takket være den standardiserte tilnærmingen, muliggjør containere portabilitet av ressurser ved å redusere problemer med forskyvning av applikasjoner gjennom syklusen med utvikling, testing og produksjon. Containere kan distribueres på en enkel og sikker måte uavhengig av miljøet. Det er ikke nødvendig å konfigurere servere manuelt, og nye funksjoner kan slippes lettere.

**Driftseffektivitet**

Med containere kan du kjøre flere applikasjoner på samme instans og spesifisere riktig mengde ressurser som skal brukes, ved å sikre optimalisering av dem. Containere er lettere enn VM og gjør systemet mer smidig, ved å øke driftseffektiviteten, utviklingen og administrasjonen av applikasjoner.

**Versjonskontroll**

Containerteknologi gjør det mulig å administrere versjonene av applikasjonskoden og dens avhengigheter. Du kan holde styr på containerversjonene, analysere forskjellen mellom dem og til slutt komme tilbake til forrige versjon.

## Vanlige Docker CLI-kommandoer

> Mye av containermanipulasjonen av kjørende containere kan gjøres direkte i Docker Desktop GUI.
> 

For hver kommando brukes følgende mal:

- Tittel
- Kort beskrivelse av hva kommandoen gjør
- Kommandomal
- Tabell som viser ofte brukte ALTERNATIVER (hvis aktuelt)
- Eksempel på kommando (hvis aktuelt)

### Generell

Disse kommandoene brukes til å samle informasjon om Docker på vertssystemet.

**Info**

Viser informasjon om den installerte versjonen av Docker, den kan enkelt brukes til å sjekke om du har Docker installert.

```docker
docker info
```

**Version**

Viser den gjeldende installerte versjonen av Docker.

```docker
docker version
```

### Opprettelse

Disse kommandoene er opptatt av å lage bilder og beholdere.

**Build**

Bygg et bilde fra en Dockerfile . Denne kommandoen bygger Docker-bilder fra en Dockerfil og en “kontekst”. En builds kontekst er settet med filer som ligger i den angitte `PATH`eller `URL`.

```docker
docker build [OPTIONS] PATH | URL | -
```

Alternativer:

[Untitled](https://www.notion.so/005eecdbc0ed43aba68ab91cb10c71df)

Eksempler:

1. Bygg Dockerfilen i gjeldende directory

```
docker build .
```

1. Bygg en dockerfil med et GIT-repo

```
docker build gitlab.com/noroff-accelerate/dotnet/hello-docker
```

Dette vil klone GitLab- depotet og bruke det klonede repoet som kontekst. Dockerfilen i roten av repoet brukes som Dockerfile .

1. Merk et bilde

```
docker build -t nicholaslennox/dockerdemoapi:1.0 .
```

Dette vil lage et Docker-bilde kalt `nicholaslennox/dockerdemoapi`med versjonen satt til `1.0`, ved å bruke Dockerfilen i gjeldende kontekst.

> Navnekonvensjonen for Docker-bilder er i malen <Your Docker ID>/<Repository Name>:<tag>. Dette gjør at bilder enkelt kan deles gjennom Docker Hub og andre registre . <tag>er vanligvis versjonen av bildet, selv om det ikke trenger å være det, hvis det står tomt, er det som standard siste.
> 

Ved å bruke disse navnekonvensjonene fra starten starter bildene dine slik at de enkelt kan deles. Merk at det ikke er opprettet noe dockerdemoapi- repo ennå i Docker Hub, men når bildet skyves, har det allerede et passende navn. Selv om det ikke er noen planer om å plassere beholderen i et register, hjelper bruk av disse navnekonvensjonene uansett å organisere dine lokale bilder. I denne situasjonen kan `<Repository Name>`bare være hva du vil kalle Docker-bildet.

> Denne byggemetoden vil være den vanligste vi bruker. De andre vises for en bredere kontekst av de tilgjengelige alternativene.
> 

Du kan bruke flere tagger på et bilde. Du kan for eksempel bruke den “latest” taggen på et nybygd bilde og legge til en annen kode som refererer til en bestemt versjon. For å merke et bilde både som `nicholaslennox/dockerdemoapi:latest`og `nicholaslennox /dockerdemoapi:v2.1`bruker du følgende:

```
docker build -t nicholaslennox/dockerdemoapi:latest -t nicholaslennox/dockerdemoapi:v2.1 .
```

Dette kan brukes til å spore versjoner av bildene dine for enkelt å se en historikk og som er den nyeste versjonen uten å måtte konsultere ekstern dokumentasjon .

1. Angi en dockerfil

Dette er når du har flere Dockerfiler i samme kontekst, `-f`-taggen lar deg konfigurere hvilken som er bygget.

```
docker build -f dockerfiles/Dockerfile.debug -t myapp_debug .
docker build -f dockerfiles/Dockerfile.prod  -t myapp_prod .
```

Kommandoene ovenfor vil bygge gjeldende byggekontekst (som spesifisert av `.`) to ganger, en gang ved å bruke en feilsøkingsversjon av en Dockerfile og en gang ved å bruke en produksjonsversjon.

**Run**

Denne kommandoen brukes til å lage en beholder fra et bilde. Den `oppretter først`en beholder og `starter`den deretter ved å bruke de angitte kommandoene.

```
docker run [OPTIONS] IMAGE [COMMAND] [ARG...]
```

Alternativer:

[Untitled](https://www.notion.so/ac3f3965572244a8b904e3b8513dd5fc)

> Det er mye [mer](https://docs.docker.com/engine/reference/commandline/run/) enn bare oppført her.
> 

De fleste av argumentene som vises her kan forhåndskonfigureres i Dockerfilen . Derfor blir `miljøvariabler`, `nettverk`og `volumer`diskutert i delen om `å skrive en Dockerfile`.

> Du trenger IKKE å legge til alle disse kommandoene for å kjøre en container, de kan være veldig enkle.
> 

Eksempler:

1. Enkel kjørende beholder fra eksisterende bilde

```
docker run nicholaslennox/dockerdemoapi:latest
```

Denne kommandoen oppretter ganske enkelt en container (kjørende forekomst) av `nicholaslennox/dockerdemoapi-`bildet vårt med den `latest` taggen. Docker vil generere et navn for denne beholderen, du kan se dette ved å se i Docker Desktop og inspisere løpende container, eller bruk `docker ps`for å se alle kjørende forekomster.

1. Videresending av utsatte porter

```
docker run -p 8080:80 nicholaslennox/dockerdemoapi:latest
```

Denne kommandoen oppretter en container fra det samme bildet som før, bortsett fra at denne gangen publiserer (sender) den eksponerte porten `80`fra containeren til port `8080`på verten. Verten er datamaskinen vår.

> Mal for portvideresending (publisering) er <vertsport>:<eksponert containerport>.
> 

Den eksponerte porten `80`er definert i Dockerfilen, du kan definere den på docker run-kommandoen med `--expose`, men det er mye enklere å gjøre det inne i Dockerfilen .

> Dette er den vanligste docker run -kommandoen vi vil bruke i dette kurset. De andre skal gi en bredere kontekst.
> 
1. Gi beholderen et navn

```
docker run -p 8080:80 --name demo-api nicholaslennox/dockerdemoapi:latest
```

Dette vil tvinge Docker til å navngi beholderen dmeo-api og ikke bruke den automatiske genereringen utført.

1. Legger til en omstartspolicy

```
docker run -p 8080:80 --name demo-api --restart=on-failure nicholaslennox/dockerdemoapi:latest
```

- `--restart` flagget forteller Docker hvordan beholderen starter på nytt, retningslinjene er vist nedenfor.

[Untitled](https://www.notion.so/b39c3561d9b14cd1ada2dedace795447)

> `Arbeidskataloger`, `volumer`, `miljøvariabler` og `nettverkskonfigurasjoner`er utenfor omfanget av denne leksjonen, men vil bli nevnt svært lite i deler av leksjonen.
> 

**Bilder**

Liste bilder. Som standard vil dette vise alle bilder på toppnivå , deres repo og tagger, og deres størrelse.

```
docker images [OPTIONS] [REPOSITORY[:TAG]]
```

Alternativer:

[Untitled](https://www.notion.so/d8a25e814b7342599a086eeb7134519a)

1. Rett frem eksempel, liste opp alle bildene

```
docker images
```

1. Vis docker-bilder etter `name:tag`  ``` docker images java

REPOSITORY TAG IMAGE ID CREATED SIZE
java 8 308e519aac60 6 dager siden 824,5 MB 
java 7 493d82594c15 3 måneder siden 656.3 MB 
java siste 2711b1d6f3aa 5 måneder siden 603,9 MB

```
Hvis "tag" utelates, vil alle bilder som samsvarer med det repoet bli oppført.

3. Ved å bruke `--filter` har formatet `key=value`.

Hvis det er mer enn ett filter, sender du flere flagg (f.eks. `--filter "foo=bar" --filter "bif=baz"`).
```

REPOSITORY TAG IMAGE ID CREATED SIZE
<ingen> <ingen> 48e5f45168b9 4 uker siden 2.489 MB
<ingen> <ingen> 980fe10e5736 12 uker siden 101,4 MB
<ingen> <ingen> dea752e4e117 12 uker siden 101,4 MB

```
De for øyeblikket støttede filtrene er:

- dangling (boolsk - "true" eller "false")
- label (`label=<key>` eller `label=<key>=<verdi>`)
- before (`<image-name>[:<tag>]`, `<image id>` eller `<image@digest>`) - filter bilder opprettet før gitt ID eller referanser
- since (`<bildenavn>[:<tag>]`, `<bilde-id>` eller `<bilde@digest>`) - filter bilder opprettet etter gitt id eller referanser
- reference (mønster av en bildereferanse) - filter bilder hvis referanse samsvarer med det angitte mønsteret.

**Ps**

List beholdere.

```shell
docker ps [OPTIONS]
```

Alternativer:

[Untitled](https://www.notion.so/5f543d2005f446ea91cf6a1f111bfe1c)

## Bruke Docker

> Denne delen er et sammendrag av hvilke kommandoer som trengs for å lykkes med containerisering av en applikasjon.
> 
1. Last ned Docker fra den [offisielle nettsiden](https://www.docker.com/products/docker-desktop)
2. Opprett en fil i rotkatalogen til prosjektet ditt kalt `Dockerfile`(ingen filtype) med følgende tekst

```
FROM openjdk:15
VOLUME tmp
ADD target/<build-jar-name>.jar <docker-jar-name>.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]
```

`FROM`spesifiserer Docker-bildet som vi skal bruke for applikasjonen vår. En liste over tilgjengelige bilder finner du [her](https://hub.docker.com/) .

Vi fortalte Docker at applikasjonen vår trenger OpenJDk for å kjøre, dette er *basisbildet* . Vi la ut applikasjon på toppen av dette bildet, men resultatet ble et *nytt* docker-bilde som nå er vår applikasjon.

Fordi `OpenJDK`-bildet er uforanderlig (vi kan ikke gjøre endringer i det), trenger vi faktisk ikke å kopiere det rundt med applikasjonen vår. Når vi distribuerer applikasjonen vår et sted, vil verten laste ned basis `OpenJDK`-bildet for seg selv.

Når applikasjonen vår startes opp (kjører) oppretter verten en beholder som den kan kjøres inne i, dette er en **KOPI** av bildet.

Dette er en veldig effektiv måte å distribuere servere på, og gjør at flere forekomster kan kjøres veldig enkelt, men det skaper noen komplikasjoner. En du vil møte er at du ikke kan gjøre lokale filendringer som lagres permanent.

`ADD`ber Docker om å kopiere .jar-artefakten opprettet av prosjektet vårt i målmappen til rotkatalogen til applikasjonen vår

`ENTRYPOINT`beskriver kommandoen som Docker vil kjøre på .jar-filen vår etter at den har blitt kopiert

`VOLUME`spesifiserer en tildelt lagringsplass som holder dataene generert av våre Docker-beholdere permanent. I dette tilfellet genereres og lagres en katalog kalt `tmp`på vertsmaskinen og administreres fullstendig av Docker. Dette volumet kan brukes av andre beholdere til å dele informasjon.

> `VOLUME` er utenfor omfanget for våre formål, det er her for å gi deg en kort forklaring på det, da det er noe du mest sannsynlig vil se når du jobber med Docker. Mer finner du ut her .
> 
1. Kjør følgende kommando i terminalen:

```bash
docker build -t <your docker tag name here> .
```

1. For å kjøre forekomsten av Docker-bildet ditt, skriv inn følgende kommando i terminalen:

```bash
docker run -p <port number>:<portnumber> <your docker tag name here>
```

Vi har nå opprettet og kjørt en Docker-beholder. For å liste opp alle kjørende beholdere eller alle eksisterende beholdere sammen med relevant informasjon om dem, bruk følgende kommandoer:

```bash
docker ps
docker ps -a
```

Dockers skrivebordsdashbord gir oss muligheten til å stoppe og starte forekomstene av containerne våre. Det tilsvarer å kjøre følgende kommandoer fra terminalen:

```bash
$ docker container start <container id>
$ docker container stop <container id>
```

> Det er en Docker-plugin for Intellij som kan “generere” disse filene for deg. Det anbefales å ikke bruke dette verktøyet da det genereres mange unødvendige kommandoer og kan gjøre feilsøking vanskelig.
> 

## Heroku

Heroku er en plattform som en tjeneste (PaaS) som lar oss bygge, kjøre og distribuere applikasjonene våre på virtuelle containere kalt dynos. Den har en generøs gratis abonnementsmodell som vi vil dra nytte av for læringsformål.

> Som et teknisk notat vil Heroku-applikasjoner kjøre på AWS-servere.
> 

### Distribuere en applikasjon

> Disse trinnene forutsetter en Spring Boot-applikasjon, men vil også fungere for [node](https://nodejs.org/en/) og [andre](https://devcenter.heroku.com/) typer applikasjoner.
> 

Slik setter du opp en Heroku-app med Spring Boot:

1. Registrer deg på [Heroku-nettstedet](https://signup.heroku.com/)

> En gratis konto lar deg lage opptil 5 applikasjoner og bruke databaser med opptil 10 000 poster. Du kan sjekke applikasjonene dine via [Heroku Dashboard](https://dashboard.heroku.com/apps)
> 
1. Installer [Heroku CLI-verktøyene](https://devcenter.heroku.com/articles/heroku-cli) . Før du fortsetter til neste trinn, sørg for at Heroku CLI-installasjonen er fullført ved å kjøre følgende kommando i terminalen.

```bash
heroku
```

> En liste over Heroku-kommandoer vil vises hvis CLI er riktig konfigurert.
> 
1. Logg på Heroku-tjenesten ved å bruke følgende terminalkommando (Merk: dette har vært kjent for å mislykkes når du bruker GitBash for Windows, du må bruke Windows-kommandolinjen):

```
heroku login
```

1. Bruk terminalen, naviger til rotkatalogen til Spring Boot-applikasjonen og skriv inn følgende kommando:

```
heroku create
```

> `create` er en forkortelse for apps:create
> 

Hvis opprettet vellykket, skal to URL-er vises til terminalen. Den første er lenken til forekomsten av applikasjonen som er vert på Heroku. Den andre peker på det eksterne git-repoet for applikasjonen som er automatisk opprettet på plattformen. Etter å ha oppdatert dashbordet på Heroku-nettstedet, skal den nye applikasjonen din vises.

1. Som standard vil Heroku tildele et tilfeldig navn til applikasjonen din. For å gi nytt navn, skriv inn følgende kommando:

```
heroku apps:rename --app <old-app-name> <desired-app-name>
```

Dette vil endre både endepunktet til applikasjonen så vel som URL-en til det eksterne git-depotet.

> Det er mulig å navngi applikasjonen direkte når du oppretter den, men dette navnet må være unikt globalt:
`heroku create <app-navn>`
> 

1. Mens du fortsatt er i rotkatalogen til applikasjonen din, lag et lokalt git-repo med følgende kommandoer:

```
git init
```

```
git add .
```

```
git commit -m "initial commit"
```

Disse kommandoene vil initialisere det lokale depotet, legge til alle nødvendige filer og forplikte dem. Vi må da skyve vårt lokale depot til Heroku.

```
git remote add heroku <link to your remote git repository on heroku>
```

```
git push heroku master
```

Heroku-arbeidere vil begynne å bygge applikasjonen automatisk. Etter en vellykket bygging bør du kunne navigere til en live-forekomst av applikasjonen din.

> MERK: Dette kan ta noen sekunder å reflektere etter bygging.
> 

Det kan hende du må legge til en system.properties -fil i rotkatalogen til prosjektet med følgende tekst for å kunne distribuere på Heroku.

```
java.runtime.version=15
```

> Når du kjører en applikasjon på Heroku (med eller uten Docker), er applikasjonen alltid en kontainer, og disse vil slå seg av hvis de ikke brukes i 15 minutter. Du vil legge merke til at en applikasjon kan ta noen minutter å starte på nytt hvis den har blitt stengt .
> 

**Sjekke opp på serveren din**

Når som helst, mens du er i en Heroku-app, kan du se de siste serverloggene med:

```bash
heroku logs
```

Men hvis du vil se i sanntid, kan du bruke følgende kommando:

```bash
heroku logs --tail
```

> Trykk `Ctrl-C` for å avslutte.
> 

Disse kommandoene vil vise de siste meldingene som programmet har skrevet ut til konsollen. Hvis du kobler til rett etter å ha gjort et push, kan du se Heroku-arbeiderne bygge applikasjonen din.

**Heroku-regionen**

Som standard vil offentlige applikasjoner bli opprettet i `us`- regionen, men dette kan spesifiseres (når du oppretter applikasjonen), men legg til flagget `--region <region>`til kommandoen din.

```bash
heroku create --region eu
```

Selv om dette vil sette opp applikasjonen i den angitte regionen, kan deler av Heroku-plattformen fortsatt distribueres til andre regioner, for eksempel PostGres- databaser eller andre tilpassede tillegg.

Du kan enkelt se de tilgjengelige regionene ved å kjøre følgende kommando:

```bash
heroku regions
```

Hvis du ikke bruker en privat applikasjon, vil du være begrenset til to alternativer: `us`eller `eu`.

### Docker på Heroku

Før du distribuerer Docker-bildet vårt til Heroku, må du sette prosjektet ditt til å bruke Heroku-tilordnet port i stedet for standard 8080-porten som brukes med Tomcat. Naviger til `src/main/java/resources/application.properties`og legg til følgende kodelinje.

```
server.port=${PORT:8080}
```

> Applikasjonen vil bruke `PORT` -miljøvariabelen hvis den er tilgjengelig (som på Heroku), ellers vil den som standard være `8080`
> 

Når du har gjenoppbygd løsningen, gjør du følgende:

1. Naviger til rotkatalogen til applikasjonen din og logg på Heroku ved hjelp av CLI-verktøyene
2. Skriv inn følgende kommandoer i terminalen:

```bash
heroku container:login
```

```bash
heroku create
```

Dette vil opprette en ny app på Heroku, som beskrevet i forrige avsnitt.

```bash
heroku container:push web --app <heroku-app-name>
```

Dette vil bygge bildet og skyve det til Heroku. Hvis det lykkes, bør du kunne navigere til applikasjonen via nettleseren din.

## Ressurser

> [Docker Desktop](https://www.docker.com/products/docker-desktop)
> 

> [Docker CLI-kommandoer](https://docs.docker.com/engine/reference/commandline/docker/)
> 

> [Docker Hub](https://hub.docker.com/)
> 

Copyright 2021, Noroff Accelerate AS