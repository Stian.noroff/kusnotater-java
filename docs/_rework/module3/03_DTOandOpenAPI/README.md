# 3. OpenAPI og dataoverføringsobjekter

# OpenAPI og dataoverføringsobjekter

I denne leksjonen vil du lære om riktig REST API-dokumentasjon med OpenAPI/Swagger, samt hvordan du kan koble fra klienten og domenet på riktig måte ved å bruke DTOer og en avhengighet kalt Map Struct.

## Åpne API

Å dokumentere et REST API er en utfordrende oppgave. Det er mange verktøy for å hjelpe med denne prosessen. Et populært verktøy er **Swagger** . Den går under navnet **OpenAPI** og konverterer automatisk kontrollere, metoder og modeller til et brukergrensesnitt som du kan samhandle med.

For å aktivere Swagger/OpenAPI må du få avhengigheten:

```
implementation 'org.springdoc:springdoc-openapi-ui:1.6.8'
```

Dette vil generere en html-side på url `localhost:8080/swagger-ui.html`:

![Untitled](3%20OpenAPI%20og%20dataoverf%C3%B8ringsobjekter%20beac8659d99f4459b730ca3457d385bc/Untitled.png)

Swagger har skannet etter alle REST-kontrollerne og modellene som brukes i dem. Den oppretter deretter en seksjon for hver kontroller og hver metode i den kontrolleren.

Denne konfigurasjonen gjør mange forutsetninger, som kan sees når du utvider noen av endepunktene. Skjermbildet nedenfor illustrerer de genererte svarene for `POST: /api/v1/students`endepunkt:

![Untitled](3%20OpenAPI%20og%20dataoverf%C3%B8ringsobjekter%20beac8659d99f4459b730ca3457d385bc/Untitled%201.png)

Dette er ikke riktig, ettersom en POST-forespørsel skal ha et `201 CREATED-`svar. Videre representerer Skjema-delen alle modellene. Husk at studenten vår hadde en midlertidig løsning ved å bruke `JsonGetter`for å forhindre de sykliske referansene. Dette betyr at objektet er serialisert annerledes, skjermbildet nedenfor viser studentskjemaet:

![Untitled](3%20OpenAPI%20og%20dataoverf%C3%B8ringsobjekter%20beac8659d99f4459b730ca3457d385bc/Untitled%202.png)

På grunn av forenklingen av domenemodellen bruker eksempel POST- og PUT-forespørsler følgende mal:

![Untitled](3%20OpenAPI%20og%20dataoverf%C3%B8ringsobjekter%20beac8659d99f4459b730ca3457d385bc/Untitled%203.png)

Hva skjer hvis vi ikke har en forenklet modell og bruker enheten direkte? Skjermbildet nedenfor viser skjemaet for en professor:

![Untitled](3%20OpenAPI%20og%20dataoverf%C3%B8ringsobjekter%20beac8659d99f4459b730ca3457d385bc/Untitled%204.png)

Dette er en veldig oppblåst representasjon som inneholder mange irrelevante aspekter og dupliseringer. For å se hvordan dette påvirker brukervennligheten til den genererte dokumentasjonen, kan vi se på malene Swagger vil generere. Skjermbildet nedenfor viser hva Swagger forventer når en ny professor legges til:

![Untitled](3%20OpenAPI%20og%20dataoverf%C3%B8ringsobjekter%20beac8659d99f4459b730ca3457d385bc/Untitled%205.png)

Dette er en unødvendig stor gjenstand. Ved å ha dette vist som vår representasjon, forteller vi enhver klient at de må oppgi alle detaljer om en professor (inkludert den autogenererte IDen), fagene de underviser og studentene de veileder.

*Tenk på hvordan du til og med begynner å designe et skjema for dette.*

Det kan hevdes at dette ikke er et problem for et internt system, der utviklerne forstår domenet. Hvor de forstår å ikke sende hele objektet, men bare ID-ene. Dette er imidlertid ikke en unnskyldning for dårlig design, ethvert system bør designes godt fra grunnen av for å lette vedlikeholdet.

Dette problemet løses ved å bruke *dataoverføringsobjekter* . Det er en senere del trukket fra dette, så vi går videre inntil videre.

### Konfigurasjon

Å endre hvordan Swagger er konfigurert kan gjøres på flere måter. Den første tingen å endre ville være en gruppering av endepunktene sammen. Dette kan gjøres ved å legge til følgende linje i `applications.properties`:

```
springdoc.swagger-ui.operationsSorter=method
```

Dette resulterer i at dokumentasjonen ordnes på følgende måte:

![Untitled](3%20OpenAPI%20og%20dataoverf%C3%B8ringsobjekter%20beac8659d99f4459b730ca3457d385bc/Untitled%206.png)

Det neste trinnet er å legge til noen beskrivelser til endepunktene. Dette gjøres ved å legge til `@Operation`til hver metode og tilpasse `summary`-egenskapen:

```java
@Operation(summary = "Get all students")
@GetMapping // GET: localhost:8080/api/v1/students
public ResponseEntity<Collection<Student>> getAll() {
    return ResponseEntity.ok(studentService.findAll());
}
```

Dette resulterer i at følgende genereres:

![Untitled](3%20OpenAPI%20og%20dataoverf%C3%B8ringsobjekter%20beac8659d99f4459b730ca3457d385bc/Untitled%207.png)

Et endepunkt kan også merkes som utdatert med:

```java
@Operation(summary = "Get all students", deprecated = true)
```

Dette resulterer i følgende:

![Untitled](3%20OpenAPI%20og%20dataoverf%C3%B8ringsobjekter%20beac8659d99f4459b730ca3457d385bc/Untitled%208.png)

Det neste trinnet er å konfigurere alle svarkodene som endepunktet kan returnere. Dette gjøres ved å konfigurere en `@ApiResponses-`kommentar. Følgende kodebit viser konfigurasjonen for en PUT-forespørsel:

```java
@Operation(summary = "Updates a student")
@ApiResponses( value = {
    @ApiResponse(responseCode = "204", 
            description = "Student successfully updated", 
            content = @Content),
    @ApiResponse(responseCode = "400", 
            description = "Malformed request", 
            content = @Content),
    @ApiResponse(responseCode = "404", 
            description = "Student not found with supplied ID", 
            content = @Content)
})
@PutMapping("{id}") // PUT: localhost:8080/api/v1/students/1
public ResponseEntity update(@RequestBody Student student, @PathVariable int id)
```

Dette resulterer i følgende dokumentasjon generert:

![Untitled](3%20OpenAPI%20og%20dataoverf%C3%B8ringsobjekter%20beac8659d99f4459b730ca3457d385bc/Untitled%209.png)

`Innholdsegenskapen`definerte hva som vises som et eksempelsvar . Hvis ingen respons (blank) er ønsket, kan du angi `@Content`som verdien.

For endepunkter der svar inneholder data (GET-forespørsler) kan skjemaet defineres. For å få en student etter ID kan for eksempel konfigureres som følger:

```java
@Operation(summary = "Get a student by ID")
@ApiResponses(value = {
    @ApiResponse(responseCode = "200",
            description = "Success",
            content = { @Content(mediaType = "application/json",
            schema = @Schema(implementation = Student.class)) }),
    @ApiResponse(responseCode = "404",
            description = "Student does not exist with supplied ID",
            content = @Content)
})
@GetMapping("{id}") // GET: localhost:8080/api/v1/students/1
public ResponseEntity<Student> getById(@PathVariable int id)
```

`schema = @Schema(implementation = Student.class))`er linjen som instruerer Swagger om å bruke `Student`-klassen som eksempelresponsobjekt. Dette resulterer i følgende svarmaler:

![Untitled](3%20OpenAPI%20og%20dataoverf%C3%B8ringsobjekter%20beac8659d99f4459b730ca3457d385bc/Untitled%2010.png)

Dokumentasjonen tar form, et aspekt som ikke er helt korrekt er at vi definerer 400 og 404 med blanke svar, men hvis vi får disse feilene, gir Spring en standard feilhåndteringsklasse (som vi brukte tidligere):

![Untitled](3%20OpenAPI%20og%20dataoverf%C3%B8ringsobjekter%20beac8659d99f4459b730ca3457d385bc/Untitled%2011.png)

Dette er ikke et blankt svar, som kan forårsake konflikter med implementerende klienter.

### Tilpasset feilhåndtering

Det er ikke trivielt å finne modellen som Spring bruker da den er ganske dypt inne i rammeverket og skjult av et grensesnitt ( [ErrorAttributes-grensesnitt](https://docs.spring.io/spring-boot/docs/current/api/org/springframework/boot/web/reactive/error/ErrorAttributes.html) ). Dette problemet kan løses på to måter:

- Lag en dummy-modell som representerer strukturen
- Implementer *kontrollerråd* og tilpasset *feilhåndtering*

Den første løsningen er en “quick fix” som ikke er den ideelle løsningen. For demonstrasjonens skyld kan det rettes opp på denne måten med følgende kode:

```java
public class ApiErrorResponse {
    private String timestamp;
    private Integer status;
    private String error;
    private String trace;
    private String message;
    private String path;
    // Getters and Setters omitted
```

```java
@Operation(summary = "Get a student by ID")
@ApiResponses(value = {
    @ApiResponse(responseCode = "200",
            description = "Success",
            content = { @Content(mediaType = "application/json",
            schema = @Schema(implementation = Student.class)) }),
    @ApiResponse(responseCode = "404",
            description = "Student does not exist with supplied ID",
            content = { @Content(mediaType = "application/json",
            schema = @Schema(implementation = ApiErrorResponse.class)) }) //HERE
})
@GetMapping("{id}") // GET: localhost:8080/api/v1/students/1
public ResponseEntity<Student> getById(@PathVariable int id)
```

Når vi har satt skjemaet til å være den tilpassede ErrorResponse-klassen, får vi følgende genererte dokumentasjon:

![Untitled](3%20OpenAPI%20og%20dataoverf%C3%B8ringsobjekter%20beac8659d99f4459b730ca3457d385bc/Untitled%2012.png)

Dette gir ønsket utgang, men denne løsningen er mer en “få det til å fungere”-fiks. En alternativ implementering bruker en nyere funksjon kalt `ControllerAdvice`. Denne prosessen er bedre atskilt, men krever mye konfigurasjon.

> **MERK** : Implementering av ControllerAdviceer et ekstra innhold fra kurset. Det er ikke påkrevd og vil ikke bli dekket under den underviste leksjonen.
> 

> **RESSURSER** : Artikler om kontrollråd, [her](https://thepracticaldeveloper.com/custom-error-handling-rest-controllers-spring-boot/) , [her](https://www.toptal.com/java/spring-boot-rest-api-error-handling) , [her](https://www.baeldung.com/global-error-handler-in-a-spring-rest-api) .
> 

## Dataoverføringsobjekter (DTOer)

I følge Martin Fowler er et dataoverføringsobjekt (DTO):

*Et objekt som bærer data mellom prosesser for å redusere antall metodekall.*

De er anvendelige i mange scenarier og er generelle klasser for å frakte informasjon mellom lag i en applikasjon. De kan være sammendrag av domeneobjekter, eller aggregering av flere domeneobjekter, eller til og med ganske enkelt meldinger som skal sendes rundt.

Et dataoverføringsobjekt er kjent som en *fasade* og er derfor i de fleste tilfeller involvert i en implementering av fasademønsteret. De må settes sammen et sted og av noe, vanligvis en slags *Mapper* . DTO-er er også veldig gode kandidater for serialisering og deserialisering av domeneobjekter, siden de ikke har noen av domenekonfigurasjonen til stede på disse modellene ( `@Entity`og så videre).

> **MERK** : DTO-er går også etter verdiobjekter eller visningsmodeller avhengig av konteksten deres.
> 

Når vi bruker konteksten til REST APIer og DTOer kommer vi til et veldig interessant sted. Modellene som representerer domenet til applikasjonen din og modellene som representerer dataene som håndteres av API-en din, bør være forskjellige bekymringer og bør være frikoblet fra hverandre. Du vil ikke ødelegge API-klientene dine når du legger til, fjerner eller gir nytt navn til et felt fra applikasjonsdomenemodellen.

Mens tjenestelaget ditt opererer over domene-/persistensmodellene, bør API-kontrollerne operere over et annet sett med modeller. Etter hvert som domene-/persistensmodellene dine utvikler seg for å støtte nye forretningskrav, for eksempel nye versjoner av API-modellene for å støtte endringer. Det kan også være lurt å avvikle de gamle versjonene av API-en din etter hvert som nye versjoner utgis. Dette er fullt mulig å oppnå når tingene er frakoblet.

> **MERK** : Bruk av @JsonGetterkobler kontrolleren til domenet, noe som gjør dette ikke mulig.
> 

Å inkludere DTOer og påfølgende kartlegging i søknaden din er både beregningsmessig kostbart og tidkrevende. Imidlertid tilbyr de noen virkelige fordeler:

- Koble utholdenhetsmodeller fra API-modeller.
- DTO-er kan skreddersys til dine behov, og de er gode når du kun eksponerer et sett med attributter til utholdenhetsenhetene dine.
- Ved å bruke DTO-er vil du unngå mange ekstra merknader i utholdenhetsenhetene dine (@JsonGetter, @JsonSetter, @JsonIgnore, ect..)
- Du vil ha full kontroll over attributtene du mottar når du oppretter eller oppdaterer en ressurs.
- Hvis du bruker Swagger, kan du bruke `@ApiModel-`og `@ApiModelProperty-`kommentarer for å dokumentere API-modellene dine uten å rote utholdenhetsenhetene dine. Dette gjør at de kan vises på de genererte dokumentene uten å være i metodene.
- Du kan ha forskjellige DTO-er for hver versjon av API-en din.
- Du vil ha mer fleksibilitet når du kartlegger relasjoner.
- Du kan ha forskjellige DTOer for forskjellige medietyper.
- DTO-ene dine kan ha en liste over lenker for HATEOAS.

Kort sagt: DTO-er tilbyr mange fordeler, men krever innsats og tid.

*Bør de implementeres fullt ut?*

Ideelt sett, ja, men dette er sjelden tilfelle. En god tommelfingerregel er å bruke dem der det trengs. Det er greit å ikke ha perfekt separasjon og å gjøre unødvendig arbeid for å strebe mot perfekt abstraksjon er ikke den beste tilnærmingen. Men når det blir problematisk å ikke bruke DTO-er, bør bryteren være en no brainer. Fleksibiliteten de tilbyr er verdt det for å unngå flere metodeanrop og oppblåsthet å jobbe med.

Du bør heller ikke ha DTOer med redundante attributter, dette oppstår ved å gjenbruke dem i ulike sammenhenger hvor det er ulike krav. En DTO bør aldri ha tomme felt med mindre selve dataene er tomme, noe som betyr at *hver forskjellig representasjon av data må innkapsles, og disse innkapslingene bør kun være for disse dataene* . Vi kan gjenbruke DTO-er hvis de to datastrukturene er de samme, som ofte er tilfellet for POST/PUT-forespørsler.

### Struktur

Når du bruker DTOer er det viktig å ha et klart formål med deres eksistens og hvilken rolle de spiller. En DTO er en POJO-lignende klasse som ikke har noen logikk innenfor, logikken for å lage DTO kommer fra Mapper. For å holde oss til vårt Postgrad-eksempel, hadde vi `Student -`domeneobjektet:

```java
@Entity
@Getter
@Setter
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 50, nullable = false)
    private String name;
    @OneToOne
    @JoinColumn(name = "project_id")
    private Project project;
    @ManyToOne
    @JoinColumn(name = "professor_id")
    private Professor professor;
    @ManyToMany
    @JoinTable(
            name = "student_subject",
            joinColumns = {@JoinColumn(name = "student_id")},
            inverseJoinColumns = {@JoinColumn(name = "subject_id")}
    )
    private Set<Subject> subjects;
}
```

Dette resulterer i et veldig stort skjema når det er integrert med Swagger, det nestede `prosjektet`, `professoren`og `emnene`er problemet. Dette resulterte i en stor oppblåsthet, og kan resultere i en stor mengde *overpostering* hvis domeneobjektet ble returnert til klienten på en GET-forespørsel. Dette ble delvis løst med `@JsonGetter`, men resulterer i flere merknader og konfigurasjon i enhetene våre, noe som er enestående.

*Hvordan kan vi opprette en DTO for å hjelpe oss her?*

Vi må spørre når en klient ønsker å FÅ en student, hva vil de se? I dette tilfellet ønsker vi ikke å representere de relaterte dataene i sin helhet, dette er noe som kan spørres om nødvendig. Som med `@JsonGetter`vil vi ganske enkelt gjøre de relaterte feltene om til enkle datatyper:

```java
@Data
public class StudentDTO {
    private int id;
    private String name;
    private int project;
    private int professor;
    private Set<Integer> subjects;
}
```

Her ønsker vi å representere de relaterte enhetene som deres ID-er. Dette kan endres avhengig av konteksten, du kan trenge en ID og et navn, eller en lenke eller mange andre formater. Eksemplet er enkelt for å illustrere poenget.

> **MERK** : Vi har kvittet oss med all enhetsrelatert konfigurasjon da det ikke er viktig for vår presentasjon av dataene.
> 

*Hvordan bør DTOer navngis?*

Husk at det tidligere ble diskutert at DTO-er trenger å representere forskjellige datainnkapslinger, noe som betyr at det kan være flere forskjellige `StudentDTO-er`. DTO-er bør navngis for å representere det de innkapsler, dette inkluderer formålet.

> **MERK** : Prøv å gjenbruke DTO-er så mye som mulig, ettersom å legge til flere øker mengden kartleggingslogikk og den generelle oppblåsningen av applikasjonen.
> 

Det er ingen regel på dette, men noen tilnærminger er å navngi dem: enhet + handling + DTO. For eksempel: `StudentGetDTO`. Andre tilnærminger fjerner DTO-etiketten til fordel for formålet, for eksempel: `StudentSummary`.

Det faktiske navnet spiller ingen rolle, det som betyr noe er hvor lett det er å se hensikten og jobbe med. I det minste bør de plasseres i en `dtos-`pakke i `modellpakken din`.

> **RESSURSER** : Dette StackOverflow-innlegget gir noen gode alternativer til å bare kalle klassene DTO.
> 

For vårt scenario ønsker vi å gjenbruke strukturen til den ovenfor definerte DTO for `POST-`og `PUT -`forespørsler. Dette betyr at vi har en generell DTO for studenter, så vi kan bare kalle den `StudentDTO`for enkelhets skyld.

Det neste problemet er konverteringen, hvordan konverterer vi domeneobjektene våre til DTO-er på en effektiv og vedlikeholdbar måte? Dette kan gjøres ved å bruke en dedikert karttjeneste for å kapsle inn logikken.

## Mapping

Kartleggere med DTO-er lager en *fasade* mellom lag, selve kartleggeren fungerer som en *assembler* , disse er begge designmønstre. Dette betyr at det er biblioteker dedikert til å tilby kartfunksjonalitet på en mer automatisert måte. Vi kan enten benytte oss av disse bibliotekene, eller lage egen karttjeneste, førstnevnte er løsningen som er minst anstrengelse.

Det er viktig å merke seg at en kartlegger også er en egen bekymring, og har derfor ingen anelse om datatilgangsprosessen vår. Rent praktisk kan våre kartleggere ikke konvertere en `ID`til en Entity, men kan gå den andre retningen ettersom kartleggeren jobber med domeneobjekter og ikke repositoriene.

> **MERK** : En kartlegger skal heller ikke kunne konvertere IDer til enheter, den er utformet på denne måten for å være gjenbrukbar og separerbar. Ikke ta med datatilgangslogikk (Repositories) i karttjenestene dine.
> 

### Kartleggingsverktøy

En veldig populær karttjeneste for Spring heter [Map Struct](https://mapstruct.org/) . Den er designet for å automatisere kartlegging og er designet for å fungere med Spring Beans. En tjeneste som denne sparer mye tid og krefter, ta eksempelet som brukes når man diskuterer DTOer; kartlegging mellom vår `Student`og `StudentGetDTO`.

> **DOKUMENTASJON** : Kartstrukturdokumenter og vanlige spørsmål om kartstruktur
> 

*Hvis vi ønsket å gjøre dette manuelt* , måtte vi lage vår egen karttjeneste. En måte vi kan gjøre dette på er å definere følgende:

```java
public interface StudentMapper {
    StudentGetDTO toStudentDto(Student student);
    Student toStudent(StudentGetDTO dto);
}
```

Som deretter kan implementeres ved:

```java
@Service
public class StudentMapperImpl implements StudentMapper {
    @Override
    public StudentGetDTO toStudentDto(Student student) {
        StudentGetDTO dto = new StudentGetDTO();
        dto.setId(student.getId());
        dto.setName(student.getName());
        if(student.getProfessor() != null)
                dto.setProfessor(student.getProfessor().getId());
        if(student.getProject() != null)
                dto.setProject(student.getProject().getId());
        if(student.getSubjects() != null)
                dto.setSubjects(student.getSubjects().stream()
                .map(s -> s.getId()).collect(Collectors.toSet()));
        return dto;
    }

    @Override
    public Student toStudent(StudentGetDTO dto) {
        Student student = new Student();
        student.setId(dto.getId());
        student.setName(dto.getName());
        return student;
    }
}
```

Det kan sees at denne prosessen vil være svært tidkrevende å implementere ettersom antall modeller skaleres, da vi trenger en kartlegging til en fra vårt domene og DTO. En generisk tilnærming kan implementeres for å lette mengden grensesnitt, men implementeringene må fortsatt eksistere.

En annen veldig viktig ting å merke seg er at når vi konverterer `til Student`utelater vi `professor`, `prosjekt`og `emner`. Dette er fordi kartleggerne våre ikke burde vite om implementeringen av datatilgangen vår, og i dette tilfellet krever Hibernate fullstendige enheter (sporet av Hibernate) som kartleggeren først må spørre ut ved hjelp av depotene - noe den ikke er ansvarlig for.

Dette er en begrensning for alle karttjenester, automatiserte eller manuelle. Når vi jobber med en datatilgangsløsning som krever en *utholdenhetskontekst* , kan vi ikke bare sende objekter med bare IDer rundt. Disse er kjent som *forbigående objekter* og kan ikke opprettholdes gjennom dvalemodus.

En løsning er å ganske enkelt inkludere de forbigående objektene og håndtere dem i tjenestene med `findById`for å få dem sporet. Men når du bruker et automatisert kartleggingsverktøy, kan det ikke lage disse objektene for oss, så vi håndterer det vanligvis separat i alle tilfeller. På denne måten ignorerer vi det i kartleggingen og trekker ut IDene fra DTO manuelt og konverterer dem.

> **MERK** : Dette kan hjelpes ved å ha navngitte forespørsler i våre repositorier for å imøtekomme tilføyelse/oppdatering med bare IDer, da dette problemet er unikt for ORM-er.
> 

*Hvordan kan dette forbedres med et kartleggingsverktøy?*

Et kartverktøy vil hjelpe med å automatisere de enkle tingene, men vil fortsatt trenge konfigurasjon for mer kompliserte tilordninger. Et dedikert verktøy for kartlegging bidrar også til å skille kartleggingsansvaret bort fra enten våre kontrollere eller tjenester.

**TIL SIDEN** : I vårt eksempel skal vi gjøre *kartleggingen i kontrollerene*, og kartleggerne våre vil *bruke tjenestene våre*. Det er ikke noe riktig sted å gjøre kartleggingslogikk ettersom noen utviklere liker å kartlegge i tjenestene slik at kontrollerene dine aldri bruker domeneobjekter, og noen liker å gjøre kartlegging i kontrollerene fordi det er en del av presentasjonslogikken. Begge deler går bra, og for **Map Struct** er det lettere å kartlegge i kontrollere fordi du bruker tjenester i kartleggingslogikken.

For det første må vi få de riktige avhengighetene, Map Struct krever to: en for selve kartleggingen og en for merknadskonfigurasjoner:

```
implementation 'org.mapstruct:mapstruct:1.4.2.Final'
annotationProcessor 'org.mapstruct:mapstruct-processor:1.4.2.Final'
```

> **RESSURSER** : [Link](https://mapstruct.org/documentation/dev/reference/html/#setup) i dokumenter om installasjon.
> 

Map Struct fungerer ved at du definerer et grensesnitt for å gjøre kartleggingen, og deretter vil Map Struct generere implementeringen. For å illustrere dette, la oss lage en enkel `prosjektkartlegging`:

```java
@Entity
@Getter
@Setter
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(nullable = false, length = 100)
    private String title;
    @OneToOne(mappedBy = "project")
    private Student student;
}
```

```java
@Data
public class ProjectDTO {
    private int id;
    private String title;
}
```

> **MERK** : Data er en lombok-annotering for å legge til getters, settere, toString, equals og getHashCode til klassen.
> 

For å gjøre denne kartleggingen, må vi lage en `ProjectMapper-`abstraksjon. I dette tilfellet kan vi bruke et grensesnitt.

> **MERK** : Map Struct lar både grensesnitt og abstrakte klasser brukes til å definere tilordninger. Du trenger abstrakte klasser når det er tilpassede kartleggingsmetoder som skal kalles, et senere eksempel illustrerer dette.
> 

```java
@Mapper(componentModel = "spring")
public interface ProjectMapper {
    ProjectDTO projectToProjectDto(Project project);
}
```

`@Mapper-kommentaren er en kartstrukturspesifikk`merknad . Vi leverer også et `componentModel`-argument, dette er hvordan vi konfigurerer kartleggeren vår til å administreres av Spring.

Hvis vi bygger applikasjonen vår nå, genereres implementeringen. I Gradle kan denne generasjonen sees i `build > generert > kilder > annotationProcessor > java-`mappen:

![Untitled](3%20OpenAPI%20og%20dataoverf%C3%B8ringsobjekter%20beac8659d99f4459b730ca3457d385bc/Untitled%2013.png)

> **MERK** : Hvis dette mislykkes, kontroller at du har installert de riktige avhengighetene inkludert.
> 

Hvis vi undersøker den genererte utgangen:

```java
@Component
public class ProjectMapperImpl implements ProjectMapper {
    @Override
    public ProjectDTO projectToProjectDto(Project project) {
        if ( project == null ) {
            return null;
        }
        ProjectDTO projectDTO = new ProjectDTO();
        projectDTO.setId( project.getId() );
        projectDTO.setTitle( project.getTitle() );
        return projectDTO;
    }
}
```

Vi ser at den er generert som en `@Component`, noe som betyr at vi kan injisere den. Den brukte også refleksjon for å lage disse kartleggingsmetodene. Vi kan nå bruke denne kartleggeren i kontrollerene våre:

```java
@RestController
@RequestMapping(path = "api/v1/projects")
public class ProjectController {
    private final ProjectService projectService;
    private final ProjectMapper projectMapper;

    public ProjectController(ProjectService projectService, ProjectMapper projectMapper) {
        this.projectService = projectService;
        this.projectMapper = projectMapper;
    }
```

```java
@GetMapping("{id}")
public ResponseEntity findById(@PathVariable int id) {
    ProjectDTO proj = projectMapper.projectToProjectDto(
        projectService.findById(id)
    );
    return ResponseEntity.ok(proj);
}
```

*Hva med å gå andre veien?*

Å gå fra domene til DTO er like enkelt, definer en ny metode i `ProjectMapper`-grensesnittet vårt:

```java
@Mapper(componentModel = "spring")
public interface ProjectMapper {
    ProjectDTO projectToProjectDto(Project project);
    // Mapping the other way
    Project projectDtoToProject(ProjectDTO projectDTO);
}
```

> **MERK** : Husk å bygge om (det beste alternativet er å kjøre Gradles monteringsoppgave ).
> 

Du vil legge merke til at det nå vises en advarsel til oss:

```
warning: Unmapped target property: "student".
```

Dette er `studenteiendommen`på vår `prosjektmodell`. Applikasjonen vil fortsatt fungere, den gir oss bare beskjed om at den ikke er kartlagt (siden den ikke vet hvordan man lager et `studentobjekt`). Vi kan eksplisitt si at feltet skal ignoreres med:

```java
@Mapping(target = "student", ignore = true)
Project projectDtoToProject(ProjectDTO projectDTO);
```

`Mål`her er egenskapen på destinasjonsobjektet (Prosjekt). Dette vil fjerne advarselen. La oss se hvordan dette påvirker bruken vår uten tjenester. Husk at vi kan gjenbruke vår `ProjectDTO`for `POST-`og `PUT-`forespørsler. La oss oppdatere prosjektet vårt og se hvordan Hibernate håndterer vår manglende studenteiendom:

```java
@PutMapping("{id}")
    public ResponseEntity updateProject(@RequestBody ProjectDTO projectDTO, @PathVariable int id) {
    if(id != projectDTO.getId())
        return ResponseEntity.notFound().build();
    projectService.update(
        projectMapper.projectDtoToProject(projectDTO)
    );
    return ResponseEntity.noContent().build();
}
```

I dette tilfellet gikk ingenting galt og alt oppdateres som forventet, dette er fordi `Project`ikke eier forholdet til `Student`( `Project`has `mapdBy`). Hva skjer når kartlegging ikke er så lett?

### Kompleks mapping

Så langt har vi jobbet med enkle datatyper og har ikke trengt å inkludere noen forretningslogikk for å fullføre kartleggingene våre. I virkeligheten er det imidlertid sjelden så enkelt. Spesielt når du arbeider med et kjernedomeneobjekt, som `Student`. For å oppsummere, `Student`har flere relasjoner til andre domeneobjekter, den *eier også* disse relasjonene. La oss utforske implikasjonene av dette når du håndterer kartleggingslogikk. La oss først definere `StudentMapper`på samme måte som vi gjorde `ProjectMapper`:

```java
@Mapper(componentModel = "spring")
public interface StudentMapper {
    StudentDTO studentToStudentDto(Student student);
}
```

Hvis vi prøver å bygge prosjektet vårt nå, får vi noen problemer, la oss fikse dem en om gangen:

```
error: Can't map property "Project project" to "int project". Consider to declare/implement a mapping method: "int map(Project value)".
```

For å fikse dette trenger vi ikke implementere noe ennå, vi kan bruke `@Mapping-`kommentaren som er sett tidligere:

```java
@Mapping(target = "project", source = "project.id")
StudentDTO studentToStudentDto(Student student);
```

Her sier vi at `prosjektegenskapen`på `målet vårt`( `StudentDto.project`) kan kartlegges fra `project.id -`egenskapen til `kilden vår`( `Student.project.id`). Vi får også en lignende feil for `professor`og `emne`, la oss gå og se om vi kan fikse dem på samme måte:

```java
@Mapping(target = "project", source = "project.id")
@Mapping(target = "professor", source = "professor.id")
@Mapping(target = "subjects", source = "subjects")
StudentDTO studentToStudentDto(Student student);
```

Vi ser at det også løste `professorproblemet vårt`, men `emner`er ikke løst:

```
error: Can't map property "Set<Subject> subjects" to "Set<Integer> subjects". 
Consider to declare/implement a mapping method: "Set<Integer> map(Set<Subject> value)".
```

Denne kartleggingen er ikke enkel, tidligere, da vi gjorde alt manuelt, måtte vi bruke StreamAPI og noen nullsjekker. Vi må implementere noe for å gjøre dette. Heldigvis, forteller Map Struct oss hva vi skal gjøre, implementer: `Set<Integer> map(Set<Subject> value)`. Hvordan kan vi implementere en metode i et grensesnitt? Enkelt og greit, vi kan ikke. Vi kan fikse dette ved å konvertere grensesnittet vårt til en abstrakt klasse. Husk at Map Struct kan bruke begge. Dette resulterer i at kartleggeren vår endres til:

```java
@Mapper(componentModel = "spring")
public abstract class StudentMapper {
    @Mapping(target = "project", source = "project.id")
    @Mapping(target = "professor", source = "professor.id")
    @Mapping(target = "subjects", source = "subjects")
    public abstract StudentDTO studentToStudentDto(Student student);

    Set<Integer> map(Set<Subject> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }
}
```

Dette fungerer imidlertid fortsatt ikke, det klages fortsatt over at vi ikke har noen kartlegging definert. Dette er fordi vi må fortelle Map Struct at denne `map`metoden er for vår`subjects`mappe. Vi gjør dette ved å kommentere metoden vår med `@Named`og deretter legge til et `qualifiedByName -`argument til vår `@Mapping`:

```java
@Mapper(componentModel = "spring")
public abstract class StudentMapper {
    @Mapping(target = "project", source = "project.id")
    @Mapping(target = "professor", source = "professor.id")
    @Mapping(target = "subjects", source = "subjects", qualifiedByName = "subjectsToIds")
    public abstract StudentDTO studentToStudentDto(Student student);

    @Named("subjectsToIds")
    Set<Integer> map(Set<Subject> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(s -> s.getId()).collect(Collectors.toSet());
    }
}
```

Kombinasjonen av `qualifiedByName`og `@Named`erstatter tilordningen med vår egen. Vi trenger også å navngi metodekartet `map`, vi kan kalle det for det vi vil, for eksempel `mapSubjectsToIds`. Alt som trenger å matche er parameterne og returtypene, så kan *Map Struct* løse erstatningen.

Det neste trinnet er å integrere den med kontrollerene og se om den fungerer:

```java
private final StudentService studentService;
private final StudentMapper studentMapper;

public StudentController(StudentService studentService, StudentMapper studentMapper) {
    this.studentService = studentService;
    this.studentMapper = studentMapper;
}
```

```java
@GetMapping("{id}") // GET: localhost:8080/api/v1/students/1
public ResponseEntity getById(@PathVariable int id) {
    StudentDTO stud = studentMapper.studentToStudentDto(
        studentService.findById(id)
    );
    return ResponseEntity.ok(stud);
}
```

Dette fungerer som ønsket, neste steg er å returnere alle elevene. Vi kan legge til en ny metode til kartleggeren vår:

```java
public abstract Collection<StudentDTO> studentToStudentDto(Collection<Student> students);
```

Vi trenger ikke å rekonfigurere alt vi gjorde for singularkartleggingen. Dette er fordi Map Struct er smart nok til å gjenbruke singularkartleggingen når vi kartlegger samlinger. Når vi ser på den genererte implementeringen, kan vi se dette:

```java
@Override
public Collection<StudentDTO> studentToStudentDto(Collection<Student> students) {
    if ( students == null ) {
        return null;
    }
    Collection<StudentDTO> list = new ArrayList<StudentDTO>( students.size() );
    for ( Student student : students ) {
        list.add( studentToStudentDto( student ) );
    }
    return list;
}
```

Vi ser her at Map Struct bare kaller `studentToStudentDto`for hvert element.

> **MERK** : Metodenavnene trenger ikke samsvare for at dette skal fungere.
> 

Når du flytter til kontrollerene, er dette implementeringen:

```java
@GetMapping // GET: localhost:8080/api/v1/students
public ResponseEntity getAll() {
    Collection<StudentDTO> studs = studentMapper.studentToStudentDto(
        studentService.findAll()
    );
    return ResponseEntity.ok(studs);
}
```

### Implementering av DTO til domene

Når en DTO må konverteres til et domeneobjekt, må ID-ene konverteres til domeneobjekter. Dette er for at Hibernate har riktig konfigurasjon for utholdenhet.

> **MERK** : Hvis du ikke gjorde dette, vil du få en feilmelding knyttet til at løsrevet enhet sendes til å vedvarenår du prøver å lagre.
> 

Vi kan enten gjøre dette i selve tjenesten, hvor vi trekker ut hver relaterte enhet og bruker depotene til å fylle ut de tomme feltene. Vi kan også gjøre dette i vår kartlegger.

Spørsmålet er, *hvilken er riktig?*

De har begge rett. Du kan til og med unngå dette med tilpassede søk gjennom hibernate, det er mange løsninger på problemer. For eksempelet vårt vil vi bruke tjenester i kartleggeren vår for å manipulere domeneobjektene, og fordi kartleggerne våre er `@Components`, kan vi injisere tjenester.

Dette bryter ikke med designet vårt, da bruk av tjenester i kartleggerne våre er det samme som å bruke tjenester i kontrollerene våre, begge tar for seg *bekymringen om presentasjon* . Diagrammet nedenfor hjelper til med å illustrere bekymringene:

![Untitled](3%20OpenAPI%20og%20dataoverf%C3%B8ringsobjekter%20beac8659d99f4459b730ca3457d385bc/Untitled%2014.png)

For å begynne, må en metode for å konvertere DTO til domene defineres:

```java
public abstract Student studentDtoToStudent(StudentDTO dto);
```

Det neste trinnet for å legge til all `@Mapping`for å konvertere heltallene til domeneobjekter:

```java
@Mapping(target = "project", source = "project", qualifiedByName = "projectIdToProject")
@Mapping(target = "professor", source = "professor", qualifiedByName = "professorIdToProfessor")
@Mapping(target = "subjects", source = "subjects", qualifiedByName = "subjectIdsToSubjects")
public abstract Student studentDtoToStudent(StudentDTO dto);
```

Skjelettene for de tilpassede metodene er som følger:

```java
@Named("projectIdToProject")
Project mapIdToProject(int id) {
    return null;
}
@Named("professorIdToProfessor")
Professor mapIdToProfessor(int id) {
    return null;
}
@Named("subjectIdsToSubjects")
Set<Subject> mapIdsToSubjects(Set<Integer> id) {
    return null;
}
```

Det neste trinnet er å injisere alle tjenestene vi trenger:

```java
@Mapper(componentModel = "spring")
public abstract class StudentMapper {
    @Autowired
    protected ProjectService projectService;
    @Autowired
    protected ProfessorService professorService;
    @Autowired
    protected SubjectService subjectService;
```

> **MERK** : Dette kan gjøres på mange måter, se Map Struct dokumentene hvis du er interessert i alternativer.
> 

Vi kan da bruke tjenestene:

```java
@Named("projectIdToProject")
Project mapIdToProject(int id) {
    return projectService.findById(id);
}
@Named("professorIdToProfessor")
Professor mapIdToProfessor(int id) {
    return professorService.findById(id);
}
@Named("subjectIdsToSubjects")
Set<Subject> mapIdsToSubjects(Set<Integer> id) {
    return id.stream()
        .map( i -> subjectService.findById(i))
        .collect(Collectors.toSet());
}
```

For å se om dette har fungert, må vi teste det ut. Først ønsker vi å få student-ID 1:

```json
{
  "id": 1,
  "name": "Ola Nordmann",
  "project": 1,
  "professor": 1,
  "subjects": [
    1,
    2
  ]
}
```

Oppdater den deretter til:

```json
{
  "id": 1,
  "name": "Ola Ola",
  "project": 1,
  "professor": 2,
  "subjects": [
    1
  ]
}
```

Vi endrer: navnet hans, professoren og fagene hans. Hvis vi plasserer et break point i `StudentService`, kan vi se at enheten er kartlagt som følgende:

![Untitled](3%20OpenAPI%20og%20dataoverf%C3%B8ringsobjekter%20beac8659d99f4459b730ca3457d385bc/Untitled%2015.png)

Inspeksjon av de produserte loggene viser at Hibernate har gjort flere søk etter hensikten. For å bekrefte at oppdateringen vår har fungert, må vi GET denne studenten igjen:

```json
{
  "id": 1,
  "name": "Ola Ola",
  "project": 1,
  "professor": 2,
  "subjects": [
    1
  ]
}
```

Oppdateringen er fullført. En lignende tilnærming kan brukes for POST, men det er viktig å ikke at de relaterte enhetene allerede trenger å eksistere - dette er en standard praksis.

Det kan imidlertid ikke ignoreres hvor mange søk Hibernate må gjøre for å utfœre denne funksjonaliteten. Dette er noe som ikke kan unngås med mindre tilpassede søk brukes. Som en siste påminnelse trenger ikke tjenesteinteraksjonen å gå i kartleggeren, det kan gjøres i selve tjenestene, men da trenger vår `StudentService`vite om vårt `ProfessorRepository`, `ProjectRepository`og `SubjectRepository`. Dette viser at i programmering er det ingen korrekt implementering, bare en som fungerer og gir mening. Disse tingene kan refaktoreres på et senere tidspunkt.

> **MERK** : Neste avsnitt er en diskusjon av refaktorisering, ingen eksempler er gitt.
> 

En potensiell redesign ville være å trekke ut `subjects` fullstendig og ha spesifikke endepunkter for alt fagrelatert:

```java
GET: api/v1/students/:id/subjects // Get subjects for student
POST: api/v1/students/:id/subjects // Add new subject for student
PUT: api/v1/students/:id/subjects // Update current subjects for student
DELETE: api/v1/students/:id/subjects // Remove subject for student
```

Dette lar oss ganske enkelt skrive en `@Query` i `StudentRepository`som tar inn fremmednøklene. Dette fjerner behovet for å konvertere `projectId`og `professorId`til henholdsvis `Project`og `Professor`- og eliminerer mange spørringer.

Da har vi et helt eget sett med elevfagsrelatert funksjonalitet i tjenesten vår å kalle på ved behov.

### Sammendrag av DTO-bruk

Dataoverføringsobjekter bidrar til å skape et skille mellom lag ved å la ulike lag kommunisere gjennom enkle POJO-er. Dette har mange fordeler, men medfører store implementeringskostnader hvis det er mange representasjoner. En DTO må settes sammen av en eller annen klasse, dette er kjent som en Mapper. Sammen skaper de en Facade for lag til interaksjon mellom lag.

Kartlegging manuelt er en utfordrende oppgave, det er flere verktøy som letter denne prosessen, for Java er det et bibliotek som heter Map Struct. Dette biblioteket lar deg definere grensesnitt, omtrent som Spring Data JPA-repoer, som deretter genererer implementeringen. Du kan se de genererte Map Struct-implementeringene i utdatamappen.

Kartleggere kan enten defineres med grensesnitt eller abstrakte klasser, valget avhenger av kartleggingsnivået som kreves. Map Struct lar ikke-trivielle tilordninger konfigureres via merknader, og om nødvendig en tilpasset metode som løses ved hjelp av `@Named`og `qualifiedByName`.

Map Struct lar også tjenester injiseres, forutsatt at `componentModel`er konfigurert. Dette lar kartleggeren samhandle med andre lag i applikasjonen. Husk at det er mange måter å løse problemer på, og ofte kan en liten refaktor unngå et svært komplekst kartleggingskrav. Husk, Keep It Simple Stupid.

> **RESSURSER** : [Repository](https://gitlab.com/noroff-accelerate/java/projects/spring-web-with-dtos-and-openapi) som inneholder prosjektet for leksjonen.
> 

Copyright 2022, Noroff Accelerate AS