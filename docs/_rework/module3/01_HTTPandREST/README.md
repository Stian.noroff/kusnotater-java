# 1. RESTful Services

# Kommunikasjon med HTTP

I denne leksjonen lærer du hvordan kommunikasjon håndteres på nettet gjennom HTTP-forespørsler og svar. Du vil også lære om ulike standarder som muliggjør denne kommunikasjonen.

**Læringsmål:**

Du vil kunne…

- **skille mellom** APIer med Web APIer.
- **forklare** REST-arkitekturen.
- **definere** den typiske strukturen til en http-forespørsel.
- **diskutere** HTTP-verb og vanlige HTTP-statuskoder.
- **demonstrere** navn på endepunkt i samsvar med beste praksis for REST.
- **beskrive** rollen Postman spiller i API-oppretting.

## Hypertext Transfer Protocol (HTTP)

For å utvikle webapplikasjoner trenger du en grunnleggende arbeidsforståelse om hvordan Internett fungerer. Internett er rett og slett et massivt globalt nettverk av nettverk. Nettet er teknologiene som gjør det mulig for folk å utnytte Internett på meningsfulle måter, dvs. gjennom nettapplikasjoner/nettsteder som bruker HTML, CSS, Javascript og backend-teknologier som ASP.NET, Java eller Express. Uten disse nettteknologiene for å gi intrikate brukergrensesnitt, er Internett ganske enkelt et middel til å sende grunnleggende datapakker mellom datamaskiner rundt om i verden. Figuren nedenfor illustrerer utvekslingen av nettfiler og data som skjer over Internett under en typisk interaksjon på nettstedet.

![Untitled](1%20RESTful%20Services%2064f7cde060864dca9ca281e3fd84317e/Untitled.png)

En bruker skriver inn en nettadresse (URL) i nettleseren, www.website.com. Nettleseren utløser deretter en forespørsel til serveren hostet på www.website.com om innholdet på nettsidens landingsside som leveres i form av statiske HTML-, CSS- og Javascript - filer, disse lastes ned og lagres av nettleseren. Nettleseren gjengir deretter HTML og CSS for å presentere hjemmesiden. JS-koden vil da utløse ytterligere forespørsler om flere filer og data avhengig av brukernes interaksjoner og utformingen av nettstedet. Filer og data overføres over Internett flere ganger mellom nettleseren og serveren der nettstedet er vert. Som webutviklere er det derfor viktig å forstå hvordan data overføres over Internett.

Hyper Text Transfer Protocol (HTTP) er en nettverksprotokoll designet for å overføre ressurser som ren tekst, HTML og bilder over Internett. Den brukes i et klient-server-forhold for å tillate at ressurser distribueres over Internett på en avtalt måte. HTTP er “Kommandospråket” som både avsender (klient) og mottaker (server) må forholde seg til for å kommunisere. Figuren nedenfor illustrerer den grunnleggende interaksjonen som vanligvis forekommer.

![Untitled](1%20RESTful%20Services%2064f7cde060864dca9ca281e3fd84317e/Untitled%201.png)

Klienten vil sende en HTTP-forespørsel til serveren som deretter sender et HTTP-svar. Dette brukes mest av en brukeragent, vanligvis en nettleser (klient) og en nettserver (server) for å sende og motta data, noe som letter nettsider som vist i forrige eksempel.

Figuren nedenfor illustrerer dette forholdet i sammenheng med utvikling av webapplikasjoner.

![Untitled](1%20RESTful%20Services%2064f7cde060864dca9ca281e3fd84317e/Untitled%202.png)

Figuren ovenfor illustrerer en mer ‘tradisjonell’ nettapplikasjon der hele webapplikasjonen er i én monolittisk løsning. Klienten ber om nettsider som deretter mottas og vises av nettleseren. Backend og Frontend logiske operasjoner er alle inneholdt i en enkelt nettapplikasjon.

Moderne webapplikasjonsutvikling separerer backend- og frontend-utvikling i separate applikasjoner, for å gjøre ansvar klart og tilrettelegge for en rekke andre utviklingsfordeler. Sluttresultatet er en nettside/nettapplikasjon eller tjeneste som kan virke som en enkelt applikasjon, men som faktisk er spredt over flere separat distribuert applikasjon. Dette er kjent som en **Rest Arkitetkur**.

## REST-arkitektur

REST er et akronym for REpresentational State Transfer. Det er en arkitektonisk stil for distribuerte hypermediasystemer og ble først presentert av Roy Fielding i 2000. REST har 6 veiledende begrensninger som må tilfredsstilles for at et grensesnitt skal anses som “RESTful”.

1. Klient–server – Ved å skille bekymringene for brukergrensesnittet fra bekymringene om datalagring, forbedrer vi portabiliteten til brukergrensesnittet på tvers av flere plattformer og forbedrer skalerbarheten ved å forenkle serverkomponentene.
2. Stateless – Hver forespørsel fra klient til server må inneholde all informasjon som er nødvendig for å forstå forespørselen, og kan ikke dra nytte av noen lagret kontekst på serveren. Sesjonstilstand holdes derfor helt på klienten.
3. Bufres – Hurtigbufferbegrensninger krever at dataene i et svar på en forespørsel er implisitt eller eksplisitt merket som bufres eller ikke bufres. Hvis et svar kan bufres, gis en klientbuffer rett til å gjenbruke disse svardataene for senere tilsvarende forespørsler.
4. Ensartet grensesnitt – Ved å bruke det generelle programvaretekniske prinsippet på komponentgrensesnittet, forenkles den generelle systemarkitekturen og synligheten til interaksjoner forbedres. For å oppnå et enhetlig grensesnitt, er det nødvendig med flere arkitektoniske begrensninger for å veilede komponentenes oppførsel. REST er definert av fire grensesnittbegrensninger: identifikasjon av ressurser; manipulering av ressurser gjennom representasjoner; selvbeskrivende meldinger; og hypermedia som motoren for applikasjonstilstand.
5. Lagdelt system - Den lagdelte systemstilen lar en arkitektur være sammensatt av hierarkiske lag ved å begrense komponentadferd slik at hver komponent ikke kan “se” utover det umiddelbare laget de samhandler med.
6. Kode på forespørsel (valgfritt) – REST lar klientfunksjonaliteten utvides ved å laste ned og kjøre kode i form av appleter eller skript. Dette forenkler klienter ved å redusere antall funksjoner som kreves for å være forhåndsimplementert.

Abstraksjonen av informasjon i REST er en *ressurs* , dette kan være hvilken som helst informasjon. For å identifisere den spesielle ressursen som er involvert i en interaksjon, bruker REST ressursidentifikatorer. Tilstanden til en ressurs til enhver tid er kjent som ressursrepresentasjon; denne representasjonen består av data, metadata som beskriver dataene og hypermedialenker som kan hjelpe klienter med å gå over til neste ønskede tilstand. Dataformatet til en representasjon er kjent som en medietype. Medietypen identifiserer en spesifikasjon som definerer hvordan en representasjon behandles. Den vanligste medietypen som sees er **JSON** .

Å bruke en RESTful-konvensjon har mange fordeler og ulemper, men noen av de vanligste årsakene til å opprette og bruke RESTful-tjenester er:

- Alle svar kan være enkle HTTP-statuskoder
- Språkuavhengig, klienter kan enkelt være forskjellige språk/teknologier til serveren
- Dataformatet er fleksibelt (vanligvis JSON, enkelt å jobbe med)
- Sikkerhet håndteres med lag, ikke teknologier (enklere å implementere)

Bruk av REST-arkitekturen på nettapplikasjoner resulterer i en separasjon av frontend fra backend. Funksjonelt fungerer den som en enkelt applikasjon, men den er mer fleksibel, skalerbar og gjør utviklingsoperasjonen renere. Figuren nedenfor illustrerer REST-arkitekturen som vanligvis brukes i moderne webapplikasjonsutvikling. Dette er et alternativ til den monolittiske applikasjonen vist tidligere i denne leksjonen.

![Untitled](1%20RESTful%20Services%2064f7cde060864dca9ca281e3fd84317e/Untitled%203.png)

> INFO Merk: Detaljene om frontend-applikasjoner vil bli dekket i frontend-delen av dette kurset. Foreløpig er vi bare interessert i utviklingsdetaljene til Backend.
> 

Backend innebærer å administrere data og gjøre dem tilgjengelige for klienter med **web-APIer** og databaser på en server. Den vil levere data ved hjelp av JSON- og HTTP-svarkoder.

> INFO Merk: Du skal bygge RESTful Web API-er som back-end webutvikler
> 

## Web APIer

API er forkortelsen for *Application Programming Interface* . Det er et programvaregrensesnitt som lar to applikasjoner samhandle med hverandre uten brukerintervensjon. APIer lar et produkt eller en tjeneste kommunisere med andre produkter og tjenester uten å måtte vite hvordan de implementeres.

Dette har vært den primære måten vår interaksjon med biblioteker og avhengigheter har vært. Husk designleksjonene der vi har lært hvordan vi kan gjøre applikasjonene våre renere ved å stole på å lage gode grensesnitt for interaksjon.

En *Web API* er ganske enkelt en API som er tilgjengelig over et nettverk. Web-APIer er ofte kjent som *webtjenester* , dette gjøres for å skape et klart skille mellom nettverks- og ikke-nettverks-APIer.

> INFO Merk: Alle webtjenester er APIer, men ikke alle APIer er webtjenester. Vi kan bytte vilkårene Web API og webtjenester for våre bruksområder.
> 

Web API implementerer protokollspesifikasjoner og inkorporerer derfor konsepter som *caching* , *URIer* , *versjonering* , *forespørsels-/svar-headers* og forskjellige *innholdsformater* i den.

Web APIer returnerer informasjon i form av webdokumenter, den vanligste typen returneres i [JSON](https://www.json.org/json-en.html) -format. JSON står for *JavaScript Object Notation* og er en standardisert måte å kommunisere på en veldig lesbar, lett parserbar og enkel å generere måte.

> INFO Merk: Du vil lære hvordan du bygger et web-API i neste leksjon.
> 

For å oppsummere i henhold til figuren ovenfor, i moderne nettutvikling er backend-komponenten til et nettsted/applikasjon en web-API som kun er ansvarlig for å administrere data for nettstedet/applikasjonen. Frontend ClientApp er statisk HTML, Javascript og CSS-filer lastes ned av en nettleser fra frontend-webserveren, som deretter vil be om data og operasjoner fra Web API over HTTP for å fylle nettstedet med relevante data basert på brukerinteraksjon. Det neste trinnet er å undersøke hvordan HTTP-forespørsler til en web- API fungerer.

## HTTP-forespørsler

En Restful HTTP-forespørsel fra klienten til serveren består vanligvis av følgende komponenter:

- **URL-sti**[https://api.example.com/user]
- **HTTP-metode** [GET, PUT, POST, PATCH, DELETE]
- **Overskrift** – (valgfritt) tilleggsinformasjon som klienten trenger å gi videre i forespørselen, for eksempel autorisasjonslegitimasjon, innholdstype, brukeragent for å definere hvilken type applikasjon som sender forespørselen, og mer]
- **Parametere** – (valgfritt) variabelfelt som endrer hvordan ressursen skal returneres.
- **Body** – (valgfritt) inneholder data som må sendes til serveren.

Figuren nedenfor illustrerer et eksempel på en HTTP-forespørsel til eksempel- API for enkelte data

![Untitled](1%20RESTful%20Services%2064f7cde060864dca9ca281e3fd84317e/Untitled%204.png)

> MERK : HTTP-svar følger samme mønster, men inneholder en HTTP-statuskode i stedet for en HTTP-metode
> 

## Http-metoder/verb

HTTP definerer et sett med forespørselsmetoder for å indikere ønsket handling som skal utføres for en gitt ressurs. Selv om de også kan være substantiv, blir disse forespørselsmetodene noen ganger referert til som HTTP-verb.

Hver av dem implementerer en annen semantikk, men noen fellestrekk deles av en gruppe av dem: for eksempel kan en forespørselsmetode være trygg, idempotent eller hurtigbufringsbar.

> MERK : Idempotens er et konsept som brukes for å sikre robustheten til APIer, det kan bli ganske komplisert . For nå er alt som trenger å vites at en idempotent forespørsel ALLTID vil gi samme effekt uansett hvor mange ganger den kalles.
> 

Det er mange verb, vi vil fokusere på noen viktige: GET, POST, PUT, PATCH, og DELETE.

**GET**

[HTTP GET](https://tools.ietf.org/html/rfc7231#section-4.3.1) -metoden ber om en representasjon av den angitte ressursen. Forespørsler som bruker GET skal bare brukes til å be om data - noe som betyr at de ikke skal inkludere data.

> HTTP GET er idempotent. Den samme forespørselen returnerer de samme dataene.
> 

De inkluderte dataene her refererer til data i forespørselskroppen.

**POST**

[HTTP POST](https://tools.ietf.org/html/rfc7231#section-4.3.3) -metoden sender data til serveren . Type body i forespørselen er angitt av *innholdstype-* overskriften. En POST-forespørsel sendes vanligvis via et HTML-skjema og resulterer i en endring på serveren.

> HTTP POST er ikke idempotent. Flere INNLEGG resulterer i at nye ressurser opprettes hver gang.
> 

Content-Type er vanligvis applikasjon/ json og JSON-data sendes i forespørselsteksten.

**PUT**

[HTTP PUT](https://tools.ietf.org/html/rfc7231#section-4.3.4)- forespørselsmetoden oppretter en ny ressurs eller erstatter en representasjon av målressursen med forespørselens nyttelast.

> HTTP PUT er idempotent. Flere forespørsler resulterer ikke i noe nytt ettersom den første oppdaterer ressursen.
> 

**PATCH**

[HTTP PATCH-](https://tools.ietf.org/html/rfc5789) forespørselsmetoden bruker delvise modifikasjoner på en ressurs .

PATCH ligner på “oppdatering”-konseptet som finnes i CRUD. En PATCH-forespørsel betraktes som et sett med instruksjoner om hvordan du endrer en ressurs. Kontrast dette med PUT; som er en fullstendig representasjon av en ressurs. Både PUT og PATCH kan brukes til å oppdatere ressurser.

> MERK : HTTP PATCH er vanligvis idempotent, men det kan oppstå scenarier der det ikke er det.
> 

**DELETE**

[HTTP DELETE](https://tools.ietf.org/html/rfc7231#section-4.3.5) -forespørselsmetoden sletter den angitte ressursen.

> HTTP DELETE er idempotent. Når en ressurs er borte, er den borte.
> 

## Http statuskoder

Det er mange statuskoder som svar fra servere kan gi. Hver av dem betyr noe spesifikt. Heldigvis er de gruppert i “bands” på 100 hvor relaterte koder er nummerert. HTTP-svarstatuskoder indikerer om en spesifikk HTTP-forespørsel er fullført.

Svarene er gruppert i fem klasser:

- Informasjonssvar (100–199)
- Vellykkede svar (200–299)
- Viderekoblinger (300–399)
- Klientfeil (400–499)
- Serverfeil (500–599)

Det er mange inkludert i disse timene, vi vil bare fokusere på noen få viktige som du vil møte. Figuren nedenfor illustrerer svaret mottatt fra forrige HTTP-forespørsel-eksempel.

![Untitled](1%20RESTful%20Services%2064f7cde060864dca9ca281e3fd84317e/Untitled%205.png)

**200 OK**

[HTTP 200 OK](https://tools.ietf.org/html/rfc7231#section-6.3.1) suksessstatussvarskoden indikerer at forespørselen har lyktes. Et 200-svar er bufret som standard.

Betydningen av en suksess avhenger av HTTP-forespørselsmetoden:

- GET: Ressursen er hentet og blir overført i meldingsteksten.
- POST: Ressursen som beskriver resultatet av handlingen overføres i meldingsteksten

Det vellykkede resultatet av en PUT eller en DELETE er ofte ikke en *200 OK ,* men en *204 No Content* (eller en : når ressursen lastes opp for første gang).

**201 Created**

Responskoden for [HTTP 201 Created](https://tools.ietf.org/html/rfc7231#section-6.3.2) suksessstatus indikerer at forespørselen har lyktes og har ført til opprettelsen av en ressurs. Den nye ressursen opprettes effektivt før dette svaret sendes tilbake, og den nye ressursen returneres i bodyen i meldingen, hvor dens plassering er enten URL-en til forespørselen eller innholdet i posisjonsoverskriften.

Vanlig bruk av denne statuskoden er et resultat av en POST-forespørsel.

**204 No Content**

[HTTP 204 No Content -](https://tools.ietf.org/html/rfc7231#section-6.3.5) suksessstatusresponskoden indikerer at en forespørsel har lyktes, men at klienten ikke trenger å navigere bort fra sin gjeldende side.

Dette kan for eksempel brukes når du implementerer “lagre og fortsett redigering”-funksjonalitet for et wiki-nettsted. I dette tilfellet vil en PUT-forespørsel bli brukt for å lagre siden, og 204 No Content-svaret vil bli sendt for å indikere at redaktøren ikke bør erstattes av en annen side.

> Tilbakekalling fra 200 Ok , 204 No Content er ofte svaret for PUT og DELETE.
> 

**400 Bad Request**

[HTTP 400 Bad Request -](https://tools.ietf.org/html/rfc7231#section-6.5.1) svarstatuskoden indikerer at serveren ikke kan eller vil behandle forespørselen på grunn av noe som oppfattes som en klientfeil (f.eks. misformet forespørselssyntaks, ugyldig forespørselsmeldingsramme eller villedende forespørselsruting).

> Klienten bør ikke gjenta denne forespørselen uten endringer.
> 

**401 Unauthorized**

Responskoden for [HTTP 401 Unauthorized](https://tools.ietf.org/html/rfc7235#section-3.1) klientfeilstatus indikerer at forespørselen ikke har blitt brukt fordi den mangler gyldig autentiseringslegitimasjon for målressursen.

**403 Forbidden**

Responskoden for [HTTP 403 Forbidden](https://tools.ietf.org/html/rfc7231#section-6.5.3) klientfeilstatus indikerer at serveren forsto forespørselen, men nekter å godkjenne den.

Denne statusen ligner på 401, men i dette tilfellet vil re-autentisering ikke ha noen forskjell. Tilgangen er permanent forbudt og knyttet til applikasjonslogikken, for eksempel utilstrekkelige rettigheter til en ressurs.

> Autentisering og autorisasjon er begreper vi diskuterer i en senere leksjon.
> 

**404 Not Found**

[HTTP 404 Not Found -](https://tools.ietf.org/html/rfc7231#section-6.5.4) klientfeilsvarkoden indikerer at serveren ikke kan finne den forespurte ressursen. Lenker som fører til en 404-side kalles ofte ødelagte eller døde lenker og kan bli utsatt for “[link rot](https://en.wikipedia.org/wiki/Link_rot)”.

En 404-statuskode indikerer ikke om ressursen mangler midlertidig eller permanent. Men hvis en ressurs fjernes permanent, bør en *410 Gone* brukes i stedet for en 404-status.

**405 Method Not Allowed**

[HTTP 405 Not Allowed -](https://tools.ietf.org/html/rfc7231#section-6.5.5) klientfeilsvarkoden indikerer at forespørselsmetoden er kjent av serveren, men ikke støttes av målressursen.

> For eksempel kan et API forby å SLETTE en ressurs.
> 

**500 Internal Server Error**

Svarkoden for [HTTP 500 Internal Server Error](https://tools.ietf.org/html/rfc7231#section-6.6.1) serverfeil indikerer at serveren møtte en uventet tilstand som forhindret den i å oppfylle forespørselen.

Dette feilsvaret er et generisk “catch-all”-svar. Vanligvis indikerer dette at serveren ikke kan finne en bedre 5xx-feilkode som svar. Noen ganger logger serveradministratorer feilsvar som 500-statuskoden med flere detaljer om forespørselen for å forhindre at feilen skjer igjen i fremtiden.

## Postman

Testing av web-API-er er et avgjørende skritt i backend-utviklingen, da dette er en indikasjon på hva som fungerer og hva som må fikses. Det er mange tilgjengelige verktøy for å konfigurere og sende HTTP-forespørsler slik at API-er kan testes, et veldig vanlig verktøy er **Postman.** Postman er et godt støttet robust API-testverktøy designet for å hjelpe til med å utvikle APIer raskere ved å tillate enkel testing og til og med automatisert testing. Ved å bruke denne typen verktøy kan web-API-er testes uten behov for en frontend-applikasjon som vil gjøre disse forespørslene.

Det er mange funksjoner tilgjengelig for utviklere gjennom postman, denne delen vil detaljere de vanligste funksjonene som brukes. Postman lar **forespørselsmetoden** velges på en enkel måte, figuren under illustrerer dette.

![Untitled](1%20RESTful%20Services%2064f7cde060864dca9ca281e3fd84317e/Untitled%206.png)

Overskrifter kan legges til en forespørsel ved å velge fanen **Headers**, figuren nedenfor illustrerer dette.

![Untitled](1%20RESTful%20Services%2064f7cde060864dca9ca281e3fd84317e/Untitled%207.png)

Informasjon kan legges til bodyen i **Body -** fanen, her presenteres ekstra formateringsalternativer. For JSON-data velger du **raw**. Figuren under illustrerer dette.

![Untitled](1%20RESTful%20Services%2064f7cde060864dca9ca281e3fd84317e/Untitled%208.png)

Figuren nedenfor viser hvordan eksempelet på api -kallet ville sett ut hvis det ble laget fra postman.

![Untitled](1%20RESTful%20Services%2064f7cde060864dca9ca281e3fd84317e/Untitled%209.png)

![Untitled](1%20RESTful%20Services%2064f7cde060864dca9ca281e3fd84317e/Untitled%2010.png)

![Untitled](1%20RESTful%20Services%2064f7cde060864dca9ca281e3fd84317e/Untitled%2011.png)

![Untitled](1%20RESTful%20Services%2064f7cde060864dca9ca281e3fd84317e/Untitled%2012.png)

> AKTIVITET Prøv det! Det er mange gratis offentlig hostede Web Api-er. Bruk Postman til å sende en GET-forespørsel om en programmeringsvits til JokesAPI . Du kan bruke URLen https://v2.jokeapi.dev/joke/Programming. Du trenger ikke legge til noen overskrifter i denne forespørselen. Du bør få en vits i JSON-format tilbake i HTTP-svaret.
> 

Du kan enkelt angi **miljøvariabler** ved å følge trinnene nedenfor:

- Klikk på **Manage Environment** fra Innstillinger (ikon tilgjengelig øverst til høyre).
- Klikk på **ADD -** knappen.
- Skriv ned **navnet** på **miljøet** .
- Fyll nøkkel og verdi, som kan brukes som variabel i samling senere.

Figuren under illustrerer dette.

![Untitled](1%20RESTful%20Services%2064f7cde060864dca9ca281e3fd84317e/Untitled%2013.png)

Du kan samle API-forespørsler til en **samling** og eksportere dem for å dele med andre utviklere. Figuren nedenfor illustrerer opprettelsen av en samling.

![Untitled](1%20RESTful%20Services%2064f7cde060864dca9ca281e3fd84317e/Untitled%2014.png)

Figuren nedenfor illustrerer hvordan du **eksporterer** samlinger.

![Untitled](1%20RESTful%20Services%2064f7cde060864dca9ca281e3fd84317e/Untitled%2015.png)

Figuren nedenfor illustrerer hvordan du **importerer** samlinger.

![Untitled](1%20RESTful%20Services%2064f7cde060864dca9ca281e3fd84317e/Untitled%2016.png)

> MERK : Du vil jobbe tettere med å konfigurere og sende HTTP-forespørsler i frontend-delen av dette kurset.
> 

Å skrive API-tester i Postman er rett frem. Du kan skrive og kjøre tester for hver forespørsel ved å bruke JavaScript. Du navigerer til **Test-** fanen. Figuren under illustrerer beskrivelsen av en test.

![Untitled](1%20RESTful%20Services%2064f7cde060864dca9ca281e3fd84317e/Untitled%2017.png)

Figuren nedenfor gir et eksempel på et **testskript** .

![Untitled](1%20RESTful%20Services%2064f7cde060864dca9ca281e3fd84317e/Untitled%2018.png)

Figuren nedenfor er et eksempel på et **testresultat** .

![Untitled](1%20RESTful%20Services%2064f7cde060864dca9ca281e3fd84317e/Untitled%2019.png)

Eksempelet ovenfor var en enkel test for å se om en mottatt svarkode er 200. Du kan ha så mange tester du vil for en forespørsel. De fleste testene er like enkle JavaScript-utsagn. Utdraget nedenfor illustrerer noen ekstra tester som kan skrives for denne forespørselen.

![Untitled](1%20RESTful%20Services%2064f7cde060864dca9ca281e3fd84317e/Untitled%2020.png)

## REST navnekonvensjoner

Som nevnt tidligere, i REST, kaller vi primærdatarepresentasjon for Ressurs. Å ha en sterk og konsistent REST-ressursnavngivningsstrategi er godt design. REST API-er bruker `Uniform Resource Identifiers`(URI) for å adressere ressurser. REST API-designere bør lage URIer som formidler en REST APIs ressursmodell til potensielle klientutviklere. De gjør dette ved å lage URI-maler som lar klientene konstruere URL-er fra bruk av ressurser. URI-maler er ganske enkelt URL-er med variabler, kalt uttrykk og pakket inn i krøllete klammeparenteser. Når ressurser er navngitt godt, er en API intuitiv og enkel å bruke. Hvis det gjøres dårlig, kan det samme API-et føles vanskelig å bruke og forstå.

En `ressurs`kan være en `singleton`eller en `collection`. For eksempel `customers` er en samlingsressurs og `customer` er en enkeltressurs (i et bankdomene). Vi kan identifisere `customers` samlingsressursen ved å bruke URIen `/customers`. Vi kan identifisere én enkelt `customer` ressurs ved å bruke URIen `/customers/{ customerId }`.

En ressurs kan også inneholde undersamlingsressurser. For eksempel, under-samlingsressur `accounts` til en bestemt `customer` identifiseres ved å bruke URI `/customers/{ customerId }/accounts` (i et bankdomene). På samme måte kan en enkeltressurs `account` inne i under-samlingsressurs `accounts` identifiseres som følger: `/customers/{ customerId }/accounts/ { accountId }`.

Denne delen inneholder noen beste fremgangsmåter for å bestemme endepunktnavn og den generelle strukturen til API-en din.

### Bruk substantiv for å representere ressurser

RESTful URI bør referere til en ressurs som er en *ting* (substantiv) i stedet for å referere til en *handling* (verb) fordi substantiv har egenskaper som verb ikke har. Noen eksempler på en ressurs er:

- Brukere av systemet
- Brukerkontoer
- Nettverksenheter etc.

Utdraget nedenfor gir noen eksempler på URIer for substantivene ovenfor.

```
http://api.example.com/device-management/managed-devices
http://api.example.com/device-management/managed-devices/{device-id}
http://api.example.com/user-management/users/
http://api.example.com/user-management/users/{id}
```

Du kan dele ressursarketypene inn i fire kategorier ( `document`, `collection`, `store` og `controller`) og deretter bør du alltid målrette deg mot å sette en ressurs i én arketype og deretter bruke dens navnekonvensjon konsekvent. For enhetlighetens skyld, motstå fristelsen til å designe ressurser som er hybrider av mer enn én arketype. Vær oppmerksom på at dette bare er et eksempel på hvordan URIer kan konstrueres basert på substantivnavn, det er ikke det nødvendige formatet. Poenget er å velge en konsekvent måte å representere ulike områder av søknaden din på.

**1. Doument**

En dokumentressurs er et enkelt konsept som er som en objektforekomst eller databasepost. I REST kan du se den som en enkelt ressurs i ressurssamlingen. Et dokuments tilstandsrepresentasjon inkluderer vanligvis både felt med verdier og lenker til andre relaterte ressurser. Bruk `entallsnavn` for å angi dokumentressursarketype. Utdraget nedenfor gir noen eksempler.

```
http://api.example.com/device-management/managed-devices/{device-id}
http://api.example.com/user-management/users/{id}
```

**2. Collection** En samlingsressurs er en serverstyrt katalog med ressurser. Klienter kan foreslå nye ressurser som skal legges til en samling. Det er imidlertid opp til samlingen å velge å opprette en ny ressurs eller ikke. En samlingsressurs velger hva den vil inneholde og bestemmer URIene til hver inneholdt ressurs. Bruk `flertallsnavn`for å angi samlingsressursarketype. Utdraget nedenfor gir noen eksempler.

```java
http://api.example.com/device-management/managed-devices
http://api.example.com/brukeradministrasjon/brukere
http://api.example.com/brukeradministrasjon/brukere/{id}/kontoer
```

**3. Store** En butikk er et klientadministrert ressurslager. En butikkressurs lar en API-klient sette inn ressurser, få dem ut igjen og bestemme når de skal slettes. En butikk genererer aldri nye URIer. I stedet har hver lagrede ressurs en URI som ble valgt av en klient da den opprinnelig ble lagt inn i butikken. Bruk `flertallsnavn`for å angi butikkressursarketype. Utdraget nedenfor gir noen eksempler.

```
http://api.example.com/cart-management/users/{id}/carts
http://api.example.com/song-management/users/{id}/playlists
```

**4. Controller** En kontrollerressurs modellerer et prosedyrekonsept. Kontrollerressurser er som kjørbare funksjoner, med parametere, returverdier, innganger og utganger. Bruk `verb`for å betegne kontrollerarketype. Utdraget nedenfor gir noen eksempler.

```
http://api.example.com/cart-management/users/{id}/carts/checkout
http://api.example.com/song-management/users/{id}/playlists/play
```

### Konsistens er nøkkelen

Bruk konsistente ressursnavnekonvensjoner og URI-formatering for minimal tvetydighet og maksimal lesbarhet og vedlikeholdbarhet. Noen gode regler for konsistens er i de følgende underavsnittene.

**1. Bruk skråstrek (/) for å indikere hierarkiske relasjoner** Skråstreken ( `/`) brukes i banedelen av URIen for å indikere et hierarkisk forhold mellom ressurser.

```
http://api.example.com/device-management
http://api.example.com/device-management/managed-devices
http://api.example.com/device-management/managed-devices/{id}
http://api.example.com/device-management/managed-devices/{id}/scripts
http://api.example.com/device-management/managed-devices/{id}/scripts/{id}
```

**2. Ikke bruk etterfølgende skråstrek (/) i URIer** Som det siste tegnet i en URIs bane, gir en skråstrek ( `/`) ingen semantisk verdi og kan forårsake forvirring. Det er bedre å droppe dem helt.

Dårlig eksempel:

```
http://api.example.com/device-management/managed-devices/
```

**3. Bruk bindestreker (-) for å forbedre lesbarheten til URIer** For å gjøre URIene dine enkle å skanne og tolke, bruk bindestrek ( `-`) for å forbedre lesbarheten til navn i lange banesegmenter.

Dårlig eksempel:

```
http://api.example.com/managedEntities/{id}/installScriptLocation
```

Lesbart eksempel:

```
http://api.example.com/managed-entities/{id}/install-script-location
```

1.  **Ikke bruk understrek (_)** Det er mulig å bruke et understrek i stedet for en bindestrek som skal brukes som skilletegn. Avhengig av applikasjonens skrifttype, er det mulig at understrekingstegnet enten kan bli delvis skjult eller helt skjult i enkelte nettlesere eller skjermer. For å unngå denne forvirringen, bruk bindestrek (`-`) stedet for understreking ( `_`).``

Dårlig eksempel:

```
http://api.example.com/managed_entities/{id}/install_script_location
```

**5. Bruk små bokstaver i URIer** Når det er praktisk, bør små bokstaver konsekvent foretrekkes i URI-baner. RFC 3986 definerer URIer som store og små bokstaver bortsett fra skjemaet og vertskomponentene.

**6. Ikke bruk filtypene** Filtypene ser dårlige ut og gir ingen fordel. Fjerning av dem reduserer også lengden på URIer.

Bortsett fra grunnen ovenfor, hvis du vil fremheve medietypen API ved å bruke filtypen, bør du stole på medietypen, som kommunisert gjennom `innholdstypeoverskriften`, for å bestemme hvordan du skal behandle innholdet i kroppen.

### Bruk aldri CRUD-funksjonsnavn i URIer

URIer skal ikke brukes for å indikere at en CRUD-funksjon utføres. URIer skal brukes til å identifisere ressurser unikt og ikke noen handling på dem. HTTP-forespørselsmetoder bør brukes for å indikere hvilken CRUD-funksjon som utføres.

Dårlig eksempel:

```
http://api.example.com/device-management/get-managed-devices/
```

### Bruk spørringskomponent for å filtrere URI-samling

Mange ganger vil du komme over krav der du trenger en samling av ressurser sortert, filtrert eller begrenset basert på en viss ressursattributt. For dette, ikke opprett nye APIer – aktiver heller `sorterings-`, `filtrerings-`og `nummereringsfunksjoner`i ressursinnsamlings-API og send inn parameterne som spørringsparametere.

```
.../device-management/managed-devices
.../device-management/managed-devices?region=USA
.../device-management/managed-devices?region=USA&brand =XYZ
.../device-management/managed-devices?region=USA&brand=XYZ&sort=installation-date
```

### Representerer relaterte ressurser

Når en ressurs etterspørres fra et API, er det ofte en tilknyttet ressurs i databasen. Et eksempel på dette er `books` og `authors`; boken har forfattere. Når en bokressurs etterspørres, skal de tilknyttede forfatterne også returneres. Den enkleste måten å returnere forfatterne på er å ha en rekke forfatter - `ID`-er , som vist i kodebiten nedenfor.

```json
{
  "bookid": 1,
  "booktitle": "Example book",
  "authors": [
    {
      "authorid": 1,
      ...
    },
    {
      "authorid": 2,
      ...
    }
  ]
}
```

Dette virker greit, men det har flere ulemper. Hvis klienten ønsket å få informasjonen til disse forfatterne, måtte de konstruere URL-ene selv ved å se på dokumentasjonen. Dette er ikke beste praksis, da APIer bør utformes med klienten uten forkunnskaper utover den opprinnelige URIen (bokmerket). Dette løses ved å returnere lenker til ressursene i tillegg til en identifikator. I dette tilfellet legges en `authorLink` til.

```json
{
  "bookid": 1,
  "booktitle": "Example book",
  "authors": [
    {
      "authorid": 1,
      "authorLink": "/authors/1",
    },
    {
      "authorid": 2,
      "authorLink": "/authors/2",
    }
  ]
}
```

På denne måten trenger ikke klienten å se gjennom dokumentasjonen for å se hvordan man navigerer til relaterte forfattere, og de vet at forfattere og bøker er relatert. Å inkludere en `self` egenskap gjør det eksplisitt hvilke nettressursersegenskaper vi snakker om uten å kreve kontekstuell kunnskap – som hvilken URL utførte du en GET på for å hente denne representasjonen – eller applikasjonsspesifikk kunnskap. Dette er spesielt viktig for nestede JSON-objekter der den ytre konteksten ikke hjelper til med å fastslå hvilken ressurs vi snakker om, men det er god praksis overalt. Kodebiten nedenfor viser det samme bok-svaret med dette nye feltet i tankene.

```json
{
  "self": "/books/1",
  "bookid": 1,
  "booktitle": "Example book",
  "authors": [
    {
      "authorid": 1,
      "self": "/authors/1",
    },
    {
      "authorid": 2,
      "self": "/authors/2",
    }
  ]
}
```

Det er flere andre spesifikasjoner å velge mellom som definerer mer komplekse tilnærminger for koblingsrepresentasjon, som `Siren`, `HAL`, `JSON-LD`, `RDF/JSON`, `Collection+JSON`, `Hydra`, og sannsynligvis flere. Hver av disse legger til betydelige funksjoner og konsepter utover bare hvordan de skal representere lenker. Hensikten med denne delen var å illustrere bruken av lenker for å hjelpe klienter når de bruker API. Nøkkelpoenget er å ikke tvinge klienter til å gå gjennom dokumentasjon for å identifisere hvordan de kan få tak i de relaterte ressursene og å eksplisitt vise at det er et forhold mellom enheter. Dette krever mye av gjetningen for kundene. For eksempel bruker Microsoft navigering i [HATEOAS-stil](https://docs.microsoft.com/en-us/azure/architecture/best-practices/api-implementation#provide-links-to-support-hateoas-style-navigation-and-discovery-of-resources) og oppdagelse av ressurser for API-design; denne metoden gir et lenkeobjekt der du beskriver alle relaterte ressurser og oppgir URL-er og operasjonstyper.

### Filtrering, sortering og nummerering

**Filtrering**

Muligheten til å filtrere data er en viktig funksjon som alle REST APIer bør gi. Grunnleggende filtrering gjør det mulig å velge ressurser ved å matche ett eller flere medlemmer av ressursen til verdier som sendes som spørringsparametere. For eksempel:

```
GET /books?genre=horror
```

Vil returnere alle bøker som har sjangeren skrekk. Mer komplekse implementeringer kan legge til støtte for grunnleggende operatører ( f.eks `<`, `>`) og komplekse boolske operatorer (f.eks.`and` & `or`) som kan gruppere flere filtre sammen.

```
GET /books?genre=horror&language=English&price<1000
```

Implementeringen av søket gjøres på serversiden; Derfor er måten søk håndteres på opp til utvikleren. Den enkle spørringsmetoden vist her er rett frem fra klientens perspektiv, men kan være kompleks fra et utviklingssynspunkt; det er fortsatt den vanligste metoden sett.

**Sortering**

Sortering bestemmes normalt ved bruk av `sort` spørringsstrengparameteren. Verdien til denne parameteren er en kommadelt liste over sorterings-`keys`. Sorterings-`retninger` kan eventuelt legges til hver sorteringsnøkkel, atskilt med `:` -tegnet. Sorteringsretninger er vanligvis enten `asc` for stigende eller `desc` for synkende.

```
GET /books?genre=horror&sort=price:asc
```

Dette vil få alle bøker med skrekksjangeren og sortere deretter etter stigende pris. Nok en gang er det mange måter å håndtere sorteringsprosessen på og det gjøres på serversiden; dette er bare den vanligste løsningen.

**Nummerering**

API-spørringer til tette databaser kan potensielt returnere millioner, om ikke milliarder, av resultater. Dette resulterer i en veldig tung overhead(lite ledig plass) og belastning på serveren og skaper forsinkelser for klienten. Nummerering bidrar til å begrense antall resultater for å holde nettverkstrafikken i sjakk. Når du returnerer resultater som er nummerert, er det beste praksis å inkludere noen tilleggsfelt.

- først
- forrige
- selv-
- neste
- siste

Disse feltene vil ha URL-er generert for å navigere til; den første siden, den forrige siden, denne siden, den neste siden og den siste siden.

**Offset nummerering** Dette er den enkleste formen for personsøking. `Limit/Offset`ble populært med apper som bruker `SQL-databaser` som allerede har `LIMIT`og `OFFSET`som en del av `SQL SELECT`-syntaksen. Svært lite forretningslogikk kreves for å implementere grense-/forskyvningssøking.

Et eksempel på grense-/forskyvningssøking er:

```
GET /items?limit=20&offset=100
```

Denne spørringen vil returnere de 20 radene som starter med den 100. raden. Ulempen med offsetpaginering er at den snubler når man arbeider med store offsetverdier og legger til nye oppføringer i tabellen kan forårsake forvirring, som er kjent som sidedrift.

**Nøkkelset nummerering**

Nøkkelset nummerering bruker filterverdiene på forrige side for å bestemme neste sett med elementer. Disse resultatene vil deretter bli indeksert.

Tenk på dette eksemplet:

- Klienten ber om de siste elementene `GET /items?limit =20`
- Når du klikker på neste side, finner søket minimum opprettet dato `2019–01–20T00:00:00`. Dette brukes deretter til å lage et spørringsfilter for neste side. `GET /items?limit=20& created:lte :2019-01-20T00:00:00`.

Og så videre…

Fordelene med denne tilnærmingen er at den ikke krever ekstra backend-logikk. Det krever bare én grense-URL-parameter. Den har også konsekvent sortering, selv når nye elementer legges til databasen. Det fungerer også jevnt med store offset-verdier.

**Søk nummerering**

Søk nummerering er neste trinn utover tastesettpaginering. Ved å legge til spørringene `after_id`og `before_id`, kan du fjerne begrensningene for filtre og sortering. Unike identifikatorer er mer stabile og statiske enn felter med lavere kardinalitet, for eksempel tilstands enumer eller kategorinavn. Den eneste ulempen med å søke nummerering er at det kan være utfordrende å lage tilpassede sorteringsrekkefølger.

Tenk på dette eksemplet:

- Klienten ber om en liste over de nyeste varene `GET items?limit=20`.
- Klienten ber om en liste over de neste 20 elementene, ved å bruke resultatene av den første spørringen `GET /items?limit=20&after_id=20`.
- Klienten ber om den neste/rulle siden, og bruker den siste oppføringen fra den andre siden som startpunktet `GET /items?limit=20&after_id=40`.

Fordeler med å søke nummerering:

- Kobler filterlogikk fra nummereringslogikk
- Konsekvent bestilling, selv når nye elementer legges til databasen. Sorterer de nyeste elementene godt.
- Fungerer jevnt, selv med store forskyvninger.

Ulemper med Søk nummerering:

- Mer krevende for backend-ressurser ved bruk av offset- eller nøkkelset nummerering
- Når elementer slettes fra databasen, kan `start_id`ikke lenger være en gyldig id.

### Versjonskontroll

En API er svært sjelden helt uendret etter utgivelse, en redesign blir ofte gjort på et tidspunkt. For å forhindre brudd på alle klientene som bruker API, kan versjonskontroll brukes. Dette gjør det mulig for det eldre systemet å være operativt parallelt med det nye og gi kundene tid til å bytte over eller akseptere bruken av en utdatert ressurs. Det er tre hovedmåter å versjonere APIen din:

- URL, også kjent som ruteversjon (f.eks `/v1/:foo/:bar/:baz`).
- Versjonsbehandling via en tilpasset overskrift ( f.eks `Godta-versjon: v2`).
- (f.eks `/:foo/:bar/:baz?version=v2`).

Av de tre nevnte er ruteversjon den vanligste. Det er veldig enkelt å oppnå fordi det ganske enkelt er et prefiks av `vx`til endepunktene. Mange rammeverk støtter rutedefinert versjonskontroll, og når den ikke gjør det, kan du ganske enkelt strukturere API-en din slik at den tillater ruteversjon. På grunn av dette er det ekstremt enkelt å implementere; det gir imidlertid lengde til REST API-ruten din, noe som er ulempen.

Egendefinerte overskrifter kan virke veldig rett frem og enkle, men ikke alle klienter ønsker å levere en tilpasset overskrift fordi de bare vil trekke ut noe informasjon. Vanligvis vil du ikke tvinge klienten til å gjøre noe de kanskje ikke vil. I tillegg krever det overhead på serveren for å analysere før svaret, noe som kan legge til ventetid.

Det siste alternativet er  “dårlig praksis”, der versjonskontroll er en ettertanke for designerne. Denne metoden bør unngås for enhver pris, men hvis det ikke er noe annet alternativ; det kan gjøres.

Copyright 2022, Noroff Accelerate AS