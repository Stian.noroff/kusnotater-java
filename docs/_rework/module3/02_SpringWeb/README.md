# 2. Bygge et REST API i Spring

# Bygge et REST API i Spring

Denne leksjonen dekker ulike merknader og underliggende teknologier for å aktivere en REST API i Java. Denne leksjonen skisserer hva Spring Web-rammeverket tilbyr og hvordan det kan brukes til å eksponere databaseinteraksjon via Spring Data.

**Læringsmål:**

Du vil kunne…

- **forklare** MVC-mønsteret og dets implementering i Spring Boot.
- **demonstrere** bruken av merknader for å konfigurere en MVC-aktivert applikasjon.
- **forklar** rollen Tomcat spiller i Spring Boot-applikasjoner.
- **diskuter** hva en http -servlette er og hvordan den brukes.
- **utvikle** en kontroller med endepunkter.
- **demonstrere** å trekke ut informasjon fra forespørselsparametere og path variabler.
- **demonstrere** eksponeringsinformasjon over REST-endepunkter, gitt en kravbeskrivelse og et sett med modeller.
- **organisere** relaterte endepunkter i kontrollere.

## Spring web

Å bygge et riktig REST API er en ikke-triviell prosess. På et høyt nivå må applikasjonen:

- Vær vert på en server for å motta HTTP-forespørsler
- Trenger å tilordne forespørsels-URLer til metoder
- Trenger å kunne skille mellom ulike HTTP-metoder
- Trenger å deserialisere JSON til Java-objekter
- Trenger å serialisere Java-objekter til JSON
- Trenger å lage passende responsobjekter og koder

Heldigvis har Spring en avhengighet og en startmal for nettapplikasjoner. Det er tatt hensyn til vanlige webapplikasjoner og REST APIer. Spring Web er også Cloud-klar og er bygget med testing i tankene.

Avhengigheten er følgende:

```bash
implementation 'org.springframework.boot:spring-boot-starter-web'
```

En full applikasjon med: Web, Data, Postgres, Testing og DevTools vil ha følgende avhengigheter:

```bash
dependencies {
    implementation 'org.springframework.boot:spring-boot-starter-data-jpa'
    implementation 'org.springframework.boot:spring-boot-starter-web'
    developmentOnly 'org.springframework.boot:spring-boot-devtools'
    runtimeOnly 'org.postgresql:postgresql'
    testImplementation 'org.springframework.boot:spring-boot-starter-test'
}
```

> MERK : DevTools er en livskvalitetsforbedrende avhengighet som gir live reload blant annet.
> 

Spring Web gir mange merknader for å konfigurere følgende:

- Ruting
- Http-metoder
- Svartyper
- Serialisering og deserialisering

> **MERK** : Det er mange andre merknader tilgjengelig, de diskuteres når de støtes på.
> 

En Spring Web-applikasjon er vert på en [Tomcat -](https://tomcat.apache.org/) server som standard, men kan konfigureres til å fungere med andre, som [Jetty](https://www.eclipse.org/jetty/) . Hvis applikasjonen kjøres uten noe annet konfigurert, vil det bli logget en ny linje:

```bash
Tomcat startet på port(er): 8080 (http) med kontekstbane ''
```

Når du navigerer til `localhost:8080`gjennom en nettleser, blir du møtt med det som er kjent som en **Whitelabel Error Page** . Dette er ganske enkelt en standard feilside når ingen er konfigurert. Grunnen til at vi ser denne siden er fordi det ikke er gjort noen konfigurasjon på dette tidspunktet.

Når du oppretter klasser for å konfigurere applikasjonen, må vi huske på at Spring Web er opprettet for å tilpasse seg Model View Controller (MVC)-mønsteret.

## Arkitektur og flyt

Model-View-Controller (MVC) arkitektonisk mønster, eller designmønster, skiller en applikasjon inn i tre hovedgrupper av relaterte komponenter: *Modeller* , *Visninger* og *Kontrollere* . Dette bidrar til å oppnå separasjon av bekymringer.

Disse komponentene danner lag i vår applikasjon, på et høyt nivå:

- Modelllaget håndterer alle datainteraksjoner **.** Dette inkluderer: domenemodeller , forretningslogikk og datatilgang.
- Kontrollerlaget håndterer all brukerinteraksjon**.** Den lytter etter forespørsler, trekker ut informasjon fra forespørselen, utsetter ansvaret for datainteraksjon til modelllaget, og konverterer deretter resultatet til et format som passer for visningslaget.
- Visningslaget håndterer alle **visuelle** representasjoner av applikasjonen vår.

I en 3-lags arkitekturapplikasjon utgjør kontrolleren det som er kjent som *Presentation Layer*. Det er også et *Business Logic Layer* og et *Data Access Layer*. Dette er en ofte sett arkitektur da den stemmer godt overens med MVC-mønsteret. For formålet med dette kurset brukes en generalisert lagdelt tilnærming til arkitektur. Andre varianter diskuteres når det er hensiktsmessig.

Diagrammet nedenfor illustrerer flyten til en generisk MVC-applikasjon:

![Untitled](2%20Bygge%20et%20REST%20API%20i%20Spring%20fa7e1e7cf1004a3486d5138b75e5b136/Untitled.png)

I sammenheng med en REST API opprettet med Spring Web, oppnås følgende flyt:

1. *Brukeren* gjør en HTTP-forespørsel gjennom en *klient* (nettleser).
2. En *frontkontroller* ( `DispatcherServlet`) lytter til alle innkommende forespørsler og videresender (delegerer) dem til en *Handler Mapper* .
3. Handler Mapper videresender deretter forespørselen til riktig *kontrollør* .
4. Kontrolleren deserialiserer deretter forespørselen og trekker ut alle nødvendige metadata.
5. Kontrolløren delegerer deretter forespørselen til modelllaget via en *tjeneste* .
6. Tjenesten bruker deretter et depot for å manipulere alle databasedata og utføre alle forretningslogikkrelaterte operasjoner.
7. Tjenesten returnerer deretter alle data til kontrolløren som deretter oppretter et *svarobjekt* .
8. Responsobjektet sendes deretter tilbake til DispatcherServlet som sender det tilbake til klienten for å bli sett av brukeren.

Diagrammet nedenfor illustrerer denne prosessen:

![Untitled](2%20Bygge%20et%20REST%20API%20i%20Spring%20fa7e1e7cf1004a3486d5138b75e5b136/Untitled%201.png)

Det meste av funksjonaliteten som vises her leveres automatisk av Spring, vi trenger bare å bruke den. Spring vil automatisk lytte etter forespørsler og rute dem til passende kontrollermetoder hvis vi gir de riktige merknadene.

Modellen *vist* i diagrammet ovenfor representerer ikke nødvendigvis en *enhet*. Det er bare en hvilken som helst innkapsling av returnerte data. Årsaken til dette skillet diskuteres i detalj i en senere leksjon, men den korte versjonen er: *du vil unngå å sende domeneobjekter (entiteter ) tilbake til en klient*. Foreløpig skal vi gjøre dette.

En siste ting å gjenta er at denne flyten er spesifikt for en REST API. Det er en egen flyt for en fullstendig MVC-applikasjon som bruker en View Templating Engine som *Thymeleaf* der visninger (html-sider med Java-kode) må løses. Dette er ikke inkludert i kurset da det er laget for å skille front-end og back-end.

## Konfigurering av kontrollere

Det burde være tydelig nå at kontrolleren er kjerneaspektet i en MVC-aktivert applikasjon. Dette betyr at det meste av konfigurasjonen skjer på kontrolleren.

Aktiveringskommentaren for en kontroller er `@RestController`. Den kombinerer de eldre `@ Controller-`og @ `ResponseBody-`kommentarene, og bidrar til å redusere mengden synlig kjeleplatekode.

> **MERK** : @Controller brukes fortsatt når vi vil ha en MVC-aktivert applikasjon - gjennom Thymeleaf vanligvis.
> 

En klasse merket med `@RestController`kan blant annet utføre følgende:

- Brukes til å løse en URL til en `@RequestMapping`
- Konverter JSON til Java-objekter (og omvendt )
- Opprett en `ResponseEntity`med passende overskrifter

### Grunnleggende URL-kartlegging

Nedenfor er et eksempel på malen til en REST-kontroller:

```java
@RestController
public class FooBarController {
  @GetMapping("foo") // GET: localhost:8080/foo
  public ResponseEntity<String> foo() {
      return new ResponseEntity<>("Foo!", HttpStatus.OK);
  }
  @GetMapping("bar") // GET: localhost:8080/bar
  public ResponseEntity<String> bar() {
      return ResponseEntity.ok().body("Bar!");
  }
  // GET: localhost:8080/baz
  @RequestMapping(method = RequestMethod.GET, path = "baz")
  public ResponseEntity<String> baz() {
      return ResponseEntity.ok().body("Baz!");
  }
}
```

`@GetMapping-`kommentarene konfigurerer hvordan URL-er tilordnes kontrollermetodene. Det er ulike kartleggingskommentarer for hvert **Http-verb**. Det er også en generisk `@RequestMapping`som kan konfigureres med en `metode`og en `bane`, dette er hvordan `baz-`metoden er konfigurert.

Husk at Spring bruker noe som kalles en **komponentskanning** for å registrere alle bønner i applikasjonen. Komponentskanning er det som registrerer alle tilordningene og nettadressene som skal brukes av *Dispatcher Servlet* og *Mapping Handler*. Denne prosessen kan illustreres i diagrammet nedenfor:

![Untitled](2%20Bygge%20et%20REST%20API%20i%20Spring%20fa7e1e7cf1004a3486d5138b75e5b136/Untitled%202.png)

> **MERK** : En url /mapping må være unik. Eventuelle tvetydige ruter vil gi en feil under kjøring.
> 

Den generiske klassen `ResponseEntity`hjelper til med å konfigurere responsobjektet produsert av serveren. Dette tillater konfigurasjon av overskrifter, status og brødtekst. Det er to måter å produsere en responsenhet på; ved å lage et nytt objekt, eller ved å bruke responsbyggeren. Valget mellom disse to er stort sett personlig preferanse, men til tider er det lettere å bruke på den andre.

Ofte har URI-skjemaer en delt basis-URL. Denne basis-URLen kan inkludere navnet på kontrolleren samt versjonen av API. For å redusere mengden av gjentatt kode, kan kontrolleren merkes med `@RequestMapping( path = "base- url ")`for å bruke en basisrute til alle metoder i kontrolleren.

### Bruke forespørselsmetadata

Å kunne kartlegge URL-er til metoder er en del av bildet. Et annet viktig aspekt er å gjøre bruk av informasjonens preset i forespørselen. Dette er:

- Banevariabler
- Spørrestrenger
- Overskrifter
- Body
- Skjemaer

Det er merknader for hver av disse funksjonene, kodebiten nedenfor illustrerer dem:

```java
@RestController
@RequestMapping(path = "api/v1/test") // Base URL
public class TestController {
  // Extracting path variable
  @GetMapping("{id}") // GET: localhost:8080/api/v1/test/1
  public ResponseEntity<String> path(@PathVariable int id) {
      return ResponseEntity.ok().body(String.valueOf(id));
  }
  // Extracting a query string
  @GetMapping // GET: localhost:8080/api/v1/test?key=value
  public ResponseEntity<String> query(@RequestParam String key) {
      return ResponseEntity.ok().body(key);
  }
  // Extracts the User-Agent header
  @GetMapping("header") // GET: localhost:8080/api/v1/header
  public ResponseEntity<String> headers(@RequestHeader("User-Agent") String agent) {
      return ResponseEntity.ok().body(agent);
  }
  // Extracts the body
  @GetMapping("body") // GET: localhost:8080/api/v1/body
  public ResponseEntity<String> body(@RequestBody String value) {
      return ResponseEntity.ok().body(value);
  }
}
```

Valget av hvilken metode som skal brukes avhenger av konteksten. Det er imidlertid viktig å følge REST-konvensjonene. For håndtering av forespørsler betyr dette:

- Bruk banevariabler for enkeltstående ressurser
- Bruk søkestrenger for filtrering
- Bruk brødtekst for POST/PUT/PATCH

> **MERK**: Egendefinerte overskrifter kan også legges til svar.
> 

## Integrering av datatilgang

For å få et mer fullstendig bilde av en Java-webserver, må datatilgang være integrert. Dette driver arkitekturen til applikasjonen og veileder en utvikler om hvilke kontrollere de skal lage. Eksemplet som ble brukt bygger på applikasjonen som ble utviklet da *Hibernate* ble dekket. En kontroller må opprettes per domenekontekst. I vårt tilfelle trenger vi følgende kontrollere:

- Student
- Professor
- Emne
- Prosjekt

Disse klassene går i `kontrollerpakken`og danner et lag i arkitekturen. For hver kontroller må funksjonaliteten fra den relaterte tjenesten være til stede. Følgende kodebiter illustrerer hvordan `StudentController`er konstruert:

```java
@RestController
@RequestMapping(path = "api/v1/students")
public class StudentController {

  private final StudentService studentService;

  public StudentController(StudentService studentService) {
      this.studentService = studentService;
  }
```

Først defineres en basis-URL og den relevante tjenesten injiseres. Det neste trinnet er å implementere hver tjenestemetode:

```java
@GetMapping // GET: localhost:8080/api/v1/students
public ResponseEntity<Collection<Student>> getAll() {
    return ResponseEntity.ok(studentService.findAll());
}
```

```java
@GetMapping("{id}") // GET: localhost:8080/api/v1/students/1
public ResponseEntity<Student> getById(@PathVariable int id) {
    return ResponseEntity.ok(studentService.findById(id));
}
```

```java
@GetMapping("search") // GET: localhost:8080/api/v1/students/search?name=Ola
public ResponseEntity<Collection<Student>> findByName(@RequestParam String name) {
    return ResponseEntity.ok(studentService.findAllByName(name));
}
```

For å implementere søket må en søkestreng oppgis. Dette er i tråd med REST beste praksis.

```java
@PostMapping // POST: localhost:8080/api/v1/students
public ResponseEntity add(@RequestBody Student student) {
    Student stud = studentService.add(student);
    URI location = URI.create("students/" + stud.getId());
    return ResponseEntity.created(location).build();
    // return ResponseEntity.status(HttpStatus.CREATED).build();
}
```

For å opprette en ny student, må dataene komme gjennom *body*. Den nye student-IDen brukes deretter til å bygge en URL hvor den kan hentes. Den `opprettede`metoden krever at en plassering oppgis for å legges til som en `stedsoverskrift`. Dette er den forventede oppførselen til en opprettet respons. Det er ikke obligatorisk, og ofte ikke nødvendig. For å unngå å måtte generere en url, kan statuskoden returneres manuelt (sett i den kommenterte kodelinjen). 

Formatet på nettadressen vil avhenge av behovene. Den fullstendige URL-en kan være nødvendig ( `localhost:8080/v1/...`) eller bare halen ( `students/:id`). Dette avhenger av den forbrukende klienten. Ofte har grensesnittapplikasjoner basis- url -en lagret i en http-tjeneste ( `localhost:8080/v1/ api`). 

```java
@PutMapping("{id}") // PUT: localhost:8080/api/v1/students/1
public ResponseEntity update(@RequestBody Student student, @PathVariable int id) {
    // Validates if body is correct
    if(id != student.getId())
        return ResponseEntity.badRequest().build();
    studentService.update(student);
    return ResponseEntity.noContent().build();
}
```

Oppdateringer (PUT og PATCH) krever et ekstra trinn for valideringsformål. Dette innebærer en sjekk for å se om ID-en i banen samsvarer med ID-en til kroppen. Det gir ganske enkelt ekstra beskyttelse mot menneskelige feil og kan beskytte mot angrep som retter seg mot forespørselsorganer (man-in-the-middle-angrep). Dette håndheves ikke, men for å samsvare med beste praksis.

Hvis ID-ene ikke stemmer overens, er det kjent som en `dårlig forespørsel`, noe som betyr at forespørselen er feil utformet og ikke skal behandles.

```java
@DeleteMapping("{id}") // DELETE: localhost:8080/api/v1/students/1
public ResponseEntity delete(@PathVariable int id) {
    studentService.deleteById(id);
    return ResponseEntity.noContent().build();
}
```

### Midlertidig løsning for sykliske referanser

Når du kjører applikasjonen og gjør en GET-forespørsel, gjentas dataene uendelig til nettleseren går tom for minne. Dette betyr at det er en syklisk referanse. Dette er et veldig vanlig problem når du jobber med en ORM da det ikke er et skille mellom vårt domene og det som vises til klienten.

Å forklare hva som skjer her er ganske enkelt. Når du ser på enhetene, vil du legge merke til noe.

`Studentenheten`har for eksempel en `professor`den er tilknyttet. At `professor`har en samling av `studenter`den er tilknyttet, samt `emner`. Disse studentene har hver en `professor`, og hvert av disse emnene har en samling av `studenter`og en `professor`.

Det er åpenbart at når Java-objektene konverteres til JSON og alle deres `getX-`metoder kalles, skaper dette et dypt nestet nett av data som fører til den uendelige løkken.

For å løse dette har vi flere alternativer. Det enkleste og mest rett frem er å bruke `Jackson Databind -`biblioteket og det andre er å implementere *dataoverføringsobjekter*. Sistnevnte har en dedikert leksjon til seg, så foreløpig vil vi gjøre førstnevnte.

Selv om bruk av *Jackson* ikke er en komplett eller beste praksis-løsning, fungerer den fortsatt når den er i klemme. Måten Spring serialiserer og deserialiserer JSON på, er via *Jackson Databind -* biblioteket. Vi trenger bare å benytte oss av det med noen merknader for å endre hvordan det skjer.

De vanligste merknadene for å fjerne sykliske referanser er `@JsonIgnore`og `@JsonGetter`. `JsonIgnore setter`ganske enkelt verdien til `null`som om det ikke var noen verdi. `JsonGetter`utsetter serialiseringsprosessen til en bestemt metode. Dette kan gjøres for å returnere ID-ene i stedet for hele objektet. Kodebiten nedenfor illustrerer bruk av `JsonGetter`:

```java
@JsonGetter("professor")
public Integer jsonGetProfessor() {
  if(professor != null)
      return professor.getId();
  return null;
}
@JsonGetter("project")
public Integer jsonGetProject() {
  if(project != null)
      return project.getId();
  return null;
}
```

For `professor`og `prosjekt`er dette rett frem. I stedet for det faktiske objektet, returnerer vi bare ID-en. Nullsjekken er påkrevd siden `getId`ville gi et unntak hvis det kalles på et null-objekt.

```java
@JsonGetter("subjects")
public List<Integer> jsonGetSubjects() {
  if(subjects != null)
      return subjects.stream().map(s -> s.getId())
              .collect(Collectors.toList());
  return null;
}
```

For `emner`må stream API brukes for å kartlegge samlingen til en som representerer ID-ene.

> **MERK**: Disse metodene går i klassen Student-enhet. Navnene kan ikke være getProfessoreller getProjecteller getSubjectsda disse kolliderer med gettere og settere Hibernate er avhengig av.
> 

Resultatet av en GET-forespørsel er nå formatert som vist nedenfor:

```json
{
  "id": 1,
  "name": "Ola Nordmann",
  "project": 1,
  "professor": 1,
  "subjects": [
    1, 2
  ]
}
```

Dette formatet er veldig nært hvordan en DTO ville se ut for å representere Student -domeneklassen, men den viktige forskjellen er at en DTO er definert og tilordnet separat og ikke er knyttet til enhetene selv. Leksjonen som inneholder DTO -er utdyper dette mer.

## Egendefinerte unntak

Hva skjer hvis det ikke er noen student med en bestemt ID? Så langt har dette stort sett blitt sett forbi, for å bli adressert nå når hele konteksten er gitt.

En rett frem løsning ville være å sjekke om studenten eksisterer før den hentes, og deretter returnere `ResponseEntity.notFound ( ).build ();`. Denne metoden fungerer, men den blåser opp kontrollerene våre mer enn nødvendig, og den gir ikke detaljer om hva som er galt.

Husk at du kan opprette et tilpasset unntak som skal kastes når det er nødvendig. Dette kan brukes i sammenheng med en Spring Web-applikasjon, den eneste forskjellen er at en statuskode kan knyttes til unntaket. Dette lar oss benytte Springs automatiserte feilhåndtering og standardsvar.

Et tilpasset unntak er enkelt å definere:

```java
@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class StudentNotFoundException extends RuntimeException {
  public StudentNotFoundException(int id) {
    super("Student does not exist with ID: " + id);
  }
  // Generated constructors omitted
}
```

Her utvider vi `RunTimeException`i stedet for et generelt unntak og vi legger ved en svarkode til det. Det neste spørsmålet er *hvor du skal kaste dette unntaket?*

Husk at når vi bruker repositoriene i tjenestene, vil vi bruke `findById`som returnerer en `valgfri`verdi. Det er en metode vi kan lenke til den som lar oss kaste et unntak. Dette vises i kodebiten nedenfor:

```java
// In StudentServiceImpl
@Override
public Student findById(Integer id) {
  return studentRepository.findById(id)
          .orElseThrow(() -> new StudentNotFoundException(id));
}
```

> **MERK**: Dette resulterer også i at en enkelt forespørsel blir gjort til databasen i stedet for de potensielle to forespørslene når du sjekker om det finnes.
> 

Når vi nå kaller hent en student som ikke eksisterer, får vi følgende svar:

```json
{
    "timestamp": "--omitted--",
    "status": 404,
    "error": "Not Found",
    "trace": "--omitted--",
    "message": "Student does not exist with ID: 9",
    "path": "/api/v1/students/9"
}
```

Dette responsobjektet er kjent som et **Common Response Object** og er hvordan Spring viser all informasjon til klienter fra webserveren. Vi kan lage våre egne versjoner av DTOer.

> **MERK**: Denne feilhåndteringen kan brukes til å rydde opp i noen andre metoder i våre kontrollere der servicenivåkontroller utføres.
> 

> **RESSURSER**: Det finnes andre måter å håndtere feil i en Spring Web-applikasjon. En artikkel som skisserer noen finner du [her](https://www.baeldung.com/exception-handling-for-rest-with-spring).
> 

Copyright 2022, Noroff Accelerate AS