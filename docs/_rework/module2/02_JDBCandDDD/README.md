# 2. JDBC og DDD

# **JDBC og domenedrevet design**

> TODO: Oversikt og LO
> 

## **Introduksjon til Spring Boot**

Spring Framework gir en omfattende programmerings- og konfigurasjonsmodell for moderne Java-baserte bedriftsapplikasjoner - på alle typer distribusjonsplattformer.

Spring er det mest populære rammeverket for Java-utvikling. Se flere motivasjoner fra [why spring?](https://spring-io.translate.goog/why-spring?_x_tr_sl=en&_x_tr_tl=no&_x_tr_hl=no&_x_tr_pto=op,wapp)

Spring fokuserer på "rørlegging" av bedriftsapplikasjoner, slik at teamene kan fokusere på forretningslogikk på applikasjonsnivå, uten unødvendige bånd til spesifikke distribusjonsmiljøer.

Spring Boot gjør det enkelt å lage frittstående, produksjonsbaserte Spring-baserte applikasjoner som du "bare kan kjøre".

**Funksjoner:**

- Lag frittstående Spring-applikasjoner
- Bygg inn Tomcat, Jetty eller Undertow direkte
- Gi meningsfulle "starter"-avhengigheter for å forenkle byggekonfigurasjonen
- Konfigurer Spring- og tredjepartsbiblioteker automatisk når det er mulig
- Gi produksjonsklare funksjoner som beregninger, helsesjekker og eksternalisert konfigurasjon
- Absolutt ingen kodegenerering og ingen krav til XML-konfigurasjon

For dette kurset ligger fokus på å lage webapplikasjoner med Spring Boot. For å komme til det punktet begynner vi med enkle applikasjoner for å lette datatilgang.

> DOKUMENTASJON : Ulike startveiledninger for Spring Boot finner du her .
> 

For å lage Spring Boot-applikasjoner bruker vi **Spring Initializer** . Dette kan gjøres enten med et online verktøy eller gjennom IntelliJ.

![https://noroff-accelerate.gitlab.io/java/course-notes/_rework/module2/02_JDBCandDDD/resources/01_springinitializr.png](https://noroff-accelerate.gitlab.io/java/course-notes/_rework/module2/02_JDBCandDDD/resources/01_springinitializr.png)

Det er flere alternativer å velge mellom angående: artefakter, byggesystemer, språk og versjoner. Valget av hvilke kommer ned til preferanse og prosjektbehov, men vi vil holde oss til:

- Java 17
- Krukkeemballasje
- Enten Gradle eller Maven byggesystem.

> MERK : Gradle velges for leksjonsnotatene nesten alltid, siden den har færre linjer i konfigurasjonen. I praksis er valget av Maven eller Gradle mest ned til personlig preferanse. Gradle, etter vår erfaring, tar imidlertid lengre tid å bygge enn Maven.
> 

Når du har valgt konfigurasjonen, er det neste vinduet hvor du skal konfigurere avhengighetene dine.

![https://noroff-accelerate.gitlab.io/java/course-notes/_rework/module2/02_JDBCandDDD/resources/02_dependencies.png](https://noroff-accelerate.gitlab.io/java/course-notes/_rework/module2/02_JDBCandDDD/resources/02_dependencies.png)

Foreløpig vil vi ikke legge til noen avhengigheter. Ved å klikke på fullfør vil du *starte* opp en Java-applikasjon i henhold til de nødvendige konfigurasjonene. Når det er fullført, opprettes følgende mappestruktur:

![https://noroff-accelerate.gitlab.io/java/course-notes/_rework/module2/02_JDBCandDDD/resources/03_folderstructure.png](https://noroff-accelerate.gitlab.io/java/course-notes/_rework/module2/02_JDBCandDDD/resources/03_folderstructure.png)

Spring Boot legger til noen veldig nyttige funksjoner som standard:

- Hovedklasse med forhåndskonfigurert hovedmetode
- En måte å gi ekstern konfigurasjon via`application.properties`
- JUnit 5 med en testklasse generert

Ser vi dypere, har vi opprettet et inngangspunkt for oss, kalt `SpringTestApplication`. Denne klassen vil bli oppkalt etter prosjektet ditt og er der `main`metoden er plassert og konfugert med Intellij. Når vi ser på selve klassen, er det noen nye ting til stede:

```java
package no.noroff.springtest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplicationpublic class SpringTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringTestApplication.class, args);
    }

}
```

Den første bemerkelsesverdige forskjellen er `@SpringBootApplication`nøkkelordet. Spring bruker merknader med prefiks `@` for å gi klasser konfigurasjon. Merknader gjør ganske enkelt kjeleplaten som trengs for å sette opp visse funksjoner - du kan tenke på dem som en måte å "arve" en forhåndsdefinert konfigurasjon. I dette tilfellet `@SpringBootApplication`spesifiserer merknaden at denne klassen er inngangspunktet for et Spring Boot-prosjekt. Den legger også til *autokonfigurasjon* og *komponentskanning* , med andre ord får denne merknaden et program til å oppføre seg som et Spring Boot-program.

`SpringApplication.run(SpringTestApplication.class, args);`er det neste interessante tillegget. Det ruter inngangspunktet fra applikasjonen til Spring Boot. Grunnen til at dette kreves er veldig enkel - Spring Boot gjør mye bak kulissene, og det er slik du kan bruke det.

> MERK : Ikke fjern eller endre disse konfigurasjonene, da det vil føre til at programmet ikke fungerer som forventet.
> 

Når applikasjonen kjøres, er det en annen utgang enn du ville ha sett hvis du kjører en vanlig Java-applikasjon:

![https://noroff-accelerate.gitlab.io/java/course-notes/_rework/module2/02_JDBCandDDD/resources/04_springstarting.png](https://noroff-accelerate.gitlab.io/java/course-notes/_rework/module2/02_JDBCandDDD/resources/04_springstarting.png)

Spring har intern logging som produserte godt formaterte utganger som vi kan benytte oss av. Det eneste andre bemerkelsesverdige tillegget i dette bildet er linjen om "No active profile set", Spring lar deg angi profiler for å definere separate konfigurasjoner som representerer forskjellige stadier. For eksempel: utvikling, testing, iscenesettelse og produksjon.

Det er langt mer å dekke angående Spring Boot. Dette var en enkel introduksjon til basisprosjektets struktur og funksjonalitet. Etter hvert som flere emner dekkes, blir Spring Boot revist med ny kontekst. Foreløpig er det her vi lar det ligge.

## **Java Database Connectivity API**

I følge Oracle gir Java Database Connectivity (JDBC) API universell datatilgang fra programmeringsspråket Java.

Ved å bruke JDBC API kan du få tilgang til praktisk talt alle datakilder, fra relasjonsdatabaser til regneark og flate filer. JDBC-teknologi gir også en felles base som verktøy og alternative grensesnitt kan bygges på.

JDBC API består av to pakker:

- java.sql
- javax.sql

Du får automatisk begge pakkene når du laster ned Java Platform Standard Edition. For å bruke JDBC API med et bestemt databasebehandlingssystem, trenger du en JDBC-teknologibasert driver for å formidle mellom JDBC-teknologi og databasen.

Samspillet mellom Java og en database kan sees ved følgende illustrasjon:

![https://noroff-accelerate.gitlab.io/java/course-notes/_rework/module2/02_JDBCandDDD/resources/05_jdbcinteraction.png](https://noroff-accelerate.gitlab.io/java/course-notes/_rework/module2/02_JDBCandDDD/resources/05_jdbcinteraction.png)

Denne interaksjonen kommer i mange former via avhengigheter som Spring Data. Foreløpig vil vi gjøre den mest grunnleggende og enkle metoden: bruke JDBC direkte og ikke gjennom noen innpakninger.

For å gjøre dette trenger vi en JDBC-avhengighet for Postgres. Den kan finnes på mange måter, en veldig vanlig måte er å få den via [Maven Repository](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://mvnrepository.com/) , en populær avhengighetsressurs. Den spesielle avhengigheten vi trenger er: [PostgreSQL JDBC Driver](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://mvnrepository.com/artifact/org.postgresql/postgresql) , åpne siden for den nyeste, sentrale versjonen. Klikk deretter på formatet du ønsker. For dette prosjektet ønsker vi det i Gradle (kort) format, da vi jobber i gradle.

Kopier og lim den inn i `build.gradle`(vil være `pom.xml`for Maven) og "last inn Gradle-endringer". Dette gjøres ved å klikke på elefantikonet som dukker opp eller trykke "Ctrl-Shift-O". Dine avhengigheter i `build.gradle`skal se ut som følger:

```sql
dependencies {
    implementation 'org.springframework.boot:spring-boot-starter'    
		testImplementation 'org.springframework.boot:spring-boot-starter-test'    
		implementation 'org.postgresql:postgresql:42.3.3'
}
```

> MERK : Bare få den nyeste versjonen på utviklingstidspunktet, med mindre annet er oppgitt.
> 

## **Koble til Postgres fra Intellij**

For å opprette en forbindelse mellom en Java-applikasjon og en database, må en **tilkoblingsstreng** skrives og sendes til *JDBC-driveren*. Dette kan gjøres på mange forskjellige måter, for nå vil vi bruke en funksjon i Intellij Ultimate kalt *Datakilde og drivere*. Det kan sees ved å åpne fanen helt til høyre i utviklingsvinduet i Intellij (over Gradle/Maven-fanen). Ved å klikke på det utvides et vindu og det er en knapp (+-formet) for å legge til en ny datakilde. Når du klikker på PostgreSQL, vises et nytt vindu:

![https://noroff-accelerate.gitlab.io/java/course-notes/_rework/module2/02_JDBCandDDD/resources/06_addnewdatasource.png](https://noroff-accelerate.gitlab.io/java/course-notes/_rework/module2/02_JDBCandDDD/resources/06_addnewdatasource.png)

De neste trinnene er å fylle ut de obligatoriske feltene: vert, bruker, passord, database, URL og port. Noen av dem har standardverdier, disse kommer fra det som forventes i en Postgres-installasjon. Fyll inn all informasjonen vår for å koble til databasen vi konfigurert i forrige leksjon, kalt `postgrads`, konfigurasjonen er som følger:

![https://noroff-accelerate.gitlab.io/java/course-notes/_rework/module2/02_JDBCandDDD/resources/07_connectingtopostgrads.png](https://noroff-accelerate.gitlab.io/java/course-notes/_rework/module2/02_JDBCandDDD/resources/07_connectingtopostgrads.png)

Dette resulterer i at følgende vises i **Database** - fanen i Intellij:

![https://noroff-accelerate.gitlab.io/java/course-notes/_rework/module2/02_JDBCandDDD/resources/08_databasetab.png](https://noroff-accelerate.gitlab.io/java/course-notes/_rework/module2/02_JDBCandDDD/resources/08_databasetab.png)

> MERK : Tilkoblingsstrengen for denne databasen er:jdbc:postgresql://localhost:5432/postgrads
> 

Du kan utføre operasjoner direkte på databasen via denne fanen. Vi ønsker imidlertid å samhandle programmet med databasen. Dette krever at vi lager datatilgangsklasser som bruker JDBC API.

## **Bruke JDBC**

Det er mange klasser tilgjengelig gjennom bibliotekene `java.sql`og `javax.sql`, de viktigste i forhold til datatilgang er:

- `Connection`
- `DriverManager`
- `PreparedStatement`
- `ResultSet`

Detaljene for hver klasse dekkes etter hvert som eksempelet gjennomarbeides. Flyten er alltid den samme, men:

1. Opprett tilkobling til database
2. Skriv en klar setning for å innkapsle SQL-spørring
3. Håndter resultatet
4. Håndtere feil

> MERK: Programmet opprettet i denne delen er for å illustrere en barebones-konfigurasjon for å gi datatilgang, det mangler større viktige konsepter. Når du oppretter ditt eget program for utvikling, følg metoden vist i Ekstern konfigurasjon og Repositories og DDD - seksjoner. Videre er det et prøvelager med passende konfigurasjon som en ressurs på slutten av leksjonen.
> 

### **Forbindelse**

Det første trinnet er å etablere en tilkobling til databasen. Den nødvendige informasjonen for dette er:

- Tilkoblingsstreng
- Brukernavn
- Passord

Denne informasjonen har vi allerede da vi konfiguerte vår `Database`, og kan gjenbruke den:

- Tilkoblingsstreng:`jdbc:postgresql://localhost:5432/postgrads`
- Brukernavn:`postgres`
- Passord:`postgres`

> MERK : Tilkoblingsstrengformatet er veldig viktig. Den ølger mønsteret:jdbc:driver:source
> 

For faktisk å lette denne forbindelsen, må vi opprette en klasse. I dette tilfellet lager vi en `PostGradDAO`klasse.

> MERK : Suffikset DAO betyr Data Access Object . Navnekonvensjonene våre endres når vi går over til domenedrevet design. Ha dette i bakhodet.
> 

Kodebiten nedenfor viser den nåværende tilstanden til datatilgangsklassen vår:

```java
public class PostgradDAO {
    // Default values that can be overridden    private String url = "jdbc:postgresql://localhost:5432/postgrads";
    private String username = "postgres";
    private String password = "postgres";

    public PostgradDAO() {
    }

    public PostgradDAO(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

```

Dette lar oss bruke standardforbindelsen, men også gi overstyringer gjennom en overbelastet konstruktør. Det neste trinnet er å faktisk opprette en forbindelse:

```java
public void test() {
    try(Connection conn = DriverManager.getConnection(url, username,password);) {
        System.out.println("Connected to Postgres...");
    } catch (SQLException e) {
        e.printStackTrace();
    }
}
```

Denne metoden bruker **prøve-med-ressurser** for å opprette og avslutte tilkoblingen når utførelsen er fullført. Dette er for å følge anbefalingen om at "hver databaseinteraksjon gjøres med en ny tilkobling". Det bidrar til å forhindre ressursblokkeringer og lekkasjer når du arbeider med databaser programmatisk. Det er det samme som med **File I/O** .

Denne metoden gjør ikke så mye akkurat nå. Den kobler seg til databasen og skriver ut eventuelle feil som oppstår. Den neste underseksjonen beskriver den faktiske skrivingen av SQL og utføringen av den på databasen.

### **Utarbeidede uttalelser**

Forberedte uttalelser brukes til å lage paramateriserte spørringer. Dette betyr at enhver SQL-spørring vi gir som en forberedt uttalelse ikke er sårbar for angrep som SQL-injeksjon. For eksempel, hvis SQL-spørringen vår ble implementert med enkel strenginterpolering, kan en ondsinnet bruker sende parametere med andre instruksjoner.

For eksempel forventer du en filterverdi, som en `name`, men får i stedet `Bob; DROP TABLE user;`eller `Bob; UNION SELECT username, password FROM user;`som verdier. Hvis disse ikke er parametrisert, vil kommandoene etter `;`også bli utført.

> MERK : Alle SQL-setninger må paramatiseres. Alle databasehandlinger i rammeverk som Spring Data er som standard, men vi må parametrisere våre når vi bruker JDBC direkte.
> 

Å lage en forberedt setning er like enkelt som å lage et nytt `PreparedStatement`objekt og sende det til en spesifikk SQL-spørring, som vist nedenfor:

```java
public void getAllStudents() {
    String sql = "SELECT * FROM student";
    try(Connection conn = DriverManager.getConnection(url, username,password)) {
        // Write statement        PreparedStatement statement = conn.prepareStatement(sql);
        // Execute statement        ResultSet result = statement.executeQuery();
    } catch (SQLException e) {
        e.printStackTrace();
    }
}
```

`ResultSet`inneholder tabellen som returneres fra spørringen. Dette må konverteres til Java-objekter for å kunne brukes, dette gjøres i et senere avsnitt. Foreløpig er fokuset på den utarbeidede uttalelsen. Overnevnte setning har ingen parametere, og føles derfor litt overflødig å gå gjennom all denne behandlingen - men det anbefales likevel å sende spørringen via en forberedt erklæring.

Den neste kodebiten inneholder noen parametere:

```java
public void getSpecificStudent(int id) {
    String sql = "SELECT * FROM student WHERE stud_id = ?";
    try(Connection conn = DriverManager.getConnection(url, username,password)) {
        // Write statement        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, id);
        // Execute statement        ResultSet result = statement.executeQuery();
    } catch (SQLException e) {
        e.printStackTrace();
    }
}
```

Legg merke til parameteren `stud_id = ?`som skal erstattes av den beståtte `id`parameteren. Dette gjøres ved å kalle `setInt(parameterIndex, value)`metoden (det finnes setX-metoder for alle relevante datatyper).

> MERK : parameterIndexstarter kl 1.
> 

Inntil dette punktet er ingenting gjort med de returnerte verdiene. Neste trinn er å håndtere resultatet.

### **Håndtering av resultater**

Å skrive ut resultatsettet direkte i kode er en kjedelig og ineffektiv prosess. Det er langt bedre å konvertere (deserialisere) resultatsettet til en samling av objekter. Ved å ta eksemplene ovenfor som omhandler elever, kan det opprettes en klasse som representerer en student. I dette tilfellet vil vi bruke en spesiell type klasse kalt **Record** (introdusert i Java 14).

Poster er uforanderlige dataklasser som bare krever typen og navnet på feltene. Ideell for "vanlige databærere", klasser som inneholder data som ikke er ment å bli endret og bare de mest grunnleggende metodene som konstruktører og aksessere.

Kodebiten nedenfor illustrerer en `Student`post:

```java
public record Student(int id, String name, int supervisor) {
}
```

For å få samme effekt med en vanlig Java-klasse må vi lage:

- `privatefinal`
    
    ,
    
    felt for hver del av data
    
- `getter`
    
    for hvert felt
    
- `public constructor`
    
    med et tilsvarende argument for hvert felt
    
- `equals`
    
    metode som returnerer sann for objekter av samme klasse når alle feltene samsvarer
    
- `hashCode`
    
    metode som returnerer samme verdi når alle feltene samsvarer
    
- `toString`
    
    metode som inkluderer navnet på klassen og navnet på hvert felt og dens tilsvarende verdi
    

Som ville legge til dusinvis av linjer med kode, øke kodeoppblåstheten og tilsløre hensikten med klassen.

Det er veldig viktig å huske på at poster kun kan brukes til uforanderlige data. Det er noen datatilgangsscenarier der data må endres, og Records kan ikke brukes. Disse scenariene utforskes senere, og denne diskusjonen tas opp igjen når vi kommer dit.

> MERK : Det er alternative biblioteker som Lombok som hjelper til med å lette disse prosessene.
> 

Når vi går tilbake til eksemplet på hånden, er vår oppgave å skjule det `ResultSet`inn i `Student`datastrukturen vår. Følgende kodebit illustrerer denne prosessen:

```java
public List<Student> getAllStudents() {
    String sql = "SELECT * FROM student";
    List<Student> students = new ArrayList<>();
    try(Connection conn = DriverManager.getConnection(url, username,password)) {
        // Write statement        PreparedStatement statement = conn.prepareStatement(sql);
        // Execute statement        ResultSet result = statement.executeQuery();
        // Handle result        while(result.next()) {
            Student student = new Student(
                    result.getInt("stud_id"),
                    result.getString("stud_name"),
                    result.getInt("supervisor_id")
            );
            students.add(student);
        }
    } catch (SQLException e) {
        e.printStackTrace();
    }
    return students;
}
```

Her er returtypen endret slik at en samling studenter kan sendes ut av denne metoden. Det `result.next()`er å gå gjennom alle radene i `ResultSet`. Deretter trekkes verdiene ut med `getX`metoder og en ny `Student`postforekomst opprettes og deretter legges til returverdien.

> MERK : getInt("stud_id")kan også skrives med indeksen til kolonnen. Det er bare lettere å lese ved å bruke kolonnenavnene.
> 

Denne prosessen er nøyaktig den samme for alle søk. Hva med ikke-søk (innsettinger, oppdateringer eller slettinger)? Heldigvis er de ikke så forskjellige.

### **Utføre NonQueries**

En ikke-spørring utføres når vi ønsker å utføre DML-spørringer som; sette inn nye data, oppdatere eksisterende data eller slette eksisterende data. Dette betyr at det ikke returneres noe resultatsett, bare et heltall som forklarer hvor mange rader som er berørt. Kodebiten nedenfor illustrerer hvordan du setter inn en ny student:

```java
public int insert(Student student) {
    String sql = "INSERT INTO student VALUES (?,?,?)";
    int result = 0;
    try(Connection conn = DriverManager.getConnection(url, username,password)) {
        // Write statement        PreparedStatement statement = conn.prepareStatement(sql);
        statement.setInt(1, student.id());
        statement.setString(2,student.name());
        statement.setInt(3, student.supervisor());
        // Execute statement        result = statement.executeUpdate();
    } catch (SQLException e) {
        e.printStackTrace();
    }
    return result;
}
```

Den `executeUpdate()`utfører alle DML-spørringer. Alle andre DML-spørringer (oppdateringer og slettinger) følger samme mal.

Det neste trinnet herfra er å forbedre implementeringen vår når det gjelder design. De følgende avsnittene dekker hvordan du optimalt bruker Spring og Domain Driven Design.

## **Ekstern konfigurasjon**

Inntil dette punktet har vi ennå ikke brukt noen store Spring Boot-funksjoner bortsett fra prosjektoppretting. Et vanlig Java-prosjekt kan brukes til å gjenskape det som har blitt gjort i denne leksjonen så langt. Denne delen beskriver noen hovedfunksjoner i Spring Boot som kan brukes til å forbedre applikasjonen din.

En av de mest brukte aspektene ved Spring Boot er muligheten til å eksternalisere konfigurasjon. Hva betyr dette på vanlig engelsk?

Enhver konfigurasjon, applikasjon eller kjøretid kan gjøres utenfor forretningslogikken din. I eksemplet vist så langt, med postgraduates, kan databasetilkoblingen (url, brukernavn og passord) defineres separat for datatilgangen vår og deretter brukes ved å be om den fra Spring.

Spring gjør dette via en spesiell type konfigurasjon kalt en **Bean**. I Spring kalles objektene som utgjør ryggraden i søknaden din, og som administreres av Spring IoC-beholderen, bønner. I en mer formell definisjon:

*En bønne er et objekt som er instansiert, satt sammen og på annen måte administrert av en Spring IoC-beholder.*

Dette reiser det neste spørsmålet, hva er Spring IoC-beholderen?

Enkelt sagt er Inversion of Control (IoC) en prosess der et objekt definerer avhengighetene sine uten å opprette dem. Dette objektet delegerer jobben med å konstruere slike avhengigheter til en IoC-beholder.

> MERK : Dette burde høres kjent ut siden det er Springs implementering av Dependency Inversion Principle fra SOLID. Denne prosessen er også kjent som Dependency Injection.
> 

Hvordan kommer dette til uttrykk i våre applikasjoner? Det gjøres via merknader. I praksis vil du kanskje aldri se `@Bean`merknaden, siden den er erstattet noe av mer detaljerte merknader, som fortsatt representerer bønner under panseret.

Hvorfor vil vi i det hele tatt bruke bønner? Enkelt sagt kan det bli et mareritt å administrere tilstanden til mange objekter som kan være nestet i hverandre. Det er langt enklere å delegere det ansvaret til noe annet som vil fungere nøyaktig slik det er konfigurert.

Det er mange typer merknader som representerer ulike bønner i Spring Boot. Noen svært vanlige bønner å se i moderne Java er:

- `@Component`
- `@Configuration` med `@ComponentScan`
    
    

Per nå trenger du ikke å forstå detaljene i disse merknadene, de vil bli forklart etter behov. Det er også forskjellige andre bønnelignende merknader som vi vil bruke.

### **Bruke applikasjon konfigurasjon**

I praksis lar Spring Boot deg eksternalisere konfigurasjonen din slik at du kan jobbe med samme applikasjonskode i forskjellige miljøer.

Du kan bruke `properties`filer, `YAML`filer, `environment variables`og `command-line arguments`til å eksternalisere konfigurasjonen. Egenskapsverdier kan injiseres direkte i bønnene dine ved å bruke `@Value`kommentaren, få tilgang til gjennom Springs `Environment`abstraksjon, eller bindes til strukturerte objekter gjennom `@ConfigurationProperties`.

Foreløpig gjøres vår konfigurasjon via: `application.properties`og injisering med `@Value`. Det er den enkleste måten å bruke konfigurasjonen uten mye ekstra innsats.

> MERK : I fremtidige leksjoner blir dette emnet tatt opp flere ganger for å vise alternativene som er nevnt her i deres gjeldende kontekst.
> 
> 
> **DOKUMENTASJON** : En referanseveiledning om eksternalisert konfigurasjon fra Spring finner du [her](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://docs.spring.io/spring-boot/docs/2.0.6.RELEASE/reference/html/boot-features-external-config.html%23:~:text%3DSpring%2520Boot%2520lets%2520you%2520externalize,line%2520arguments%2520to%2520externalize%2520configuration.) .
> 

I et nytt prosjekt, `application.properties`er tom. Vi kan fylle den med mange egenskaper, både relatert til våren, og eventuelle tilpassede konfigurasjoner vi ønsker. For vårt formål ønsker vi å gi konfigurasjonen for databasetilkoblingen. Dette krevde lagring av url, brukernavn og passord. Dette kan gjøres med Spring-spesifikke søkeord, kodebiten nedenfor illustrerer denne konfigurasjonen:

```java
spring.datasource.url= jdbc:postgresql://localhost:5432/postgrads
spring.datasource.username= postgres
spring.datasource.password= postgres
```

> MERK : For utvikling og testing lokalt er det greit å avsløre passordet for en engangsdatabase, men det bør aldri bli eksponert for en offentlig utplassering. Dette tas opp igjen når avansert konfigurasjon dekkes senere i kurset, da det bruker noen verktøy vi ennå ikke har dekket.
> 

Det neste trinnet er å bruke `@Value`merknaden til å injisere disse i klassen vår. Kodebiten nedenfor viser denne prosessen:

```java
@Componentpublic class PostgradDAO {
    @Value("${spring.datasource.url}")
    private String url;
    @Value("${spring.datasource.username}")
    private String username;
    @Value("${spring.datasource.password}")
    private String password;
    // ...
```

> MERK : @Componenter en merknad som konfigurerer klassen slik at den kan oppdages av merknadsbasert konfigurasjon. En komponent er en type bønne .
> 

Vi fjernet også konstruktørene, siden vi bare trenger standardkonstruktøren nå, da verdiene vil bli injisert via IoC. Hva skjer hvis vi kjører applikasjonen? Den skal injisere verdiene og koble til databasen.

```java
java.sql.SQLException: The url cannot be null
```

Det ser ut til at det ikke fungerer. Ta en titt på hvordan vi kalte vår `PostGradDAO`:

```java
public static void main(String[] args) {
    SpringApplication.run(SpringTestApplication.class, args);
    PostgradDAO postgradDAO = new PostgradDAO();
    postgradDAO.test();
}
```

Aha, ved å instansiere `postgradDAO`oss selv, omgår vi IoC-prosessen, ettersom vi kontrollerer den. Hvordan kan vi snu dette for å la Spring styre klassen vår? Den enkleste tilnærmingen er å bruke en merknad kalt `@AutoWired`. Kodebiten nedenfor illustrerer bruken:

```java
@Autowired
PostgradDAO postgradDAO;
```

> MERK : @Autowiredmarkerer en konstruktør, felt, settermetode eller konfigurasjonsmetode som skal kobles automatisk av Springs avhengighetsinjeksjonsfasiliteter.
> 

Når applikasjonen kjøres, får vi en feil med linjen `postgradDAO.test()`om å kalle et ikke-statisk medlem fra en statisk kontekst. Men hvis vi definerer `static PostgradDAO postgradDAO;`vil det være `null`på tidspunktet for utførelse. Hva kan gjøres her?

Vi må ha en ikke-statisk metode som bruker klassen vår, dette kan gjøres på flere måter. Det enkle er å enten bruke `ApplicationRunner`eller `CommandLineRunner`-grensesnittene. De er grensesnitt som brukes til å indikere at en bønne skal `run`når den er inneholdt i en `SpringApplication`og brukes til å kjøre litt applikasjonslogikk ved oppstart.

> MERK : Begge er veldig like og skiller seg bare i hvordan kjøretidsargumentene sendes.
> 

Å endre klassen vår til å bli implementert `ApplicationRunner`er illustrert nedenfor:

```java
@SpringBootApplicationpublic class SpringTestApplication implements ApplicationRunner {
    @Autowired    PostgradDAO postgradDAO;

    public static void main(String[] args) {
        SpringApplication.run(SpringTestApplication.class, args);
    }

    @Override    public void run(ApplicationArguments args) throws Exception {
        postgradDAO.test();
    }
}
```

Selv om dette fungerer, er det flere endringer vi kan gjøre for å gjøre det bedre og mer tilpasset godt design.

### **Forbedring av gjennomføringen**

Når du konfigurerer applikasjonen som vist i forrige seksjon, vil du legge merke til at Intellij foreslår at "feltinjeksjon anbefales ikke". Dette er fordi feltinjeksjon ikke fremmer testbarhet. Klassene kan ikke lett spottes (falskes) på grunn av at falske verdier ikke kan overføres på konstruksjon.

Det andre aspektet er ikke en hard regel, men mer en generell tommelfingerregel. Du bør ikke ha applikasjonslogikk i inngangspunktet til en Spring Boot-applikasjon - `@SpringBootApplication`.

For å forbedre designet vårt bør vi lage en egen dedikert løperklasse, det vil si konstruktørinjisert.

```java
@Componentpublic class PgAppRunner implements ApplicationRunner {

    private final PostgradDAO postgradDAO;

    public PgAppRunner(PostgradDAO postgradDAO) {
        this.postgradDAO = postgradDAO;
    }

    @Override    public void run(ApplicationArguments args) throws Exception {
        postgradDAO.test();
    }
}
```

Mønsteret for avhengighetsinjeksjon følger universelt følgende struktur:

```java
private final PostgradDAO postgradDAO;

public PgAppRunner(PostgradDAO postgradDAO) {
    this.postgradDAO = postgradDAO;
}
```

Merknaden `@Autowired`er valgfri på dette tidspunktet, fordi Spring kjenner dette mønsteret og vil utføre konstruktørinjeksjon automatisk. Hvis du vil inkludere `@Autowired`, kan du plassere den over konstruktøren.

> MERK : Hvis du har flere konstruktører, må du merke den du vil være @Autowirednår den brukes som en bønne.
> 

## **Domenedrevet design (DDD)**

Inntil dette tidspunktet har datatilgangen blitt gjort veldig direkte og er ikke godt utformet. Den er funksjonell, men kan forbedres betydelig. Denne delen tar sikte på å omstrukturere datatilgangen vår til et mer allment akseptert format mens vi lærer det grunnleggende om domenedrevet design. Et viktig konsept fremover da Spring Data i stor grad bruker dette konseptet i sin konfigurasjon.

Det er viktig å merke seg at det er mange måter å utvikle programvare på. DDD er bare en av dem, den er svært anvendelig i utvikling av programvare for bedrifter, da denne tilnærmingen dreier seg om å lage produkter for å møte forretningsbehov.

Domenedrevet design kan defineres som:

*Prosessen med å designe programvare basert på modeller av det underliggende domenet.*

Denne uttalelsen har to stikkord; domene og modell. Du har kanskje hørt disse begrepene før, inkludert i dette kurset. Disse begrepene representerer konsepter som har eksistert lenge før definisjonen av DDD i 2003 av Eric Evans.

### **Domener og modeller**

> MERK : Følgende informasjon er hentet fra både Eric Evans og Martin Fowler.
> 

Domenet er **sfæren** av kunnskap og aktivitet som applikasjonslogikken dreier seg om. Domenet går under mange forskjellige navn: *domenelag* , *domenelogikk* eller *forretningslogikk* .

> MERK : Det vanligste synonymet for domene som programvareutviklere bruker, er forretningslogikk .
> 

Forretningslogikken til en *applikasjon* refererer til reglene på høyere nivå for hvordan forretningsobjekter samhandler med hverandre for å lage og endre modellerte data.

En **modell** fungerer som et allestedsnærværende språk for å hjelpe kommunikasjonen mellom programvareutviklere og domeneeksperter. Den fungerer også som det konseptuelle grunnlaget for utformingen av selve programvaren. Med andre ord:

*En domenemodell er en objektmodell av domenet som inkluderer både atferd og data.*

> MERK : Et allestedsnærværende språk bygger inn domeneterminologi i programvaresystemene vi bygger.
> 

Forretningslogikk kan være svært kompleks. Regler og logikk beskriver mange forskjellige tilfeller av atferd, og det er denne kompleksiteten objekter ble designet for å jobbe med. En domenemodell skaper et nett av sammenkoblede objekter, der hvert objekt representerer et meningsfullt individ, enten det er så stort som et selskap eller så lite som en enkelt linje på et bestillingsskjema.

> MERK : Domeneeksperter hjelper til med å definere domenemodellen.
> 

Eric Evans har noen kommentarer til bruken av domeneeksperter:

> Ved å bruke det modellbaserte språket gjennomgående og ikke være fornøyd før det flyter, nærmer vi oss en modell som er komplett og forståelig, bygd opp av enkle elementer som kombineres for å uttrykke komplekse ideer.
> 
> 
> Domeneeksperter bør protestere mot termer eller strukturer som er vanskelige eller utilstrekkelige til å formidle domeneforståelse; Utviklere bør se etter tvetydighet eller inkonsekvens som vil snuble design.
> 

Å gå gjennom denne prosessen garanterer at den endelige modellen nøyaktig gjenspeiler forretningsprosessene som trengs. Dette er det mest avgjørende stadiet i programvareutvikling siden du ikke kan bygge et hus på dårlig grunnlag.

### **Kontekster**

En kontekst er settingen der et ord eller utsagn som vises bestemmer betydningen. Utsagn om en modell kan bare forstås i en sammenheng.

Dette er utvidet med en **avgrenset kontekst**, som er en beskrivelse av en grense (typisk et delsystem, eller arbeidet til et spesifikt team) der en bestemt modell er definert og anvendelig.

Avgrensede kontekster spiller vanligvis inn når et større system designes, Martin Fowler gir et godt eksempel:

![https://martinfowler.com/bliki/images/boundedContext/sketch.png](https://martinfowler.com/bliki/images/boundedContext/sketch.png)

Her har vi flere objekter inne i en modell, de er atskilt av to avgrensede kontekster. Den ene handler om **salg** , mens den andre handler om **support** . Som man kan se her, er noen objekter delt mellom klasser og definerer hvordan de ulike kontekstene samhandler.

Avgrensede kontekster spiller inn for større applikasjoner. Skalaen vi jobber med for øyeblikket tilsier ikke å inkludere dette. Men når modellen din når et visst punkt, er det logisk å kartlegge disse ulike kontekstene.

> MERK : Vær forsiktig med dogmatiske tilnærminger. Du bør bare ta aspekter som gjelder dine nåværende behov. En lignende anbefaling er gitt angående TDD og Design Patterns tidligere i kurset. En dyktig utvikler vet hvilke verktøy de trenger og hvor mye de skal bruke.
> 

### **Implementeringskonsepter**

Domenemodellen er modifisert ved hjelp av flere andre konsepter. Noen av dem er gjeldende for vår nåværende bruk, og andre er bare gjeldende i andre scenarier.

En **Entity** er et objekt som har en distinkt identitet som går gjennom tid og ulike representasjoner. Disse klassene representerer aspektene ved domenemodellen. Dette er ikke aktuelt i vår nåværende kontekst da vi definerer modellstrukturen vår via SQL-skript eksternt. Når vi bruker Spring Data kommer *Entities* inn i bildet.

Et **verdiobjekt** er et uforanderlig objekt som har attributter, men ingen distinkt identitet. Dette er perfekt representert mine *records* i det nåværende scenariet. Verdiobjekter har mange andre navn, bortsett fra *Record*, kan de også *Data Transfer Objects* .

En **tjeneste** er en operasjon eller form for forretningslogikk som ikke naturlig passer inn i objektets område. Med andre ord, hvis en funksjonalitet må eksistere, men den ikke kan relateres til en enhet eller verdiobjekt, er det sannsynligvis en tjeneste. I vår nåværende sammenheng er ikke dette helt aktuelt. Når vi begynner å bruke Spring Data, trengs tjenester for å innkapsle kompleks forretningslogikk.

Et **repository** er en tjeneste som bruker et globalt grensesnitt for å gi tilgang til alle enheter og verdiobjekter. Metoder bør defineres for å tillate opprettelse, modifikasjon og sletting av objekter. Lagre finnes for å kapsle inn datatilgangen vår. Husk at det er formålet med denne delen.

> MERK : Repositories og tjenester brukes ofte sammen for å separere datatilgang og forretningslogikk. Spring Data oppfordrer til bruk av Repository-Service-mønsteret.
> 

DDD oppfordrer til bruk av **fabrikker** for å kapsle inn kompleks objektskaping. Dette er perfekt representert av *Spring Boots IoC* i forbindelse med de forskjellige *BeanFactories* som presenteres i rammeverket.

Til slutt legger DDD vekt på konseptet **kontinuerlig integrasjon**, som ber hele utviklingsteamet bruke ett delt kodelager og push-forplikter seg til det daglig (om ikke flere ganger om dagen). En automatisk prosess kjøres på slutten av arbeidsdagen som sjekker integriteten til hele kodebasen, kjører automatiserte enhetstester, regresjonstester og lignende, for raskt å oppdage eventuelle potensielle problemer som kan ha blitt introdusert i de siste commits.

> MERK : Kontinuerlig integrasjon er allerede dekket ved automatisering av enhetstester og skriving av testrapporter via GitLab-CI .
> 

### **Implementering av DDD**

Når du implementerer DDD må et spørsmål om kontekst stilles. Hvilke begreper er anvendelige i den aktuelle konteksten?

Vi trenger ganske enkelt å fasilitere modifikasjonen av ulike aspekter av en modell som representerer en postgraduate kontekst. Vi har allerede definert databasen vår eksternt, noe som betyr at vi ikke trenger Entities.

Alt vi trenger er:

- Verdiobjekter (domenemodeller/poster) for å innkapsle våre uforanderlige data.
- Repositories for å kapsle inn modifikasjonen av aspektene ved modellen.

Vi kan bruke noen Java-funksjoner for å programmere på en TØRR måte. Vi kan definere et generisk depot for å innkapsle CRUD:

```java
public interface CRUDRepository<T, U> {
    List<T> findAll();
    T findById(U id);
    int insert(T object);
    int update(T object);
    int delete(T object);
    int deleteById(U id);
}
```

Dette kan utvides med ethvert objektspesifikt depot. F.eks `StudentRepository`, `ProfessorRepository`, `ProjectRepository`, og `SubjectRepository`. Kodebiten nedenfor illustrerer den utvidede `StudentRepository`:

```java
public interface StudentRepository extends CRUDRepository<Student, Integer> {
    // Any extended methods can go here}
```

Vi definerte StudentRepository for å utvide CRUDRepository, men erstattet generiske T, U med det faktiske objektet og dets identifikatordatatype.

Kodebiten nedenfor viser vår `StudentRepositoryImpl`:

```java
@Repositorypublic class StudentRepositoryImpl implements StudentRepository {

    private final String url;
    private final String username;
    private final String password;

    public StudentRepositoryImpl( @Value("${spring.datasource.url}") String url, @Value("${spring.datasource.username}") String username, @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    @Override    public List<Student> findAll() {}

    @Override    public Student findById(Integer id) {}

    @Override    public int insert(Student object) {}

    @Override    public int update(Student object) {}

    @Override    public int delete(Student object) {}

    @Override    public int deleteById(Integer id) {}
}
```

> MERK : Implementeringen må merkes med @Repository. Repository er bare en spesiell type komponent som gir kontekst om formålet med klassen. Dette betyr at vårt depot kan injiseres og brukes med IoC.
> 

Nå kan vi endre vår PgAppRunner til å bruke Student-depotet:

```java
@Componentpublic class PgAppRunner implements ApplicationRunner {

    private final StudentRepository studentRepository;

    @Autowired    public PgAppRunner(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    @Override    public void run(ApplicationArguments args) throws Exception {
        studentRepository.test();
    }
}
```

Vi tilfredsstiller fullt ut alle aspektene ved DDD med implementeringen vår og gir konfigurasjon eksternt gjennom Spring Boots IoC-beholder og Dependency-injeksjon. Et diagram over forholdet mellom alle abstraksjonsnivåene kan sees nedenfor:

![https://noroff-accelerate.gitlab.io/java/course-notes/_rework/module2/02_JDBCandDDD/resources/09_finaluml.png](https://noroff-accelerate.gitlab.io/java/course-notes/_rework/module2/02_JDBCandDDD/resources/09_finaluml.png)

> KILDER: Hvis du er nysgjerrig på å lære mer om DDD, er [Domain-Driven Design: Tackling Complexity in the Heart of Software](https://www.amazon.com/Domain-Driven-Design-Tackling-Complexity-Software/dp/0321125215) av Eric Evans og [Implementing Domain-Driven Design](https://www.amazon.com/gp/product/0321834577/ref=as_li_tl?ie=UTF8&camp=1789&creative=9325&creativeASIN=0321834577&linkCode=as2&tag=martinfowlerc-20) av Vaughn Vernon, to fantastiske ressurser som er allment anbefalt å ha ved din side.
> 

---

Copyright 2022, Noroff Accelerate AS