# SQL med Postgres

# **SQL med Postgres**

Denne leksjonen introduserer konseptet vedvarende lagring gjennom et datalager. Den dekker ACID-prinsippene som alle leverandører av relasjonsdatabaser følger. Structured Query Language (SQL) er dekket for å få en forståelse av å manipulere data mellom et datalager og en Java-applikasjon gjennom forhåndsdefinerte opprette, lese, oppdatere og slette (CRUD) uttalelser.

SQL-kommandoer er dekket som er relatert til å sette opp et Postgres-databaseskjema. Disse inkluderer; lage en database, lage tabeller, lage begrensninger (nøkler) og sette opp relasjoner mellom tabeller.

Læringsmål:

Du vil kunne...

- **forklare** hva en relasjonsdatabase er.
- **demonstrere** bruken av SQL med Postgres
- **konstruer** en SQL-setning for å produsere det forventede resultatet, gitt en rapportforespørsel.
- **velg, kombiner,** og **organiser** data fra flere databasetabeller ved hjelp av passende SQL-kommandoer og parametere.

## **Databaseintroduksjon**

Databaser er den primære måten vi lagrer og henter data på:

- Databaser består av tabeller
- Tabeller har rader og kolonner
- Kolonner er attributtene til dataene dine
- Rader er unike forekomster av hvert sett med attributter
- Rader identifiseres med en nøkkel (primærnøkkel)

> MERK : Rader kalles ofte *poster*.
> 

Relasjonsdatabaser som navnet antyder har relasjoner (lenker) mellom tabeller. Attributtet i en tabell kan referere til en nøkkel i en annen tabell (fremmednøkler).

## **Relasjonsmodellering**

For ikke så lenge siden var det mye mer komplisert å holde styr på lagrede data enn det er i dag. Ta et eksempel på et universitet med administrasjonspersonell som administrerer doktorgradsstudenter og veiledere i et listeformat.

### **Problemet**

I dette scenariet legges hver nye elev inn i en liste via tekst. En illustrasjon av det er vist nedenfor:

```
# Student list

- Mrs, Rhonda Patricks, 25
- Mr, Serj Tankian, 24
- Mr, David Gilmour, 22
- Mrs, Tracy Chapman, 25
```

Administratorene holder også styr på veiledere, illustrert nedenfor:

```
# Professor list

- Prof, John Danaher, Astro Physics
- Prof, Bernardo Faria, Molecular Biology
- Prof, Steve Bennington, Tectonic Geography
```

Det neste logiske trinnet er å holde styr på hvilke professorer som veileder hvilke studenter. Dette krever en annen liste for å holde styr på forholdet. Dette er illustrert nedenfor:

```
# Student supervisor list

- Mrs, Rhonda Patricks, Prof, Steve Bennington
- Mr, Serj Tankian, Prof, Bernardo Faria
- Mr, David Gilmour, Prof, John Danaher
- Mrs, Tracy Chapman, Prof, Bernardo Faria
```

Før vi går videre, la oss ta et minutt til å se på hvilke problemer disse tre enkle listene kan forårsake. Det første og mest åpenbare problemet er at vi gjentar data:

- Studentdata finnes i studentlisten samt studentveilederlisten.
- Professordata er i professorlisten, studentveilederlisten, samt innenfor studentveilederlisten (Prof Bernardo Faria er duplisert).

> MERK : Stavefeil i en av disse tre listene resulterer i tap av integriteten til dataene. Flere repetisjoner fører til større sjanse for feil.
> 

Et annet stort problemområde er å endre informasjon. Hvis en student eller professor endrer navn, må vi sørge for at hver post og hver liste gjenspeiler denne endringen. Å holde styr på å endre informasjon uten hjelp er en veldig kjedelig og vanskelig oppgave.

> MERK : Dette problemet stammer hovedsakelig fra dupliserte data.
> 

Det siste problemet er når vi fjerner data. Ta et scenario der en professor forlater, vi sletter dem fra listen. De er imidlertid fortsatt til stede i studentveilederlisten. Den informasjonen har mistet sin integritet.

### **Løsningen**

Løsningen er å erstatte en dataoppføring med en referanse til disse dataene. På denne måten er det bare én versjon av et gitt datastykke som deretter refereres til ved behov. Dette konseptet brukes når vi lager relasjonsdatabaser. Ved å gjøre dette eliminerer vi duplisering av data, og vi trenger ikke å bekymre oss for data som har endret seg.

Når vi beskriver relasjonsmodellering er det noen begreper som brukes. De forstås best ved å se på en definisjon for relasjonsmodellering:

*Relasjonsmodellering definerer enheter og relasjoner mellom disse enhetene for å representere et systems informasjon.*

Enhetene nevnt ovenfor er ganske enkelt objektene som er representert i listene ovenfor, som vil bli oversatt til databasetabeller. Når vi snakker om relasjoner, beskriver vi hvordan ulike enheter samhandler. Det er tre typer relasjoner som kan eksistere mellom to enheter, disse diskuteres senere. For å erstatte en oppføring av data med en referanse, trenger vi en unik nøkkel for å identifisere hver post.

Ved å følge relasjonsdatabasedesign elimineres problemene knyttet til oppføring. For å illustrere dette, representerer figuren nedenfor en relasjonsmodell av postgraduate-systemet ovenfor.

![https://noroff-accelerate.gitlab.io/java/course-notes/_rework/module2/01_SQLwithPostgres/resources/01_prof-stud-rel.png](https://noroff-accelerate.gitlab.io/java/course-notes/_rework/module2/01_SQLwithPostgres/resources/01_prof-stud-rel.png)

Diagrammet ovenfor er et eksempel på et Entity Relationship Diagram (ERD). Det er en standard måte å illustrere mange aspekter ved programvare. Linjen med en `|`på professorsiden og `o<`på studentsiden representerer et forhold. I dette tilfellet illustrerer det at én professor kan ha null eller mange studenter. Dette bidrar til å formidle logikken bak systemet.

### **Hvordan brukes den**

Hvis informasjon er nødvendig fra databasen , defineres *typen* , *omfanget* og *kriteriene for den nødvendige informasjonen gjennom en spørring.* Databasen henter deretter informasjonen fra tabellene tilsvarende.

Enhver endring av dataene (innsetting, sletting og oppdatering) må gjøres ved å følge regler som håndheves av en relasjonsmodell. Dette forhindrer endringer som kan gjøre informasjonen upålitelig (kompromitterer dataintegriteten).

> MERK : Dette er kjent som databasen som forsterker referanseintegriteten.
> 

Verdien som unikt identifiserer en rad kalles en primærnøkkel (PK). Hvis en tabell inneholder primærnøkkelen til en annen tabell for å referere til den for mer detaljer, kalles den en fremmednøkkel (FK).

> MERK : Primærnøkler er vanligvis numeriske (heltall). Dette gjør det lettere å automatisk tilordne nye ID-er ved å øke den med 1. En ekstra faktor er at oppslag er raskere på mindre datatyper.
> 

Det er tre typer relasjoner som kan eksistere mellom to tabeller.

- En til en
- En til mange
- Mange til mange

Når det eksisterer en én til mange-relasjon, må det være en fremmednøkkelreferanse i mange-siden av forholdet (avhengig tabell). I dette tilfellet har `student`tabellen en `prof_id`verdi som refererer til `professor`tabellen.

> MERK : For en-til-én-relasjoner spiller tabellen som fremmednøkkelen er plassert ingen rolle og er opp til hva som gir mening for virksomheten.
> 

### **Bestemme design**

Det kreves praktisk erfaring for å forstå hvordan man modellerer et system til en relasjonsmodell. Forretningsreglene og funksjonskravene til et system vil bli brukt til å designe en relasjonsmodell for systemet som betjener virksomheten. Dette gjøres vanligvis ved å samle informasjon om forretningsinformasjonsprosessene og definere regler.

En prosess som bidrar til å skape en god database når data og forretningsregler er samlet er **normalisering** .

### **Normalisering**

Normalisering er prosessen med å organisere kolonnene og tabellene i en database for å minimere redundansen til data. Normalisering innebærer å dele store tabeller i mindre tabeller og definere relasjoner mellom dem.

De forskjellige typene normaliseringsformer er:

### **Første normalform (1NF)**

- Data lagres i tabeller med rader som kan identifiseres unikt med en primærnøkkel.
- Data i hver tabell lagres i individuelle kolonner i sin mest reduserte form.
- Det er ingen gjentakende grupper.

### **Andre normalform (2NF)**

- Alle reglene fra 1NF skal tilfredsstilles.
- Bare de dataene som er relatert til en tabells primærnøkkel, lagres i hver tabell.

2NF har som mål å skape relasjoner for å sikre at alle ikke-nøkkelfelt er avhengige av nøkkelfelt.

### **Tredje normalform (3NF)**

- Alle reglene fra 2NF skal tilfredsstilles.
- Det skal ikke være noen intra-tabellavhengigheter mellom kolonnene i hver tabell. Dette er kjent for å være **ikke-transitivt avhengig** av primærnøkkelen til tabellen. Dette betyr at alle kolonner i en tabell bare skal stole på primærnøkkelen og ingen annen kolonne.

Beregnede felt bryter også med 3NF da de er avhengige av ikke-nøkkelverdier, og det er grunnen til at beregnede verdier gjøres programmatisk.

> MERK : De fleste databaser bør være i tredje normalform, men det er andre former som går lenger (til 6NF).
> 
> 
> **RESSURSE** : Dokumentasjon fra Microsoft om [normalisering](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://docs.microsoft.com/en-US/office/troubleshoot/access/database-normalization-description) .
> 

### **Denormalisering**

De-normalisering er prosessen med å optimalisere leseytelsen til en database ved å legge til redundante data eller ved å gruppere data. Dette bidrar til å unngå komplekse sammenføyninger og mange andre komplekse operasjoner.

De-normalisering betyr ikke at normalisering ikke vil bli utført. Det er en optimaliseringsstrategi som finner sted etter normaliseringsprosessen. Det gjøres vanligvis med datalagring og ikke på operasjonelle databaser.

## **ACID-prinsipper**

ACID (atomisitet, konsistens, isolasjon, holdbarhet) er et sett med egenskaper for **databasetransaksjoner** som er ment å garantere datagyldighet til tross for feil, strømbrudd og andre uhell.

> MERK : En transaksjon er en logisk enhet av arbeid utført mot en database der alle trinn må utføres eller ingen.
> 
- *Atomisk*
    
    : Transaksjoner må være atomære, de må mislykkes eller lykkes som en enkelt enhet.
    
- *Konsistent*
    
    : Databasen må alltid være i konsistent tilstand. Det skal ikke være noen deltransaksjoner
    
- *Isolasjon*
    
    : Endringene som gjøres av en bruker skal bare være synlige for denne brukeren inntil transaksjonen er forpliktet.
    
- *Holdbarhet*
    
    : Når en transaksjon er forpliktet, skal den være permanent og kan ikke angres.
    

## **Postgres**

For å lage relasjonsmodeller trengs en databaseleverandør. Det er mange tilbydere som eksisterer med egne administrasjonstjenester. For dette kurset `Postgres`brukes, og administreres med `PgAdmin`og `Intellij`.

PostgreSQL (aka Postgres) er et objektrelasjonelt databasestyringssystem (ORDBMS) basert på POSTGRES, utviklet ved University of California i Berkeley Computer Science Department. POSTGRES var banebrytende for mange konsepter som først ble tilgjengelig i noen kommersielle databasesystemer mye senere.

PostgreSQL er en åpen kildekode-etterkommer av denne originale Berkeley-koden. Den støtter en stor del av SQL-standarden og tilbyr mange moderne funksjoner:

- ACID-prinsippet
- komplekse spørringer
- fremmednøkler
- transaksjonsintegritet
- lagrede prosedyrer
- og mer...

Postgres er fullstendig utvidbar av alle og kan brukes, modifiseres og distribueres av alle gratis til ethvert formål, enten det er privat, kommersielt eller akademisk.

### **Installerer Postgres og PgAdmin**

Installering av Postgres er rett frem takket være et ett-klikks installasjonsprogram.

1. Last ned og kjør Postgres 14-installasjonsprogrammet fra [nettstedet deres](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://www.postgresql.org/).
2. Oppgi et passord for en Postgres-administrator. Det anbefales å lage en enkel for utvikling, for eksempel `postgres`. Det betyr at legitimasjonen din vil være `postgres` og `postgres`.
3. La alt annet være standard. Porten din skal være `5432`.
4. Last ned og installer PgAdmin. Postgres-installasjonsprogrammet vil be deg om å gjøre dette. Nok en gang, installer alle med standardinnstillinger.
5. Ikke bekymre deg for Stack builder-verktøyet. Ignorer dette når det blir bedt om det.

Når installasjonen er fullført, bør du kunne starte PgAdmin og bli presentert med følgende skjermbilde:

![https://noroff-accelerate.gitlab.io/java/course-notes/_rework/module2/01_SQLwithPostgres/resources/02_pgadminstartup.png](https://noroff-accelerate.gitlab.io/java/course-notes/_rework/module2/01_SQLwithPostgres/resources/02_pgadminstartup.png)

Når du har skrevet inn passordet ditt, vil du kunne utvide `Servers`fanen og se `Postgres 14`serveren din kjøre. Følgende skjermbilde viser en utvidet `Postgres 14`server med databaser samt tilkoblingsegenskapene til serveren.

![https://noroff-accelerate.gitlab.io/java/course-notes/_rework/module2/01_SQLwithPostgres/resources/03_pgadmindatabases.png](https://noroff-accelerate.gitlab.io/java/course-notes/_rework/module2/01_SQLwithPostgres/resources/03_pgadmindatabases.png)

Det bør være en enkelt `postgres`database i en ny installasjon. Herfra kan du opprette en ny database og begynne å skrive SQL.

### **Postgres navnekonvensjoner**

Hver leverandør har sine egne forventede navnekonvensjoner basert på en rekke faktorer. Postgres, for eksempel, lagrer alle navn som små bokstaver (kalt nedbretting) med mindre "angitt". Dette betyr at tabell- og kolonnenavnene ikke kan være store. Individuelle ord er atskilt med et understrek.

For eksempel: `app_user`, `APP_USER`, `App_User`vil alle bli lagret som `app_user`.

I tillegg til dette bør tabellnavn være entall: `app_user`ikke `app_users`. Det er også en liste over reserverte ord for Postgres, en feil vil bli kastet hvis noen av de reserverte ordene brukes for identifikatorer.

> RESSURS : Finn en liste over stikkord for Postgres [her](https://www.postgresql.org/docs/14/sql-keywords-appendix.html) .
> 

## **Structured Query Language (SQL)**

Structured Query Language (SQL) er et standardisert programmeringsspråk som brukes til å administrere relasjonsdatabaser og utføre ulike operasjoner på dataene i dem.

SQL brukes til følgende:

- endre databasetabell og indeksstrukturer
- legge til, oppdatere og slette rader med data
- hente undergrupper av informasjon fra relasjonsdatabasestyringssystemer

SQL-spørringer og andre operasjoner har form av kommandoer skrevet som setninger og er aggregert i programmer som gjør det mulig for brukere å legge til, endre eller hente data fra databasetabeller.

Det er en SQL-standard som er implementert av leverandører, disse implementeringene av standard SQL avviker litt, og kalles **dialekter**. I dette kurset bruker vi Postgres-dialekten.

> MERK : Selv om dialektene er litt forskjellige, er de stort sett like.
> 

### **Typer forespørsler**

SQL-spørringer kan kategoriseres i Data Definition Language (DDL), Data Manipulation Language (DML) og Data Query Language (DQL).

- **DDL-**
    
    spørringer brukes til å lage relasjonsmodeller. For eksempel opprette eller slette tabeller og definere relasjoner.
    
- **DML-**
    
    spørringer brukes til å endre verdier inne i en tabell ved å oppdatere, sette inn eller slette rader.
    
- **DQL-**
    
    spørringer brukes til å hente informasjon fra databasen ved å velge bestemt informasjon.
    

> **DOKUMENTASJON** : [Referanse for DDL](https://www.postgresql.org/docs/14/ddl.html)
> 
> 
> **DOKUMENTASJON** : [Referanse for DML](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://www.postgresql.org/docs/14/dml.html)
> 
> **DOKUMENTASJON** : [Referanse for DQL](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://www.postgresql.org/docs/14/queries.html)
> 

Det er også andre typer som er mer på linje med administrering:

- Data Control Language ( **DCL** ) kontrollerer synligheten av data og håndhever databasesikkerhet i et databasemiljø med flere brukere. Det inkluderer operasjoner som å gi databasetilgang, sette privilegier for å lage tabeller osv. To typer DCL-kommandoer er GRANT og REVOKE.
- Transaction Control Language ( **TCL** ) kommandoer brukes til å administrere transaksjoner i databasen. f.eks COMMIT og ROLLBACK.

### **Datatyper**

Det finnes ulike datatyper i SQL-standarden for å innkapsle et bredt spekter av behov. Postgres støtter de fleste av dem og har noen generelle formål, samt muligheten til å lage tilpassede datatyper. Det er mange støttede datatyper, de vanligste er:

- `integer` eller `bigint` - hele tall
- `decimal` eller `numeric` - brukerspesifisert presisjon for desimaltall
- `serial` eller `bigserial` - autoinkrementerende heltall
- `money` - valutabeløp
- `varchar(n)` - Tekst med variabel lengde med grense
- `char(n)` - tekst med fast lengde
- `text` - variabel tekst med ubegrenset lengde
- `boolean` - representerer sanne, usanne eller ukjente verdier
- `timestamp` - både dato og klokkeslett (ingen tidssone)
- `date` eller `time` - dato eller klokkeslett alene
- `point` - (x,y) koordinater

> DOKUMENTASJON : En fullstendig liste over Postgres-støttede datatyper finner du [her](https://www.postgresql.org/docs/14/datatype.html).
> 

### **Begrensninger**

Datatyper er en måte å begrense typen data som kan lagres i en tabell. For mange applikasjoner er imidlertid ikke begrensningen de gir nok. For eksempel bør en kolonne som inneholder en produktpris sannsynligvis bare akseptere positive verdier. Det er imidlertid ingen standard datatype som bare aksepterer positive tall.

Et annet problem er at du kanskje vil begrense kolonnedata med hensyn til andre kolonner eller rader. For eksempel, i en tabell som inneholder produktinformasjon, skal det bare være én rad for hvert produktnummer.

For det formål lar SQL deg definere begrensninger på kolonner og tabeller. Begrensninger gir deg så mye kontroll over dataene i tabellene dine som du ønsker. Hvis en bruker forsøker å lagre data i en kolonne som vil bryte en begrensning, oppstår det en feil. Dette gjelder selv om verdien kom fra standardverdidefinisjonen.

Begrensninger er plassert *etter* kolonnedefinisjoner i DDL-spørringer. Vanlige begrensninger er:

- `NOT NULL` - det må være en verdi for denne kolonnen
- `CHECK` - bruker logikk på en kolonne (f.eks. > 0)
- `UNIQUE` - sikrer at kolonnen er unik blant andre rader. Dette oppretter IKKE en primærnøkkel da den fortsatt kan være *null*
- `PRIMARY KEY` - setter kolonnen som en primærnøkkel. Den samme effekten kan oppnås med `UNIQUE NOT NULL`
- `REFERENCES table_name` - oppretter en fremmednøkkel til en spesifisert `table` på primærnøkkelen. En annen kolonne kan settes som fremmednøkkel ved å legge til `(column_name)`

Begrensninger kan gis aliaser for å forbedre lesbarheten ved å legge til `CONSTRAINT constraint_name`før begrensningen er definert. Begrensninger kan også påvirke flere kolonner, for eksempel `PRIMARY KEY (column_1, column_2)`oppretter en sammensatt primærnøkkel ved å bruke de to oppførte kolonnene.

> **DOKUMENTASJON** : En referanseguide om begrensninger i Postgres finner du [her](https://www.postgresql.org/docs/14/ddl-constraints.html).
> 

## **Bruker SQL**

For å illustrere SQL i aksjon opprettes en eksempeldatabase. Følgende underavsnitt beskriver trinnene som er utført for å lage en komplett database basert på et scenario.

### **Scenario og planlegging**

Dette er en utvidelse av postgraduate-systemet beskrevet i starten av timen. Det involverer `students`, `professors`, `subjects`, og `research projects`.

Studentene er i sentrum av systemet. De er kandidater som i tillegg til å skrive et forskningsprosjekt, må ta flere timer for å fullføre studiene. Forretningslogikken er skissert som følgende:

- En student veiledes av en enkelt professor. En professor kan veilede mange studenter.
- En student har ett enkelt forskningsprosjekt. Et forskningsprosjekt gjennomføres av en enkelt student.
- En student kan ta mange fag. Et emne kan tas av mange elever.
- En professor kan undervise i mange fag. Et emne undervises av en enkelt professor.

Dette informerer oss om hvordan vi konstruerer databaseskjemaet. En god idé er å lage diagrammer for å visualisere relasjonene. En vanlig tilnærming er å lage en `conceptual ERD`. En `concptual ERD`er et enkelt diagram som viser tabeller og sammenhenger, det er ingen felt i dette diagrammet. Det hjelper å få en følelse av logikken. Det neste trinnet ville være å legge til detaljer, dette kalles en `physical ERD`og inneholder alle de forventede feltene, deres datatyper og deres begrensninger. Diagrammet nedenfor illustrerer den **konseptuelle modellen** for databasen:

![https://noroff-accelerate.gitlab.io/java/course-notes/_rework/module2/01_SQLwithPostgres/resources/04_concptualerd.png](https://noroff-accelerate.gitlab.io/java/course-notes/_rework/module2/01_SQLwithPostgres/resources/04_concptualerd.png)

Foreløpig har vi gjort alle relasjoner obligatoriske i diagrammet, dette er noe som mest sannsynlig vil endre seg når vi begynner å fylle ut databasen og revurdere designet vårt. Diagrammet nedenfor illustrerer den **fysiske modellen** til databasen:

![https://noroff-accelerate.gitlab.io/java/course-notes/_rework/module2/01_SQLwithPostgres/resources/05_physicalerd.png](https://noroff-accelerate.gitlab.io/java/course-notes/_rework/module2/01_SQLwithPostgres/resources/05_physicalerd.png)

Bortsett fra inkluderingen av felt, er det en annen betydelig forskjell mellom de to modellene. En fysisk modell har ikke et mange-til-mange forhold. Fysiske modeller representerer virkeligheten, og du kan ikke ha mange-til-mange i virkeligheten da dette ikke logisk fungerer innenfor rammen av relasjonsdatabaser. Dette betyr at en ny tabell må konfigureres for å håndtere forholdet. Denne tabellen kalles en **koblingstabell** og kan sees på diagrammet som `student_subject`.

Koblingstabellen her eksisterer for å holde styr på kombinasjoner av `student`og `subject`. Ved å spørre om dette vil du fortelle deg hvilke elever som går i hvilke klasser, med muligheten til å få informasjon om begge tabellene gjennom *sammenføyninger* .

En annen ting å merke seg er at enkelhet er foretrukket her for å fokusere på kjerneprinsippene bak datamodellering. I virkeligheten vil et system som dette måtte holde styr på langt mer informasjon om hver enhet (student, professor, emne og prosjekt). Alle relasjoner håndheves også, noe som betyr at *FK* ikke kan være `null`.

### **Database opprettelse**

Denne underdelen beskriver trinnene for å opprette den ovennevnte fysiske ERD.

### **Opprette selve databasen**

Det første trinnet i å lage skjemaet er å lage selve databasen. Dette kan gjøres gjennom PgAdmin ved å høyreklikke på `Databases`> `Create`> `Database...`.

En lignende effekt kan oppnås ved å åpne `Query Tool`i PgAdmin på en eksisterende database. Dette kan gjøres ved enten å klikke på det dedikerte ikonet, eller høyreklikke på databasen og velge `Query Tool`. Skjermbildet nedenfor illustrerer spørringsvinduet som vises for standarddatabasen `postgres`.

![https://noroff-accelerate.gitlab.io/java/course-notes/_rework/module2/01_SQLwithPostgres/resources/06_querywindow.png](https://noroff-accelerate.gitlab.io/java/course-notes/_rework/module2/01_SQLwithPostgres/resources/06_querywindow.png)

Vi kan deretter skrive følgende spørring for å lage databasen:

```sql
CREATE DATABASE postgrads;
```

Det er viktig å huske at enhver spørring på postgres må kjøres på en spesifikk database, i motsetning til andre SQL-leverandører som tillater deg `USE DATABASE`, er Postgres scoped til den gjeldende databasen.

Dette betyr at vi må åpne et spørringsvindu for vår nyopprettede `postgrads`database.

> MERK : Når du samhandler med postgres gjennom CLI ved å bruke `psql` kommandoen, må du koble til en bestemt database, ikke server, før du kan begynne å kjøre skript.
> 

### **Oppretting av tabell**

Den første tabellen vi skriver er `professor`tabellen. Grunnen til dette er at det er en av tabellene som ikke har fremmednøkler. Vi kunne ikke opprette disse ennå, da de krever at visse felt allerede eksisterer.

Når vi ser på vår ERD, vet vi hvilke felt vi trenger å opprette for denne tabellen. Følgende SQL-spørring vil generere `professor`tabellen i henhold til ERD ovenfor:

```sql
/* Ensures we dont try re-create the table */
DROP TABLE IF EXISTS professor;

CREATE TABLE professor (
    prof_id int PRIMARY KEY,
    prof_name varchar(50) NOT NULL,
    field varchar(50)
);
```

Den neste tabellen som skal opprettes er `research_project`, en annen tabell uten fremmednøkler. Følgende SQL-spørring vil generere `research_project`tabellen i henhold til vår ERD:

```sql
/* Ensures we dont try re-create the table */
DROP TABLE IF EXISTS research_project;

CREATE TABLE research_project (
    proj_id serial PRIMARY KEY,
    title varchar(100) NOT NULL
);
```

Legg merke til at noe er gjort annerledes her. Datatypen til primærnøklen er ikke den vanlige, `int`men snarere `serial`. Husk, `serial`er et automatisk inkrementert heltall, noe som betyr at vi ikke trenger å bekymre oss for å levere en unik verdi etter hvert som en genereres. Starter vanligvis på 1 og øker med 1.

Den neste tabellen å lage er `student`tabellen. Denne har referanser til både `professor`og `research_project`. Følgende SQL-spørring vil generere `student`tabellen i henhold til vår ERD:

```sql
/* Ensures we dont try re-create the table */
DROP TABLE IF EXISTS student;

CREATE TABLE student (
    stud_id int PRIMARY KEY,
    stud_name varchar(50) NOT NULL,
    project_id int NOT NULL REFERENCES research_project,
    supervisor_id int NOT NULL REFERENCES professor
);
```

I dette skriptet laget vi tre begrensninger. Den første er primærnøkkelen, deretter to fremmednøkler.

`FK-project_id`refererer til `proj_id`feltet i `research_project`tabellen. Vi trenger ikke spesifisere feltet, hvis ingen er spesifisert tar det primærnøkkelen - som er akkurat det vi ønsker. `FK-supervisor_id`er konfigurert på samme måte.

Den neste tabellen å lage er `subject`tabellen. Denne tabellen refererer til `professor`, og `student`gjennom en koblingstabell. Følgende SQL-spørring vil generere `subject`tabellen i henhold til vår ERD:

```sql
/* Ensures we dont try re-create the table */
DROP TABLE IF EXISTS subject;

CREATE TABLE subject (
    sub_id int PRIMARY KEY,
    sub_code varchar(10) NOT NULL,
    sub_title varchar(100) NOT NULL,
    lecturer_id int NOT NULL REFERENCES professor
);
```

Den siste tabellen er `student_subject`koblingstabellen. Dette eksisterer for ganske enkelt å imøtekomme mange-til-mange-forholdet mellom `student`og `subject`. Følgende SQL-spørring vil generere `student_subject`tabellen i henhold til vår ERD:

```sql
/* Ensures we dont try re-create the table */
DROP TABLE IF EXISTS student_subject;

CREATE TABLE student_subject (
    stud_id int REFERENCES student,
    sub_id int REFERENCES subject,
    PRIMARY KEY (stud_id, sub_id)
);
```

Syntaksen for denne tabellen er litt annerledes enn det som er sett før. Det er en primærnøkkel som består av to felt, dette kalles en **sammensatt primærnøkkel** og er ikke begrenset til to felt. Motivasjonen for dette er veldig enkel; en kombinasjon av elev og fag skal være unikt i vårt system. Et felt kan være involvert i flere begrensninger, sett her med begge primærnøklene som også er fremmednøkler.

Per nå representerer databasen vår plan i henhold til ERD, neste trinn er å sette inn data og se om det samsvarer med forventningene våre.

### **Setter inn data**

Til å begynne med er noen professorer lagt til databasen:

```sql
/* We need to know the order of the columns */
INSERT INTO professor VALUES (1, 'Ola Nordmann', 'Artificial Intelligence');
/* Columns are specified */
INSERT INTO professor (prof_id, prof_name, field) VALUES (2, 'Kari Nordmann', 'Security Governance');
```

Legg merke til hvordan det er to metoder for å sette inn data. Den kortere versjonen er avhengig av at du kjenner rekkefølgen på kolonnene i tabellen.

Den neste tabellen for å sette inn verdier ville logisk sett være studenter. Har imidlertid `student`en fk til `research_project`, dette hindrer oss i å sette inn data da fk vil referere til noe som ikke eksisterer. På dette tidspunktet står vi overfor et valg:

- Sett inn i `research_project`først, eller
- **Endre** tabellene for å flytte fk til `research_project`

Til å begynne med kan det første alternativet føles riktig, men dette er en viktig avgjørelse som avhenger av forretningslogikken til ønsket system. Plasseringen av fk beskriver **avhengighet** . For en-til-mange er fk tvunget til å være i en bestemt tabell, men med en-til-en kan fk være på begge sider.

Tabellen med fk sies å være **avhengig** av tabellen den refererer til. La oss se på den nåværende avhengigheten til systemet vårt, vi sier "student er avhengig av forskningsprosjekt". En annen måte å se det på, er gjennom eierskap. Student eier forholdet, da det er en student som HAR-Et prosjekt.

Vi kan endre dette ved å endre de to tabellene for å flytte fk til `research_project`. Dette gjøres bare som et eksempel for å lette seeding-prosessen, i virkeligheten avhenger dette av forretningslogikken din:

```sql
/* Remove the fk from student */
ALTER TABLE student DROP COLUMN project_id;
/* Add a fk to research_project */
ALTER TABLE research_project ADD COLUMN stud_id int REFERENCES student;
```

Den samme effekten kan oppnås ved å droppe og gjenskape tabellene individuelt, men denne metoden er mer effektiv.

Nå kan vi legge til nye studenter uten å ha eksisterende prosjekter:

```sql
INSERT INTO student (stud_id, stud_name, supervisor_id) VALUES (1, 'Bob Smith', 1);
INSERT INTO student VALUES (2, 'Alice Smith', 2);
```

La oss legge til noen forskningsprosjekter for disse nye studentene:

```sql
INSERT INTO research_project (title, stud_id) VALUES ('Exploring the Applications of Deep Learning to traffic control', 1);
INSERT INTO research_project (title, stud_id) VALUES ('Applications of Security Governance in DevOps', 2);
```

Legg merke til hvordan primærnøkkelen ikke ble oppgitt, husk at dette er et *automatisk inkrementert* felt. Når du setter inn med autoinkrementerte verdier, må kolonnene spesifiseres, ellers ville den prøve å sette `title`inn i `proj_id`.

Deretter er noen emner satt inn:

```sql
INSERT INTO subject (sub_id, sub_code, sub_title, lecturer_id) VALUES (1, 'ONT401', 'Advanced programming', 1);
INSERT INTO subject VALUES (2, 'DL101', 'Applications of Deep Learning', 1);
INSERT INTO subject VALUES (3, 'SEC401', 'Security Governance', 2);
INSERT INTO subject VALUES (4, 'DS401', 'Securing applications', 2);
```

På dette stadiet kan elever og fag kobles sammen, dette kan gjøres ved å sette inn poster i `student_subject`tabellen:

```sql
INSERT INTO student_subject (stud_id, sub_id) VALUES (1,1);
INSERT INTO student_subject (stud_id, sub_id) VALUES (1,2);
INSERT INTO student_subject (stud_id, sub_id) VALUES (1,4);
INSERT INTO student_subject (stud_id, sub_id) VALUES (2,3);
INSERT INTO student_subject (stud_id, sub_id) VALUES (2,4);
```

### **Enkle spørsmål**

Nå som det er noen data i databasen, er det et godt tidspunkt å spørre dem ut og se om alt har gått bra. For å gjøre dette skrives spørsmål. Grunnlaget for spørringene er `SELECT`setningen og `FROM` for å detaljere hvor de valgte verdiene er fra. For å begynne med, la oss bare få alle verdiene for professorer:

```sql
SELECT * FROM professor;
```

> MERK : Jokertegnet * betyr  "velg alle felt fra tabellen".
> 

Dette resulterer i følgende tabell:

[Untitled](https://www.notion.so/375735534595486483f317be7882238c)

Deretter sjekker vi om alle elevene er det vi forventer:

```sql
SELECT * FROM student;
```

Som resulterer i:

[Untitled](https://www.notion.so/1c880fd603e548b5a1433150613e2d73)

Hva om vi ønsket å vise veilederens navn i stedet for id:

```sql
SELECT stud_id AS id, stud_name AS name, prof_name AS supervisor
FROM student 
INNER JOIN professor 
ON supervisor_id = prof_id;
```

Som resulterer i følgende returnerte dat:

[Untitled](https://www.notion.so/b4c48d2bd3f94ca996945bc203ded1e3)

Det er et par ting som er nye her. For det første `AS`fungerer nøkkelordet et alias for å erstatte kolonnenavnet med vårt eget; forbedrer lesbarheten til rapportene.

Det neste aspektet å diskutere er `INNER JOIN`, dette nøkkelordet lar verdier fra flere tabeller spørres ved å "føye sammen" dataene basert på `ON`noen kriterier. Denne sammenføyningen brukes til å spørre `prof_name`fra `professor`tabellen ved å koble sammen *FK* og *PK* . Det finnes ulike typer sammenføyninger, for nå returnerer bruken av *indre sammenføyninger* poster som har samsvarende verdier i begge tabellene.

> MERK : De andre typene sammenføyninger er forklart senere i leksjonen.
> 

### **Oppdaterer data**

Endring av eksisterende data er et kjerneaspekt av dataadministrasjon, dette kan være å rette opp feil i datainntak, eller dataendring gjennom forretningsprosesser.

For dette eksemplet er emnetittelen endret. Den aktuelle posten har opprinnelig følgende verdier:

[Untitled](https://www.notion.so/593f16626cf043849e214b509983c26e)

For å endre posten kan følgende spørring kjøres:

```sql
UPDATE subject 
SET sub_title = 'Deep Learning applications' 
WHERE sub_id = 2;
```

Dette resulterer i at posten har verdien av:

[Untitled](https://www.notion.so/fea19dedd00047cfad328d9ec115f6fe)

> MERK : Flere kolonner kan oppdateres samtidig ved å avgrense verdiene med komma.
> 

### **Sletter data**

Sletting av data er noe som høres enkelt ut, men kan ofte by på mange problemer. Disse problemene er rundt referanseintegritet. Det betyr at du ikke kan slette en post som er referert til i en annen tabell.

For eksempel har vi professor med id `1`, de underviser i fag `1`og `2`, og de veileder student `1`. Hvis følgende SQL kjøres:

```sql
DELETE FROM professor 
WHERE prof_id = 1;
```

Vi får følgende feilmelding:

```sql
ERROR:  update or delete on table "professor" violates foreign key constraint "student_supervisor_id_fkey" on table "student"
DETAIL:  Key (prof_id)=(1) is still referenced from table "student".
SQL state: 23503
```

For å løse dette har vi noen alternativer:

- Ikke tillat sletting av et referert produkt
- Slett også de relaterte dataene
- Et annet alternativ

Dette styres ved å sette en kontraint knyttet til `ON DELETE`, det er tre hovedverdier:

- `RESTRICT`
- `CASCADE`
- `NO ACTION`

Begrensning (`RESTRICT`) og gjennomgripende (`CASCADE`) sletting er de to vanligste alternativene. `RESTRICT`forhindrer sletting av en referert rad. `NO ACTION`betyr at hvis noen referanserader fortsatt eksisterer når begrensningen er sjekket, oppstår en feil; dette er standard oppførsel hvis du ikke spesifiserer noe.

`CASCADE`spesifiserer at når en referert rad slettes, skal rad(er) som refererer til den også slettes automatisk. Det er to andre alternativer: `SET NULL`og `SET DEFAULT`. Disse fører til at referansekolonnen(e) i referanseraden(e) blir satt til henholdsvis null eller deres standardverdier når den refererte raden slettes.

Normalt vil du i de fleste tilfeller utføre en form for restriksjon fordi du ikke ønsker å fjerne viktig forretningsinformasjon ved et uhell.

Det ene området der kaskader vanligvis spiller inn, er når det er mange-til-mange forhold. I vårt tilfelle studenter og fag. Som standard, hvis vi prøver å slette et emne som har studenter i denne feilen:

```sql
ERROR:  update or delete on table "subject" violates foreign key constraint "student_subject_sub_id_fkey" on table "student_subject"
DETAIL:  Key (sub_id)=(1) is still referenced from table "student_subject".
SQL state: 23503
```

Hvis vi ønsker at alle de relaterte studentene skal slettes i koblingstabellen (ikke de faktiske studentene selv), kan vi sette `ON DELETE CASCADE`. For å gjøre dette, må vi droppe FK-begrensningen, og deretter legge den til på nytt med de nye parameterne:

```sql
ALTER TABLE student_subject
DROP CONSTRAINT student_subject_sub_id_fkey,
ADD CONSTRAINT student_subject_sub_id_fkey
FOREIGN KEY (sub_id) REFERENCES subject
ON DELETE CASCADE;
```

Nok en gang kan dette replikeres med `DROP`og så `CREATE`for tabellen, så `ON DELETE CASCADE`legges det bare til fra begynnelsen. Vi må imidlertid legge til dataene på nytt; denne metoden sikrer at dataene forblir lagret når du endrer begrensninger. Vi skal nå kunne SLETTE et emne, og få `student_subject`fjernet alle postene for det emnet i tabellen også. Før noen emner opprettes, har `student_subject`tabellen følgende verdier:

[Untitled](https://www.notion.so/e7869e39001642ee9e4ed7fff0a3c4d5)

Når vi kjører følgende kommando:

```sql
DELETE FROM subject
WHERE sub_id = 1;
```

Emnetabellen har nå følgende verdier (merk at den første posten er slettet):

[Untitled](https://www.notion.so/f97434f34d2e48f7b855d978521589ea)

Ser på `student_subject`tabellen:

[Untitled](https://www.notion.so/3d4654b55e2c4315aca2f6434e16f902)

Legg merke til hvordan posten knyttet til `sub_id = 1`også er slettet, selv om vi ikke har slettet denne selv.

Hvilke begrensninger som bør eller ikke bør kaskade er fullstendig knyttet til forretningslogikk og er en avgjørelse som ikke bør tas lett på, siden en feil kaskadekonfigurasjon potensielt kan slette verdifulle data.

### **Avanserte søk**

Denne underseksjonen fokuserer på funksjoner som tillater mutasjon av data utover den enkle "les hva som er inne"-tilnærmingen.

### **Aggregasjon**

Setningen `GROUP BY`brukes til å gruppere de radene i en tabell som har de samme verdiene i alle kolonnene som er oppført. Rekkefølgen kolonnene er oppført i spiller ingen rolle. Effekten er å kombinere hvert sett med rader med felles verdier til én grupperad som representerer alle rader i gruppen. Dette gjøres for å eliminere redundans i utdataene og/eller beregningsaggregatene som gjelder for disse gruppene.

> MERK : Dette er ofte kombinert med en eller annen **aggregeringsfunksjon** .
> 

La oss for eksempel lage en spørring som returnerer en liste over studenter og hvor mange fag de tar:

```sql
SELECT stud_name AS student, count(sub_id) AS subjects
FROM student AS s
INNER JOIN student_subject AS sj
ON s.stud_id = sj.stud_id
GROUP BY stud_name;
```

Dette resulterer i følgende:

> MERK : Gruppering uten aggregerte uttrykk beregner effektivt settet med distinkte verdier i en kolonne. Dette kan også oppnås ved å bruke `DISTINCT` setningen.
> 

Det finnes andre aggregerte funksjoner i SQL i tillegg til `count`:

- `avg`
- `sum`
- `min`
- `max`

Disse kan brukes til å lage mer komplekse spørringer som gir mer verdi for systemet ditt.

### **Betinget matching**

Operatoren  `LIKE` brukes til å matche tekstverdier mot et mønster ved hjelp av jokertegn. Den følger en `WHERE` setning og har en bestemt mal:

`WHERE column LIKE '%value%'`

`%` representerer et hvilket som helst antall tegn:

- Hvis `%`er i starten, er det et hvilket som helst antall tegn før.
- Hvis `%`er på slutten, er det et hvilket som helst antall tegn etter.
- Hvis det er `%`på begynnelsen og slutten, representerer det alle tegn før eller etter.

For eksempel følgende spørring:

```sql
SELECT * FROM student
WHERE stud_name LIKE '%it%';
```

Vil resultere i:

[Untitled](https://www.notion.so/82b35101318949fabd962b84ec81a012)

Dette er fordi `Smith`inneholder `it`. Men hvis vi skrev:

```sql
SELECT * FROM student
WHERE stud_name LIKE 'it';
```

Resultatsettet ville være tomt, siden det ikke er noen tilfeller av `student`hvor `stud_name`nøyaktig samsvarer `it`.

### **Filtrering og bestilling**

Rader kan elimineres med `HAVING` setningen. For eksempel, hvis vi la til følgende etter `GROUP BY`:

```sql
HAVING count(sub_id) > 2;
```

For aggregeringsspørringen vil resultatsettet vårt være tomt, siden det ikke er noen studenter som har mer enn to emner i databasen vår.

Etter at en spørring har produsert en utdatatabell (etter at valglisten er behandlet), kan den eventuelt sorteres. Hvis sortering ikke er valgt, vil radene bli returnert i uspesifisert rekkefølge. Setningen `ORDER BY`spesifiserer sorteringsrekkefølgen:

```sql
ORDER BY column_name [ASC|DESC];
```

`LIMIT`og `OFFSET`lar deg hente bare en del av radene som genereres av resten av spørringen:

```sql
LIMIT { number | ALL } ] [ OFFSET number ]
```

En fullstendig mal for sortering og filtrering er som følger:

```sql
SELECT selected_columns
FROM table[ ORDER BY ... ]
[ LIMIT { number | ALL } ] [ OFFSET number ]
```

### Joins

Her er de forskjellige typene JOINs i SQL:

- (INNER) JOIN: Returnerer poster som har samsvarende verdier i begge tabellene
- LEFT (OUTER) JOIN: Returnerer alle poster fra venstre tabell, og de samsvarende postene fra høyre tabell
- HØYRE (OUTER) JOIN: Returnerer alle poster fra den høyre tabellen, og de samsvarende postene fra den venstre tabellen
- FULL (OUTER) JOIN: Returnerer alle poster når det er samsvar i enten venstre eller høyre tabell

![https://www.devtodev.com/upload/images/sql5_1.png](https://www.devtodev.com/upload/images/sql5_1.png)

Bildekreditt : [devtodev](https://translate.google.com/website?sl=en&tl=no&hl=no&client=webapp&u=https://www.devtodev.com/education/articles/en/414/sql-for-beginners-joins-and-funnels)

---

Copyright 2022, Noroff Accelerate AS