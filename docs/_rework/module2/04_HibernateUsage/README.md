# 4. Jobbe med Hibernate

# Jobbe med Hibernate

Denne leksjonen dekker praktisk bruk av Hibernate. Dette inkluderer å utføre CRUD-operasjoner på ulike enheter, lage tilpassede spørringer og introdusere konseptet med tjenester.

**Læringsmål:**

Du vil kunne…

- **opprette** repoer som representerer enhetene, gitt et eksisterende skjema og entiteter.
- **beskrive** hva en tjeneste er og beskrive dens rolle.
- **demonstrere** bruken av repoer for å manipulere lagrede data.
- **lage** tilpassede søk
- **diskutere** livssyklusen til en enhet
- **skille mellom** bruken av repoer og sessions

> RESSURSE : [Repository med leksjonens prosjekt](https://gitlab.com/NicholasLennox/spring-data-jpa-with-gradle)
> 

## JPA Repositories

Husk at Jakarta/Java Persistence API (JPA) er et sett med grensesnitt som definerer hvordan databaseinteraktivitet utføres i Java. Den implementeres i Spring ved å bruke en Object Relalational Mapper (ORM) kalt Hibernate.

Den forrige bruken av Hibernate var utelukkende for å konfigurere datakilden gjennom skjemagenererings-verktøyene som ble levert. Hibernate brukes i denne leksjonen for å manipulere de lagrede dataene, gjennom **JPA Repositories**.

*Repoet* er kjerneabstraksjonen som brukes. Det krever domeneklassen å administrere, så vel som typen ID. Deretter utvides det med `CrudRepository` -grensesnittet som gir implementeringer :

```java
public interface CrudRepository<T, ID> extends Repository<T, ID> {
  <S extends T> S save(S entity);      
  Optional<T> findById(ID primaryKey); 
  Iterable<T> findAll();               
  long count();                        
  void delete(T entity);               
  boolean existsById(ID primaryKey);   
  // … more functionality omitted.
}
```

På toppen av `CrudRepository`er det en `PagingAndSortingRepository` -abstraksjon som legger til flere metoder for å lettere få nummerert tilgang til enheter:

```java
public interface PagingAndSortingRepository<T, ID> extends CrudRepository<T, ID> {
  Iterable<T> findAll(Sort sort);
  Page<T> findAll(Pageable pageable);
}
```

Så til slutt, for vår bruk, er `JpaRepository`, som er en teknologispesifikk persistens-abstraksjon levert av Spring, `JpaRepository`utvider følgende grensesnitt:

- `CrudRepository < T,ID >`
- `PagingAndSortingRepository < T,ID >`
- `QueryByExampleExecutor <T>`
- `Repository< T,ID >`

> MERK : Dette er de angitte grensesnittene som vi utvider, vi trenger ikke å redefinere dem.
> 

For å lage våre domenespesifikke repoer utvider vi `JpaRespository`, for eksempel til å lage et StudentRespository :

```java
@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {

}
```

Her definerer vi vårt tilpassede repo for `Student`-domeneklassen (entitet). Legg merke til hvordan vi måtte gi klassewrapperen og ikke det primitive for ID-typen. Uten utvidet funksjonalitet er vi i stand til å gjøre følgende til vår `studentdomeneklasse`:

- Legg til en ny student
- Oppdater en eksisterende student
- Få alle elevene
- Få en spesifikk student
- Slett en student
- Sorter og nummerere resultater fra get
- Kan også batchforespørsler

> DOKUMENTASJON : Referanse for [JpaRepository](https://docs.spring.io/spring-data/jpa/docs/current/api/org/springframework/data/jpa/repository/JpaRepository.html)
> 

Følgende illustrasjon viser hvordan alle grensesnittene er relatert for å gi ønsket funksjonalitet:

![Untitled](4%20Jobbe%20med%20Hibernate%20b169df81ea8e42b8a2877bfe3a03e234/Untitled.png)

Repository heiarki

Den følgende delen omhandler kun henting av data. Det er separate seksjoner for å legge til, oppdatere og slette, da de alle har viktige vanlige fallgruver å unngå.

## Dataspørring

Vi bruker depotet ved konstruktørinjeksjon med `@Autowired`:

```java
@Component
public class AppRunner implements ApplicationRunner {

  private final StudentRepository studentRepository;

  public AppRunner(StudentRepository studentRepository) {
      this.studentRepository = studentRepository;
  }

  @Override
  public void run(ApplicationArguments args) throws Exception {
      System.out.println(studentRepository
      .findById(1).get());
  }
}
```

> MERK : Denne delen dekker kun henting av data. Det er dedikerte seksjoner om manipulering av eksisterende data.
> 

Husk, Hibernate ble konfigurert til å skrive ut all generert SQL, dette er utdata når du kjører applikasjonen:

```sql
Hibernate: 
select student0_.id as id1_2_0_, 
 student0_.name as name2_2_0_, 
 student0_.professor_id as professo3_2_0_, 
 student0_.project_id as project_4_2_0_, 
 professor1_.id as id1_0_1_, 
 professor1_.name as name2_0_1_, 
 project2_.id as id1_1_2_, 
 project2_.title as title2_1_2_ 
from student student0_ 
left outer join professor professor1_ 
 on student0_.professor_id=professor1_.id 
left outer join project project2_ 
 on student0_.project_id=project2_.id 
where student0_.id=?

no.acclerate.hibernatetestgradle.models.Student@5ba184fc
```

Ved å undersøke den genererte SQL - en kan vi observere noen av standardoppførselene nevnt tidligere. Husk, som standard er samlinger *Lazy Loaded* . Hva betyr dette? Det betyr at Hibernate bare vil spørre objektene når de er tilgjengelig.

Du kan se dette ved at `subjects` er ikke nevnt i det hele tatt i den genererte SQL-en, men både `professor`og `project`er tilstede.

> MERK : Husk at vi kan endre `FetchType` i enhetene. Lazy Loading foretrekkes imidlertid der det er mulig.
> 

### Vanlig Lazy Loading-problem

For å illustrere Lazy loading, og feilen som er oftest sett , inkluderer vi en forespørsel om å få fagene for en student, som vist nedenfor:

```java
@Override
public void run(ApplicationArguments args) throws Exception {
    System.out.println(studentRepository
    .findById(1).get()
    .getSubjects());
}
```

Når du kjører applikasjonen, får vi en feilmelding som ligner på følgende:

```java
org.hibernate.LazyInitializationException : could not initialize proxy – no Session
```

*Hva skjer her?*

For å forstå dette er det noen nøkkelbegreper å huske og et nytt:

- *Session* er en vedvarende kontekst som representerer et samspill mellom en applikasjon og databasen.
- *Lazy Loading* betyr at objektet ikke lastes inn i sesjonskonteksten før det er åpnet i kode.
- Hibernate oppretter en dynamisk *Proxy Object* -underklasse som vil treffe databasen først når vi bruker objektet.

Denne feilen oppstår når vi prøver å hente et lazy loaded objekt fra databasen ved å bruke et proxy-objekt, men hibernate-økten er allerede lukket. I dette tilfellet `emner` er proxy-objektet vårt, siden det ikke lastes inn i Hibernates session og hentes når det åpnes.

> MERK : `JpaRespository` er et Spring Data JPA-spesifikt grensesnitt, som samhandler med Hibernates Enhetsleder.
> 

*Hvorfor skjer dette så lett?*

Når vi bruker repositories, gjør de visse ting for å være mer kodeeffektive. En av disse tingene er å administrere økter automatisk. Vi ringte ganske enkelt:

```java
studentRepository.findById(1).get()
```

For å få studenten vår, mens hvis vi brukte økten direkte , måtte vi ha gjort noe som ligner på følgende:

```java
Session session = sessionFactory.openSession();
session.beginTransaction();
Student persistentUser = session.find(Student.class, 1);
session.getTransaction().commit();
session.close();
```

> MERK : Noe lignende kan oppnås med Hibernates Entity Manager.
> 

For å oppnå linjeeffektivitet ga vi opp kontrollen over økten. Dette betyr at så snart vi sier `findById`er transaksjonen allerede fullført og økten er stengt, så når vi ringer `getSubjects`er det ingen økt tilgjengelig.

> DOKUMENTASJON : Forholdet mellom økt- og enhetsadministrator kan sees fra [Hibernates dokumentasjon](https://docs.jboss.org/hibernate/orm/current/userguide/html_single/Hibernate_User_Guide.html#architecturedokumentasjon).
> 

*Hvordan kan dette fikses?*

Det er to vanlige metoder for å fikse dette, begge krever nøye oppmerksomhet.

Den første er ved *å endre hentetypen til EAGER*:

```java
@ManyToMany(fetch = FetchType.EAGER)
@JoinTable(
        name = "student_subject",
        joinColumns = {@JoinColumn(name = "student_id")},
        inverseJoinColumns = {@JoinColumn(name = "subject_id")}
)
private Set<Subject> subjects;
```

Denne metoden ANBEFALES IKKE og anses som en dårlig praksis selv om den fungerer. Hvorfor blir dette sett på som en dårlig praksis? Enkelt sagt, eager lasting henter ALLE data selv når det ikke er nødvendig. Dette resulterer i at store, langsomme spørringer blir generert og utført.

> MERK : Eager lasting er et svært vanlig område for ytelsesflaskehalser.
> 

Den andre, og foretrukne, måten å håndtere dette på er ved å få metoden til å samhandle med depotet `@Transactional`.

`@Transactional` -kommentaren er ganske komplisert, men det viktigste aspektet er at *den automatisk administrerer økten og vil holde den åpen til metoden er ferdig utført* .

```java
@Override
@Transactional
public void run(ApplicationArguments args) throws Exception {
    System.out.println(studentRepository
    .findById(1).get()
    .getSubjects());
}
```

Dette resulterer i følgende SQL:

```sql
Hibernate: 
select student0_.id as id1_2_0_, 
 student0_.name as name2_2_0_, 
 student0_.professor_id as professo3_2_0_, 
 student0_.project_id as project_4_2_0_, 
 professor1_.id as id1_0_1_, 
 professor1_.name as name2_0_1_, 
 project2_.id as id1_1_2_, 
 project2_.title as title2_1_2_ 
from student student0_ 
left outer join professor professor1_ 
 on student0_.professor_id=professor1_.id 
left outer join project project2_ 
 on student0_.project_id=project2_.id 
where student0_.id=?

Hibernate: 
select subjects0_.student_id as student_1_3_0_, 
 subjects0_.subject_id as subject_2_3_0_, 
 subject1_.id as id1_4_1_, 
 subject1_.code as code2_4_1_, 
 subject1_.professor_id as professo4_4_1_, 
 subject1_.title as title3_4_1_, 
 professor2_.id as id1_0_2_, 
 professor2_.name as name2_0_2_ 
from student_subject subjects0_ 
inner join subject subject1_ 
 on subjects0_.subject_id=subject1_.id 
left outer join professor professor2_ 
 on subject1_.professor_id=professor2_.id 
where subjects0_.student_id=?

[no.acclerate.hibernatetestgradle.models.Subject@5c4714ef]
```

Legg merke til her hvordan Hibernate genererte og utførte en andre spørring for å få de relaterte `subjects`. `@Transactional`har noen andre bivirkninger som må tas til etterretning; mest bemerkelsesverdig, det krever ikke at du `lagrer`i det hele tatt, siden det hele tiden synkroniserer eventuelle endringer med databasen, noe som gjør `save` overflødig. Dette er en vanlig fallgruve - å bruke `@Transactional`og `save`.

> MERK : `@Transactional` vil bli tatt opp igjen i fremtiden. Den bør også bare brukes når den er nødvendig, da den eksisterer for å tjene spesifikke formål.
> 

> MERK : Hvis vi ønsket å få fagene for en student, kunne vi ha gjort det langt mer effektivt ved å bruke en tilpasset spørring. Eksemplet ovenfor er bare for å illustrere Lazy Loading.
> 

Det er en lignende metode for `findById`kalt `getById`. Dette returnerer en referanseproxy til et objekt, ikke selve objektet, siden hele objektet er Lazily Loaded. Dette høres bra ut, men å bruke `findById`er mer forutsigbart ettersom det er en retur `null`som vi deretter kan kaste et tilpasset unntak av. `findById`vil også tillate at felt lastes Lazy, som vist i eksemplet ovenfor. `getById`opptrer også på uvanlige måter på grunn av sin fullstendige lazy loaded-tilstand, noe som resulterer i uvanlige problemer når du prøver å oppdatere objekter gjennom `save`.

> MERK : Kort sagt, bruk `findById` over `getById` for vår bruk.
> 

## Egendefinerte søk

Noen ganger gir ikke samhandling med de medfølgende implementeringene fra Hibernate og Spring Data JPA den ønskede funksjonaliteten når det kommer til tilpasset forretningslogikk. Dette forventes da disse implementeringene er der for å gi et utgangspunkt med alle de vanlige interaksjonene.

For å utføre tilpassede spørringer, eller lagrede prosedyrer, må vi bruke måter å utvide atferd på. Dette gjøres hovedsakelig på to måter:

- Bruk av Spring Data JPAs robuste egendefinerte spørringsgenerering gjennom *spørringsmetoder*.
- Bruke en merknad for å skrive faktisk SQL

> MERK : Vi dekker ikke hvordan du utfører lagrede prosedyrer eller spørringer definert andre steder i dette kurset.
> 

> DOKUMENTASJON : En fullstendig referanseveiledning for spørring med Spring Data JPA finner du [her](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repositories.query-methods.details)
> 

### Bruke spørremetoder

Spring Data JPA bruker en spørringsbygger for å oversette nøkkelord til SQL-kommandoer. Dette er hvordan alle de oppgitte metodene implementeres - f.eks `findById`er delt opp i `find`, `ById`og forventer en parameter av typen ID.

Uten å gå inn på detaljer, kan vi bruke dette til å lage våre egne metoder på vårt repo, for eksempel hvis vi endrer StudentRepository til å inkludere følgende:

```java
@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {
    Optional<Student> findFirstByNameIsContaining(String name);
}
```

Vi kan da kalle på denne metoden:

```java
@Override
public void run(ApplicationArguments args) throws Exception {
    Student search = studentRepository
    .findFirstByNameIsContaining("Ola").get();
    System.out.println(search.getName());
}
```

Legg merke til hvordan vi ikke har `@Transactional`lenger, dette er fordi det ikke er nødvendig. Når du kjører applikasjonen , produseres følgende utdata:

```sql
Hibernate: 
select student0_.id as id1_2_, 
 student0_.name as name2_2_, 
 student0_.professor_id as professo3_2_, 
 student0_.project_id as project_4_2_ 
from student student0_ 
where student0_.name like ? escape ? limit ?

Ola Nordmann
```

Dette søket søker med en delvis oppdatering og får deretter toppresultatet. Vi kan fortsette å lenke dette - Intelli - sansen hjelper deg med å fullføre spørringer.

> DOKUMENTASJON : Søkemetoder er svært allsidige, det er alt for mange nøkkelord å nevne. En fullstendig liste finner du [her](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#appendix.query.method.subject), [her](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#appendix.query.method.predicate) og [her](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.query-creation).
> 

En begrensning ved å bruke spørringsmetodene er at de kun kan få domeneklassen repoet er konfigurert for - da det bruker refleksjon. Hvis du for eksempel ønsker å få alle fagene til en student, må du lage et `SubjectRepository`. Dette er designet og er i tråd med beste praksis rundt DDD.

### Bruk av navngitte søk

En annen metode er å skrive egendefinerte spørringer, kalt navngitte spørringer. Disse omgår kartleggingen fullstendig og utfører SQL-en du skriver direkte. De to hovedmåtene du gjør dette på er med en *navngitt spørring* som er skrevet på enhetene og peker til en repometode, eller med `@Query`direkte på repoet. For våre formål vil vi bruke `@Query`-tilnærmingen.

Som et eksempel etterligner følgende `@Query`forrige eksempel:

```java
@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {
    @Query("select s from Student s where s.name like %?1%")
    Set<Student> findAllByFirstName(String name);
}
```

Spring Data JPA bruker noe som heter JPQL for å oversette til SQL. Dette betyr at intellisense kan hjelpe når du skriver spørringene for å sikre at syntaksen er korrekt.

Dette resulterer i følgende utgang:

```sql
Hibernate: 
select student0_.id as id1_2_, 
student0_.name as name2_2_, 
student0_.professor_id as professo3_2_, 
student0_.project_id as project_4_2_ 
from student student0_ 
where student0_.name like ?

Ola Nordmann
```

Du kan også sortere og nummerere resultatene ved å bruke nøkkelord. Måten parametere sendes på er den samme som ved bruk av JDBC, det er etter rekkefølge. Du kan også ha navngitte parametere.

Hvis du ønsker å skrive riktig SQL og omgå JPQL, kan du legge til flagget `nativeQuery = true`til spørringen:

```java
@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {
    @Query(value = "SELECT * FROM Student s WHERE s.name LIKE %?%", 
    nativeQuery = true)
    Set<Student> findAllByFirstName(String name);
}
```

Dette resulterer i følgende utgang:

```sql
Hibernate: 
SELECT * 
FROM Student s 
WHERE s.name LIKE ?

Ola Nordmann
```

Legg merke til hvordan JPQL ligner mye på Hibernate, det er fordi det er akkurat det Hibernate bruker. En innebygd spørring er ekte SQL.

> DOKUMENTASJON : Referansedokumentasjon for spørremetoder (JPQL) finner du [her](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#repositories.query-methods.query-creation) og [her](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.query-creation).
> 

> DOKUMENTASJON : Referansedokumentasjon for navngitte forespørsler finner du [her](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.query-methods.at-query).
> 

> DOKUMENTASJON : Referanse (ekstra) for arbeid med [lagrede prosedyrer](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#jpa.stored-procedures)
> 

## Datamanipulasjon

Å fortsette med nye, oppdatere eksisterende og slette data er rett frem å utføre med Repositories. Denne delen viser den enkle måten å gjøre ting på og noen vanlige fallgruver når du gjør DML.

### Legger til

Først legger du til en ny student:

```java
@Override
  public void run(ApplicationArguments args) throws Exception {
      Student newStudent = new Student();
      newStudent.setName("John Smith");
      add(newStudent);
  }

  public void add(Student student) {
      studentRepository.save(student);
      System.out.println("Stud ID:" + student.getId());
  }
```

Dette resulterer i følgende utgang:

```sql
Hibernate: 
insert into student (name, professor_id, project_id) 
values (?, ?, ?)

Stud ID: 2
```

Legg merke til hvordan `ID:2`ble generert av Postgres og enhetens ID har blitt oppdatert for å gjenspeile dette. Vi leverte ingen FK-er for relaterte enheter. De har lov til å være null, noe som betyr at forholdet er valgfritt.

### Oppdaterer

La oss oppdatere enheten til å inkludere en professor:

```java
public void update() {
    Student stud = studentRepository.findById(2).get();
    Professor prof = professorRepository.findById(1).get();
    stud.setProfessor(prof);
    studentRepository.save(stud);
    System.out.println("Prof ID: " + stud.getProfessor().getId());
}
```

Dette resulterer i følgende utgang:

```sql
Hibernate: 
update student 
set name=?, professor_id=?, project_id=? 
where id=?

Prof ID: 1
```

> MERK : Når du arbeider med Hibernate, må du bruke enheter og kan ikke bare sende FK-er rundt. Grunnen til dette er litt komplisert, det er en ekstra del om *Entity Lifecycles* hvis du er interessert.
> 

Oppdateringsmetoden ovenfor produserte faktisk mer produksjon enn det som ble inkludert i notatene, hele produksjonen er sett nedenfor og oppsummert:

```sql
Hibernate: select student -- full student id 2 with joins
Hibernate: select professor -- prof id 1
Hibernate: select student -- student id 2 with just fks
Hibernate: select professor -- same prof id 1
Hibernate: update student -- updates
```

Etter å ha sett denne utgangen, er det fornuftig hvorfor ORM-er har en tendens til å være tregere enn tradisjonelle metoder. De gir en god del livskvalitet i noen områder, men det er avveininger. La oss gjøre den samme metoden, men ved å bruke en spørringsmetode:

```java
@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {
  @Modifying
  @Query("update Student s set s.professor.id = ?2 where s.id = ?1")
  int updateStudentsProfessorById(int studentId, int professorId);
}
```

Denne metoden ber ganske enkelt om de to nøklene og bruker JPQL for å lage spørringen. `@Modifying`er nødvendig for å aktivere DML-spørringer, og `@Transactional`er nødvendig for anropsmetoden. Returtypen kan enten være `void`, eller `int`(antall rader berørt) - akkurat som JDBC.

Hvis du tenker på det, er alt vi egentlig trenger FK-ene når vi grupperer en `student`og en `professor`sammen, vi trenger ikke alle de andre feltene siden vi ikke endrer studentdataene. Så det gir ikke mening å ha all den generaliserbare ekstra-koden for en generisk oppdatering når den ikke er nødvendig. Vår `oppdateringsmetode`er også modifisert for å kalle denne nye navngitte spørringen:

```java
public void update() {
    logger.info("Updating student 2 with prof 1");
    int result = studentRepository.updateStudentsProfessorById(2,1);
}
```

Dette resulterer i følgende utgang:

```sql
Hibernate: 
update student 
set professor_id=? 
where id=?
```

Kort og enkelt. Å gå tilbake til normal SQL brukes ofte når ORM genererer langt større spørringer enn nødvendig, eller det blir vanskelig å jobbe med programmessig. Beslutningen om når du skal bruke tilpassede søk vil avhenge av mange forskjellige faktorer, vanligvis ned til forretningslogikk som ikke samsvarer med funksjonaliteten som tilbys.

> MERK : For det meste av kurset vil det å bruke Hibernate direkte, ikke ved å bruke `@Query`, være hvordan vi vil håndtere det store flertallet av interaksjonene. Dette er bare for å illustrere hvordan man kan optimalisere datatilgang.
> 

### Sletter

Fjerne eksisterende data er normalt ikke så vanskelig, vi kaller ganske enkelt `studentRepository.delete (student)`. Imidlertid er det noen ganger det blir vanskelig - på grunn av referanseintegritet. Som et eksempel, la oss prøve å slette en `professor`og se hva som skjer:

```java
professorRepository.deleteById(4);
```

Dette resulterer i følgende:

```sql
Hibernate: 
select professor0_.id as id1_0_0_, 
professor0_.name as name2_0_0_ 
from professor professor0_ 
where professor0_.id=?

Hibernate: 
delete from professor 
where id=?
```

Hibernate spør først for å sjekke om professoren eksisterer. I dette tilfellet har `professor 4`ingen `studenter`og underviser ikke i noen `fag`- ingen problemer med referanseintegritet.

Hva skjer hvis vi prøver å slette en professor som ikke eksisterer?

```java
professorRepository.deleteById(5);
```

Vi får følgende feil:

```
Caused by: org.springframework.dao.EmptyResultDataAccessException: 

No class no.acclerate.hibernatetestgradle.models.Professor entity with id 5 exists!
```

Dette er Spring Data som gjør noen interne kontroller for å sikre at alt fungerer bra. Denne `EmptyResultDataAccessException` kan fanges opp, men det er bedre måter å håndtere dette på:

```java
public void deleteProfessor(int id) {
  if(professorRepository.existsById(id)) {
      professorRepository.deleteById(id);
  }
  else
      logger.warn("No professor exists with ID: " + id);
}
```

Her sjekker vi først om en professor eksisterer med vår egen forretningslogikk. Dette resulterer i følgende utgang:

```sql
Hibernate: 
select count(*) as col_0_0_ 
from professor professor0_ 
where professor0_.id=?

WARN 3344 --- [main] n.a.h.runners.AppRunner: No professor exists with ID: 5
```

Her sjekker Hibernate om noe eksisterer ved å telle forekomstene der ID-en stemmer. Dette er mer effektivt enn å spørre ut dataene og se om de er null.

Hvis vi går videre til å slette en `professor`som er referert:

```java
professorRepository.deleteById(1);
```

Dette gir følgende utgang:

```sql
Hibernate: 
select count(*) as col_0_0_ 
from professor professor0_ 
where professor0_.id=?

Hibernate: 
select professor0_.id as id1_0_0_, 
professor0_.name as name2_0_0_ 
from professor professor0_ 
where professor0_.id=?

Hibernate: 
delete from professor 
where id=?

ERROR 14800 --- [main] o.h.engine.jdbc.spi.SqlExceptionHelper: 
ERROR: update or delete on table "professor" violates foreign key constraint "fk6qd34ryg9xge96upkktpry261" 
on table "student"

Detail: Key (id)=(1) is still referenced from table "student".
```

Som det fremgår av loggene, kan vi ikke slette en `professor`som blir referert et annet sted.

Husk at når du lærer om SQL, kan *kaskader* brukes til å løse disse problemene. Nå, hvis vi setter forholdet mellom `professor`og `student`til å kaskade slette - ville alle studenter for den professoren bli slettet (som også ville ha sine egne referanseproblemer).

> MERK : “Cascade delete be default”-tilnærmingen er en svært farlig måte å programmere på og kan ofte føre til at det meste av databasen blir slettet.
> 

Som et eksempel, la oss se hva som skjer hvis vi konfigurerer kaskade slettinger:

```java
@Entity
public class Professor {
    // -- omitted
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "professor")
    private Set<Student> students;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "professor")
    private Set<Subject> subjects;
    // -- omitted
```

```java
@Entity
public class Student {
    // -- omitted
    @OneToOne(cascade = CascadeType.ALL, mappedBy = "student")
    private Project project;
    // -- omitted
```

Her konfigurerer vi: når en `professor`er slettet, slett eventuelle poster over `student`og `emne`der `professoren`er referert. Vi må også kaskadere `prosjekter`fra `student`, som det er referert til.

Når applikasjonen kjøres, produseres følgende utdata:

```sql
Hibernate: delete from student_subject where student_id=?
Hibernate: delete from student_subject where student_id=?
Hibernate: delete from project where id=?
Hibernate: delete from student where id=?
Hibernate: delete from project where id=?
Hibernate: delete from student where id=?
Hibernate: delete from subject where id=?
Hibernate: delete from professor where id=?
```

Som du kan se, når vi ønsker å fjerne en `professor`, må alle referansene slettes:

- Fjern studenten for denne professoren
- Fjern emnet for denne professoren

Ved å gjøre det er bivirkningene:

- Prosjekter for den studenten fjernes
- Link mellom fag og elev fjernes

> MERK : Mange-til-mange-relasjoner vil fjerne oppføringen som forbinder de to enhetene når en av de refererte enhetene fjernes. Cascading slettinger trenger ikke å konfigureres da de administreres automatisk.
> 

Det er lett å se hvor uforsiktig `cascadeType.ALL` -bruk er svært farlig.

*Hvordan skal dette håndteres da?*

Nok en gang avhenger dette av forretningslogikken din. Akkurat som før når man diskuterer kaskader i SQL. Kaskader er greit hvis du ikke trenger de relaterte dataene for revisjon eller andre forretningsprosesser. For andre situasjoner er det greit å ikke tillate sletting av referanseenheter.

Det eneste nye alternativet vi har nå er å bruke hibernate og nullbare fremmednøkler. Vi er i stand til å fjerne forholdet ved å sette studenten for en professor til å være `null`. For eksempel:

```java
Professor prof = professorRepository.findById(id).get();
prof.setStudents(null);
prof.setSubjects(null);
professorRepository.delete(prof);
```

Men når vi kjører dette, fungerer det ikke. *Hvorfor er det sånn?*

Husk at enheten som `mapdBy`er på, ikke eier forholdet. Det betyr at det ikke kan legge til eller fjernes fra forholdet. Professor har alle `mapdBy` -egenskapene, og vi kan ikke flytte dem til de relaterte enhetene på grunn av `@ManyToOne`.

*Hvordan kan vi omgå dette?*

Det er litt komplisert, men vi må fjerne professoren fra studentene:

```java
Professor prof = professorRepository.findById(id).get();
prof.getStudents().forEach(s -> s.setProfessor(null));
prof.getSubjects().forEach(s -> s.setProfessor(null));
professorRepository.delete(prof);
```

> MERK : Metoden som kaller dette må merkes som `@Transactional,` av samme grunn som tidligere. Å ha en aktiv økt for å få de relaterte enhetene.
> 

Koden ovenfor resulterer i følgende utgang:

```sql
Hibernate: select professor -- get professor
Hibernate: select students -- lazy load students
Hibernate: select subjects -- lazy load subjects

-- Foreach student
Hibernate: 
update student 
set name=?, professor_id=? 
where id=?

Hibernate: 
update student 
set name=?, professor_id=? 
where id=?

-- Foreach subject
Hibernate: 
update subject 
set code=?, professor_id=?, title=? 
where id=?

-- Delete professor
Hibernate: 
delete from professor 
where id=?
```

Nå virker dette ganske kronglete for en enkel sletting og kaskade til null. Det er imidlertid ikke en triviell prosess når det gjelder flere sett med relaterte enheter. Så det er faktisk ikke en så oppblåst måte å håndtere dette på.

> MERK : Det finnes andre metoder for å håndtere dette, men det krever kunnskap om enhetens livssykluser og deres *hooks*, så disse løsningene dekkes i et senere avsnitt.
> 

Det siste aspektet som skal dekkes er rundt en vanlig praksis i bedrifter der *slettinger ikke er slettinger, men oppdateringsflagg* .

Dette er en praksis der du har en kolonne i enheten dedikert til om enheten er *slettet* *eller ikke* . Når du så vil slette det, *oppdaterer du ganske enkelt* flagget til `sant`. Alle spørringene har forretningslogikk for å filtrere ut ikke-aktive poster.

> MERK : Denne metoden krever en god mengde ekstra kode, og derfor gjør vi ikke dette i kurset. Det er viktig å i det minste vite om det.
> 

## Tjenester

Før du går videre er det viktig å forklare konteksten til tjenestene. Husk at `@Component` er en Spring Bean som kan komponentskannes og ha verdier injisert eller være injisert. Vi har allerede sett `@Repository` som en spesiell type komponent, nå har vi `@Service`, og vi vil møte en til, kalt `@Controller`i  en fremtidig leksjon.

Alle disse merknadene behandles likt innen Spring, de er alle undertyper av `@Component`, så det er ingen funksjonell forskjell mellom dem - du kan bytte dem fritt og fortsatt ha alt funksjon.

Dette betyr imidlertid ikke at du bør gjøre dette. Hver merknad har sin egen spesielle plass og forbedrer lesbarheten. Noen har ekstra funksjonalitet akkurat nå, men kan utvides senere av Spring - så det er viktig å respektere konvensjonene.

Når det er sagt, er forskjellene mellom sterotypene som følger:

- `@Component`er en generisk stereotypi for enhver Spring-administrert komponent.
- `@Repository`er en spesialisering av `@Component`for en klasse som fungerer som et datalager, eller et datatilgangsobjekt (DAO). Dette blir ofte referert til som utholdenhetslaget da de kobles direkte til databasen. Et repository kaster også eventuelle unntak på nytt.
- `@Service`er en spesialisering av `@Component`som holder forretningslogikken og kaller opp lagrene. Alle data manipuleres i dette laget og sendes ganske enkelt videre. Tjenestelaget kan også gi alle forretningslogikkrelaterte unntak. Klasser som er tjenester utgjør tjenestelaget til applikasjonen.
- `@Controller`er en spesialisering av `@Component`som håndterer klientinteraksjon. Denne klassen kan ikke erstattes med `@Respository`eller `@Service`og fungere som tiltenkt. Kontrollere har spesiell funksjonalitet knyttet til seg, og vi vil se denne igjen og diskutere dens plass når vi flytter inn i *Spring Web* .

Oppsummert hjelper disse merknadene med å definere lag og grensene mellom dem i en applikasjon. Det er viktig å ha denne distinksjonen siden den forbedrer lesbarheten og vedlikeholdsvennligheten , samt sikrer at vi opprettholder **atskilte bekymringer**. Dette emnet tas opp igjen når vi har den fulle konteksten.

Når vi flytter tilbake til fokuset i denne delen, eksisterer `@Services`for å holde forretningslogikken og all manipulasjon. Så langt har vi gjort alt dette i `ApplicationRunner`som er en dårlig praksis siden det er klienten og ikke skal manipulere data - det må være en separasjon .

Det ideelle for vår nåværende kontekst er å få klienten ( `ApplicationRunner`) til å samhandle med en svart boks for å utføre operasjonene. Denne svarte boksen er vår `@Service`. Vi bør lage en tjeneste for å representere hver domenekontekst i applikasjonen vår, for eksempel kan vi lage en tjeneste for studentene våre:

```java
public interface CrudService <T, ID> {
  // Generic CRUD
  T findById(ID id);
  Collection<T> findAll();
  T add(T entity);
  T update(T entity);
  void deleteById(ID id);
  void delete(T entity);
}
```

Først lager vi en `CrudService`for å være litt mer DRY og lagre noen linjer når vi ønsker å lage andre tjenester. Denne tilnærmingen ligner også mye på Repository-implementeringene våre, så det er logisk å følge den. Deretter lager vi den faktiske tjenesten med eventuelle ekstra metoder vi trenger for å håndtere forretningslogikken:

```java
public interface StudentService extends CrudService<Student, Integer> {
  // Extra business logic
  void setProfessor(int studentId, int professorId);
  void setProject(int studentId, int projectId);
}
```

Her kan vi legge til så mange metoder som trengs for forretningslogikk. I dette tilfellet har vi definert at en student kan, i tillegg til vanlig crud:

- Endre hvilken professor som veileder dem
- Endre prosjektet de gjør
- Definer hvilke fag de tar

Nå gir det ikke helt mening å la dette gjøres av studenter, det kan være mer fornuftig at visse ting blir gjort av professorer. Dette var imidlertid bare for å illustrere hvordan forretningslogikk er definert. Det neste trinnet er å gi en implementering:

```java
@Service
public class StudentServiceImpl implements StudentService {
  private final StudentRepository studentRepository;
  private final Logger logger = LoggerFactory.getLogger(StudentServiceImpl.class);

  public StudentServiceImpl(StudentRepository studentRepository) {
      this.studentRepository = studentRepository;
  }

  @Override
  public Student findById(Integer id) {
      return studentRepository.findById(id).get();
  }
  // omitted
```

Denne klassen må gi implementering av alle metodene som er definert i tjenestegrensesnittet. Det meste av den generiske CRUD er ganske enkelt en utsettelse til repoet. Noen implementeringer kan imidlertid kreve at flere operasjoner utføres på repoet. Tjenester kan også bruke flere repoer.

For eksempel sletting av professorer. Det krevde noen få trinn og til og med en `@Transactional` -kommentar. La oss se hvordan det er innkapslet i en tjeneste:

```java
@Service
public class ProfessorServiceImpl implements ProfessorService {
  private final Logger logger = LoggerFactory.getLogger(ProfessorServiceImpl.class);
  private final ProfessorRepository professorRepository;

  public ProfessorServiceImpl(ProfessorRepository professorRepository) {
      this.professorRepository = professorRepository;
  }

  @Override
  @Transactional
  public void deleteById(Integer id) {
    if(professorRepository.existsById(id)) {
        Professor prof = professorRepository.findById(id).get();
        prof.getStudents().forEach(s -> s.setProfessor(null));
        prof.getSubjects().forEach(s -> s.setProfessor(null));
        professorRepository.delete(prof);
    }
    else
        logger.warn("No professor exists with ID: " + id);
  }
```

> MERK : Det er noen feil som ikke håndteres riktig, vi vil se hvordan vi håndterer disse unntakene mer elegant enn logging når vi flytter inn i Spring Web.
> 

Til slutt er klientklassen vår ( `ApplicationRunner`) ryddet kraftig opp og trenger kun en referanse til tjenesten:

```java
@Component
public class AppRunner implements ApplicationRunner {
  private final ProfessorService professorService;

  public AppRunner(ProfessorService professorService) {
      this.professorService = professorService;
  }

  @Override
  public void run(ApplicationArguments args) throws Exception {
      professorService.deleteById(1);
  }
}
```

Det er ideelt å ha klientklassene uvitende om implementeringer eller logikk. De er løst koblet og det er en godt definert grense mellom dem. De bør bare prøve å utføre handlinger og håndtere eventuelle unntak som kan oppstå.

Et siste poeng er at hvis vi har flere implementeringer av `StudentService`eller `ProfessorService`, kan vi bare merke en som en `@Service`eller benytte *Spring Profiles*. Grunnen til dette er enkel: Spring vet ikke hvilken instans den skal gi hvis det er tvetydighet. Vi vil dekke *Spring Profiles* i en leksjon om *avansert distribusjon* .

## Entitets livssykluser

Før du fortsetter, er det viktig å merke seg at denne underseksjonen er teknisk utenfor omfanget, det er en mer avansert “behind the scenes” titt på Hibernate som ikke er nødvendig for å kunne bruke den. Det er imidlertid noen verdifulle lærdommer å lære i denne delen.

> MERK : Vurder å lese denne delen når du har øvd litt på Hibernate.
> 

Hver Hibernate-enhet har naturlig nok en livssyklus innenfor rammeverket – den er enten i en *forbigående* , *administrert* , *frakoblet* eller *slettet* tilstand. Dette styres av en **persistenskontekst** som er noe som sitter mellom klientkoden og datalageret.

Persistenskonteksten implementerer det som er kjent som en *arbeidsenhetsmønster*. Den holder styr på alle innlastede data, sporer endringer av disse dataene og er ansvarlig for å til slutt synkronisere eventuelle endringer tilbake til databasen ved slutten av forretningstransaksjonen.

> MERK : Hvis dette høres kjent ut, er det nøyaktig hva GIT gjør for å spore endringer for commits.
> 

*JPA EntityManager* og *Hibernates Session* er en implementering av konseptet persistenskontekst.

> MERK : Vi samhandler ikke direkte med økten, vi gjør det gjennom JPA-repositoriene vi oppretter. Så enhver referanse til øktsett videre bør sees i referanse til depotene.
> 

En **administrert/vedvarende enhet** er en representasjon av en databasetabellrad (selv om den raden ikke trenger å eksistere i databasen ennå). Dette administreres av sesjonen som kjører for øyeblikket, og hver endring som gjøres på den vil spores og overføres til databasen automatisk.

En **løsrevet enhet** er bare en vanlig enhet POJO hvis identitetsverdi tilsvarer en databaserad. Forskjellen fra en administrert enhet er at den ikke lenger spores av noen vedvarende kontekst.

En **forbigående enhet** er ganske enkelt et enhetsobjekt som ikke har noen representasjon i den vedvarende lagringen og ikke administreres av noen session. Et typisk eksempel på en forbigående enhet vil være å instansiere en ny enhet via dens konstruktør. For å gjøre en forbigående enhet vedvarende, må vi kalle `Session.save (entity)`eller `Session.saveOrUpdate (entity)`.

Et flott diagram over forholdet mellom de forskjellige statene er levert av [Vlad Mihallcea](https://vladmihalcea.com/a-beginners-guide-to-jpa-hibernate-entity-state-transitions/):

![https://vladmihalcea.com/wp-content/uploads/2014/07/jpaentitystates.png](https://vladmihalcea.com/wp-content/uploads/2014/07/jpaentitystates.png)

Dvaletilstandsdiagram

> MERK : Denne informasjonen er en mer dypere titt på Hibernate, men en vanlig feil å se er “detached entity passed to persist”. Å forstå persistensmekanismen hjelper i stor grad med å feilsøke den feilen.
> 

Hver enhet har flere livssyklus-*hooks* som kan benyttes for å hjelpe til med å administrere/inspisere staten i ulike stadier. Dette er mest brukt når vi ønsker å kontrollere hva som skjer med relaterte enheter når en enhet endres eller slettes. De tilgjengelige livssyklus-*hooks-*ene er representert av følgende merknader:

- `@PrePersist`er kalt for en ny enhet
- `@PostPersist`kalles opp etter at en enhet er lagret
- `@PreRemove`kalles før en enhet fjernes
- `@PostRemove`kalles opp etter at en enhet er slettet
- `@PreUpdate`kalles opp før oppdateringsoperasjonen
- `@PostUpdate`kalles opp etter at en enhet er oppdatert
- `@PostLoad`kalles opp etter at en enhet er lastet inn

Det er to tilnærminger for bruk av livssyklushendelsesmerknader: annoteringsmetoder i enheten og opprettelse av en `EntityListener`med kommenterte tilbakeringingsmetoder.

> RESSURSE : Implementeringer av livssykluskrokene finnes i en artikkel fra [Baeldung](https://www.baeldung.com/jpa-entity-lifecycle-events)
> 

## Oppsummering av fallgruver

Dette er et kort avsnitt for raskt å liste opp de vanligste feilene som er sett og hvor du kan se for å begynne å fikse dem:

- Ingen aktiv økt - løs med `@Transactional`
- Frakoblet enhet overført til å vedvare – arbeid med hele enheter som ikke er delvise (bruk `finn`).
- Bryter fremmednøkkel - se å løse med enten; sette fk til null eller kaskade slettinger

## Ressurser

- [Dvaledokumentasjon](https://docs.jboss.org/hibernate/orm/current/userguide/html_single/Hibernate_User_Guide.html)
- [Spring Data JPA-dokumentasjon](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/)
- [Repository med leksjonens prosjekt](https://gitlab.com/NicholasLennox/spring-data-jpa-with-gradle)

Copyright 2022, Noroff Accelerate AS