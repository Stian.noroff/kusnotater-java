# 3. Hibernate-konfigurasjon

# Hibernate-konfigurasjon

Denne leksjonen dekker det velkjente og brukte Java Persistence API (JPA), som bruker Spring Boot og Hibernate som implementering. Denne leksjonen dekker prosessen med å lage en PostgreSQL-database ved hjelp av Hibernate og JPA Entities.

**Læringsmål:**

Du vil kunne…

- **beskrive** JPA.
- **diskutere** rollen til JPA i moderne Java-utvikling.
- **definere** Hibernate i sammenheng med JPA.
- **konfigurere** en PostgreSQL-database.
- **generere** et databaseskjema ved hjelp av Hibernate, gitt en scenariobeskrivelse.
- **skille mellom** entitet skjemaannnotasjoner
- **designe** enheter som resulterer i ulike relasjoner mellom tabeller.

## Spring Data

Spring Data gir en kjent og konsistent, Spring-basert programmeringsmodell for datatilgang. Det gjør det enkelt å bruke datatilgangsteknologier, relasjonelle og ikke-relasjonelle databaser, kartreduserende rammeverk og skybaserte datatjenester. Spring Data gir følgende funksjoner:

- Repo og tilpassede objektkartleggingsabstraksjoner
- Dynamisk spørringsavledning fra repo metodenavn
- Baseklasser med grunnleggende egenskaper som implementerer domene
- Støtte for transparent revisjon (opprettet, sist endret)
- Mulighet for å integrere tilpasset repokode
- Enkel Spring integrasjon
- Avansert integrasjon med Spring MVC-kontrollere

Spring Data inneholder mange delprosjekter, de to som for øyeblikket er bemerkelsesverdige er: JDBC og JPA.

Spring Data JDBC er Springs implementering av datatilgang via JDBC. Den gir en `JdbcTemplate`-klasse for å sende spørringer til og håndtere resultater. Den parameteriserer automatisk hver spørring.

> MERK : JDBC ble brukt direkte tidligere fordi poenget var å bli kjent med datatilgang på et lavere nivå slik at verktøy som Spring Data blir forstått i seg selv.
> 

Det andre delprosjektet er Spring Data JPA. Det er dette det vil bli satt fokus på fremover i kurset.

### JPA

JPA står for Jakarta Persistence API (tidligere Java Persistence API). Det er en API-spesifikasjon for databasetilkobling i Java-applikasjoner. Siden JPA kun er en spesifikasjon, trenger du en implementering for å bruke den. Den mest populære implementeringen er [Hibernate ORM](https://hibernate.org/orm/) .

> MERK : ORM står for Object Relational Mapping. De er verktøy som konverterer objekter til databasetabeller.
> 

Spring Data JPA er et delprosjekt av Spring Data som brukes til å enkelt implementere JPA-baserte repositories. Gjør det enklere å bygge Spring-drevne applikasjoner som bruker datatilgangsteknologier.

Implementering av et datatilgangslag i en applikasjon har vært tungvint en stund. For mye standardkode må skrives for å utføre enkle spørringer samt utføre paginering og revisjon. Spring Data JPA har som mål å forbedre implementeringen av datatilgangslag betydelig ved å redusere innsatsen til den mengden som faktisk er nødvendig .

Som utvikler skriver du repogrensesnittene dine, inkludert tilpassede finnermetoder, og Spring vil sørge for implementeringen automatisk.

> MERK : Spring Data JPA bruker Hibernate som standardimplementering av JPA.
> 

I denne leksjonen er fokuset på Hibernate-konfigurasjon og ikke bruk av Hibernate i en Spring Data JPA-kontekst (repositories). Det er en dedikert leksjon om bruken av JPA.

Forholdet Spring har til JPA og implementeringen av det er vist i diagrammet nedenfor:

![Untitled](3%20Hibernate-konfigurasjon%203efa6a225b3046e3a9a0249f8afa08e8/Untitled.png)

Forholdet kan oppsummeres som:

- **Spring Data JPA** er et abstraksjonslag (innpakning) på toppen av JPA for å redusere mengden kode som trengs til et datatilgangsobjekt
- **JPA** er ganske enkelt en spesifikasjon for å fasilitere objektrelasjonell kartlegging for å administrere data i en Java-applikasjon
- **Hibernate** er ganske enkelt en JPA-implementering
- Hibernate genererer SQL-spørringer og kjører dem via **JDBC**

## Hibernate atferdskonfigurasjon

Det første trinnet når du ønsker å bruke Hibernate er å sette opp riktig konfigurasjon slik at Hibernate vet hva de skal gjøre. Dette innebærer å gi detaljer om følgende:

- Hvor databasen er
- Hvilken SQL-dialekt databaseleverandøren er
- Hvordan bør Hibernate generere tabeller
- Skal Hibernate skrive ut den genererte SQL-en

Til å begynne med, la oss få et grunnleggende prosjekt som inkluderer Hibernate. Lag et nytt Spring Initialzr- prosjekt med to avhengigheter:

- Spring Data JPA
- PostgreSQL-driver

Dette bør resultere i at følgende avhengigheter er tilstede i `build.gradle`:

```java
dependencies {
    implementation 'org.springframework.boot:spring-boot-starter-data-jpa'
    runtimeOnly 'org.postgresql:postgresql'
    testImplementation 'org.springframework.boot:spring-boot-starter-test'
}
```

> MERK : Spring har flere startmaler for ulike typer prosjekter.
> 

Det første trinnet er å definere vår datakilde . Dette gjøres på samme måte som når du bruker JDBC direkte, da Hibernate fortsatt bruker det under panseret. Kodebiten nedenfor illustrerer koblingen vår til en postgres- database kalt `postgradhibernate`:

```java
spring.datasource.url= jdbc:postgresql://localhost:5432/postgradhibernate
spring.datasource.username= postgres
spring.datasource.password= postgres
```

Neste trinn er å definere hvordan Hibernate skal oppføre seg når du kjører. Den første egenskapen vi ønsker å konfigurere er `dialekten`. Dette definerer ganske enkelt hvordan Hibernate vil konstruere SQL-spørringer for å spille til styrken til den valgte leverandøren. Dette forbedrer ytelsen litt.

```
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.PostgreSQL95Dialect
```

> MERK : Verdien av `PostgreSQL95Dialect`kommer fra anbefalingen fra Hibernates dokumentasjon. Den aktuelle dokumentasjonen finner du her .
> 

Den neste egenskapen handler om å kontrollere Hibernates påvirkning av strukturen til databasen din. Denne egenskapen kalles `spring.jpa.hibernate.ddl -auto`og har flere mulige verdier:

- `ingen`(standard) - denne verdien slår effektivt av DDL-genereringen. Brukes vanligvis i produksjon.
- `validere`Hibernate validerer bare om tabellene og kolonnene eksisterer, ellers gir det en exception.
- `opprette`Hibernate slipper først eksisterende tabeller, og oppretter deretter nye tabeller.
- `create-drop`(standard hvis du bruker en innebygd database) - ligner på create, med tillegg til at Hibernate vil slippe databasen etter at alle operasjoner er fullført. Brukes vanligvis til enhets-/integrasjonstesting.
- `oppdatering`objektmodellen som er opprettet basert på tilordningene sammenlignes med det eksisterende skjemaet, og deretter oppdaterer Hibernate skjemaet i henhold til forskjellen. Den sletter aldri eksisterende tabeller eller kolonner selv om de ikke lenger kreves av applikasjonen.

> MERK : Husk, `ddl` står for Data Definition Language.
> 

For våre formål er det fornuftig å bruke `create`. Vi ønsker å fullstendig slette og gjenskape tabeller på hver kjøring mens vi er i utvikling.

```java
spring.jpa.hibernate.ddl-auto=create
```

Det neste aspektet å konfigurere er om Hibernate skal skrive ut alt det gjør. Dette inkluderer statistikken for hver transaksjon og den genererte SQL-en. Dette er noe som hjelper med å identifisere ytelsesproblemer og er vanligvis deaktivert for et produksjonsmiljø.

```java
#Turn Statistics on
logging.level.org.hibernate.stat=debug
# Enable logging
spring.jpa.show-sql=true
```

Den fullstendige konfigurasjonen for applikasjonen er som følger:

```java
# Datasource configuration
spring.datasource.url= jdbc:postgresql://localhost:5432/postgradhibernate
spring.datasource.username= postgres
spring.datasource.password= postgres
# Hibernate behaviour
spring.jpa.properties.hibernate.dialect=org.hibernate.dialect.PostgreSQL95Dialect
spring.jpa.hibernate.ddl-auto=create
#Turn Statistics on
logging.level.org.hibernate.stat= trace
# Enable logging
spring.jpa.show-sql=true
```

På dette tidspunktet er vi klare til å begynne å bruke Hibernate for å lage databasetabeller.

## Konfigurering av entiteter

Husk at i Domain Driven Design er **Entities** objekter som har en identitet og representerer et aspekt av domenemodellen. På vanlig engelsk er *entiteter objekter som representerer databasetabeller* .

Entiteter er basert på vanlige gamle Java-objekter (POJOs) og er derfor veldig enkle klasser som ikke inneholder noen forretningslogikk. Dette betyr at en enhet inneholder følgende aspekter:

- private felt
- offentlige gettere og settere
- merknader for å konfigurere enheten

### Grunnleggende konfigurasjon

Som et eksempel, la oss gjenskape `elevtabellen`som vist i en tidligere leksjon. For det første trenger vi en `student`POJO for å representere informasjonen:

```java
public class Student {
    int id;
    String name;
}
```

> MERK : Denne strukturen er en ren POJO og vil svært sjelden bli sett i produksjon. Normalt vil objekter enten være et verdiobjekt, dto eller domenemodell.
> 

Å gjøre elevklassen om til en enhet er veldig enkelt, vi må gjøre feltene private (med gettere og settere) og legge til en `@ Entity-`kommentar. Dette vil kreve at vi definerer en **primærnøkkel**, vi kan gjøre det med `@Id` merknad :

```
@Entity
public class Student {
    @Id
    private int id;
    private String name;
    // Getters and setters
}
```

> MERK : Gettere og settere er kun nødvendig for datatilgang, tabellen vil bli generert uavhengig av deres inkludering.
> 

Hvis applikasjonen kjøres, vil en tabell bli generert i den konfigurerte databasen. Prosjektet er konfigurert til å logge all SQL generert, så å kjøre programmet skriver ut følgende utdata:

```sql
Hibernate: drop table if exists student cascade

Hibernate: create table student (id int4 not null, name varchar(255), primary key (id))
```

Det er et par ting å merke seg her:

- Tabellnavnet ble hentet fra enhetsnavnet.
- Kolonnenavnene ble hentet fra enhetens felt
- Kolonnetypene ble hentet fra datatypene til feltene

Det er en kartlegging som Hibernate har mellom Java-typer og de spesifikke SQL- dialekttypene , som den bruker for å automatisere mye av denne prosessen.

Kartlegging kan også konfigureres ved å bruke `@Basic-`kommentaren. Dette kan brukes til å si “bruk standard hibernate mapping” med tilpasninger for nullbarhet og hentetype (lat eller ivrig).

> MERK : Alle felt er merknader med `@Basic` som standard.
> 

> MERK : Hvis en tilpasset tilordning er nødvendig, må denne konfigureres.
> 

Nå er det viktig å merke seg at dette kan tilpasses, vi kan endre, blant annet:

- Tabellnavn
- Kolonnenavn
- Nullbarhet
- Maks lengder

Dette gjøres gjennom merknader, kodebiten nedenfor illustrerer noen av disse tilpasningene:

```java
@Entity
@Table(name = "tb_student")
public class Student {
    @Id
    @Column(name = "stud_id")
    private int id;
    @Column(name = "stud_name", length = 50, nullable = false)
    private String name;
    // Getters and setters
}
```

Dette resulterer i følgende genererte SQL:

```sql
Hibernate: drop table if exists tb_student cascade

Hibernate: create table tb_student (stud_id int4 not null, stud_name varchar(50) not null, primary key (stud_id))
```

> MERK : Den slettet ikke vår forrige studenttabellda den ikke er en del av vår nåværende konfigurasjon. Hibernate vil bare slette tabeller som er representert av enheter. Vi må slippe den tabellen manuelt.
> 

> MERK : Lengde-egenskapen til `@Columner` er direkte koblet til databasen. Det er andre merknader, `@Length` og `@Sizesom` er opptatt av validering. Du kan lære mer om forskjellene [her](https://www.baeldung.com/jpa-size-length-column-differences).
> 

### Automatisk inkrementering

Et annet aspekt å vurdere er håndtering av automatisk inkrementerte primærnøkler. Så langt er studentenheten konfigurert til å kreve innsetting av en primærnøkkel fra en klient (klassen som bruker enheten). Hvis **automatisk genererte** verdier ønskes, kan dette konfigureres ved å bruke `@GeneratedValue-`kommentaren. Denne merknaden har flere implementeringer (kalt strategier) som kan velges mellom:

- `AUTO` - Standard oppførsel , JPA bestemmer basert på leverandør.
- `IDENTITY` - er avhengig av kolonnen for automatisk økning av databasen. Databasen genererer primærnøkkelen etter hver innsettingsoperasjon. JPA tildeler primærnøkkelverdien etter å ha utført innsettingsoperasjonen eller ved transaksjonsbekreftelse.
- `SEQUENCE` - genererer primærnøkkelen ved hjelp av en databasesekvens. Vi må først lage en sekvens på databasesiden før vi bruker denne strategien.
- `TABLE`genererer primærnøkkelen fra en tabell og fungerer på samme måte uavhengig av den underliggende databasen. Vi må lage en generatortabell på databasesiden for å generere primærnøkkelen.

I praksis er `IDENTITY`den enkleste å bruke, siden den krever minst mulig konfigurasjon og hver tabell har sin egen teller som øker uavhengig. Kodebiten nedenfor inkluderer en generasjonsstrategi:

```java
@Entity
@Table(name = "tb_student")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "stud_id")
    private int id;
    @Column(name = "stud_name", length = 50, nullable = false)
    private String name;
    // Getters and setters
}
```

Dette resulterer i følgende SQL:

```sql
Hibernate: drop table if exists tb_student cascade

Hibernate: create table tb_student (stud_id  serial not null, stud_name varchar(50) not null, primary key (stud_id))
```

### Håndtering av dato og klokkeslett

Lagring av tidsdata (datoer og klokkeslett) kan noen ganger være en vanskelig oppgave . Heldigvis gir Hibernate en nyttig merknad for nøyaktig å kontrollere hvordan `java.util .Date` - typene skal lagres. `@Temporal` - kommentaren har flere alternativer:

- `DATE`
- `TIME`
- `TIMESTAMP`

Videre har Hibernate flere tilordninger for de nyere datetime-typene i Java som kartlegger til forskjellige tidstyper:

- `LocalDate`er kartlagt til `DATE`
- `LocalTime`og `OffsetTime`er kartlagt til `TIME`
- `Instant`, `LocalDateTime`, `OffsetDateTime`og `ZonedDateTime`er kartlagt til `TIMESTAMP`

> MERK : Dette betyr at du ikke trenger å inkludere `@Temporal`når du bruker disse Java-typene.
> 

La oss inkludere elevenes fødselsdato i enheten vår:

```java
@Entity
@Table(name = "tb_student")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "stud_id")
    private int id;
    @Column(name = "stud_name", length = 50, nullable = false)
    private String name;
    @Column(name = "date_of_birth", length = 15)
    private LocalDate birthday;
    @Enumerated(EnumType.STRING)
    private Gender gender;
    @Column(name = "stud_type", length = 15)
    @Enumerated(EnumType.STRING)
    private StudentType studentType;
    // Getters and setters
}
```

Dette resulterer i følgende SQL:

```sql
Hibernate: drop table if exists tb_student cascade

Hibernate: create table tb_student (stud_id  serial not null, date_of_birth date, stud_name varchar(50) not null, primary key (stud_id))
```

### “Enumerated” verdier

Noen ganger krever forretningslogikken bruk av “enumerators”. Husk at disse klassene har en numerisk og tekstlig representasjon. Hvis typen er spesifisert som enum , vet ikke Hibernate hvilken verdi som skal lagres, denne kan konfigureres med `@Enumerated` - kommentaren. Denne merknaden har to verdier: `STRING`(tekst) og `ORDINAL`(tall). Mesteparten av tiden er det fornuftig å lagre tekstverdien til en oppregnet verdi:

```java
@Entity
@Table(name = "tb_student")
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "stud_id")
    private int id;
    @Column(name = "stud_name", length = 50, nullable = false)
    private String name;
    @Column(name = "date_of_birth", length = 15)
    private LocalDate birthday;
    @Enumerated(EnumType.STRING)
    private Gender gender;
    @Column(name = "stud_type", length = 15)
    @Enumerated(EnumType.STRING)
    private StudentType studentType;
    // Getters and setters
}
```

Dette resulterer i følgende SQL:

```sql
Hibernate: drop table if exists tb_student cascade

Hibernate: create table tb_student (stud_id  serial not null, 
date_of_birth date, gender varchar(255), stud_name varchar(50) not null, 
stud_type varchar(15), primary key (stud_id))
```

Det siste aspektet ved Entity-konfigurasjon er ideen om **forbigående felt** . Enkelt sagt, dette er felt i enheten som ikke bør vedvares. De er ikke veldig vanlige, og brukes normalt til forretningslogikk knyttet til beregnede felt. For å stoppe et felt fra å vedvare ( bli en kolonne i en tabell), merk det med `@Transient`.

## Entitetsforhold

Husk at tabeller har relasjoner for å representere domenet de modellerer. Disse er:

- En til en
- En til mange
- Mange til mange

I Java har vi gjennom OOP muligheten til *komponere* objekter, eller samlinger av objekter inn i hverandre. Dette tillater modellering av relasjoner gjennom Hibernates Entities. De oppgitte merknadene for å representere disse relasjonene gir en enkel og intuitiv måte å håndtere relaterte enheter på.

Ethvert forhold har i sin kjerne:

- En retning
- En eier

For **retning** ; et forhold kan enten være *uni-* eller *toveis* . For det meste er toveis ideell da den lar oss navigere fra begge sider.

> MERK : Retningen til forholdet har ingen betydning for den resulterende tabellen. Den brukes utelukkende i Hibernates-”persistence context” for å kontrollere navigasjonen.
> 

For **eierskap** er dette direkte oversatt fra *avhengig tabell* i relasjonsdatabaser. Husk at eieren av forholdet har *fremmednøkkelen* . For en-til-mange er dette alltid på mange - siden , men for en-til-en kan det være enten. Hvordan eierskap gjenspeiles i praksis med Hibernate er veldig direkte - du kan bare legge til/oppdatere relaterte enheter fra eiersiden. Dette er noe som vil bli oppdaget i en senere leksjon når du håndterer datatilgang med Hibernate.

> MERK : All relasjonskonfigurasjon vil bli gjort med de enklest mulige enhetene for å fokusere på å forstå relasjonene selv.
> 

### En til en

For en en-til-en-relasjon gjenskapes student-prosjekt-relasjonen ved hjelp av Entities. De enkelte enhetene har følgende struktur:

```java
@Entity
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 50, nullable = false)
    private String name;
}
```

```java
@Entity
public class Project {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(nullable = false, length = 100)
    private String title;
}
```

For å skape et forhold mellom disse to enhetene, må vi:

- Legge til relevante komposisjoner
- Legge til fremmednøkkel (via `@JoinColumn`)
- Aktiver toveis (via `mapdBy`)

Dette resulterer i at enhetene inneholder følgende:

```java
@Entity
public class Student {
    @Id
    private int id;
    @Column(length = 50, nullable = false)
    private String name;
    @OneToOne
    @JoinColumn(name = "project_id")
    private Project project;
}
```

Fremmednøkkelen konfigureres ved å `angi`navnegenskapen til `@JoinColumn`. Dette resulterer i at en fremmednøkkelkolonne av samme datatype som `@Id`of `Project`opprettes med navnet `project_id`. Selve `prosjektet`er ikke tilordnet databasen da det ikke er en `@Basic-`type og derfor er forbigående som standard. `Project` feltet , sammen med getter og setter, brukes til navigering og endringssporing innenfor Hibernates utholdenhet.

Ser på `prosjekt`enheten:

```java
@Entity
public class Project {
    @Id
    private int id;
    @Column(nullable = false, length = 100)
    private String title;
    @OneToOne(mappedBy = "project")
    private Student student;
}
```

Vi ser at det er en `mapdBy`egenskap definert. Denne verdien må være navnet på feltet i `Student` enheten som refererer til `prosjektet`. Hibernate løser dette ved å bruke **refleksjon** , faktisk er det meste av det Hibernate (eller en hvilken som helst ORM) gjør ved refleksjon. Dette er grunnen til at konfigurasjon er så viktig når du arbeider med disse verktøyene. Denne `mapdBy`verdien aktiverer toveisrelasjonen. Uten den kunne vi ikke gå fra `prosjekt`til `student`, bare fra `student`til `prosjekt`.

Når applikasjonen kjøres, genereres følgende SQL:

```sql
Hibernate: drop table if exists project cascade
Hibernate: drop table if exists student cascade

Hibernate: create table project (id int4 serial not null, title varchar(100) not null, primary key (id))

Hibernate: create table student (id int4 serial not null, name varchar(50) not null, project_id int4, primary key (id))

Hibernate: alter table if exists student add constraint FKr6av6arpoy7wpbqipg41id1mn foreign key (project_id) references project
```

> MERK : For å spare tid vil ikke konseptene som er omtalt i denne underseksjonen bli gjentatt i samme detalj fremover. Dessuten vil bare den relevante genererte SQL-en vises.
> 

### En til mange

For en en-til-mange-relasjon må vi ha den fremmede nøkkelen i mange- siden , som kreves av relasjonsdesign. For eksempel vil vi gjenskape professor-student-forholdet sett før:

```java
@Entity
public class Professor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 50, nullable = false)
    private String name;
    @OneToMany(mappedBy = "professor")
    private Set<Student> students;
}
```

Samlingen av `studenter`for en `professor`er representert som et `sett`fordi vi vet at den ikke skal ha noen duplikater. Dette gjør det også lettere å jobbe med når du gjør datatilgang i en senere leksjon.

```java
@Entity
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 50, nullable = false)
    private String name;
    @OneToOne
    @JoinColumn(name = "project_id")
    private Project project;
    @ManyToOne
    @JoinColumn(name = "professor_id")
    private Professor professor;
}
```

Legg merke til `@ManyToOne`syntaksen, siden dette er en viktig merknad, uten den ville Hibernate måtte opprette flere, uventede tabeller eller relasjoner for å fungere for konfigurasjonen.

SQL generert når du kjører applikasjonen er som følger:

```sql
Hibernate: create table professor (id  serial not null, name varchar(50) not null, primary key (id))

Hibernate: create table student (id  serial not null, name varchar(50) not null, professor_id int4, project_id int4, primary key (id))

Hibernate: alter table if exists student add constraint FK6qd34ryg9xge96upkktpry261 foreign key (professor_id) references professor
```

### Mange til mange

Det siste forholdet å vurdere er mange-til-mange. Dette forholdet kan ikke modelleres som det er i en ekte database. Husk at vi trenger en **koblingstabell** . Vi har noen alternativer i forhold til dette, enten:

- Opprett koblingstabellen som en enhet
- La Hibernate håndtere det

Mesteparten av tiden er det beste alternativet å la Hibernate håndtere det. Siden tabellene vanligvis kun er for å fange forholdet. De gangene det ikke er aktuelt er når du trenger å fange opp ytterligere informasjon om forholdet.

Tar man elev-fag-forholdet, når det tidligere ble demonstrert, var det utelukkende for å fange opp forholdet. Men hvis *påmeldingsdatoen* eller *gjeldende karakter* må beholdes, må en egen enhet defineres. Denne enheten vil da ha to en-til-mange-relasjoner til både `elev`og `fag`. Opprettelsen av den sammensatte primærnøkkelen er også triviell - bare ha to felt merket med `@Id`.

For våre formål er det imidlertid demonstrert en ren koblingstabell for å vise hva Hibernate kan gjøre. For eksempel, hvis vi endrer student og oppretter emne på følgende måte:

```java
@Entity
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 50, nullable = false)
    private String name;
    @OneToOne
    @JoinColumn(name = "project_id")
    private Project project;
    @ManyToOne
    @JoinColumn(name = "professor_id")
    private Professor professor;
    @ManyToMany
    private Set<Subject> subjects;
}
```

```java
@Entity
public class Subject {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 6, nullable = false)
    private String code;
    @Column(length = 100, nullable = false)
    private String title;
    @ManyToOne
    @JoinColumn(name = "professor_id")
    private Professor professor;
    @ManyToMany(mappedBy = "subjects")
    private Set<Student> students;
}
```

Når applikasjonen kjøres, genereres følgende SQL:

Når applikasjonen kjøres, genereres følgende SQL:

```sql
Hibernate: create table student (id  serial not null, name varchar(50) not null, professor_id int4, project_id int4, primary key (id))

Hibernate: create table student_subjects (students_id int4 not null, subjects_id int4 not null, primary key (students_id, subjects_id))

Hibernate: create table subject (id  serial not null, code varchar(6) not null, title varchar(100) not null, professor_id int4, primary key (id))

Hibernate: alter table if exists student_subjects add constraint FKhdmqwtnox9c02j4jupvpvoh5u foreign key (subjects_id) references subject
Hibernate: alter table if exists student_subjects add constraint FKn2wd1se568760orjgn9txveuw foreign key (students_id) references student
```

Legg merke til her at det ble opprettet en tabell som vi ikke hadde som entitet: `student_subjects`. Videre har den en sammensatt primærnøkkel `primærnøkkel ( student_id , subjects_id )`som også hver er en fremmednøkkel. Husk at dette er nøyaktig hvordan vi håndterte opprettelsen av en koblingstabell før.

Et annet aspekt å merke seg er navnene på tabellen/kolonnene. Legg merke til hvordan refleksjon brukes til å lage tabellnavnet; ved å kombinere eiertabellnavnet med feltnavnet som refererer til den andre tabellen. Primærnøklene lages ved å bruke feltnavnet som er knyttet til `Sets`.

> MERK : Refleksjon er grunnen til at vi ender opp med `student+subjects`(se hvor `mappedBy` er i enhetene våre) samt `student+id` og `subjects+id`.
> 

Nå kan dette også tilpasses hvis du ikke liker de oppgitte navnene. Dette gjøres via en `@JoinTable`kommentar. Dette må plasseres i den eierende enheten (den der `mapdBy` ikke er konfigurert), dette gjenspeiler perfekt hvordan vi konfigurerte de andre relasjonene med `@JoinColumn` . Å oppdatere konfigurasjonen vår for å gjenspeile en tilpasset sammenføyningstabell krevde følgende modifikasjoner:

```java
@Entity
public class Student {
    // Ommitted for brevity
    @ManyToMany
    @JoinTable(
            name = "student_subject",
            joinColumns = {@JoinColumn(name = "student_id")},
            inverseJoinColumns = {@JoinColumn(name = "subject_id")}
    )
    private Set<Subject> subjects;
}
```

`name`egenskapen lar oss definere et navn for den genererte koblingstabellen. Egenskapen `joinColumns`tillater tilpasning av fremmednøkkelen som representerer gjeldende tabell (student). Egenskapen `inverseJoinColumns`tillater tilpasning av fremmednøkkelen som representerer tabellen på den andre siden (subject). Når applikasjonen kjøres, genereres følgende SQL:

```sql
Hibernate: create table student_subject (student_id int4 not null, subject_id int4 not null, primary key (student_id, subject_id))
```

Som man kan se, overstyrer girkonfigurasjonene det som antas via refleksjon. Til nå har vi bare undersøkt hva som skjer når enheter er riktig konfigurert. Den neste underdelen viser hva som kan gå galt, hvordan du oppdager det og hvordan du fikser det.

### Feilkonfigurasjoner

Denne underdelen viser ved eksempel hva som kan gå galt når du konfigurerer en database via Hibernate-enheter. Dette er i et forsøk på å illustrere vanlige problemer og at man må være på vakt.

En svært vanlig feil er å utelate `mapdBy`når du konfigurerer et toveis forhold. For å illustrere dette, la oss ta den forrige til mange mellom professor og student og fjerne `mapdBy`og se hva som genereres:

```java
@Entity
public class Professor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(length = 50, nullable = false)
    private String name;
    @OneToMany // mappedBy removed
    private Set<Student> students;
    @OneToMany(mappedBy = "professor")
    private Set<Subject> subjects;
}
```

Dette resulterer i følgende SQL:

```sql
Hibernate: create table professor (id  serial not null, name varchar(50) not null, primary key (id))

Hibernate: create table professor_students (professor_id int4 not null, students_id int4 not null, primary key (professor_id, students_id))

Hibernate: create table student (id  serial not null, name varchar(50) not null, professor_id int4, project_id int4, primary key (id))
```

Legg merke til hva som har skjedd her, en `professor_students`-tabell har blitt generert for å håndtere One to Many - forholdet. For å fikse dette, trenger vi bare å slette tabellen manuelt og legge til `mapdBy`der den er ment å være. Dette resultatet sees også når `mapdBy` utelates for noen av de konfigurerbare relasjonene.

> MERK : Ikke vær redd for å slette hele databasen og deretter kjøre den på nytt. create gjør dette uansett, men vil la alle tabeller som ikke er konfigurert som enheter alene.
> 

Den neste vanlige feilen er å utelate `@JoinColumn`når det er nødvendig. Dette resulterer imidlertid ikke i en ekstra generert tabell. Som oftest fungerer dette helt greit. Som refleksjon er på spill for å gjøre antakelser. Det anbefales fortsatt ikke å utelate disse, da navngivningen av feltene er svært viktig for å stole helt på forutsetninger.

## Seeding

Seeded data er de første dataene som finnes i en database. Den konfigureres vanligvis når en databasestruktur er bestemt. Det er mange måter å så data på med Spring:

- `data`og `schema` skript
- tilpassede skript
- `ApplicationRunner`/ `CommandLineRunner`

`ApplicationRunner`/ `CommandLineRunner`brukes svært sjelden i et virkelig scenario da de krever bruk av JPA Repositories. Det er også langt enklere å bare skrive SQL-setningene selv.

En veldig vanlig seedingprosess involverer bruk av `data.sql`og `schema.sql`. Forklarelse hva hver av disse filene gjør:

- `schema.sql`lastes inn for å generere databasetabellene
- `data.sql`lastes inn for å sette inn data i tabellene

> MERK : Disse to filene og deres funksjonalitet er knyttet til Spring Boot og ikke Hibernate . Hibernate har en lignende prosess via en `import.sql` fil. Det er imidlertid ikke anbefalt å bruke det i et Spring Boot-prosjekt, og favoriserer bruken av `data.sql`.
> 

> MERK : `schema.sql` og `data.sql`er plassert i ressursmappen, ved siden av `application.properties`.
> 

Å ta en titt på disse to filene kan gi et enkelt spørsmål:

*Hvis Hibernate og schema.sql begge er ment å generere databasen, hvilken blir utført?*

Svaret på det er avhengig av konfigurasjon, hvis de begge ble stående alene ville det oppstå en konflikt. Spring Boot har imidlertid flere egenskaper som hjelper til med å kontrollere denne generasjonen.

Vanligvis, i et **produksjonsmiljø** , er Hiberntate- generering slått av via:

```
spring.jpa.hibernate.ddl-auto= none
```

Dette gjør at `schema.sql`kan kjøres uten forstyrrelser. `schema.sql`vil fungere som en **oppdatering** til det eksisterende skjemaet, i likhet med Hibernate’s `update`konfigurasjonen.

Men hvis du ønsker at Hibernate skal fungere sammen med `schema.sql`, kan du ganske enkelt legge til linjen:

```
spring.jpa.defer-datasource-initialization= true
```

`schema.sql`leses for eventuelle ytterligere endringer etter at databaseopprettingen er utført . Deretter `kjøres data.sql`for å fylle databasen.

> MERK : Egenskapen ovenfor gjør at Hibernate kan konfigureres med en annen `ddl -auto`-verdi enn `none` og fortsatt fungere. Til tider ønskes dette ved utvikling/testing.
> 

Databasepopulasjon ( `data.sql`) lastes bare inn som standard når du bruker innebygde databaser (som H2) da de vanligvis bare er i live så lenge applikasjonen kjører. For å kjøre `data.sql`på en ikke-innebygd database, må vi legge til følgende konfigurasjon:

```
spring.sql.init.mode= always
```

> MERK : De andre alternativene er innebygd(standard) og aldri.
> 

> DOKUMENTASJON : Databasegenereringsstrategier for Spring Boot finner du her .
> 

Copyright 2022, Noroff Accelerate AS