# Kursoversikt

# Kursoversikt

Dette kurset tar sikte på å lære deg mer om moderne programvareutvikling, med bruk av Java som base. Konseptene i dette kurset er ikke begrenset til Java-økosystemet, og gjelder uansett språk.

Etter å ha fullført kurset vil du ha grunnleggende kunnskap om viktige aspekter innen moderne programvareutvikling, som kan bygges videre på.

## Aspekter ved programvareutvikling

De neste seksjonene beskriver de ulike aspektene ved programvareutvikling på et detaljert nivå.

### Programvare

Programvare (Software) er et system eller sett med programmer som inneholder instruksjoner for å gi ønsket funksjonalitet. *Engineering* er prosessen med å designe og bygge noe som tjener et bestemt formål og finner en kostnadseffektiv løsning på problemet.

> Programvare = programmer + dokumentasjon + lisensiering
> 

Programvareutvikling (eng. Software Engineering) per definisjon er en systematisk, disiplinert, kvantifiserbar studie og tilnærming til design, utvikling, drift og vedlikehold av et programvaresystem. Programvare fungerer som både produktet og kjøretøyet for å levere produktet.

Designet og arkitekturen til et programvareprodukt bør lages på en måte som gjør at produktet kan være: vedlikeholdbart, effektivt, korrekt, gjenbrukbart, testbart, pålitelig, portabelt, tilpasningsdyktig og interoperatibelt.

Programvareutvikling har endret seg i stort tempo for å tilpasses moderne krav og utviklende teknologier. Den største innvirkningen skjedde ved at agile-metoder ble tatt i bruk.

### Agile

Agile er en prosjektledelsesmetodikk som har erstattet den tradisjonelle Waterfall-metodikken. Med Waterfall måtte alt planlegges fullstendig fra starten med en antagelse om at det er korrekt. Produktet ble deretter utviklet over en lenger periode på sekvensielt vis.

Agile er en tidsbundet, iterativ tilnærming til programvareleveranse som bygger programvare trinnvis fra starten av prosjektet, i stedet for å prøve å levere alt samtidig. Dette skiftet gir mulighet for rask iterasjon i små sykluser og muligheten til å endre krav uten stor innvirkning på resten av systemet. Det favoriserer en høy grad av modularitet, fleksibilitet og testbarhet.

Et prosjekt utviklet med Agile må ha god design og arkitektur fra start, ellers er det dømt til å mislykkes. Dette tvinger team til å vurdere design og krav samtidig, og ikke separat.

### Shift Left and DevOps

Prinsippet bak Shift Left er å ta en oppgave som vanligvis håndteres på et senere stadium av prosessen og utføre den på tidligere stadier. Et eksempel er testing. Shift Left Testing involverer testing tidlig og involverer testing- og QA-team i prosessen helt fra start. Dette identifiserer problemer og flaskehalser tidlig i prosessen slik at endringer kan skje uten press fra en tidsfrist. Dette fremmer hyppigere utgivelser av høyere kvalitet.

En viktig faktor for å bruke Shift Left er automatisering. Automatisering er avgjørende og muliggjør Continual Integration (CI). Dette reduserer menneskelige feil, øker testdekningen og lar testerne fokusere på mer inspirerende oppgaver. Testautomatisering og testdrevet utvikling sammenfaller perfekt med Shift Left, men det er ikke begrenset til dette. Shift Left omfatter også sikkerhet og distribusjon.

Fra et DevOps-perspektiv er Shift Left en tilnærming for å forbedre samarbeid og kommunikasjon ved å engasjere parter tidligere i livssyklusen. Med Continuous Deployment (CD) er distribusjons-pipelinen fullstendig automatisert, slik at interessenter kan bli involvert tidlig og gi tilbakemeldinger av god kvalitet.

Shift Left presser også utviklere til å bli enige om en felles visjon for produktet, som gjør det mer sannsynlig at man tilpasser seg forretningsbehovene.

## Konsepter og verktøy.

I løpet av kurset vil følgende IT-verktøy og konsepter dekkes:

- Git med Gitlab
- Agil utvikling
- Java 17
- Testing med JUnit 5
- SQL med Postgres
- Spring Framework, Spring Data, og Spring Security
- Domenedrevet Design
- Docker og Docker-compose
- CI/CD (DevOps) med Gitlab-ci
- Cloud-tjenester med Heroku
- OAuth2.0 og OIDC
- Identity Providers med Keycloak

## Moduler

Kurset er delt opp i fire moduler. Hver modul tar for seg et eget felt innen programvareutvikling.

### Modul 1

Den første kursmodulen fokuserer på å bli kjent med det grunnleggende innenfor kodespråket Java. Det vil også være fokus på hva som er god programvaredesign. God programvaredesign handler om å utforme etter SOLID prinsipper, rollen til designmønstre og hvordan skape testbar kode. Automasjon vil også introduseres gjennom Gitlab-CI for automatisering av tester og generering av rapporter.

Denne modulen vil også ta for seg gode vaner for å jobbe med git. Disse inkluderer: korrekt dokumentasjon, presise commit-meldinger, god branching, sporing av feil, og milepæler. Dette vil være gjennomgående for hele kurset.

### Modul 2

Den neste modulen fokuserer på persistens og hvordan det håndteres. Modulen starter med å ta for seg SQL innenfor Postgres-dialekten på en Postgres server som blir styrt av PgAdmin. JDBC blir deretter brukt til å sende meldinger til en database slik at man kan kommunisere mellom den og Java.  Domenedrevet Design blir deretter introdusert for å organisere datatilgangen vår til et “repository pattern” (mønster for programvarelager). 

Spring Data introduseres deretter med fokus på JPA og Hibernate. Man vil lære hvordan Hibernate og seed data konfigureres ved å bruke Spring Data. JPA-repoer opprettes deretter for å bruke Hibernate effektivt.

### Modul 3

Den tredje modulen i kurset tar for seg webserverutvikling. Først med HTTP og REST for å forstå hvordan utdataene til en webserver skal se ut. Deretter brukes Spring Web til å lage REST APIer i Java med fokus på beste praksis. Videre integreres Spring Data med Spring Web for å tilby et komplett produkt.

Dataoverføringsobjekter vil introduseres for å hjelpe til med å skille domenet fra klienter. Du vil så lære hvordan lage dokumentasjon for et REST API ved å bruke OpenAPI/Swagger. Det siste emnet som dekkes i denne modulen er Docker. Docker vil benyttes til å distribuere applikasjoner til en Heroku-applikasjon.

### Modul 4

Den siste modulen i kurset dekker mer avanserte deployment-strategier (produksjonssettingsstrategier) og sikkerhet. Dere vil lære om forskjellige konfigureringer for forskjellige deler (utvikling, staging, og produksjon) av et Spring prosjekt ved å bruke Spring sin eksterne konfigurasjon. Dere vil også lære å replisere produksjonsmiljøene med Docker Compose. 

Avslutningsvis vil man lære å lage en fullstendig CI/CD-pipeline, slik at det kan lages et artefakt, som deretter plasseres i et register og deployes til et produksjonsmiljø.

## Veien videre

Ved fullført kurs, vil man ha essensiell kunnskap til mange ulike aspekter av moderne programvareutvikling. Hva man vil velge å lære videre er opp til den enkelte. Vi anbefaler imidlertid å fortsette videre med Microservices og DevSecops

## Generell informasjon

Forelesninger i dette kurset sørger for innholdet som trengs for å oppnå relevante læringsmål som blir gått gjennom i starten av hver forelesning. Det er imidlertid visse emner som kan være interessante å lære mer om via selvstudier. Referanser til passende ressurser for disse emnene vil gis.

Offisiell dokumentasjon vil ta presedens ettersom utviklere bør være vant til å benytte offisiell dokumentasjon for å holde seg oppdatert.

> Blokker som ser slik ut inneholder ekstra informasjon eller hint som kan være lurt å få med seg. De kan også inneholde lenker til ytterligere informasjon å lese på. Under ser dere eksempler på hvordan blokkene er laget.
> 

> **LES** Indikerer en artikkel eller en annen type litteratur som **må** leses.
> 

> **DOKUMENTASJON** Indikerer en lenke til offisiell dokumentasjon.
> 

> **RESSURS** Indikerer en link til en ressurs som er utarbeidet av Noroff Accelerate, typisk i form av et prosjekt i en ekstern Git lagrignsplass. Disse kan du laste ned og utforske nærmere. Dette anbefaler vi for å øke kunnskapen din.
> 

> **SELVSTUDIE** Det vil fra tid til annen komme relevante temaet som ikke er relevant for kurset, men som det kan være lurt å undersøke på egenhånd. Vi anbefaler dere å sette av litt av deres egen tid til å utforske disse linkene og temaene.
> 

> **INFO** Indikerer et viktig notat eller hint.
> 

> **ADVARSEL** Indikerer et viktig poeng som må forstås for å unngå utilsiktede effekter om det ikke tas hensyn til
> 

---

Copyright 2022, Noroff Accelerate AS