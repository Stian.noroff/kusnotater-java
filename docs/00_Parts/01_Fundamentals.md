# Java Fundamentals

This module covers all the information needed to understand how to use Java at a fundamental level. This module is not an in depth look at individual aspects, but more of an overview of the existing technology, how it can be used, and its importance in industry.

This topics covered in this module are:

- Datatypes
- Data structures
- Control structures
- String manipulation
- Exception handling
- Unit testing
- Object-oriented design best practices
- I/O in Java
- Stream API
- Lambda functions

## Lessons

### 1 - Essential Java Features Part 1

### 2 - Essential Java Part Features 2

### 3 - Objektorientert programmering

### 4 - Enhetstesting

### 5 - Objektorientert Design

### 6 - Prominente Java-funksjoner

---

Copyright 2022, Noroff Accelerate AS
