# Thymeleaf
This lesson covers Thymeleaf a commonly used templating engine used in Java applications. This allows a front end to be created with Java using HTML templates which are served to clients based on requests. This lesson covers the process of taking data from a data store, processing it into a model, and passing that model to a templating engine to create a visual representation of the data. The process of capturing user information and using it within a Java application is also covered. 

**Learning objectives:**

You will be able to...
- **explain** what a templating engine is.
- **discuss** Thymeleaf's role in Java development.
- **contrast** Thymeleaf with client-side Javascript frameworks.
- **modify** given html pages to represent model data throught the **integration** of Thymeleaf elements.
- **create** a form in Thymeleaf to capture information.

---


## Templating Engines

When developing full-stack applications, reusing the static segments of our websites and simultaneously designating those which are dynamically built based on request parameters is a common task. Templating engines such as Thymeleaf are used to accomplish this.

Thymeleaf serves HTML views to the client, adhering to the principle of `Natural Templates`. These function as both static prototypes or containers for any dynamic content which we would like to embed in our views.

It is one of the most well-established templating engines used within the Spring Framework, offering excellent integration with existing functionality.

## Getting started

Using Spring Initializr, create a new Spring Boot application folder with the Web and Thymeleaf libraries added as dependencies.

![Thymeleaf Dependency](./resources/01_ThymeleafDepend.PNG)

Once you have opened the generated project in IntelliJ, create a new package called `controllers` as we did in previous lessons. Now you are ready to start making a Thymeleaf applicaiton.

## Important folders

**Resources.static**

This is where our persistent resources will be, these can be css, js, or any other file we want to use as a permanent resource. They can be accessed very easilty through Thymeleaf in this folder.

**Resources.templates**

This is where all our Thymeleaf templates will live. These files need unique names, as the Thymeleaf engine will scrape through this folder when trying ot find a match. These files can be numerous file types, but for our concerns, they willbe HTML 5 files. 

## Controllers

As with previous lessons where we called database manipulation classes from the controller and return it as JSON, with Thymeleaf the information is returned as a `model` in a Thymeleaf template. 

> Controllers used for Thymeleaf projects are @Controller NOT @RestController

To showcase how Thymeleaf uses controllers: add a new class to the _controllers_ package called `GreetingController.java`. Edit this class so that it contains the following code:

```java
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.ui.Model; //You may nee to add this manually

@Controller
public class GreetingController {
    @GetMapping("/greeting")
    public String greeting(@RequestParam(name = "name", required = false, defaultValue = "World") String name, Model model) {
        model.addAttribute("name", name);
        return "greeting";
    }
}

```

> This example uses a request parameter for simplicity

> Take note of the `Model` parameter, this is what is passed to the Thymeleaf template.

Here we can bind specific attributes to our _model_, which will be displayed in our _view_. In this case, we are binding a request parameter named `name` to a _model attribute_. Finally, we return a view called `greeting` back to the client. Spring Boot will automatically search our _resources/templates_ folder for this view.

## Using the template

Now that there is a _controller_ calling a _template_ and there is a _model_ with some _attributes_, we can begin templating out that information.

Add the following HTML file named `greeting.html` to the `resources/templates` folder.

```html
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
  <head>
    <meta charset="UTF-8" />
    <title>Hello Controller</title>
  </head>
  <body>
    <p th:text="'Hello '+ ${name} + '!'" />
  </body>
</html>
```
Some things to note:

- Thymeleaf requires the `xmlns:th="http://www.thymeleaf.org"` attribute embedded in the html element of our webpage in order to function.

- The `th:text` attribute, which evaluates its value expression and sets the result of this evaluation as the body of the tag it is in, replaces the "name" placeholder.

- When accessing the endpoint that we have just created, if we have supplied the `name` parameter, it will be displayed to the browser. Otherwise, the default value `World` will be displayed.

## Adding resources

Recall that the resources/static folder can easily be accessed and that we place static resources in there. We can then use this in our tempalates with ease:

```html
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
<link rel="stylesheet" th:href="@{/css/bootstrap.min.css}">
<link rel="stylesheet" th:href="@{/css/main.css}">
<script th:src="@{/js/bootstrap.min.js}" ></script>
<head>
    <meta charset="UTF-8">
    <title>Welcome</title>
</head>
<body>
...
```

This will import these resources into our template so we can use them.

> You can still link to a CDN as usual with Thymeleaf.

## Directives

This is where the power of templating engines comes to the forefront. We can render html conditionally based on some logic from our model data.

> This behaviour is done by all modern JS frameworks, but Thymeleaf was around much earlier.

There are several directives available throught Thymeleaf, we will look at the following:
- **th:each** - for loops
- **th:if** - boolean logic
- **th:unless** - boolean logic
- **th:switch** and **th:case** - emulating a swithc statement.

## Using directives

For this example, we will need some actual information. Leaning heavily on the past lessons, we reuse the customer repositories and Northwind database. 

We will be creating a view where we can see the information about all the customers. To do this we need to create a `CustomerController` with the following method:

> This example assumes you have the codebase from the SQL to REST example and are reusing the `CustomerRepository` class.

```java
@Controller
public class CustomerController {
 /*
  "GET /"
  "GET /customers"
 */

 CustomerRepository crep = new CustomerRepository();
  @GetMapping("/")
    public String home(){
      return "index";
    }

  @GetMapping("/customers")
  public String getAllCustomers(Model model){
    model.addAttribute("customers", crep.getAllCustomers());
    return "view-customers";
  }
```

Now we need to create a Thymeleaf template to view this information. In this case we need to create the `view-customers.html` file.

> Styling and web semantics is ommited for brevity.

```html
<!DOCTYPE html>
<html lang="en" xmlns:th="http://www.thymeleaf.org">
<link rel="stylesheet" th:href="@{/css/bootstrap.min.css}">
<link rel="stylesheet" th:href="@{/css/main.css}">
<script th:src="@{/js/bootstrap.min.js}" ></script>
<head>
    <meta charset="UTF-8">
    <title>View customers</title>
</head>
<body>
<nav>
  ...
</nav>

<h1>Existing information</h1>
<table>
  <thead>
    <tr>
      <th>Customer</th>
      <th>View</th>
    </tr>
  </thead>
  <tbody>
    <tr th:each="customer: ${customers}">
      <td th:text="${customer.getContactName()}"></td>
      <td>
        <a th:href="'/customers/' + ${customer.getCustomerId()}">View</a>
      </td>
    </tr>
  </tbody>
</table>
```

In this view, a simple table is created where a list is printed of all the elemtents in the `customers` list. We can refer to customers as such, because we added the list as the _attribute_ `customers`. Now we can utilize any functionality the `Customer` class has, in this case, getters.

## Capturing information

Captuing information to be used with a post is normally done in a `Html form`. Thymeleaf has support for this throught the **th:action**, **th:object**, and **th:field** tags which we place in the form.

To accomplish this functionality, we need to do some alterations to our controller. As a form has a submit button, or any way to post the form data, we need a _POST_ mapping to cater for this. 

We will create a _GET mapping_ called `/customers/register` and a _POST_ mapping called `/customers/register` as well. They have the same name by convention, as it is then easy to track which posts match which gets for forms. 

Looking at these methods implemented in our controller:

```java
@GetMapping("/customers/register")
public String showAddCustomer(Model model){
  model.addAttribute("customer", new Customer());
  return "add-customers";
}

@PostMapping("/customers/register")
public String handleAddCustomer(@ModelAttribute Customer customer, BindingResult errors, Model model){
  boolean success = crep.addCustomer(customer);
  if(success){
    model.addAttribute("customer", new Customer());
  } else {
    model.addAttribute("customer", customer);
  }
  model.addAttribute("success", success);
  return "add-customers";
}
```

Looking at the GET mapping, we see we create an empty customer to be filled in by form data and pass that to the `add-customer.html` view. This view contains the form, and once submitted, now his the POST mapping.

Some additions in the parameters are: `@ModelAttribute Customer customer` and `BindingResult errors`. The _ModelAttribute_ is the actual form data we have entered and the _BindingResult_ is any errors that happened while trying to bind the entered values to the _Customer_ object (incorrect data types).

Once we enter the POST mapping, the new customer is sent off to the `CustomerRepository` to be added, if it succeeds, then an empty `Customer` obejct is added to the same model attribute - essentially clearing its data - and a success flag is added. If it fails, the customer is returned with the failed flag. The success flag is used in the add-customer-html to display an error to the user (this is very basic front end error handling, but front end is not the focus of this course and the basics will suffice.)

> Important to note here, these two controller methods use the SAME view for both operations. 

Having a look at the add-customer.html view shows how this is utilized in a Thymeleaf template.

> Styling has been ommited for brevity

```html
<div th:if="${success}">
  <strong>Success!</strong> The new customer was added.
</div>

<div th:if="${success} == false">
  <strong>Oops!</strong> Something has gone wrong :(
</div>

<h1>New customer</h1>
<form action="#" th:action="@{/customers/register}" th:object="${customer}" method="post">
  <label for="inputId">Identification code</label>
  <input type="text" id="inputId" th:field="*{customerId}" required />
    <small id="idHelp">This must be unique</small>

  <label for="inputCompany">Company name</label>
  <input type="text"id="inputCompany" th:field="*{companyName}" required />
    <small id="companyHelp">Who do you work for?</small>

  <label for="inputContact">Contact name</label>
  <input type="text"id="inputContact" th:field="*{contactName}" required />
    <small id="contactHelp">What's your name?</small>

  <label for="inputPhone">Phone</label>
  <input type="text" id="inputPhone" th:field="*{phone}" required />
    <small id="phoneHelp">Include your international code (e.g. +47)</small>

  <button type="submit">Add</button>
</form>
```

Here we see how the success flag is used, if it has a value of _null_, as it would initially, it would simply display no message. The information is captured in the `<input>` tags and assigned with the `th:field` by Thymeleaf. When the submit button is pressed, the `th:action="@{/customers/register}"` is called and the `/customers/register` mapping is requested as a _POST_.

> A full repository with the example used in this lesson can be found [here](https://gitlab.com/noroff-accelerate/java/projects/oslo-class-exmaples/thymeleaf).

---

Copyright 2021, Noroff Accelerate AS