# Working with Hibernate
This lesson covers the practical use of Hibernate. This includes performing CRUD operations on various entities and introducing the concept of services, the use of Data Transfer Objects (DTOs), and formatting response data using annotation from the Jackson JSON processing library.

**Learning objectives:**

You will be able to...
- **create** repositories representing the entities, given an existing schema and entities.
- **create** REST endpoints to manipualte data, given repositories.
- **describe** what a service is and describe its role.
- **describe** a DTO and discuss its role.
- **demonstrate** the use of services and DTOs to modify controllers, given existing controllers and repositories.
- **dicuss** the role of Jackson databind library.
- **apply** Jackson annotations to **modify** the format of returned information, given a set of entities.

---

## Repositories
 
The real power of the Spring Data JPA come from the use of `JpaRepositories`. We create one `JpaRepository` of these for each `@Entity` and we must match a specific interface signature:

For the `Book` example we specify the Repository type to be `<Book, Long>`, that is `Book` entities are identified by an `Long` value. Within the `BookRepository` we need only specify the _signature_ of a function that would logically find an individual `Book`.

```java
package no.noroff.HibernateLibrary.repositories;

import no.noroff.HibernateLibrary.models.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookRepository extends JpaRepository<Book,Long> {

}
```

> This interface is empty, the functionality come through inheritence.

This extension of the `JpaRepository` will create the `CRUD` commands that we would normally have had to write ourselves.

> You could also make a `CRUDRepository`, it is one level up from JPA repository in the heirarchy and offers the same functionality. CRUDRepository -> JpaRepository -> BookRepository

The `@Repository` annotation tells hibernate this is a repository and makes it available to the IOC (dependency injection) and can be injected into controllers. Meaning you don't have to create a new repository, you just ask for it and hibernate will pass it to you.

> You could also not make repositories at all and simply use `Context`. However, we are using repositories because it affords great grouping of logic.

Through this inheritance we have access to the following methods:
- save()
- findOne(id)
- findAll()
- count()
- delete(entity)
- exists(id)

We can leverage these methods to achieve just about any functional requirement needed. For the few times these do not satisfy your needs (could be custom, domain-related queries), you can make your own.


**Adding custom methods**

We can use the same syntax Hibernate uses to generate SQL, to create our own custom queries. This is done inside of the repository interfaces you make as extra methods. This will add to Hibernates abilities and you still will not have to write any SQL. These are fairly limited, but still powerful.

```java
@Repository
public interface AuthorRepository extends JpaRepository<Author, Long> {
    Author getByFirstName(String firstName);
    Author findByFirstNameOrDobAfterOrderByLastNameDesc(String firstName, Date dob);
}
```

You can keep typing and _Spring Data_ will suggest more commands, the first one created is a simple "find by name", the second adds a lot more. You find by name or after a certain date of birth and its ordered by last name descending. Notice no SQL was written. There are far more ways you can create these custom [query methods](https://docs.spring.io/spring-data/data-jpa/docs/current/reference/html/#jpa.query-methods.query-creation).


> You can actually write SQL if you want using `@Query`, `@NamedQuery` or using the `Querydsl` language. This is up to you to self-study if you want to try it out.

## Improved controllers

So far, we have created our controllers to serve the purpose we need them for and nothing more. This section shows some improvements a developer can do to improve the client experience. These come with the use of some annotations and return types.

**Annotations**

You can add a base path to your controller so you dont need to add a full path to every controller method. `@RequestMapping("/api/v1")` adds the prefix api/v1 to every controller method, this way there is lower chance or an error as you repeat less code.

We can make use of Spring's _dependency injection_ to clean up our controllers even further with `@Autowired`. This way we dont need to create a _new_ repository. 

> You can use `@Autowired` on any class that is marked as `@Component`, `@Repository`, `@Service`, `@Controller`, or `@Configuration`.

```java
@RestController
public class AuthorController {
    @Autowired
    private AuthorRepository authorRepository;
}
```

This is equivalent to:

```java
@RestController
public class AuthorController {
    private AuthorRepository authorRepository;

    public AuthorController(AuthorRepository authorRepository) {
        this.authorRepository = authorRepository;
    }
}
```
> Intellij may suggest you convert `@Autowired` to the second version, it is identical, the `@Autowired` version is [syntactic sugar](https://en.wikipedia.org/wiki/Syntactic_sugar#:~:text=In%20computer%20science%2C%20syntactic%20sugar,style%20that%20some%20may%20prefer.).

**ResponseEntity**

Until this point, we have been simply returning JSON data and leaving the status code assignment up to the _Spring Framework_, we should look to do that ourselves as this gives a large degree of control and customization. We can make use of the response type `ResponseEntity` and the `HttpStatus` enumerator.

We alter our controller methods to return a `ResponseEntity<>` with the old return type enclosed, and a `HttpStatus` to supply a code.

```java
@GetMapping(value = "/authors")
public ResponseEntity<List<Author>> getAllAuthors() {
    List<Author> authors = authorRepository.findAll();
    HttpsStatus resp = HttpStatus.Ok;
    return new ResponseEntity<>(authors, resp);
}
```

> `authorRepository.findAll();` is a call to our generated repository.

This configuration allows for some response logic to be performed. For example, if an update request is performed, you need to check if the object in the body is the correct one, this is done by checking the path variable against the request body. For this example, assume the relavent models and respository methods exist. 

```java
@PutMapping("/authors/{id}")
    public ResponseEntity<Author> updateAuthor(@PathVariable Long id, @RequestBody Author author){
        Author returnAuthor = new Author();
        HttpStatus status;
        /*
         This is to ensure some level of security, making sure someone
         hasn't manipulated our request body or a client error.
        */
        if(!id.equals(author.getId())){
            status = HttpStatus.BAD_REQUEST;
            return new ResponseEntity<>(returnAuthor,status);
        }
        returnAuthor = authorRepository.save(author);
        status = HttpStatus.NO_CONTENT;
        return new ResponseEntity<>(returnAuthor, status);
    }

```

We can do the same to signify a `not found` error:

```java
@GetMapping("/authors/{id}")
public ResponseEntity<Author> getAuthor(@PathVariable Long id){
    Author returnAuth = new Author();
    HttpStatus status;

    if(authorRepository.existsById(id)){
        status = HttpStatus.OK;
        returnAuth = authorRepository.findById(id).get();
    } else {
        status = HttpStatus.NOT_FOUND;
    }
    return new ResponseEntity<>(returnAuth, status);
}
```

> In these cases we returned a different `HttpStatus` based on different states.

**Common Response Object**

Instead of returning domain objects in the body, many services return something called a common response object. This is done so any client knows exactly the retuned data structure. This allows clients to have a simple way of extracting messages or errors from responses, while still having all the returned data in an approriate field.

An example of a common response object is shown below.

```java 
public class CommonResponse {
    public Object data;
    public String message;
    public String error;
    public HttpStatus status;
}
```

> This is not the only structure for a common response, this is just one example. 

- There is a `data` field with the returned data.
- A `message` field for any information about the request.
- A `error` field where an error would be written. This is _null_ when there is no error.
- A `status` field where they can easily get the http status.
- Whatever else you want to add that will be common for all responses.

You will always return `ResponseEntity<CommonResponse>` in your controllers.
```java
return new ResponseEntity<>(commonResp, status);
```

This gives your REST API some consistency and makes it easier for your clients to navigate. This is not mandatory, just a design choice.

> `ResponseEntity` is already somewhat of a common response. 

Spring has a common response for errors, an example can be seen below:

```json
{
 "timestamp": 1500597044204,
 "status": 400,
 "error": "Bad Request",
 "exception": "org.springframework.http.converter.HttpMessageNotReadableException",
 "message": "JSON parse error: Unrecognized token 'three': was expecting ('true', 'false' or 'null'); nested exception is com.fasterxml.jackson.core.JsonParseException: Unrecognized token 'aaa': was expecting ('true', 'false' or 'null')\n at [Source: java.io.PushbackInputStream@cba7ebc; line: 4, column: 17]",
 "path": "/authors"
}
```

> This is a dummy error and not something we have done in our models or controllers. 


## Related Data

As discussed in the RESTful Services lesson, representing related data is quite often a tricky situation. There are many downsides, like overposting and a risk of infinite recursion. In that lesson the HATEOAS style was mentioned, and a similar style will be adopted to represent related data in this lesson. 

If you change nothing about your entities and query out _authors_, you will get all the _books_ they have written. Those _books_ will contain the _author_ that wrote them, and now we are in an infinite loop. You ideally want to stop this loop as soon as an _author_ has already been represented. There are a few ways to counter this behaviour, two of which are covered in this lesson. The first is to use data transfer objects (covered in a later section) and the second is to return related data as a URI to the related resource.

> Both of these techniques can be used together as well.

For the purposes of this section, we will be replacing the related entity with a URI to that resource. E.g. Instead of `Book` serialized to JSON, the following is returned:

```java
"/api/v1/books/:bookId"
```

This is achieved by overloading the _GET_ resource in the entity using a library called `Jackson Databind`.

**Jackson Databind**

[Jackson Databind](https://github.com/FasterXML/jackson-databind) is a library that allowed a developer to change the way Java objects are serialized and deserialized. This is achieved with the use of annotations. There are many available methods to alter the serialization and deserialization behaviour of your entities, we are focussing on one, `@JsonGetter`. This changes the way our entities are serialized into Json. We can do this at a attribute level.

As an example, lets take the Book entity. A book has an author, represented as a `Author` type, looking the snippet below.

```java
@ManyToOne
@JoinColumn(name = "author_id")
public Author author;
```

We want to change the way our `author` attribute is serialized. To do this we add a method and annotate it with `@JsonGetter` and pass the name of the attribute we want to change as a paramter. Then we can override its default behaviour to something we want. 

> It is important to have the return type as `String` and the paramter to `@JsonGetter` to be the name of the attribute you want to serialize, otherwise the library doesnt know which attribute you mean and do nothing. 

```java
@JsonGetter("author")
public String author() {
    if(author != null){
        return "/api/v1/authors/" + author.getId();
    }else{
        return null;
    }
}
```

> The `null` check is to prevent a _null pointer exception_ from being thrown if there is no author for a book.

Now when we query out a book, instead of a full author object being returned with our book, we simply have a link to where the client can get that resource, if they wish to do so.

> This allows our API to be more flexible to the clients that consume it.

If we have a many to many, like library and books, then we need to return a `List` of strings representing URIs. Take the example below:

```java
// Book entity
@ManyToMany(mappedBy = "books")
public List<Library> libraries;

@JsonGetter("libraries")
public List<String> librariesGetter() {
    if(libraries != null){
        return libraries.stream()
                .map(library -> {
                    return "/api/v1/libraries/" + library.getId();
                }).collect(Collectors.toList());
    }
    return null;
}
```

> The Stream API is heavily used for this to create simple mappings.

## Services

Services are another layer of abstraction, annotated with `@Service`, that have the job of interacting with your repositories to remove uneeded logic from controllers. 

These classes will hold references to the various repositories needed and handle all the additional manipulation needed aside from basic functionality. A case for this would be handling all the cascading delete behaviours or a patch update. These lines of logic would have gone into the controller if a service is not used. Think about how bloated a controller can get when it has to return a different status based on a state, retrieve and then manipulate resources.

Another reason to look to use a repository is if your controller is using more than one repository, i.e. the `AuthorController` needs to use both the `AuthorRepository` and the `BookRepository`. In this case, it would be better to have an `AuthorService` and use that instead. 

> Keep in mind, like design patterns, if services are just going to add complexity and not offer much else, its best to not use them.

```java
@Service
public class AuthorService {
    @Autowired
    private AuthorRepository authorRepository;
    @Autowired
    private BookRepository bookRepository;

    public List<Book> getBooksFromAuthor(Long id) {
        return authorRepositry.getOne(id).getBooks();
    }

    ...
}
```

## Data transfer objects

Data transfer objects (DTOs) have been lightly mentioned in previous lessons as an aside. This section aims to give some context to their role in software development. A DTO is a simple Java class, like a POJO, that handles communication between layers in your application.

DTOs typically represent aspects of our domain models. In this role, they decouple our layers even further as the layers rely on these data abstractions and not the internal schema. DTOs also allow us to not expose too much information to the client, the clients are also decoupled from our application as they are coupled to the DTOs instead. This allows our internal schema to change without breaking clients, this is rare however. 

In practicality, what DTOs offer us is ways of returning data we want. For example, a list of `Authors` to pick from a drop down - make a DTO with `AuthorId` and `Name`. We dont have to worry about overposting and the client doesnt have to sift through information they dont need. Another practical use is to aggregate data, think of it like the Thymeleaf `Model`, you can create a DTO to hold various lists or fields from many tables, and pass that to the client. 

To include DTOs is very simple, in yuor Models package you can create a DTO package and place them all there. You will then need to map from your domain models to your DTOs. This mapping can be done manually or with a library like `ModelMapper`, and it is done in the controllers. Rrepositories are not responsible for this mapping, controllers handle the requests, which include arragning data into the required state. 

> If you've used services, you can do it in the service instead.

DTOs are not a necessary part of a web application, they just help clean it up. Our use of `@JsonGetter` to alter the serialization behaviour is essentially doing the job of a DTO, if we are using these Jackson annotations, the only use for DTOs is to create custom objects to pass to the client or if you want to make your application as clean as possible - they do add a lot of work however, so be careful when deciding.

> **RESOURCE** Example repo showing the bookstore made with Hibernate. [Link](https://gitlab.com/NicholasLennox/hibernate-library).

---

Copyright 2021, Noroff Accelerate AS