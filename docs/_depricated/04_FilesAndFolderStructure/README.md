# Packages and File IO

This lesson introduces candidates to creating proper folder structure that is implemented by common IDEs. The candidates should understand why certain folders exist and where they are located.

Once the folder structure has been understood, the candidates will learn about File IO in Java. This includes various methods of reading information from files, methods to write data to files, and how to handle exceptions related to File IO.

**Learning objectives:**

You will be able to...
- **explain** the role of packages in a Java appliation and how scope affects what is usable.
- **modify** your codebase, using packages, in alignment with best practices.
- **describe** the file system in Java.
- **demonstrate** the use of path variables to locate files in a Java application.
- **manage** files in a Java application, this includes: **creation**, **modification**, and **examination**.  
- **defend** your Java application against file exceptions by **constructing** proper error handling, where exceptions are **predicted** and **managed**.
- **execute** multiple class compilation, entry point creation, and **demonstrate** the ability to run packaged Java applications.

---

## Packages

When we create multiple file (multiple class) solutions it is often useful to bundle related classes together. We can do this by creating a package.

Creating a package is very simple, we need to add the following statement as the first line of each source file:

```java
package pets;

// rest of the file
```

> **INFO** _Note_: Package names should always be written in lower-case.

There is a second important step (or rule) for packages, the source files must be in a folder that matches their package declaration. There are other folders of importance that should be placed in a Java application, these deliniate where your source files are and where your resources are. They will be detailed as the the `pet example` is considered further.

We have a main program, which will be called `Program` and we will place the `Program.java` file in the root of our code folder (`/src/`). Next, we have the `Pet` class and `PetType` enumerated type. Both of these will be in the `pets` package, and thus must be placed in the `pets/` folder within the code folder. As you can see here, everything that logically is grouped with pets, goes in the pets package.

**Pet Project Source code**

```
 /src/
   │
   ├─ Program.java      // main program
   │
   └─ pets/
       │
       ├─ Pet.java      // Pet class
       │
       └─ PetType.java  // PetType enum
```

**Program.java**

```java
import pets.*;

public class Program {

    public static void main(String[] args) {
	    Pet fluffy = new Pet("Fluffy", "Craig", 4, PetType.cat);
        System.out.println("Name: " + fluffy.name);
        System.out.println("Owner: " + fluffy.owner);
        System.out.println("Pet Type: " + fluffy.petType);
    }
}
```

> **INFO** _Note_: The import statement seen here tells the program we want to use the classes that are in that package.

**Pet.java**

```java
package pets;

public class Pet {
    public String name;
    public String owner;
    public int age;
    public PetType petType;

    // Creates a pet with a specific name, and unknown owner or age
    public Pet(String name){
        this.name = name;
        owner = "Unknown";
    }

    // Creates a pet with a specific name, owner, age, and pet type
    public Pet(String name, String owner, int age, PetType petType){
        this.name = name;
        this.owner = owner;
        this.age = age;
        this.petType = petType;
    }
}
```

**PetType**

```java
package pets;

public enum PetType {
    cat,
    dog,
    fish,
    rat,
    rabbit
}
```

If we compile and execute the program, the output would be the following:

```
Name: Fluffy
Owner: Jeff
Pet Type: cat
```

**Why use packages**

Packages give us a few useful advantages:

1. Readability; related classes and types can be easily determined
2. Access; internal to the package classes can have unrestricted access to each other
3. Separation; Names (of classes) within the package wont interfere with code in other packages
4. Ease of access; we need only import the package to get access to all containing classes and types

## Compiling multiple files

Consider the Pet example, but placed within a project folder, let us call it `PetProject`.

```
 /PetProject/             // Project Root
   │
   ├─ out/                // Compiled output
   │
   └─ src/                // Source code
       │
       ├─ Program.java
       │
       └─ pets/
           │
           ├─ Pet.java
           │
           └─ PetType.java
```

We open the `PetProject` folder in our terminal. We will be using the Java Compiler (`javac`), you will recall that we can check that we have the correct version installed using the `-version` flag:

```bash
$ javac -version
javac 14
```

To compile this correctly, we must at very least specify all the _uncompiled_ source files within the project (in this case, all of them).

```bash
$ javac src/*.java src/pets/*.java
```

> **INFO** The `javac` command does not show any interesting output if run successfully. We can tack the `-verbose` tag on the end if we want to see all the nitty-gritty details

This will compile and create a corresponding `.class` file for each of our `.java` files. The created files will be in the same folder as the source files, and this is not ideal!

We can use the `-d` flag to specify the desired output folder, a good convention would be `/out/`.

```bash
$ javac -d out src/*.java src/pets/*.java
```

Finally, if we want to run the program we do so with the following commands:

```bash
$ cd out/
$ java Program
```

This will produce the following output:

```
Name: Fluffy
Owner: Geoff
Pet Type: cat
```

> **INFO** Once we switch to using the IntelliJ IDE, it will take care of the compilation steps for us.

## Building a .jar

This section details build .jar files using the console/terminal, in later chapters we will use _Intellij_ to achieve the same thing in a easier way. 

Consider the following project:

```
 /PetProject/             // Project Root
   │
   ├─ out/                // Compiled output 
   │
   └─ src/                // Source code
       │
       ├─ Program.java    // Contains main()
       │
       └─ pets/
           │
           ├─ Pet.java
           │
           └─ PetType.java
```

You will recall that we can compile this to the `/out` folder with the following command from the `/PetProject` folder:

```bash
$ javac -d out src/*.java src/pets/*.java
```

Doing so creates the corresponding `.class` files in the `/out` folder.

We can always run the program by navigating into the `/out` folder and using the following command:

```bash
$ java Program
```

But this will leave out code unpackaged, meaning its difficult to distribute. From the `/out` folder we can use the following command to create an executable `.jar` file:

```bash
$  jar cfe PetProgram.jar Program *.class pets/*.class
```

Lets break this down into it's components;

`jar`: We are using the jar application.

`cfe`: These are the command flags to instruct the jar application what we are providing and what we want it to do.

- `c`: _Create_ a new `.jar` file
- `f`: The output _File_ name we want to create
- `e`: Specifies the _Entry_ point for `.jar` file, this is the location of the `main()` function

Another useful flag to include is `v`  for verbose, this show detailed information of the jar applications execution progress.

`PetProgram.jar`: This corresponds to the `f`, and is the name of the file we want to create.

`Program`: This is the fully-qualified name (FQN) of the class containing the `main()` function as required by the `e` flag. In more thorough solutions this would be `com.company.Program`.

`*.class pets/*.class`: Finally we must list the class files that we want to include in the `.jar` file, here we specify all the `.class` files in the current folder and in the `/pets` folder.

## Project Structure
Expanded look at the program structure. And how large numbers of included class begin to overwhelm CLI compilation.

Project structure is one situation where _conventions_ begin to appear. In the examples here we always place source code in the `/src` folder, we could have named this folder `/source`, `/sourcecode`; instead we stick the convention of `/src`. Similarly, our compiled files are placed in the `/out` folder. Test classes will be placed in the `/test` folder.

The concept of `Convention over Configuration` will be covered later, but this bis a small example of it: When opening the project, a new developer should understand the structure of our project simply from the conventions that we have followed. Had we used obscure folder names, the developer would need to understand the configuration first before the structure would make sense.

Following from this we will use the following structure when working with IntelliJ:

```
 /ProjectName/                      // Project Root
   │
   ├─ README.md                     // Markdown file documenting the project
   │
   ├─ lib/                          // Third party libraries
   │
   ├─ out/                          // Compiled output 
   │
   └─ src/                          // Source code
       │
       ├─ main/                     // The build version of the project  
       │   │
       │   ├─ java/                 // Java files that will be compiled
       │   │   │
       │   │   ├─ Program.java      // Contains main()
       │   │   │
       │   │   └─ pets/             // A package
       │   │       │
       │   │       ├─ Pet.java      // Class inside package
       │   │       │
       │   │       └─ PetType.java  // Class inside package
       │   │
       │   └─ resources/            // Files that will not be compiled (non-Java)
       │
       └─ test/                     // Test classes
```

In reality the  `Program.java` will be nested twice more inside the `/java` folder. This would correspond to the base package; assuming the `com.company.Program` base package, then `Program.java` would be located at:

```bash
/ProjectName/src/main/java/com/company/Program.java
```

These long file paths, coupled with the tedium of including many class files and external libraries makes compiling manually very tiresome. Thankfully when using an IDE like IntelliJ, the structure is auto-generated and maintained automatically for us. We need only follow good naming conventions for our classes and packages.

> **INFO** _Note_: This structure will differ slightly depending on the build system/framework we use. However, the idea is the same.

## Files

We all work with files every day of our lives; images, documents, save game files, etc. Most file IO (Input/Output) is handled by the application in context, or by the operating system (for deletes etc.), but if we want our own solutions to be able to retrieve data or store data we must handle the IO pragmatically ourselves.

Working with files with one of the fundamental purposes of

**Example file**

Examples in this section will use a plain-text version of [_Twenty Thousand Leagues under the Sea_](https://www.gutenberg.org/cache/epub/164/pg164.txt) by Jules Verne.

In all cases the following declaration defines the file:

```java
String pathToFile = "pg164.txt";
```

### File error handling

When working with files we always need to use enclosing try/catch blocks. This is for the following reasons:

- The file may be missing
- The file may be corrupt
- The file may be temporarily inaccessible (locked)
- The drive may fail during the read/write

For these reasons, we enclose out file reads inside `try` blocks and follow with appropriate `catch` blocks.

It is good practise to include a `finally` block that will try to close the file (or safely end any operations) in cases where the exception occurred with the file open.

This is an example template for how exception handling could be handled for file access, there are many possible shortcuts that exist in modern File IO libraries.

```java
try() {
		// Do file operation

	}
	catch (IOException ex) {
		// Handle IO Exception
	}
	catch (Exception ex) {
		// Handle other exceptions
	}
	finally {
		try() {
				// Close the file
			}
			catch (Exception ex) {
				// Handle exceptions during close
			}
	}
```

## File IO

We will be using text encoded examples here, but many other proprietary file formats will have specialized libraries that will decode the file contents. As an example, _Apache POI_ is a commonly used library for working with _Microsoft Excel_ files (`.xls` and `.xlsx`).

Text encoded files will not require any decoding and we can work with their contents directly.

**FileInputStream**

The most primitive way to access a file is through reading the raw bytes, we do this by using the `FileInputStream` class.

This method is useful if you want to check manually calculate file size, for instance when manually calculating a _checksum_.

In the following example, the `fileInputStream.read();`command is used to repeatedly read the next byte of data from the file.

```java
public static void FileInputStreamExample(){
    try (FileInputStream fileInputStream = new FileInputStream(pathToFile)) {

        int data = fileInputStream.read();
        int byteCount = 0;

            // -1 indicates no more data
        while(data != -1) {

            // process data
            byteCount++;

            // next data
            data = fileInputStream.read();
        }
        System.out.println("File is " + byteCount + " bytes, or " +
                byteCount/1024.0 + " kilobytes (" + (int)Math.ceil(byteCount/1024.0) + "KB on disk).");
    }
    catch (IOException ex) {
        System.out.println(ex.getMessage());
    }
}
```

In this case, we calculate file size, 608KB on disk for _Twenty Thousand Leagues under the Sea_.

The `read()` function will read a single byte and return an integer value between `-1` (_end of file_) and 255 (_maximum value byte_).

**FileReader**

In modern computing systems, a single character may be any one of thousands, so a single byte does not describe all possible characters.

If we wanted to read one character at a time we can use the `FileReader`:

```java
public static void FileReaderExample(){

    try(Reader fileReader = new FileReader(pathToFile)){

        int data;
        char character;
        int bigNCount = 0;

        while((data = fileReader.read()) != -1) {
            character = (char)data;
            if(character == 'N') {
                bigNCount++;
            }
        }
        System.out.println(bigNCount + " big N's were found.");
    }
    catch (IOException ex) {
        System.out.println(ex.getMessage());
    }
}
```

In this example we count the number of upper-case `N`s in _Twenty Thousand Leagues under the Sea_, there are 1683 of them.

**BufferedReader**

A far more useful solution is to read the file line for line, to achieve this we can use `BufferedReader`:

```java
import java.io.*;

try (BufferedReader bufferedReader = new BufferedReader(new FileReader(pathToFile))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                // process the line
            }
        }
        catch (Exception ex) {
                // exception code
        }
```

Here we use `BufferedReader` to count the number of lines containing the word "Nautilus" with the file:

```java
public static void BufferedReaderExample() {
    try (BufferedReader bufferedReader = new BufferedReader(new FileReader(pathToFile))) {
        int linesWithNautilus = 0;

        String line;
        while ((line = bufferedReader.readLine()) != null) {
            // process the line
            if(line.contains("Nautilus")){
                linesWithNautilus++;
            }
        }
        System.out.println(linesWithNautilus + " lines have \"Nautilus\" in them.");
    }
    catch (IOException ex) {
        System.out.println(ex.getMessage());
    }
}
```

This returns a value of 520 times.

`BufferedReader` may also be used to read console input:

```java
try{
    String input = null;
    System.out.print("Please enter a value: ");
    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    input = bufferedReader.readLine();
    System.out.println("User entered: " + input);
}
catch (Exception ex){
    System.out.println(ex.getMessage());
}
```

**Scanner**

Another option for reading a file is the use of `Scanner`. In the case of `Scanner` we must tell it what separator we want to use.

```java
public static void ScannerExample(){
    try(Scanner scanner = new Scanner(new File(pathToFile))){
        int scannerNautilus = 0;

        scanner.useDelimiter(" ");
        String word;

        while(scanner.hasNext()){
            word = scanner.next();
            if(word.contains("Nautilus")){
                scannerNautilus++;
            };
        }
        System.out.println(scannerNautilus + " words have \"Nautilus\" in them.");
    }
    catch (IOException ex) {
        System.out.println(ex.getMessage());
    }
}
```

Scanner returns a value of 520 "Nautilus"s ("_Nautili_"?).

We can also use scanner to capture user input from the console:

```java
try{
    System.out.print("Please enter a value: ");
    Scanner scanner = new Scanner(System.in);
    String input = scanner.nextLine();
    System.out.println("User entered: " + input);
}
catch (Exception ex){
    System.out.println(ex.getMessage());
}
```

> There are a few technical details about this user input captured (such as length of input), that we have not discussed here as it is beyond the needs of what we are doing.
> You are encouraged to look this up yourself if it interests you

**StreamTokenizer**

The final method of fire reading that we are going to consider is tokenization. The StreamTokenizer class takes an input stream and parses it into "tokens", allowing the tokens to be read one at a time. The parsing process is controlled by a table and a number of flags that can be set to various states. They may be categorized into one of five categories:

- Whitespace
- Alphabetic
- Numeric
- Single Quote
- Comment

The tokenizer has some important fields we can access to control what tokens it counts:

- TT_EOF – A constant indicating the end of the stream
- TT_EOL – A constant indicating the end of the line
- TT_NUMBER – A constant indicating a number token
- TT_WORD – A constant indicating a word token

```java
public static void StreamTokenizerExample(){
    try(FileReader reader = new FileReader(pathToFile)){
        StreamTokenizer tokenizer = new StreamTokenizer(reader);

        // tokenizer.wordChars('!', '-');

        int nautilusTokens =0;

        while(tokenizer.nextToken() != StreamTokenizer.TT_EOF)
        {
            if(tokenizer.ttype == StreamTokenizer.TT_WORD
                    && tokenizer.sval.contains("Nautilus")){
                nautilusTokens++;
            }
        }
        System.out.println(nautilusTokens + " \"Nautilus\" tokens were matched.");
    }
    catch (IOException ex) {
        System.out.println(ex.getMessage());
    }
}
```

This produces a count of 466 times, which is not correct based on our previous counts. What happened here? The most common culprit is quotes, anything between two quotes (double or single) is counted as one word token. So if we have cases where a quote contains the work "Nautilus" more than once, it is only counted once. To resolve this, we need to set our quotes as ordinary characters. This can be done by adding the code below:

```java
tokenizer.ordinaryChar('\'');
tokenizer.ordinaryChar('\"');
```

The solution then correctly counts 520 instances of "Nautilus", due to it not including the entire quote.

---

Copyright 2021, Noroff Accelerate AS
