# Object Oriented Programming
In this lesson, you will learn to create object hierarchies through the process of Object-Oriented Programming (OOP). The lesson covers the core pillars of OOP and how they are present in the Java library. Interfaces are also discussed and contrasted with inheritance.

**Learning objectives:**

You will be able to...
- **explain** the pillars of object oriented programming (OOP).
- **dicuss** the role and use of OOP in the Java environment. 
- **construct** class relationships to meet given functional requirements.
- **demonstrate** the use of polymorphism and inheritence.

---

Object Oriented Programming (OOP) was created to allow a developer to represent and interact with code in the same way that humans represent and interact with the world, through objects. This made it more intuitive to design, arrange and understand code.

In an OOP system, everything is an object. An OOP application is made up of numerous objects interacting to perform various tasks. Classes are used to define an object.

> **INFO** The lesson will contain diagrasm based on UML for the various aspects of OOP. If this is unfamiliar to you, there is a section at the end of the lesson to explain UML.

## Classes

By definition, a class is "A combination of variables, functions, and data structures."

At their core, classes represent things that could exist. For example, if I wanted to model a pet I could create a class as follows:

```java
public class Pet {
	public String name;
	public String owner;
	public int age;
}
```

A class is made up of four core components

1. Constructors
2. Fields
3. Behaviours
4. Properties

**Constructors** are used to create objects and provide various options for doing so. **Fields** are a set of variables which represent the data that the object is responsible for (what is known about an object). **Behaviours** are a set of methods that represent the functional operations that the object can perform (what the object can do). **Properties** are used to control access to the fields which belong to an object.

Once a class is defined it forms a blueprint of an object. It can then be used to create multiple instances of the class (objects) to serve an application by storing data in fields using properties and invoking methods as behaviours.

Programming in this way is more intuitive as developers are working with objects which house relevant data and operations instead of having multiple methods and variables loosely scattered in an application.

### Java Class Rules

Java has three very strict rules relating to classes:

**The file name must match the class name**

The name of the class and the file storing it must match, and this is case sensitive too.

**The package name (FQN) must match the path to the file**

The file containing the class must be in a folder that maps to the package name of the class. This should also be in all lower-case.

**One class per file**

We can only place one **public** class in a given file. We can have other private classes within the same file.

### Naming Convention

As with all coding conventions, it always best to stick to the practises as mandated by your team (or organization).

Historically, we are told that "Class names should be nouns, in mixed case with the first letter of each internal word capitalized. Try to keep your class names simple and descriptive".

### Constructors

When we instantiate (create a new copy) of a class it will initialise it's properties to there default values (usually `0` or `null`). We can, however, specify these ourselves with a `constructor`. The constructor is called when we use the `new` keyword.

The constructor is a special method that has the same name as the class, with no return type. It must be public.

```java
public class Pet {
	public String name;
	public String owner;
	public int age;

	public Pet(){
		name = "Fluffy";
		owner = "Geoff";
		age = 4;
	}
}
```

But this would mean that each pet created is identical, we can instead use parameters and overloads to make more useful constructors:

```java
1	public class Pet {
2		public String name;
3		public String owner;
4		public int age;
5
6		// Creates a pet with a specific name, and unknown owner or age
7		public Pet(String name){
8			this.name = name;
9			owner = "Unknown";
10		}
11
12		// Creates a pet with a specific name, owner, and age
13		public Pet(String name, String owner, int age){
14			this.name = name;
15			this.owner = owner;
16			this.age = age;
17		}
18	}
```

Unspecified properties will once again use their default values.

The `this` keyword refers to the instance of the variable name in the containing class. The `this.name` on `line 8` or `line 14` both refer to the instance of name on `line 2`.

> **DOCUMENTATION** Read about naming conventions from [Oracle](https://www.oracle.com/technetwork/java/codeconventions-135099.html).

## OOP Concepts

There are four principle concepts that exist in OOP

1. Abstraction
2. Encapsulation
3. Inheritance
4. Polymorphism

The first two principles are applied when defining a valid class.

**Abstraction** refers to the process of defining a specific idea of an object which is different from a general idea of an object. If two people are asked to visualize a frog, they would likely look different. They are both abstractions of a frog. Therefore, by defining a class to represent a frog that has a name and weight and can hop and croak, an abstraction process has taken place.

**Encapsulation** refers to the containment of related variables and methods into an object. All the variable values (fields) and method calls available to a specific frog are contained in an instance of a frog object and nowhere else.

**Inheritance** and **polymorphism** are optional features. As previously mentioned, objects are created using constructors and a default constructor is provided. Constructors can be configured to run code whenever an object is created, and various versions of a constructor can be created using overloading. This provides useful flexibility, which makes object creation cleaner and more efficient.

In addition to these OOP concepts, classes can have relationships with one another. This is known as **association**, which is discussed later.

## Inheritance

Inheritance denotes that one class has all the properties and methods of another class. We say that the **subclass** `extends` (inherits from) the **superclass** (These can be called **child** and **parent** classes respectively).

When using inheritance, the inherited properties and methods from the parents can be **overridden** to be different, but still have the same name. i.e. `Animals` may `makeNoise()` differently based on their subtype. This is known as **Polymorphism** (one name namy forms). We will come back to polymorphism throughout the lesson as it is interwoven with OOP.

We say that the **subclass** is still of the same type as the **superclass**, in-fact the **subclass** inherits any types associated to the **superclass**.

In Java, this creates a chain of inheritance back to the base `Object` class (`java.lang.Object`).

#### The Big Limit of Inheritance

A class may only directly inherit from **ONE** superclass.

We are however allowed to use the **subclass** of one inheritance as the **superclass** of the next, but this creates a lot of tightly coupled code.

> **INFO** _Note_: If the concept of tightly coupled code is unfamilair, we cover it in the _OO Design_ lesson.

This "balancing act" of inheritance can be very difficult to maintain, a single chain in one of the _higher_ **superclasses** will have rippling effect down to all its **subclasses** and their **subclasses**.

> **INFO** To get around this, we can use _interfaces_, which are covered later in this lesson.

#### Inheritance example: Person

Consider the following, a `Teacher` is a `Person`:

In code we denote this as follows:

**Person.java**

```java
class Person{
	String firstName;
	String lastName;

	public Person(String firstName, String lastName){
		this.firstName = firstName;
		this.lastName = lastName;
	}

	public String GetName(){
		return firstName + " " + lastName;
	}
}

```

Here a `Person` class has two properties (`firstName` and `lastName`) that we set inside the constructor.

**Teacher.java**

```java
class Teacher extends Person{
	String department;
	int salary;

	public Teacher(String firstName, String lastName, String department, int salary){
		super(firstName, lastName);
		this.department = department;
		this.salary = salary;
	}
}

```

The `Teacher` class extends `Person` (by adding a `department` and a `salary`).

It's important that we call the `super()` method within the `Teacher` constructor, as this creates the underlying `Person` object first.

`Teacher` also adds a new `GetName()` method, and notice that it has access to the properties of `Person`.

We can also have a `Student` class extend the same `Person` class:

**Student.java**

```java
class Student extends Person{
	String course;

	public Student(String firstName, String lastName, String course){
		super(firstName, lastName);
		this.course = course;
	}
}

```

Now we have two **subclasses** which are both extended from the same **superclass**. We say that a `Teacher` is a `Person`, and a `Student` is a `Person`.

This allows us to treat a Teacher as a Person in our code. Another form of polymorphism, called **Dynamic Polymorphism**. This is explained in more detail in a later section.

## Abstract Classes

An abstract class is one that **cannot** be instantiated. Typically this is used to create a common design for a **superclass** without allowing instances of said **superclass** to be created. Only **subclasses** may exist.

The `abstract` definition still allows for complete methods and properties to be declared freely, and these will be inherited by all **subclasses** as normal.

We declare a class as abstract by using the `abstract` keyword as shown below:

```java
public abstract class Animal {

	public String getInfo(){
		return "This is an Animal"
	}

	// other class code
}
```

> **INFO** _Note_: Besides the `abstract` keyword, this is identical to a normal class.

#### Abstract Methods

We may declare methods as abstract too:

```java
public abstract class Animal {

	public abstract void makeNoise();

	public String getInfo(){
		return "This is an Animal"
	}
}
```

Doing so indicates that the **subclasses** must each have an operation matching the signature of the abstract method.

In this example we have a `Cat` class:

```java
public class Cat extends Animal{

	public void makeNoise(){
		System.out.println("Meow");
	};
}
```

And a `Mouse` class:

```java
public class Mouse extends Animal{

	public void makeNoise(){
		System.out.println("Squeak");
	};
}
```

Much like interfaces, we will get a compile error if we do not create concrete implementations of the abstract methods in the **subclass**.

Notice that in both cases we did not need to declare a method for `getInfo()` as it is implemented in the **superclass**.

## Polymorphism

Object orientation and abstraction allow us to take advantage of polymorphism; remember from before that we can say the following:

- `Cat` is an `Animal`
- `Mouse` is an `Animal`

We can say they are both `Animals`; this is how we could describe them within the physical world, but we may do the same within our code.

```java
Animal anAnimal = new Cat();
Animal anotherAnimal = new Mouse();
```

We declare our variable of type `Animal`, but we assign it the value of `Cat` or `Mouse`. The `Animal` class (as the superclass), has become polymorphic through the use of inheritance.

We know for certain (due to our definition of `Animal`), that the `Cat` class must have two methods, `makeNoise()` and `getInfo()`. When we call `makeNoise()` on the `Cat` stored in `anAnimal`, the `Cat` will _Meow_.

```java
anAnimal.makeNoise();        // Prints "Meow"
anotherAnimal.makeNoise();   // Prints "Squeak"
```

We can also use this principle in may other aspects of code, such as in collections:

```java
ArrayList<Animal> pets = new ArrayList<>(); // A collection of pet animals

pets.add(new Cat());
pets.add(new Cat());         // I have two pet cats
pets.add(new Mouse());       // and one mouse

for (Animal anim : pets) {   // Get each animal
	anim.makeNoise();        // Ask it to makeNoise()
}
```

This will produce the following output:

```
Meow
Meow
Squeak
```

> **INFO** Another example of polymorphsim is overloaded constructors, this is executed in the same manner as overloading methods and is known as **Static Polymorphism**. 

## Interfaces

Interfaces allow us to specify a set of method signatures that a given class must implement.

We do this by adding only the method signatures to the interfaces, these signatures have no `body` (not even the `{}`).

When declaring the implementing class we use the `implements` keyword followed by the interface name.

Finally, we explicitly implement each method as required by the interface.

#### Interface Example: Steerable

As an example, consider a `Steerable` interface:

```java
public interface Steerable{
	public void steer();
	public void stop();
}
```

> **INFO** _Note_: Interfaces are usually named using adjectives (and sometimes nouns)

In this example, we have specified that we want any implementing class to have methods (functions) called `steer()` and `stop()`. We have not in any way said how these should be implemented, but we have specified return types.

An interfaces can be described as a contract that a given class agrees to, it must have the requirements specified in the contract (signatures defined in the interface).

> **INFO** Like with inheritance, interfaces can be used as types and classes implementing those interfaces can polymorphically be treated as those types.

```java
public class Trolley implements Steerable{
	public void steer(){
		// Steering code goes here
		// A trolley is steered with a handle
	};
	public void stop(){
		// Stopping code goes here
		// Stop pushing it
	};
}

public class Car implements Steerable{
	public void steer(){
		// Steering code goes here
		// A car is steered a steering wheel
	};
	public void stop(){
		// Stopping code goes here
		// Use the brake
	};
}
```

Here you can see that both `Trolley` and `Car` can be steered and stopped, we can describe them as _steerable_.

Following the analogy, a `Trolley` would have a handle that gets pushed around while a `Car` would a steering wheel. To stop a `Trolley`, we simply stop pushing it. While a `Car` requires the use of brakes to stop. Even though they have the same methods that achieve the same results, they are vastly different in the implementation.

#### Multiple Interfaces

We can specify multiple interfaces by separating them with a comma.

```java
class Car implements Steerable, Refuelable{
	// Car code goes here
}
```

We will be required to implement **all** method signatures as defined by both interfaces.

> **INFO** Be careful not to implement two interfaces with the same required method signature (overlapping).

#### Java library example

As another example, in Java the `List<T>` is actually an interface, and it implements both `Collection<T>` and `Iterable<T>` interfaces. This is then implemented by the `ArrayList` class, and it must abide by the requirements of `List<T>`,`Collection<T>`, and `Iterable<T>` (as well as some other interfaces).

We can then reliably use any given `ArrayList` in a few different ways:

- `Collection<T>` gives us `add()`,`remove()`, and other utility
- `Iterable<T>` gives us `forEach()`, and the means to step through each element in sequence
- `List<T>` ensure that the items are stored in a predetermined sequence and that individual items have an index

Another common _collection_ is `Set<T>`, which also implements `Collection<T>` and `Iterable<T>` (again with some others).

We can rely on Sets and Lists both having the `add()` method because they both implement the `Collection` interface.

#### Interfaces at the same time as Inheritance

We can easily use both _interfaces_ and _inheritance_ at the same time. We do this by specifying the _extends_ first followed by the _implements_.

Consider a `Car` that is a `Vehicle` and implements `Steerable`:

```java
class Car extends Vehicle implements Steerable{
	// Car code goes here
}
```

We may still implement multiple interfaces if need be.

## Association

Objects can also contain other objects. In OOP this is known as **aggregation** and/or **composition**.

**Aggregation** refers to an object that may contain another object. For example, a House may or may not have a Pool. This means an aggregation is optional (allowed to be null), its an independent relationship.

**Composition** refers to an object that contains another object which it cannot exist without. For example, a House cannot exist without a Floor. This means that composition is manditory, its a dependent relationship.

Aggregation and composition are implemented by using an object for a field. With aggregation, the object does not have to be instantiated, whereas with composition the object must be instantiated when the object to which it belongs to is instantiated. 

## UML

The full specification of UML is vast and can be found online.[^1] UML describes diagrams for modelling and representing many different aspects of software and the development thereof. For our purposes, we will focus only on _Class Diagrams_.

An online _cheat sheet_ can be found [here](https://www.uml-diagrams.org/).

> **INFO** The specifications of UML can be found [here](https://www.omg.org/spec/UML/2.5.1/PDF). It is not needed to know all of this.

### Classes

An individual class is represented as a partitioned rectangle; it has three parts only the first of which (the name) is mandatory.

Parts of a class:

![UML class representation](./resources/01_class_basic.svg)

The terms _attributes_, _operations_, and _visibility_ are used because UML is language-agnostic; a UML diagram does not differ depending on the implementation language. For this lesson, we will talk of _attributes_, _operations_, and _visibility_, but these correspond to _variables_, _functions_ (or _methods_), and _access modifiers_ respectively in Java. There is no _Java UML_ or _Python UML_ only standard **UML**.

The _class name_, _attributes_, and _operations_ (and _visibility_ thereof) will match 1-to-1 with the implementation in code.

#### Class Name

This should match the class name used in the source code, this is usually centre aligned.

As we discuss later, we may optionally add a descriptor to the class name partition of the class.

#### Visibility

UML uses a single symbol to represent access levels of a given attribute or operation.

| modifier  | symbol |
| --------- | ------ |
| public    | +      |
| default   | (none) |
| protected | #      |
| private   | -      |

These symbols precede the attribute or operation in question.

Leaving off the visibility symbol indicates a _default_ visibility.

#### Attributes

Attributes are the properties/variables within the class.

These are written in the following format:

```
<access modifier> name: type
```

#### Operations

Operations are the functions/methods of the class.

These are written in the following format:

```
<access modifier> name(<parameters>): <return type>
```

_Access modifier_, _parameters_, and _return type_ are all optional.

As a normal example consider a `Person` class:

![Person class in UML](./resources/02_class_person.svg)

Or, as an example with no attributes, consider a `Utility` class:

![Utility class in UML](./resources/03_class_utility.svg)

> For clarity we leave the _attributes_ block present, but empty.

#### Simplification

We may at times want to represent the class without describing its' _attributes_ and _operations_, we can do this by simply describing it as a rectangle containing only the _class name_.

![A class without attributes or operations shown](./resources/04_class_simple.svg)

### Class Diagrams

We may combine multiple classes together in a single diagram if we want to represent relationships between them.

![A sample class diagram](./resources/05_class_diagram.svg)

There are many ways that class can be joined within a _Class Diagram_, we will cover specific examples of these when discussing the concepts themselves.

#### Others Parts of UML

UML has many other diagram types described within it and could be worth investigating further on your own.

Two other particularly useful diagrams are _state transition diagrams_ and _use case diagrams_.

### Inheritance in UML

Inheritance is depicted in UML by using an unfilled arrow.

The head attaches to the `superclass`.

The tail attached to the `subclass`.

The `Person` - `Teacher` relationship is depicted as follows:

![Simple inheritance in UML](./resources/09_class_inheritance_simple.svg)

We can extend this to include student by giving the arrow a new tail, but sharing the same head:

![Advance inheritance in UML](./resources/10_class_inheritance_advanced.svg)

### Abstract Classes in UML

We represent an abstract class in UML by writing the name in italics.

![Animal, Cat, and Mouse classes in UML](./resources/02_class_cat_and_mouse.svg)

Abstract methods are also written in italic within the abstract class, but are written normally in the implementation classes (`subclasses`).

### Interfaces in UML

When depicting interfaces in UML, we add the word `«interface»` (with the `«»`'s) above the name of the interface within the name partition.

![Interfaces depiction in UML](./resources/11_class_interface_depiction.svg)

We may use this `«»`-convention for any other stereotypes within our code. For example, the `«Controller»` or `«Entity»` when working with the Spring framework.

To depict the implementation of said interface we use a short line with an open circle, the circle is labelled with the interface name.

![Interfaces implementation in UML](./resources/12_class_interface_usage.svg)

### Association in UML

Association is depicted as a line joining two classes.

![Association in UML](./resources/06_class_association.svg)

#### Aggregation

We denote aggregation using an open diamond.

![Aggregation in UML](./resources/07_class_aggregation.svg)

We may also label the joining line; the house `is at` an address. You may label both directions if you desire.

We may also label the number of the relationship. In this case, `1..1` denotes a `one to one` relationship.

#### Composition

We denote aggregation using a filled diamond.

![Composition in UML](./resources/08_class_composition.svg)

We may also label the joining line; the house `has` many floors, and a floor `is in` a house.

We may also label the number of the relationship, in this case `1..*` denotes a `one to many` relationship.

---

Copyright 2021, Noroff Accelerate AS
