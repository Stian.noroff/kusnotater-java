# IDEs and Build systems
This lesson introduces integrated development environments (IDEs) to the candidates. The IDE of choice for this course is Intellij Ultimate which can be used as a trial version for the duration of the course. A general overview of Intellij is provided, focusing on the aspects that differ from traditional text editors; such as refactoring, autocompletion, code generation, and debugging.

The various build systems which are used in Java are covered, focusing on Maven and Gradle. A walkthrough is provided on the creation and development of applications made with these two build systems. Finally, a .jar (artifact) is created in each build system and without any build system using Intellij.

**Learning objectives:**

You will be able to...
- **create** a new Java application using Intellij
- **demonstrate** the use of built-in refactoring tools in Intellij to better **organize** your codebase.
- **demonstrate** the use of built-in code generation tools in Intellij to **create** new content to be used in your application.
- **demonstrate** the use of built-in debugging tools in Intellij to **detect** programming errors.
- **create**, **develop**, and **maintain** a Maven project.
- **create**, **develop**, and **maintain** a Gradle project.
- **create** a packed version of your application using the covered build systems.

---

Working with a text editor is perfectly fine for some smaller projects and even for making quick changes in larger projects, but most developers would opt to use an _Integrated Development Environment_ (IDE) for larger projects.

## Features of an IDE

An IDE is an editor built for programmers often with many useful features, a fully customizable text editor can sometimes rival an IDE for functionality. Generally speaking, most IDEs are built for a specific language or family of languages, but multi-purpose IDEs are also very common.

IDEs provide a wide variety of features to the developer, different IDEs may have some of these or more.

#### Syntax Highlighting

As with a good text editor, the IDE must provide language-specific code highlighting. This makes reading existing code much easier.

Typically text is shown in a mono-space font (all characters are the same width), and the code is highlighted using an appealing colour theme. Many IDEs (and editors) not make use of dark mode themes to help developers who spend hours at a time staring at code.

#### Code Automation

Most IDEs will include short cuts that will save time typing frequently used pieces of code. These are usually invoked by typing a partial word, using the arrow keys to select an option from the menu that appears, and pressing the `Tab` key to generate the desired code.

**IntelliJ: Live Templates**

IntelliJ has a number of built-in short cuts that are worth investigating. These can be found by going to:

`Settings` -> `Editor` -> `Live Templates`

Two such short cuts that are worth mentioning are the `sout` and `fori` templates.

Typing `sout` and pressing `Tab` will produce:`System.out.println();` and will place the cursor between the parentheses.

Typing `fori` and pressing `Tab` will produce:

```java
for (int i = 0; i < ; i++) {

}
```

- The cursor will have the first `i` selected, and typing will replace all instances of `i` within the generated code.
- Pressing `Tab` again will advance the cursor to after the `<` in the condition portion of the for statement so that you may complete the condition.
- Pressing `Tab` a final time will move the cursor into the body of the statement.

You may freely create your own Live Templates as you need.

**IntelliJ: Generate or Refactor**

Many other options are available to you within IntelliJ by right-clicking within the code and selection options from the dropdown menu.

_Refactor_ lets you change file and variable names automatically in multiple places at once.

_Generate_ lets you insert some of the other more common methods and signatures that you may need.

In this example _Generate_ is used to automatically create getters and setters for four variables in the `Person` class:

![Creating getters and setters with generate](./resources/01_GetterAndSetter.gif)

#### Integrated Help

Many IDEs will pre-load imports and using statements, and can thus offer suggestions and corrections to your code before compilation.

Some IDEs also have built-in documentation for their specific language.

In the case of IntelliJ, holding down the `Ctrl` key and mousing over variables, fields, and methods will display a summary of their definitions with a click-able link to the full definition.

![Ctrl Clicking](./resources/02_CtrlClick.gif)

Alternatively, placing the caret within a variable, field, or method and pressing `Ctrl` + `Shift` + `I` will display a detailed definition pop-up.

![Ctrl Shift I](./resources/03_CtrlShiftI.gif)

#### Debugging & Profiling

Most IDEs will let you test your software by simply pressing a button to initiate the build toolchain.

While your solution is running from within the IDE, the IDE will profile it's memory usage and performance. In most cases, if an error or crash is encountered the IDE will pause execution on the offending line so that you can inspect the memory configuration that resulted in the crash.

This process does result in a small amount of overhead in terms of execution speed, and the final performance of any solution should be measured outside of the IDE.

## IntelliJ IDEA

IntelliJ IDEA ([jetbrains.com/idea](https://www.jetbrains.com/idea/)) is the Java offering from JetBrains. JetBrains offers specific commercially available IDEs for a wide selection of languages.

The free _Community Edition_ has all of the required features that we will need in this course. _However_, the `ultimate edition` has a one month free trail which has some features that are very useful for later lessons, it also becomes community edition when it expires.

> Codes for ultimate edition are normally available through the lecturers.

#### Creating a new project in IntelliJ CE

1. When launching IntellIj for the first time you will be greeted with the welcome screen
2. From this screen you want to select new project
3. Ensure that `Java` is selected on the left hand side and that the correct Java version is selected
4. Press `Next`
5. The second screen will allow you to build a project from a template. By default the only template is the `Command Line App`. You may create an empty project to start by not selecting anything on this screen
6. Press `Next` again
7. Give your project an appropriate name. You may change the location of the project here too

These steps are demonstrated here:

![Creating a new project](./resources/05_NewProject.gif)

These steps will produce a project named `HelloWorld` with the `main()` method in the `Main.java` file inside the `/src` folder.

![A new project](./resources/06_NewProjectIDE.png)

We can execute our code inside the IDE by pressing the small green triangle in the upper right. You may notice that to the left of this triangle is a drop-down with `Main` selected, this is specifying the entry point class.

This `Command Line App` will print output to the IntelliJ terminal window at runtime.

## Build Systems

As projects grow in size, the steps to manage imports and compilation becomes increasingly difficult to maintain. To this end, we use a build system. The two most common options are Maven and Gradle. Both of these are integrated completely with IntelliJ.

### Maven

Maven is the more establish of the two build automation systems we are going to investigative. It has a longer history and you are more likely to encounter it in existing development projects. It has a high degree of customizability.

**Creating a Maven project**

1. We create a Maven project by starting from the `New Project` screen
2. Select `Maven` on the left-hand side, and ensure that you have the correct version of Java selected
   > Optionally, you may select an archetype (template)
3. Press `Next`
4. The next screen lets us name the project
5. Expanding the `Artifact Coordinates` drop-down allows us to specify other parts of our project
6. Clicking `Finish` will generate the starting project

![Creating a new Maven project](./resources/01_Maven.gif)

> **INFO** _Note_: In your own projects it is a good idea to manually specify the `GroupId`. This should match your organizational structure. Be it the company that you work for: `com.company` or the team you are part: `com.company.team`

The default project that is created like this will have no entry point specified, in-fact it will not even have a source code file. It will, however, contain a `pom.xml` file and the needed configuration to compile the (empty) project.

**pom.xml**

The `pom.xml` specifies the dependencies and project configuration for a Maven project.

Here is a sample `pom.xml` for a an empty project:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.company</groupId>
    <artifactId>MavenExample</artifactId>
    <version>1.0-SNAPSHOT</version>

</project>
```

> **INFO** _Note_: If we make any changes to this file (or when starting a new project), we should instruct IntelliJ to `Load Maven changes`. We can do this by pressing the reload button that may appear or by pressing `Ctrl` + `Shift` + `o`.

We will be using this file to manage dependencies now, but many other frameworks may require configuration elements to be added to this file later.

**Building a .jar with Maven**

Instructing our existing Maven project to build a compiled version of our source code is extremely simple:

1. Open the `Maven` menu on the right-hand side
2. Expand the project folder (`MavenExample` in image)
3. Expand `Lifecycle`
4. Double click the `compile` option

![Compiling a Maven project](./resources/02_MavenBuild.gif)

The options in this menu are short-cuts for the various command-line arguments that can be used with the `mvn` tool.

The above steps are equivalent to running a variant the following from a console:

```bash
$ mvn compile
```

We can see in the Debug window if the build succeeded or failed.

The compiled `.jar` file can be found inside the `/target` folder inside our project, in this example the file will have the name:

```
MavenExample-1.0-SNAPSHOT.jar
```

**Possible build error**

Occasionally you will encounter this error:

```
Unable to Build using MAVEN with ERROR - Failed to execute goal org.apache.maven.plugins:maven-compiler-plugin:3.1:compile
```

This is caused by my own system trying to use a default version of Maven which is very old, we simply specify for the project to use a newer version of the Maven Compiler.

The following is added to the `pom.xml` file (after dependencies):

```xml
<dependencies>
...
</dependencies>
<build>
    <plugins>
        <plugin>
            <groupId>org.apache.maven.plugins</groupId>
            <artifactId>maven-compiler-plugin</artifactId>
            <version>3.8.1</version>
            <configuration>
                <release>15</release>
            </configuration>
        </plugin>
    </plugins>
</build>
```

> **INFO** _Note_: `<release>15</release>` specifies that we are using Java 15. This can be changed to whichever Java version you want. If you want to select Java 8, there is a different configuration you need, as `<release>` wont work.
 
**Setting the Main-Class (manifest)**

A second plug-in we need to add to the `pom.xml` and configure correctly is the `maven-jar-plugin`, with this we can set the main entry point for our project if we want to build an executable `.jar` file:

```xml
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-jar-plugin</artifactId>
    <configuration>
        <archive>
            <manifest>
                <addClasspath>true</addClasspath>
                <mainClass>com.company.Program</mainClass>
            </manifest>
        </archive>
    </configuration>
</plugin>
```

In this example, `com.company.Program` should be replaced with the full path to you own main class.

### Gradle

Gradle is the preferred build system for Android projects. Working with the `build.gradle` file may seem easier than working in the Maven `pom.xml` file, but in essence they achieve the same result.

> **INFO** The choice of build system will be left to you (or based on project requirements), but historically Maven has had better support for the _Spring Framework_ and you may find that finding example code is easier for Maven projects .

**Creating a Gradle project**

Creating a Gradle project is very similar to creating a Maven project, the only major difference is selecting `Gradle` from the `New Project` screen.

It's important to still remember to expand the `Artifact Coordinates` drop-down and specify the project `GroupId` correctly.

![Creating a new Gradle project](./resources/03_Gradle.gif)

**build.gradle**

Instead of the `pom.xml` file, Gradle has the `build.gradle` file, this contains the same information albeit in a _cleaner_ format.

```
plugins {
    id 'java'
}

group 'com.company'
version '1.0-SNAPSHOT'

repositories {
    mavenCentral()
}

dependencies {

}
```

Adding a required library to this file is the same process as Maven, except we can use the Gradle dependency statement such as the following for [`org.json:json`](https://mvnrepository.com/artifact/org.json/json/20200518):

```
// https://mvnrepository.com/artifact/org.json/json
implementation 'org.json:json:20200518'
```

We simply add this line to the `dependencies` list, giving us the following:

```
plugins {
    id 'java'
}

group 'com.company'
version '1.0-SNAPSHOT'

repositories {
    mavenCentral()
}

dependencies {
    // https://mvnrepository.com/artifact/org.json/json
    implementation 'org.json:json:20200518'
}
```

If we want to be specific we could use the full import definition; these two lines are equivalent:

```
// https://mvnrepository.com/artifact/org.json/json
implementation group: 'org.json', name: 'json', version: '20200518'
```

Once added, save the file and remember to trigger a `Load Gradle changes` using the same short-cut as before: `Ctrl` + `Shift` + `o`.

**Building a .jar with Gradle**

Building a .jar with a Gradle project is nearly identical to the steps for a Maven project:

1. Open the `Gradle` menu on the right-hand side
2. Expand the project folder (`GradleExample` in image)
3. Expand `Tasks`
4. Expand `build`
5. Double click the `build` option

![Compiling a Gradle project](./resources/04_GradleBuild.gif)

Again, we can see `BUILD SUCCESSFUL` is printed for us, and the compiled `.jar` file can be found in the `/builds/libs` folder.

**Adding an entry point in a gradle project**

If we want a specific class to be specified as the entry point for our project (when build a `.jar` file), we do so by adding the following to the `build.gradle` file:

```
jar {
    manifest {
        attributes 'Main-Class': 'com.company.Program'
    }
}
```

In this example, `com.company.Program` should be replaced with the full path to you own main class.

### No build system

It is also possible to create `.jar` files with IntelliJ directly (without Maven or Gradle), but to do this we must first define an _artifact_.

> **INFO** _Note_: This method is a quicker method for creating standalone libraries.

1. Open `File` -> `Project Structure`
2. Select `Artifacts` on the left-hand side
3. Press the `+` icon (`Alt` + `Insert`)
4. Select `JAR` -> `From modules with dependencies...`
5. Select a `Main Class` (entry point) if applicable
6. Press `Apply` and then `OK`

![Define an artifact](./resources/05_DefineArtifact.gif)

This process will define what artifacts can be built, the next step instructs the IDE to perform the build:

1. Select the `Build` menu, and pick `Build Artifacts...`
2. A new menu will appear, select the artifact you want to build
3. Click `Build`

![Build an artifact](./resources/06_BuildArtifact.gif)

Once the build is complete, the `.jar` file can be found in the `/out/artifacts` folder.

## Maven Repository

Often we will need to include dependencies from projects created by other developers, a common such dependency is the `org.json` `json` library. This will add support for converting Java objects to and from the JSON (Java Script Object Notation) format.

To add this library to our project we could go through the process of:

1. Locate the project homepage
2. Downloading the latest version
3. Extracting it to the correct location
4. Manually link it into our project

**or**

1. Locate it within the Maven Central Repository (or other)
2. Add it as a dependency in our `pom.xml` file

The MVN Repository ([mvnrepository.com](https://mvnrepository.com)) is a searchable database of existing software libraries within the Maven Central Repository as well as other associated repositories.

> **INFO** _Note_: Gradle uses the same Maven Central Repository, and you can find the dependency imports under the `Gradle` tab.

**Adding a dependency**

Once we have located a needed library (such as [`org.json:json`](https://mvnrepository.com/artifact/org.json/json/20200518)), we can copy the Maven dependency code block:

```xml
<dependency>
    <groupId>org.json</groupId>
    <artifactId>json</artifactId>
    <version>20200518</version>
</dependency>
```

This must be added to the `pom.xml` file within a new `<dependencies>` tag:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.company</groupId>
    <artifactId>MavenExample</artifactId>
    <version>1.0-SNAPSHOT</version>

    <dependencies>
        <dependency>
            <groupId>org.json</groupId>
            <artifactId>json</artifactId>
            <version>20200518</version>
        </dependency>
    </dependencies>

</project>
```

Triggering the IDE to `Load Maven changes` (`Ctrl` + `Shift` + `o`) will trigger a download of the needed files, the `org.json` namespace is now available for import in our projects source code.

> **INFO** _Note_: The same can be done in Gradle by copying the Gradle import from Maven repository.

---

Copyright 2021, Noroff Accelerate AS
