# Test Driven Development
This lesson introduces the Extreme Programming concept related to Test-Driven Development (TDD) where there is a large emphasis on creating code to pass predefined unit tests. This lesson teaches the 3 laws of TDD and how to apply them, with a practical walkthrough of the process.

**Learning objectives:**

You will be able to...
- **recite** the three laws of test driven development (TDD).
- **discuss** the role of TDD in industry.
- **formulate** unit tests to meet those requirements, given a design specification.
- **develop** or **modify** code to pass those unit tests, given a set of unit tests 

---

For every piece of code that we write, we need a means to test if it achieves what we intended it to do. We write unit tests which allows us design and partition tests for our code. For every class that we create in code, we can create a matching test class. Within a single test class there may be many tests, often times we will have multiple test for each method that we have written (or will write).

We could have written manual code to test our production code, but that is not advisable; instead we use a unit testing framework (such as JUnit).

## Unit Testing in IntelliJ

One of the many advantages of using an IDE like IntelliJ is the built in features and support for common developer tasks, by following a convention we are able to easily run and review our unit test directly inside IntelliJ.

#### Create a sample class for testing

Before we can start testing, we need something to test. In this example, we will create a simple class with a single function that will return `true` if it is passed an even number and `false` if it is passed an odd number.

1. To do so in IntelliJ we start by `Right-clicking` on the `/src` folder.
2. Select `New` and `Java Class`
3. Type the name of the class and ensure that `Class` is selected
4. Press `Enter`

![Create a new class file](./resources/01_CreateClass.gif)

This creates an empty class file for us.

For this example I will paste the following code into the `Util.java` file:

```Java
public class Util {

    public boolean isEven(int num){
         return num % 2 == 0;
    }

}
```

Save the file, and now we have a simple class suitable for testing.

#### Create a test root folder

Depending on your project there may or may not be a test root folder defined.

If you need to create one do the following:

1. `Right-click` on the project name
2. Select `New` and `Directory`
3. Type `test` and press `Enter`
4. `Right-click` on the newly created directory
5. Select `Mark Directory as` and finally `Test Sources Root`

![Create tests root folder](./resources/02_CreateTestRoot.gif)

This creates a separate source code folder for test code. This code will not be compiled but is still considered part of the project.

#### Generate a test class (and sample class)

Switching back to the `Util` class that we created:

1. `Right-click` on the class name (in the code)
2. Select `Generate...` and then `Test...`
3. Select a testing library (I choose JUnit 5)
4. Pressing the `Fix` button will download the library into the project
5. Press `OK` to finalize generating the class

> **INFO** _Note_: There are options to create sample tests based on existing methods, but for this example we will write our own.

![Generate a test class](./resources/03_GenerateTest.gif)

This process creates an empty `UtilTest` class inside the `/test` folder:

![An empty test class](./resources/04_TestsEmpty.png)

#### Defining Test Annotations

Each test we write will be it's own `void` function marked with the `@Test` annotation:

```java
@Test
void checkEven() {
    assertTrue(util.isEven(4));
}
```

We may can any number of `assert` statements inside a single method, but to be as clear as possible, strive to have one assert per test method. You can pass multiple values to each test by using `@ParameterizedTest`.

We could also use the `@BeforeEach` or `@BeforeAll` annotations to do need set-up for each or all tests respectively.

> **DOCUMENTATION** Read about JUni 5 from the [User Guide](https://junit.org/junit5/docs/current/user-guide/).

Completing my simple test, the `UtilTest` class looks as follows:

```Java
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UtilTest {

    // Create an instance of Util for testing
    private final Util util = new Util();

    @Test
    void checkEven() {
        assertTrue(util.isEven(4));
    }

    @Test
    void checkOdd() {
        assertFalse(util.isEven(3));
    }

}
```

#### Run tests

There are a few options for how to run tests:

Individually:

- Select a test function, `Right-click` and select `Run '<test name>'`

Per class:

- Select a class in the project explorer, `Right-click` and select `Run '<class name>'`

All tests:

- Select the `/test` Directory, `Right-click` and select `Run 'AllTests'`

Here we `Run 'AllTests'` for the project:

![Running tests](./resources/05_RunTests.gif)

As you can see in the example, IntelliJ opens a new window displaying all the tests that were run, and the outcome thereof.

In this example, both of our tests were successful.

#### Failed Tests

Consider the following test (designed to fail):

```java
@Test
void checkOdd() {
    assertFalse(util.isEven(8));
}
```

When a test fails, IntelliJ indicates what the expected and actual results were:

```
org.opentest4j.AssertionFailedError: expected: <false> but was: <true>
<4 internal calls>
	at UtilTest.checkOdd(UtilTest.java:17) <19 internal calls>
	... remained of message
```

This indicates that we expected a value of `false` but instead got a value of `true` inside the `checkOdd` function inside `UtilTest.java` file on line 17.

## Testing with Gradle build systems

When creating a gradle project in IntelliJ, the default will be JUnit 4, we want to update to [JUnit5](https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter-api). This allows us to use the following dependency: 

```
dependencies {
    // https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter-api
    testCompile group: 'org.junit.jupiter', name: 'junit-jupiter-api', version: '5.6.2'
}
```

We need to tell the IntelliJ to run tests itself, and not through gradle.
Under `Preferences -> Build, Execution, Deployment -> Build Tools -> Gradle` change the following setting to `IntelliJ IDEA`:
![IntelliJ Gradle Config](./resources/06_GradleSetting.png)


We also need to inform gradle to run the tests we have created inside IntelliJ byt adding the following to the `build.gradle` file:

```
test {
    useJUnitPlatform()
}
```

All of these settings will let us use the gradle build tool while keeping tests and development inside IntelliJ.

## Test Driven Development

Given that we use tests to check if code is performing correctly, we will need to write many tests during the lifetime of a project. It is conceivable that the majority of these test can be designed and codified before any production code is written.

This leads to the concept of test-driven development (TDD), which can be summarized by the following three rules:

1. Only write production code _when_ you need to address a failing test
2. Only write enough of a test to make it fail
3. Only write enough code to make a failing test pass

Uncle Bob (Robert C. Martin) is highly respected in the field of Software Development, and he has given an excellent talk about Test Driven Development; available here: [Robert C. Martin - The Three Laws of TDD](https://www.youtube.com/watch?v=AoIfc5NwRks).

> **ARTICLE** TDD has far reaching implications and gets quite philosophical, you can read about [The Three Rules of TDD](http://butunclebob.com/ArticleS.UncleBob.TheThreeRulesOfTdd) from Robert Martin.

> **ARTICLE** Read about JUnit 5 best practices [here](https://howtodoinjava.com/best-practices/unit-testing-best-practices-junit-reference-guide/).

---

Copyright 2021, Noroff Accelerate AS

