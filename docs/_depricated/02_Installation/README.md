# Installation

This brief section will provide an overview of the software needed to start working with Java.

To start off we will need three pieces of software:

- A version of the Java Development Kit
- An Editor
- A shell (A terminal/console)

> **INFO** Reminder: We are not using IDE's from the beginning. We will do the first bit in a text editor. 

Let us have a look at each of these in turn:

## Java Development Kit

This will include amongst other things: the JRE, the Java compiler, and the standard libraries.

At the time of writing the latest major version of the JDK is 15, we suggest using version 15 or the latest stable version available at the time of reading.

**LTS (Long Term Support)**

Oracle cannot reasonably be expected to maintain all version of Java forever, instead they mark certain releases as having LTS.

You can check the the version support [here](https://www.oracle.com/java/technologies/java-se-support-roadmap.html).

At the time of writing, Java 15 is marked as **non-LTS** and the latest version to have LTS was Java 11. For this reason it is completely possible to stick to Java 11, and for new long-term projects this may be the best choice. For our uses though, Java 15 is safe to use as none of the assignments (or projects) will see this kind of long-term use.

For many existing projects (or for those with the absolute longest expected life time), it may be necessary to go back to Java 8 which has LTS until December 2030.

### Oracle Java SE

The _regular_ version of the JDK is maintained by Oracle.

We advocate using the latest, stable release from Oracle; this will require an account with Oracle in order to enable the download options.

[Oracle Java SE Downloads](https://www.oracle.com/java/technologies/javase-downloads.html)

> INFO Note: This is free to use for development, but anything published as a product needs a license. A fully free alternative from Oracle is recommended (shown below).

### An Open-source Alternative

Oracle also maintains an open-source version of the JDK (available under a version of [GPL](https://openjdk.java.net/legal/gplv2+ce.html)).

[Open-source JDK 15 builds](https://jdk.java.net/15/)

## Text Editors

For the beginning lessons of this course, we will be using a general-purpose text editor.

This should be a light-weight application that lets us create/open/edit any file that is encoded in plain text.

Many modern text editors now have built-in git integration and shell access. They are often able to directly invoke compilers to build solutions, and with the extensions available it is possible to blur the line between a simple text editor and a fully-featured IDE.

There are many possible options here, and if you have a preferred editor then feel free to use that instead.

### Visual Studio Code (VS Code)

Visual Studio Code is Microsoft's offering as a text editor for web developers. It has excellent support for all imaginable web languages and a marketplace of extensions for adding support and features.

This is a very powerful solution, but it is slightly more resource-intensive than the other options.

[Homepage](https://code.visualstudio.com/)

## A Shell

You can stick to the default command prompt or default terminal for your OS. If you want to use something else then the following are good choices:
#### Cmder
For windows, this is currently the best solution. Cmder offers the best experience overall while bundling all the needed features into one download.

We will need to install git later in this course anyway, so we can go ahead and get the _git-for-windows_ version of Cmder.

[Cmder](https://cmder.net/)

#### Git BASH for Windows

If you just want the basics, this is a good solution. For some commands there will still be conflicts that cannot be resolved de to file permission restrictions.

[git for Windows](https://gitforwindows.org/)

#### PowerShell

Since Windows 10, Microsoft has added a tool called PowerShell. This may be an alternative to Git BASH, but as we said we still need git for Windows anyway.

You may use PowerShell if you prefer, but course notes will assume you have git installed.

## Tying it all together

These are all vital tools along this path as a Java developer, and some configuration may still be required.

### Adding JDK to Path (Windows)

To access the Java developer tools easily from the shell, we need them configured as part of the `Path Variables`(System) for Windows.

**Guided Steps:**

- Click Start (or press the Windows key)
- Type "environment"
- Select the `Edit the system environment variables` option
- The `System Properties` dialogue will appear
- Click the `Environment Variables` button (bottom right)
- In the bottom half (System Variables), select the `Path` Variable
- Click the `Edit...` button
- Click the `New` button
- Type `C:\Program Files\Java\jdk-15\bin` and press return (this may change depending on Java version)
- Click the `OK` buttons to close all the dialogues (not `Cancel`)
- Finally, reload system variables (reboot the computer)

### Checking that it works

Once we have everything all set up and we have rebooted the system (to reload the `Path Variables`), we can do a quick test to check that the system is ready.

We use two simple commands the check both `java` version (JRE) and `javac` version (Java Compiler).

**Checking Java Version**

We use the following command:

```bash
$ java -version
```

This will ask the JRE to report it's version number, with this we can check that both the JRE functions correctly and that we have the desired version installed.

If it functions correctly we should see the following:

```bash
$ java -version
java version "14" 2020-03-17
Java(TM) SE Runtime Environment (build 14+36-1461)
Java HotSpot(TM) 64-Bit Server VM (build 14+36-1461, mixed mode, sharing)
```

You can see this system is using the (Oracle) `Java SE` at version 14 of the JRE. This version number may be different depending on which version of the JRE you have downloaded.

While a failure may look like this:

```bash
$ java -version
bash: java: command not found
```

This means that `bash` (our shell), could not find the program (or alias) called `java` to run. In this case, we should check that Java was installed correctly and added to the `Path Variables` (or we may simply need to restart the system if we have not yet done so).

**Checking Java Compiler Version**

Similarly, we can check the Compiler version as follows:

```bash
$ javac -version
javac 14
```

---

Copyright 2021, Noroff Accelerate AS
