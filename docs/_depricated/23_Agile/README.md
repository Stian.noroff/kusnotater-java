# Agile & Sprint

When developing non-trivial applications, the process by which we plan and organize phases of our project becomes just as important as implementing the solution itself.

A system development life cycle (SDLC) refers to the individual stages which a software project undergoes over its lifetime. This usually includes:

- **Planning**: setting project goals and a high-level plan for the project.
- **Analysis**: determining the functional requirements and needs of end-users.
- **Design**: the overall infrastructure of the project is described.
- **Implementation**: the project is built according to the specifications described in the previous phases.
- **Maintenance**: the continued monitoring of the system in production to ensure it stays updated to meet business goals. 
- **Testing and deployment** are often included within the **Maintenance** phase, though in reality they are not limited to said phase. They also do not need to be described as a separate phase.

**TODO SDLC IMAGE - VISIO**

There are many competing SDLC models. We will be focusing on Agile because of its widespread adoption.

## Agile

Agile uses an adaptive approach to development. This means that an emphasis is placed on developing features and adapting rapidly to changing requirements as opposed to meticulous planning and requirement analysis involved in more traditional, predictive models such as the Waterfall SDLC. These models prescribe that each phase must be done in its entirety before starting subsequent phases.

**TODO Waterfall vs. Agile - VISIO**

To facilitate the Agile approach, short development iterations with frequent releases and testing are favoured over longer development cycles.

Customer and end-user interaction are required to allow the development team to constantly adapt to changing requirements. This promotes a flexible, dynamic approach to managing software projects.

### Pair Programming

Pair programming is an agile technique by which two programmers collaborate on a single computer. Though many variants of pair programming exist, typically one developer will act as the driver, actively writing code, while the other will act as a navigator, guiding the driver towards specific goals.

Both developers bring their unique experience and perception of a specific problem to the table. If executed well, this can assist greatly in solving complex problems more effectively.

Some variants of pair programming include:

- **Driver-Navigator**: The classic variant as described above.
- **Pomodoro**: Timekeeping is used to signal the driver and navigator roles to alternate at regular intervals, usually around 25 minutes.
- **Ping pong**: Popular in the Test-Driven Development(TDD) paradigm, roles alternate when one programmer writes a failing test and the other attempts to write code which will pass the test.'
- **Mob programming**: One driver, more than one navigator. Encourages broad discussion regarding the problem, and could potentially bypass code review if the majority of the team is involved in the mobbing session.

A differential in skill level also needs to be considered when utilizing pair programming. Pairing is usually described in terms of whether the programmers are at an expert or novice level. Experts pairing together may be the most productive but can lead to stagnation and recycling of old, proven ideas. Novices paired together may be more effective than when working alone but may encourage bad habits from lack of guidance.

### Scrum

Scrum is an Agile framework which is widely utilized to manage the development process. It defines specific roles for various members of a Scrum team.

Scrum masters facilitate the exchange of information between team members. This is mainly done during brief, daily stand-up meetings (called scrums, named after the concept from rugby) where the scrum master will generally ask each team member what they have done the previous day, what they will do on the current day and if anything is blocking their progress.

Development team members carry out the tasks required of the specific sprint in question. This does not only include software developers but also testers, software architects, designers.

Product owners interact directly with customers, users and other key stakeholders to ensure the continued stability of the product as a whole. They will often be involved with gathering feedback, acting as the voice of the customer. They will have a much greater focus on the broader business objectives of the product, and as a result, are responsible for managing the product backlog.

In a Scrum driven project, the product backlog includes all future feature releases; usually containing descriptions and time estimates for completion.

### Kanban

Agile is not limited to Scrum. Kanban is a competing methodology which has gained prominence recently. The visualization of work in progress is emphasised to attempt to maximize efficiency, prioritising continuous delivery of development goals over specific work iterations or sprints.

![Trello Board Example](./resources/03_TrelloBoard.png)

At minimum a Kanban board will include columns for **To Do**, **Doing** (In Progress), and **Done**. Depending on the Sprint goals it may be commonplace to add a **Backlog** and **Code Review** columns. Many teams will add other custom columns as needed.

#### Backlog 

The backlog contains the tasks that you will no be including in the current sprint, but that should be completed later in the project (in future sprints).

I practise, two easy to use task board solutions are [Trello Boards](https://trello.com/) and [GitLab Issue Boards](https://docs.gitlab.com/ee/user/project/issue_board.html). 



There is no perfect solution to managing software projects, each methodology has its own benefits and drawbacks. It is ultimately up to the team in question to choose one which addresses its specific needs and to alter it when necessary.

---

Copyright 2021, Noroff Accelerate AS
