# Design patterns
This lesson will cover basic design patterns, their role in software development, and how they can be used to write better code. This lesson will be largely practical where a walkthrough example of refactoring an existing example that benefits from the use of design patterns.

**Learning objectives:**

You will be able to...
- **discuss** what design patterns are, and the role they have in industry.
- **describe** the various categories of design patterns.
- **demonstrate** basic implementations of common design patterns.
- **identify** and **integrate** an applicable design pattern, given existing code and a functional requirement.

---

## Introduction

Design patterns are reusable solutions to common problems found in software development. These patterns are not concrete solutions, but rather templates on how these common problems can be solved in various situations. 

> Patterns are not algorithms. Algorithms define a clear set of actions that can achieve some goal, a patterns are a more high-level description of a solution. 

These patterns represent the various best practices used by experienced object oriented software developers, first shown in a book written by the "Gang of Four" entitled [Design Patterns - Elements of Reusable Object-Oriented Software](https://www.oreilly.com/library/view/design-patterns-elements/0201633612/). Since this, more patterns have been added, but they are still based on the original ideals set forth in this book.

The creation of Design Patterns was based on two principles:

1. Program to abstractions not implementations
2. Favour composition over inheritance

> These two principles should sound very familiar. SOLID principles are heavily tied to Design Patterns.

### Advantages

Reusing design patterns helps to prevent subtle issues that can cause major problems and improves code readability for coders and architects familiar with the patterns. This is because Design Patterns are a common language of well-known, well understood names for software interactions.

Design Patterns save time as building software based on the experiences of other developers and best practices. Design Patterns help making your code more testable, which saves even more time as that refactoring does not need to be done. In additon to this, learning these patterns helps unexperienced developers to learn software design in an easy and fast way.

Due to Design Patterns being well-proven and testified solutions built on the knowledge and experience of experts, there is a high degree of transparency in the applications. 

> Design patterns are the industry standard approach

One thing to be careful of is the overuse of Design Patterns. Applying them everywhere will cause much more harm than good. When deciding on whether to use a Design Pattern or not, keep in mind the `KISS` principle covered in the `OO Design` lesson to see if much less complicated code would get the job done. 

> If all you have is a hammer, everything looks like a nail.

## Types

There are many Design patterns that have been created over the years. They typically fall within one of three categories:

- Creational patterns
- Structural patterns
- Behavioral patterns

> There is a forth category, called J2EE Patterns, which deals with front-end related problems. 

The following sub-sections give an explanation of the categories.

### Creational

These design patterns provide a way to create objects while hiding the creation logic, rather than instantiating objects directly using new operator. Increasing flexibility and reuse of existing code.

### Structural
These patterns explain how to assemble objects and classes into larger structures while keeping these structures flexible and efficient.

### Behavioural

These patterns are concerned with algorithms and the assignment of responsibilities and communication between objects.

## Common patterns

This section showcases some commonly found Design Patterns. The general structure of each example contains the following aspects:

- **Intent of the pattern**. Both the problem and the solution of the pattern are briefly described.
- **Motivation of the pattern**. This further explains the problem and the solution the pattern makes possible.
- **Code example**. This shows the structure of classes shows each part of the pattern and how they are related. This makes it easier to grasp the idea behind the pattern.

### Singleton

It is a creational design pattern that ensures that there is only one instance of a given class and providing a global access point to this instance. This means that there will be one instance of the class that is passed around.

A large reason you would want a single instance is when resources are shared. Take for instance accessing a file, you dont want multiple streams trying to interact with your file at the same time. This file could be a physical resource or a database, the idea still stands. You would also want this to be available anywhere, otherwise there is no point in creating a single instance it. 

To create a singleton requires a small change to the classes constructor, as the requirements needed for this pattern are impossible to implement with a normal constructor. The following example shows the template for a singleton. There are a few noteworthy additions:

1. A reference to itself in the form of a variable
2. A private constructor
3. A static method to return the reference

> Static is important in the method declaration, as it allows the method to be called without instances of the class existing.

```java
class Singleton
{
    // Static variable of the same type as the class (reference)
    private static Singleton single_instance = null;

    // Other variables
    ...

    // Private constructor
    private Singleton() {
        // Instantiation of any variables
        ...
    }

    // Static method to  create a new instance or return existing instance
    public static Singleton getInstance() {
        if(single_instance == null)
            single_instance = new Singleton();
        
        return single_instance;
    }
}
```

Setting the constructor to `private` prevents other objects from usign the `new` operator. This means the only way to access the instance is through a static method which is acting as a constructor. This all ensures only the single instance of the class is being shared around. 

> The advantage of Singleton over global variables is that you are absolutely sure of the number of instances.

Singletons are incredible common and are used throughout the Java environment. `Java.lang.Runtime` allows the application to interface with the environment in which the application is running through many singletons. Their main use is for:
- Logging
- Drivers
- Caching
- Pooling

There are also some downsides to Singletons and their use:
- They actually violate the _Single Responsibility Principle_ as they are solving two problems at the same time (single instance and global access).
- Singletons require some thread-safe additions for multithreaded environments, as there could be occasions where multiple threads could create the object.
- Singletons can be difficult to test due to their limitations with instance creation.

> These downsides are valid, but most of the time you are better off using a Singleton where applicable.

### Strategy

It is a behavioral design pattern that lets you define a family of algorithms, put each of them into a separate class, and make their objects interchangeable. This interchanging can happen at runtime, and happens normally with a family of interfaces.

The idea behind the pattern is that you take a class that does something specific in a lot of different ways and extract all of these algorithms into separate classes called _strategies_.

The original class, called _context_, must have a field for storing a reference to one of the strategies. The context delegates the work to a linked strategy object instead of executing it on its own. The context isn't responsible for selecting an appropriate algorithm. Instead, the client passes the desired strategy to the context. 

> This approach minimises coupling as their is only a coupling to abstraction.

The context interacts and works with all strategies through the same generic interface, which only exposes a single method for triggering the algorithm encapsulated within the selected strategy. The context becomes independent of concrete strategies, so you can add new algorithms or modify existing ones without changing the code of the context or other strategies. Below is a illustration of a typical Strategy Pattern:

<img src="resources/strategy_structure.png" alt="drawing" width="400"/>

The context could be any class, including a different class heirachy to give all children the ability to have the same strategy. 

> This makes the code very flexible and reusable

**Example:**

Taking the example of a bank and payments again. In this scenario you have a `Bank` that can, for the purpose of this example, handle various payments. 

```java
class Bank {
    // Variables
    ...

    // Constructor
    ...

    public void payByCreditCard() {
        // Implementation
        ...
    }

    // Other, non-payment related functionality
}
```

If we then wanted to add other payments we need to add extra methods, or find some other solution. The strategy pattern can be used here to great advantage.

> If this is looking familiar, Strategy Pattern is very similar to the _Open/Closed_ and _Interface Segregation_ principles from SOLID.

We can solve this by creating a family of Payments and compose the abstration into our Bank. To change the payment method, we need to add methods to change the payments methods.

> `Bank` is our _context_ and `PaymentStrategy` is our _Strategy_

```java
// Parent interface - the abstraction
interface PaymentStrategy  {
    void makePayment();
}

// The concrete payments
class PayPal implements PaymentStrategy {
    @Override
    public void makePayment() {
        // Implementation
        ...
    }
}

class MasterCard implements PaymentStrategy {
    @Override
    public void makePayment() {
        // Implementation
        ...
    }
}

class Visa implements PaymentStrategy {
    @Override
    public void makePayment() {
        // Implementation
        ...
    }
}

class Bitcoin implements PaymentStrategy {
    @Override
    public void makePayment() {
        // Implementation
        ...
    }
}
```
We then can modify our contect, `Bank` in the following way:

```java
class Bank {
    // Variables
    private PaymentStrategy paymentMethod;
    ...

    // Constructor
    public Bank(PaymentStrategy paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    // Functionality to change payment methods
    public PaymentStrategy getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentStrategy paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    // Executing the strategy
    public void ExecutePayment() {
        paymentMethod.makePayment();
    }

    // Other, non-payment related functionality
    ...
}
```

What has changed here is there is a composition of the abstraction in the Bank and methods to handle payments. This allows Bank to never change as new payments are added and it doesnt have to know anything about payments. This has the added benefit of easily changing payment methods based on the kind of Bank created by whichever client class is chosen.

> Extracting out behaviour and then combining original classes reduces duplicate code.

> The Strategy pattern lets you isolate the code, internal data, and dependencies of various algorithms from the rest of the code. 

Keep in mind, as common as the Strategy Pattern is, it is not manditory. If you only have a few algorithms that rarely change, there is no real reason to overcomplicate your code to include this pattern. 

Another aspect is of functional types and anonymous functions; you could use these functions exactly as you'd have used the strategy objects, but without bloating your code with extra classes and interfaces. This is obviously reliant on the language having an simple way of creating these anonymous functions.

> In Java you can use Lambda expressions to create annomous methods on the fly. Normally, Strategy is simpler, but the choice is up to each situation.

### Observer

This is a behavioral design pattern that allows you create a publish/subscribe mechanism to notify multiple objects about any events that happen to the object they are observing. This pattern is incredibly widespread: any event listener library is using this pattern, all social media applications use this pattern as the foundation of thier notification systems, and Java has a rudamentory observer pattern built into all objects. 

The object that has some interesting state is often called _subject_. All other objects that want to track changes to the subject's state are called _observers_. In short:
- Subject – the one who changes
- Observer – the ones who are notified of the subject's changes.

The pattern suggests you need to add some subscription mechanism to the _subject_ so that _observers_ can `subscribe` or `unsubscribe` from the events of the _subject_. The _observers_ need a mechanism to recieve the updated data from the _subjects_ and the _subject_ needs a way to notify these _observers_.

> This sounds complicated, but it is very straight forward.

In its simplest form, we are going to create a `Subject` class with a List of `Observers`, a way to add `Observers`, and a way to notify `Observers`.

As for the `Observer`, we create an abstraction with an `update` method to be the common interface. Then we create a concrete `Observer` that does something with the updated data from the `Subject`. 

```java
// Observer abstraction
public interface Observer {
    void update(String event);
}
// Concrete Observer
public class ConcreteObserver implements Observer {
    // Variables
    ...

    // Functionality
    ...

    // Update method for the Observer pattern
    public void update(String event) {
        // Do something with the updated info
    }
}
```

Now looking at how the `Subject` is structured:

```java
public class Subject {
    // List to store all observers
    private List<Observer> observers = new ArrayList<>();
    // Some state of the application the observers care about
    private String state;

    // Method to add observers
    public void addObserver(Observer observer) {
        observers.add(observer);
    }

    // Method to alter state
     public void setState(String state) {
      this.state = state;
      notifyAllObservers();
   }

   // Method to notify all observers of the change
   private void notifyObservers() {
       for(Observer observer : observers) {
           observer.update(state);
       }
   }
}
```


### Factory

The Factory Pattern is a very commonly used pattern. It is a creational pattern that handles the creation of classes, through an interface, while not exposing the creation logic to the client. This decouples the client from the concrete classes by removing the `new` keyword. 

> Why do we care about doing this? 

Once again its about maintenance and scaling. Take a simple example of a vehicle vendor, they start off just selling `Cars`. The application is created and there are classes dependent on Car throughout. What happens when we want to add `Motorbikes`? We still want the same functionality in general but the specifics change. So now we have to go back in the code base and add conditionals to help with catering for this. This is further compounded when the company wants to handle `Trucks`. The solution is to rather rely on some abstraction and only rely on the exposed functionality to avoid all the conditionals to switch the applications behaviour. 

> This just sounds like Dependency Inversion, or the Open/Closed principle. That is because the Factory Pattern is a very direct implementation of those principles.

The Factory Method pattern suggests that you replace direct object construction calls (using the `new` operator) with calls to a special factory method. Objects returned by a factory method are often referred to as `products`.

> Don't worry: the objects are still created via the `new` operator, but it’s being called from within the factory method.

This makes your code far more extendable and eliminates the need to high levels of maintenane. So far, just the idea of Factory has been discussed, not actually what it takes to make one and the changes that need to happen. 

Firstly, we need to create some abstraction all the products the factory can create. This could be some base class, or an interface; it doesnt matter. The important part is that all the functionality that the client needs to use is encapsulated in that base abstraction.

Lets take the example from before, a Vehicle vendor. 

```java
public abstract class Vehicle {
    // Variables common for all vehicles
    ...

    // Constructors
    ...

    // Common behaviour between all vehicles
    public abstract void inspect();

    public abstract void runSafteyCheck();

    ...
}

public class Car extends Vehicle {
    // Constructor 

    public void inspect() {
        // Car-specific inspect
    }

    public void runSaftyCheck() {
        // Car-specific runSafteyCheck
    }
}

public class Motorbike extends Vehicle {
    // Constructor 

    public void inspect() {
        // Motorbike-specific inspect
    }

    public void runSaftyCheck() {
        // Motorbike-specific runSafteyCheck
    }
}

public class Truck extends Vehicle {
    // Constructor 

    public void inspect() {
        // Truck-specific inspect
    }

    public void runSaftyCheck() {
        // Truck-specific runSafteyCheck
    }
}
```

Secondly we need to create the factory itself which will handle the object creation. This can be done in numerous ways; you can create a simple class with a `getVehicle` method returning a new `Vehicle`, you can create a family of factories with the children being responsible for creating each type of `Vehicle` (better for the Open/Closed principle), or you can implement an _Abstract Factory_.

> For our purposes we create the simplest factory; a single class with a `getInstance()` method.

```java
public class VehicleFactory {
    public Vehicle getVehicle(String vehicleType) {
        if(vehicleType == null) {
            return null;
        } 
        if(vehicleType.equalsIgnoreCase("CAR")) {
            return new Car();
        }
        if(vehicleType.equalsIgnoreCase("MOTORBIKE")) {
            return new Motorbike();
        }
        if(vehicleType.equalsIgnoreCase("TRUCK")) {
            return new Truck();
        }
    }
}
```
> The `vehicleType` parameter can eaily be replaced by a `VehicleType` enumerator. 

Now we can use this Factory in our client to create a new type of `Vehicle`:

```java 
public static void main(String[] args) {
    VehicleFactory vehicleFactory = new VehicleFactory();

    Vehicle carInstance = vehicleFactory.getVehicle("CAR");
    Vehicle bikeInstance = vehicleFactory.getVehicle("MOTORBIKE");
    Vehicle truckInstance = vehicleFactory.getVehicle("TRUCK");
}
```

Now we can freely add more types of vehicles with changes only needed in one place to cater for the new functionality. If there are many subclasses, consider using the _family of factories_ approach where you have a base factory and extend children factories to handle the creation of all the individual products.

Looking back at the Factory Pattern you may find yourself asking "that is a lot of work for something as simple as decoupling, where would this be used". The answer to that is evreywhere. Factories are used in almost every library and every major framework; they play a vital role in providing extendability and loose coupling to the various libraries you use in your programs. Anywhere where there is a _getinstance()_, _getObject()_, or _createSomething()_ that is a call to a factory. 

> The entire _Spring Framework_ makes use of classes called `Beans` and every `Bean` needs to be created by a `BeanFactory`.

### Builder

It is a creational design pattern that lets you construct complex objects step by step. The pattern allows you to produce different types and representations of an object using the same construction code.

A complex object normally requires step-by-step initialization of many fields and nested objects. This code is usually buried inside a scary constructor with lots of parameters - that are often NULL - at best, or at worst, all over the application.

> The Builder Pattern provider better control over the construction process.

The Builder pattern suggests that you extract the object construction code out of its own class and move it to separate objects called builders. This removed the need to have multi parameter constructors which result in a great deal of confusion. It has the added benefit of being far easier to extend the functionality of the class.

> You will create objects step-by-step with a `Builder` and get a new instance of that object from the `Builder`.
 
Builder adds complexity to the program through many additional classes, be careful of using it. Sometimes it is easier to try find a solution where a complicated constructor is not needed.

> The implementation of this pattern is self study

## Conclusion

This lesson covered Design Patterns. They are testable, proven solutions to common problems in software development first conceptualized in the early 1990s. Their creation was done to achieve more testable, maintainable, and resuable code. 

Design patterns can be categorized into three groups: creational, structural, and behavioural. There were originally 23 conceptualized, but many more have been added over the years. They are widely used in all enterprise software solutions and frameworks. In this lesson we had a closer look at five common patterns: Singleton, Strategy, Observer, and Factory. The alternative to Factory, Builder was also introduced. 

---

Copyright 2021, Noroff Accelerate AS
