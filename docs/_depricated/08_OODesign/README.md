# Object Oriented Design
In this lesson, object-oriented best practises will be explored. This includes an introduction to the SOLID principles and how they facilitate clean, maintainable, testable code. The lesson will focus on each principle individually and show their use by cleaning up an existing codebase. Other concepts that are introduced are KISS, DRY, and YAGNI.

**Learning objectives:**

You will be able to...
- **describe** the five SOLID principles.
- **discuss** why it is beneficial to develop clean, maintainable, and testable code.
- **assess** how well given code is aligned with SOLID principles.
- **modify** existing code to align with SOLID principles.
- **explain** and **assess** the roles of KISS, DRY, and YAGNI.
- **discuss** what design patterns are, and the role they have in industry.
- **describe** the various categories of design patterns.

---

Knowing how to do Object Oriented Programming is half the battle, doing it accordance with best practices is the other half. The main reason why this is the case is due to one simple fact: 

> For enterprise solutions, you are not the only developer working on the project and that project is going to go through multiple iterations.

With this is mind, as a developer you want to create programs that are easily _readable_, _reusable_, and have a clear _purpose_ in mind. This results in less time and money spent on integrating code as there is a lower likelyhood of an error occuring. 

Due to the iterative nature of enterprise solutions, especially in todays world of Agile development, there is a strong requirement that writen code be easily extended on - this means that you should write code that can have features added on, while maintaining its original functionality. This, coupled with the need for automation, results in _testing_ being an integral part of the Software Development Lifecycle.  

As seen previously, unit testing is a powerful tool that can be leveraged to iterate in small steps on existing code at a rapid pace  while still ensuring functionality. For this reason, writing code that is easily testable is a vital component of good programming design. 

There are several principles and guidelines that help developers write better code, this lesson covers some of the most popular ones. 

## Coupling
Coupling is more of a metric, it is a way to measure how much various software components depend on each other. Interrelated classes are either _tightly_ or _loosely_ coupled.

Classes that are _tightly coupled_ are more difficult to reuse in isolation as they rely on other classes to function. This is problematic when considering the goal described in the introduction to this lesson. _Loosley coupled_ classes have higher portability and reusability allowing easier extension and modification without breaking other aspects of the system. It is clear that developers:

> Strive to develop loosley coupled classes

So far, tight and loose coupling has been more discussed in terms on their outcomes rather than what these individual components mean.

**Tight coupling**

A more formal definition of tight coupling can be described as:

> If A knows more than it should about the way in which B was implemented, then A and B are **tightly coupled**.

Classes are seen as tighly coupled when they change together, meaning; when class A changes, class B has to as well. Another side effect of tight coupling could be sharing many dependencies, resulting in difficulty maintaining and modifying the code.

The tightest form of coupling is when one class modifies the data of another class, i.e. the class itself is passed and is acted on. This is called `content coupling` and is something that should be avoided at all costs.

**Loose coupling**

On the other side of the spectrum is loose coupling, something developers should strive towards. A more formal definition of loose coupling can be described as:

> If the only knowledge that class A has about class B, is what class B has exposed through its interface, then class A and class B are said to be **loosely coupled**.

The ideal state is to have classes communicating purely by `data`, resulting in the classes being completely independent. This data could be the exchanging of data through primitives or an interface. 

## Cohesion 
Cohesion is another tool to measure class similarity, unlike coupling, cohesion focusses on the elements of a `single class`. It is a way to measure the degree to which elements are functionally related - in other words, are the parts of the class related to doing the same thing. 

Cohesion can either be _high_ or _low_. Cohesion often goes hand in hand with coupling, where classes that have `high cohesion` individually, often have `loose coupling`. 

**High cohesion**

Cohesion can be visualized like a glue, if everything is sticking together, the cohesion is high. What does this _sticking together_ mean? If elements (methods) of the class are serving the same purpose, i.e. various ways to print lines from a text file, then they are sticking together; resulting in high cohesion. This is linked to responsibility, if a class has one responsibility and all its elements serve that responsibility its cohesion is high.

**Low cohesion**

When elements of a class are doing different things, having various responsibilities, the class is unfocussed and therefore has low cohesion. This results in the only relationship being location for the various elements. Taking the previously discussed example of printing; if there was a method added that coverted the text line into [pig latin](https://en.wikipedia.org/wiki/Pig_Latin#:~:text=Pig%20Latin%20is%20a%20language,to%20create%20such%20a%20suffix.) added to the class, the cohesion would lower. The ideal fix is to have a class dedicated to various ways of converting text and then make use of the printing class to print the converted line - hightening cohesion and loosening coupling. 

> You cannot attach a quantity to cohesion and coupling, it is more abstract and comes from experiencing both well written and poorly written code. 

Due to its abstract nature, cohesion and coupling can be difficult to grasp for newer developers. Luckily, this is an old problem and far smarter people have created principles and guidelines to help ensure loose coupling and high cohesion. The most well known is the SOLID principles. 

## S.O.L.I.D Principles
In the early 2000s, a prominent figure in the movement for writing better code, Robert C. Martin, proposed a set of principles in his paper ["Design Principles and Design Patterns"](https://web.archive.org/web/20150906155800/http://www.objectmentor.com/resources/articles/Principles_and_Patterns.pdf). These principles make software designs more understandable, flexible, and maintainable. Since then, the SOLID principles have been highly incorperated into enterprise solutions and and are often the subject of many `technical interviews for junior developers`.

These principles offer guidance on common methods of achieving loose coupling and high cohesion. They are powerful when wanting to scale applications as the complexity of the systems are lowered. An added side effect of this, is that systems are easier to test. Each letter in the SOLID acronym represent a principle, the following sub-sections detail each principle.

### Single Responsibility
The Single-responsibility Principle (SRP) states:

> A class should have one and only one reason to change, meaning that a class should have only one job.

This helps in a number of ways: 

- It improves testing as there are fewer test cases per class.
- There is less functionality and dependencies in the classes, therefore coupling is looser.
- Smaller, packaged classes are easier to search - improving overall project organisazation. 

**Example of the principle**

This example has a `Book` class, it has some methods relating to a manipualting a book.
```java
public class Book {
    private String name;
    private String author;
    private String text;

    // constructor, getters and setters

    // methods relating to book properties
    public String replaceWord(String word) {
        return text.replaceAll(word, text);
    }

    public boolean isWordIntext(String word) {
        return text.contains(word);
    }

    // method to print book
    public void printText() {
        System.out.println(text);
    }
}
```

This class has the following methods: `replaceWord`, `isWordInText`, and `printText`. These methods seem related, however, there is an outlier - `printText`. This method has the reposibility of printing the text, whereas the other two methods have the responsibility of manipulating the text. Furthermore, is has a tight coupling to the `System.out.println` method, limiting the way in which text can be displayed. What if there needed to be an expansion to print text in different ways - this class needs to change everytime (this problem is solved in the Open/Closed principle). 

To adhere to the `SRP` we need to extract out the printing method to a different class that has the responsibility of printing text in various ways. Replacing the code with a call to a different class, and simply passing the text (as data) to that class. This both _increased cohesion_ and _loosens coupling_ as we are communicating with data between two smaller, cohesive classes. 

```java
public class BookWriter {
    // methods to print the book, from Book class
    public void printTextToConsole(String text) {
        System.out.println(text);
    }

    public void logTextToFile(String text) {
        // logic to log the text
    }
}
```

This example seems straight forward when there is a step by step process and the violation is this clear. However, this is very nuanced and requires knowledge of the problem, business needs, and the applications artictecture. An easy way to approach this is to ask "what is this class meant to do? Is it doing one thing?" and try not overthink it and end up in [analysis paralysis](https://en.wikipedia.org/wiki/Analysis_paralysis#Software_development).

> SRP violations are almost always corrected on review. Meaning, it is very difficult to pick up on the violations as the code is being written. So dont worry if your classes arent perfect the first time.

### Open/Closed
The Open-closed Principle (S.R.P.) states:

> Objects or entities should be open for extension but closed for modification.

This principle is pretty self explanatory by its definition, inheritence and interfaces help greatly with adhearing to this principle. 

**Example of the principle**

> This example may not be how you would model this, it is for illustration.

Let us take the scenario of modelling a calculator, we want to create an interface to refer to any calculator operation, called `CalculatorOperation`. Then we can make an `Addition` operation implementing that interface and make a `Calculator` class to perform the operation. 

> This example contains multiple classes in one file, this is for illustration purposes and is not possible with the Java compiler.

```java
public interface CalculatorOperation {}

// Addition class
public class Addition implements CalculatorOperation {
    private double left;
    private double right;
    private double result = 0.0;
    // getters, setters, and constructor
}
// Subtraction class - similar to Addition

// Calculator class to perform operations
public class Calculator {
    public void calculate(CalculatorOperation operation) {
        if (operation instanceof Addition) {
            Addition addition = (Addition) operation;
            addition.setResult(addition.getLeft() + addition.getRight());
        } else if (operation instanceof Subtraction) {
            // same as above but subtract
        }
    }
}
```

As you can see, if any extra operations like `Subtraction`, the `Calculator` class needs to be modified. This _breaks_ the Open/Closed principle. Fixing this problem requires a bit of rethinking of the approach.

We can keep the class for each operation, as well as how the operands are stored - this is to limit the amount of changes that need to be done to fix the violation. We can simply add a `perform` method to the `CalculatorOperation` interface, and instead of checking its instance type, we simply ask the operation to perform, and depending on its instance - it will do something different.

```java
public interface CalculatorOperation {
    void perform();
}

// Addition class with functionality
public class Addition implements CalculatorOperation {
    private double left;
    private double right;
    private double result = 0.0;
    // getters, setters, and constructor 

    // Perform method
    @Override
    public void perform() {
        result = left + right;
    }
}

// Improved calculator class
public class Calculator {
    public void calculate(CalculatorOperation operation) {
        operation.perform();
    }
}
```

This results in far less code being in the calulator class, and everytime we add a new operation nothing needs to be changed. The code is not OCP comlpliant, and as a bonus, loosely coupled. 

### Liskov Substitution
The Liskov Substitution Principle states:

> Let q(x) be a property provable about objects of x of type T. Then q(y) should be provable for objects y of type S where S is a subtype of T.

This means that every subclass or derived class should be substitutable for their base or parent class. To be substitutable, the subtype must behave like its supertype. This sounds fairly complicated, but its normally automatically adheared by following the other principles. 

The only thing that needs to be kept in mind, is overrriden methods and the data they return. Always try _override_ methods that have different functionalty that their parent, and not create new methods to handle the variation. This way, the program can substitute parent classes with base classes and the program should still function properly - this does not mean compile, this means the output is still what you expect. 

> This principle is very subtle and difficult to find violations, it is the least enforced in code reviews. However is normally completely avoided when following the other principles.

**Example of the principle**

This principle is best explained with an example, where a violation is forced. Let us assume you are trying to create a social media application that has various forms of `posts`, a general `Post`, a `Tag`, and a `Mention`.

> The `Database` class mentioned here is a dummy class, we assume all these methods are implemented properly. 

```java
class Post
{
    void CreatePost(Database db, string postMessage)
    {
        db.Add(postMessage);
    }
}

class TagPost : Post
{
    override void CreatePost(Database db, string postMessage)
    {
        db.AddAsTag(postMessage);
    }
}

class MentionPost : Post
{
    void CreateMentionPost(Database db, string postMessage)
    {
        string user = postMessage.parseUser();

        db.NotifyUser(user);
        db.OverrideExistingMention(user, postMessage);
        base.CreatePost(db, postMessage);
    }
}
```

To handle the posts, there is a `PostHandler` class:

```java 
class PostHandler
{
    private database = new Database();

    void HandleNewPosts() {
        List<string> newPosts = database.getUnhandledPostsMessages();

        foreach (string postMessage in newPosts)
        {
            Post post;

            if (postMessage.StartsWith("#"))
            {
                post = new TagPost();
            }
            else if (postMessage.StartsWith("@"))
            {
                post = new MentionPost();
            }
            else {
                post = new Post();
            }

            post.CreatePost(database, postMessage);
        }
    }
}
```

By looking at the code you will see that `MentionPost` posts by making use of the `CreateMentionPost` method, and the `PostHandler` class uses _polymorphism_ to refer to any `Post` subtype as a `Post`, and calls the `CreatePost` method. This results in the `MentionPost` case not being handled as expected when it replaces `Post` - because the `Post` parent class has its `CreatePost` method called as it isnt overriden in the child, violating the principle.

To fix this is fairly simple, instead of creating a method to handle the slight variation of how posts are created, simply override `CreatePost`.

```java
class MentionPost : Post
{
    override void CreatePost(Database db, string postMessage)
    {
        string user = postMessage.parseUser();

        db.NotifyUser(user);
        db.OverrideExistingMention(user, postMessage);
        base.CreatePost(db, postMessage);
    }
}
```

This results in all children of `Post` being able to substitute `Post` and everything works correctly. Keep in mind:

> This is a very obvious example, often the violations are far more subtle.

### Interface Segregation
The Interface segregation principle states:

> A client should never be forced to implement an interface that it doesn’t use, or clients shouldn’t be forced to depend on methods they do not use.

This principle is very similar to the `SRP` and `OCP` but for interfaces and behaviours, where we strive to break up larger interfaces into smaller, more focussed versions. Like other refactoring proceces, this is very time consuming but results in a much more flexible code. 

**Example of the principle**

To illustrate the pitfalls of not complying with this principle, let us consider an example of modelling payment systems. In this example we have a `Payment` interface which serves as the abstraction for all payments.

> Using Object as a type is a result of this being an example of design rather than a real world implementation.

```java
public interface Payment {
    void initiatePayments();
    Object status();
    List<Object> getPayments();
}
```

Now lets add a `BankPayment` implementation.

```java
public class BankPayment implements Payment {
    @Override
    public void initiatePayments() { // ... }

    @Override 
    public Object status() { // ... }

    @Override 
    public List<Object> getPayments() { // ... }
}
```

All is fine at this stage, however, when the functionality needs to be expanded on by making a new kind of payment, `LoanPayment`. This new payment has some requirements that dont yet exist, namely `initiateLoanSettlement` and `initiateRepayment`. These are then added to the existing code, resulting in:

```java
public interface Payment {
    // original methods
    ...
    void initiateLoanSettlement();
    void initiateRepayment();
}

public class LoanPayment implements Payment {
    @Override
    public void initiatePayments() { 
        throw new UnsupportedOperationException();
    }

    @Override 
    public Object status() { // ... }

    @Override 
    public List<Object> getPayments() { // ... }

    @Override
    public void initiateLoanSettlement() { // ... }

    @Override
    public void initiateRepayment() { // ... }
}
```

Now we have a problem, the `LoanPayment` and `BankPayment` now both need to implement methods they dont need, so `BankPayment` now needs to change:

```java
public class BankPayment implements Payment {
    @Override
    public void initiatePayments() { // ... }

    @Override 
    public Object status() { // ... }

    @Override 
    public List<Object> getPayments() { // ... }

    @Override
    public void initiateLoanSettlement() { 
        throw new UnsupportedOperationException();
    }

    @Override
    public void initiateRepayment() { 
        throw new UnsupportedOperationException();
    }
}
```

This is now violating the _interface segregation principle_, and if you have a keen eye, it also violates the _single responsibility principle_ and the _open closed principle_.

> Think about why these prinicples are violated.

> It is often the case that if code is violating one of the SOLID principles, it is also in violation of others.

This is due to the natural overlap and specific differences in scope of the principles. In these cases, it is best to pick the most specific principle that is being violated and make changes according to that.

In this case, to fix the _interface segregation_ violation, we make use of _interfaces_ AND _inheritence_ to create a heirarchy of interfaces to extract out common methods and have differences encapsulated in different interfaces. 

In this case; `status` and `getPayments` are common between _all types_ of payments, `initiatePayments` is Bank-specific, and
`initiateLoanSettlement` and `initiateRepayment` are Loan-specific.

```java
public interface Payment {
    Object status();
    List<Object> getPayments();
}

public interface Bank extends Payment {
    void initiatePayments();
}

public interface Loan extends Payment {
    void initiateLoanSettlement();
    void initiateRepayment();
}
```

This results in three interfaces; a parent `Payment`, `Bank`, and `Loan`, all with seperate logic with inheritence being leveraged for code reuse. The payment implementations now can choose which interface to implement. 
Allows the classes to only implement methods they need, and nothing more. Resulting in no more excpetions, cleaner code, fewer bugs, and no more SRP and OCP violations. 

```java
public class BankPayment implements Bank {
    // ...
}

public class LoanPayment implements Loan {
    // ...
}
```

**Aside: Interfaces vs Inheritance**

When looking at inheritance, it seems like the obvious choice for modeling heirarchies and differing behaviours. This is due to our window being limited to a smaller scale. In industry if everything was modelled exclusively with inheritence, there would be heirarchies so deep you would not be able to figure our what behaviours are applicable to your specific class. This is then compounded by the lack of flexibility that comes with inheritance - you HAVE to implement all the inherited behaviours.

We have already seen how interfaces attach behaviours to any class that implements them. For relationships deeper than grandparent-grandchild, it is far better to try model a flatter heirarchy and defer some behaviour to interfaces, or even a familiy of interfaces.

Composing classes with behaviours rather than try modelling a heirarchy is another popular approach. This notion of thinking is called "faviouring composition over inheritance" and is the basis for most of what drives the idea behind Design Patterns - where the knowledge of this lesson is the base. 

### Dependency Inversion
The Dependency inversion principle states:

> Entities must depend on abstractions, not on concretions. It states that the high-level module must not depend on the low-level module, but they should depend on abstractions.

This principle allows for decoupling. Instead of high-level modules depending on low-level modules, both will depend on abstractions.

> Abstractions should not depend on details. Details should depend on abstractions.

As a result we have code that is well structured, highly-decoupled, and reusable. The concept of "programming to abstraction rather than implementation" is a key driver in the formation of Design patterns. 

**Example of the principle**

To best illustrate the principle, we need to violate it by creating tightly coupled code. In this example we are modelling a computer store's computers. In our case, a `Computer` has a `StandardKeyboard` and a `Monitor`.

```java
public class Computer {
    private final StandardKeyboard keyboard;
    private final Monitor monitor;

    // constructor
    public Computer() {
        monitor = new Monitor();
        keyboard = new StandardKeyboard();
    }
}
```

This seems fine at first, until you look at it a bit further. What if we wanted to use a different keyboard? Another thing that is not clear until you think about is the _new_ keywords. Instantiating a _new Monitor_ and a _new StandardKeyboard_ tightly couples your class to those low-level implementations. Based on the dependency inversion principle, this should not happen. The ideal is to remove those low level dependencies and rather depend on abstractions. 

> **INFO** _Note_: Multiple classes in one file is for illustration purposes.

```java
public interface Keyboard { // ... }

public class StandardKeyboard implements Keyboard { // ... }

public public class Computer {
    private final Keyboard keyboard;
    private final Monitor monitor;

    // constructor
    public Computer(Keyboard keyboard, Monitor monitor) {
        this.keyboard = keyboard;
        this.monitor = monitor;
    }
}
```

Now the `Keyboard` is an interface and we have classes for each kind of `Keyboard` that implement the abstraction. As of this example there is no behaviour for the keyboards, this is just for demonstration purposes.

Another important change was the dependency inversion in the constructor where abstractions are passed, and through polymorphism, can be any type of Keyboard at runtime. An added benefit of this is that our class is far easier to test - something to always keep in mind. 


## Smaller principles

Aside from the SOLID principles, there are smaller principles that are not as impactful. However, these principles should still be in the back of every developers mind while writing code. The following sub-sections detail some common smaller design priciples.

#### D.R.Y
It stands for "Dont repeat yourself" and can be formally defined as:

> Every piece of knowledge or logic must have a single, unambiguous representation within a system.

The goal when writing _dry_ code is to divide logic into reusable units and call these units instead of rewriting them. This sounds very simple on the surface, but there are some repetitions that are not obvious at first glance and may require a code review to pick up. A good way to help strive for _dry_ code is to not write lengthy methods and try use existing code where you can. The `Single Responsibility Principle` and `Open/Closed Principle` only function when `DRY` is followed.  

**Benefits**

The largest and most beneficial outcome of having code written with a _dry_ mindset is that there is far less of it. This smaller codebase results in some benefical side effects; the developer spends less time and effort writing code, the code is easier to maintain, and there is a lower likelyhood of encountering bugs. 

A good example of _dry_ programming is all the helper classes in enterprise libraries. These are common bits of functionality that are written in one place, in isolation, that are used by other parts of the system. 

#### K.I.S.S
It stands for "Keep it simple, stupid". It is as simple as looking at a completeled method and asking yourself "have I made this too complicated, can it achieve the same goal in a simpler way". Much like _dry_, _kiss_ seems very simple and striaght forward on the surface. However, this is a never ending battle; as you gain more knowledge about programming or the frameworks you use, there will always be a simpler or more efficient way to do something. _Kiss_ is more of a "sniff" test when reviewing code, where you are trying to simplify and clarify your code. 

A very easy way of simplfiying a codebase is to keep methods as small and foccussed as possible - by adhearing to the `Single Responsibility Principle`. This way the maintenance is easier and bugs can be found faster. The refactoring tools in IDEs helps greatly with this process. 

#### Y.A.G.N.I
It stands for "You aint gonna need it". In essence, dont implement code and logic until it is needed; often extra methods or logic is written for a _possible_ functionality addition or expansion. If the codebase is designed well, in accordance with the SOLID principles, extending or adding functionality should be simple. 

This reduces the amount of bloat in the system, limiting the existing code to what is actually being used. This has several benefits, all relating to project costs; less time is spent writing tests for code not used, less time is spent developing unused code, and less time is spent reviewing unused code. Due to time being money in business, project costs are kept down - keeping all stakeholders happy. 

YAGNI is becomming less of an issue as software is developed in smaller faster iterations, due to Agile and other similar methodologies gaining popularity. There simply isnt enough time and space in these small iterations to ponder what could be added or altered. 

## Design Patterns

Design patterns are reusable solutions to common problems found in software development. These patterns are not concrete solutions, but rather templates on how these common problems can be solved in various situations. 

> **INFO** _Note_: Patterns are not algorithms. Algorithms define a clear set of actions that can achieve some goal, a patterns are a more high-level description of a solution. 

These patterns represent the various best practices used by experienced object oriented software developers, first shown in a book written by the "Gang of Four" entitled [Design Patterns - Elements of Reusable Object-Oriented Software](https://www.oreilly.com/library/view/design-patterns-elements/0201633612/). Since this, more patterns have been added, but they are still based on the original ideals set forth in this book.

The creation of Design Patterns was based on two principles:

1. Program to abstractions not implementations
2. Favour composition over inheritance

These two principles should sound very familiar. SOLID principles are heavily tied to Design Patterns.

### Advantages

Reusing design patterns helps to prevent subtle issues that can cause major problems and improves code readability for coders and architects familiar with the patterns. This is because Design Patterns are a common language of well-known, well understood names for software interactions.

Design Patterns save time as building software based on the experiences of other developers and best practices. Design Patterns help making your code more testable, which saves even more time as that refactoring does not need to be done. In additon to this, learning these patterns helps unexperienced developers to learn software design in an easy and fast way.

Due to Design Patterns being well-proven and testified solutions built on the knowledge and experience of experts, there is a high degree of transparency in the applications. 

> **INFO** _Note_: Design patterns are the industry standard approach

One thing to be careful of is the overuse of Design Patterns. Applying them everywhere will cause much more harm than good. When deciding on whether to use a Design Pattern or not, keep in mind the `KISS` principle covered in the `OO Design` lesson to see if much less complicated code would get the job done. 

> **WARNING** If all you have is a hammer, everything looks like a nail.

### Categories

There are many Design patterns that have been created over the years. They typically fall within one of three categories:

- Creational patterns
- Structural patterns
- Behavioral patterns

> **INFO** _Note_: There is a forth category, called J2EE Patterns, which deals with front-end related problems. 

The following sub-sections give an explanation of the categories.

#### Creational

These design patterns provide a way to create objects while hiding the creation logic, rather than instantiating objects directly using new operator. Increasing flexibility and reuse of existing code.

#### Structural
These patterns explain how to assemble objects and classes into larger structures while keeping these structures flexible and efficient.

#### Behavioural

These patterns are concerned with algorithms and the assignment of responsibilities and communication between objects.

Implementation of Design Patterns are outside of the scope of this course and should be revisited later once you have more experience.

To recap, this lesson looked at why developers want to create systems with good design and the benefits of it. Common metrics, namely coupling and cohesion, were covered and context given for each. The SOLID principles that help improve these metrics were also discussed with an example of each given and what each priciple prings to the table. Finally, smaller common principles to keep in the back of your mind were covered, namely; DRY, KISS, and YAGNI.

Two very important concepts were introduced in this lesson and play a vital role in understanding Design Patterns:

> Favour composition over inheritance.

> Program to abstractions and not implementations.

This lesson was a theoretical look at the principles behind good Object Oriented Design. You will begin to notice patterns in enterprise solutions or frameworks which adhere to these principles as your confidence grows with practice.

> **INFO** _Note_: For the unfamilair or newer students, this lesson may have felt a bit overwhelming and there is an uncertainty in everything you write now. Dont worry, it gets much easier with practice.

---

Copyright 2021, Noroff Accelerate AS
