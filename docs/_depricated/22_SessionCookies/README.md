# Cookies and Sessions

## Cookies

Cookies are key-value pairs that are stored on the users computer. When an HTTP request is made to a server, the browser will check if a matching cookie is found for the domain and send it with the request.

> The European Union has rightfully declared that _cookies are evil_, please familiarize yourself with the [GDPR](https://ec.europa.eu/info/law/law-topic/data-protection/eu-data-protection-rules_en).

There are two simple ways two add cookies to our server endpoints, and they will play a greater role when working on the front-end (along with local storage).

In a general sense, cookies let us store small amounts of information between pages and sessions.

Consider the following landing page endpoint:

```java
// Assuming Thymeleaf is being used for templating
@GetMapping(value = "/landing")
public String landing(@CookieValue(value = "userName", defaultValue = "") String userName) {
	if(userName.equals("")){
		return "newUser";
	}
	else{
		return "welcomeBack";
	}
}
```

This example start by checking if the request sent has a cookie attached with a key of `userName`, if none is found then a default blank value is assigned. This is typical because we would get a Missing cookie value exception if none is found (`IllegalStateException`).

This example represents a user that may have previously used our server, we can tell their system to keep a cookie of their `username` so we know to show them the login screen by default and not the default new user page.

If we know the user does not have a cookie, we could also add it the the response as follows:

```java
// Assuming Thymeleaf is being used for templating
@GetMapping(value = "/cookieMaker")
public String cookieMaker(HttpServletResponse response) {

	Cookie newCookie = new Cookie("cookieName", "cookieData");
    newCookie.setMaxAge(1000); // age in seconds, approx. 16 minutes
    response.addCookie(newCookie);

    // If we don't care about expiry
    response.addCookie(new Cookie("foreverCookieName", "foreverCookieData"));
    
    return "cookiePage"; // Any page would do here
}
```

In this case we need to use the `HttpServletResponse` object and attach the cookie(s) to that. 

This method adds the cookie to the users system, among other options we can set the cookie to expire after a specified timespan.

## Session

One of the biggest problems with `cookies` is that the user can easily manipulate them. The solution is to keep a record of the user on the server instead, but we don't want to repeatedly send login (authentication) credentials with each request. So we want to login the suer once and keep a record of them, but if we want the server to track something for the user we must manually do so ourselves. One of the easiest ways of tracking a user is to create a `session`. A `session` is a unique value that identifies a current web browser session, this is per browser and device.

> Session is really just a special _fast_ expiring `cookie`.

```java
@PostMapping(value = "/saveSession")
    public String add(@ModelAttribute("Password") Password password,
            		  HttpServletRequest request,
            		  HttpSession session,
               		  Model model) {

    if(password.value.equals("cat")){
        System.out.println("login successful");
        SessionKeeper.getInstance().AddSession(session.getId());
    }
    else
    {
        System.out.println("login failed");
    }
    return "redirect:/login";
    }
```

This code example uses a simple POJO called `Password` that simple has a `String` attribute called `value`. This allows for us to easily pass a string from Thymeleaf in the `HTML` back to the server code.

For session to function correctly, we _must_ add `HttpSession` to our endpoint signature. The actual session will exist regardless of weather we add this parameter of not, but we cannot access it without this addition. This functions much like the `HttpServletRequest` and `HttpServletResponse` objects do too.

Session has various methods associated to it, in this method we simple use the ID of the session for our own internal record keeping, but we could also have the server respond to a new session or optionally bind an object (only one) to the session.

The code example presented does a basic check to see if the user has enter the password of "cat" and if so uses the `SessionKeeper` to record the current `Session ID` as _authenticated_.

In other code (endpoints) we can use the same `SessionKeeper` to check if the `Session ID` matches an authenticated one by using the following code:

```java
if(SessionKeeper.getInstance().CheckSession(session.getId())){ 
	// Session ID is recognized  
}
```

This lets us avoid having to repeated get the `Password` object from the user on every endpoint, as the `Session ID` value is maintained so long as they do not close their browser itself.

> We could also optionally use a time-out to expire sessions after a while.

A working code example of cookies and sessions can be found [here](https://gitlab.com/noroff-accelerate/java/projects/simple-session-example)


### Passwords

Sending passwords as plain text over `http` is not a good idea. Ideally we should send them via a secure connection such as `HTTPS`, doing so requires setting up certificates. Luckily, Heroku servers are already certified, so if we deploy there we are _safe enough_, for now.

> The process for creating self-signed certificates on a Spring server is not a lengthy one, but beyond the scope of this lesson.

When we store the user password in our database we should use a one way hash of some sort. In practise it is a good idea to _salt and hash_ the password on the client side before securely transmitting it to the server for storage. (The server stores both the salt and the hashed password).

### SessionKeeper Singleton

This example uses a a `Singleton pattern` for the session keeper, this is a simplification and not ideal for distributed servers.

There are many examples of using sessions with Spring online, and they will suggest using a `Redis` database for storing. Both solutions store the session data in memory and are temporary. When writing production code the `Redis` solution is a better option, but requires more overheard/setup (a `Redis` server) whereas this sample code is _ready to run_.

---

Copyright 2021, Noroff Accelerate AS