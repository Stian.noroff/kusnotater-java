# JWT generation

This serves as a standalone way to generate a JWT and validate it with no Spring Security. This is if you are interested in playing with JWTs and dont want to have the learning curve or complex implementation of doing Spring Security. 

## Introduction

[jwt.io](https://jwt.io/) provides links to numerous libraries that can be used to integrate JWTs into a web application in a variety of different languages/platforms.

The example provided here uses [Java JWT](https://github.com/auth0/java-jwt), the complete source code for the server (Back-end only) can be found here: [source.zip](./resources/source.zip)

## Front-end Code

**This is only a toy front-end and not the intended usage of a JWT**

**index.html**:

```html
<!DOCTYPE html>
<html>
<head>
	<title>JWT tool</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

	<script type="text/css" src="index.css"></script>

	<script src="https://unpkg.com/axios/dist/axios.min.js"></script>

</head>
<body>
	<div class="container">
		<div class="row d-flex justify-content-center">
			<form>
				<div style="width: 400px; height: 200px;" class="form-group center p-10">
			    	<h1 class="text-center">JWT tool</h1>
			    	<textarea class="form-control p-2" id="tokenText" rows="3"></textarea>
			    <div class="d-flex flex-row">
			  		<div class="mx-auto p-2">
				    	<button 
				    		type="button" 
				    		class="btn btn-primary px-10" 
				    		onclick="getToken()"
			    		>GET</button>
					</div>

				    <div class="mx-auto p-2">
						<button 
							type="button" 
							class="btn btn-success px-10" 
							onclick="checkToken()" 
						>CHECK</button>
					</div>
				</div>
			  </div>
			</form>
		</div>

		<div class="row d-flex justify-content-center">
			<h1 id="invalidToken"></h1>
		</div>

		<div class="row d-flex justify-content-center">
			<div style="width: 250px; height: 250px" class="m-2 p-1 border border-danger rounded"><h3 class="text-center">header</h3>
				<pre id="textHeader"></pre>
			</div>
		  	<div style="width: 250px; height: 250px" class="m-2 p-1 border border-success rounded"><h3 class="text-center">payload</h3>
		  		<pre id="textPayload"></pre>
		  	</div>
		  	<div style="width: 250px; height: 250px" class="m-2 p-1 border border-info rounded"><h3 class="text-center">signature</h3>
		  		<pre style="white-space: pre-wrap;"id="textSignature"></pre>
		  	</div>
		</div>
	</div>
</body>

<script type="text/javascript" src="index.js"></script>
</body>
</html>
```

**index.js**:

```javascript
let tokenText = document.getElementById("tokenText")

function getToken() {
	clear()

	axios.get('/sign')
		.then(function (res) {
			tokenText.value = res.data.jwt
			document.getElementById("invalidToken").innerHTML = ""
		})
		.catch(function (error) {
			console.log("ERROR: " + error)
			document.getElementById("invalidToken").innerHTML = "ERROR GETTING TOKEN"
		})
}

function checkToken() {
	console.log("checking token ...")

	let token = { "jwt": tokenText.value }
	axios.post('/verify', token)
		.then(function (res) {
			if(res.data != "invalid"){
				document.getElementById("invalidToken").innerHTML = "VALID TOKEN"

				let jwt = res.data

				let header = { 'alg': jwt.algorithm, 'type': jwt.type }

				document.getElementById("textHeader").innerHTML = JSON.stringify(header, null, 2)
				document.getElementById("textPayload").innerHTML = JSON.stringify(jwt.claims, null, 2)
				document.getElementById("textSignature").innerHTML = JSON.stringify(jwt.signature, null, 2)
			}
			else
			{
				document.getElementById("invalidToken").innerHTML = "INVALID TOKEN"
				clear()
			
			}
		})
		.catch(function (error) {
			console.log("ERROR: " + error);
			document.getElementById("invalidToken").innerHTML = "INVALID TOKEN"
			clear()
		});
}

function clear(){
	document.getElementById("textHeader").innerHTML = ""
	document.getElementById("textPayload").innerHTML = ""
	document.getElementById("textSignature").innerHTML = ""
}
```

## Java Controller Code

```Java
// GET /sign
@GetMapping("/sign")
public Token signJWT(HttpServletRequest request){
    PrintInfo(request);
    Token token = new Token();
    //RSA
    RSAPublicKey publicKey = (RSAPublicKey) KeyLoader.getInstance().publicKey();
    RSAPrivateKey privateKey = (RSAPrivateKey) KeyLoader.getInstance().privateKey();

    if(publicKey!= null && privateKey != null) {
        Algorithm algorithmRS = Algorithm.RSA256(publicKey, privateKey);

        token.jwt = JWT.create()
                .withIssuer("Noroff Accelerate")
                .withClaim("name", "Ola Nordmann")
                .sign(algorithmRS);
    }
    return token;
}

// POST /verify
@PostMapping("/verify")
public DecodedJWT verifyJWT(HttpServletRequest request, @RequestBody Token token){
    PrintInfo(request);

    RSAPublicKey publicKey = (RSAPublicKey) KeyLoader.getInstance().publicKey();
    RSAPrivateKey privateKey = (RSAPrivateKey) KeyLoader.getInstance().privateKey();

    if(publicKey!= null && privateKey != null) {
        Algorithm algorithmRS = Algorithm.RSA256(publicKey, privateKey);

        JWTVerifier verifier = JWT.require(algorithmRS)
                .withIssuer("Noroff Accelerate")
                .build();

        try {
            DecodedJWT jwt = verifier.verify(token.jwt);
            if(jwt != null) {

                System.out.println(jwt.getClaim("name").asString());
                return jwt;
            }
            return null;
        }
        catch (JWTVerificationException exception){
            //Invalid signature/claims
            return null;
        }

    }

    return null;
}
```