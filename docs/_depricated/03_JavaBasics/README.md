# Java basics

This lesson covers the basics of the Java language. The various datatypes and data structures are discussed and given context of when their use is applicable. The common control structures present in all programming languages are detailed, as well as the Java-specific Stream API. Finally, classes and methods are introduced as a way to break up large code segments and create a more orderly code base. 

**Learning objectives**

You will be able to...
- **discuss** memory management in Java.
- **describe** the various data types and data structures present in Java.
- **apply** data types and data structures in appropriate situations.
- **design** your code with control structures to expand the functionality of your code.
- **demonstrate** refactoring into methods and classes, given a code base.

---

## Primitives

Primitive types are the most basic data types available within the Java language. There are 8: boolean, byte, char, short, int, long, float and double. These types serve as the building blocks of data manipulation in Java. Such types serve only one purpose — containing pure, simple values of a kind.

They can be categorized based on the type of data they store:
- Numeric - short, int, long, float and double. Simple arthimatic such as addition and comaprisons can be done to these types.
- Textual - byte and char. These types simply hold characters, Unicode or even numbers.
- Boolean and null - boolean and null.

**Wrapper Classes**

Each primitive has an associated class (wrapper class) that provides functionality to the data stored therein.

> **INFO** These classes usually have the same name as the primitive but follow Java class naming convention; the class must start with an upper-case character.

We can convert between data types using these wrapper classes. Take the example of requesting a number from a user through the console. This value comes through as a `String`, however, we can convert it to a number using the `parseInt` method on the `Integer` wrapper class.

```
String userInput = "34";
int userAge = Integer.parseInt(userInput);
```

As you can see, the wrapper class does not always have exactly the same name (`int` maps to `Integer`). `String` is a special type used for storing words. It is an array of `char`.

Let's now consider each of the primitive types and where we would use them:

**boolean**

The boolean type holds a single `true` or `false` value (case sensitive), we can equate this to the equivalent of `on` and `off` or `yes` and `no`.

```java
1	boolean isTheSunHot = true;
2
3	boolean canPigsFly;
4	canPigsFly = false;
```

On `line 1` we declare a `boolean` named `isTheSunHot` with an initial value of `true`.

On `line 3` we declare a `boolean` named `canPigsFly` without an initial value, in Java this primitive will be given an initial value of `false` (the equivalent of `0`). On `line 4` we specifically assign it the value of `false` to be safe.

**byte**

The byte type holds `8-bits` of data, in the form of a _signed integer_ value from `-128` to `+127` (`signed 8-bit integer`).

More specifically, the Java byte always has one of its eight bits reserved to designate the sign. This gives seven digits for the numeric value.
(Java bytes are _always_ signed).

Consider the following:

```java
1	byte num = 100;
2	num += 42;
3	System.out.println(num)
```

On `line 1` we declare a `byte` exactly the same as we did a `boolean`. We proceed to add `42` to `num` on `Line 2`. Finally, we output the result to the console on `Line 3`.

This produces an output of `-114`, and not `142`. This error is caused by an overflow, and `-114` is the _Two's Compliment_ of `142`.

If we exceed a value of `127` then the sign digit is flipped to negative and all other digits are zeroed, we then proceed to add the remaining numeric value to `-128`.

> **INFO** _Info_: Read about the [Two's Compliment](https://en.wikipedia.org/wiki/Two's_complement).

**char**

The char primitive is used to store one single Unicode character in `16-bits`, this gives four _hexadecimal_ digits for representing a single character. In practise, we don't normally need to worry about the actual bits, but the option is there.

We declare chars as follows:

```java
1	char letter1 = 'c'
2	System.out.println(letter1);
3
4	char letter2 = '\u26E9';
5	System.out.println(letter2);
6
7	char letter3 = '✓';
8	System.out.println(letter3);
```

On `line 1` we declare a `char` named `letter1` with a value of `c`.

- We must give chars a value, we cannot use it uninitialized
- We must encase the value in single quotes (`'`)

Similarly, on `line 4` we declare a `char` named `letter2`; in this example, we use the `\u` escape sequence with a value of `26E9`, which is the code for `⛩`(Shinto Shrine). This is useful if you want to directly type in the UTF-8 Unicode value for a particular character. It is also possible (and easier) to type the character directly, as shown with `letter3` on `line 7` (Check Mark).

**short**

The Java short holds `16-bits` of data, in the form of a _signed integer_. This means a value from `-32768` to `32,767`. This primitive type is typically used to help save memory in large use cases.

**int**

The Java int holds `32-bits` of data, in the form of a _signed integer_, however, the corresponding `Integer` class allows some method to use these `32-bits` as if it were an _unsigned integer_(Read the official documentation[^2] or this SO[^3] discussion to understand how this is achieved).

> **DOCUMENTATION** Read about integers from [Oracle](https://docs.oracle.com/javase/8/docs/api/java/lang/Integer.html).

**long**

The final integer primitive is the long, storing a `64-bit` _signed integer_. As with the int/Integer, it is possible to use the `Long` class if an _unsigned integer_ is needed.

**float**

The Java float primitive is a single-precision (signed) _IEEE 754 floating point_ value. The 32-bits are divided into 3 parts:

- `S`: sign (1 bit)
- `E`: exponent (8-bits)
- `F`: fraction (23-bits)

The float primitive is very versatile, especially in graphical calculations. While float appears to be very accurate, it is still a floating-point value and prone to encounter rounding errors.

> **INFO** _Note_: The official Java documentation make an explicit recommendation that float (and double) should not be used to store currency information. Instead the `java.math.BigDecimal` class should be used.

**double**

The double primitive is the `64-bit` implementation of the _IEEE 754 floating point_ standard.

With the following representation:

```
S EEEEEEEEEEE FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
0 1        11 12                                                63
```
## Collections

Collections are a means to store multiple values or objects together. The Java language supports a variety of useful operations when working with these collections, such as _inserting_, _removing_, _sorting_, and _searching_. There are many various types of collections in Java and we will only investigate some of the common, useful, or archetypal instances.

**Off-by-one error**

Java, as with most programming languages, uses _zero based indexing_ (counting starts from zero). This means that an array with _n_ elements will have elements number from `n` to `n-1`.

```java
n = 10
start: 	0
end:	9
```

If you make a mistake trying to access an element that does not exist, it will raise an “Index out of bounds” exception, specifically: `java.lang.ArrayIndexOutOfBoundsException`.


> **WARNING** Be careful when modifying collections while iterating through them; this is another common way to create runtime errors, such as the `java.lang.ArrayIndexOutOfBoundsException`.

### Array

An array is the 'primitive' version of the collection types, it is a fixed allocation of memory for a collection of a specific type.

This means that is must:

- have a type
- have a specified size

As an example, we can declare an array called `nums` that will hold `10` values of `int` by using either:

```java
int nums[] = new int[10];
```

or

```java
int[] nums = new int[10];
```

We will also need the `Util` library (as with most collections) add to the import statements at the beginning of our source file.

Specifically:

```java
import java.util.Arrays;
```

> **INFO** _Note_: The values in a new array are initialized to `0` (zero), `false` or `null`; whichever is the default for the enclosed type.

Arrays are simple and can be very memory efficient (and fast) if we know exactly how many values we will be working with. They do not, however, offer any dynamic sizing, so a change in the number of elements will necessitate the creation of a new array.

### Lists

If we need a dynamically sized collection we use a list, however, the base `List` class is _abstract_ in Java.

In practise, this means that we while can declare a `List`:

```java
List<> myList;
```

We cannot allocate memory (instantiate) for it:

```java
List<> myList = new List<>; // DOES NOT WORK
```

It is possible to make use of this base `List` class when you understand polymorphism, but for now we will avoid it and instead use one of the specialized versions, such as:

- ArrayList
- LinkedList
- Vector
- Stack

All `List` subtypes (specializations) support a common set of methods (tools):

- Add / Remove
- Get / Set
- Search
- Iteration
- Sub-setting

Along with this, `Lists` have three key features:

- Items are stored in an order
- Duplicate values are allowed
- Items are retrieved based on position in the list

Additionally, any number of items can be of the value `null`.

> **INFO** _Note_: The <> seen above has to do with generics, it will be discussed in more detail at that point.


**ArrayList**

The ArrayList is the easiest solution to use when working with a collection of variable size.
This collection is an automatically resizing `Array`, with all of the features of the `List` class.

When declaring it we must specify the type that it will contain:

```java
1	ArrayList<String> Names = new ArrayList<String>();
2
3	Names.add("Emma");
4	Names.add("Jakob");
5	Names.add("Lucas");
6	Names.add("Sophie");
7
8	for(String name : Names) {
9		System.out.println(names);
10	}
```

In this example, we have created a collection of type `String` on `Line 1`. We individually add 4 names to the collection on `Lines 3 - 6`. Finally, we use a `for-each` statement to print each name from the list on `Lines 8 - 10`.

Besides adding items, most ArrayList methods will require the index of the item you want to access. For example: `Names.remove(2);`, will remove "Lucas" from the example above.

### Sets

In comparison to `Lists`, `Sets` have a very different usage, specifically when you want to have a collection of _unique_ items. The key features of a `Set` are as follows:

- Items are **NOT** stored in any order
- Duplicate values are **NOT** allowed (Only one `null` permitted)
- Retrieved based on set type

Some notable examples of `Sets` are:

- HashSet
- LinkedHashSet
- TreeSet

Of these, the `HashSet` is particularly useful.

**HashSet**

The `HashSet` uses hashing to store items, making for faster operations on larger collections. Other than that it can be used similarly to the `ArrayList`

```java
1	HashSet<String> hashNames = new HashSet<String>();
2
3	hashNames.add("Emma");
4	hashNames.add("Jakob");
5	hashNames.add("Lucas");
6	hashNames.add("Sophie");
7
8	for(String name : hashNames) {
9		System.out.println(names);
10	}
11
12  hashNames.Remove("Lucas"); 		// Not index based
```

The difference here is that the `HashSet` does not guarantee the order of items will be maintained, so the names _might_ be in a different order when we print them here compared to previously.

## Control Structures

Computers and the programs that we write are deterministic; given knowledge of the current state, we can predict the future states or steps that will be taken. Understanding syntax and order of precedence is how we build programs that achieve our goals.

In this section, we look at the most common Java control structures and how they are interpreted into operations (actions) by the compiler.

We can classify the execution into three broad categories:

- Sequential
- Selection
- Repetition

With these code structure, we are able to leverage variables and parameters to create all manner of behaviours in our programs.

**Flow control**

_Sequential flow of code_

In the case of sequential, we are referring to the normal order of execution of code.
For a single source file, it is executed first from the `main entry point`, then in an ordered fashion as you would read, left to right, top to bottom. Unless, of course, we encounter a branch of some sort.

**Selection**

_Branches and Decisions_

These structures give us the ability to make choices about which piece of code executes next based on a give pre-condition or at the occurrence of a defined event.

- if
- if ... else
- switch
- try-catch
- break\*

**Repetition**

_Multiples passes of the same code_

- for
- while

#### if

The `if` statement tests if a given `condition` evaluates to `true`, if it does then the enclosed code is executed. If the condition evaluates to false, then the code is completely skipped.

**Syntax**

```java
if ( condition ) {
	// code that must execute if condition is true
}
```

When can use any code we want in the _condition_, so long as the return type is a boolean value.

```java
int num = 42 ;
if ( num >= 0) {
	System.out.println("Number is positive.");
}
```

**Boolean logic**

| Symbol | Meaning                  |
| -----: | :----------------------- |
|   `==` | equal to                 |
|   `!=` | not equal to             |
|    `<` | less than                |
|   `<=` | less than or equal to    |
|    `>` | greater than             |
|   `>=` | greater than or equal to |

#### if ... else

The `if ... else` provides an alternate piece of code to be executed if the `condition` evaluates to `false`.

**Syntax**

```java
if ( condition ) {
	// code that must execute if condition is true
} else {
	// code that must execute if condition is false
}
```

The `false` code block is often extended to include subsequent conditions, in this case we may use the `else if` shorthand:

```java
if ( condition1 ) {
	// code that must execute if condition1 is true
} else if ( condition2 ) {
	// code that must execute if condition2 is true
} else {
	// code that must execute if neither condition1 nor condition2 is true
}
```

There is also an `if` shorthand that can replace a full `if` statement if brevity is desired; often when a quick in-line condition evaluation is needed. This shorthand is known as a _Ternary_.

After stating the condition, we follow with a single `?`. We then add the `code-if-true` followed by a single `:`, then the `code-if-false`. As always, we end the line with a `;`.

**Ternary Syntax**

```

condition ? code-if-true : code-if-false ;

```

#### switch

If we were to find ourselves a with a long list of `if` and `else if` statements, it better to use a `switch` statement. The only caveat is that a `switch` statement tests for explicit values, not ranges.

**Syntax**

```java
switch(value) {
	case test_value1:
		// code that must execute if value matches test_value1
		break;
	case test_value2:
		// code that must execute if value matches test_value2
		break;
	default:
		// code that must execute if value does not match test_value1 or test_value2
}
```

We usually end each distinct statement with a break, unless we want the code to _fall through_ to the next statement. The break command instructs the execution to exit the current code block or control structure. In this case, it will immediately leave the switch block.

> **INFO** _Note_: Break can be handy if decide to stop a logical code block from continuing. Although they should be used sparingly outside of switch statements.

In the below example, `test_value1` has it's own distinct code, but shares code with `test_value2`, however it is completely valid to have multiple cases enter into exactly the same code block:

```java
switch(value) {
	case test_value1:
	case test_value2:
		// code that must execute if value matches test_value1 or test_value2
		break;
	default:
		// code that must execute if value does not match test_value1 or test_value2
}
```

As we stated previously, it is common to replace chained `else if` statements with a `switch` statement under certain conditions.
Given the following example of chained `else if` statements:

```java
int num = *** ;
if ( num == 0) {
	// code that must execute if num is 0
} else if ( num == 1 ) {
	// code that must execute if num is 1
} else if ( num == 2 ) {
	// code that must execute if num is 2
} else if ( num == 3 ) {
	// code that must execute if num is 3
} else {
	// code that must execute if num is any other value
}
```

We could instead write the following `switch` statement:

```java
int num = *** ;
switch(num) {
	case 0:
		// code that must execute if num is 0
		break;
	case 1:
		// code that must execute if num is 1
		break;
	case 2:
		// code that must execute if num is 2
		break;
	case 3:
		// code that must execute if num is 3
		break;
	default:
		// code that must execute if num is any other value
}
```

Finally, although we must have a `default` it may also contain no actual code.

#### try-catch

When performing certain actions (disk IO, user input, etc.) it may be impossible to predict the code execution. In this case, we use the `try` keyword followed by one or more `catch` statements.

> **INFO** _Note_: The `try` keyword indicates a code statement is _volatile_; prone to raise an exception.

The _try-catch_ has the following structure:

```java
try {
	// block of volatile code
} catch (Exception ex) {
	// code to handle exception
}
```

In practise it is common to chain multiple _catches_ after a _try_, **all** applicable exception will be executed in the order they are written. If we only wish one to execute, we can add `break` statements to the end of the exception code.

```java
try {
	// block of volatile code
} catch (SpecificException ex) {
	// code to handle SpecificException
} catch (LessSpecificException ex) {
	// code to handle LessSpecificException
} catch (VagueException ex) {
	// code to handle VagueException ex
}
```

#### for

When we want to perform a certain action a given number of times, we can use a _for-loop_.

The _for-loop_ is comprised of four parts:

- A setup
- A condition (test)
- An increment (or decrement)
- A body

These are executed as follows:

![For statement logical flow diagram](./resources/01_flow_for.svg)

**Structure**

In Java we write a _for-loop_ by using the following structure:

```java
for ( setup; test; increment ) {
	// body (of code)
}
```

**Setup**

This is used to _setup_ the control that we will use to control the _for-loop_ (_iterator_). Typically this will be an integer initialization.

```
int i = 0;
```

In this case, we create an integer `i` with a value of `0`.

This is executed only once, before any other part of the for structure.

**Condition**

This is used to _test_ if the _body_ must be executed again. Typically this is an expression, but it could be an independent function so long as the return type is a boolean value. If the result is `true` then to _body_ will execute, if it is `false` then _for-loop_ is skipped over and the code will continue.

```java
i < 10
```

In this case we check if `i` is less than 10.

**Increment**

We use the increment part of the for structure to make a modification to our _iterator_.

The increment is executed once per iteration, _after_ the _body_.

We could leave out the increment and make direct changes to the _iterator_ directly from within the _body_, but this can be very confusing to maintain.

```java
i++
```

This is the simplest case, we modify the _iterator_ by adding one each loop. In practise, we have a lot of freedom here.

It's also common to use _for-loops_ in reverse; starting with a large _iterator_ and counting down to zero.

**Body**

The _body_ is simply the code we wish to repeat.

```java
System.out.println(i);
```

In this example, we will simply print out the value of `i`.

**All together**

Putting all the code together we have the following:

```java
for (int i = 0; i < 10; i++) {
	System.out.println(i);
}
```

This example will print the numbers 1 to 10, each on a new line.

#### for-each

As we will see shortly, it is common to store value in collections; Java provides us with a _for-each_ (_enhanced for loop_) shorthand to iterate through collections.

**Structure**

```java
for( type variable : collection )
{
	// BODY
}
```

Once again we use the _for_ keyword, but the parts are different. We declare a _type_, this is the _class_ (or _primitive_) of the variables stored in the collection.

Next, we name a _variable_ that we will use to reference an individual instance of the variable as we iterate through the collection. This is equivalent to the `i` from the _for_ statement. The `:` is the word _in_.

Next, we specify the collection we wish to iterate through. As above, _body_ is simply the code we wish to repeat.

**Example**

```java
int[] numbers = new int[]{ 1,2,3,4,5,6,7,8,9,10 };

for(int num : numbers){
	System.out.println(num);
}
```

As above, this example will print the numbers 1 to 10.

#### while

The while is a simplified version of the for loop, having only a _condition_ and _body_, giving it the following logical structure:

![While statement logical flow diagram](./resources/02_flow_while.svg)

It is written as follows:

```java
while ( condition ) {
  // body
}
```

The _condition_ and _body_ are identical to those of the _for-loop_, the only exception being the _body_ must do one of the following:

- Modify some condition that will eventually terminate the _while_
- Contain a `break` (which will exit the code block)


## Generics

It’s common to see an upper-case T in code examples (and documentation). Usually, it will appear as `<T>`. T stands for _Type_, as in a type of `class`. When you use it in code you should replace it with a `class` that exists in your code base:

```java
ArrayList<T> myList = new ArrayList<T>(); 		// You cannot actually use T
ArrayList<int> myList = new ArrayList<int>(); 	// Here we have a list of integers
```

## Methods

Modularization in programming is the idea of breaking up logic into units of work. Each unit of work has a well-defined purpose. These units of work can then be sequenced to perform tasks which would accumulate to larger tasks. The benefits of this is that the units of work are reusable, reducing the need to repeat the writing of logic; Any errors which occur can also be isolated to a unit of work; Updating units of work only needs to take place in one place.

Methods are made up of two core parts, the _signature_ and the _body_.

```java
1	public void printHello() {
2		System.out.print("Hello, ");
3		System.out.print("World!");
4	}
```

`Line 1` is out method _signature_, up until the `{`. The `{` and `}` enclose the _body_ of out method (`Lines 2 & 3`). The _signature_ is further divided into four parts, three of which are optional.

Parts of a method _signature_:

- Access Modifier
- Return Type
- Method Name
- Parameters

> **INFO** _Note_: The last optional is _throws_. It is used with exceptions, and we will discuss its' use in later topics.

From the previous example:

```java
public void PrintHello()

Access Modifier:   public
Return Type:       void
Method Name:       PrintHello
Parameters:        - none -
```

These will be further discussed individually.

**Method Body**

The _body_ contains the code for the method. It has access to the parameters declared in the _signature_, but you may freely allocate new variables as needed (memory permitting). New variables will not exist outside the scope of the method body.

If the method signature has a non-_void_ return type, then _body_ must contain a return statement matching said type.

For example:

```java
public int GetNumber() {
	int num = 42;
	return num;
}

public int GetNumberSimpler() {
	return 42;
}
```

**Access Modifiers**

When dealing with variables, mathods, or objects (classes) we usually need to specify an access modifier; we must specify which other methods or objects have permission to access the current variable, method, or object.

This is done by using one of the _access modifier_ keywords, the most common ones are:

- public
- protected
- private

There is also the option of leaving of the keyword, this results in the _default_ access modifier.

The different levels of access provided are summarised here:

**Access Modifiers**

| modifier  | outside of package | subclass | within same package |
| --------- | ------------------ | -------- | ------------------- |
| private   | ❌                 | ❌       | ❌                  |
| default   | ❌                 | ❌       | ✔️                  |
| protected | ❌                 | ✔️       | ✔️                  |
| public    | ✔️                 | ✔️       | ✔️                  |

> **INFO** _Note_: Access within the current class (or nested classes) is automatically provided at all levels.

**Return Type**

A method must have a return type specified in its declaration. This must be either; a primitive type, reference type, or void. Unless you are using the `void` return type, the method _must_ contain a matching return statement for each possible outcome.

When reference types are copied, the underlying data is **not** copied; instead a new reference to the same data is created. If a true copy is required, this must be done manually. Reference types include class instances, interfaces, enums and collections.

**Void or null?**

If we do have a non-primitive return type but want to return nothing; we use the `null` object.

```java
public Customer getCustomer(int customerId){
	if(customerExists){
		return theCustomer;
	} else {
		return null;     // We must have a return
	}
}
```

Null _is_ an object, but one with no value assigned. If we do not want the method to _ever_ return a value, then we use the `void` keyword.

```java
public void printCustomer(int customerId){
	if(customerExists){
		System.out.println(theCustomer.name);
	}
	// We can skip the else part here
}
```

**Method Name**

The method name serves _mostly_ to help us identify it's purpose. Method names also help us ensure that each method is uniquely identifiable within the package (namespace).

Historically, method names in Java should be "should be verbs, in mixed case with the first letter lower-case, with the first letter of each internal word capitalized". Although, it is always best practise to be consistent with your projects (or organizations) naming convention.

**Parameters**

We must specify each parameter (variable) type that is required by the method, we must also explicitly state the name of each parameter. These parameters (with the specified names) are accessible only within the scope of the current method. The parameters may be left empty if no parameters are required.

**Overloaded Methods (and uniqueness)**

A method is only unique if it's `return type`, `name`, and `parameters` form a unique combination. This means that with a given return type and name (purpose), then we can have multiple _versions_ of the same method if we vary the parameters.

For example:

```java
public String getHelloMessage(){
	return "Hello";
}

public String getHelloMessage(String name){
	return "Hello " + name;
}
```

With code reuse in mind, we can restructure for better efficiency:

```java
public String getHelloMessage(){
	return "Hello";
}

public String getHelloMessage(String name){
	return getHelloMessage() + " " + name;
}
```

**Body**

The body is where we place the code for our method, and as stated previously: unless using a `void` return type, the method _must_ contain a matching return statement for each possible outcome.

For example:

```java
public boolean isEvenLength(String name){
    int length = name.length();
    if(length % 2 == 0){
        return true;
    } else {
        return false;
    }
}
```

---

Copyright 2021, Noroff Accelerate AS

