# Databases and JDBC
This lesson introduces the concept of persistent storage through a data store. It covers the ACID principles by which all relational database providers adhere to. Structured Query Language (SQL) is covered to gain an understanding of manipulating data between a data store and a Java application through predefined create, read, update, and delete (CRUD) statements.

SQL commands are covered that relate to setting up a database schema. These include; creating a database, creating tables, creating constraints (keys), and setting up relationships between tables.

The Java Database Connection API (JDBC) library is also discussed and its practical use demonstrated. This library facilitates the process of sending queries to a database and handling responses.

**Learning objectives:**

You will be able to...
- **explain** what a relational database is.
- **define** the ACID principles that govern relational databases.
- **construct** a SQL statement to produce the expected output, given a report request.
- **select**, **combine**, and **organize** data from multiple database tables using the apprpriate SQL commands and paramaters.
- **construct** and diagram a database schema, given a scenario.
- **explain** the role JDBC plays in database communication.
- **demonstrate** database interaction in Java using the JDBC.
For this course, we will be working with _Relational Databases_ and using _Structured Query Language_ (SQL).

---

Databases are the primary means by which we store and retrieve data:

- Databases are comprised of tables
- Tables have rows and columns
- Columns are the attributes of your data
- Rows are unique instances of each set of attributes
- Rows are identified by a key (Primary Key)

> Rows are often called _records_.

Relational Databases as the name suggest have _relationships_ (links) between tables. The attribute in one table may refer to a key in another table (foreign keys).

## Relational Databases

Much like OO programming lets us model anything that exists, relational databases let us store that same information.

Although this is not always a perfect one-to-one mapping, there are technologies and frameworks that can be used to generate tables from objects or vice versa. For this lesson, we will focus on writing SQL commands our selves. In later lessons, we will use _Spring Data_ and _Hibernate_ to automate many of these steps.

> Some workplaces will value the speed that automation gives you, while other workplaces will expect the due diligence that writing your own commands ensures.

## SQL Primer

SQL provides a standardised way for interacting with various database technologies, while each such technology will have its own specific variant of SQL the basics will stay the same.

Those basics are the four `CRUD` operations (transactions):

- [C]reate
- [R]ead
- [U]pdate
- [D]elete

Usually, you will want at least one of each of these commands for each table in your database.

The [W3 Schools SQL Tutorial](https://www.w3schools.com/sql/) is a great resource for those without any previous SQL experience.

#### Create

This command is used to add new rows to existing tables.

```sql
INSERT INTO table_name (column1, column2, column3, ...) VALUES (value1, value2, value3, ...);
```

#### Read

This command is used to read one or more rows (from one or more tables).

```sql
SELECT column1, column2, ... FROM table_name;

SELECT * FROM table_name;
```

> You can get information from multiple tables with `INNER JOIN` and filter your selections with `WHERE`, `ORDER BY`, and `GROUP BY` clauses.

#### Update

This command is used to change (modify) the record in one or more rows (in one or more tables).

```sql
UPDATE table_name
SET column1 = value1, column2 = value2, ...
WHERE condition;
```

#### Delete

> This command destroys the database

This command deletes one or more rows (from one or more tables).

```sql
DELETE FROM table_name WHERE condition;
```

**Delete problems**

Imagine a database with three tables `Buildings`, `Floors` and `Rooms`. A building has one or more floors and a floor has one or more rooms. Besides the possibility of a floor spanning more than one floor (such as an atrium) this is a simple database structure.

If we were to delete a building, what do we do with the related floors? In practise this would trigger a cascading delete, all floors and rooms on those floors will be deleted. Depending on our intention this result may or may not be desirable.

**Rules for Deleting**

We must consider two things when deleting from a database:

Firstly, do not destroy (delete) data unless absolutely necessary, it is a good practise to always have an `Active` column in each table that lets you disable a given row. Your select statement can simply filter out inactive rows.

Secondly, because of [GDPR](https://ec.europa.eu/info/law/law-topic/data-protection/eu-data-protection-rules_en), we must always ensure that we have an audit-able means of permanently removing personal data from our databases.

While the first point is a simple best practise, the second should be considered a critical feature when developing database products.

#### Other SQL Commands

Beyond these four _primary_ commands there are various commands for creating tables, users and other parts of the database as a whole. These other commands generally relate to database administration and their syntax can vary slightly depending on technology.

## Working with a Database

When working with an existing database, there is a general set of process that we will always follow:

- Create and open a connection using the connection string
- Execute a statement
- Process the result set
- Close the connection

In this example, we will read basic information for `Customers` from the `SQLite3` version of the `Northwind Database`.

> Northwind is a sample database originally created for Microsoft Access that is often used for educational purposes.
>
> An SQLite3 version of Northwind is available [here](https://github.com/jpwhite3/northwind-SQLite3)

The complete code is presented here, but we will discuss components of it separately:

**Database Code**

```Java
import java.sql.*;
import java.util.ArrayList;

import customer;

// Setup
private String URL = "jdbc:sqlite::resource:Northwind_small.sqlite";
private Connection conn = null;
public ArrayList<customer> customers = new ArrayList<customer>();

try{
	// Open Connection
	conn = DriverManager.getConnection(URL);
	System.out.println("Connection to SQLite has been established.");

	// Prepare Statement
	PreparedStatement preparedStatement =
		conn.prepareStatement("SELECT Id,ContactName,City,Phone FROM customer");
	// Execute Statement
	ResultSet resultSet = preparedStatement.executeQuery();

	// Process Results
	while (resultSet.next()) {
		customers.add(
			new customer(
					resultSet.getString("Id"),
					resultSet.getString("ContactName"),
					resultSet.getString("City"),
					resultSet.getString("Phone")
			));
	}
}
catch (Exception ex){
	System.out.println("Something went wrong...");
	System.out.println(ex.toString());
}
finally {
	try {
		// Close Connection
		conn.close();
	}
	catch (Exception ex){
		System.out.println("Something went wrong while closing connection.");
		System.out.println(ex.toString());
	}
}

```

**customer.java**

```java
public class customer {
    private String customerID;
    private String contactName;
    private String city;
    private String phone;

    public customer(String customerID, String contactName, String city, String phone) {
        this.customerID = customerID;
        this.contactName = contactName;
        this.city = city;
        this.phone = phone;
    }

    // Getter and Setter code removed for brevity
}
```

### Setup

In the setup code we do three things:

- Define a connection string (`URL`)
- Instantiate a `Connection` object (`conn`)
- Create an ArrayList of `customers` to store the results

These are all reasonably straight forward, but the connection string does follow a specification:

```
jdbc:<subprotocol>:<subname>
```

In this example our `subprotocol` is `sqlite` (SQLite3) and the `subname` points to a resource file named `Northwind_small.sqlite`

The exact contents of the `subname` are determined by the `subprotocol` implementation.

> JDBC specification can be found [here](https://www.jcp.org/aboutJava/communityprocess/mrel/jsr221/index2.html), and Section 9.4 discusses the `DriverManager` and it's interaction with the connection string.

The `DriverManager` will use the first compatible drive that accepts the connection string.

As this project uses SQLite3, it needs to import a driver that can support the SQLite specification; for this example I used the [org.xerial:sqlite-jdbc](https://mvnrepository.com/artifact/org.xerial/sqlite-jdbc) library.

**SQLite JDBC**

`sqlite-jdbc` provided by `org.xerial`

Maven link: [org.xerial](https://mvnrepository.com/artifact/org.xerial/sqlite-jdbc)

### Open Connection

The `DriverManager` here is part of `java.sql`, we use it to establish a connection (open) the database.

We store this open connection as `conn`.

### Prepare Statement

In this example, we are retrieving the `Id`, `ContactName`, `City`, and `Phone` values for the customers store in the `customer` table. This is achieved by writing our SQL statement, and use the connections `prepareStatement()` function to create a new `PreparedStatement`.

We may create multiple `PreparedStatement` objects from a single connection.

**Variables in Prepared Statements**

Within the prepared statement, we may need to provide a variable to specify what we want from the query.

The simplest way to specify a value is to use a `?` in the statement, when would then need to provide a value after the statement **before** execution:

```java
PreparedStatement preparedStatement =
		conn.prepareStatement("SELECT Id,ContactName,City,Phone FROM customer WHERE Id = ?");
preparedStatement.setInt(1, 42) // Corresponds to 1st '?' (must match type)
```

This statement will only select the customer with `Id = 42`.

If we have multiple values, we would use multiple `.set` commands on the prepared statement.

### Execute Statement & Process Results

Calling the `executeQuery()` on the `PreparedStatement` will produce a new `ResultSet` of the retrieved data.

We iterate through this `ResultSet` to create the `customer` objects that we need, these are stored inside our `customers` ArrayList.

### Close Connection

The last thing that we _must_ ensure that we do is to close the connection. Different SQL technologies behave differently if you leave the connection open, but all of them will work as intended if you close the connection.

> Some SQL technologies/servers will have a hard limit of how many connections may be open and it is not feasible to wait for the server to close them automatically.

### try-catches

This code is an excellent example of when a `try-catch` block should be used, there are many possible points of failure that can occur when retrieving data over the internet and the `JDBC` expects you as the developer to handle them.

Additionally, things can even go wrong when we are trying to close the connection, so we need a second `try-catch` to enclose the `conn.close();` statement.

> RESOURCE Example repo using SQL Lite JDBC to access data. [Link](https://gitlab.com/noroff-accelerate/java/projects/oslo-class-exmaples/sqlite-simple-example).

---

Copyright 2021, Noroff Accelerate AS