# Working with Git

Git is a distributed version control software, allowing you to collaborate with others while maintaining _snapshots_ of your code at defined points. Git is often used in an online (distributed) fashion, but it can also be used solely locally on your device.
Collaborative software development

Git is a critical part of your future as a developer!

## Installation and Setup

In all instances, the installation of Git is relatively quick and easy.

On Windows:

Install `Git for Windows` available [here](https://gitforwindows.org/)

On Macintosh (You will need to install [Homebrew](https://brew.sh/) first):

```bash
$ brew install git
```

On Linux use your preferred package manager:

```bash
$ sudo apt-get install git
```

Once installed, you can check the version by running the following command:

```bash
$ git --version
git version 2.21.1
```

From this point, you could start using Git in an unsecured way, but for this course, we strongly advocate using SSH at all times.

## SSH-Keys

### Setting up keys

#### Open a terminal

**Windows:**

Use git bash:

- Open `Explorer`
- Right-Click inside any folder
- Select `Open Git Bash here`

> If you encounter permission errors later you may need to run Git Bash at an administrative level

**Mac or Linux:**

Any terminal will do!

#### Check if you already have a key

> If you already have a key **DO NOT** generate a new one!
>
> In that case, skip ahead to `Copying the key to the clipboard`

Run the following command from the terminal we just opened:

```bash
$ ls -al ~/.ssh
```

> These commands will not work from the Windows Command Prompt, they _may_ work from the Windows Powershell, but for consistency, we recommend using Git Bash.

The command above will display a list of files.
If any exist, they will be in pairs (one with no extension and another with a `.pub` extension).

This image shows my keys (and `known_hosts` file) on my MacBook:

![My Keys](resources/02_keys.png)

> These will either be `id_rsa` or `id_ed25519`
>
> The second half denotes the encryption used, `ed25519` is now recommended instead of `rsa` because of security concerns.

If any file exists then you already have a key(s)!

**The Public Key**

This file ends with `.pub` extension (`id_ed25519.pub`), and can be shared freely. It is used by others to verify your identity.

> Think of it as your photo ID, when somebody sees it they can confirm that you are who you say you are.

**The Private Key**

This file ends no extension (`id_ed25519`)

This is the private key, and should **NEVER** be copied or shared!

> If the public key is photo ID, then the private key would be your physical face.

![Never share private keys](resources/03_sharekeys.jpg)

#### Generating a key

> **Are you sure?**
>
> This process is non-reversible, and if you’ve used a key somewhere else you will need to update that one again with any newly generated key.

The following steps will generate a new key using `ed25519`, but read through all the steps before proceeding as there are several prompts that appear.

1. Start with the following command, using your e-mail instead of `you@example.com`:

```bash
$ ssh-keygen -t ed25519 -C "you@example.com"
```

2. Let the key be created in the default location:

```
/something/something/.ssh/id_ed25519
```

You will need to press `Enter`

3. You will be prompted to enter a passphrase (password) and repeat it after.
   > The prompt **will not** show characters (or `*`'s as you type)
4. When done, you will be presented a visual representation of your key.
5. Check if you have a key agent running:

```bash
$ eval $(ssh-agent -s)
```

You should be presented with a `pid` (process ID) and a number, this indicates that the process is running.

> The `ssh-agent -s` command will start the agent if it is not running.

6. Add your key to the agent:

```bash
$ ssh-add ~/.ssh/id_ed25519
```

At this point, you may be asked to enter the passphrase for the key.

The key is now generated and ready to be used to identify you, but you will need a separate key for each device (computer) that you want to be identified from.

Using the same e-mail address, it is possible to have multiple devices _registered_ as your identity, such as a desktop and a laptop. These different devices have unique keys that can have separate passphrases.

#### Copying the key to the clipboard

To use the key (such as on Github/Gitlab) you will need to copy it out of the `id_ed25519.pub` file, this is easy to achieve with one of the following commands:

On Windows:

```bash
$ clip < ~/.ssh/id_ed25519.pub
```

On Macintosh:

```bash
$ pbcopy < ~/.ssh/id_ed25519.pub
```

On Linux (install `xclip` first):

```bash
$ sudo apt-get install xclip
$ xclip -sel clip < ~/.ssh/id_ed25519.pub
```

> Within you account settings on Github/Gitlab you will need to paste the key in and give it an identifying name, it's a good idea to match this name to the computer itself.

#### Global settings

While the SSH key is very secure and has our e-mail address stored in it, there are still two settings we can set within the Git application to help it better _reflect_ our identity.

We start by setting our `global user name`, as follows:

```
$ git config --global user.name "your name"
```

This ensures that our git messages will have our name attached to them by default.

Secondly, we explicitly specify our e-mail address as the one we set in our key:

```
$ git config --global user.email you@example.com
```

These two steps may seem trivial, but in the long run, they will help us work easier with Git.

## Cloning

In a later lesson, we will be using SQLite and will need a JDBC Driver, and we will use a Driver that is part of the `org.xerial` package. Suppose while we are using this driver we encounter a bug. We should start by submitting an `Issue` on the [GitHub page for sqlite-jdbc](https://github.com/xerial/sqlite-jdbc).

We could as developers also download a copy of the source code and try to find the problem in the code ourselves. Looking at the repository (_repo_), we can see a green `Code` button. By pressing this button we will (depending on our account) have a few options:

- Download (Download as ZIP)
- Clone with HTTPS
- Clone with SSH

> On GitLab there is instead a blue `Clone` button.

### Download (Download as ZIP)

This lets us use our browser to download the current branch of the _repo_ as a compressed archive, this is useful when we just want to see how some piece of code was executed or when we will not be contributing back to the _repo_.

If we want to get an updated version of a _repo_ that we downloaded like this, then we will need to download the new version manually (overwriting existing files without regard).

### Clone with HTTPS

Cloning with HTTPS will let us use our git client to download a copy of the _repo_. Using the same client we can retrieve new updated versions of the _repo_ and update files with the git version control system.

Through using your account username/password pair you are able to contribute your changes to a remote _repo_. While GitHub suggests this as the recommend means of connecting, we disagree and instead suggest the use of SSH keys.

### Clone with SSH

Much like the HTTP clone, the SSH version will work with a git client and will let us easily retrieve changes. However, it will further give us the ability to contribute directly to the same _repo_ using our SSH credentials.

This will save a lot of time in the long run and is the preferred method that should be used when working with git. Through this method, the only devices that can modify your _repos_ are the ones that are manually set up by you, and they require unique passphrases to work.

**Problems with port 22**

If you encounter network related problem due to using port 22, you may add the following to the `config` file inside `~/.ssh/`:

```
Host gitlab.com
 Hostname altssh.gitlab.com
 User git
 Port 443
 PreferredAuthentications publickey
 IdentityFile ~/.ssh/id_ed25519
 ```

Remember to save the file, and you may need to restart you bash session.

## Remotes (Origin)

Remotes represent the different server hosted versions of your source code. These may be on physical computers you have set up or accounts with GitHub or GitLab, anything that is running the git server software.

> A guide to setting up your own server can be found in the [Git Server Documentation](https://git-scm.com/book/en/v2/Git-on-the-Server-Setting-Up-the-Server).

When you have one of these _remote versions of a repository_ we call it a _remote_.

We label these with convenient names, the default of which is `origin`.
You may freely name remotes for your own convenience using your git client.

Personally, I keep a duplicate of my class notes in a secondary remote that I have named `backup`.

## Branches

Branching is a vital part of collaboration. A branch is a different version of the same source code on the same server.

When we branch we create a new branch out of the previous one, this can be seen as a split. After the branch, we have the original version and the branched version. These can now be modified separately without affecting each other.

The default first version of your source code is called `master` (as in master record).

You may freely rename `master` or other branches as you see fit, but it is advised to retain the `master` branch.

When describing a branch we can also denote it's location, for example:

**Example Branches:**

```
origin/master
backup/master

origin/dev
origin/release
```

When referring to local branches (on your own computer) you do not include the remote name.

**Local Branches:**

```
master
dev
release
```

### Branch Names

Branch names should indicate either ownership or purpose or both.

So if I were to create a bug fixing branch (locally), I would name it:

```
craigm-bugfix
```

The convention of `owner`-`purpose` is a simple one that avoids any confusion.

#### Remote Branches

As we said earlier, you will have a `master` branch (local and remote), but it's not typical to create many remote branches. Instead, remote branches should match version, milestones or tasks:

**Remote Branch Examples**

```
origin/master
origin/test
origin/experimental
origin/dev
origin/release
```

### The golden rule of branches

**Branch early, branch often.**

You may always create as many branches as you need, in practise it's best not to do this on remotes, but say to do on your local machine.

Remote branches will be visible to all contributors and may create unnecessary clutter for them.

## Commits

> A commit is a `save point` for your code.

When you make changes to your code, you will periodically want to save those changes. These changes are different from you saving a file within you IDE. Instead, this is a concerted choice to record progress that has been made.

You may re-write and save a piece of code many times before you are happy with how it works; all the time-saving file changes and build new versions of the software. Not until you are satisfied you have made progress on a task will you want to contribute the changes towards the final version of the project.

> It is also a good idea to get in the habit of making commits at regular intervals throughout the day, such as before lunch or when packing up at the end of the day.

Even if you make further changes, you may always revert them back to a commit.

> Commits are made to the local versions of the _repo_, not to _remotes_.

### Commit Messages

Every commit you create must have a message that describes what the given commit has achieved.

Commit messages should always be two things:

- Meaningful
- Brief

A commit message may technically be multi-line, in practise only the first line ever gets looked at.

### Conventional Commits

Conventional Commits is a set of standards set out to help developers create better commit messages.

It is highly recommended that you follow the specification of [Conventional Commits](https://www.conventionalcommits.org) both in your personal projects and while working within teams.

There also have the added benefit that by using a commit message convention, you can create automated software changelogs for the project.

## Pulling & Pushing

These are the two methods that you will use to copy source code changes between the local _repos_ and the remote _repos_.

Pulling is the act of fetching changes from the remote. This will (by default) pull for **all branches** that exist on the remote, this is why it is a good idea to local development branches that exist only on your local computer.

> You should always _pull_ before you _push_. You have better (easier) control to fix problems on your own local machine.

Pushing is the act of sending your changes to the remote _repo_, but if anyone has made changes that are not compatible with what you have done, then you will encounter problems[^1].

[^1]: The sheer volume and types of errors that can occur cannot be covered here. Instead, we suggest you follow a diligent set of practises to avoid conflict creating scenarios.

## Merging

Eventually, you will need to save changes that are made on one branch into a branch closer to master (or master itself).

This is done through a merge.

### Merging Locally

Simply tell you git client to merge one branch into another.

For example, I could merge `bugfix` into `dev`. By doing this I am asking my git to calculate all the difference between the two branches and try to "squash" one into the other. If no conflicting changes have occurred, then the client can automatically merge the one branch into the other.

If conflicts do occur you will need to manually edit files to complete the merge. You git client may or may not visually assist you with this process.

> You may optionally delete the _side_ branch after the merge, so only the final branch remains.

This kind of merge is perfectly fine for you to do when working only with your own source code, but more typically you will be in a team and changes may affect source code beyond your own.

### Merging Remotely (Preferred)

The better way to merge branches is to create a merge request from the GitHub/GitLab website.

We can select one branch and create a merge request into another. We will need to write a short description to explain the purpose of the merge and optionally we can assign someone to review it.

> Always try to get another developer to review the merge for you.

The best practise in this scenario is to leave the assignee open and instead the first available developer should handle the merge request. depending on the project this reviewer may need to be a senior developer or someone with elevated rights on the remote _repo_.

## Visual Clients

There are many visual clients available that will help you perform all the task described here (and more), many IDEs even boast built-in git clients that can be very handy. Under the hood, all git clients will still perform the same git commands (via an internal command line).

My personal preference is [Sublime Merge](https://www.sublimemerge.com/), many students also enjoy [Git Kraken](https://www.gitkraken.com/). It is important to remember that while the client will help you with tasks, you must still understand which git command needs to be executed.

## Working with IntelliJ (and others) and git

Any project you create should always have the following two files:

- `README.md`
- `.gitignore`

### README.md

This file is really just a description of the project and its contributors, but you may add any information you desire here.

This file is written in markdown.

### .gitignore

There are some files (or folders) that you should not push to remote _repos_, typically these are files such as compiled source code, libraries that will be downloaded by your build system, sensitive files, or system files.

Git has a built-in system to handle this problem, it is called "ignored files", which are tracked in the `.gitignore` file.

> The purpose here is to keep the remote _repo_ clean and tidy, and to not track files that git cannot version control.

You may manually add to this file directly or thought git commands, but the easiest is to generate an encompassing `.gitignore` file by using the online tool at [gitignore.io](https://www.toptal.com/developers/gitignore).

[gitignore.io](https://www.toptal.com/developers/gitignore) requires that you list the operation systems, programming languages, build systems and IDEs that will affect your project; it will then generate the contents of a `.gitignore` file that you may save or manually copy across.

## A Git Reminder

These are the two rules that will help you avoid _most_ problems that you will encounter when working with git:

- Branch early, branch often
- _pull_ before you _push_

---

Copyright 2021, Noroff Accelerate AS
