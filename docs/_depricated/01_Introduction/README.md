# Java Introduction
This lesson introduces Java as a programming language, it covers the low-level operations of compilation. The Java Virtual Machine and Java Runtime Environment are detailed in the context of turning written code to a running application.

**Learning objectives**

You will be able to...
- **explain** what the Java Virtual Machine is, how it works, and its components.
- **explain** how your written code is compiled into a Java application.

---

Java is a general-purpose, class-based, object-oriented programming language used for application development. First released by Sun Microsystems in 1995 with the aim of being an all-purpose language that runs on any platform and with as few dependencies as possible. It is now maintained by Oracle.

The Java platform is a suite of programs that facilitate developing and running programs written in the Java programming language. A Java platform includes an execution engine (called a virtual machine), a compiler and a set of libraries; there may also be additional servers and alternative libraries that depend on the requirements. Java platforms have been implemented for a wide variety of hardware and operating systems with a view to enable Java programs to run identically on all of them.

To start, we are going to be working with the Java compiler via the console (terminal), we will not switch to using an IDE until we have covered _build systems_.

## Java Virtual Machine

A cornerstone of the Java language is the principle of "write once, run anywhere", this is accomplished by way of the Java Virtual Machine (JVM). The JVM is a standardised environment within which the compiled Java code will run.

The JVM is installed when we install the Java Runtime Environment (JRE) for our operating system. This allows our code to execute independently of any underlying hardware difference or operating systems.

Assuming a given target computing system has the same version of JRE installed as our development system, we can assume that our code will execute correctly on it.

The JVM provides a standardised platform for our code to run, thus the compiler need not compile code to the machine code level. Instead, it is compiled to **bytecode**. Bytecode is not human-readable and is compiled, although it cannot execute without the presence of the JVM.

In summary:
- Source code is **compiled** into bytecode.
- Bytecode is **translated** into processor instractions by _Just In Time (JIT)_ compilation using the _Java Virtual Machine_.

> **INFO** In practise, this means that while we as developers will need to have the Java compiler on our computer (from the JDK), the user will be required to have the JVM installed (from the JRE).

## Java Compiler

As mentioned previously, to produce the desired bytecode, we need to compile our code from source code.

The compiler will interpret all the code we have written and ideally produce an output file for later execution.
Compiled code is stored in files ending with `.class`.

**Errors are your friends**

If the compiler encounters a failure in logic it will produce an error. It is vitally important in your career as a developer to learn to read and understand errors. If you have a good understanding of the code you wrote and you are familiar with error types, it can be very quick to resolve an error when it appears.

**Example compilation commands**

Assume that we have written a single file solution and saved it as `program.java`.

We open a console window and navigate to the folder containing `program.java`.

We need to instruct the compiler to parse the source code file (`program.java`) and to produce bytecode. In this example we do not need to provide an output file name (or folder), thus the source file name is going to dictate the name the newly created file (which will be created in the same local folder).

```
program.java -> program.class
```

**Compiling `program.java`**

```bash
$ javac program.java
```

Here we are instruction the `javac` (Java Compiler) to parse the file named `program.java`.
If this works correctly it will simply create a bytecode file named `program.class`

**Executing `program.class`**

```bash
$ java program
```

Next, we instruct `java` (JRE) to execute our program `program`. Take note that we do not include the file extension `.class` in this command.

> **Console Commands**
>
> Command examples that are typed into a console will always be preceded by a `$`(dollar sign).
>
> You do not type this `$`, we use it here to differentiate console commands from other code blocks.

## HelloWorld

First we create a `HelloWorld.java` and place the following code into it.

```java
1	public class HelloWorld {
2		public static void main(String[] args) {
3			System.out.println("Hello, World!");
4		}
5	}
```

**Line-by-line Breakdown**

```java
1	public class HelloWorld {
```

Our first line is a `class` declaration. Here we have named the class as `HelloWorld`.
Because of this class name, we must name the source code file as `HelloWorld.java`.

> **INFO** Java requires one class per file, and the class name needs to match the file name.

We will discuss access modifiers in more detail later, but we mark this class `public` so that we can access it from outside the package.
Specifically, we want the JVM to execute this, so it needs access to some part of the code to start execution.

```java
2	public static void main(String[] args) {
```

This is the main (`main()`) entry-point to our program, and there should always be one for these for applications. This entry-point must follow a very specific pattern as we have done here.

There are a lot of other important keywords here; `public`, `static`, and `void`. We will also cover these in detail later, but now we can read this as meaning:

> "This is an accessible method called `main` that is always available in this class and does not return any result when it finishes executing."

```java
3	System.out.println("Hello, World!");
```

Inside the `main` method we use the `System.out` library, specifically the `println()` method. This library is included by default, so we don't need to include this library as we will need to for others.

Any string we put inside the `println` parentheses will be written to the terminal. So we provide the string: "Hello, World!".

```java
4		}
5	}
```

Finally, we demarcate the end of first the `main` method (line 4), and then the `HelloWorld` class (line 5).

**Compilation and Execution**

```bash
$ javac HelloWorld.java
$ java HelloWorld
Hello, World!
```

We compile and execute our code as explained earlier, and we see the words "Hello, World!" printed afterwards.

That sums up the basics of compiling a Java application using the CLI.

## Our first error

Let's take a quick look at some common mistakes.

Consider this program:

**program.java**

```java
public class hello {
	public static void main(String[] args) {
		System.out.println("Hello, how do you do?");
	}
}
```

Seems about right? Let's try to compile it:

**Compiling program.java**

```bash
$ javac program.java
program.java:1: error: class hello is public, should be declared in a file named hello.java
public class hello{
       ^
1 error
```

Our file has the wrong name because `program` (file name) and `hello` (class name) do not match.
The class name must match the file name.

Here we can make a call to change our class and file to something more descriptive, `Greeting`.

Let's fix up the program:

**Greeting.java**

```java
public class Greeting {
	public static void main(String[] args) {
		System.out.println("Hello, how do you do?");
	}
}
```

Changes:

- Renamed the class to `Greeting`
- Renamed the file to `Greeting.java`

And now we can try compilation again (and try executing it).

**Compiling and executing Greeting.java**

```bash
$ javac Greeting.java
$ java Greeting
Hello, how do you do?

```

---

Copyright 2021, Noroff Accelerate AS
