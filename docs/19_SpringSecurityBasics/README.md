# Spring Security Basics
This lesson covers the basics of Spring Security, Java's framework dedicated to security. Spring Security is introduced and some of its common uses are detailed. A focus is placed on authentication, this involves authenticating a user via a login, generating a JWT for that user and their login, and consuming/validating the token to allow or prevent access to protected resources. This is something done internally with Spring Security and is, therefore, an example of implicit auth flow. The users are managed in two ways, either using the built-in user manager or manually, both of these scenarios are covered.

**Learning objectives:**

You will be able to...
- **describe** Spring Security.
- **modify** a given application to be protected with Spring security.
- **generate** a access JWT through Spring security for authenticated users.
- **develop** a configuration to only accept valid issuers, given an access JWT 

---

Recall that [Spring Security](https://docs.spring.io/spring-security/site/docs/5.0.0.RELEASE/reference/htmlsingle/) is a powerful and highly customizable authentication and access-control addition built into the Spring Framework. 

> It is the standard for securing Spring-based applications.

Our goal is to secure a stateless application using JSON web tokens, or JWTs. This requires a great deal of configuration as Spring Security, by default, is oriented towards the traditional MVC approach where everything is rendered on the server (Thymeleaf). To get S_pring Security_ to work with _Single Page Applications_ and _Tokens_, we need to make some changes. 

> There is a lot of configuration needed to get Spring Security to behave this way.

To refresh your memory, we are going to be covering some common terms needed to understand Spring Security:

- **Authentication:** process of verifying the identity of a user, based on provided credentials. A common example is entering a username and a password when you log in to a website. You can think of it as an answer to the question *Who are you?*.
- **Authorization:** process of determining if a user has proper permission to perform a particular action or read particular data, assuming that the user is successfully authenticated. You can think of it as an answer to the question *Can a user do/read this?*.
- **Principle:** the currently authenticated user.
- **Granted authority:** the permission of the authenticated user.
- **Role or claim:** a group of permissions of the authenticated user. Normally as a set of *claims*.

The rest of this lesson is dedicated to going through the process of securing a Spring Boot application using JWTs. 

## Getting started

Before diving head first into Spring Security and all its wonderful configuraitons. We should take a step back and look at something we want to secure. We will use a basic _Spring Boot_ app with the _Web_ dependency as we have previously done - this is done through https://start.spring.io/. It will have our controller that we want to secure. An example is shown below.

```java
@RestController
@RequestMapping("api")
public class HomeController {
    @GetMapping("/public/resource")
    public String openToAnyone(){
        return "This endpoint is open to the public";
    }
    @GetMapping("/protected/resource")
    public String needsAuthentication(){
        return "This endpoint requires authentication";
    }
}
```

If we now run this application and navigate to `http://localhost:8080/api/public/resource` and `http://localhost:8080/api/protected/resource` we will see that we are able to access both without any issue as an anonymous user. This is not ideal, we should not be able to access the protected endpoint. To configure this we will use _Spring Security_. 

To include Spring Security we add the following dependency:

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-security</artifactId>
</dependency>
```

> Including this dependency will have an immidiate effect on our application.

Now everytime we navigate to any endpoint in our application, we are redirected to `http://localhost:8080/login` where an HTML page of a login form is presented. This is because by default, Spring Security secures ALL endpoints and uses traditional session based authentication. This works okay if the endpoints are hit in a browser, but through a service, recieving a response as an html page is not ideal. 

To login to this generated page, we use the default username of `user` and a generated password that can be found in the console (the terminal output of the application), in this particular case it was 

```
Using generated security password: ce7c8a6b-c052-4fff-af4a-6e317ae276b3
```
> Keep in mind this is generated everytime we run the application, so it will always be different. 

Once we enter the appropriate credentials we are then redirected back to our endpoint. The generated user and password, as well as sessions are stored internally in Spring Security's own user manager. While this is okay for traditional MVC applications using Thymeleaf and session based authentication, this is not the behaviour we want. We want stateless authentication with tokens. Now we need to add some configurations.

## Spring Security Architecture

To begin customising our applications to use tokens not sessions, we need to understand how Spring Security works, that is, how it actually handles this whole authentication process.

> The Spring Security Architecture is very complex, we will cover the major components. A detailed article can be found [here](https://medium.com/@satyakm.dev/understanding-spring-security-internals-with-code-walkthrough-850d5749252c#:~:text=Spring%20Security%20is%20a%20framework,LDAP%20(Lightweight%20Directory%20Access%20Protocol)).

> The direct article from Spring Security on its architecture can be found [here](https://spring.io/guides/topicals/spring-security-architecture).


**Filters Chain**

When you add Spring Security to your application, it automatically creates and registers a _filter chain_. This filter chain intercepts all incoming requests and decides what should happen based on the actual request details. As the name suggests, a filter chain is made up of many filters which all have individual responsibilities. These include, but are not limited to: checking public accessability of URLs, checking if the user is already authenticated (session based), and checking if the user has the authorization to perform the action.

> The filter chain sits between the client and the Http Servlet. This acts as a layer that intercepts requests like a gatekeeper before requests are sent to controllers.

![Filter chain](./resources/filter-chain.png)

These filters are in a specific order, and you can add your own filters infront of them (meaning that your filters will be the first line) by padding the _Spring Security_ filters back. You can do this in `application.properties` with `spring.security.filter.order=X` where `X` is the number of filters you want to be before the _Spring Security_ ones. 

> We will be creating a JWT filter to bypass the session-based authentication and instead use our JWT validation.

**AuthenticationManager**

You can think of this as a coordinator where you can register multiple _providers_, and based on the request type, it will deliver an authentication request to the correct provider.

**AuthenticationProvider**

This processes specific types of authentication. It is an interface, and it exposes only two functions:

- `authenticate` performs authentication with the request.
- `supports` checks if this provider supports the indicated authentication type.

> We will use `DaoAuthenticationProvider`, which retrieves user details from a `UserDetailsService`.

**UserDetailsService**

This is described as a core interface that loads user-specific data in the Spring documentation.

In most use cases, authentication providers extract user identity information based on credentials from a database and then perform validation. Because this use case is so common, Spring developers decided to extract it as a separate interface, which exposes the single function:

- `loadUserByUsername` accepts username as a parameter and returns the user identity object.

> We will use the `UserDetailsService` to manage our users in a `UserDetails` object. 


## Authentication Using JWT

This section will be the remainder of the lesson and it contains all the various aspects needed to configure an application to authenticate users via tokens and provide access control based on a role.

### Project setup and flow

We need the following Dependencies in our application when it is created through _Spring Initializr_:
- `Spring Web`
- `Spring Security`
- `Spring Data JPA`
- `PostgreSQL Driver`

**Controllers**

This application will have two controllers:
- `AuthController` handles signup/login requests
- `TestController` has accessing protected resource methods with role based validations.

_Login_ and _sign up_ should not be protected by Spring Security, meaning anyone can access them. The _test_ controller methods should only be accessable to authenticated who have the appropriate roles - there is an _all_ methods for any authenticated user. 

Our system has three roles:
- User
- Moderator
- Admin

**Endpoints**

The following endpoints will be exposed from our application:

| Methods | Uri | Action |
|---------|-----|--------|
| POST    | /api/auth/signup    |  Create new account      |
| POST    | /api/auth/signin    | Login with existing account       |
| GET     | /api/test/all    |  Retrieve public resources      |
| GET     | /api/test/user    |  Access user's content      |
| GET     | /api/test/mod    |  Access Moderator's content      |
| GET     | /api/test/admin    |  Access Admin's content      |


**Models**

There are two main models for; Authentication (User) and Authorization (Role). They have _many-to-many_ relationship.

- `User`: id, username, email, password, roles
- `Role`: id, name

These will be our `@Entities`, and are stored in our database. We will also create a `@Repository` for each of these models. 

> Connections to our database are configured in `application.properties`.

**Spring Security Additions**

We will create many classes as implementations of the available interfaces through Spring Security. These classes allow us to control the behaviour of our authentication and authorizaton. Each class will be listed below, with the abstraction it implements, as well as the role it plays in our application.

- `WebSecurityConfig` which extends `WebSecurityConfigurerAdapter`. This class provides HttpSecurity configurations to configure cors, csrf, session management, and rules for protected resources.
- `UserDetailsServiceImpl` which implements `UserDetailsService`. The `UserDetailsService` interface has a method to load User by username and returns a `UserDetails` object that _Spring Security_ can use for authentication and validation.
- `UserDetailsImpl` which implements `UserDetails`. This contains necessary information (such as: username, password, authorities) to build an `Authentication` object. 
- `AuthEntryPointJwt` which implements `AuthenticationEntryPoint`. This class will catch any authentication errors relating to JWTs.
- `AuthTokenFilter` which extends `OncePerRequestFilter`. The `OncePerRequestFilter` class makes a single execution for each request to our API. It provides a `doFilterInternal()` method that we will implement to parse and validate our JWT. It also allows us to load User details (using `UserDetailsService`), and check Authorization (using `UsernamePasswordAuthenticationToken`).
- `JwtUtils` which provides methods for generating, parsing, validating JWTs.

> This is a lot of classes to create, but it will always be the same each time. The more you do it, the easier it becomes as its a set structure.

> If we had done traditional session-based authentication we would have had all but two of the above classes - so its not that much more complicated to do tokens.

**Request and Response classes**

We do need some extra classes to handle the request and responses. This results in us having the following extra classes:

- `LogInRequest`
- `SignUpRequest`
- `JwtResponse`
- `MessageResponse`

**Resulting folder structure**

All these extra classes result in the following folder structure:

<img src="./resources/structure-packages.png" alt="drawing" style="width:300px;"/>

The same folder structure expanded to show the classes:

<img src="./resources/structure-classes.png" alt="drawing" style="width:300px;"/>

> It is easy to get lost in all these new classes at first, so lets look back at what we are doing.

**The process**

Before we continue with implementation, lets remind ourselves what we are actually trying to do. We are trying to protect certain endpoints, everything in the `TestController`, by forcing users to be authenticated. We are using JWTs to accomplish this by adding a filter to the _filter chain_ that _Spring Security_ creates. We are configuring what endpoints need to be protected in our `WebSecurityConfig`. Instead of redirecting the user to a generated login, we are going to return a 401 status code, and allow the client to handle that however they need. They can get access tokens by signing up and logging in through our endpoints.

We have got three scenarios we are catering for:
- User Registration
- User Login
- Accessing Resources


When a user registers, they do a _POST_ request to `/api/auth/signup` and provide a valid `SignupRequest` object which contains all the information needed to create a new user account. The server then adds them to the database if they dont already exist. The server then responses with a `MessageResponse` informing the user they have successfully registered. 

> No tokens are generated or validated for a sign up

When an existing user logs in, they make a _POST_ request to `/api/auth/signin` and provide a valid `LoginRequest` containing the users credentials. The server then authenticates these credentials and creates an access JWT, this JWT is signed with a secret on our server. The server then returns a `JwtResponse` with the JWT itself with some user information and authorities (what they can access/their role).

> Signing with a secret is a way for us to validate future access JTWs coming in with requests.

When a user attempts to access any protected resources they need to provide their access JWT as an _Authorization Header_. The server will check this JWT for validity using the secret we have stored on the server, the user's information (roles) are retrieved from our database and authenticated (the user info in the JWT exists in our database) and they are granted access based on their Authority (role). The server then responds based on their Authority.

> This is the _Access Control_ portion of the applicaiton. The whole reason we create all these security measures.

**Setting up our datastore**

We will be creating a `PostgreSQL` database to handle our users and roles. We will be using _Hibernate_ to handle all database interaction. The `application.properties` configuration used for this project is seen below.

> Keep in mind, your database credentials may be different.



### Security configuration

As mentioned in a previous section; to customize Spring Security, we need a configuration class annotated with `@EnableWebSecurity` annotation in our classpath. Also, to simplify the customization process, the framework exposes a `WebSecurityConfigurerAdapter` class. We will extend this adapter and override both of its functions so as to:

- Configure the authentication manager with the correct provider
- Configure web security (public URLs, private URLs, authorization, etc.)

```java
@EnableWebSecurity
@EnableGlobalMethodSecurity(
	securedEnabled = true,
    jsr250Enabled = true,
    prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // TODO configure authentication manager
    }
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // TODO configure web security
    }
}
```

> `@EnableGlobalMethodSecurity( securedEnabled = true, jsr250Enabled = true, prePostEnabled = true )` allows us to do some easy role checks in the controller methods themselves, this configuration is discussed when we get to the controllers.

**Configuring AuthenticationManagerBuilder**

The first step in our security configuration is telling _Spring Security_ how to actaully access and manipulate our internal users. This is done through the configuration of the `AuthenticationManagerBuilder`. 

Mainly, we rely on setting a `UserDetailService` via the `userDetailsService()` method. Lukcily, we already have our own implementation of this service as `UserDetailsServiceImpl`. We can pass that through as an arguement to tell _Spring Security_ how to access and manipulate our users.

> Recall, `UserDetailsService` is what Spring Security uses for authentication via the `UserDetails` class. The `UserDetails` interface has methods to: `getAuthorities`, `getPassword`, `getUsername`, see if the account is locked, and check for expirations. We need to provide this implementation ourselves.

> If using an external identity provider (like KeyCloak), we would not have to worry about any of this `UserDetailService`, or `AuthenticationManagerBuilder` configuration.

The second thing we need to do is provide a way that Spring Security should encrypt and cerify our passwords. We need to provide this as a `PasswordEncoder`.

> For our example we are using the [bcrypt](https://en.wikipedia.org/wiki/Bcrypt) password-hashing algorithm as it is the standard. If you dont specify this, it uses plain text, something you never want to do.

This results in our `protected void configure(AuthenticationManagerBuilder auth)` configuration being the following:

```java
@Autowired
UserDetailsServiceImpl userDetailsService;

@Override
protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
}

@Bean
public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
}
```

> `PasswordEncoder` is annotated with `@Bean` so it can be injected into our controller when we need it. Same reason for `AuthenticationManager` in the example repository.

The next step is to configure our http security. This is done by providing configuration to our other configure method.

**Configuring HttpSecurity**

This is where we actually provide our implementation of how to secure our application. In this method we are goin to configure:

- CORS
- Stateless session management
- Unauthorized requests exceptions
- Permissions
- Adding our JWT filter

All of this is configured in one large call the the HttpStatus builder:

```java
@Autowired
private AuthEntryPointJwt authEntryPointJwt;

@Override
protected void configure(HttpSecurity http) throws Exception {
    // Configuring http using the builder pattern
    http
        // Enable CORS and disable CSRF
        .cors().and().csrf().disable()
        // Set unauthorized requests exception handler
        .exceptionHandling().authenticationEntryPoint(authEntryPointJwt).and()
        // Set session management to stateless
        .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS).and()
        // Set permissions on endpoints
        .authorizeRequests()
        // Our public endpoints
        .antMatchers("/api/auth/**").permitAll()
        // Our protected endpoints
        .antMatchers("/api/test/**").permitAll()
        .anyRequest().authenticated();

        // Add JWT token filter
        http.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);
 }
// We do it this way, as a seperate method, so this can be injected into our services. 
@Bean
public AuthTokenFilter authenticationJwtTokenFilter() {
    return new AuthTokenFilter();
}
```

> We can set roles here manually with .HasRole(), but we are doing that via annotations in this example.

We disable CSRF and enable cors with the simple call of: 
```java
.cors().and().csrf().disable()
```

The first thing we need to do is configure the `authenticationEntryPoint`, we provide our implementation of the `AuthenticationEntryPoint` interface, `AuthEntryPointJwt`, which is passed throught the dependency injection framework. This tells _Spring Security_ that we are using JWTs as our first line of defense. It is represented by the call:
```java
.authenticationEntryPoint(authEntryPointJwt) 
```

The next step is enabling stateless session management (using our tokens instead). This is configured with a `SessionCreationPolicy` in the line:
```java
.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
```

Now that we have done some base configuration, we can now tell Spring Security which endpoints are protected by these measures and which are not.

The first is all our AuthController methods, these should not be protected as they are the mechanisms by which the users gain access to our server, shown in the line:

```java
.authorizeRequests().antMatchers("/api/auth/**").permitAll()
```

> The `**` configures all endoints that come after that layer in our heirarchy. This is a "catch-all" wildcard where we say anything in the _auth_ layer down is open.

Now, we need to configure the protected endpoints. We want everything in _test_ and lower to have all requests protected. This is done in the line:

```java
.antMatchers("/api/test/**").permitAll()
            .anyRequest().authenticated()
```

> Note that this doesnt enable authentication on those endpoints, its just listing the endpoints we want to be able to be protected. We still need to configure that aith annotations. This is done later.

> Ant matchers is the underlying mechanism in Spring that matches URI paths to controller methods.

The final step is adding our filter to the _filter chain_. Recall, we do this to stop the default session authentication and instead divert _Spring Security_ to use our JWTs. 

We added the `AuthTokenFilter` before the _Spring Security_ internal `UsernamePasswordAuthenticationFilter` with the `http.addFilterBefore()` method. We're doing this because we need access to the user identity at this point to perform authentication/authorization, and its extraction happens inside the JWT token filter based on the provided JWT token. `AuthTokenFilter`, which extends `OncePerRequestFilter`, has one method, `doFilterInternal()` where we provide the implementation of validating the token. 

Every request to the server that is trying to access protected resources will first face the `doFilterInternal()` method for validation. This is done on the line:

```java
http.addFilterBefore(authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);
```
With the accompaning method:
```java
@Bean
public AuthTokenFilter authenticationJwtTokenFilter() {
    return new AuthTokenFilter();
}
```

> We will go through the implementation of `AuthTokenFilter` in a later section.

> The usage of a @Bean and a method is important for injection, otherwise it cant be injected and we will run into issues with our filters being null.

Now we have configured our first line of defense, the next step is to actually implement how users are managed in our application.

### User service and details

If the authentication process is successful, we can get User’s information such as _username_, _password_, and _authorities_ from an `Authentication` object.

An example of this is shown below:

```java
Authentication authentication = 
        authenticationManager.authenticate(
            new UsernamePasswordAuthenticationToken(username, password)
        );

UserDetails userDetails = (UserDetails) authentication.getPrincipal();
userDetails.getUsername();
userDetails.getPassword();
userDetails.getAuthorities();
```

**User details**

For our purposes, we want more than just _username_ and _password_. We also want to access the user's _id_, and _email_. This is done by creating our own `UserDeatils` implementation. This is our `UserDetailsImpl` class.

> Our `Role` needs to be converted into `GrantedAuthority` to work with _Spring Security_.

This conversion is done in the `UserDetailsImpl`. Firstlym looking at the fields in this class:

```java
public class UserDetailsImpl implements UserDetails {
    // Fields
    private Long id;
    private String username;
    private String email;
    // We dont want the password to be shown, so we simply ignore it.
    @JsonIgnore
    private String password;
    // This is what our roles will be represented as
    private Collection<? extends GrantedAuthority> authorities;

	// Ommitted
}
```

> This class is a mirror of our User entity, with roles represented as `GrantedAuthority`s.

To convert our roles, we make use of the StreamAPI and a `SimpleGrantedAuthority` class. This class can take a single arguement in its constructor - the role it is associated with. The reason that we dont simple use the role name for everything and instead use a `GrantedAuthority` is because Spring Security does a lot of other things with it aside from check does role match. Our class makes use of the builder pattern to create this new `UserDetailsImpl`:

```java
public static UserDetailsImpl build(User user) {
    /*
        Here we use StreamAPI to create a list of GrantedAuthority from or role names.
        SimpleGrantedAuthority has a constructor that can take an argument that represents the role.
    */
    List<GrantedAuthority> authorities = user.getRoles().stream()
        .map(role -> new SimpleGrantedAuthority(role.getName().name()))
        .collect(Collectors.toList());

    return new UserDetailsImpl(
        user.getId(),
        user.getUsername(),
        user.getEmail(),
        user.getPassword(),
        authorities);
}
```

> `List<GrantedAuthority>` is important to work with _Spring Security_ and `Authentication` object later.

**User details service**

Recall, we need `UserDetailsService` for getting `UserDetails` object. The `UserDetailsService` interface that has only one method:
```java
UserDetails loadUserByUsername(String username)
```

We need to implement this and provide a method for _Spring Security_ to fetch our users from our users from our database, here we will use our `UserRepository` we made earlier:

```java
@Service
public class UserDetailsServiceImpl implements UserDetailsService {
@Autowired
UserRepository userRepository;

@Override
public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    // We fetch our user from our database and create a new UserDetailImpl with our database user
    User user = userRepository.findByUsername(username)
        .orElseThrow(() -> new UsernameNotFoundException("User Not Found with username: " + username));
    return UserDetailsImpl.build(user);
    }
}
```

Now that we have made the user details service, we are able to actually do something when we filter the requests. The next step is to then create the request filter, `AuthTokenFilter`.

### Creating our filter

As discussed earlier, we define a filter that executes once per request. So we create `AuthTokenFilter` class that extends `OncePerRequestFilter` and override `doFilterInternal()` method.

**AuthTokenFilter class**

This class makes use of out previously created `UserDetailsServiceImpl` class along with some new classes. The JwtUtils class is used to actually decode extract informtation out of our tokens, and we also want to implement proper logging in this class. As this is a vital component of our security system and we want to see everything that is happening inside it.

```java
public class AuthTokenFilter extends OncePerRequestFilter {
    // Dependencies
    @Autowired
    private JwtUtil jwtUtil;
    @Autowired
    private UserDetailsServiceImpl userDetailsService;
    /*
     Here we create a singleton constant logger
     We configure the logger to work with our AuthTokenFilter class.
    */
    private static final Logger logger = LoggerFactory.getLogger(AuthTokenFilter.class);

	// Omitted
}
```

> Details of the logger used can be found [here](http://www.slf4j.org/apidocs/org/slf4j/LoggerFactory.html), [here](http://www.slf4j.org/manual.html), and [here](https://stackify.com/slf4j-java/).

Now we need ot provide implementation of the `doFilterInternal()` method. With this implementation we are trying to acheive the following:
- Get a JWT from the `Authorization` header (by removing _Bearer_ prefix).
- If the request has JWT, validate it, and parse username from it.
- With the username, get `UserDetails` to create an `Authentication` object.
- Set the current `UserDetails` in `SecurityContext` using `setAuthentication(authentication)` method.

> _Security Context_ is how _Spring Security_ manages its security details, in the same way we use _Hibernates_ persistence context to access our domain objects. Information about _Security Context_ can be found [here](https://docs.spring.io/spring-security/site/docs/4.2.20.RELEASE/apidocs/org/springframework/security/core/context/SecurityContext.html).

The implementation of `doFilterInternal()` is shown below:

```java
@Override
protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
    try {
        // First we need to extract the JWT from the requests
        String jwt = parseJwt(httpServletRequest);
        /*
         If the token is present in the request header and it is valid
         according to our JwtUtil class, then we may proceed.
        */
        if(jwt != null && jwtUtil.validateJwtToken(jwt)){
            // Extract username from the JWT
            String username = jwtUtil.getUserNameFromJwtToken(jwt);
            // Use our UserDetailsService to fetch the user by their username
            UserDetails userDetails = userDetailsService.loadUserByUsername(username);
            /*
             We then use our UserDetails object and create and Authentication object out of it.
             In this case, its a UsernamePasswordAuthenticationToken.
            */
            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                    userDetails, null, userDetails.getAuthorities());
            /*
             We need to tell Spring Security how this authentication object was created.
             We did it through our http servlet.
            */
            authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpServletRequest));
            // Finally, we set the security context with our new authentication object
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }

    } catch (Exception e) {
        logger.error("Cannot set user authentication: {}", e);
    }

    // Here we are allowing the rest of the filter chain to continue.
    filterChain.doFilter(httpServletRequest, httpServletResponse);
}

private String parseJwt(HttpServletRequest httpServletRequest) {
    // Extract the Authorization header from the request
    String headerAuth = httpServletRequest.getHeader("Authorization");
    // Check if the Authorization header has a Bearer token
    if (StringUtils.hasText(headerAuth) && headerAuth.startsWith("Bearer ")) {
        /*
         If it does we return the token itself, which is the string minus "Bearer ".
         We use .substring to achieve this split
        */
        return headerAuth.substring(7);
    }
    return null;
}
```

Now everytime we want to get the current `UserDetails`, we need to access the `SecurityContext` by doing the following:

```java
UserDetails userDetails =
	(UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();

userDetails.getUsername();
userDetails.getPassword();
userDetails.getAuthorities();
```

We have not yet shown the JwtUtils class, it is where our tokens are created and verified.

**JwtUtil class**

This class has three functions:
- Generate a JWT from username, date, expiration, and secret
- Get username from JWT
- Validate a JWT

To do these functions, we are making use of the standard Jwt dependency for Java, [JSON Web Token Support For The JVM](https://mvnrepository.com/artifact/io.jsonwebtoken/jjwt), please ensure this dependency is added to your `pom.xml`.

Lets first look at the fields inside the `JwtUtil` class:

```java
@Service
public class JwtUtil {
	// Fields
	private static final Logger logger = LoggerFactory.getLogger(JwtUtil.class);

	@Value("${noroff.app.jwtSecret}")
	private String jwtSecret;
	@Value("${noroff.app.jwtExpirationMs}")
	private int jwtExpirationMs;

	// Omitted
}
```

The values `jwtSecret` and `jwtExpiration` are retrieved from our `application.properties` file.

> This is done so these values are treated as environment variables. We will then add these to our server we are publishing on and wont be exposing our sensitive information. For now they are in application.properties for simplicity.

Once again we are using a proper logger to log our errors. Next lets look at how we generate a token:

```java
public String generateJwtToken(Authentication authentication) {
    // Here we access the current user through Spring Security.
    UserDetailsImpl userPrincipal = (UserDetailsImpl) authentication.getPrincipal();
    // We use the builder pattern to create a JWT
    return Jwts.builder()
        // We set our subject as our user's username - this who the token is for
        .setSubject((userPrincipal.getUsername()))
        // We set the issued at date to now
        .setIssuedAt(new Date())
        // Expiration date is determined by our jwtExpirationMs value
        .setExpiration(new Date((new Date()).getTime() + jwtExpirationMs))
        // And we finally sign the token with our secret, this secret is important for validation.
        .signWith(SignatureAlgorithm.HS512, jwtSecret)
        .compact();
}
```

> The builder pattern is heavily utilized in security configurations.

Now looking at how we can extract the _username_ from our Jwt:

```java
public String getUserNameFromJwtToken(String jwt) {
    return Jwts.parser().setSigningKey(jwtSecret)
		.parseClaimsJws(jwt).getBody().getSubject();
}
```

> We retrieve the subject we set on creation using the _io.jsonwebtoken_ `Jwts` helper class.

Finally, this is how a token is validated, and how the various errors are handled and logged:

```java
public boolean validateJwtToken(String authToken) {
    try {
        Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
        return true;
    } catch (SignatureException e) {
        logger.error("Invalid JWT signature: {}", e.getMessage());
    } catch (MalformedJwtException e) {
        logger.error("Invalid JWT token: {}", e.getMessage());
    } catch (ExpiredJwtException e) {
        logger.error("JWT token is expired: {}", e.getMessage());
    } catch (UnsupportedJwtException e) {
        logger.error("JWT token is unsupported: {}", e.getMessage());
    } catch (IllegalArgumentException e) {
        logger.error("JWT claims string is empty: {}", e.getMessage());
    }
    return false;
}
```

> We make use of the `.parseClaimsJws()` method once more. Keep in mind the extraction of the username is done AFTER the token is validated.

The final step in the configuration of our filter is to create a class to handle `AuthenticationExceptions`. Keep in mind, if you run the application in its current state, without this class, nothing will happen. Every response will go through as if there was no security. We have not configured how to handle when things arent authenicated yet. To do that we need to implement `AuthenticationEntryPoint`.

The class has one method called `commence()` and it is responsible for handling the error. In this case we want to return a 401 Unauthorized to the client to let them know they need to go and get a valid access token by logging in.

```java
@Service
public class AuthEntryPointJwt implements AuthenticationEntryPoint {

    private static final Logger logger = LoggerFactory.getLogger(AuthEntryPointJwt.class);

    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        // We log what was wrong with the authorization
        logger.error("Unauthorized error: {}", e.getMessage());
        // Send a response to the client to let them know they are unauthorized, this is the 401 we see.
        httpServletResponse.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Error: Unauthorized");
    }
}
```

> Now we have completely setup our security. All that is left is to create controllers to handle all the logic.

## Using the security

Before we worry about user creation and roles, lets test the application and the `/api/test/**` endpoints.

Essentially, we want to limit certain endpoints to certain types of users. Recall `/api/test/all` should be able to be accessed by everyone, so we add nothing to it. Then `/api/test/user`, `/api/test/mod`, and `/api/test/admin` should only be accessed by those roles. 

> We also dont want any security on our `/api/auth/**` endpoints so we dont add any annotations. 

### Controller annotations

The Spring Security framework defines the following annotations for web security:

- `@PreAuthorize` supports Spring Expression Language and is used to provide expression-based access control before executing the method.
- `@PostAuthorize` supports Spring Expression Language and is used to provide expression-based access control after executing the method (provides the ability to access the method result).
- `@PreFilter` supports Spring Expression Language and is used to filter the collection or arrays before executing the method based on custom security rules we define.
- `@PostFilter` supports Spring Expression Language and is used to filter the returned collection or arrays after executing the method based on custom security rules we define (provides the ability to access the method result).
- `@Secured` doesn’t support Spring Expression Language and is used to specify a list of roles on a method.
- `@RolesAllowed` doesn't support Spring Expression Language and is the JSR 250's equivalent annotation of the @Secured annotation.

These are disabled by default and can be enabled in our `WebSecurityConfig` with 
```java
@EnableGlobalMethodSecurity(
    securedEnabled = true,
    jsr250Enabled = true,
    prePostEnabled = true
)
```

> Recall from an earlier section this was spoken of

- `securedEnabled = true` enables `@Secured` annotation.
- `jsr250Enabled = true` enables `@RolesAllowed` annotation.
- `prePostEnabled = true` enables `@PreAuthorize`, `@PostAuthorize`, `@PreFilter`, `@PostFilter` annotations.

After enabling them, we can enforce role-based access policies on our API endpoints. For our demonstration, we will use: `@PreAuthorize`.

> Feel free to experiment with the other Role options.

> Like `@RequestMapping`, these can be used at the controller or method level for control. I.e. if you want an entire controller to be restrcited to admins, you can add the annotation at the controller level.

Looking at our `TestController`, we can see these configurations in action:

```java
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/test")
public class TestController {
    // Endpoints
    @GetMapping("/all")
    public String allAccess() {
        return "Public Resources.";
    }

    @GetMapping("/user")
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public String userAccess() {
        return "User Resources.";
    }

    @GetMapping("/mod")
    @PreAuthorize("hasRole('MODERATOR')")
    public String moderatorAccess() {
        return "Moderator Resources.";
    }

    @GetMapping("/admin")
    @PreAuthorize("hasRole('ADMIN')")
    public String adminAccess() {
        return "Admin Resources.";
    }
}
```

Running the application and heading to `http://localhost:8080/api/test/all` should result in the following response:

![All resources](./resources/all_resources.png)

Then navigating to `http://localhost:8080/api/test/user` or `admin` or `mod` should result in the following response:

![All resources](./resources/user_resources.png)

What we need now is a way to gain access. This is done in our AuthController where we take in login information and validate it against our user base. Then a Jwt can be created.


### Creating users

For this process to work, we need some roles in our database. In this case, we want to have one of each role:

```sql
INSERT INTO roles(name) VALUES('ROLE_USER');
INSERT INTO roles(name) VALUES('ROLE_MODERATOR');
INSERT INTO roles(name) VALUES('ROLE_ADMIN');
```

> `ROLE_` is very important as this is how Spring Security prefixes all roles. Without this, no roles will work.

> Do this any way you wish. Simplest is to do it via `PgAdmin`.

Our `AuthController` contains all the dependencies we need to create and retreive user information:

> NOTE: `AuthController` is a prime candidate to be cleaned up with some extra services, for our example and simplicity, it is all left inside.

```java
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/auth")
public class AuthController {
    // From WebSecurityConfig
    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    // From WebSecurityConfig
    @Autowired
    PasswordEncoder encoder;

    @Autowired
    JwtUtil jwtUtil;

    // Endpoints ommitted
}
```
Here we make use of our repositories and the configured Spring Security classes: `AuthenticationManager` and `PasswordEncoder`. Looking at a way to login to our application:

```java
@PostMapping("/signin")
public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
    // Use loginRequest data to authenticate against our db with authentication manager.
    Authentication authentication = authenticationManager.authenticate(
        new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
    // Set the authentication context to our logged in user
    SecurityContextHolder.getContext().setAuthentication(authentication);
    // Generate a access Jwt for the user
    String jwt = jwtUtil.generateJwtToken(authentication);

    // Get the currently logged in user from authentication, and print out their roles for the response.
    UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
    List<String> roles = userDetails.getAuthorities().stream()
        .map(item -> item.getAuthority())
        .collect(Collectors.toList());

    // Response with JwtResponse
    return ResponseEntity.ok(new JwtResponse(jwt,
        userDetails.getId(),
        userDetails.getUsername(),
        userDetails.getEmail(),
        roles));
}
```

> We are making use of all the classes we created before to validate the login credentials and generate a token.

The sign up process is a bit long to show in its entirety in a consumable form. The following snippets will be the one method `registerUser(@Valid @RequestBody SignupRequest signUpRequest)` broken up into sections.

First we check if the user exists by username or email. They should be unique.

```java
// Checking if user exists with provided username
if (userRepository.existsByUsername(signUpRequest.getUsername())) {
    return ResponseEntity
        .badRequest()
        .body(new MessageResponse("Error: Username is already taken!"));
}

// Checking if user exists with provided email
if (userRepository.existsByEmail(signUpRequest.getEmail())) {
    return ResponseEntity
        .badRequest()
        .body(new MessageResponse("Error: Email is already in use!"));
}
```

Then the account is created:

```java
// Create new user's account
User user = new User(signUpRequest.getUsername(),
    signUpRequest.getEmail(),
    encoder.encode(signUpRequest.getPassword()));
```

Next we extract roles from the request and add them to the user:

```java
// Setting roles
Set<String> strRoles = signUpRequest.getRole();
Set<Role> roles = new HashSet<>();

// Default set to user if no roles are provided
if (strRoles == null) {
    Role userRole = roleRepository.findByName(RoleType.ROLE_USER)
        .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
} else {
    // We loop through each role in the strRoles set and switch to decide which role to add to the enw user.
    strRoles.forEach(role -> {
    	switch (role) {
            case "admin":
                Role adminRole = roleRepository.findByName(RoleType.ROLE_ADMIN)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                roles.add(adminRole);
			break;
            case "mod":
                Role modRole = roleRepository.findByName(RoleType.ROLE_MODERATOR)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            	roles.add(modRole);
			break;
            // Default role is ROLE_USER
            default:
                Role userRole = roleRepository.findByName(RoleType.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                roles.add(userRole);
            }
        });
    }
// Setting the roles and saving the user
user.setRoles(roles);
userRepository.save(user);

return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
```

Now we are able to create users and recieve tokens. When we want to sign up, we do the following request and get the response shown below:

![Register user](./resources/register_user.png)

Then the user is added to our database:

![User in DB](./resources/user_in_database.png)

<img src="./resources/user_roles_database.png" alt="roles" style="width:200px;"/>


We can then use these credentials to login and get an access token:

![Login](./resources/login.png)

Now we can use this access token on subsequent requests as an _Authorization header_. As shown below:

![Authorized](./resources/admin_resource.png)

> You will get a 403 Unauthorized if you try access resources not meant for your role.

Thats how you set up stateless Authentication and role based Authorization in Spring Security.

> **RESOURCE** The link to the repository can be found [here](https://gitlab.com/NicholasLennox/spring-security-internal)

---

Copyright 2021, Noroff Accelerate AS
