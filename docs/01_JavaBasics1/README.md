# Del 1

# Grunnleggende Java Del 1

**Læringsmål**

Du vil lære å…

- **Forklare** Hva Java Virtual Machine er, hvordan det fungerer, og dets komponenter
- **Forklare** hvordan koden din er kompilert til en Java-applikasjon.
- **Beskrive** de ulike datatypene og datastrukturene som finnes i Java
- **Anvende** datatyper og datastrukturer i hensiktmessige situasjoner
- **Designe** koden din med kontrollstrukturer for å utvide funksjonalitet.

---

## Java-språket

Java er et allsidig, klassebasert, objektorientert programmeringsspråk som benyttes til applikasjonsutvikling. Det ble først utgitt av Sun Microsystems i 1995 som et allsidig språk som kan kjøres på hvilken som helst plattform med så få avhengigheter som mulig. Det blir nå vedlikeholdt av Oracle, og siste stabile versjon med langtidsstøtte er **Java 17**.

Viktige Java-funksjoner er blant annet:

- Det er et relativt enkelt språk å lære seg å bruke.
- Skriv kode en gang og kjør den på omtrent alle dataplattformer.
- Java er plattformuavhengig. Programmer utviklet på en spesifikk plattform kan kjøres på andre plattformer.
- Det er designet for å bygge objektorienterte applikasjoner.
- Det er et flertrådet språk (eng. *Multithreaded*) med automatisk minnehåndtering
- Det er laget for distribuerte nettverkssystemer
- Forenkler distribuert databehandling da det er nettverkssentrisk

Viktige Java-applikasjoner:

- Native Android applikasjoner
- Bedriftssystemer med Spring Framework
- Vitenskapelige databehandlingsapplikasjoner
- Stordataanalyse med Hadoop
- Embedded-systemer
- Server-Side-teknologier som Apache, JBoss, GlassFish, etc.

Java er et av de mest benyttede programmeringsspråkene. Det er raskt, sikkert og pålitelig; og foretrekkes av mange organisasjoner for å bygge prosjekter

### Java Plattformen

Java-programvareplattformen består av JVM, Java API og et komplett utviklingsmiljø. JVM analyserer og kjører (tolker) Java-bytekode. Java API består av en omfattende mengde biblioteker og tjenester.

Java-språket med programvareplattformen skaper en kraftig, velutprøvd teknologi for utvikling av programvare for bedrifter.

**The Java Virtual Machine (JVM)** lar oss å skrive kode én gang og kjøre den hvor som helst. Den gjør dette ved å tilby et standardisert miljø for kompilert Java-kode å kjøre i. **Java Runtime Environment (JRE)** inkluderer JVM. JRE muliggjør selvstendig kodekjøring som er uavhengig av underliggende maskinvare- eller OS-forskjeller.

**The Java Development Kit (JDK)** er et programvareutviklingsmiljø som brukes til å lage applets og Java-applikasjoner. Java-utviklere kan bruke det på Windows, macOS, Solaris og Linux. JDK hjelper det med å kode og kjøre Java-programmer. Det er mulig å installere mer enn én JDK-versjon på samme datamaskin. JDK inneholder verktøy som kreves for å skrive Java-programmer og JRE for å kjøre dem.

Fire typer Java-programmeringsspråkplattformer er:

1. Java Platform, Standard Edition (Java SE)
2. Java Platform, Enterprise Edition (Java EE)
3. Java Platform, Micro Edition (Java ME)
4. JavaFX

### Java Kompilering

Java-kode er ikke kompilert til maskinkode, men til en mellomform kalt **bytekode**. Bytekode er ikke lesbar for mennesker - den tolkes kun av JVM.

`.class` filer lagrer kompilerte Java-klasser. Java bruker just-in-time (JIT) kompilering for å konvertere bytekode til maskinkode under kjøringen (eng. runtime). Hvis kompilatoren støter på en feil syntaks, vil den produsere en **error**. Det er viktig å forstå hva errors betyr og hvordan du kan fikse dem.

**Integrated Development Environments (IDEs)** håndterer filkompilering og pakkeoppretting automatisk. For å manuelt kompilere klassen `Program.java` til`Program.class`, åpne en terminal der filen er plassert og kjør følgende kommando:

```bash
javac Program.java
```

En ytterligere fil kalt `Program.class` er opprettet, forutsatt at det ikke var oppsto noen feil. For å kjøre programmet, skriv inn følgende kommando:

```bash
java Program
```

JIT-kompilatoren laster bytekoden `Program.class`, og konverterer den deretter til maskinkode for å kjøre på JRE.

### Kompilering av flere filer

Manuell kompilering av flere filer krever at du inkluderer dem som argumenter til`javac` kommandoen.

```bash
javac src/*.java src/foo/*.java
```

Alle  `.java` filene i  `src` og  `src/foo` mappene kompilere av denne kommandoen. De resulterende `.class` filene eksisterer sammen med `.java` filene, et uønsket resultat fra et lesbarhetssynspunkt.

En dedikert `out` mappe for å plassere vår kompilerte kode i forbedrer organiseringen.

```bash
javac -d out src/*.java src/foo/*.java
```

Denne kommandoen resulterer i en speilet prosjektstruktur `out` mappen.

> MERK: IDE-er gjør dette automatisk for kompilering av flere filer.
> 

### Java Programstruktur

Et Java-program omfatter følgende seksjoner:

- Dokumentasjon
- Pakkeerklæring
- Importerklæringer
- Grensesnitterklæring
- Klassedefinisjon
- Main metode klasse
- Main metode definisjon

> MERK: Denne delen berører mange begreper og konsepter. Senere leksjoner dekker hver av dem i større detalj.
> 

### Dokumentasjonsseksjon

Koommentarer kan skrives i denne seksjonen. Kommentarer er fordelaktige for å forstå koden.

```java
// Single line comment

/*It is an example of 
multiline comment*/ 

/**It is an example of documentation comment*/
```

> MERK: Du bør også inkludere kommentarer i tråd med koden din. En generell regel er å forklare hvorfor noe er her i stedet for hva det gjør. Unntaket er når det ikke er klart hva koden din gjør.
> 

### Pakkeerklæring

En pakke kan lages med hvilket som helst navn. En pakke er en gruppe med klasser definert med et navn. Hvis du vil gruppere relaterte klasser, legg dem til i samme pakke.

```java
package no.noroff.accelerate;
```

### Importerklæring

Denne linjen indikerer at hvis du ønsker å bruke en klasse av en annen pakke, så kan du gjøre dette ved å importere den direkte inn i programmet.

```java
import java.util.Random;
```

### Grensesnitterklæring

Grensesnitt består av en gruppe relaterte metodeerklæringer uten innhold i body. Det er en valgfri seksjon.

### Klassedefinisjon

Et Java-program kan inneholde flere klassedefinisjoner. Klasser er hovedelementene i ethvert Java-program.

> MERK: Et Java-program kan inneholde flere klasser/grensesnitt/enums. Hver skal være i sin egen fil, med et filnavn som er identisk med deres klasse/grensesnitt/enum-navn.
> 

> INFO: Man kan ha flere klasser i én fil gjennom`inner` eller `sealed` klasser.
> 

### Main Method klasse

Hvert frittstående Java-program krever en Main-metode som startpunktet for programmet, kjent som inngangspunktet. Det kan være mange klasser i et Java-program, og bare **én klasse definerer Main-metoden.**

```java
public static void main(String[] args)
```

### Eksempel på Main-klasse

Eksempelet under viser en klasse kalt `[Main.java](http://Main.java)` og hvordan de ulike aspektene diskutert ovenfor guider dens struktur.

```java
/*
 * Main class
 */

// Package the class belongs to
package no.noroff.accelerate;
// Imports (other classes) the class uses
import java.util.Random;
// Class definition
public class Main {
    // Main method (entrypoint)
    public static void main(String[] args) {
        /*
         This method generates a random number between 0-10.
         Then prints the result to the console.
        */
        Random rand = new Random();
        int max = 10; // Upper bound
        int min = 0;  // Lower bound
        int randomNum = rand.nextInt((max - min) + 1) + min;
        // Print result to console
        System.out.println(randomNum);
    }
}
```

 `import`-erklæring spesifiserer eksterne klasser som brukes av programmet vårt.`java` biblioteket har mange klasser og dekker de fleste av våre behov. Det vil imidlertid skje at vi trenger mer kompliserte klasser og rammeverk. Disse legges til som **avhengigheter** til våre prosjekter og kan deretter importeres. En senere leksjon går gjennom denne prosessen.

> RESSURS: Offisielle Java kodekonvensjoner er tilgjengelig [her](https://www.oracle.com/java/technologies/javase/codeconventions-introduction.html).
> 

## Datatyper

**Variabler** er beholdere for lagring av dataverdier. Hver variabel har en **type** som definerer dens størrelse og hvordan datamaskinen lagrer den. En variabelerklæring følger malen:

```java
type variable = value;
```

Det finnes mange ulike typer i Java. De er bredt kategorisert i **primitive** og **ikke-primitive** typer.

### Primitive typer

Primitive typer er de mest grunnleggende datatypene som er tilgjengelige i Java-språket. De består av boolean, byte, char, short, int, long, float og double.

Disse primitivene fungerer som byggesteinene for datamanipulering i Java. En primitiv datatype spesifiserer størrelsen og typen av variabelverdier, og den har ingen tilleggsmetoder. Ikke-primitive datatyper inkluderer **String**, **Arrays** og **Classes**.

De kan kategoriseres basert på typen data de lagrer:

- **Numerisk** - short, int, long, float og double. Disse typene kan brukes til å utføre enkel aritmetikk, som addisjon og subtraksjon.
- **Textual** - byte og char. Disse typene inneholder tegn, Unicode, eller til og med tall.
- **Boolean** - Sanne og falske verdier.
- **Null** - Tom (ingen verdi)

Detaljer knyttet til primitiver kan sees i tabellen nedenfor:

Kodeutdraget under viser hvordan variabler er deklarert med primitive datatyper:

```java
int myNum = 5;               // Integer (Hele tall)
double myDouble = 1.5;       // Double (Decimal number)
float myFloatNum = 5.99f;    // Floating point number
char myLetter = 'D';         // Character
boolean myBool = true;       // Boolean
String myText = "Hello";     // String
```

### Type casting

Type casting er når man tilordner en verdi av en datatype til en annen datatype. I Java er det to typer casting:

- Utvidende casting (automatisk) - konvertering av en mindre type til en større type-størrelse
`byte -> short -> char -> int -> long -> float -> double`
- Innsnevrende casting (manuelt) - konvertering av en større type til en mindre type-størrelse
 `double -> float -> long -> int -> char -> short -> byte`

> MERK: Vi må manuelt caste large>small fordi det resulterer i informasjonstap. Å la kompilatoren gjøre dette automatisk vil være uegnet.
> 

Nedenfor er et eksempel på de to typene casting:

```java
// Widening (automatic) casting
int myInt = 9;
double myDouble = myInt;

System.out.println(myInt);      // Outputs 9
System.out.println(myDouble);   // Outputs 9.0
```

```java
// Narrowing (manual) casting
double myDouble = 9.78d;
int myInt = (int) myDouble;

System.out.println(myDouble);   // Outputs 9.78
System.out.println(myInt);      // Outputs 9
```

### Wrapper-klasser

Hver primitiv har en tilknyttet klasse (wrapper-klasse) som gir funksjonalitet til data som er lagret der.

Disse klassene har vanligvis samme navn som den primitive, men følger Java-klassens navnekonvensjon; klassen må starte med store bokstaver.

For eksempel inneholder`Integer` wrapper-klassen en verdi for `int` og utvider noe funksjonalitet. Koden nedenfor konventerer en `String` til `int`:

```java
String userInput = "34";
int userAge = Integer.parseInt(userInput);

// Then back into String
String userAgeString = String.valueOf(userAge);
```

> MERK: Manipulasjon av strenger i Java går mer i dybden enn vist her. Det vil senere gås mer innpå på dette.
> 

### Klasser

Husk at Java er et objektorientert programmeringsspråk. Alt i Java er assosiert med klasser og objekter, sammen med dets attributter og metoder. En klasse er en planskisse for å lage objekter som utviklere kan bruke til å modellere virkelige ting.

For eksempel, hvis vi ønsket å modellere en`Dog` (les: hund) vil vi definere følgende klasse:

```java
public class Dog {
    public String breed;
    public String colour;
    public int age;
}
```

> MERK: Generelt skal ikke attributter være offentlig tilgjengelige. Forelesning om *objektorientert programmering* vil gå mer inn på dette.
> 

Disse tre variablene inneholder **state** av klassen og er kjent som **attributes**. Den mest grunnleggende bruken av en klasse er å gruppere relaterte variabler i en **custom type**. For å lage en ny forekomst av objektet bruker vi `new` nøkkelordet:

```java
Dog myDog = new Dog();
myDog.breed = "Labradoodle";
```

> MERK: Klasser er langt mer enn vist her. Forelesning om *objektorientert programmering* vil gå mer inn på dette.
> 

### Generisk

Generisk programmering er en stil med dataprogrammering der algoritmer skrives med *types-to-be-specified-later* og deretter instansieres når det er nødvendig for spesifikke typer gitt som parametere.

Det er fordelaktig fremfor kun å bruke`Object` som typen fordi det vil bli typesjekket ved kompilering.Representert som en stor `T`, deretter erstattet med den spesifikke nødvendige typen ved instansiering.

Det vanligste stedet å se generikk er i de innebygde`java` bibliotekene. Disse bibliotekene er designet for å være så generaliserbare som mulig, og forbedrer språkets fleksibilitet.

> MERK: Generisk programmering kommer inn i bildet når man diskuterer generiske samlinger senere i leksjonen.
> 

## Datastrukturer

Datastrukturer lar utviklere ordne grupper av data i programmer for å brukes på en effektiv og strukturert måte. Det finnes mange forskjellige datastrukturer for å dekke ulike behov. De vanligste er **Arrays** og **Generic Collections**, levert gjennom *Java API*. Det er også mulig å lage egne tilpassede datastrukturer.

### Arrays

“Arrays” lagrer flere verdier i en enkelt variabel; i stedet for å deklarere separate variabler for hver verdi. En array er en spesifikk type og kan bare inneholde variabler av den typen.

> MERK: Arrays kommer fra `java.util.Arrays` importering.
> 

### Endimensjonell

En endimensjonell array er i fundementalt en **rad** med data. For å erklære en 1D array, kan variabeltypen defineres med firkantede parenteser:

```java
dataType[] arrayName = new dataType[size];
```

Størrelsen på arrayen er definert med **size** variabelen ved instansiering. Arrays er kjent som **statiske datastrukturer**.

```java
// deklarere en array 
int[] ages; 

// allokere minne 
ages = new int[5];
```

Dette vil instansiere en ny`int` array med 5 elementer. Man kan deretter initialisere verdier ved å referere til `index`:

```java
// access array elements
array[index]
```

```java
age[0] = 25; 
age[1] = 50;
```

> MERK: Java, som med de fleste programmeringsspråk, bruker nullbasert indeksering (telling starter fra null). Dette betyr at et array med n elementer vil ha elementer nummer fra n til n-1.
> 

Erklæring, instansiering og tildeling kan gjøres på én linje ved å bruke en **array literal**:

```java
int[] ages = {15,21,50,44,9};
```

### Todimensjonell

En todimensjonell array er i hovedsak data i en **tabell** med **rader** og **kolonner**. For å deklarere en 2D-array, definer variabeltypen med to sett firkantede parenteser:

```java
dataType[][] arrayName = new dataType[rows][cols];
```

```java
int[][] ages = new int[3][4];
```

Ved å gi kombinasjonen av indekser for rad og kolonne får man tilgang til ønsket element:

```java
ages[0,1] = 10;
```

Erklære en 2D-array med en **array literal**:

```java
// deklarere og initialisere en 2D array
int ages[][] = { {2,7,9},{3,6,1},{7,4,2} };
```

### Generiske Objektsamlinger

**Collections Framework** tilbyr mange grensesnitt og klasser som en arkitektur for å lagre og manipulere en gruppe objekter.

Collection grensesnitt (`java.util.Collection`) og Map grensesnitt (`java.util.Map`) er de to hovedgrensesnittene til Java-samlingsklasser.

En collection er en enkelt enhet av objekter. De bruker *generics* for å holde og manipulere alle `Object` i Java. Enhver samling kan utføre følgende operasjoner på data: **søk, sortering, innsetting, manipulering** og **sletting.**

### Lister

`List` grensesnittet hemmer en list-type datastruktur der vi kan lagre den ordnede samlingen av objekter. Den kan ha dupliserte verdier og vokser etter behov. Rekkefølgen for innsetting opprettholdes.

List grensesnittene implementeres av klassene `ArrayList`, `LinkedList`, `Vector`, og `Stack`. For å instansiere List-grensesnittet, må vi bruke:

```java
List<String> arrayList= new ArrayList();  
List<String> linkedList = new LinkedList();  
List<String> vector = new Vector();  
List<String> stack = new Stack();
```

 `<String>` argumentet er den generiske parameteren for`List<T>`. Den spesifiserer at `List` skal være av typen`String`.

Den oftest brukte `List` er en `ArrayList`. Det er en generell `List` brukt som et fleksibelt 1D array. Kodeutdraget nedenfor viser grunnleggende bruk av `ArrayList`:

```java
import java.util.ArrayList;
import java.util.Collections; // Only for sort
...
// Declaring a new list
ArrayList<String> guests = new ArrayList();
// Adding items to the list
guests.add("Ola");
guests.add("Bartas");
guests.add("Emma");
guests.add("Jakob");
guests.add("Olivia");
int size = guests.size(); // size = 5
int indexOfJakob = guests.indexOf("Jakob"); // index = 3
// Getting and setting elements by index
String firstGuest = guests.get(0); // OUTPUT: "Ola"
guests.set(0, "Olav"); // Setting new element
String newFirstGuest = guests.get(0); // OUTPUT: "Olav"
guests.remove(0); // Removing by index
guests.remove("Jakob"); // Removing my element
Collections.sort(guests); // Sorts
guests.clear(); // Removes all elements
```

### Sets

Set-grensesnittet ligger i `java.util` pakken og utvider `Collection`-grensesnittet. Det er en uordnet samling av objekter hvor duplikate verdier ikke kan lagres.  Rekkefølger for *insertion* er opprettholdes **ikke**. 

Set-grensesnittet er implementert av klassene `HashSet`, `EnumSet`, `LinkedHashSet`, og `TreeSet`. For å instansiere Set-grensesnittet, må vi bruke:

```java
List<String> hashSet = new HashSet();
List<String> enumSet = new EnumSet();
List<String> linkedHashSet = new LinkedHashSet();
List<String> treeSet = new TreeSet();
```

Det mest brukte `Set` er et `HashSet`. Det er et generelt `Set` brukt som et fleksibelt 1D-array som ikke kan ha duplikater. Kodeutdraget nedenfor viser et enkelt eksempel påbruk av `HashSet:`

```java
import java.util.HashSet;
...
// Declaring a new HashSet
HashSet<String> guests = new HashSet();
// Adding items to the HashSet
guests.add("Ola");
guests.add("Bartas");
guests.add("Emma");
guests.add("Jakob");
guests.add("Olivia");
int size = guests.size(); // size = 5
boolean containsJakob = guests.contains("Jakob"); // true
guests.remove("Jakob"); // Removing element
guests.clear(); // Removes all element
```

> MERK: HashSet opprettholder ikke rekkefølgen til elementene sine. Derfor er det ikke mulig med sortering.
> 

### Maps

Map grensesnittet i `java.util` pakken representerer en “mapping” mellom en **nøkkel** og en **verdi**. `Map`-grensesnittet er ikke en subtype av `Collection`-grensesnittet. Derfor oppfører det seg annerledes fra resten av collection-typene. Et map inneholder unike nøkler.

Map-grensesnittet er implementert av klassene `HashMap`, `LinkedHashMap`, and `TreeMap`. For å instansiere Map-grensesnittet må vi bruke:

```java
Map<String, Integer> hashMap = new HashMap<>();
Map<String, Integer> linkedHashMap = new LinkedHashMap<>();
Map<String, Integer> treeMap = new TreeMap<>();
```

Maps er perfekt å bruke for ****assosiasjons-mapping av **nøkkel-verdier.** Maps er brukt for å oppslag med nøkler, eller når noen vil hente og oppdatere elementer med nøkler. Noen eksempler er:

- Et map med feilkoder og beskrivelser
- Et map med postkoder og byer

Et map kan ikke ha duplikatnøkler og hver nøkkel kan forbindes med maks en verdi. Et vanlig brukt `Map` er et `HashMap`; det er en god stedfortreder for en Java-ordbok. Kodeutdraget nedenfor viser enkel bruk av `HashMap:`

```java
import java.util.HashMap;
...// Declaring a new HashMap
HashMap<Integer, String> guestsWithCodes = new HashMap<>();
// Adding items to the HashMap
guestsWithCodes.put(1, "Ola");
guestsWithCodes.put(2, "Bartas");
guestsWithCodes.put(3, "Emma");
guestsWithCodes.put(4, "Jakob");
guestsWithCodes.put(5, "Olivia");
int sizeOfMap = guestsWithCodes.size(); 
// 5
String guest = guestsWithCodes.get(4); 
// guest = Jakob
guestsWithCodes.remove(2);
boolean hasCode1 = guestsWithCodes.containsKey(1); 
// True
boolean hasOlivia = guestsWithCodes.containsValue("Olivia"); 
// True
guestsWithCodes.clear();
```

> MERK: Du kan erstatte et element med .put() med en eksisterende nøkkel.
> 

## Kontrollstrukturer

I grunnleggende forstand er et program en liste med instruksjoner. Kontrollstrukturer er programmeringsblokker som kan endre veien vi går gjennom de instruksjonene.

Det er tre typer kontrollstrukturer:

- **Kondisjonelle Branches (greiner)** som vi bruker for å velge mellom to eller flere stier. Det er tre typer i Java: if/else, *ternary-operatøren og switch.*
- **Loops** som blir brukt til å iterere gjennom flere verdier/objekter og gjentatt kjøre spesifikk kode blokker. De grunnleggende loop typene i Java er for, while og do while.
- **Forgrenende setninger,** som blir brukt til å endre flyten av kontroll i loops. Det er two typer i Java: break og continue.

### If-else

If/else setninger er den mest grunnleggende av alle kontroll strukturene. Den former det basisen av beslutningstaking i programmering. Malen for en if/else setning er som følger:

```java
if(condition) {    
// Utføres om condition er sann
} else {    
// Utføres om condition ikke er sann
}
```

Mens `if` kan brukes alene, er det vanligste bruksscenarioet å velge mellom to retninger med `if/else`. Under er et eksempel på en enkel if/else setning:

```java
if (count > 2) {    
System.out.println("Count er høyere enn 2");
} else {    
System.out.println("Count er lavere eller lik 2");
}
```

> MERK: Vi kan nøste if/else blokker uendelig, men dette er detrimentelt for kodens lesbarhet, og derfor er anbefales ikke dette.
> 

### Ternary

Det finnes også en kortform if/else. Den er kjent som **ternary operatøren** da den består av tre operander. Den kan brukes til å korte flere linjer med kode ned til en enkelt linje. Det benyttes ofte for å erstatte enkle if/else setninger, og har følgende mal:

```java
variable = (condition) ? expressionTrue :  expressionFalse;
```

> MERK: De forskjellige operatørene i Java omtales i en senere leksjon.
> 

Kodeutdraget nedenfor illustrerer bruk av en ternary setning og sammenligner det til if/else setningen ovenfor:

```java
System.out.println(count > 2 ? "Count is higher than 2" : "Count is lower or equal than 2");
```

> MERK: En ternary setning kan bli sett som syntaktisk sukker (eng: syntactical sugar).
> 

> INFO: Syntaktisk sukker er syntaks innenfor et programmeringsspråk designet for å gjøre det lettere å lese eller uttrykke. Det gjør språket mer behagelig for menneskelig bruk, uttrykt mer tydelig, mer presist, eller i en alternativ stil som enkelte foretrekker.
> 

### Switch

Switch-setningen gjør det mulig å erstatte flere nøstede if-else kontrollstrukter, og på denne måten bedre lesbarheten til koden vår. Malen for en switch-setning finnes nedenfor:

```java
switch(expression) {  
case x:    
		// code block    
		break;  
case y:    
		// code block    
		break;  
default:    
		// code block
}
```

> MERK: Break-setningen stopper en switch-setning, og stanser kjøring av mer kode, og case-testing innenfor kodeblokken
> 

> MERK: Nøkkelordet *default* spesifiserer kode som skal kjøres hvis ingen case blir matchet.
> 

Kodeutdraget nedenfor illustrerer bruk av en switch setning:

```java
int count = 3;
switch (count) {
case 0:
    System.out.println("Count is equal to 0");
    break;
case 1:
    System.out.println("Count is equal to 1");
    break;
case 2:
case 3:
    System.out.println("Count is equal to 2 or 3");
    break;
default:
    System.out.println("Count is either negative, or higher than 3");
    break;
}
```

> MERK: Switch-setninger må ha verdier som matcher eksakt. Hvis et område (mellom 1 og 5) er ønsket, så er en nøstet if/else den foretrukkede løsningen.
> 

Vi kan ikke sammenligne alle typer av objekter og primitiver i en switch-setning. En switch virker kun med fire primitiver og deres wrapper, og i tillegg med enum-typen og String-klassen: 

- byte and Byte
- short and Short
- int and Integer
- char and Character
- enum
- String

### For

Når man vet eksakt hvor mange ganger vil kjøre gjennom en kodeblokk skal kjøre, bruk *for-*loopen. Malen for en *for-*loop er som følger:

```java
for (statement 1; statement 2; statement 3) {  
		// code block to be executed
}
```

**Statement 1** utføres (en gang) før utførelsen av kodeblokken.

**Statement 2** definerer betingelsen for å utføre kodeblokken.

**Statement 3** utføres (hver gang) etter kodeblokken har blitt utført.

Kodeutdraget nedenfor printer ut tallene 0-4:

```java
for (int i = 0; i < 5; i++) {
  System.out.println(i);
}
```

*Statement 1* setter en variabel før loopen starter **(int i = 0)**. *Statement 2* setter betingelsen for at loopen skal kjøre **(i må være mindre enn 5)**. Hvis betingelsen er sann, vil loopen starte på igjen, hvis den er false, vil loopen avsluttes. *Statement 3* øker en verdi **(i++)** hver gang kodeblokken har blitt kjørt.

### For-each

En for-each loop er syntaktisk sukker for å loope gjennom alle elementer i et array/collection. Under er malen for en for-each loop:

```java
for (type variableName : arrayName) {
  // code block to be executed
}
```

Kodeutdraget under illustrerer bruken av for-each for å printe ut alle *guests* i en collection:

```java
String[] guestsArray = {"Ola", "Bartas", "Emma", "Jakob", "Olivia"};
for (String guest: guestsArray) {    
	System.out.println(guest);
}
```

Looping gjennom de fleste collections blir gjort på denne måten utenom for Maps, ettersom de er key-value par. Under er et eksempel på bruk av `HashMap` med en for-each loop

```java
HashMap<Integer, String> guestsWithCodes = new HashMap<>();
guestsWithCodes.put(1, "Ola");
guestsWithCodes.put(2, "Bartas");
guestsWithCodes.put(3, "Emma");
guestsWithCodes.put(4, "Jakob");
guestsWithCodes.put(5, "Olivia");
// Iterating over keysfor (int i : guestsWithCodes.keySet()) {
    System.out.println(i);
}
// Iterating over valuesfor (String i : guestsWithCodes.values()) {
    System.out.println(i);
}
// Iterate over keys and use them to get valuesfor (int i : guestsWithCodes.keySet()) {
    System.out.println("key: " + i + " value: " + guestsWithCodes.get(i));
}
// Iterate over entrySet (both key and value)for (Map.Entry<Integer, String> entry : guestsWithCodes.entrySet()) {
    int key = entry.getKey();
    String value = entry.getValue();
    // ...}
```

### While

While loopen gjentar en kodeblokk så lenge det betingende Boolean-uttrykket er sant. Kodeutdraget nedenfor viser en mal for bruken av en while loop:

```java
while (condition) {  
	// code block to be executed
}
```

Kodeutdraget nedenfor viser et eksempel av en while loop som printer ut tallene 0-4:

```java
int i = 0;
while (i < 5) {
  System.out.println(i);
  i++;
}
```

### Do-while

Do-while loopen er en variant av while-loopen. Denne loopen utfører kodeblokken en gang før den sjekker om betingelsen er sann. Deretter vil den gjenta loopen så lenge betingelsen er sann.

```java
do {  
	// code block to be executed
}while (condition);
```

Under er det samme eksempelet som i while-loopen med en do-while loop:

```java
int i = 0;
do {  
	System.out.println(i);  
	i++;
}while (i < 5);
```

> Merk: While-looper har en pre-check (forhåndssjekk), do-while looper har en post-check (ettersjekk)
> 

### Break and continue

*Break* and *continue* lar oss avbryte den normale flyten i applikasjonen. Dette brukes når en oppfyller en spesifikk betingelse uten behov for mer utregning.

B*reak-*setningen brukes til å unnslippe (tidlig utgang) en løkke. *Continue-*setningen hopper over én iterasjon.

- **Break** - går ut av hele sløyfen
- **Continue** - hopper over gjeldende iterasjon

Nedenfor er et kodeutdrag som illustrerer bruken av b*reak* og *continue*:

```java
// Loop stops when i=4
for (int i = 0; i < 10; i++) {
  if (i == 4) {
    break;
  }
  System.out.println(i); // OUTPUT: 0,1,2,3
}
// Skips when i=4
for (int i = 0; i < 10; i++) {
  if (i == 4) {
    continue;
  }
  System.out.println(i); // OUTPUT: 0,1,2,3,5,6,7,8,9
}
```

---

Copyright 2021, Noroff Accelerate AS