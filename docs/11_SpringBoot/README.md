# Spring Framework
This lesson serves as an introduction to the Spring Framework. It covers the various facets and tools available within the framework. A focus is placed on Spring Boot and creating Spring Boot applications. The concept of annotated code is explored in this lesson by describing and investigating the various annotations provided by Spring to make creating enterprise-ready Java applications simpler. Some tools that Spring is built on are also discussed, this includes Tomcat as a local hosting server and Serveletes.

**Learning objectives:**

You will be able to...
- **summarize** the Spring framework.
- **describe** Spring's role in modern software development.
- **create** a Spring Boot application using Spring Initialzr.
- **explain** the MVC pattern and its implementation in Spring Boot.
- **demonstrate** the use of annotations to configure Spring Boot applications.
- **explain** the role Tomcat plays in Spring Boot applications.
- **discuss** what an http servlette is and how its used.
- **develop** a controller with endpoints.
- **demonstrate** extracting information from request parameters and path variables.

---

## Java Spring

Building web servers is tedious, a lot of repetitive coed needs to be written and in reality much of this will be duplicated between different servers. To alleviate this we use a framework of technologies, specifically we use the Spring Framework.

The Spring Framework contains various modular solutions depending on the task you want to achieve, for this course we stay at a higher level, but it is also possible to delve into the workings of the framework to tweak how any aspect of it performs.

Throughout the remainder of the course we will make use of many parts of the Spring framework, but two parts form the basis of nearly every project we will build; _Spring Boot_ and the _Spring Initializr_.

## Spring Boot

We will be using [Spring Boot](https://spring.io/projects/spring-boot) as the basis for all our Spring Framework applications, it will lets us quickly and painlessly set up various solutions. Part of it's offering is the embedded Tomcat solutions. 

[Tomcat](https://tomcat.apache.org/) is the basis for the server technologies that we would need to integrate when building our own web server. Now, thanks to Spring Boot, we will need to do almost _zero_ configuration to get a server running.

### Spring Initializr

The [Spring Initializr](https://start.spring.io/) is a web based tool that does all the tedious steps of initialising a new Spring project for us.

Using Spring Initializr is straight forward:

- Navigate to [https://start.spring.io/](https://start.spring.io/)
- Select either `Maven` or `Gradle` for the project build system
- Select either `Java`, `Kotlin` or `Groovy` for the project language 
- Select a Spring Boot version
- Fill in the project metadata
- Select the correct Java version 
- Add required dependencies, in this case: `Spring Web`
- Click `Generate`, the solution will be packaged and a download will initiate

> **Dependencies**
>
> For this small project we only add one dependency (`Spring Web`), but in future projects we may need to add more. 
> They can be selected by pressing the `Add Dependencies...` button (top right) after completing the project metadata.
> Dependencies may also be added manually via the `pom.xml` or `build.gradle` files depending on the project built system.

![Spring Initializr Demo](./resources/01_SpringInitializr.gif)

- Proceed to extract the folder and import the project into IntelliJ
- Initiate the Gradle import
- Wait for the build to complete

![Spring Boot Import](./resources/02_Import.gif)

> **Downloads and Imports**
>
> IntelliJ will download a local copy of `Maven` or `Gradle` for use with this project, so it may take a few minutes for the project to fully initialise the first time.
>
> If you make any changes to the `pom.xml` or `build.gradle`, IntelliJ will once again download local copies and you may need to wait. (Remember to `Load Changes`)

Depending on the SDK selected and the configuration of you system you may need to specify the Java version for the project again.

You may then use your preferred build system to do a build test of the entire project.

In this example I specify `Java 14` and execute a `Build` instruction using `Gradle`.

![Set SDK and test build](./resources/03_SDK_andBuild.gif)

At this point the base project should be fully configured and ready to be tested.

### Launch it

![Gradle BootRun](./resources/04_BootRun.gif)

For the image you can see that the `HelloSpringBootApplication` was started successfully,

> Remember to press the red square in the top right to stop the project, it is possible to run multiple web servers simultaneously which can create confusion.

If we navigate to [localhost:8080](http://localhost:8080/) using a web browser (while the project is running locally), we will see the following error:

![White Label Error](./resources/05_WhiteLabel.png)

**This is error a good thing**

From this we can tell that the server is running, and responding to our requests. We have simply neglected to tell it _how_ to respond. 

## Creating Endpoints

To get a meaningful response from our fresh server, we will need to create a `RestController` and to define an `Mapping` inside that controller.

- Create a package (folder) called `Controllers` inside of `/src/main/java/<package name>/`. This should be next to the `HelloSpringBootApplication.java` file
- Create a file called `HelloController.java` inside the new `controllers` package
- Insert (or type) the following code into the `HelloController.java` file:

```Java
package com.example.HelloSpringBoot.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {

    @GetMapping("/")
    public String index() {
        return "This is the root page";
    }

    @GetMapping("/hello")
    public String greetGuest() {
        return "Hello, guest!";
    }
}
```

We have used two simple annotations that are doing a lot of work for us, namely `@RestController` and `@GetMapping`.

### RestController

This (`@RestController`) annotation informs the application that the given class contains a controller with rest endpoints; while this in itself is obvious for us, the compiler will use this annotation to construct the automated linking required for the containing endpoints to function as desired.

We can create multiple such controllers to help organize endpoints when working with larger applications.

### GetMapping

Next our code contains two functions, each preceded by a `@GetMapping(...)`

```java
@GetMapping("/")
public String index() {
    return "This is the root page";
}
```

This mapping indicates that the function should be run when the endpoint is accessed with the `GET` method; we can freely change the method type (`GET`,`PUT`,`POST`, etc.), the endpoint address (`/`,`/hello`,etc.), the return type, or the function body (logic).

In this example, both mappings return the `String` type and are accessed on the `GET` method, but in future examples we will work with other alternatives for both of these.

## Testing Endpoints

Build and execute the application, then navigate to the endpoints in your browser.

The root (`/`) endpoint will look as follows:

![Root mapping](./resources/06_Root.png)

The `/hello` endpoint will look as follows:
![Hello mapping](./resources/07_Hello.png)

We could also add a simple reporting function on the server side by writing to the (server) console:

```java
@GetMapping("/")
public String index() {
	System.out.println("/ was accessed")
    return "This is the root page";
}
```

This helps us assess functionality from both the user (browser) perspective and the server perspective.


### RequestMapping

The `@GetMapping` is a special case of the `@RequestMapping`.

```java
@GetMapping("/")
```

is equivalent to:

```java
@RequestMapping(
	value = "/",
	method = RequestMethod.GET // or just: method = GET
	)
```

This long form also allows for the easy addition of headers:

```java
@RequestMapping(
	value = "/",
	headers = {"key = value"},
	method = GET
	)
```

## Sending Data

In the next lesson we will send data via the `POST` method, but for this exercise we will send data along with a `GET` method, typically we would use this to specificity a parameter of the request or as part of the path to the resource.

### Request Parameters

> These kinds of request parameters are usually not the best choice and should never be used for sensitive information.
> 
> Request/Query parameters should be used to filter or to specify a search criteria for a set of data at a given endpoint.

Consider the following address: `https://www.google.com/search?query=cat+pictures`

This request indicates that at the `/search` endpoint we are specifying our `query` to be `cat+pictures`.

This is a hypothetical example of how this endpoint could look in Java:

```java
@RequestMapping(value = "/search", method = GET)
public String simpleSearch(@RequestParam("query") String terms) {
    return "Search results for " + terms;
}
```

We can optionally (to avoid confusion) specify the parameters within the `@RequestMapping` as follows:

```java
@RequestMapping(value = "/search", params = "query", method = GET)
```

### PathVariables

Consider the following address: `https://pokeapi.co/api/v2/pokemon/pikachu`

In this instance the entire URL describes the data as a _resource at a location_, this is very common in restful APIs. Spcifically, we are requestion information about the _pokemon_ named _pikachu_.

In Java we could define this endpoint as follows:

```java
@RequestMapping(value = "/{resource_type}/{resource_name}", method = GET)
public String simpleSearch(
		@PathVariable String resource_type, // pokemon
		@PathVariable String resource_name  // pikachu
		) 
	{
    return "Data for " + resource_type + " with name " + resource_name;
}
```

The Pokémon API has multiple types of resources (Pokémon, Moves, Locations, etc.), and within each of those, there are a specified number of such resources. 

> The actual PokéAPI documentations names differ from those used here but this was intentionally done to help clarify the lesson concepts, when using the PokéAPI refer to the [relevant documentation](https://pokeapi.co/docs/v2).

---

Copyright 2021, Noroff Accelerate AS
