# Docker
This lesson covers the concept of containerization and its role in modern software development. The Docker container platform is used as the tool to create running containers and show their value. This includes writing Dockerfiles, exploring the common configurations available through these files as well as using multiple images in a compose file. Finally, the act of publishing a containerized application is explored.

**Learning objectives:**

You will be able to...
- **discuss** the role containerization plays in modern software development.
- **contrast** containers with virtual machines.
- **explain** what Docker is.
- **create** a Dockerfile replicating a given application's environment.
- **create** a Docker image, given a Dockerfile.
- **create** a Docker container, given a Docker image.
- **demonstrate** the process of adding a Docker image to an online container registery.
- **demonstrate** publishing a Docker image to a platform like Heroku.
- **compose** several given Docker images together to **create** several running, interconnected, containers.

---


Deploying a fully-fledged web application can prove to be a complicated task if done manually. Platforms such as Heroku and Docker simplify this process considerably, allowing developers to align their focus on productivity rather than the configuration of web servers. Containerisation is used to achieve this.


> Docker is an open-source, Linux-based, tool for the deployment and management of containers. Containers allow applications to be packaged, transferred and run easily accross multiple platforms. 

This lesson is dedicated to expanding on the above statement. It covers the theory surrounding Docker, its practical implementation, and its role in modern software development.

## Containers
> To understand Docker and its mechanisisms, the concept of containers needs to be understood first. 

A container is what is known as a `standard unit of software`, where code is packaged up with all its dependencies included in a lightweight virtualized form in isolation from the rest of the environment. In other words, you can virtualize a part of your system and share it. 

This allows high levels of portability as these containers can run quickly and reliably from one environment to another. Containers revolutionalised cloud computing by providing a boundry at the application level. This isolation means that if anything goes wrong with the container, only it is affected and not the rest of the server.

Cloud computing and containers have given rise to a new paradigm in service delivery called `Microservices`. The details of this are out of our scope. A heavily simplified explanation is that you carve up your monolithic application into multiple services running independently from each other. These services can communicate internally with one another or the outside world. 

Docker has become the standard for creating and deploying containers, but it is not the only tool. Its success has brought on a partnership with Micrsoft to bring Docker containers to Windows through the [Docker Desktop](https://www.docker.com/products/docker-desktop) application, leveraging Hyper-V (Windows Hypervisor) or more recently with Windows Subsystem for Linux 1 & 2 ([WSL2](https://devblogs.microsoft.com/commandline/wsl2-will-be-generally-available-in-windows-10-version-2004/)).

> For the purposes of this course, install the Docker Desktop application.

> Microservices are out of the scope of this course but you are encouraged to read up about them, as they are strongly tied to containers and cloud computing.

## Docker architecture
Docker makes use of a client-server architecture. The *client* talks to the Docker *daemon*, which is responsible for building, running and distributing Docker containers. The client and daemon can exist on the same computer, but it is not required - as they communicate using a REST API over UNIX sockets or a network interface.

### Docker deamon 
The Docker daemon (`dockerd`) listens for Docker API requests and manages Docker objects such as images, containers, networks, and volumes. A daemon can also communicate with other daemons to manage Docker services. This is a background task when Docker Desktop is installed.

### Docker client
The Docker client (`docker`) is the primary way that many users interact with Docker. When you use commands such as `docker run`, the client sends these commands to `dockerd`, which carries them out. The `docker` command uses the Docker API. The Docker client can communicate with more than one daemon. When you install `Docker Desktop` you get access to all the CLI commands mentioned here, and [many more](https://docs.docker.com/engine/reference/commandline/docker/). 

### Docker registries
A Docker registry stores Docker images. [Docker Hub](https://hub.docker.com/) is a public registry that anyone can use, and Docker is configured to look for images on Docker Hub by default. This is where most of the base image you are going to use are stored.

When you use the `docker pull` or `docker run` commands, the required images are pulled from your configured registry. When you use the `docker push` command, your image is pushed to your configured registry.

## Images and dockerfiles
Images are snapshots of software that contain all the requirements for an application to run as intended. This includes instructions on how to start, what resources are needed, various configurations, environment variables, libraries, the actual code, and the needed runtimes. These images can then be run to create one or more running instances of the defined application and environment - this is the container discussed earlier. 

Docker images are immutable, meaning they cant be changed but can be copied, this affords the strength of being a consistent element when testing new systems. This makes a Docker image a reusable asset that can be deployed on any host and shared to other developers through any container registery (GitHub and GitLab have provisions for container registeries in addition to Docker Hub being the default).

Like any executable from traditional programming, a image needs a written file to understand what is required. These instructions are defined in a Dockerfile, where all the instructions mentioned above are written, in the [Go programming language](https://golang.org/).

> The details of; what goes into a Dockerfile, how to create images from these files, and run instances of the images as containers; are detailed later in the lesson.

## Containers vs Virtual machines
At the most basic level; containers and virtual machines have similar resource isolation and allocation benefits, but function differently because containers virtualize the operating system instead of hardware. This means that containers are more portable and efficient.

<p float="left">
    <img src="resources/vm.png" alt="Virtual Machine structure" width="49%"/>
    <img src="resources/container.png" alt="Container structure" width="49%"/>
    <br>
    <center>
    <em>Structure of Virtual Machines (left) vs Containers (right)</em>
    </center>
</p>

To delve into the above statement we can start with `Virtual machines` (VMs). As mentioned above, VMs are an abstraction of physical hardware, turning one server into many servers. The hypervisor allows multiple VMs to run on a single machine. Each VM includes a full copy of an operating system, the application, necessary binaries and libraries - taking up a great deal of space (typically GBs). Aside from the added space requirements, VMs are typically slow to boot as they are running on a portion of the available hardware, not to mention the costs involed of having multiple licences for software if the VM is not running on an open source platform like Linux.

`Containers` are an abstraction at the app layer that packages code and dependencies together. Multiple containers can run on the same machine and share the OS kernel with other containers, each running as isolated processes in user space. Containers take up less space than VMs (container images are typically tens of MBs in size), can handle more applications and require fewer VMs and Operating systems.

> In practicality, containers and virtual machines are often used together. Where a VM is rented from a cloud provider and containers are then hosted on it. It is far more efficient than having a dedicated VM for every service.

## Why use containers?
> This section may feel somewhat repetitive, as it is a summary of the pros of using containers and what they can provide to your business. 

**Isolation**

A great benefit of containers technology is the isolation of resources: RAM, processes, devices and networking are virtualized at the Operating System level, and the applications are isolated from each other.
This means that you don't have to worry about conflicts of dependencies or shared resources, because each application has defined limitations in the use of resources. In addition, thanks to isolation, the protection level is higher.

**Increase of productivity**

Another benefit of containers, maybe the most obvious, is the chance to host a large amount of containers also on PC or laptop. This allows there to always be a deployment and testing environment availagble for any applciation - a far more expensive operation with virtual machines. 

This kind of technology results in an increase of developers productivity, thanks to the removal of dependencies and conflicts among different services. Each container can host an application or a single part of it and, as we saw above, is isolated from other containers. In this way developers can forget about synchronization and dependencies for any service, as well as they are free to run the updates without worrying about possible problems among the components.

**Easy deployment and shorter start-up times**

Each container includes not only the application but also all the packages used to run it. This simplifies any deployment operation and facilitates its distribution on different operating systems with no further effort of configuration. In addition to this, virtualizing only the operating system, results in far shorter start-up times.

**Coherent environment**

Thanks to the standardized approach, containers enable the portability of resources, by reducing issues about the displacement of applications through the cycle of development, testing and production. Containers can be deployed in an easy and secure way independently form the environment. It's not necessary to configure servers manually and new features can be released more easily.

**Operating efficiency**

With containers you can run more applications on the same instance and specify the right amount of resources which should be used, by ensuring their optimization. Containers are lighter than VM and make the system more agile, by increasing the operating efficiency, the development and the management of applications.

**Version control**

Container technology enables to manage the versions of application code and its dependencies. You can keep track of the container versions, analyzing the difference among them and eventually come back to the previous version.

## Common Docker CLI commands
> A lot of the container manipulation of running containers can be done directly in the Docker Desktop GUI.

For each command the following template is used:
- Title
- Short description of what the command does
- Command template
- Table showing commonly used OPTIONS (if applicable)
- Example command (if applicable)

#### General

These commands are used to gether information about Docker on the host system.

**Info**

Shows information about the installed version of Docker, it can be easily used to check if you have Docker installed.

```shell
docker info
```

**Version**

Shows the currenltly installed version of Docker.

```shell
docker version
``` 

#### Creation

These commands are concerned with the creation of images and containers.

**Build**

Build an image from a Dockerfile. This command builds Docker images from a Dockerfile and a "context". A build's context is the set of files located in the specified `PATH` or `URL`. 

```shell
docker build [OPTIONS] PATH | URL | -
```

Options:

|Name, shorthand  |Description                                             |
|:----------------|:-------------------------------------------------------|
| `--file , -f`   |Name of the Dockerfile (Default is 'PATH/Dockerfile')   |
| `--quiet , -q`  |Suppress the build output and print image ID on success |
| `--tag , -t`    |Name and optionally a tag in the 'name:tag' format      |
| `--target`      |Set the target build stage to build.                    |
<br/>

Examples:
1. Build the Dockerfile in the current directory
```shell 
docker build .
```
2. Build a dockerfile using a GIT repo
```shell 
docker build gitlab.com/noroff-accelerate/dotnet/hello-docker
```
This will clone the GitLub repository and use the cloned repository as context. The Dockerfile at the root of the repository is used as Dockerfile.

3. Tag an image
```shell 
docker build -t nicholaslennox/dockerdemoapi:1.0 .
```

This will create a Docker image called `nicholaslennox/dockerdemoapi` with its version being set to `1.0`, using the Dockerfile in the current context.

> Naming convention for Docker images is in the template `<Your Docker ID>/<Repository Name>:<tag>`. This allows images to easily be shared through Docker Hub and other registeries. `<tag>` is typically the version of the image, althought it doesn not have to be, if left blank it defaults to `latest`.

Using this naming conventions from the start primes your images to easily be shared. Note, there is no dockerdemoapi repository created yet in Docker Hub, but when the image is pushed it is already named appropriatly. Even if there are no plans to place the container on a registry, using this naming conventions regardless helps organise your local images. In this situation, `<Repository Name>` can just be whatever you want to call the Docker image.

> This method of building will be the most common one we use. The others are shown for a wider context of the options available.

You can apply multiple tags to an image. For example, you can apply the latest tag to a newly built image and add another tag that references a specific version. For example, to tag an image both as `nicholaslennox/dockerdemoapi:latest` and `nicholaslennox/dockerdemoapi:v2.1`, use the following:

```shell 
docker build -t nicholaslennox/dockerdemoapi:latest -t nicholaslennox/dockerdemoapi:v2.1 .
```

This can be utilized to help tracking versions of your images to easily see a history and which is the latest version without having to consult external docuemtation.

4. Specify a Dockerfile

This is when you have multiple Dockerfiles in the same context, the `-f` tag allows you to configure which one is built.

```shell 
docker build -f dockerfiles/Dockerfile.debug -t myapp_debug .
docker build -f dockerfiles/Dockerfile.prod  -t myapp_prod .
```
The above commands will build the current build context (as specified by the `.`) twice, once using a debug version of a Dockerfile and once using a production version.

**Run**

This command is used to create a container from an image. It First `creates` a container then `starts` it using the specified commands. 

```shell 
docker run [OPTIONS] IMAGE [COMMAND] [ARG...]
```

Options:

|Name, shorthand    |Description                                             |
|:------------------|:-------------------------------------------------------|
| `--detach, -d`    |Run container in background and print container ID      |
| `--entrypoint`    |Overwrite the default ENTRYPOINT of the image           |
| `--env, -e`       |Set environment variables                               |
| `--env-file`      |Read in a file of environment variables                 |
| `--expose`        |Expose a port or a range of ports                       |
| `--help`          |Print usage                                             |
| `--name`          |Assign a name to the container                          |
| `--net, --network`|Connect a container to a network                        |
| `--publish, -p`   |Publish a container’s port(s) to the host               |
| `--restart`       |Restart policy to apply when a container exits          |
| `--volume, -v`    |Bind mount a volume exits                               |
| `--workdir, -w`   |Working directory inside the container                  |
| `--label, -l`     |Set meta data on a container (key=value)                |
<br/>

> There are a lot [more](https://docs.docker.com/engine/reference/commandline/run/) than just listed here.

Most of the arguements seen here can be preconfigured in the Dockerfile. Thus, `environment variables`, `networks`, and `volumes` are discussed in the `writing a Dockerfile section`.

> You do NOT need to add all these commands to run a container, they can be very simple.

Examples:

1. Simple running container from existing image
```shell
docker run nicholaslennox/dockerdemoapi:latest
```
This command simply creates a container (running instance) of our `nicholaslennox/dockerdemoapi` image with the `latest` tag. Docker will generate a name for this container, you can see this by looking in Docker Desktop and inspecting the runnnig container, or use `docker ps` to see all running instances.

2. Forwarding exposed ports
```shell
docker run -p 8080:80 nicholaslennox/dockerdemoapi:latest
```
This command creates a container from the same image as before, except this time it publishes (forwards) the exposed port `80` from the container to port `8080` on the host. The host is our computer. 

> Template for port forwarding (publishing) is `<host port>:<exposed container port>`.

The exposed port `80` is defined in the Dockerfile, you can define it on the docker run command with `--expose`, but it is far easier to do that inside your Dockerfile.

> This is the most common `docker run` command we will use in this course. The other ones are to give a broader context.

3. Giving the container a name
```shell
docker run -p 8080:80 --name demo-api nicholaslennox/dockerdemoapi:latest
```
This will force Docker to name the container dmeo-api and not use the auto-genertate done.

4. Adding a restart policy
```shell
docker run -p 8080:80 --name demo-api --restart=on-failure nicholaslennox/dockerdemoapi:latest
```
The `--restart` flag tells Docker how to handle the container restarts, the policies are shown below.

|Policy                      |Result                                         |
|:---------------------------|:----------------------------------------------|
| `no`                       |Do not automatically restart the container when it exits. This is the default.|
|`on-failure[:max-retries]` | Restart only if the container exits with a non-zero exit status. Optionally, limit the number of restart retries the Docker daemon attempts. |
|`unless-stopped` | Restart the container unless it is explicitly stopped or Docker itself is stopped or restarted. |
| `always` | Always restart the container regardless of the exit status. When you specify always, the Docker daemon will try to restart the container indefinitely. The container will also always start on daemon startup, regardless of the current state of the container.|
<br/>

> `Working directories`, `volumes`, `environment variables`, and `network` configurations are out of scope of this lesson, but will be mentioned very slightly at parts in the lesson. 


**Images**

List images. By default, this will show all top level images, their repository and tags, and their size.

```shell
docker images [OPTIONS] [REPOSITORY[:TAG]]
```

Options:

|Name, shorthand    |Description                                             |
|:------------------|:-------------------------------------------------------|
| `--all , -a`      |Show all images (default hides intermediate images)     |
| `--filter , -f`   |Filter output based on conditions provided              |
| `--no-trunc`      |Don’t truncate output                                   |
| `--quiet , -q`    |Only show numeric IDs                                   |
<br/>
Examples:

1. Straight forward example, list all images
```
docker images
```

2. List docker images by `name:tag`
```
docker images java

REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
java                8                   308e519aac60        6 days ago          824.5 MB
java                7                   493d82594c15        3 months ago        656.3 MB
java                latest              2711b1d6f3aa        5 months ago        603.9 MB
```
If the `tag` is ommitted, all images matching that repository would be listed. 

3. Using `--filter`, format is of `key=value`.

If there is more than one filter, then pass multiple flags (e.g., `--filter "foo=bar" --filter "bif=baz"`).

```
docker images --filter "dangling=true"

REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
<none>              <none>              48e5f45168b9        4 weeks ago         2.489 MB
<none>              <none>              980fe10e5736        12 weeks ago        101.4 MB
<none>              <none>              dea752e4e117        12 weeks ago        101.4 MB
```

The currently supported filters are:

- dangling (boolean - `true` or `false`)
- label (`label=<key>` or `label=<key>=<value>`)
- before (`<image-name>[:<tag>]`, `<image id>` or `<image@digest>`) - filter images created before given id or references
- since (`<image-name>[:<tag>]`, `<image id>` or `<image@digest>`) - filter images created since given id or references
- reference (pattern of an image reference) - filter images whose reference matches the specified pattern.

**Ps**

List containers.

```shell
docker ps [OPTIONS]
```

Options:

|Name, shorthand    |Description                                             |
|:------------------|:-------------------------------------------------------|
| `--all , -a`      |Show all containers (default shows just running)        |
| `--filter , -f`   |Filter output based on conditions provided              |
| `--latest , -l`   |Show the latest created container (includes all states) |
| `--no-trunc`      |Don’t truncate output                                   |
| `--quiet , -q`    |Only display numeric IDs                                |
<br/>

## Using Docker

> This section is a summary of what commands are needed to successfully containerize an application.

1. Download Docker from the [official website](https://www.docker.com/products/docker-desktop)
2. Create a file at the root directory of your project called `Dockerfile` (No file extension) with the following text

```
FROM openjdk:15
VOLUME tmp
ADD target/<build-jar-name>.jar <docker-jar-name>.jar
ENTRYPOINT ["java", "-jar", "/app.jar"]
```

`FROM` specifies the Docker image which we will use for our application. A list of available images can be found on [here](https://hub.docker.com/).

We told Docker that our application needs the OpenJDk to run, this is the _base_ image.
We added out application on top of this image, but the result was a _new_ docker image that is now our application.

Because the `OpenJDK` image is immutable (we cannot make changes to it), we don't actually need to copy it around with our application. When  we deploy our application somewhere, the host will download the base `OpenJDK` image for itself. 

When our application is started up (running) the host creates a container for it to run inside of, this is a **COPY** of the image.

This is a very efficient way to deploy servers, and allows for multiple instance to be run very easily, but it does create some complication. One that you will encounter is that you cannot make local file changes that save permanently. 


`ADD` instructs Docker to copy the .jar artifact created by our project in the target folder to the root directory of our application

`ENTRYPOINT` describes the command which Docker will run on our .jar file after it has been copied

`VOLUME` specifies an allocated bit of strorage that case permantly hold data generated by our Docker containers. In this case, a directory called `tmp` is generated and stored on the host machine and is completely managed by Docker. This volume can be used by other containers to share information.

> `VOLUME` is out of scope for our purposes, it is here to give you a brief explanation of it as it is something you most likely will see when working with Docker. More can be found out [here](https://docs.docker.com/storage/volumes/).

3. Run the following command in the terminal:

```bash
docker build -t <your docker tag name here> .
```

4. To run the instance of your Docker image, enter the following command into the terminal:

```bash
docker run -p <port number>:<portnumber> <your docker tag name here>
```

We have now successfully created and run a Docker container. To list all running containers or all existing containers along with relevant information about them, use the following commands:

```bash
docker ps
docker ps -a
```

Docker's desktop dashboard gives us the option of stopping and starting the instances of our containers. It is the equivalent of running the following commands from the terminal:

```bash
$ docker container start <container id>
$ docker container stop <container id>
```

> There is a Docker plugin for Intellij that can "generate" these files for you. It is recommended to not use this tool as a lot of unnecessary commands are generated and can make debugging difficult. 

## Heroku

Heroku is a platform as a service (PaaS) which allows us to build, run and deploy our applications hosted on virtual containers called dynos. It has a generous free tier subscription model that we will take advantage of for learning purposes.

> As a technical note, Heroku applications will run on AWS servers.

#### Deploying an Application

> These steps assume a Spring Boot application, but will also work for [node](https://nodejs.org/en/) and [other](https://devcenter.heroku.com/) types of applications. 

How to set up a Heroku app using Spring Boot:

1. Sign-up on the [Heroku website](https://signup.heroku.com/)

> A free account lets you create up to 5 applications and use databases with up to 10000 records.
> You can check you applications via the [Heroku Dashboard](https://dashboard.heroku.com/apps)

2. Install the [Heroku CLI tools](https://devcenter.heroku.com/articles/heroku-cli).
   Before proceeding to the next step, ensure that your Heroku CLI installation has completed successfully by running the following command in the terminal.

```bash
heroku
```

> A list of Heroku commands will be displayed if the CLI is configured correctly.

3. Log in to the Heroku service using the following terminal command (Note: this has been known to fail when using GitBash for Windows, you will need to use the Windows Command Line):

```
heroku login
```

4. Using the terminal, navigate to the root directory of your Spring Boot application and enter the following command:

```
heroku create
```

> `create` is a shorthand for `apps:create`

If created successfully, two URLs should be displayed to the terminal. The first is the link to the instance of the application hosted on Heroku. The second points to the remote git repository for the application which has been automatically created on the platform.
After refreshing your dashboard on the Heroku website, your new application should be displayed.

5. By default, Heroku will assign a random name to your application. To rename it, enter the following command:

```
heroku apps:rename --app <old-app-name> <desired-app-name>
```

This will change both the endpoint of the application as well as the URL to its remote git repository.

> It is possible to name the application directly when creating it, but this name must be unique globally:
>
> `heroku create <app-name>`

6. While still in the root directory of your application, create a local git repository with the following commands:

```
git init
```

```
git add .
```

```
git commit -m "initial commit"
```

These commands will initialize the local repository, add all the necessary files and commit them. We will then need to push our local repository to Heroku.

```
git remote add heroku <link to your remote git repository on heroku>
```

```
git push heroku master
```

Heroku workers will start to build the application automatically. After a successful build, you should be able to navigate to a live instance of your application. 

> NOTE: this might take a few seconds to reflect after building.

You may need to add a system.properties file in the root directory of your project with the following text to successfully deploy onto Heroku.

```
java.runtime.version=15
```

> When you run an application on Heroku (with or without Docker), you application is always a container, and these will shutdown if unused for 15 minutes. 
> You will notice that an application may take a few minutes to start again if the have been shutdown.

**Checking up on your server**

At any time, while in an Heroku app you can view the recent server logs with:

```bash
heroku logs
```
However if you want to watch in real-time, you can use the following command:

```bash
heroku logs --tail
```
> Press `Ctrl-C` to exit.


These commands will show the recent messages that you application has printed to the console. If you connect right after doing a push, you are able to see the Heroku workers building your application.


**Heroku Region**

By default public applications will be created in the `us` region, but this can be specified (when creating the application) but add in the `--region <region>` flag to your command.

```bash
heroku create --region eu
```

While this will set up the application in the specified region, parts of the Heroku platform may still be distributed to other regions, such as PostGres databases or other custom addons.

You can easily see the available regions by running the following command:

```bash
heroku regions
```

If you are not using a private application, then you will be restricted to two options: `us` or `eu`.

#### Docker on Heroku

Before deploying our Docker image to Heroku, you will need to set your project to use Heroku assigned port instead of the default 8080 port that is used with Tomcat. Navigate to `src/main/java/resources/application.properties` and add the following line of code.

```
server.port=${PORT:8080}
```

> The application will use the `PORT` environment variable if available (such as on Heroku), otherwise it will default to `8080`

Once you have rebuilt your solution, do the following:

1. Navigate to the root directory of your application and log in to Heroku using the CLI tools
2. Enter the following commands in the terminal:

```bash
heroku container:login
```

```bash
heroku create
```

This will create a new app on Heroku, as described in the previous section.

```bash
heroku container:push web --app <heroku-app-name>
```

This will build the image and push it to Heroku. If successful, you should be able to navigate to the application via your browser.

## Resources
> [Docker Desktop](https://www.docker.com/products/docker-desktop)

> [Docker CLI commands](https://docs.docker.com/engine/reference/commandline/docker/)

> [Docker Hub](https://hub.docker.com/)

---

Copyright 2021, Noroff Accelerate AS