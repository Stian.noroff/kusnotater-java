# Objektorientert programmering

# Objektorientert programmering

I denne leksjonen lærer du å lage objekthierarkier gjennom prosessen med objektorientert programmering (OOP). Leksjonen dekker kjernepilarene i OOP og andre konsepter som hjelper til med å designe modulær programvare.

**Læringsmål:**

Du vil være i stand til å…

- **Forklare** pilarene i objektorientert programmering (OOP).
- **Diskutere** rollen og bruken av OOP i Java.
- **Konstruere** klasseforhold for å møte gitte funksjonskrav.
- **Demonstrere** bruken av polymorfisme og ****inheritance (arv).

---

## Introduksjon

Objekter er virkelige enheter og objektorientert programmering er et paradigme for å lage programmer ved hjelp av klasser og objekter. Det forenkler programvareutvikling og vedlikehold via noen konsepter:

- Objekt
- Klasse
- Arv (Inheritance)
- Polymorfisme
- Abstraksjon
- Innkapsling

Utover disse konseptene, er det noen andre begreper som brukes i objektorientert design:

- Kobling
- Kohesjon
- Assosiasjon
- Aggregasjon
- Komposisjon

> **MERK**: Kobling og kohesjon er konsepter som dekkes i en dedikert leksjon om objektorientert design.
> 

Objektorientert programmering har flere fordeler:

- OOP gir en klar struktur for et program
- OOP hjelper til med å holde koden DRY «Don't Repeat Yourself», og gjør koden enklere å vedlikeholde, modifisere og feilsøke
- OOP gjør det mulig å lage fullt gjenbrukbare applikasjoner med mindre kode og kortere utviklingstid
- OOP deler opp komplekse problemer i mindre objekter

> **MERK**: «Don't Repeat Yourself» (DRY)-prinsippet handler om å redusere gjentakelse av kode. Du bør trekke ut gjentatt kode og plassere den på ett sted for gjenbruk.
> 

Enhver enhet som har en tilstand og atferd kalles et objekt. **Tilstand** beskriver objektet, **atferd** beskriver hva objektet kan gjøre.

I Java lages et objekt fra en klasse. Minne tildeles objekter på en spesifikk adresse. De kan kommunisere uten å kjenne detaljene til hverandres data eller kode.

> **MERK**: En klasse er som en objektkonstruktør, eller en "blåkopi" for å lage objekter
> 

Husk at malen for en klasse er som følger:

```java
class ClassName {
  // fields
  // methods
}
```

## Pilarer i OOP

De fire pilarene i objektorientert programmering er:

- **Abstraksjon**: bare avsløre offentlige metoder på høyt nivå for å få tilgang til et objekt
- **Innkapsling**: inneholder informasjon i et objekt, og eksponerer kun valgt informasjon
- **Arv**: underordnede klasser arver data og atferd fra overordnet klasse
- **Polymorfisme**: mange metoder kan gjøre den samme oppgaven

Vi har allerede sett noen av disse konseptene i bruk. De følgende avsnittene utdyper dem.

### Abstraksjon

Abstraksjon har som mål å skjule viss detaljer og viser kun viktig informasjon til brukeren. Man kan tenke på når man betjener en TV, datamaskin, bil, osv. Du trenger ikke å kjenne de indre funksjonene, kun hvordan man samhandler med objektene.

> **MERK**: Abstraksjon innebærer å bruke enkle klasser for å representere kompleksitet.
> 

Ved å bruke abstrakte klasser og grensesnitt kan man definere metodesignaturer (navn og parameterliste) og la hver klasse implementere dem på sin måte.

Nøkkelpunkter for abstraksjon:

- Skjuler den underliggende kompleksiteten til data
- Hjelper med å unngå repetitiv kode
- Presenterer kun signaturen for intern funksjonalitet
- Gir fleksibilitet til å endre implementeringen av den abstrakte atferden
- Delvis abstraksjon kan oppnås med abstrakte klasser
- Total abstraksjon kan oppnås med grensesnitt

### Abstrakte klasser

En abstrakt klasse *kan ikke* **instansieres**, bare **utvides**. Dette betyr at man ikke kan bruke  `new` på et objekt av en abstrakt type.

> **MERK**: Ikke alle klasser trenger å være abstrakte. Hvis du modellerer én type sykkel uten undertyper, er det ikke nødvendig med en abstrakt base.
> 

Som et eksempel, kan det lages en abstraksjon for `Bicycle`. Vi kan gjøre følgende:

```java
public abstract class Bicycle {
    // Fields/State
    // Methods/Behaviour
}
```

Denne abstrakte klassen representerer basen av typer sykler som kan eksistere i vår applikasjon. Abstrakte klasser kan ha **abstrakte metoder**; metoder uten *body*. Kodeutdraget nedenfor illustrerer dette:

```java
public abstract class Bicycle {
    // Fields/State
    // Methods/Behaviours
    // Abstract method - no implementation
    public abstract String getDetails();
}
```

Denne metoden representerer en måte for enhver type *Bicycle* å vise sin spesifikke tilstand. Enhver klasse som utvider dette må implementere denne metoden. Abstrakte metoder brukes når det ikke er noen standardimplementering for alle typer sykler.

> MERK: Utvidelse av klasser dekkes i underseksjonen for Arv.
> 

Abstrakte klasser kan også ha normale metoder. Disse representerer en standard implementasjon. Hvis en utvidet klasses oppførsel ikke er annerledes, trenger den ikke inneholde en ny implementetasjon. Dette oppnår delvis abstraksjon. Kodeutdraget nedenfor illustrerer dette:

```java
public abstract class Bicycle {
    // Fields/State
    // Methods/Behaviours
    // Abstract method - no implementation
    public abstract String getDetails();
    // Normal method displaying default implementation
    public String hoot() {
        return "Tring tring";
    }
}
```

### Grensesnitt

Et grensesnitt er en abstraksjon som representerer atferd uten implementering. Det er en kontrakt for enhver implementeringsklasse. Når en klasse implementerer et grensesnitt står det "Jeg vil levere implementeringer for følgende metoder".

> **MERK**: Styrkene til grensesnitt blir tydeligere når objektorientert design diskuteres i en senere leksjon.
> 

Et grensesnitt er definert med kun metodesignaturer, ingen *body* eller tilstand. Enhver **tilstand** definert i et grensesnitt behandles som "endelig" (uforanderlig/uendrende/konstant). Kodeutdraget nedenfor illustrerer opprettelsen av et grensesnitt for å definere hvordan Bicycles kjøres:

```java
public interface Drivable {
    public void accelerate();
    public void brake();
    public double getSpeed();
}
```

Dette grensesnittet gir en kontrakt for akselerasjon, bremsing (deselerasjon) og lesing av gjeldende hastighet. Legg merke til at hastigheten ikke er definert som en tilstand; det er ikke noe som må defineres eksplisitt. Dette understrekes ytterligere av det faktum at du ikke kan definere *mutable* (foranderlig) **tilstand i et grensesnitt.

Kodeutdraget nedenfor illustrerer implementeringen av grensesnittet i abstraktklassen vår Bicycle:

```java
public abstract class Bicycle implements Drivable {
    // Fields/State
    // Methods/Behaviours

    // Abstract method - no implementation
    public abstract String getDetails();
    // Normal method displaying default implementation
    public String hoot() {
        return "Tring tring";
    }
}
```

Legg merke til `implements` nøkkelordet. Det slår fast at denne klassen har samtykket til Drivable-kontrakten. Legg også merke til mangelen på implementering. Abstrakte klasser trenger ikke å gi implementeringer da de er en abstraksjon. Klasser som utvider Bicycle må implementere alt fra Bicycle og Drivable.

### Innkapsling

Innkapsling lar oss beskytte dataene som er lagret i en klasse mot systemomfattende tilgang. Som navnet antyder, beskytter den det interne innholdet i en klasse som en fysisk kapsel ville gjort. Innkapsling i Java kan implementeres ved å holde feltene (klassevariablene) private og gi offentlige getter- og setter-metoder til hver av dem.

Nøkkelpunkter for innkapsling:

- Begrenser direkte tilgang til datamedlemmer (felt) for en klasse
- Felt er satt som private
- Each field has a getter and setter method
- Getter-metode returnerer feltet
- Setter-metoder muliggjør endring av verdien til feltet

> **MERK**: Tilstanden til en klasse håndteres gjennom innkapsling. Alle felt kan være satt til offentlig, men dette er dårlig praksis og bør unngås
> 

Å utvide den abstrakte Bicycle-klassen til å inkludere en tilstand er neste steg. Kodeutdraget nedenfor legger til en tilstand til klassen vår:

```java
public abstract class Bicycle implements Drivable {
    // Fields/State
    private String brand;
    private int riders;
    private double weight;
    private double speed;
  ...
  // Public getters and setters
    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getRiders() {
        return riders;
    }

    public void setRiders(int riders) {
        this.riders = riders;
    }
  ... // Further getters and setters omitted
```

> MERK: Getters og setters kan enkelt genereres via IntelliJ.
> 

Bruk av private felt og offentlige metoder tillater riktig innkapsling av tilstanden vår. Det betyr at alle klasser som bruker klassen vår (kjent som klienter) ikke kjenner tilstanden, men kan samhandle med dem gjennom metoder.

> **MERK**: Ikke alle felt trenger en getter og setter. Hva som skal endres direkte er opp til forretningslogikken. For dette kurset trenger man ikke ta slikt hensyn, men være obs på dette.
> 

> **MERK**: Innkapsling forbedrer lesbarheten til koden.
> 

### Polymorfisme

Før arv dekkes, er det viktig å gå gjennom begrepet polymorfisme. Polymorfisme refererer til evnen til å utføre en spesifikk handling på forskjellige måter.

I Java kan polymorfisme ha to former: metode***overbelastning*** og metode***overstyring***.

Metodeoverbelastning oppstår når ulike metoder med samme navn er tilstede i en klasse. Metodeoverstyring skjer når subklassen (child class) overstyrer en metode i dens superklasse (parent class). 

> **MERK**: Alle objekter i Java har en basisklasse kalt *Object* som er kjernen av OOP.
> 

Nøkkelpunkter for polymorfisme:

- Det samme metodenavnet brukes flere ganger
- Ulike metoder med samme navn kan kalles fra objektet
- Alle Java-objekter kan betraktes som polymorfe på grunn av basisklassen til objektet
- Eksempel på statisk polymorfisme i Java er metodeoverbelastning
- Eksempel på dynamisk polymorfisme i Java er metodeoverstyring

Kodeutdraget nedenfor overstyrer en metode som hvert objekt i Java har, `toString`:

```java
@Override
public String toString() {
  return "Bicycle{" +
    "brand='" + brand + '\'' +
    ", riders=" + riders +
    ", weight=" + weight +
    ", speed=" + speed +
    '}';
}
```

En metode er merket som overstyrt med nøkkelordet `@Override`. Denne metoden eksisterer for å føre tilstanden til et objekt. Måten dette gjøres på avhenger av objektet og dets forretningslogikk. `toString`-metoden kan enkelt genereres gjennom Intellij, som resulterer i kodeutdraget ovenfor.

> **MERK**: Andre vanlige overstyrte metoder fra basisklassen til Object er: equals() og getHashCode(). Disse kan definere likheten til våre egendefinerte objekter, slik at vi kan se om tilstanden til en instans samsvarer med en annen. Disse kan også genereres gjennom Intellij.
> 

### Arv (Inheritance)

Arv gjør det mulig å lage en subklasse (child class) som arver feltene og metodene til dens superklasse (parent class). Subklassen kan valgfritt overstyre verdiene og metodene til superklassen. Den kan også utvide nye data og funksjonalitet til superklassen. Java bruker nøkkelordet **extends** for å implementere prinsippet om arv i kode.

Nøkkelpunkter for arv:

- En klasse (subklasse) kan utvide en annen klasse (superklasse) ved å arve dens funksjoner
- Implementerer DRY (Don't Repeat Yourself) programmeringsprinsippet
- Forbedrer gjenbrukbarhet av kode
- Flernivåarv er tillatt i Java (en subklasse kan utvides til en annen klasse)
- Multippel arv er ikke tillatt i Java (en klasse kan ikke utvide mer enn én klasse)

Fra eksempelet med `Bicycle` kan vi legge til subklasser som kan instansieres:

```java
public class BMX extends Bicycle {
  // Details ommitted
}
public class Mountain extends Bicycle {
  // Details ommitted
}
```

Disse klassene deler de samme feltene og metodene som deres superklasse, `Bicycle`. Vi kan opprette nye forekomster av disse klassene, vist i kodeutdraget nedenfor:

```java
public class Main {
    public static void main(String[] args) {
        BMX testBMX = new BMX();
        Mountain testMountain = new Mountain();
        testBMX.getDetails();
        testMountain.getDetails();
    }
}
```

Her oppretter vi to nye instanser og påkaller den delte atferden fra superklassen.

> **MERK**: Hvis vi hadde skrevet *Bicycle testBMX = new BMX();* , ville vi polymorfet castet BMX-en vår som en Bicycle. Det går greit så lenge BMX ikke har noen ekstra tilstand eller atferd som utvider, da den ikke vil være tilgjengelig ettersom kompilatoren behandler den som en Bicycle.
> 
> 
> *Det går greit så lenge det ikke er noen ekstra tilstand eller atferd som BMX utvider, da den ikke vil være tilgjengelig ettersom kompilatoren behandler den som en Bicycle.
> 
> Den vil ikke være tilgjengelig ettersom den blir behandlet som en Bicycle av kompilatoren til å caste.*
> 

Vi må også nå levere implementeringer for alle de abstrakte metodene fra `Bicycle` og alle metodene i `Divable` som vi har blitt enige om å implementere

```java
public class BMX extends Bicycle{
    @Override
    public String getDetails() {

        return "A BMX bike for your average skater boy who's scared of four wheels";
    }
    @Override
    public void accelerate() {
        // Increase speed by 1
        setSpeed(getSpeed()+1);
    }
    @Override
    public void brake() {
        // Decrease speed by 1 if moving
        if(getSpeed()!=0)
            setSpeed(getSpeed()-1);
    }
}
```

Legg merke til at vi endrer ikke tilstanden direkte fordi feltene er **private**. Hvis de var **protected,** ville de vært private til alle eksterne klasser men tilgjengelig for alle subklasser. Bruk av *private* er god praksis og bryter ikke innkapsling, men det er noen tilfeller hvor protected*-*felt er nødvendig

> **MERK**: Generelt sett: prøv å følge prinsippet om minst mulig tilgang.
> 

Man er ikke begrenset til å implementere metoder via `@Override`. Alle metoder fra en superklasse kan bli overskrevet i subklassen, selv om det er en standard implementasjon. For eksempel, hoot-metoden i Bicycle har en implementasjon, men det er mulig å endre ved å overskrive den:

```java
public class BMX extends Bicycle{
    ... // Ommited
    @Override
    public String hoot() {
        return "Yo yo";
    }
}
```

> **MERK**: Ekstra tilstand kunne bli lagt til i disse subklassene for å utvide oppførselen.
> 

### Konstruktører

Konstruktører er metoder som har ingen returtype og deler navnet med klassen de konstruerer.

Tidligere brukte vi standardkonstruktøren som er generert automatisk av Java. Den har ingen parametere, derfor setningen `new BMX();` blir brukt.

Vi kan bruke våre settere til å endre tilstanden til instansen etter den er lagd, eller vi kan lage en konstuktør for å sette tilstandene når objektet blir instansiert.

Kodeutdraget nedenfor illustrerer bruken av en konstruktør til basen `Bicycle` :

```java
public abstract class Bicycle implements Drivable {
    // Fields/State
    private String brand;
    private int riders;
    private double weight;
    private double speed;

    // Empty constructor
    public Bicycle() {
    }
    // Overloaded constructor
    public Bicycle(String brand, int riders, double weight) {
        this.brand = brand;
        this.riders = riders;
        this.weight = weight;
        // Isn't moving when created
        speed = 0;
    }
    ...
```

> **MERK:** `This`-nøkkelordet refererer til den omsluttende klassens felt, ikke parameteren. Det er bare påkrevd når det er navnetvetydighet.
> 

Konstruktører er metoder, som betyr at de kan bli overbelastet med bruk av statisk polymorfisme. Du kan lage så mange konstuktører som du trenger å innkapsle de forskjellige måtene å initialisere objektene dine.

> **MERK:** Noen klasser har kompleks konstruksjon med flere valgfrie parametere. En senere leksjon om Object Orientert Design introduserer forskjellige måter å hjelpe med dette.
> 

Nå som vi har lagt til konstruktører i basisklassen vår, bør alle utvidede klasser kalle basiskonstruktøren. For å unngå å gjenta oss selv bruker vi `super`-nøkkelordet for å fortelle kompilatoren “kall min basisklasse sin konstruktør som matcher disse parameterene”:

```java
public class BMX extends Bicycle {
    public BMX(String brand,double weight) {
      // Calling base constructor. A BMX can only carry 1 person.
      super(brand, 1, weight);
    }
    ... // Ommitted
```

Nå kan vi lage et nytt BMX-objekt og gi objektet en initiell tilstand:

```java
BMX testBMX = new BMX("Mongoose",10);
```

Her lager vi en ny BMX med merkefeltet satt til *Mongoose*, som veier 10kg. For å utdype denne leksjonen må vi bevege oss forbi grunnpilarene og se på andre konsepter..

## Andre konsepter

De følgende konseptene hjelper oss modellere mer komplekse systemer:

- Assosiasjon
- Aggregering
- Komposisjon

For eksempel, For å utvide `Bicycle`-eksempelet til å inkludere `Gearboxes` eller `Engines`. kan det være nødvendig å lage klasser for å innkapsle klassene `Gearboxes` og `Engines`. Disse klassene ville da bli brukt i vår `Bicycle`-klasse for å oppnå mer kompleks oppførsel.

> MERK: Aggregering er en spesiell form for assosiasjon, mens komposisjon er en spesiell form for aggregering.
> 

### Assosiasjon

Assosiasjon betyr handlingen for å etablere et forhold mellom to ikke-relaterte klasser. Hvis vi for eksempel måtte opprette en `BicycleShop`-klasse og ha en `List<Bicycle>` som felt. har vi laget en assosiasjon som sier “en sykkebutikk har sykler”. Kodeutdraget nedenfor illustrerer dette:

```java
public class BicycleShop {
    private String name;
    // Creating an association between BicycleShop and Bicycle
    private List<Bicycle> bicycles;
    ... // Getters and setters ommitted
```

Nøkkelpunkter for Assosiasjon

- To separate klasser er assosiert gjennom objektene deres
- De to klassene er ikke relaterte, og den ene kan eksistere uten den andre
- Det kan være en-til-en, en-til-mange, mange-til-en, eller mange-til-mange forhold

> **MERK**: Dette konseptet er mye brukt i en senere leksjon hvor vi lærer å bruke et verktøy kalt Hibernate.
> 

> **MERK**: Komposisjon og aggregering etablerer et HAS-A (HAR-ET - arv) forhold og arv etablerer et IS-A (ER-ET - komposisjon) forhold.
> 

### Aggregering

En spesiell form for assosiasjon hvor et objekt bruker et annet. Eksempelet ovenfor med `Bicycle` og `BicycleShop` er et uavhengig forhold. Hvis en sykkelbutikk hadde ingen sykler ville den fungert, og hvis en sykkel ikke hører til en butikk, vil den fortsatt fungere.

### Komposisjon

En spesiell form for aggregering hvor et objekt *eier* et annet, som betyr de to klassene er gjensidig uavhengige av hverandre og kan ikke existere uten den andre. For eksempel, ta en `MotorBike` og en `Engine` klasse. En `MotorBike` kan ikke kjøre uten en `Engine`, mens en `Engine` kan heller ikke fungere uten å være bygget inni en `MotorBike`. Kodeeksempelet nedenfor illustrerer disse tilleggene.

La oss først lage en abstraksjon for `Engine`. Vi ønsker muligheten for å legge til ulike typer motorer i fremtiden uten å bekymre oss for å endre eksisterende kode for å imøtekomme det.

```java
public interface Engine {
    public void start();
    public void stop();
    public double getFuelLevel();
    public void useFuel() throws OutOfFuelException;
    public void refuel(double amount);
    public boolean isRunning();
    public boolean needsService();
    public void service();
}
```

Dette grensesnittet definerer oppførselen til enhver`Engine` i vårt system. Vi kan nå lage konkrete *Engines* som implementerer denne oppførselen.

Vi kunne laget en abstrakt klasse i stedet; dette eksempelet er for har litt variasjon i forhold til læringsformål. Et enkelt spørsmål å spørre når man velger mellom abstrakte klasser og grensesnitt er “har jeg en spesifikk standardimplementasjon som ikke skal endres?”. I dette tilfellet, har vi ikke det, alle `Engines` vil gjøre ting forskjellig.

La oss lage en `ElectricEngine`. `ElectricEngine` har et ladenivå (i %).

```java
public class ElectricEngine implements Engine {
    // State
    private boolean isRunning = false;
    // Charge is in %
    private double chargeRemaining = 100;
    // Percentage to simulate ware on the charging coils
    private double coilDurability = 100;

    @Override
    public void start() {
        // can only start if above 5% durability
        if(coilDurability > 5)
            if(!isRunning)
                isRunning = true;
    }

    @Override
    public void stop() {
        if(isRunning)
            isRunning = false;
    }

    @Override
    public double getFuelLevel() {
        return chargeRemaining;
    }

    @Override
    public void useFuel() throws OutOfFuelException {
        // Use charge if there is remaining
        if(chargeRemaining > 0)
            chargeRemaining--;
        // If there is no charge remaining, turn the engine off and throw an exception
        isRunning = false;
        throw new OutOfFuelException("Recharge the battery!");
    }

    @Override
    public void refuel(double amount) {
        // Can't go above 100% charge
        if(chargeRemaining != 100)
            if(chargeRemaining+amount >= 100)
                chargeRemaining = 100;
            else
                chargeRemaining += amount;
            // Decrease the durability of the charging coils
            coilDurability--;
    }

    @Override
    public boolean isRunning() {
        return isRunning;
    }

    @Override
    public boolean needsService() {
        // Should be serviced once coils reach 15% durability
        return coilDurability < 15;
    }

    @Override
    public void service() {
        coilDurability = 100;
    }
}
```

For den andre typen, la oss lage en `PetrolEngine` . Den bruker bensin som energikilde og har en kapasitet i liter.

```java
public class PetrolEngine implements Engine {
    private boolean isRunning = false;
    private double maxFuelCapacity;
    // Engine starts fully topped up
    private double fuelLevel = maxFuelCapacity;
    // Durability of spark plugs start at 100%
    private double sparkPlugDurability = 100;

    // A petrol engine has a certain capacity we need to define
    public PetrolEngine(double maxFuelCapacity) {
        this.maxFuelCapacity = maxFuelCapacity;
    }

    @Override
    public void start() {
        if(!isRunning) {
            // Spark plugs degrade when an engine starts
            sparkPlugDurability--;
            isRunning = true;
        }
    }

    @Override
    public void stop() {
        isRunning = false;
    }

    @Override
    public double getFuelLevel() {
        return fuelLevel;
    }

    @Override
    public void useFuel() throws OutOfFuelException {
        if(fuelLevel > 0)
            // 100 ml of fuel is used each time
            fuelLevel -= 0.1;
        isRunning = false;
        throw new OutOfFuelException("Please refill the petrol tank");
    }

    @Override
    public void refuel(double amount) {
        // Can't go above maximum capacity of the tank
        if(fuelLevel != maxFuelCapacity)
            if(fuelLevel+amount >= maxFuelCapacity)
                fuelLevel = maxFuelCapacity;
            else
                fuelLevel += amount;
    }

    @Override
    public boolean isRunning() {
        return isRunning;
    }

    @Override
    public boolean needsService() {
        // Spark plugs need to be changed at 10% durability
        return sparkPlugDurability < 10;
    }

    @Override
    public void service() {
        // Simulating changing the spark plugs
        sparkPlugDurability = 100;
    }
}
```

Legg merke til hvordan hver type motor implementerer metodene helt forskjellig og har egne separate tilstander. Hvis vi valgte å bruke en abstrakt klasse her, ville den ikke tilført noen verdi.i. Grensesnitt gir mer fleksibilitet fordi **du kan implementere flere grensesnitt, kun arve fra en klasse**, som gjør at man kan ha mer kompleks adferd i klassene våre representert enkelt. Nå som vi har noen motorer å bruke, la oss komponere vår `MotorizedBicycle` med en `Engine`:

```java
public class MotorizedBicycle extends Bicycle {
     private final Engine engine;

    public MotorizedBicycle(String brand, int riders, double weight, Engine engine) {
        super(brand, riders, weight);
        this.engine = engine;
    }
    // Extended methods
    public void startEngine() {
        engine.start();
    }

    public void stopEngine() {
        engine.stop();
    }

    public void refuel(double amount) {
        engine.refuel(amount);
    }

    public double getFuelLevel() {
        return engine.getFuelLevel();
    }

    public boolean checkIfNeedsService() {
        return engine.needsService();
    }

    public void serviceEngine() {
        engine.service();
    }

    // Inherited methods
    @Override
    public String getDetails() {
        return "A motorized bicycle for those with weak legs";
    }

    @Override
    public void accelerate() {
        if(!engine.isRunning())
            engine.start();
        try {
            engine.useFuel();
            setSpeed(getSpeed()+1);
        } catch (OutOfFuelException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void brake() {
        setSpeed(getSpeed()-1);
    }
}
```

Merk at vi ikke trenger å sjekke om `Engine` er elektrisk eller bensindrevet, klassen bryr seg ikke. Den vet bare hvilke metoder som er implementert. De utvidede metodene for å imøtekomme motoren henivser ansvaret til grensesnittet.

> **MERK**: Å ha felttypen som `Engine` og tilordne en klasse som implementerer `Engine` gjøres via polymorfisme og fungerer på samme måte som med arvede klasser.
> 

Ved å komponere klassen vår med en abstraksjon, har vi fleksibiliteten til å legge til flere *Engines* i fremtiden uten å måtte endre denne klassen. Programmering på denne måten resulterer i mer modulær kode.

> **MERK**: Koding på denne måten gjøres ved å følge et prinsipp kalt *Program to abstractions*. Det gjøres for å avklare ansvar, og dekkes i leksjonen om objektorientert design.
> 

Før vi går videre og legger mer til eksemplet vårt, la oss stoppe for å se hva vi har laget så langt. Vi har:

- En grunnleggende abstrakt `Bicycle`-klasse for å representere alle typer sykler.
- To subklasser, `BMX` og`Motorized`, som utvider `Bicycle`.
- `Motorized` er komponert med en `Engine` som legger til klassenes oppførsel
- `Engine` er en abstraksjon (grensesnitt i dette tilfellet) som representerer enhver type *Engine*
- Vi har `Electric` og `Petrol` som implementerer `Engine`-grensesnittet. Disse klassene oppfører seg forskjellig og har ulik tilstand
- `Motorized` overdrar ansvaret for å bruke *Engine* til den sammensatte `Engine` som sendes gjennom med en konstruktør.

Diagrammet nedenfor illustrerer hvordan disse klassene samhandler:

![Untitled](Objektorientert%20programmering%20b8da1130f7be4d5f85587eff1d7f36c2/Untitled.png)

> INFO: Diagrammet ovenfor bruker UML (Unified Modeling Language). Det undervises ikke i dette kurset, men er verdt å gjøre seg kjent med.
> 

En god utvidelse av applikasjonen vår er å legge til et grensesnitt for å gjøre det mulig for *Bicycles* å ha gir og skifte gjennom dem. Dette kan bidra til å skille BMX fra en terrengsykkel, for eksempel. Repositoriet for leksjonen inneholder denne utvidelsen.

## Java-refleksjon

I Java lar refleksjon oss inspisere og manipulere klasser, grensesnitt, konstruktører, metoder og felt under kjøring.

Det er en klasse i Java som heter `Class` som holder all informasjon om objekter og klasser under kjøring. Objekter av typen `Class` kan brukes til å utføre refleksjon.

Å lage en `Class`-forekomst av et objekt kan gjøres på tre måter:

```java
// Using forName() method
Class a = Class.forName("BMX");
// Using getClass() method
BMX foo = new BMX();
Class b = foo.getClass();
// Using .class extension
Class c = BMX.class;
```

Vi kan bruke dette objektet til å få informasjon om den korresponderende klassen ved runtime:

```java
import java.lang.reflect.Modifier;
...
public static void main(String[] args) {

    // Get instance of class
    Class obj = BMX.class;

    // Get name of the class
    String name = obj.getName();
    System.out.println("Name: " + name);

    // Get the access modifier of the class
    int modifier = obj.getModifiers();

    // Convert the access modifier to string
    String mod = Modifier.toString(modifier);
    System.out.println("Modifier: " + mod);

    // Get the superclass of Dog
    Class superClass = obj.getSuperclass();
    System.out.println("Superclass: " + superClass.getName());

    // OUTPUT: 
    // Name: no.noroff.accelerate.bicycles.BMX
    // Modifier: public
    // Superclass: no.noroff.accelerate.bicycles.Bicycle
```

Brukstilfeller for refleksjon er svært begrenset i dag-til-dag programmering. Det er mye brukt i rammeverk som **Spring** som gjør dem i stand til å jobbe med ethvert objekt.

## Oppsummering

- Objektorientert programmering er et paradigme for å lage programmer ved hjelp av klasser og objekter
- Klasser er blåkopier for å lage objekter. Klasser har felt for å representere tilstand, og metoder for å representere atferd
- Objekter er forekomster i minnet av klasser med egen tilstand
- OOP bryter opp komplekse problemer i mindre objekter
- OOP fremmer DRY-programmering gjennom modularisering
- Abstraksjon eksponerer kun *high-level* offentlige metoder for tilgang
- Innkapsling skjuler informasjonen til et objekt, og eksponerer bare det som kreves
- Arv lar subklasser arve data og atferd fra en superklasse
- Polymorfisme refererer til å gjøre én ting på forskjellige måter
    - Statisk polymorfisme er metodeoverbelastning
    - Dynamisk polymorfisme er metodeoverstyring
    - Polymorfe typer tillater interaksjon med klasser gjennom supertyper eller implementerte grensesnitt
- Objekter kan assosieres med hverandre
    - Aggregasjon er et selvstendig forhold
    - Komposisjon er et avhengighetsforhold
- Grensesnitt gir en kontrakt som inneholder metoder for å implementering
- Grensesnitt brukes for å gi atferd til klasser
- Klasser kan implementere flere grensesnitt, men bare utvide én klasse

---

Copyright 2021, Noroff Accelerate AS