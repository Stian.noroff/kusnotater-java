# Enhetstesting

# Enhetstesting

Denne leksjonen introduserer enhetstesting (unit testing) og testdrevet utvikling (Test-Driven Development - TDD). Leksjonen dekker hvorfor vi tester, hvordan vi kan teste og hvordan man skriver enhetstester på riktig måte.

**Læringsmål:**

Du vil kunne…

- **diskutere** programvaretesting
- **beskrive** en velstrukturert enhetstest
- **formulere** enhetstester for møte krav, gitt en designspesifikasjon.
- **utvikle** eller **endre** kode for å bestå enhetstestene, gitt et sett med enhetstester
- **resitere** de tre lovene om testdrevet utvikling (TDD)
- **diskutere** rollen av TDD i industrien

---

## Introduksjon

Programvaretesting bekrefter at applikasjonene våre fungerer etter hensikten. Den har mange former og passer til ulike omfang og stadier en applikasjon går gjennom i løpet av livssyklusen. Mangler i funksjonalitet identifiseres med testing, slik at utviklere kan fokusere på nøkkelaspekter ved applikasjonen.

> MERK: Testing spiller en kritisk rolle i å hjelpe et programvareselskap levere produkter av høy kvalitet ti sine kunder.
> 

Når man ser på programvaretesting som en helhet, er det to hovedtilnærminger:

- Hvit boks - indre logikk testing
- Svart boks - testing av avansert funksjonalitet eksponert for brukeren

Et eksempel på en hvit boks test er å verifisere at en metode fungerer korrekt. Et eksempel på en svart boks test er å se hvis en bruker kan logge inn på vår applikasjon. Alle tester kan kategoriseres som enten funksjonell, ikke-funksjonell eller vedlikehold.
 

Som utviklere fokuserer vi vanligvis på funksjo nstester. Når vi ser på funksjonstester har vi:

- Enhetstester - testing av individuelle programvareenheter
- Integrasjonstester - testing av flere enheter som jobber sammen
- Systemtester - hele applikasjonen blir testet for funksjonalitet og sikkerhet

Effektive tester krever mye ressurser, men resulterer i:

- Fanging av feil tidlig
- Eksponering av sikkerhetsfeil
- Forbedret kvalitet
- En klar utviklingssti

Når man tester en applikasjon, er det flere stadier involvert:

- Alpha - Kjernefunksjonalitet testes internt eller med en liten utenforstående gruppe
- Beta - hele applikasjonen testes med et mye større offentlig publikum
- Stress - Fullført applikasjon testes under stor belastning for å identifisere svake punkter

## Enhetstesting i Java

**JUnit** er testrammeverket for Java, nå i versjon **5**. Det spiller en kritisk rolle i testdrevet utvikling og er en del av en familie av enhetstesting-rammeverk, samlet kjent som xUnit. JUnit er svært integrert i IntelliJ og er trivielt å integrere i ethvert prosjekt.

JUnit fremmer ideen om “test først så kode”, som legger vekt på å sette opp testdataene for et stykke kode før funksjonaliteten implementeres. Denne metoden kan bli sett på som “test litt, kode litt, test litt”. Det øker produktiviteten til programmereren og stabiliteten til programkoden, som igjen reduserer stresset på programmereren og tiden brukt på feilsøkrin.

> **RESSURS**: JUnit 5 referanseguiden kan bli funnet [her](https://junit.org/junit5/docs/current/user-guide/).
> 

Nøkkelpunkter for JUnit:

- Et open-source rammeverk, som brukes til å skrive og kjøre tester
- Gir merknader for å identifisere testmetoder.
- Gir påstander for forventede testresultater.
- Gir testkjørere for å kjøre tester.
- Tester kan bli kjørt automatisk. De sjekker resultatet og gir umiddelbar tilbakemelding.
- Tester kan organiseres i testpakker som inneholder test-caser og til og med andre testpakker.
- Testfremgang vises i en linje som er grønn hvis testen kjører fint, og blir rød når en test feiler.

### JUnit med IntelliJ

Når vi har et prosjekt som må inkludere enhetstester, trenger vi et par ting:

- En klasse å teste
- En testkildemappe
- JUnit 5 avhengighet

For å legge til en testkildemappe:

1. `Høyreklikk` på roten til prosjektet i Intellij
2. `New` > `Directory`
3. Gi det navnet *tester* eller noe passende
4. På den nye mappen `Høyreklikk` > `Mark directory as` > `Test sources root`

Herfra går vi til klassen vi ønsker å lage tester for:

1. `Høyreklikk` hvor som helst i klassen
2. `Generate...` > `Test...`
3. Sørg for at `Testing Library` er **JUnit 5**
4. Klikk `Fix`. Dette legger JUnit 5-avhengigheten til prosjektet vårt.
5. *VALGFRITT*: Velg metoder å generere tester for. Kan hoppes over, vi vil uansett skrive våre egne tester.

Dette vil opprette en klasse og mappe i vår merkede testkilderotmappe.

Som et eksempel, la oss lage en *kalkulator med tester*. Først setter vi testkilderotmappen:

![Untitled](Enhetstesting%20ceb22792c5e94aeaa691ea41d883447f/Untitled.png)

Neste steg er å inkludere `JUnit` og generere en testklasse

![Untitled](Enhetstesting%20ceb22792c5e94aeaa691ea41d883447f/Untitled%201.png)

Dette resulterer i følgende mappestruktur med en tom testklasse for at vi kan begynne å skrive tester:

![Untitled](Enhetstesting%20ceb22792c5e94aeaa691ea41d883447f/Untitled%202.png)

### Skrive en enhetstest

Et enhetstesttilfelle har en kjent *input* og en forventet *output*, kjent på forhånd. Målet er å se om den **faktiske** utgangen samsvarer med den **forventede** utgangen.

### Hvor mange tester bør vi skrive?

Når det gjelder hvor mange testmetoder og testklasser, er dette generelle regler som skal følges:

- Én testklasse per enhet
- Minst to testmetoder for hver offentlig vendt metode, hvis mulig
    - Test gyldige og ugyldige input
    - Test for unntak som kaster kanttilfeller

Hver testmetode må merkes med `@Test`-merknaden. Dette markerer metoden som en test som kan kjøres gjennom testkjøreren.

En test bør ha et klart navn. En god praksis er å ha testmetodenavnet delt inn i tre deler:

```
Operation_condition_expectedoutcome
```

For eksempel, hvis vi ønsket å lage en testmetode for å se om vår `StandardCalculator` adderer riktig, kan vi ha følgende testmetode:

```java
class StandardCalculatorTest {
    @Test
    public void add_validInputs_shouldReturnSum() {
        // Test body
    }
}
```

### Hvordan strukturere en test

En testmetodes *body* er skrevet for å maksimere lesbarheten. I motsetning til i andre tilfeller, må vi være så detaljerte som mulig. Å være detaljert reduserer sjansen for å introdusere en feil i enhetstesten. En enhetstest bør også tydelig forklare funksjonaliteten den tester gjennom kun sin syntaks.

TFor å oppnå dette er det en vanlig tilnærming kalt **AAA (Arrange, Act, Assert):**

- **Arrange** - konfigurer all tilstand som trengs for testen
- **Act** - utfør funksjonaliteten for å samle den faktiske outputen
- **Assert** - se om den faktiske output samsvarer med vår forventede output

Denne enkle tilnærmingen kombinert med andre mindre mønsterpraksiser bidrar til å sikre at enhetstestene våre er klare. Kodeutdraget nedenfor illustrerer test-*body* for testmetoden ovenfor:

```java
import static org.junit.jupiter.api.Assertions.assertEquals;

class StandardCalculatorTest {
    @Test
    public void add_validInputs_shouldReturnSum() {
        // Arrange
        int lhs = 1;
        int rhs = 1;
        int expected = lhs+rhs;
        StandardCalculator standardCalculator = new StandardCalculator();
        // Act
        int actual = standardCalculator.add(lhs,rhs);
        // Assert
        assertEquals(expected,actual);
    }
}
```

Dette kodeutdraget viser flere mønsterpraksiser knyttet til å skrive en enhetest:

```java
int expected = lhs+rhs;
```

Å skrive kode på denne måten er ganske detaljert og normalt ikke noe en utvikler vil skrive utenfor en enhetstest. Hensikten er å formidle funksjonaliteten som er testet. Dette trivielle eksempelet viser dette ved å teste aritmetikk:

```java
int actual = standardCalculator.add(lhs,rhs);
```

Her sender vi de faktiske inputs og ikke `1` og `1`. Hvis vi hadde sendt tallene, ville vi brukt noe som heter *magiske variabler*. De kalles magiske fordi man er usikker på hvor de kom fra. Nok en gang er dette normalt noe vi ikke bryr oss om i konvensjonell programmering, men når vi tester, må vi være sikre på at vi sender de riktige variablene for den spesifikke tilstanden.

Valg av parametere er viktig. En mønsterpraksis er å skrive *minimum bestått tester*. Dette betyr at i stedet for å teste hver variasjon av å legge til to tall, tar vi den enkleste bruken og tester den. Vi antar at hvis den kan bestå den enkleste testen, kan den bestå hvilken som helst test - unntakskastende kanttilfeller ikke medregnet. 

Med påstander er `assertEquals` normalt det beste alternativet, men hvis man har et tilpasset objekt, vil ikke `assertEquals` fungere som forventet. For objekter vil `assertEquals` sjekke om det er samme forekomst. Dette fungerer på samme måte som *strenglikhet* fungerte i en tidligere leksjon. I disse tilfellene, generer `equals()`metoden for klassen og skriv følgende:

```java
assertTrue(expected.equals(actual));
```

Dette vil sammenligne verdiene til forekomstene i stedet. Det er en mønsterpraksis å ha bare én *assert* per testmetode. Dette gjøres for å unngå å skrive lange, komplekse tester til fordel for mindre, mer fokuserte tester. Noen ganger kan ikke dette oppnås, men streb etter så få påstander som mulig. Et godt eksempel på å ha flere *assert* er når vi må teste for unntakskasting. Kodeutdraget nedenfor illustrerer dette:

```java
@Test
public void divide_zeroDenominator_shouldThrowArithmeticException() {
    // Arrange
    int numerator = 1;
    int denominator = 0;
    StandardCalculator standardCalculator = new StandardCalculator();
    String expected = "Cant divide by zero";
    // Act & Assert
    Exception exception = assertThrows(ArithmeticException.class,
    () -> standardCalculator.divide(numerator, denominator));
    String actual = exception.getMessage();
    assertEquals(expected, actual);
}
```

Vi har to *asserts*: en for å sjekke om metoden kastet riktig unntak og en annen for å sjekke om unntaket har riktig melding. Det finnes måter å omgå dette på, men det er enklere å bare ha disse to. `assertThrows`bruker *reflection* for å se om typen unntak er et `ArithmaticException` med `ArithmeticException.class`. Den bruker deretter *polymorfisme* for å behandle det som et `Exception` og kalle `getMessage()`-metoden.

Oppsummert, bruk følgende retningslinjer når du skriver enhetstester:

- Nevn testmetodene dine for å forklare hvilken funksjonalitet som testes, hvordan inngangene ser ut og hva som er forventet utfall.
- Bruk AAA-tilnærmingen for å organisere test-*body*.
- Ikke bruk magiske variabler. Merk alltid inputene dine tydelig.
- Skriv minimum beståtte t ester. Bruk de enkleste inputs for å få de ønskede outputs.
- Tilstrebe én påstand per testmetode.
- Fokus på kodens lesbarhet og å være detaljert.

> **RESSURS**: Ytterligere JUnits mønsterpraksiser finner du [her](https://howtodoinjava.com/best-practices/unit-testing-best-practices-junit-reference-guide/).
> 

### Kjøre tester

JUnit har en innebygd test-kjører. Denne vil automatisk kjøre den valgte testen og vise en utgangsverdi. Du kan kjøre:

- En spesifikk testmetode ved å høyreklikke på navnet og velge `Run 'test name'`.
- Alle testmetoder i en testklasse kan kjøres ved å høyreklikke på klassen og velge `Run 'test class name'`.
- Alle testmetoder i alle testklasser kan kjøres ved å høyreklikke på testkilderot-mappen og velge `Run 'All Tests'`.

Figuren under viser alle gyldige testmuligheter:

![Untitled](Enhetstesting%20ceb22792c5e94aeaa691ea41d883447f/Untitled%203.png)

Når en test mislykkes, forklarer test-kjøreren hva som gikk galt, figuren nedenfor illustrerer dette:

![Untitled](Enhetstesting%20ceb22792c5e94aeaa691ea41d883447f/Untitled%204.png)

## Testdrevet utvikling

Testdrevet utvikling(TDD) er en programvareutviklings-tilnærming der testtilfeller utvikles for å definere funksjonaliteten før kode skrives. Testtilfeller for hver funksjonalitet opprettes og testes først, hvis testen mislykkes skrives den nye koden for å bestå testen. Dette resulterer i enkel, feilfri kode. Testdrevet utvikling har 3 lover:

1. Skriv kun produksjonskode når du må adressere en prøve som har feilet
2. Skriv kun nok av en test til at den kan mislykke
3. Skriv kun nok kode til at en feilende test kan bestå

Testdrevet utvikling starter med å designe og utvikle tester for funksjonaliteten til en applikasjon. TDD-rammeverket instruerer utviklere til å skrive ny kode bare hvis en automatisert test har mislyktes. Dette unngår duplisering av kode.

Det enkle konseptet med TDD er å skrive og korrigere de mislykkede testene før du skriver ny kode. Dette bidrar til å unngå duplisering av kode da vi skriver en liten mengde kode om gangen for å bestå tester. Dette sikrer at vi bare skriver så mye kode som er nødvendig for å oppnå funksjonalitet.

> MERK: Testdrevet utvikling er en prosess med å utvikle og kjøre automatiserte tester før selve utviklingen av applikasjonen. TDD (Test-Driven Development) kalles noen ganger også Test First Development.
> 

Følgende steg definerer hvordan man utfører en TDD-test:

- Skriv en test
- Få den til å kjøre.
- Endre koden for å gjøre den korrekt, dvs. refaktorering.
- Gjenta prosessen.

> **MERK**: Kan sees på som en rød-grønn-refaktoreringssyklus.
> 

Nøkkelpunkter for TDD:

- En feilet test finner en eller flere defekter. Fremskritt har blitt gjort, fordi du vet at du må løse problemet.
- Det sikrer at systemet ditt oppfyller kravene som er definert for det. Det bidrar til å bygge opp din tillit til systemet ditt.
- Mer fokus er på produksjonskode som verifiserer om testing vil fungere som den skal.
- Den oppnår 100 % testdekning. Hver eneste kodelinje blir testet ettersom den ble utviklet for å bestå en test.
- Å teste med formål bidrar til å unngå meningsløse tester.
- Det forkorter feedback-loopen. TDD ble utviklet med tanke på Agile metodikk.

TDD er en tidkrevende prosess, men er fordelaktig på følgende måter:

- Tidlig varsling om feilmeldinger.
- Bedre designet, renere og mer utvidbar kode.
    - Forstå hvordan kode brukes og hvordan den samhandler med andre moduler.
    - Resulterer i bedre designbeslutninger og kode som er enkel å vedlikeholde. Fremmer DRY-programmering.
    - Mindre moduler med kode med tydelig ansvar, i stedet for at store monolittiske moduler gjør mange ting. Dette resulterer i enklere og mer forståelig kode.
    - Tvinger produksjonskode til å skrives for å overholde brukerkravene. Innretting med et annet programmeringsprinsipp kalt YAGNI (You ain't gonna need it).
- Tillit til refaktorering
    - Refaktorering medfører risiko for å ødelegge funksjonaliteten. Når det er en automatisert testpakke som dekker 100 % av koden som er skrevet, er sjansen liten for at en ødeleggende refaktorering går ubemerket hen.
    - Koden din bør være enkel å utvide.
- Bra for teamarbeid
    - I fravær av et teammedlem kan andre teammedlemmer enkelt plukke opp og jobbe med koden.
    - Det hjelper også med kunnskapsdeling, og gjør teamet mer effektivt totalt sett.
- Bra for utviklere
    - Det brukes mer tid på å skrive test caser, men enda mer tid spares på feilsøking eller utvikling av nye funksjoner.
    - Du vil skrive renere, mindre komplisert kode.
    - Reduserer den totale utviklingstiden.
- Sørger for dokumentasjon.
    - Når du skriver tester for spesielle krav, lager programmerere umiddelbart en streng og detaljert spesifikasjon som inkluderer alle de sannsynlige brukernes handlinger.
- Går hånd i hånd med **Agile** programvareutviklingsmetoden.

Med alle disse positive sidene er det bare rettferdig å snakke om noen negative:

- Det øker den initielle utviklingstiden drastisk.
    - Hvis det er en streng tidsbegrensning på en prototype, kan TDD være mer en hindring enn en fordel.
- Det er en «alt eller ingenting»-tilnærming.
    - Enten er alt TDD eller så er ingenting. Du kan fortsatt ha enhetstester, men utviklingsfilosofien er annerledes.
    - Hver utvikler på laget må utføre det.
- Vanskelig å lære.
    - Det er et helt annet paradigme som krever at spesifikke prosesser læres. Ikke alle utviklere vet hvordan de skal utvikle seg på denne måten eller ønsker det.
- Vanskelig å tilpasse seg.
    - Hvis kravene endres, er det utfordrende å endre testene.
- Vanskelig å tilpasse seg.
    - Det er ingen garanti for null feil ved bruk av TDD, det fremhever bare feil bedre.
    - Når applikasjoner blir mer komplekse, må avhengigheter forfalskes. Dette kan være en utfordrende prosess.
    - Testing må være deterministisk, noe som betyr at vi ikke kan teste live data på dette nivået. Dette er ikke bare et TDD-problem, men et problem med testing generelt.

De fleste seniorutviklere er enige om at TDD til en viss grad er verdt det. Det er også gjort mye forskning på hvorvidt TDD er gunstig eller ikke. Den generelle aksepten fra forskning er at det øker hastigheten på den totale utviklingstiden, reduserer kostnadene og produserer færre feil.

### TDD eksempel: FizzBuzz

La oss se TDD i aksjon. Vi skal utvikle en **FizzBuzz-konverter** med TDD.

FizzBuzz er et vanlig programmeringsintervjuspørsmål der du må lage en funksjon som tar inn et heltall og produserer en konvertert verdi.

Logikken er som følger:

- Hvis tallet er et multiplum av 3, returner "Fizz".
- Hvis tallet er et multiplum av 5, returner "Buzz".
- Hvis tallet er et multiplum av både 3 og 5, returner "FizzBuzz".
- Hvis det ikke er noen av disse, returner nummeret.

Et eksempel på utdata fra `FizzBuzz` er følgende:

Gitt:

```
1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20
```

Produserer:

```
1,2,Fizz,4,Buzz,Fizz,7,8,Fizz,Buzz,11,Fizz,13,14,FizzBuzz,16,17,Fizz,19,Buzz
```

> **MERK**: Algoritmen for dette problemet er ikke komplisert. Eksemplet vil nærme seg problemet uten å anta at løsningen er kjent. Dette demonstrerer hva TDD tilbyr oss - en måte å lage algoritmer uten å tenke på dem på forhånd.
> 

### Sette opp arbeidsflyten

Hvis vi ønsker å utvikle dette på en TDD-måte, må vi sørge for å gjøre følgende:

- Start med ingenting
- Skriv en test som feiler
- Skriv kode som består testen
- Gå videre/refaktorer

Dette er illustrert nedenfor:

![Untitled](Enhetstesting%20ceb22792c5e94aeaa691ea41d883447f/Untitled%205.png)

Vi lager et Java-program, og deretter `Generate... > Test..` som før. Følgende vil vises:

![Untitled](Enhetstesting%20ceb22792c5e94aeaa691ea41d883447f/Untitled%206.png)

Når du klikker på `Fix` og `Ok`, skal prosjektstrukturen din ligne på følgende:

![Untitled](Enhetstesting%20ceb22792c5e94aeaa691ea41d883447f/Untitled%207.png)

Vi kan begynne å skrive tester. Den første test-casen vi ønsker å skrive henvender seg til et ikke-FizzBuzz-nummer:

```
Input: 1, Output: "1"
```

> **MERK**: Vi ønsker å skrive minimalt med beståtte tester, så å bestå 1 er den enkleste måten vi kan dekke et nummer som ikke er FizzBuzz. Vi antar at hvis det fungerer for 1, fungerer det for 2,4,6,7,…
> 

Kodeutdraget nedenfor illustrerer så langt vi kan komme før testen brytes:

```java
class FizzBuzzTest {
    @Test
    public void convertFizzBuzz_nonFBNumber_shouldReturnNumberAsString() {
        // Arrange
        int input = 1;
        String expected = "1";
        FizzBuzzConverter fbConverter = new FizzBuzzConverter();
    }
}
```

Dette er fordi vår `FizzBuzzConverter`-klasse ikke eksisterer. Vi må lage den, kodeutdraget nedenfor illustrerer dette:

```java
package no.noroff.accelerate.util;

public class FizzBuzzConverter {
}
```

> **MERK**: Vi skriver ikke mer kode enn nødvendig for å fjerne feil.
> 

Nå kan vi fortsette med testkoden vår og legge til følgende linje:

```java
// Act
String actual = fbConverter.convert(input);
```

Dette bryter testen vår, siden det ikke finnes noen convert-metode for `FizzBuzzConverter`-klassen. For å rette opp i dette legger vi til følgende metode:

```java
public class FizzBuzzConverter {
    public String convert(int input) {
        return null;
    }
}
```

Nok en gang skriver vi ikke mer kode enn nødvendig. Nå skal vi være i stand til å fullføre testen:

```java
@Test
public void convertFizzBuzz_nonFBNumber_shouldReturnNumberAsString() {
    // Arrange
    int input = 1;
    String expected = "1";
    FizzBuzzConverter fbConverter = new FizzBuzzConverter();
    // Act
    String actual = fbConverter.convert(input);
    // Assert
    assertEquals(expected,actual);
}
```

Når vi kjører denne testen, får vi følgende output:

```bash
org.opentest4j.AssertionFailedError: 
Expected :1
Actual   :null
```

La oss fikse dette ved å gå inn i konverteringsmetoden vår og endre den:

```java
public String convert(int input) {
    return String.valueOf(input);
}
```

Testen vår består. På dette tidspunktet går vi videre. Neste testtilfelle er når et tall er et multiplum av 3, da returnerer vi "Fizz". Testen for dette er som følger:

```java
@Test
public void convertFizzBuzz_fizzNumber_shouldReturnFizz() {
    // Arrange
    int input = 3;
    String expected = "Fizz";
    FizzBuzzConverter fbConverter = new FizzBuzzConverter();
    // Act
    String actual = fbConverter.convert(input);
    // Assert
    assertEquals(expected,actual);
}
```

> **MERK**: Vi bruker en ny forekomst av FizzBuzzConverter for hver test for å unngå potensielle endringer i forekomsten fra andre tester. Dette kan også gjøres i en dedikert metode med @Before eller @BeforeAll-kommentaren.
> 

Dette resulterer i ingen syntaksfeil, så vi kan kjøre testen. Når vi kjører testen, vises følgende output:

```bash
org.opentest4j.AssertionFailedError: 
Expected :Fizz
Actual   :3
```

Den feilede testen har fremhevet et problem, vi sjekket ikke om tallet var et multiplum av 3. Å bruke modulo-operatoren(%) er den enkleste måten å oppnå dette på. Den returnerer resten av en divisjonsoperasjon. For eksempel, `4%3=1`, fordi 4 delt på 3 gir en rest av 1. Dette lar oss refaktorere konverteringsmetoden vår:

```java
public String convert(int input) {
    if(input%3 == 0)
        return "Fizz";
    return String.valueOf(input);
}
```

Alle testene våre består nå. Den neste test-casen sjekker for multipler av 5:

```java
@Test
public void convertFizzBuzz_buzzNumber_shouldReturnBuzz() {
    // Arrange
    int input = 5;
    String expected = "Buzz";
    FizzBuzzConverter fbConverter = new FizzBuzzConverter();
    // Act
    String actual = fbConverter.convert(input);
    // Assert
    assertEquals(expected,actual);
}
```

Dette resulterer i ingen syntaksfeil, så vi kan kjøre testen. Når vi kjører testen, vises følgende utdata:

```bash
org.opentest4j.AssertionFailedError: 
Expected :Buzz
Actual   :5
```

Vi kan refaktorisere konverteringsmetoden vår på lignende måte som vi gjorde for å imøtekomme multipler på 3:

```java
public String convert(int input) {
    if(input%3 == 0)
        return "Fizz";
    if(input%5 == 0)
        return "Buzz";
    return String.valueOf(input);
}
```

Alle testene våre består nå. Det siste trinnet er delelig med både 3 og 5. Vår input her vil være 15. Hvorfor? 15 er den laveste fellesnevneren (LCD) mellom 3 og 5, gjort ved å multiplisere de to tallene sammen `(3*5=15)`:

```java
@Test
public void convertFizzBuzz_fizzBuzzNumber_shouldReturnFizzBuzz() {
    // Arrange
    int input = 15;
    String expected = "FizzBuzz";
    FizzBuzzConverter fbConverter = new FizzBuzzConverter();
    // Act
    String actual = fbConverter.convert(input);
    // Assert
    assertEquals(expected,actual);
}
```

Dette resulterer i ingen syntaksfeil, så vi kan kjøre testen. Når vi kjører testen, vises følgende output:

```bash
org.opentest4j.AssertionFailedError: 
Expected :FizzBuzz
Actual   :Fizz
```

Vi kan refaktorisere konverteringsmetoden vår på lignende måte som vi gjorde for å imøtekomme tidligere multipler:

```java
public String convert(int input) {
    if(input%3 == 0)
        return "Fizz";
    if(input%5 == 0)
        return "Buzz";
    if(input%3 == 0 && input%5 == 0)
        return "FizzBuzz";
    return String.valueOf(input);
}
```

Her har vi en ekstra erklæring som sjekker om tallet er begge de to første betingelsene. Når vi kjører testen, produseres følgende utdata:

```bash
org.opentest4j.AssertionFailedError:Expected :FizzBuzzActual   :Fizz
```

Det ser ut til at vi ikke har løst problemet. På dette tidspunktet kan vi sette et bruddpunkt i konverteringsmetoden vår. Kjør deretter testen med debugging og gå inn i metoden vår for å se linje for linje hva som skjer:

![Untitled](Enhetstesting%20ceb22792c5e94aeaa691ea41d883447f/Untitled%208.png)

Her ser vi at koden vår utfører `return` på linje 6. Dette er fordi `15%3=0`, fordi 3 går inn i 15, 5 ganger uten rest. Vi innser at vi bør flytte tilstanden vår på linje 9 opp til begynnelsen slik at logikken vår ikke går ut tidlig. Refaktorering av metoden vår gir oss følgende:

```java
public String convert(int input) {
    if(input%3 == 0 && input%5 == 0)
        return "FizzBuzz";
    if(input%3 == 0)
        return "Fizz";
    if(input%5 == 0)
        return "Buzz";
    return String.valueOf(input);
}
```

Nå består alle testene våre. Vi har dekket alle våre test-caser og kan begynne å refaktorere. En enkel refaktorering er vår sjekk om tallet er delelig med både 3 og 5. Fordi vi vet at 15 er en fellesnevner, kan vi sjekke om tallet er delelig med 15 for å redusere antall betingelser i if-erklæringen:

```java
public class FizzBuzzConverter {
    public String convert(int input) {
        if(input%15 == 0)
            return "FizzBuzz";
        if(input%3 == 0)
            return "Fizz";
        if(input%5 == 0)
            return "Buzz";
        return String.valueOf(input);
    }
}
```

Hvis vi kjører testene våre på nytt, består alle:

![Untitled](Enhetstesting%20ceb22792c5e94aeaa691ea41d883447f/Untitled%209.png)

På dette punktet er vi fornøyd med koden vår. Vi kan gjøre en commit til vår branch og er klar til å merge inn i produksjon. Vi kan så gå videre, og vite at: koden vår fungerer, den er 100% testet, den gjør akkurat det som trengs og ingenting mer.

## Sammendrag

- Testing innebærer å sammenligne forventet utgang med faktisk utgang.
- Testing kan gjøres på mange ulike nivåer og på mange ulike stadier.
    - Vårt fokus er Unit-testing i utviklingsstadiet.
- JUnit er Javas foretrukne rammeverk for enhetstesting.
- Tester bør lagres i en dedikert rotmappe for testkilden
- Test methods should be clearly named.
    - *MethodTested_Conditions_ExpectedOutcome*
- Testmetode-body bør struktureres for å fremme lesbarhet og klarhet
    - AAA metoden.
    - Ingen magiske variabler.
    - Minimum beståtte tester.
    - Så få asserts som mulig, gjerne én per test.
- TDD er et paradigme hvor man skriver produksjonskode for å bestå tester.
    - Test først og så kode.
    - Resultater i full testdekning.
    - Små og fokuserte klasser.
- TDD har mange fordeler og ulemper, men anses fortsatt som verdt det.
    - Det fremskynder den totale utviklingstiden.
    - Resulterer i færre feil.
    - Vanskelig å lære.
    - Rigid.

> **RESSURS**: [Robert Martin demonstrerer TDD](https://www.youtube.com/watch?v=AoIfc5NwRks)
> 

---

Copyright 2021, Noroff Accelerate AS